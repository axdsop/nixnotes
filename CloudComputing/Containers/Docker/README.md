# Docker Introduction

## Install

### Docker

```bash
curl -fsSL https://gitlab.com/axdsop/nixnotes/raw/master/ShellScripts/Toolkits/Application/Container/Docker-CE.sh | sudo bash -s -- -h
```


### Docker Compose

[Docker Compose][docker_compose] (compose v2 architecture and installation instructions differs from v1, see [release note](https://github.com/docker/compose/releases/tag/v2.0.0)) is a tool for defining and running multi-container Docker applications. Official install doc [Install Docker Compose](https://docs.docker.com/compose/install/).

```sh
download_command='wget -qO-'
[[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && download_command='curl -fsSL'

${download_command} https://gitlab.com/axdsop/nixnotes/raw/master/CloudComputing/Containers/Docker/Scripts/docker_compose.sh | sudo bash -s --
```

Since Docker [Compose v2](https://github.com/docker/compose/releases/tag/v2.0.0) (Sep 28, 2021), command change from `docker-compose` to `docker compose`, more details see its GitHub [page](https://github.com/docker/compose#where-to-get-docker-compose).

Check compose version

```sh
# For compose V2
$ docker compose version
# Docker Compose version v2.0.1

# For compose V1
$ docker-compose version
# docker-compose version 1.28.5, build c4eb3a1f
# docker-py version: 4.4.4
# CPython version: 3.7.10
# OpenSSL version: OpenSSL 1.1.0l  10 Sep 2019
```


## Snippets

Print running containers info

```sh
# https://docs.docker.com/config/formatting/
# docker inspect #  Return low-level information on Docker objects
docker ps --format "{{.Names}}|{{.ID}}" | while IFS="|" read -r s_name c_id; do
    ip_internal=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' "${c_id}" 2> /dev/null)
    # service name|container id|internal ip
    echo "${s_name}|${c_id}|${ip_internal}"
done
```


## Tools

```sh
download_command='wget -qO-'
[[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && download_command='curl -fsSL'
```


[docker]: https://www.docker.com
[docker_compose]:https://docs.docker.com/compose/

<!-- End -->