#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails

# Target: Install/Update docker compose (V2) on GNU/Linux x86_64.
# Writer: MaxdSre

# Change Log
# - Mar 11, 2022 Fri 15:30 ET - rewrite, add selection menu for release version
# - Oct 03, 2021 Sun 19:03 ET - upgrade compose installation method to v2
# - Sep 09, 2020 Wed 06:05 ET - First draft

# Official Doc
# - https://github.com/docker/compose
# - https://docs.docker.com/compose/cli-command/
# - https://docs.docker.com/compose/install/
# - https://docs.docker.com/compose/completion/ (V1)
# - https://api.github.com/repos/docker/compose/releases/latest
# - https://docs.github.com/en/rest/reference/releases


# https://api.github.com/repos/docker/compose/releases
# https://api.github.com/repos/docker/compose/releases/latest
# https://api.github.com/repos/docker/compose/releases/tags/v2.3.3


#########  0-1. Variables Setting  #########
readonly select_num_limit=15
download_method=${download_method:-} # curl -fsL / wget -qO-
version_check=${version_check:-0}
version_specify=${version_specify:-0}
version_choose=${version_choose:-}

readonly docker_compost_github_api='https://api.github.com/repos/docker/compose/releases'

#########  0-2. getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | [sudo] bash -s -- [options] ...

Install/Update docker compose (V2) on GNU/Linux x86_64

Release page: https://github.com/docker/compose/releases

Note: release versions (v2) just show the latest ${select_num_limit} items.

[available option]
    -h    --help, show help info
    -s    --select a specific release version from givien list
    -c    --check the latest release version
\e[0m"
}

while getopts "hcs" option "$@"; do
    case "$option" in
        c ) version_check=1 ;;
        s ) version_specify=1 ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done


#########  0-3 Custom Functions  #########
fn_ExitStatement(){
    local l_str="$*"
    [[ -n "${l_str}" ]] && echo -e "${l_str}\n"
    exit 0
}

fn_CommandExistIfCheck(){
    # $? -- 0 is find, 1 is not find
    local l_name="${1:-}"
    local l_output=${l_output:-1}
    [[ -n "${l_name}" && -n $(which "${l_name}" 2> /dev/null || command -v "${l_name}" 2> /dev/null) ]] && l_output=0
    return "${l_output}"
}

fn_InitializationCheck(){
    [[ -f /etc/os-release && $(uname -s) == 'Linux' ]] || fn_ExitStatement 'Sorry, this script just works for GNU/Linux.'

    # - Check root or sudo privilege
    [[ "$UID" -eq 0 ]] || fn_ExitStatement 'Sorry: this script requires superuser privileges (eg. root, su).'
    # fn_ExitStatement "Sorry: please run this script as normal user whthout root privilege."

    # - Download method
    if fn_CommandExistIfCheck 'curl'; then
        download_method='curl -fsL'
    elif fn_CommandExistIfCheck 'wget'; then
        download_method='wget -qO-'
    else
        fn_ExitStatement "Please install curl or wget firstly."
    fi

    # - System architecture
    # amd64, i386, arm64, armel, armhf
    # x86_64 --> amd64, i686 --> i386, armv7h --> armhf, armv6h --> armel, aarch64 --> arm64
    # docker-compose-linux-{x86_64,aarch64,armv7,armv6,s390x}

    machine_name=$(uname -m)
    case "${machine_name}" in
        x86_64|amd64|i?86_64) pack_arch='x86_64' ;;
        i?86|x86 ) pack_arch='i386' ;;
        aarch64|arm64 ) pack_arch='aarch64' ;;
        armv7* ) pack_arch='armv7' ;;
        armv6* ) pack_arch='armv6' ;;
        s390x ) pack_arch='s390x' ;;
        * )
            fn_ExitStatement "Sorry, this script doesn't support you system arch '${machine_name}'."
        ;;
    esac

    # - Check utility 'docker'
    # [[ -n $(which docker 2> /dev/null || command -v docker 2> /dev/null) ]]
    fn_CommandExistIfCheck 'docker' || fn_ExitStatement "Sorry, fail to detect utility 'docker'"

    # - Detect docker cli-plugins directory
    # https://github.com/docker/compose/tree/v2#linux
    # https://docs.docker.com/compose/cli-command/#install-on-linux
    docker_cli_plugins_dir=${docker_cli_plugins_dir:-'/usr/libexec/docker/cli-plugins'} # For Debian
    [[ -d "${docker_cli_plugins_dir}" ]] || docker_cli_plugins_dir='/usr/lib/docker/cli-plugins'
    [[ -d "${docker_cli_plugins_dir}" ]] || docker_cli_plugins_dir='/usr/local/lib/docker/cli-plugins'
    [[ -d "${docker_cli_plugins_dir}" ]] || docker_cli_plugins_dir='/usr/local/libexec/docker/cli-plugins'
    [[ -d "${docker_cli_plugins_dir}" ]] || fn_ExitStatement 'Sorry, fail to detect docker cli_plugins directory'

    compose_bin_path="${docker_cli_plugins_dir}/docker-compose"
}


#########  2-1. Latest & Local Version Check  #########
fn_VersionComparasion(){
    # - Local version
    current_local_version=${current_local_version:-}
    current_local_version=$(docker-compose --version 2> /dev/null | sed -r -n 's@.*version[[:space:]]*([^,]+).*@\1@g;p') # assume V1
    # docker-compose version 1.28.5, build c4eb3a1f

    if [[ "${current_local_version}" =~ ^[vV]?1\. ]]; then
        echo -e 'Attention: local compose is still v1, latest release version is v2 since 2021-09-28, details see https://github.com/docker/compose/tree/v2#linux'
    else
        [[ -f "${compose_bin_path}" ]] && current_local_version=$(docker compose version 2> /dev/null | sed -r -n 's@.*version[[:space:]]*([^,]+).*@\1@g;p')
    fi

    [[ -n "${current_local_version}" ]] && echo "Local installed version is ${current_local_version} ."

    # - Online release version
    release_version_list=${release_version_list:-}
    release_version_list=$(${download_method} "${docker_compost_github_api}" 2> /dev/null | sed -r -n '/"(tag_name|published_at)"/{s@^[[:space:]]*@@g;s@^[^:]+:[[:space:]]*@@g;s@",?@@g;p}' | sed 'N;s@\n@|@g' | sed -r '/^V?1\./Id; /-rc\.?[[:digit:]]+/d; s@T[[:digit:]]{2}+.*$@@g')
    # v2.3.3|2022-03-09

    [[ -z "${release_version_list}" ]] && fn_ExitStatement "Sorry, fail to extract release version list from GitHub."

    latest_release_version=''
    latest_release_version=$(sed -r -n '1{s@^([^\|]+)\|([^\|]+)$@\1 (\2)@g;p}' <<< "${release_version_list}")

    if [[ "${version_check}" -eq 1 ]]; then
        echo "Latest release version is ${latest_release_version}."
        fn_ExitStatement ''
    fi

    if [[ "${version_specify}" -eq 1 ]]; then
        if [[ $(wc -l <<< "${release_version_list}") -eq 1 ]]; then
            version_choose="${release_version_list%%\|*}"
            echo -e "Available version is '${version_choose}'.\n"
        else
            IFS_BAK=${IFS_BAK:-"$IFS"}  # Backup IFS
            IFS='|' # Setting temporary IFS
            echo -e "\nAvailable Version List: (num limit ${select_num_limit}, 'L' stands for existed version)\n"
            PS3='Choose version number(e.g. 1, 2,...): '

            local l_selection_list=${l_selection_list:-}
            l_selection_list=$(sed -r -n 's@^([^\|]+)\|([^\|]+)$@\1 (\2)@g;p' <<< "${release_version_list}" | head -n "${select_num_limit}")
            [[ -n "${current_local_version}" ]] && l_selection_list=$(sed -r -n '/^'"${current_local_version}"'[[:space:]]*/{s@[[:punct:]]+$@ L&@g};p' <<< "${l_selection_list}")

            select item in $(sed ':a;N;$!ba;s@\n@|@g' <<< "${l_selection_list}"); do
                version_choose="${item}"
                [[ -n "${version_choose}" ]] && break
            done < /dev/tty
            # v2.3.3 (2022-03-09)|v2.3.2 (2022-03-08)|v2.3.1 (2022-03-07)|v2.3.0 (2022-03-04)

            IFS=${IFS_BAK}  # Restore IFS
            unset IFS_BAK
            unset PS3
        fi

        [[ -n "${version_choose}" ]] || fn_ExitStatement 'No release version selected.'

        echo -e "\nSelected version is ${version_choose} ."
    
    else
        version_choose="${latest_release_version}"
    fi

    version_choose="${version_choose%% *}"

    if [[ -n "${current_local_version}" && "${current_local_version}" == "${version_choose}" ]]; then
        fn_ExitStatement 'Note: selected version already existed on local, bye.'
    fi
}

fn_OperatingProcess(){
    # https://api.github.com/repos/docker/compose/releases/latest
    # https://api.github.com/repos/docker/compose/releases/tags/v2.3.3
    echo -e '\nOperation processing ...'

    # - Extracting latest release info
    release_version_info=$(${download_method} "${docker_compost_github_api}/tags/${version_choose}" | sed -r -n '1,/"assets"/{/"(name|published_at)":/{p}}; /"browser_download_url":.*'"$(uname -s)"'.*?'"$(uname -m)"'/I{p}' | sed -r -n 's@^[^:]*:[[:space:]]*@@g;s@,$@@g;s@"@@g;s@T[[:digit:]:]+.*Z?$@@g;p' | sed ':a;N;$!ba;s@\n@|@g')
    # v2.0.1|2021-09-30|https://github.com/docker/compose/releases/download/v2.0.1/docker-compose-linux-x86_64|https://github.com/docker/compose/releases/download/v2.0.1/docker-compose-linux-x86_64.sha256
    # 1.28.5|2021-02-26|https://github.com/docker/compose/releases/download/1.28.5/docker-compose-Linux-x86_64|https://github.com/docker/compose/releases/download/1.28.5/docker-compose-Linux-x86_64.sha256

    compose_release_version=$(cut -d\| -f 1 <<< "${release_version_info}")
    compose_release_date=$(cut -d\| -f 2 <<< "${release_version_info}")
    compose_release_binary=$(cut -d\| -f 3 <<< "${release_version_info}")
    compose_release_binary_sha=$(cut -d\| -f 4 <<< "${release_version_info}")

    # - Download latest docker-compose
    echo -e 'Downloading ...'
    cd /tmp || exit
    ${download_method} "${compose_release_binary}" > "${compose_release_binary##*/}"

    # - SHA256 digest check
    echo -e 'SHA256 digest checking ...'
    [[ -n $(${download_method} "${compose_release_binary_sha}" | sha256sum -c - | sed -n '/:.*OK/{p}') ]] || exit 0
    # docker-compose-Linux-x86_64: OK

    echo -e "Moving binary to ${docker_cli_plugins_dir} ..."
    mv -f "${compose_release_binary##*/}" "${compose_bin_path}"
    chmod +x "${compose_bin_path}"
    chown root:root "${compose_bin_path}"

    # - Output version info
    echo -n -e '\nVersion Info:\n'
    docker compose version
    # docker-compose version
}


#########  3. Executing Process  #########
fn_InitializationCheck
fn_VersionComparasion
fn_OperatingProcess

# Script End