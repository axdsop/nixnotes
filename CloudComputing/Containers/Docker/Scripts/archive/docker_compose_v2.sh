#!/usr/bin/env bash

# Target: Install/Update docker compose (V2) on GNU/Linux x86_64.
# Writer: MaxdSre

# Change Log
# - Sep 09, 2020 Wed 06:05 ET - First draft
# - Oct 03, 2021 Sun 19:03 ET - upgrade compose installation method to v2

# Official Doc
# - https://github.com/docker/compose
# - https://docs.docker.com/compose/install/
# - https://docs.docker.com/compose/completion/
# - https://api.github.com/repos/docker/compose/releases/latest


fn_Exit(){
    local l_str="$*"
    [[ -n "${l_str}" ]] && echo -e "${l_str}\n"
    exit 0
}

# - Check if is GNU/Linux (x86_64)
if [[ $(uname -s) == 'Linux' && $(uname -m) == 'x86_64' ]]; then
    # - Check root or sudo privilege
    if [[ "$UID" -eq 0 ]]; then
        download_command='wget -qO-'
        [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && download_command='curl -fsSL'
    else
        fn_Exit 'Sorry: this script requires superuser privileges (eg. root, su).'
    fi

    # - Check utility 'docker'
    [[ -n $(which docker 2> /dev/null || command -v docker 2> /dev/null) ]] || fn_Exit "Sorry, fail to detect utility 'docker'"

    # - Detect docker cli-plugins directory
    # https://github.com/docker/compose#linux
    docker_cli_plugins_dir=${docker_cli_plugins_dir:-'/usr/libexec/docker/cli-plugins'} # For Debian
    [[ -d "${docker_cli_plugins_dir}" ]] || docker_cli_plugins_dir='/usr/lib/docker/cli-plugins'
    [[ -d "${docker_cli_plugins_dir}" ]] || docker_cli_plugins_dir='/usr/local/lib/docker/cli-plugins'
    [[ -d "${docker_cli_plugins_dir}" ]] || docker_cli_plugins_dir='/usr/local/libexec/docker/cli-plugins'
    [[ -d "${docker_cli_plugins_dir}" ]] || fn_Exit 'Sorry, fail to detect docker cli_plugins directory'

else
    fn_Exit 'Sorry, this script just works for Linux (x86_64).'
fi

# - Extracting latest release info
latest_release_info=$(${download_command} https://api.github.com/repos/docker/compose/releases/latest | sed -r -n '1,/"assets"/{/"(name|published_at)":/{p}}; /"browser_download_url":.*'"$(uname -s)"'.*?'"$(uname -m)"'/I{p}' | sed -r -n 's@^[^:]*:[[:space:]]*@@g;s@,$@@g;s@"@@g;s@T[[:digit:]:]+.*Z?$@@g;p' | sed ':a;N;$!ba;s@\n@|@g')

# v2.0.1|2021-09-30|https://github.com/docker/compose/releases/download/v2.0.1/docker-compose-linux-x86_64|https://github.com/docker/compose/releases/download/v2.0.1/docker-compose-linux-x86_64.sha256
# 1.28.5|2021-02-26|https://github.com/docker/compose/releases/download/1.28.5/docker-compose-Linux-x86_64|https://github.com/docker/compose/releases/download/1.28.5/docker-compose-Linux-x86_64.sha256

compose_release_version=$(cut -d\| -f 1 <<< "${latest_release_info}")
compose_release_date=$(cut -d\| -f 2 <<< "${latest_release_info}")
compose_release_binary=$(cut -d\| -f 3 <<< "${latest_release_info}")
compose_release_binary_sha=$(cut -d\| -f 4 <<< "${latest_release_info}")

echo "Latest available release version ${compose_release_version} (${compose_release_date})."

# - Version comparasion
compose_local_version=${compose_local_version:-}
compose_local_version=$(docker-compose --version 2> /dev/null | sed -r -n 's@.*version[[:space:]]*([^,]+).*@\1@g;p')
[[ "${compose_local_version}" =~ ^v1 ]] && fn_Exit 'Local compose is still v1, current release version is v2, details see https://github.com/docker/compose#linux'

docker_compose_bin_path="${docker_cli_plugins_dir}/docker-compose"
[[ -f "${docker_compose_bin_path}" ]] && compose_local_version=$(docker compose version 2> /dev/null | sed -r -n 's@.*version[[:space:]]*([^,]+).*@\1@g;p')

if [[ "${compose_local_version}" == "${compose_release_version}" ]]; then
    # docker-compose version 1.28.5, build c4eb3a1f
    fn_Exit 'Local version is the latest version.'
else
    echo -e '\nOperation processing ...'
fi

# - Download latest docker-compose
echo 'Downloading ...'
cd /tmp || exit
${download_command} "${compose_release_binary}" > "${compose_release_binary##*/}"

# - SHA256 digest check
echo 'SHA256 digest checking ...'
[[ -n $(${download_command} "${compose_release_binary_sha}" | sha256sum -c - | sed -n '/:.*OK/{p}') ]] || exit 0
# docker-compose-Linux-x86_64: OK

echo "Moving binary to ${docker_cli_plugins_dir} ..."
sudo mv -f "${compose_release_binary##*/}" "${docker_compose_bin_path}"
sudo chmod +x "${docker_compose_bin_path}"
sudo chown root:root "${docker_compose_bin_path}"

# - Output version info
# echo -e '\nDocker Compose Version:\n'
docker compose version
# docker-compose version

# Script End