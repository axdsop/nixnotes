#!/usr/bin/env bash

# Target: Install/Update docker compose (V1) on GNU/Linux x86_64.
# Writer: MaxdSre

# Change Log
# - Sep 09, 2020 Wed 06:05 ET - First draft

# Official Doc
# - https://docs.docker.com/compose/install/
# - https://docs.docker.com/compose/completion/
# - https://api.github.com/repos/docker/compose/releases/latest


# - Check if is GNU/Linux (x86_64)
if [[ $(uname -s) == 'Linux' && $(uname -m) == 'x86_64' ]]; then
    # - Check root or sudo privilege
    if [[ "$UID" -eq 0 ]]; then
        download_command='wget -qO-'
        [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && download_command='curl -fsSL'
    else
        echo 'Sorry: this script requires superuser privileges (eg. root, su).'
        exit 0
    fi
else
    echo 'Sorry, this script just works for Linux (x86_64).'
    exit 0
fi

# - Extracting latest release info
latest_release_info=$($download_command https://api.github.com/repos/docker/compose/releases/latest | sed -r -n '1,/"assets"/{/"(name|published_at)":/{p}}; /"browser_download_url":.*'$(uname -s)'.*?'$(uname -m)'/{p}' | sed -r -n 's@^[^:]*:[[:space:]]*@@g;s@,$@@g;s@"@@g;s@T[[:digit:]:]+.*Z?$@@g;p' | sed ':a;N;$!ba;s@\n@|@g')
# 1.28.5|2021-02-26|https://github.com/docker/compose/releases/download/1.28.5/docker-compose-Linux-x86_64|https://github.com/docker/compose/releases/download/1.28.5/docker-compose-Linux-x86_64.sha256

compose_release_version=$(cut -d\| -f 1 <<< "${latest_release_info}")
compose_release_date=$(cut -d\| -f 2 <<< "${latest_release_info}")
compose_release_binary=$(cut -d\| -f 3 <<< "${latest_release_info}")
compose_release_binary_sha=$(cut -d\| -f 4 <<< "${latest_release_info}")

echo "Latest available release version ${compose_release_version} (${compose_release_date})."

# - Version comparasion
compose_local_version=${compose_local_version:-}
compose_local_version=$(docker-compose --version 2> /dev/null | sed -r -n 's@.*version[[:space:]]*([^,]+).*@\1@g;p')

if [[ "${compose_local_version}" == "${compose_release_version}" ]]; then
    # docker-compose version 1.28.5, build c4eb3a1f
    echo 'Local version is the latest version.'
    exit 0
else
    echo 'Operation processing ...'
fi

# - Download latest docker-compose
echo 'Downloading ...'
cd /tmp
$download_command "${compose_release_binary}" > "${compose_release_binary##*/}"

# - SHA256 digest check
echo 'SHA256 digest checking ...'
[[ -n $($download_command "${compose_release_binary_sha}" | sha256sum -c - | sed -n '/:.*OK/{p}') ]] || exit 0
# docker-compose-Linux-x86_64: OK

echo 'Moving binary to /usr/local/bin/ ...'
sudo mv -f "${compose_release_binary##*/}" /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
# sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

# - Download docker-compose command completion
if [[ $SHELL =~ \/bash$ ]]; then
    echo 'bash completion generating ...'
    bash_completion_dir=${bash_completion_dir:-'/usr/share/bash-completion/completions'}
    [[ -d /etc/bash_completion.d/ ]] && bash_completion_dir='/etc/bash_completion.d'

    [[ -d "${bash_completion_dir}" ]] && $download_command https://raw.githubusercontent.com/docker/compose/${compose_release_version}/contrib/completion/bash/docker-compose | sudo tee "${bash_completion_dir}"/docker-compose 1> /dev/null
    sudo chmod 644 "${bash_completion_dir}"/docker-compose
fi

# - Output version info
echo -e '\nDocker Compose Version:\n'
docker-compose version

# Script End