# Watchtower Usage

[Watchtower][watchtower] is a container-based solution for automating Docker container base image updates.

## TOC

1. [Official Site](#official-site)  
1.1 [Help info](#help-info)  
2. [Deployment](#deployment)  
2.1 [.env](#env)  
2.2 [Running Container](#running-container)  
3. [Blogs](#blogs)  
4. [Change Log](#change-log)  


## Official Site

Item|Url
---|---
Official Site|https://containrrr.dev/watchtower/
Official Doc|https://containrrr.dev/watchtower/
GitHub|https://github.com/containrrr/watchtower
GitHub Wiki|https://github.com/containrrr/watchtower/wiki
Docker Hub|https://hub.docker.com/r/containrrr/watchtower/

### Help info

```sh
# check help info
docker run --rm containrrr/watchtower -h
```

<details><summary>Click to expand help info</summary>

```txt
Watchtower automatically updates running Docker containers whenever a new image is released.
More information available at https://github.com/containrrr/watchtower/.

Usage:
  watchtower [flags]

Flags:
  -a, --api-version string                          api version to use by docker client (default "1.25")
  -c, --cleanup                                     remove previously used images after updating
  -d, --debug                                       enable debug mode with verbose logging
      --enable-lifecycle-hooks                      Enable the execution of commands triggered by pre- and post-update lifecycle hooks
  -h, --help                                        help for watchtower
  -H, --host string                                 daemon socket to connect to (default "unix:///var/run/docker.sock")
      --http-api-metrics                            Runs Watchtower with the Prometheus metrics API enabled
      --http-api-token string                       Sets an authentication token to HTTP API requests.
      --http-api-update                             Runs Watchtower in HTTP API mode, so that image updates must to be triggered by a request
      --include-restarting                          Will also include restarting containers
  -S, --include-stopped                             Will also include created and exited containers
  -i, --interval int                                poll interval (in seconds) (default 86400)
  -e, --label-enable                                watch containers where the com.centurylinklabs.watchtower.enable label is true
  -m, --monitor-only                                Will only monitor for new images, not update the containers
      --no-color                                    Disable ANSI color escape codes in log output
      --no-pull                                     do not pull any new images
      --no-restart                                  do not restart any containers
      --no-startup-message                          Prevents watchtower from sending a startup message
      --notification-email-delay int                Delay before sending notifications, expressed in seconds
      --notification-email-from string              Address to send notification emails from
      --notification-email-server string            SMTP server to send notification emails through
      --notification-email-server-password string   SMTP server password for sending notifications
      --notification-email-server-port int          SMTP server port to send notification emails through (default 25)
      --notification-email-server-tls-skip-verify   Controls whether watchtower verifies the SMTP server's certificate chain and host name.
                                                    Should only be used for testing.
      --notification-email-server-user string       SMTP server user for sending notifications
      --notification-email-subjecttag string        Subject prefix tag for notifications via mail
      --notification-email-to string                Address to send notification emails to
      --notification-gotify-tls-skip-verify         Controls whether watchtower verifies the Gotify server's certificate chain and host name.
                                                    Should only be used for testing.
      --notification-gotify-token string            The Gotify Application required to query the Gotify API
      --notification-gotify-url string              The Gotify URL to send notifications to
      --notification-msteams-data                   The MSTeams notifier will try to extract log entry fields as MSTeams message facts
      --notification-msteams-hook string            The MSTeams WebHook URL to send notifications to
      --notification-slack-channel string           A string which overrides the webhook's default channel. Example: #my-custom-channel
      --notification-slack-hook-url string          The Slack Hook URL to send notifications to
      --notification-slack-icon-emoji string        An emoji code string to use in place of the default icon
      --notification-slack-icon-url string          An icon image URL string to use in place of the default icon
      --notification-slack-identifier string        A string which will be used to identify the messages coming from this watchtower instance (default "watchtower")
      --notification-template string                The shoutrrr text/template for the messages
      --notification-url stringArray                The shoutrrr URL to send notifications to
  -n, --notifications strings                        notification types to send (valid: email, slack, msteams, gotify, shoutrrr)
      --notifications-level string                  The log level used for sending notifications. Possible values: panic, fatal, error, warn, info or debug (default "info")
      --remove-volumes                              remove attached volumes before updating
      --revive-stopped                              Will also start stopped containers that were updated, if include-stopped is active
      --rolling-restart                             Restart containers one at a time
  -R, --run-once                                    Run once now and exit
  -s, --schedule string                             the cron expression which defines when to update
      --scope string                                Defines a monitoring scope for the Watchtower instance.
  -t, --stop-timeout duration                       timeout before a container is forcefully stopped (default 10s)
  -v, --tlsverify                                   use TLS and verify the remote
      --trace                                       enable trace mode with very verbose logging - caution, exposes credentials
```

</details>

## Deployment

Variables definition

```sh
service_name='watchtower'
service_save_path="$HOME/.docker/${service_name}"
mkdir -p "${service_save_path}"

# slack hook url (via slack app 'Incoming WebHooks')
slack_hook_url="https://hooks.slack.com/services/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"
```

### .env

```sh
tee "${service_save_path}/.env" 1> /dev/null << EOF
TZ=America/New_York
WATCHTOWER_NOTIFICATIONS=slack
WATCHTOWER_NOTIFICATION_SLACK_HOOK_URL="${slack_hook_url}"
WATCHTOWER_NOTIFICATION_SLACK_IDENTIFIER=watchtower-backend-server
WATCHTOWER_NOTIFICATION_SLACK_ICON_EMOJI=:whale:

# Optional
#WATCHTOWER_NOTIFICATION_SLACK_CHANNEL=
#WATCHTOWER_NOTIFICATION_SLACK_ICON_URL=
EOF
```

### Running Container

```sh
version_info='1.1.5'  # latest
# https://github.com/containrrr/watchtower/releases/tag/v1.1.5

docker run -d \
    --restart unless-stopped \
    --name watchtower \
    --env-file "${service_save_path}/.env" \
    -v /var/run/docker.sock:/var/run/docker.sock \
    containrrr/watchtower:${version_info} \
    --interval 10800 \
    --cleanup

# --interval 86400  # 24h
# --schedule "0 * 3 * * *"  means every day 03:00
# --monitor-only   Will only monitor for new images, not update the containers
# -v /etc/timezone:/etc/timezone:ro \
```


## Blogs

* [Watchtower - 自动更新 Docker 镜像与容器](https://p3terx.com/archives/docker-watchtower.html)


## Change Log

* Mar 10, 2021 10:20 Wed ET
  * First draft

[docker]: https://www.docker.com
[docker_compose]:https://docs.docker.com/compose/
[watchtower]:https://github.com/containrrr/watchtower

<!-- End -->