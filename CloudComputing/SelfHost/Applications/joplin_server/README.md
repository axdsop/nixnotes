# Self-hosting Joplin Server

[Joplin][joplinapp] is an open source note taking and to-do application with synchronisation capabilities

>The notes are searchable, can be copied, tagged and modified either from the applications directly or from your own text editor. The notes are in Markdown format.

## TOC

1. [Official Site](#official-site)  
2. [Prerequisite](#prerequisite)  
2.1 [Docker Compose](#docker-compose)  
3. [Deployment](#deployment)  
3.1 [Configs](#configs)  
3.1.1 [.env](#env)  
3.1.2 [docker-compose.yml](#docker-composeyml)  
3.2 [Running Containers](#running-containers)  
3.3 [Nginx Reverse Proxy](#nginx-reverse-proxy)  
3.3.1 [Conf](#conf)  
4. [Setup the website](#setup-the-website)  
4.1 [Secure the admin user](#secure-the-admin-user)  
4.2 [Create a user for sync](#create-a-user-for-sync)  
5. [Joplin Client](#joplin-client)  
5.1 [Desktop applications](#desktop-applications)  
6. [Snapshots](#snapshots)  
6.1 [Web page](#web-page)  
6.2 [Desktop App](#desktop-app)  
6.3 [Web File List](#web-file-list)  
6.4 [Markdown Rendering](#markdown-rendering)  
7. [Change Log](#change-log)  


## Official Site

Item|Url
---|---
Official Site|https://joplinapp.org/
Official Forum|https://discourse.joplinapp.org/
GitHub|https://github.com/laurent22/joplin/
Docker Hub|[joplin/server][joplin_server]


## Prerequisite

### Docker Compose

[Docker Compose][docker_compose] (compose v2 architecture and installation instructions differ from v1, see [release note](https://github.com/docker/compose/releases/tag/v2.0.0)) is a tool for defining and running multi-container Docker applications. Official install doc [Install Docker Compose](https://docs.docker.com/compose/install/).

I write a Shell script [docker_compose.sh](/CloudComputing/Containers/Docker/Scripts/docker_compose.sh) to install it.

```sh
download_command='wget -qO-'
[[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && download_command='curl -fsSL'

${download_command} https://gitlab.com/axdsop/nixnotes/raw/master/CloudComputing/Containers/Docker/Scripts/docker_compose.sh | sudo bash -s --
```

Check compose version

```sh
# For compose V2
$ docker compose version
#Docker Compose version v2.0.1
Docker Compose version v2.15.1

# For compose V1
$ docker-compose version
docker-compose version 1.28.5, build c4eb3a1f
docker-py version: 4.4.4
CPython version: 3.7.10
OpenSSL version: OpenSSL 1.1.0l  10 Sep 2019
```


## Deployment

[Joplin][joplinapp] server is running as a [Docker][docker] container.

Cloud firewalls define `reverse` droplet can listen TCP port range (8000-9000) exposed by `backend` droplets which running docker service.

I write a note [Secure Nginx With Let's Encrypt Free SSL Certificate](/CyberSecurity/WebServer/Nginx/IssueSSLCert.md) to configure Nginx.

Docker hub [joplin/server][joplin_server] lists deployment precedure and configuration details.

### Configs

Variables definition

```sh
service_name='joplin'

localhost_ip='127.0.0.1'
app_exposted_port='8100'
app_base_url="https://${service_name}.maxdsre.com"
app_mount_path="/data/${service_name}"

postgres_password=${postgres_password:-"joplin_server@$(date +'%Y')"} # e.g. joplin_server@2021
```

Create configs save dir

```sh
service_save_path="$HOME/.docker/${service_name}"
mkdir -p "${service_save_path}"
```

#### .env

GitHub repository provides a env [sample](https://github.com/laurent22/joplin/blob/dev/.env-sample).

<details><summary>Click to expand .env</summary>

```sh
tee "${service_save_path}/.env" 1> /dev/null << EOF
# ==========================================
# Production Config (.env)
#
# By default it will use SQLite, but that's mostly to test and evaluate the
# server. So you'll want to specify db connection settings to use Postgres.
# ==========================================
APP_BASE_URL=${app_base_url}
APP_PORT=22300
APP_EXPOSED_PORT=${localhost_ip}:${app_exposted_port}
APP_MOUNT_PATH=${app_mount_path}

DB_CLIENT=pg
POSTGRES_PASSWORD=${postgres_password}
POSTGRES_DATABASE=joplin
POSTGRES_USER=joplin
POSTGRES_PORT=5432
POSTGRES_HOST=db


# ==========================================
# DEV Config Example
#
# Example of local config, for development. In dev mode, you would usually use
# SQLite so database settings are not needed.
# ==========================================
# APP_BASE_URL=http://localhost:22300
# APP_PORT=22300
EOF
```

</details>

#### docker-compose.yml

GitHub repository provides a docker-compose server [sample](https://github.com/laurent22/joplin/blob/dev/docker-compose.server.yml).

Here is my `docker-compose.yml`

<details><summary>Click to expand docker-compose.yml</summary>

```sh
tee "${service_save_path}/docker-compose.yml" 1> /dev/null << EOF

# https://github.com/laurent22/joplin/blob/dev/docker-compose.server.yml
# All environment variables are optional. If you don't set them, you will get a warning from docker-compose, however the app should use working defaults.
version: '3'

services:
  db:
    #https://hub.docker.com/_/postgres
    image: postgres:15-alpine  # version 15, 20221013 ~ 20271111
    container_name: joplin_server_db
    expose:
      - "5432:5432"
    restart: unless-stopped
    networks:
      - joplin_server_network
    volumes:
      - \${APP_MOUNT_PATH}/db:/var/lib/postgresql/data
      - /etc/localtime:/etc/localtime:ro
    env_file: .env
    environment:
      - POSTGRES_PASSWORD=\${POSTGRES_PASSWORD}
      - POSTGRES_USER=\${POSTGRES_USER}
      - POSTGRES_DB=\${POSTGRES_DATABASE}

  app:
    # https://hub.docker.com/r/joplin/server
    image: joplin/server:latest
    container_name: joplin_server_app
    depends_on:
      - db
    ports:
      - "\${APP_EXPOSED_PORT}:\${APP_PORT}"
    restart: unless-stopped
    networks:
      - joplin_server_network
    volumes:
      - /etc/localtime:/etc/localtime:ro
    env_file: .env
    environment:
    - APP_PORT=\${APP_PORT}
    - APP_BASE_URL=\${APP_BASE_URL}
    - DB_CLIENT=\${DB_CLIENT}
    - POSTGRES_PASSWORD=\${POSTGRES_PASSWORD}
    - POSTGRES_DATABASE=\${POSTGRES_DATABASE}
    - POSTGRES_USER=\${POSTGRES_USER}
    - POSTGRES_PORT=\${POSTGRES_PORT}
    - POSTGRES_HOST=\${POSTGRES_HOST}

    # - MAILER_ENABLED=1
    # - MAILER_HOST=smtp.gmail.com
    # - MAILER_PORT=465
    # - MAILER_SECURE=1
    # - MAILER_AUTH_USER=youremail@gmail.com
    # - MAILER_AUTH_PASSWORD=Y0urP@ssw0rd
    # - MAILER_NOREPLY_NAME=Joplin
    # - MAILER_NOREPLY_EMAIL=email@email.com

networks:
  joplin_server_network:
    name: joplin_server_network

# End
EOF
```

</details>


### Running Containers

```sh
cd "${service_save_path}"

# - Via docker compose (For docker-compose V2)
docker compose up -d
# docker compose --env-file "${service_save_path}/docker-compose.yml" up -d
docker compose ps
docker compose down --remove-orphans

# - Via docker-compose (For docker-compose V1)
docker-compose up -d
# docker-compose -f "${service_save_path}/docker-compose.yml" up -d
docker-compose ps
# docker-compose down --remove-orphans
```

<details><summary>Click to expand output</summary>

```txt
Creating network "joplin_default" with the default driver
Creating joplin_server_db ... done
Creating joplin_server_app ... done
```

</details>

### Nginx Reverse Proxy

Variables definition

```sh
# service_name='joplin'
domain_name_specify="${service_name}.maxdsre.com"

backend_private_ip=${backend_private_ip:-'127.0.0.1'}
# backend_private_ip='192.168.8.4'

# Nginx site config dir /etc/nginx/sites-enabled/
nginx_conf_path="/etc/nginx/sites-enabled/${service_name}.conf"
mkdir -p "${nginx_conf_path%/*}"
```

#### Conf

Nginx conf

<details><summary>Click to expand Nginx conf</summary>

```conf
sudo tee "${nginx_conf_path}" 1> /dev/null << EOF
upstream ${service_name}-default {
    server ${backend_private_ip}:${app_exposted_port};
}

# Redirect HTTP to HTTPS
server {
    listen 80;
    # listen [::]:80 ipv6only=on;
    server_name ${domain_name_specify};
    return 301 https://\$host\$request_uri;
}

map \$request_uri \$joplin_log_ignore {
    ~^/(icons|fonts|images|css|js)/ 0;
    ~^/(api/|items|changes|favicon.ico) 0;
    default 1;
}

server {
    listen 443 ssl http2;
    # listen [::]:443 ssl http2 ipv6only=on;
    server_name ${domain_name_specify};

    error_log  /var/log/nginx/${service_name}_error.log warn;  # [debug|info|notice|warn|error|crit|alert|emerg]
    access_log /var/log/nginx/${service_name}_access.log main if=\$joplin_log_ignore; # format name 'main' instead of predefined 'combined' format

    include /etc/nginx/conf.d/section/ssl.conf;

    location / {
        autoindex on;

        proxy_set_header Host \$host;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto \$scheme;

        proxy_pass http://${service_name}-default;

        # https://serverfault.com/questions/587386/an-upstream-response-is-buffered-to-a-temporary-file
        proxy_buffers 32 64k;
        proxy_buffer_size 64k;
    }

    # remove errors from favicon from nginx server logs
    location = /favicon.ico {
        return 204;
        log_not_found off;
    }
}
EOF
```

</details>

Testing Nginx conf syntax then reload

```sh
nginx -t && nginx -s reload

# nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
# nginx: configuration file /etc/nginx/nginx.conf test is successful
```

Testing domain if works

```sh
$ curl -I joplin.maxdsre.com

HTTP/1.1 301 Moved Permanently
Server: nginx
Date: Wed, 03 Nov 2021 16:43:05 GMT
Content-Type: text/html
Content-Length: 162
Connection: keep-alive
Location: https://joplin.maxdsre.com/

# Date: Sun, 07 Mar 2021 20:33:10 GMT
```

## Setup the website

Once the server is exposed to the internet, you can open the admin UI and get it ready for synchronisation.

Here the Joplin server is running on <https://joplin.maxdsre.com>.

### Secure the admin user

>By default, Joplin Server will be setup with an admin user with email **admin@localhost** and password **admin**. For security purposes, the admin user's credentials should be changed. On the Admin Page, login as the admin user. In the upper right, select the Profile button update the admin password.

The first thing you need to do is change the admin email and password.

By default, the instance will be setup with an admin user

Item|Value
---|---
Email | admin@localhost
password | admin

Open <https://joplin.maxdsre.com/login> and login as admin. Then go to the *Profile* section and change the admin password.

>Once the app is installed, open the Configuration screen, then the Synchronisation section. In this screen, select "Joplin Server" as a synchronisation target, then enter the URL https://joplin.maxdsre.com and your username and password

### Create a user for sync

While the admin user can be used for synchronisation, it is recommended to create a separate non-admin user for it. To do so, navigate to the *Users* page - from there you can create a new user. Once this is done, you can use the email and password you specified to sync this user account with your Joplin clients.

Testing accounts

Full name|Email|is admin
---|---|---
Admin|admin@maxdsre.com|1 	
Test Account|test_123@maxdsre.com|0


## Joplin Client

Official site provides [installation](https://joplinapp.org/#installation) introduction.

### Desktop applications

Configuration files saved in directory *$HOME/.config/joplin-desktop*. To open configuration interface, click *Tools* -> *Option* (Ctrl + comma).

Here choose `.AppImage` from its GitHub [release page](https://github.com/laurent22/joplin/releases).

Create soft link

```sh
# https://github.com/laurent22/joplin/releases/download/v2.5.10/Joplin-2.5.10.AppImage

sudo ln -fs $PATH/Joplin-X.X.XXX.AppImage /usr/local/bin/joplin
```

Create desktop file then you can open it via app launcher.

```sh
tee $HOME/.local/share/applications/joplin.desktop 1> /dev/null << EOF
[Desktop Entry]
Name=Joplin
Comment=Joplin - a note taking and to-do application with synchronization capabilities for Windows, macOS, Linux, Android and iOS.
Exec=/usr/local/bin/joplin
Icon=joplin
Terminal=false
Type=Application
Categories=Application;Office;
EOF
```

## Snapshots

### Web page

Web main page

![joplin server mainpage](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/joplin_server/01_web_mainpage.png)

Web login page

![joplin server loginpage](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/joplin_server/02_web_login_page.png)

Change password

<details><summary>Click to expand password change page</summary>

![change password prompts](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/joplin_server/03_web_change_password.png)

![change password page](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/joplin_server/04_web_change_password.png)

</details>

User list

![user list page](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/joplin_server/05_web_user_list.png)


### Desktop App

Joplin client (desktop app)

![desktop overvie](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/joplin_server/06_desktop_overview.png)

Sync config

![desktop sysn config](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/joplin_server/07_desktop_sync_config.png)


>Success! Synchronization configuration appears to be correct.

Encrypt config

>Joplin supports end-to-end encryption (E2EE) on all the applications.

<details><summary>Click to expand other encrypt config snapshots</summary>

![desktop encrypt config](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/joplin_server/08_desktop_encrypt.png)

![desktop encrypt config](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/joplin_server/09_desktop_encrypt.png)

![desktop encrypt config](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/joplin_server/10_desktop_encrypt.png)

![desktop encrypt config](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/joplin_server/11_desktop_encrypt.png)

</details>

### Web File List

Web file list for specific user

![web file list](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/joplin_server/12_web_file_list_encrypted.png)


### Markdown Rendering

![desktop markdwon rendering](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/joplin_server/15_desktop_markdown_overview.png)

<details><summary>Click to expand other markdown rendering snapshots</summary>

![desktop markdwon rendering](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/joplin_server/13_desktop_markdown_overview.png)

![desktop markdwon rendering](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/joplin_server/14_desktop_markdown_overview.png)

</details>


## Change Log

* Mar 07, 2021 15:15 Sun ET
  * First draft
* Mar 07, 2021 20:25 Sun ET
  * Add directive *container_name* in *docker_compose.yml*
* Oct 03, 2021 18:23 Sun ET
  * change docker compose from V1 to [V2](https://github.com/docker/compose/releases/tag/v2.0.0)


[joplinapp]:https://joplinapp.org/
[docker]:https://www.docker.com
[joplin_server]:https://hub.docker.com/r/joplin/server
[docker_compose]:https://docs.docker.com/compose/

<!-- End -->