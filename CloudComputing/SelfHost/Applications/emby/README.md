# Self-hosting emby

[Emby][emby] is a service to organizes video, music, live TV, and photos from personal media libraries and streams them to smart TVs, streaming boxes and mobile devices.

Issue: It needs to connect domain *https://mb3admin.com* to authorize the service to play the media.

## TOC

1. [Official Site](#official-site)  
2. [Deployment](#deployment)  
2.1 [Configs](#configs)  
2.1.1 [.env](#env)  
2.1.2 [docker-compose.yml](#docker-composeyml)  
2.2 [Running Container](#running-container)  
2.3 [Nginx Reverse Proxy](#nginx-reverse-proxy)  
2.3.1 [Conf](#conf)  
3. [Bibliography](#bibliography)  
4. [Change Log](#change-log)  


## Official Site

Item|Url
---|---
Official Site|https://emby.media
Official Doc|https://support.emby.media/support/home
Official Forum|https://emby.media/community/
GitHub|https://github.com/MediaBrowser
Docker Hub|https://hub.docker.com/r/emby/embyserver

Official download page: https://emby.media/download.html

LinuxServer

Item|Url
---|---
Official site|https://www.linuxserver.io
Official doc|https://docs.linuxserver.io/images/docker-emby
GitHub|https://github.com/linuxserver/docker-emby
Docker|https://hub.docker.com/r/linuxserver/emby


<!-- ### Official Doc -->

## Deployment

### Configs

Variables Definition

```sh
service_name='emby'
service_save_path="$HOME/.docker/${service_name}"
mkdir -p "${service_save_path}"

data_dir="/data/"${service_name}""
config_save_dir="${data_dir}/config"

localhost_ip='127.0.0.1'
domain_for_server="${service_name%%-*}.maxdsre.com"
exposted_http_port='8006'  # default 8096
```

#### .env

File `.env`

<details><summary>Click to expand env conf</summary>

```sh
tee "${service_save_path}"/.env 1> /dev/null << EOF
# https://github.com/linuxserver/docker-emby/blob/master/README.md#parameters

PUID=1000  # id -u
PGID=1000  # id -g
TZ=$(cat /etc/timezone)  # America/New_York from file /etc/timezone

EXPOSED_PORT=${localhost_ip}:${exposted_http_port}
PROXY_DOMAIN=${domain_for_server}

# - Optional

# End
EOF
```

</details>


#### docker-compose.yml

```sh
cd "${service_save_path}"

tee docker-compose.yml 1> /dev/null << EOF
version: '3.8'

services:
  emby:
    image: linuxserver/emby:latest
    container_name: emby-server
    restart: unless-stopped
    networks:
      - emby_server_network
    ports:
      - \${EXPOSED_PORT}:8096 # http webUI
      #- 8920:8920 #optional, https webUI
    volumes:
      - ${data_dir}/config:/config
      #- /path/to/movies:/data/movies # add as many as needed
      # - /opt/vc/lib:/opt/vc/lib #optional, for Raspberry Pi OpenMAX libs
      # - /etc/localtime:/etc/localtime:ro
    env_file: .env
    # devices:
    #   - /dev/dri:/dev/dri #optional
    #   - /dev/vchiq:/dev/vchiq #optional
    #   - /dev/video10:/dev/video10 #optional
    #   - /dev/video11:/dev/video11 #optional
    #   - /dev/video12:/dev/video12 #optional

networks:
  emby_server_network:
    name: emby_server_network

# End
EOF
```

### Running Container

```sh
cd "${service_save_path}"

# - Via docker compose (For docker-compose V2)
docker compose up -d
# docker compose --env-file "${service_save_path}/docker-compose.yml" up -d
docker compose ps
docker compose down --remove-orphans

# - Via docker-compose (For docker-compose V1)
docker-compose up -d
# docker-compose -f "${service_save_path}/docker-compose.yml" up -d
docker-compose ps
# docker-compose down --remove-orphans
```


### Nginx Reverse Proxy

Nginx

```sh
# service_name='emby'
# domain_for_server="${service_name%%-*}.maxdsre.com"
# exposted_http_port='8006'  # default 8096

backend_private_ip=${backend_private_ip:-'127.0.0.1'}
# backend_private_ip='192.168.8.4'

# Nginx site config dir /etc/nginx/sites-enabled/
nginx_conf_path="/etc/nginx/sites-enabled/${service_name}.conf"
sudo mkdir -p "${nginx_conf_path%/*}"
```

#### Conf

Nginc config sample can find from froum post [NGINX Reverse Proxy Config for Emby + Website With SSL](https://emby.media/community/index.php?/topic/80952-nginx-reverse-proxy-config-for-emby-website-with-ssl/)


<details><summary>Click to expand config info</summary>

```conf
sudo tee "${nginx_conf_path}" 1> /dev/null << EOF
upstream ${service_name}-default {
    server ${backend_private_ip}:${exposted_http_port};
}

map \$http_upgrade \$connection_upgrade {
    default upgrade;
    '' close;
}

# - For Sync Server
# Redirect HTTP to HTTPS
server {
    listen 80;
    # listen [::]:80 ipv6only=on;
    server_name ${domain_for_server};
    return 301 https://\$host\$request_uri;
}

server {
    listen 443 ssl http2;
    # listen [::]:443 ssl http2 ipv6only=on;
    server_name ${domain_for_server};

    error_log  /var/log/nginx/${domain_for_server%%.*}_error.log warn;  # [debug|info|notice|warn|error|crit|alert|emerg]
    access_log /var/log/nginx/${domain_for_server%%.*}_access.log main; # format name 'main' instead of predefined 'combined' format

    include /etc/nginx/conf.d/section/ssl.conf;

    location / {
        #proxy_set_header Host \$host;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto \$scheme;

        # http://nginx.org/en/docs/http/ngx_http_proxy_module.html#proxy_http_version
        proxy_http_version 1.1;
        proxy_set_header Upgrade \$http_upgrade;
        proxy_set_header Connection \$connection_upgrade;
        proxy_set_header Host \$host;
        proxy_set_header Accept-Encoding gzip;

        # https://serverfault.com/questions/587386/an-upstream-response-is-buffered-to-a-temporary-file
        # https://www.cyberciti.biz/faq/nginx-upstream-sent-too-big-header-while-reading-response-header-from-upstream/
        proxy_buffering off;
        #proxy_buffers   4 512k;
        #proxy_buffer_size   256k;
        #proxy_busy_buffers_size   512k;

        # https://emby.media/community/index.php?/topic/80952-nginx-reverse-proxy-config-for-emby-website-with-ssl/
        add_header Content-Security-Policy "default-src 'none'; child-src 'self'; font-src 'self' https://cdn.ss-cdn.com/assets/webapp-fonts/; connect-src 'self' wss: ws: https://mb3admin.com https://github.com/MediaBrowser/; media-src 'self' blob: data: https://github.com/MediaBrowser/; manifest-src 'self'; base-uri 'none'; form-action 'self'; frame-ancestors 'self'; object-src 'none'; worker-src 'self' blob:; script-src 'self' https://www.gstatic.com; img-src data: https: http: ; style-src 'unsafe-inline' 'self' https://fonts.googleapis.com/css" always;

        proxy_pass http://${service_name}-default;
    }
}

# End
EOF
```

</details>

Testing Nginx conf syntax then reload

```sh
sudo -i

nginx -t && nginx -s reload

# nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
# nginx: configuration file /etc/nginx/nginx.conf test is successful
```

## Bibliography

* [GitHub - Compare Media Servers](https://github.com/Protektor-Desura/Archon/wiki/Compare-Media-Servers)
* [Reddit - Updated Comparison of Jellyfin vs Emby and Plex?](https://www.reddit.com/r/jellyfin/comments/mvcgmw/updated_comparison_of_jellyfin_vs_emby_and_plex/)


## Change Log

* Mar 08, 2022 10:02 Tue ET
  * First draft


[emby]:https://emby.media "Emby - The open media solution"

<!-- End -->