# Self-hosting VS Code Server

[cdr/code-server][cdr_code_server] is a project to run Mircosoft [Visual Studio Code][vscode] on any machine anywhere and access it in the browser. But here I choose its community-maintained code server [linuxserver/docker-code-server][linuxserver_code_server] which provides more features.


## TOC

1. [Official Site](#official-site)  
2. [Deployment](#deployment)  
2.1 [Variables Definition](#variables-definition)  
2.2 [env](#env)  
2.3 [docker-compose.yml](#docker-composeyml)  
3. [Deployment](#deployment-1)  
3.1 [Running Container](#running-container)  
3.2 [Extension Gallery (optional)](#extension-gallery-optional)  
3.3 [Nginx Reverse Proxy](#nginx-reverse-proxy)  
3.3.1 [Conf](#conf)  
4. [Utility code-server](#utility-code-server)  
5. [Issue Occuring](#issue-occuring)  
5.1 [WebSocket Proxy Error](#websocket-proxy-error)  
5.2 [Hash Password](#hash-password)  
6. [Tutorial](#tutorial)  
7. [Change Log](#change-log)  


## Official Site

Coder

Item|Url
---|---
Official site|https://coder.com
Official doc|https://coder.com/docs/code-server/latest ([FAQ](https://coder.com/docs/code-server/latest/FAQ))
GitHub|https://github.com/cdr/code-server ([FAQ](https://github.com/cdr/code-server/blob/main/docs/FAQ.md))
Docker|https://hub.docker.com/r/codercom/code-server

This is a repo [cdr/awesome-code-server](https://github.com/cdr/awesome-code-server).

LinuxServer

Item|Url
---|---
Official site|https://www.linuxserver.io
Official doc|https://docs.linuxserver.io/images/docker-code-server
GitHub|https://github.com/linuxserver/docker-code-server
Docker|https://hub.docker.com/r/linuxserver/code-server


## Deployment

### Variables Definition

```sh
service_name='code-server'
service_save_path="$HOME/.docker/${service_name}"
service_configs_path="${service_save_path}/configs"
mkdir -p "${service_configs_path}"
data_dir="/data/${service_name}"

localhost_ip='127.0.0.1'

domain_for_server="${service_name%%-*}.maxdsre.com"
exposted_http_port='8043'  # default 8443

# code dir in host system $HOME/Projects
```

Custom settings json (optional)

```sh
cd "${service_configs_path}"

curl -fsSL -R -O 'https://gitlab.com/axdsop/nixnotes/raw/master/ShellScripts/Toolkits/_Configs/Application/vscode/settings.json'

curl -fsSL -R -O 'https://gitlab.com/axdsop/nixnotes/raw/master/ShellScripts/Toolkits/_Configs/Application/vscode/extension_list'
```

### env

File `.env`

<details><summary>Click to expand env conf</summary>

```sh
tee "${service_save_path}"/.env 1> /dev/null << EOF
# https://github.com/linuxserver/docker-code-server#parameters

# abc:x:1000:1000::/config:/bin/false  in linuxserver/code-server:latest
PUID=1000  # id -u
PGID=1000  # id -g
UMASK=022
TZ=$(cat /etc/timezone)  # America/New_York from file /etc/timezone

EXPOSED_PORT=${localhost_ip}:${exposted_http_port}
PROXY_DOMAIN=${domain_for_server}

# - Extension gallery
EXTENSIONS_GALLERY='{"serviceUrl": "https://marketplace.visualstudio.com/_apis/public/gallery"}'

# - Optional settings
#for web gui password
#PASSWORD=
#HASHED_PASSWORD=

#user will have sudo access in the code-server terminal with the specified password
#SUDO_PASSWORD=
#SUDO_PASSWORD_HASH=

DEFAULT_WORKSPACE=/config/workspace

# custom add
DEFAULT_EXTENSION=/config/extensions

# End
EOF
```

</details>

If you want to use hashed password, you need to generate it.

<details><summary>Click to expand hashed pass generate method</summary>

Doc [Can I store my password hashed?](https://github.com/coder/code-server/blob/main/docs/FAQ.md#can-i-store-my-password-hashed) use command `npx` to generate hash password. But it's very difficult for newbie to use nodejs. Here choose docker image *authelia/authelia* to generate password.

* [Docker Compose HASHED_PASSWORD #5327](https://github.com/coder/code-server/issues/5327#issuecomment-1208106752)
* [Hashed passwords not being read correctly #5127](https://github.com/coder/code-server/issues/5127)

>The issue is caused by the $ signs inside the string.
>
>In order to make this work in your Docker Compose file, you need to change all the single `$` to `$$`.

Attention: env *SUDO_PASSWORD_HASH* doesn't work, see [HASHED_PASSWORD vs SUDO_PASSWORD_HASH; Same hash string provided but only the HASHED_PASSWORD works... #5570](https://github.com/coder/code-server/issues/5570) and [Fix not applied SUDO_PASSWORD_HASH env var #132](https://github.com/linuxserver/docker-code-server/pull/132).

I write a custom function to generate pass and correspond hashed pass.

```sh
fn_Hash_Password_Generation(){
    local l_lenth=68  # pass length
    local l_pass=${1:-}
    local l_pass_hash=''
    local l_pass_hash_format=''

    [[ -n "${l_pass}" ]] || l_pass=$(tr -dc 'a-zA-Z0-9!?@#()&$%{}<>^_+' < /dev/urandom | head -c"${l_lenth}" | xargs)
    # openssl rand -hex 100 | head -c ${l_lenth} | xargs
    l_pass_hash=$(docker run --rm authelia/authelia:latest authelia hash-password "${l_pass}" 2> /dev/null | sed -r -n 's@^[^[:space:]]+:[[:space:]]*@@g;p')
    # $argon2id$v=19$m=65536,t=3,p=4$3jO9wT7lyS/DrsxnJMcXLQ$d3xGiPH0lADAX48KBe5fPBY1drwd6LDPC80WPnqUNkI
    l_pass_hash_format=$(sed -r -n 's@\$@\$&@g;p' <<< "${l_pass_hash}")
    
    echo -e "In order to make this work in your Docker Compose file, you need to change all the single \$ to \$\$.\n"
    # echo -e "Password: \"${l_pass}\"\nHash Password: \"${l_pass_hash}\"\n\nFormatted Hash Password:${l_pass_hash_format}"

    echo -e "For Normal Usage:\nPASSWORD=\"${l_pass}\"\nHASHED_PASSWORD=\"${l_pass_hash_format}\"\n"
    echo -e "For SUDO Usate:\nSUDO_PASSWORD=\"${l_pass}\"\nSUDO_PASSWORD_HASH=\"${l_pass_hash_format}\"\n"
}

fn_Hash_Password_Generation
```

</details>

### docker-compose.yml

Official [Sample](https://github.com/linuxserver/docker-code-server#usage)

```sh
cd "${service_save_path}"

tee docker-compose.yml 1> /dev/null << EOF
version: '3.8'

services:
  code-server:
    image: linuxserver/code-server:latest  # tag 'focal' already deprecated, tag 'latest' is stable releases rebased on focal (Focal Fossa 20.04)
    container_name: code-server
    restart: unless-stopped
    networks:
      - code_server_network
    ports:
      - \${EXPOSED_PORT}:8443
    volumes:
      - ${data_dir}/config:/config
      - ./configs/workspace:/config/workspace:rw
      - ./configs/settings.json:/config/data/User/settings.json:rw
      - \$HOME/Projects:/config/Projects
      #- /etc/localtime:/etc/localtime:ro
    env_file: .env
    # environment:
    #   - HASHED_PASSWORD=
    #   - ...

networks:
  code_server_network:
    name: code_server_network

# End
EOF
```

## Deployment

### Running Container

```sh
cd "${service_save_path}"

# - Via docker compose (For docker-compose V2)
docker compose up -d
# docker compose --env-file "${service_save_path}/docker-compose.yml" up -d
docker compose ps
docker compose down --remove-orphans

# - Via docker-compose (For docker-compose V1)
docker-compose up -d
# docker-compose -f "${service_save_path}/docker-compose.yml" up -d
docker-compose ps
# docker-compose down --remove-orphans
```

### Extension Gallery (optional)

By default [cdr/code-server][cdr_code_server] use the [Open-VSX extension gallery](https://open-vsx.org/) which miss some extensions compared to Microsoft's extension marketplace ([reason](https://github.com/cdr/code-server/blob/main/docs/FAQ.md#why-cant-code-server-use-microsofts-extension-marketplace "Why can't code-server use Microsoft's extension marketplace?"))

<details><summary>Click to expand how to use Microsoft's extension marketplace</summary>

~~Tutotial [Using Open VSX in VS Code](https://github.com/eclipse/openvsx/wiki/Using-Open-VSX-in-VS-Code)~~

From [VSCodium/vscodium](https://github.com/VSCodium/vscodium/blob/master/DOCS.md#extensions--marketplace), I find Microsoft's marketplace service url

Platform|serviceUrl|itemUrl
---|---|---
VSCodium|https://open-vsx.org/vscode/gallery|https://open-vsx.org/vscode/item
VS Code|https://marketplace.visualstudio.com/_apis/public/gallery|/https://marketplace.visualstudio.com/items

But if you want to use Microsoft's extension marketplace in [cdr/code-server][cdr_code_server], see solution [How do I use my own extensions marketplace?](https://github.com/cdr/code-server/blob/main/docs/FAQ.md#how-do-i-use-my-own-extensions-marketplace).

Add the following into file `.env`

```yml
EXTENSIONS_GALLERY='{"serviceUrl": "https://marketplace.visualstudio.com/_apis/public/gallery"}'
```

</details>

In running container, the path of utility `code-server` is */usr/local/bin/code-server*

```sh
cd "${service_save_path}"

# docker compose exec --user=1000 code-server code-server --extensions-dir /config/extensions --install-extension redhat.vscode-yaml

# 1st 'code-server' is service defined in docker compose file (extension dir is /config/extensions)
# 2nd 'code-server' is the utility name in container (/usr/local/bin/code-server)
# sed -r -n '/^[^#]/{p}' ./configs/extension_list | cut -d\| -f2 | xargs -I %s docker compose exec --user=1000 code-server code-server --extensions-dir /config/extensions --install-extension "%s"
```

### Nginx Reverse Proxy

Nginx

```sh
# service_name='code-server'
# domain_for_server="${service_name%%-*}.maxdsre.com"
# exposted_http_port='8043'  # default 8443

backend_private_ip=${backend_private_ip:-'127.0.0.1'}
# backend_private_ip='192.168.8.4'

# Nginx site config dir /etc/nginx/sites-enabled/
nginx_conf_path="/etc/nginx/sites-enabled/${service_name}.conf"
sudo mkdir -p "${nginx_conf_path%/*}"
```

#### Conf

<details><summary>Click to expand config info</summary>

```conf
sudo tee "${nginx_conf_path}" 1> /dev/null << EOF
upstream ${service_name}-default {
    server ${backend_private_ip}:${exposted_http_port};
}

map \$http_upgrade \$connection_upgrade {
    default upgrade;
    '' close;
}

# - For Sync Server
# Redirect HTTP to HTTPS
server {
    listen 80;
    # listen [::]:80 ipv6only=on;
    server_name ${domain_for_server};
    return 301 https://\$host\$request_uri;
}

server {
    listen 443 ssl http2;
    # listen [::]:443 ssl http2 ipv6only=on;
    server_name ${domain_for_server};

    #error_log  /var/log/nginx/${domain_for_server%%.*}_error.log warn;  # [debug|info|notice|warn|error|crit|alert|emerg]
    #access_log /var/log/nginx/${domain_for_server%%.*}_access.log main; # format name 'main' instead of predefined 'combined' format

    include /etc/nginx/conf.d/section/ssl.conf;

    location / {
        #proxy_set_header Host \$host;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto \$scheme;

        # http://nginx.org/en/docs/http/ngx_http_proxy_module.html#proxy_http_version
        proxy_http_version 1.1;
        proxy_set_header Upgrade \$http_upgrade;
        proxy_set_header Connection \$connection_upgrade;
        proxy_set_header Host \$host;
        proxy_set_header Accept-Encoding gzip;

        add_header Content-Security-Policy "default-src 'self';frame-src 'self' code.maxdsre.com;style-src 'self' 'unsafe-inline';style-src-elem 'self' 'unsafe-inline';script-src 'self' 'unsafe-inline' 'unsafe-eval';img-src 'self' data: https://storage.googleapis.com;connect-src 'self' https://storage.googleapis.com;font-src 'self'";

        proxy_pass http://${service_name}-default;

        # https://serverfault.com/questions/587386/an-upstream-response-is-buffered-to-a-temporary-file
        proxy_buffers 16 1M; 
        proxy_buffer_size 10M;
        proxy_busy_buffers_size 10M;
    }
}

# End
EOF
```

</details>

Testing Nginx conf syntax then reload

```sh
sudo -i

nginx -t && nginx -s reload

# nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
# nginx: configuration file /etc/nginx/nginx.conf test is successful
```

## Utility code-server

Normal user is `abc` (UID=1000) in container

```sh
docker compose exec -ti code-server grep 1000 /etc/passwd

# abc:x:1000:1000::/config:/bin/false
```

Utiltiy *code-server* path in container is */app/code-server/bin/code-server*

```sh
# check help info
docker compose exec -ti code-server /app/code-server/bin/code-server --help
```

<details><summary>Click to expand help info</summary>

Utility `code-server` help info

```txt
code-server 4.9.0 0502dfa1ff42ab8a43adb911f7bf21f8b09ee25f with Code 1.73.1

Usage: code-server [options] [path]
    - Opening a directory: code-server ./path/to/your/project
    - Opening a saved workspace: code-server ./path/to/your/project.code-workspace

Options
      --auth                             The type of authentication to use. [password, none]
      --password                         The password for password authentication (can only be passed in via $PASSWORD or the config file).
      --hashed-password                  The password hashed with argon2 for password authentication (can only be passed in via $HASHED_PASSWORD or the config file).
                                         Takes precedence over 'password'.
      --cert                             Path to certificate. A self signed certificate is generated if none is provided.
      --cert-host                        Hostname to use when generating a self signed certificate.
      --cert-key                         Path to certificate key when using non-generated cert.
      --disable-telemetry                Disable telemetry.
      --disable-update-check             Disable update check. Without this flag, code-server checks every 6 hours against the latest github release and
                                         then notifies you once every week that a new release is available.
      --disable-file-downloads           Disable file downloads from Code. This can also be set with CS_DISABLE_FILE_DOWNLOADS set to 'true' or '1'.
      --disable-workspace-trust          Disable Workspace Trust feature. This switch only affects the current session.
      --disable-getting-started-override Disable the coder/coder override in the Help: Getting Started page.
   -h --help                             Show this output.
      --open                             Open in browser on startup. Does not work remotely.
      --bind-addr                        Address to bind to in host:port. You can also use $PORT to override the port.
      --config                           Path to yaml config file. Every flag maps directly to a key in the config file.
      --socket                           Path to a socket (bind-addr will be ignored).
      --socket-mode                      File mode of the socket.
   -v --version                          Display version information.
      --user-data-dir                    Path to the user data directory.
      --extensions-dir                   Path to the extensions directory.
      --list-extensions                  List installed VS Code extensions.
      --force                            Avoid prompts when installing VS Code extensions.
      --install-extension                Install or update a VS Code extension by id or vsix. The identifier of an extension is `${publisher}.${name}`.
                                         To install a specific version provide `@${version}`. For example: 'vscode.csharp@1.2.3'.
      --enable-proposed-api              Enable proposed API features for extensions. Can receive one or more extension IDs to enable individually.
      --uninstall-extension              Uninstall a VS Code extension by id.
      --show-versions                    Show VS Code extension versions.
      --github-auth                      GitHub authentication token (can only be passed in via $GITHUB_TOKEN or the config file).
      --proxy-domain                     Domain used for proxying ports.
   -e --ignore-last-opened               Ignore the last opened directory or workspace in favor of an empty window.
   -n --new-window                       Force to open a new window.
   -r --reuse-window                     Force to open a file or folder in an already opened window.
 -vvv --verbose                          Enable verbose logging.
  -an --app-name                         The name to use in branding. Will be shown in titlebar and welcome message
   -w --welcome-text                     Text to show on login page
      --link                             (deprecated) Securely bind code-server via our cloud service with the passed name. You'll get a URL like
                                         https://hostname-username.coder.co at which you can easily access your code-server instance.
                                         Authorization is done via GitHub.
```

</details>

### Extension Install

Default extension dir in container is `/config/extensions/`, default normal user is  *abc*.

```sh
# -T, --no-TTY docker compose exec Disable pseudo-TTY allocation. By default docker compose exec allocates a TTY

extension_dir=${extension_dir:-'/config/extensions'}
# if not specify extension dir, it will intall extensions into /config/.local/share/code-server/ in container.
[[ -f .env ]] && extension_dir=$(sed -r -n '/DEFAULT_EXTENSION=/{s@^[^=]+=@@g;s@"@@g;s@'\''@@g;p}' .env 2> /dev/null)

# - list installed extensions
# docker compose exec -ti / docker compose exec -T
docker compose exec -T code-server /app/code-server/bin/code-server --extensions-dir="${extension_dir}" --list-extensions

# - install extension
# User UID=1000 is 'abc', parameter '--user abc' or '--user=1000' is optional
# sed -r -n '/^$/d; /^#/d; s@^.*?\|([^[:space:]]+)[[:space:]]*@\1@g;p' configs/extension_list
sed -r -n '/^[^#]/{p}' configs/extension_list | cut -d\| -f2 | xargs -I %s docker compose exec -T code-server /app/code-server/bin/code-server --extensions-dir="${extension_dir}" --force --install-extension %s


# - uninstall
docker compose exec -T code-server /app/code-server/bin/code-server --extensions-dir="${extension_dir}" --uninstall-extension 'VisualStudioExptTeam.intellicode-api-usage-examples'
# uninstall jupyter relevant
docker compose exec -T code-server /app/code-server/bin/code-server --extensions-dir="${extension_dir}" --list-extensions | sed '/jupyter/I!d' | xargs -I %s docker compose exec -T code-server /app/code-server/bin/code-server --extensions-dir="${extension_dir}" --uninstall-extension %s 2> /dev/null
```


## Issue Occuring

### WebSocket Proxy Error

>An unexpected error occurred that requires a reload of this page.
>>The workbench failed to connect to the server (Error: WebSocket close with status code 1006)

From web broser developer tool

>WebSocket connection to 'wss://code.maxdsre.com/?type=Management&reconnectionToken=45518db6-84cc-4e86-acd4-9a85c51d5c58&reconnection=false&skipWebSocketFrames=false' failed:


Solution

* [Allowing web socket connection when using NGINX as reverse proxy](https://ajmaradiaga.com/Allowing-web-sockets-when-using-NGINX-as-reverse-proxy/)
* [nginx - NGINX as a WebSocket Proxy](https://www.nginx.com/blog/websocket-nginx/)
* [coder - Using Let's Encrypt with NGINX](https://coder.com/docs/code-server/latest/guide#using-lets-encrypt-with-nginx)


### Hash Password

[Hashed code-server password](https://github.com/linuxserver/docker-code-server#hashed-code-server-password) says

>How to create the [hashed password](https://github.com/cdr/code-server/blob/master/docs/FAQ.md#can-i-store-my-password-hashed).

Use parametet `HASHED_PASSWORD` prompt error

> TypeError: Cannot read property 'length' of undefined

Still not resolved.


## Tutorial

* [Self-Hosted SSO with Authelia and NGINX](https://matwick.ca/authelia-nginx-sso/)


## Change Log

* Dec 07, 2022 11:14 Wed ET
  * add extension install method
* Nov 29, 2022 16:15 Tue ET
  * add password generating method
* Nov 19, 2021 18:17 Fri ET
  * First draft


[vscode]:https://github.com/Microsoft/vscode
[cdr_code_server]:https://github.com/cdr/code-server/
[linuxserver_code_server]:https://github.com/linuxserver/docker-code-server

<!-- End -->