# Self-hosting Invidious

[Invidious][invidious] is an alternative front-end to YouTube.

## TOC

1. [Official Site](#official-site)  
2. [Deployment](#deployment)  
2.1 [Variables Definition](#variables-definition)  
2.2 [env](#env)  
2.3 [docker-compose.yml](#docker-composeyml)  
2.4 [Running Container](#running-container)  
2.5 [Nginx Reverse Proxy](#nginx-reverse-proxy)  
2.5.1 [Conf](#conf)  
3. [Maintance](#maintance)  
3.1 [Database](#database)  
4. [Issue Occuring](#issue-occuring)  
5. [Change Log](#change-log)  


## Official Site

Item|Url
---|---
Official Site|https://invidious.io
Official Doc|https://docs.invidious.io
GitHub|https://github.com/iv-org/invidious
Docker Hub (Quay)|https://quay.io/repository/invidious/invidious


## Deployment

Official installation page [Installation](https://docs.invidious.io/installation/).

Attention: The environment variable `POSTGRES_USER` cannot be changed. The SQL config files that run the initial database migrations are hard-coded with the username **kemal**.


### Variables Definition

```sh
service_name='invidious'
service_save_path="$HOME/.docker/${service_name}"
mkdir -p "${service_save_path}"

data_dir="/data/"${service_name}""
config_save_dir="${data_dir}/config"

localhost_ip='127.0.0.1'
domain_for_server="${service_name%%-*}.maxdsre.com"
exposted_http_port='8500'  # default 3000

EXPOSED_PORT=${localhost_ip}:${exposted_http_port}

POSTGRES_DB=invidious
POSTGRES_USER=kemal  # The SQL config files that run the initial database migrations are hard-coded with the username 'kemal'.
POSTGRES_PASSWORD=$(openssl rand -hex 64 | head -c 45)

INVIDIOUS_HMAC_KEY=$(openssl rand -hex 20)
```

### env

File `.env`

<details><summary>Click to expand env conf</summary>

```sh
tee "${service_save_path}"/.env 1> /dev/null << EOF
# https://github.com/iv-org/invidious/blob/master/config/config.example.yml
# https://docs.invidious.io/configuration/

EXPOSED_PORT=${EXPOSED_PORT}
PROXY_DOMAIN=${domain_for_server}

# - Database
#/var/lib/postgresql/data  PostgreSQL Data

#POSTGRES_HOST  # Hostname of the database server using postgres.
POSTGRES_DB=${POSTGRES_DB}
POSTGRES_USER=${POSTGRES_USER}
POSTGRES_PASSWORD=${POSTGRES_PASSWORD}

# - Optional
# https://github.com/iv-org/invidious/issues/3854
INVIDIOUS_HMAC_KEY=${INVIDIOUS_HMAC_KEY}

# End
EOF
```

</details>

Downloading essential config files

```sh
cd "${service_save_path}"

git clone https://github.com/iv-org/invidious.git
cp -p -r invidious/config .
cp -p -r invidious/docker ./config/
rm -rf invidious
```

### docker-compose.yml

```sh
cd "${service_save_path}"

tee docker-compose.yml 1> /dev/null << EOF
version: '3.8'

services:
  invidious:
    image: quay.io/invidious/invidious:latest
    # image: quay.io/invidious/invidious:latest-arm64 # ARM64/AArch64 devices
    container_name: invidious-server
    restart: unless-stopped
    networks:
      - invidious_network
    ports:
      - "\${EXPOSED_PORT}:3000"
    env_file: .env
    environment:
      # Please read the following file for a comprehensive list of all available configuration options and their associated syntax:
      # https://github.com/iv-org/invidious/blob/master/config/config.example.yml
      # https://docs.invidious.io/configuration/
      INVIDIOUS_CONFIG: |
        db:
          dbname: ${POSTGRES_DB}
          user: ${POSTGRES_USER}
          password: ${POSTGRES_PASSWORD}
          host: invidious_db
          port: 5432
        check_tables: true
        # external_port:
        # domain:
        # https_only: false
        # statistics_enabled: false
        use_quic: true
        # log_level: Info  # Accepted values: All, Trace, Debug, Info, Warn, Error, Fatal, Off
        # statistics_enabled: false
        registration_enabled: false
        # login_enabled: true
        # force_resolve: ipv4
        locale: en-US
        region: US
        quality: dash  # Accepted values: dash, hd720, medium, small
        quality_dash: 720p
        popular_enabled: true
        use_quic: true
        # captcha_enabled: true
        # banner: 
        hmac_key: \${INVIDIOUS_HMAC_KEY}  # since Jul 01, 2023, it's mandatory   https://github.com/iv-org/invidious/issues/3854

    healthcheck:
      test: wget -nv --tries=1 --spider http://${EXPOSED_PORT}/api/v1/comments/jNQXAC9IVRw || exit 1
      interval: 30s
      timeout: 5s
      retries: 2
    depends_on:
      - invidious_db


  invidious_db:
    image: postgres:14-alpine  # v14, alpine is v16 on Aug 12, 2024
    container_name: invidious-db
    restart: unless-stopped
    networks:
      - invidious_network
    env_file: .env
    environment:
      - POSTGRES_DB=\${POSTGRES_DB}
      - POSTGRES_USER=\${POSTGRES_USER}
      - POSTGRES_PASSWORD=\${POSTGRES_PASSWORD}
    volumes:
      - ${data_dir}/db:/var/lib/postgresql/data
      - ./config/sql:/config/sql
      - ./config/docker/init-invidious-db.sh:/docker-entrypoint-initdb.d/init-invidious-db.sh
      # - /etc/localtime:/etc/localtime:ro
    healthcheck:
      test: ["CMD-SHELL", "pg_isready -U \$\$POSTGRES_USER -d \$\$POSTGRES_DB"]

networks:
  invidious_network:
    name: invidious_network

# End
EOF
```

### Running Container

```sh
cd "${service_save_path}"

# - Via docker compose (For docker-compose V2)
docker compose up -d
# docker compose --env-file "${service_save_path}/docker-compose.yml" up -d
docker compose ps
docker compose down --remove-orphans

# - Via docker-compose (For docker-compose V1)
docker-compose up -d
# docker-compose -f "${service_save_path}/docker-compose.yml" up -d
docker-compose ps
# docker-compose down --remove-orphans
```

### Nginx Reverse Proxy

Nginx

```sh
service_name='invidious'
domain_for_server="${service_name%%-*}.maxdsre.com"
exposted_http_port='8500'  # default 3000

backend_private_ip=${backend_private_ip:-'127.0.0.1'}
# backend_private_ip='192.168.8.4'

# Nginx site config dir /etc/nginx/sites-enabled/
nginx_conf_path="/etc/nginx/sites-enabled/${service_name}.conf"
sudo mkdir -p "${nginx_conf_path%/*}"
```

#### Conf

<details><summary>Click to expand config info</summary>

```conf
sudo tee "${nginx_conf_path}" 1> /dev/null << EOF
upstream ${service_name}-default {
    server ${backend_private_ip}:${exposted_http_port};
}

map \$http_upgrade \$connection_upgrade {
    default upgrade;
    '' close;
}

# - For Server
# Redirect HTTP to HTTPS
server {
    listen 80;
    # listen [::]:80 ipv6only=on;
    server_name ${domain_for_server};
    return 301 https://\$host\$request_uri;
}

server {
    listen 443 ssl http2;
    # listen [::]:443 ssl http2 ipv6only=on;
    server_name ${domain_for_server};

    error_log  /var/log/nginx/${domain_for_server%%.*}_error.log warn;  # [debug|info|notice|warn|error|crit|alert|emerg]
    access_log /var/log/nginx/${domain_for_server%%.*}_access.log main; # format name 'main' instead of predefined 'combined' format

    include /etc/nginx/conf.d/section/ssl.conf;

    location / {
        #proxy_set_header Host \$host;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto \$scheme;

        # http://nginx.org/en/docs/http/ngx_http_proxy_module.html#proxy_http_version
        proxy_http_version 1.1;
        proxy_set_header Upgrade \$http_upgrade;
        proxy_set_header Connection \$connection_upgrade;
        proxy_set_header Host \$host;
        proxy_set_header Accept-Encoding gzip;

        # https://serverfault.com/questions/587386/an-upstream-response-is-buffered-to-a-temporary-file
        # https://www.cyberciti.biz/faq/nginx-upstream-sent-too-big-header-while-reading-response-header-from-upstream/
        proxy_buffering off;
        #proxy_buffers   4 512k;
        #proxy_buffer_size   256k;
        #proxy_busy_buffers_size   512k;

        add_header Content-Security-Policy "default-src 'none'; child-src 'self'; font-src 'self' data:; connect-src 'self' wss: ws:; media-src 'self' https: blob: data:; manifest-src 'self'; base-uri 'none'; form-action 'self'; frame-src 'self'; frame-ancestors 'self'; object-src 'none'; worker-src 'self' blob:; script-src 'self'; img-src data: https:; style-src 'unsafe-inline' 'self'" always;

        proxy_pass http://${service_name}-default;
    }
}

# End
EOF
```

</details>

Testing Nginx conf syntax then reload

```sh
sudo -i

nginx -t && nginx -s reload

# nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
# nginx: configuration file /etc/nginx/nginx.conf test is successful
```

## Maintance

### Database

Official doc [Database Information and Maintenance](https://docs.invidious.io/db-maintenance/)

Connect PostgreSQL database container

```sh
# invidious_db is service name defined in docker_compose.yml
docker compose exec invidious_db sh

# user name is 'kemal' (hard-coded), database name is 'invidious'
psql -U kemal invidious

# list database list
\l

# connect database 'invidious'
\c invidious
# You are now connected to database "invidious" as user "kemal".

# list tables
\d

# show table info
\d+ oc_activity

# show items count
select count(*) from nonces;

# delete action
delete from nonces * where expire < current_timestamp;

# truncate action
truncate table videos;

# exit
exit
```

>The table *videos* grows a lot and needs the most storage.

```sh
cd "${service_save_path}"

postgresql_db_info=$(sed -r -n '/^POSTGRES_/{s@^[^=]+=@@g;s@[[:space:]]*$@@g;p}' .env | sed ':a;N;$!ba;s@\n@|@g')
# POSTGRES_DB|POSTGRES_USER|POSTGRES_PASSWORD

db_host='invidious_db'
db_name=$(cut -d\| -f1 <<< "${postgresql_db_info}")
db_user=$(cut -d\| -f2 <<< "${postgresql_db_info}")
db_pass=$(cut -d\| -f3 <<< "${postgresql_db_info}")

# - clean table videos items
docker compose exec "${db_host}" sh -c 'PGPASSWORD="'${db_pass}'" psql "'${db_name}'" -h localhost -U "'${db_user}'" -c "delete from nonces * where expire < current_timestamp; truncate table videos; truncate table channel_videos;"' 1> /dev/null

# - dump database
# pg_dump path in container is /usr/local/bin/pg_dump
# PGPASSWORD="password" pg_dump [db_name] -h [server] -U [username] -f nextcloud-sqlbkp_`date +"%Y%m%d"`.bak  # https://docs.nextcloud.com/server/latest/admin_manual/maintenance/backup.html#postgresql
sql_save_dir=${sql_save_dir:-"$PWD/data/sql"}
sql_save_dir='/tmp'
[[ -d "${sql_save_dir}" ]] || mkdir -p "${sql_save_dir}"
sql_save_path="${sql_save_dir}/invidious_pqsql_dump_$(date +'%Y%m%d_%H%M%S').sql"

docker compose exec "${db_host}" sh -c 'PGPASSWORD="'${db_pass}'" pg_dump "'${db_name}'" -h localhost -U "'${db_user}'"' > "${sql_save_path}"
```


## Issue Occuring

>The media could not be loaded, either because the server or network failed or because the format is not supported.

Issue [The media could not be loaded, either because the server or network failed or because the format is not supported](https://github.com/iv-org/invidious/issues/801)

>VIDEOJS: ERROR: (CODE:4 MEDIA_ERR_SRC_NOT_SUPPORTED) No compatible source was found for this media.

* [No compatible source was found for this media. #263](https://github.com/iv-org/invidious/issues/263)
* [VIDEOJS: ERROR: (CODE:4 MEDIA_ERR_SRC_NOT_SUPPORTED) No compatible source was found for this media. #5606](https://github.com/videojs/video.js/issues/5606)

Solution like issue [263](https://github.com/iv-org/invidious/issues/263#issuecomment-446736155) says: I actually fixed the problem. I had changed the settings to Preferred Video Quality and selected small. I guess that one doesn't work on my computer. I changed it back to hd720 and it's working again.

Official doc [Geoblocking, available video quality and DASH](https://docs.invidious.io/geoblocking/) says:

>DASH is a streaming technique used by YouTube to provide resolutions higher than 720p by providing multiple files for a client to use depending on network and user preferences.
>
>You can enable DASH by selecting the appropriately named video quality in the settings or by appending `&quality=dash` to the end of a video's URL. With this option enabled, the stream is proxied through Invidious for you to then watch at a higher or automatic quality.

Solution is change `quality_dash` from *auto* to *720p*.

```yml
services:
  invidious:
    ...
    ...
    environment:
        quality: dash  # Accepted values: dash, hd720, medium, small
        quality_dash: 720p
```

## Change Log

* Jun 20, 2022 09:40 Mon ET
  * Add database maintance and dump action
* May 26, 2022 09:50 Thu ET
  * First draft


[invidious]:https://invidious.io "Invidious - an open source alternative front-end to YouTube."


<!-- End -->