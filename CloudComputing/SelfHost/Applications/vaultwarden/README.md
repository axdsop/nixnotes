# Self-hosting Bitwarden

[Bitwarden][bitwarden] is a open source password manager.

[vaultwarden][vaultwarden] is a unofficial [Bitwarden][bitwarden] compatible server written in Rust.

**Note**: From [1.21.x](https://github.com/dani-garcia/vaultwarden/releases/tag/1.21.0) release (Apr 30, 2021) and project was renamed from **bitwarden_rs** to **vaultwarden** (See issue [#1644](https://github.com/dani-garcia/vaultwarden/issues/1644)).

**Advice**: If you plan to enable *Two-step Login*, you'd better enable at least 2 methods. For example, if you only enable *YubiKey OTP Security Key* with a [YubiKey 5 Nano](https://www.yubico.com/us/product/yubikey-5-nano/), you can't use it to verify for your mobile phone. But if you added another method like *Authenticator App*, then you can use this method to verify for your mobile phone.


## TOC

1. [Official Site](#official-site)  
2. [Deployment](#deployment)  
2.1 [Configs](#configs)  
2.1.1 [.env](#env)  
2.1.2 [docker-compose.yml](#docker-composeyml)  
2.2 [Running Container](#running-container)  
2.3 [Nginx Reverse Proxy](#nginx-reverse-proxy)  
2.3.1 [htpasswd](#htpasswd)  
2.3.2 [Conf](#conf)  
3. [Usage](#usage)  
4. [Snapshots](#snapshots)  
4.1 [Vault page](#vault-page)  
4.2 [Admin panel](#admin-panel)  
5. [Error Occuring](#error-occuring)  
5.1 [upstream response is buffered](#upstream-response-is-buffered)  
5.2 [WebSocket Protocol Error](#websocket-protocol-error)  
6. [Bibliography](#bibliography)  
7. [Change Log](#change-log)  


## Official Site

Item|Url
---|---
GitHub|https://github.com/dani-garcia/vaultwarden
GitHub Wiki|https://github.com/dani-garcia/vaultwarden/wiki
Forum|https://bitwardenrs.discourse.group
Docker|https://hub.docker.com/u/vaultwarden

Bitwrden doc

* [On-premises Hosting](https://bitwarden.com/help/article/install-on-premise/)

## Deployment

[vaultwarden][vaultwarden] is running as a [Docker][docker] container.

Cloud firewalls define `reverse` droplet can listen TCP port range (8000-9000) exposed by `backend` droplets which running docker service.

I write a note [Secure Nginx With Let's Encrypt Free SSL Certificate](/CyberSecurity/WebServer/Nginx/IssueSSLCert.md) to configure Nginx.

### Configs

Docker image

```sh
# https://hub.docker.com/r/vaultwarden/server
# https://github.com/dani-garcia/vaultwarden/blob/master/docker/amd64/Dockerfile (deprecated)
# https://github.com/dani-garcia/vaultwarden/blob/main/docker/Dockerfile.debian
# Image exposes port 80, 3012 by default.
docker pull vaultwarden/server:latest

# 80 for standard Rocket server
# 3012 for WebSocket notifications
```

To enable *admin panel*, you need to configure environment variable `ADMIN_TOKEN` (Default is plain text format). From version `1.28.0` (Mar 27, 2023), you can hash the `ADMIN_TOKEN` using Argon2 by generating a PHC string. (Source [secure-the-admin_token](https://github.com/dani-garcia/vaultwarden/wiki/Enabling-admin-page#secure-the-admin_token)).

>The admin panel is disabled, please configure the 'ADMIN_TOKEN' variable to enable it.

About WebSocket

>WebSocket's are enabled by default since v1.29.0 of Vaultwarden. Previous versions needed a reverse proxy because WebSockets were running on a different port than then default HTTPS port. The old implementation is still available in v1.29.0 to not break during updates for now. But this will be removed in the future.
>
>The old `WEBSOCKET_ENABLED` and `WEBSOCKET_PORT` are not needed anymore since v1.29.0 of Vaultwarden and can be ignored.

Variables definition

```sh
service_name='vaultwarden'
container_name='vaultwarden_server'
domain_name_specify="${service_name}.maxdsre.com"

data_dir="/data/${service_name}"
localhost_ip='127.0.0.1'
server_port='8000'
# websocket_port='8012'  # Don't need any more since v1.29.0.

# - Harden
# https://github.com/dani-garcia/vaultwarden/wiki/Hardening-Guide
# https://docs.docker.com/engine/reference/commandline/run/#set-environment-variables--e---env---env-file

service_save_path="$HOME/.docker/${service_name}"
mkdir -p "${service_save_path}"
# touch "${service_save_path}/{.env,docker-compose.yml}"
```

#### .env

```sh
tee "${service_save_path}/.env" 1> /dev/null << EOF
# https://github.com/dani-garcia/vaultwarden/blob/main/.env.template

# ------- Custom Variables ------- #
# Exposed port
SERVER_PORT=${localhost_ip}:${server_port}  # default 80

# Websocket (WebSocket's are enabled by default since v1.29.0 of Vaultwarden, and the old `WEBSOCKET_ENABLED` and `WEBSOCKET_PORT` are not needed anymore.)
#WEBSOCKET_PORT=${localhost_ip}:${websocket_port}  # default 3012
# enable WebSocket notifications
#WEBSOCKET_ENABLED=true

# Data mount point
DATA_MOUNT_PATH="${data_dir}"
# Container name
CONTAINER_NAME="${container_name}"


# ------- Environment Variables ------- #
# timezon
TZ=America/New_York

# domain name
DOMAIN=https://${domain_name_specify}

# enable admin page  # openssl rand -base64 48
# Since version 1.28.0  # https://github.com/dani-garcia/vaultwarden/wiki/Enabling-admin-page#secure-the-admin_token
#ADMIN_TOKEN=63ilJwU3RIvwuiMilZ4t/XcbxNYZ29SMT53QBO5FrLmwIlpXRmlT9p9CqkEr49gj
ADMIN_TOKEN='$argon2id$v=19$m=65540,t=3,p=4$6IMA/8/Qo9qQoUnjR3MVgQ7sDtFnYlCVq1hBqR6ngyU$5lNkLhQiSNS5kSvB8LoLH7gu5ff3O+7MwJCqX2owxkw'

## Controls whether users are allowed to create Bitwarden Sends.
#SENDS_ALLOWED=true

# disable registration of new users
SIGNUPS_ALLOWED=false

# disable invitations
INVITATIONS_ALLOWED=false


# enable Yubikey OTP authentication
# https://github.com/dani-garcia/vaultwarden/wiki/Enabling-Yubikey-OTP-authentication
# https://upgrade.yubico.com/getapikey/
#YUBICO_CLIENT_ID=62364
#YUBICO_SECRET_KEY=EtfAlxwzW3f0K1Qrzvth9NF5MFM=

# disable password hint display
SHOW_PASSWORD_HINT=false

# https://github.com/dani-garcia/vaultwarden/wiki/Logging
LOG_FILE=/data/vaultwarden.log
LOG_LEVEL=warn
EXTENDED_LOGGING=true

# End
EOF

# Your account's fingerprint phrase:
# task-slacking-distrust-hyperlink-viability

# Your Bitwarden two-step login recovery code:
# FP6D S6JW B4OY VMEE HIA5 ZWBP LRJK XR3P
```

#### docker-compose.yml

[Docker Compose][docker_compose] is a tool for defining and running multi-container Docker applications. Official install doc [Install Docker Compose](https://docs.docker.com/compose/install/).

I write a Shell script [docker_compose.sh](/CloudComputing/Containers/Docker/Scripts/docker_compose.sh) to install it.

<details><summary>Click to expand install command</summary>

```sh
download_command='wget -qO-'
[[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && download_command='curl -fsSL'

${download_command} https://gitlab.com/axdsop/nixnotes/raw/master/CloudComputing/Containers/Docker/Scripts/docker_compose.sh | sudo bash -s --
```

</details>

If you run container via file *docker-compose.yml*.

```yml
# This is a docker-compose file that can be used to run bitwardenrs Server.
version: '3'

services:
    app:
        # https://github.com/dani-garcia/vaultwarden/wiki/Which-container-image-to-use#image-tags
        # https://hub.docker.com/r/vaultwarden/server
        image: vaultwarden/server:latest
        container_name: ${CONTAINER_NAME}
        ports:
            - "${SERVER_PORT}:80"
            #- "${WEBSOCKET_PORT}:3012"  # don't need anymore since v1.29.0.
        restart: unless-stopped
        volumes:
            - ${DATA_MOUNT_PATH}:/data
        #environment:
        env_file:
            - .env

# End
```

### Running Container

Via utility `docker-compose` (if existed) or command `docker compose`

```sh
cd  "${service_save_path}"

# - Via docker compose (For docker-compose V2)
docker compose up -d
# docker compose --env-file "${service_save_path}/docker-compose.yml" up -d
docker compose ps
docker compose down --remove-orphans

# - Via docker-compose (For docker-compose V1)
docker-compose up -d
# docker-compose -f "${service_save_path}/docker-compose.yml" up -d
docker-compose ps
# docker-compose down --remove-orphans
```

Via docker

```sh
# run as root in container
docker run -d --restart unless-stopped --name "${container_name}" \
    --env-file "${service_save_path}/.env" \
    -v "${data_dir}":/data/ \
    -p "${server_port}":80 \
    bitwardenrs/server:latest

#     -p "${websocket_port}":3012 \
```

For updating image

```sh
# https://github.com/dani-garcia/vaultwarden/wiki/Updating-the-bitwarden-image

# Pull the latest version
docker pull vaultwarden/server:latest

# Stop and remove the old container
docker stop ${container_name}
docker rm -f ${container_name}

# Start new container
```


### Nginx Reverse Proxy

Nginx config comes from official wiki [Proxy examples](https://github.com/dani-garcia/vaultwarden/wiki/Proxy-examples).

Variables definition

```sh
service_name='vaultwarden'
domain_name_specify="${service_name}.maxdsre.com"

backend_private_ip=${backend_private_ip:-'127.0.0.1'}
backend_private_ip='192.168.8.4'
server_port='8000'
websocket_port='8012'

# Nginx site config dir /etc/nginx/sites-enabled/
nginx_conf_path="/etc/nginx/sites-enabled/${service_name}.conf"
mkdir -p "${nginx_conf_path%/*}"

# optional
htpasswd_path="/etc/nginx/sites-enabled/.htpasswd_${service_name}"
```

#### htpasswd

(optional) If you wanna [Restricting Access with HTTP Basic Authentication](https://docs.nginx.com/nginx/admin-guide/security-controls/configuring-http-basic-authentication/) via parameter *auth_basic*, *auth_basic_user_file*, remember to uncomment them.

<details><summary>Click to expand htpasswd operation</summary>

```sh
# sudo apt-get install -y apache2-utils

sudo htpasswd -c "${htpasswd_path}" user1
# sudo htpasswd "${htpasswd_path}" user2  # if htpasswd file existed
```

</details>

#### Conf

Attention: if you sepcified header *Content-Security-Policy* in Nginx conf, it may brokes the page rendering in web browser. I tweak the policy listed in the following conf to make bitwarden_rs work properly.

Nginx conf

Version 1.29.0+

```conf
sudo tee "${nginx_conf_path}" 1> /dev/null << EOF
# For v1.29.0+

upstream ${service_name}-default {
    zone ${service_name}-default 64k;
    server ${backend_private_ip}:${server_port};
    keepalive 2;
}

# Redirect HTTP to HTTPS
server {
    listen 80;
    # listen [::]:80 ipv6only=on;
    server_name ${domain_name_specify};
    return 301 https://\$host\$request_uri;
}

map \$request_uri \$log_ignore {
    # bwrs_static used by /admin
    ~^/(icons|fonts|images|locales|notifications|app|bwrs_static)/ 0;
    ~^/(api/sync|favicon.ico)$ 0;
    default 1;
}

server {
    listen 443 ssl http2;
    # listen [::]:443 ssl http2 ipv6only=on;
    server_name ${domain_name_specify};

    error_log  /var/log/nginx/vaultwarden_error.log warn;  # [debug|info|notice|warn|error|crit|alert|emerg]
    access_log /var/log/nginx/vaultwarden_access.log main if=\$log_ignore; # format name 'main' instead of predefined 'combined' format

    # if specified SSL cert
    include /etc/nginx/conf.d/section/ssl.conf;

    client_max_body_size 64M;

    ## Using a Sub Path Config
    # Path to the root of your installation
    location / {
      proxy_set_header Host \$host;
      proxy_set_header X-Real-IP \$remote_addr;
      proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
      proxy_set_header X-Forwarded-Proto \$scheme;
      proxy_pass http://${service_name}-default;

      # https://serverfault.com/questions/587386/an-upstream-response-is-buffered-to-a-temporary-file
      # https://www.cyberciti.biz/faq/nginx-upstream-sent-too-big-header-while-reading-response-header-from-upstream/
      proxy_buffers   4 256k;
      proxy_buffer_size   128k;
      proxy_busy_buffers_size   256k;

      # if you sepcifiy header 'Content-Security-Policy', the following policy is tweaked for bitwarden_rs 
      add_header Content-Security-Policy "default-src 'none'; script-src 'unsafe-inline' 'self'; font-src 'self'; connect-src 'self'; img-src 'self' data:; style-src 'unsafe-inline' 'self'; base-uri 'self'; manifest-src 'self'; form-action 'self'";  # https://content-security-policy.com
    }

    # Optionally add extra authentication besides the ADMIN_TOKEN
    # If you don't want this, leave this part out
    location ^~ /admin {
      # See: https://docs.nginx.com/nginx/admin-guide/security-controls/configuring-http-basic-authentication/
      #auth_basic "Administrator's Private Area";
      #auth_basic_user_file ${htpasswd_path};

      proxy_set_header Host \$host;
      proxy_set_header X-Real-IP \$remote_addr;
      proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
      proxy_set_header X-Forwarded-Proto \$scheme;

      proxy_pass http://${service_name}-default;
    }
}
EOF
```

Before v1.29.0

```conf
sudo tee "${nginx_conf_path}" 1> /dev/null << EOF
upstream ${service_name}-default {
    server ${backend_private_ip}:${server_port};
}

upstream ${service_name}-ws {
    server ${backend_private_ip}:${websocket_port};
}

# Redirect HTTP to HTTPS
server {
    listen 80;
    # listen [::]:80 ipv6only=on;
    server_name ${domain_name_specify};
    return 301 https://\$host\$request_uri;
}

map \$request_uri \$log_ignore {
    # bwrs_static used by /admin
    ~^/(icons|fonts|images|locales|notifications|app|bwrs_static)/ 0;
    ~^/(api/sync|favicon.ico)$ 0;
    default 1;
}

server {
    listen 443 ssl http2;
    # listen [::]:443 ssl http2 ipv6only=on;
    server_name ${domain_name_specify};

    error_log  /var/log/nginx/vaultwarden_error.log warn;  # [debug|info|notice|warn|error|crit|alert|emerg]
    access_log /var/log/nginx/vaultwarden_access.log main if=\$log_ignore; # format name 'main' instead of predefined 'combined' format

    # if specified SSL cert
    include /etc/nginx/conf.d/section/ssl.conf;

    client_max_body_size 64M;

    ## Using a Sub Path Config
    # Path to the root of your installation
    location / {
      proxy_set_header Host \$host;
      proxy_set_header X-Real-IP \$remote_addr;
      proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
      proxy_set_header X-Forwarded-Proto \$scheme;
      proxy_pass http://${service_name}-default;

      # https://serverfault.com/questions/587386/an-upstream-response-is-buffered-to-a-temporary-file
      # https://www.cyberciti.biz/faq/nginx-upstream-sent-too-big-header-while-reading-response-header-from-upstream/
      proxy_buffers   4 256k;
      proxy_buffer_size   128k;
      proxy_busy_buffers_size   256k;

      # if you sepcifiy header 'Content-Security-Policy', the following policy is tweaked for bitwarden_rs 
      add_header Content-Security-Policy "default-src 'none'; script-src 'unsafe-inline' 'self'; font-src 'self'; connect-src 'self'; img-src 'self' data:; style-src 'unsafe-inline' 'self'; base-uri 'self'; manifest-src 'self'; form-action 'self'";  # https://content-security-policy.com
    }

    location /notifications/hub/negotiate {
      proxy_set_header Host \$host;
      proxy_set_header X-Real-IP \$remote_addr;
      proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
      proxy_set_header X-Forwarded-Proto \$scheme;

      proxy_pass http://${service_name}-default;
    }

    location /notifications/hub {
      proxy_set_header Upgrade \$http_upgrade;
      proxy_set_header Connection \$http_connection;
      proxy_set_header X-Real-IP \$remote_addr;

      proxy_pass http://${service_name}-ws;
    }

    # Optionally add extra authentication besides the ADMIN_TOKEN
    # If you don't want this, leave this part out
    location ^~ /admin {
      # See: https://docs.nginx.com/nginx/admin-guide/security-controls/configuring-http-basic-authentication/
      #auth_basic "Administrator's Private Area";
      #auth_basic_user_file ${htpasswd_path};

      proxy_set_header Host \$host;
      proxy_set_header X-Real-IP \$remote_addr;
      proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
      proxy_set_header X-Forwarded-Proto \$scheme;

      proxy_pass http://${service_name}-default;
    }
}
EOF
```

Testing Nginx conf syntax then reload

```sh
nginx -t && nginx -s reload

# nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
# nginx: configuration file /etc/nginx/nginx.conf test is successful
```


## Usage

By default, I disable registration of new users via *SIGNUPS_ALLOWED* in file *.env*. If you register new user directly, it will prompts error

>An error has occurred. Registration not allowed or user already exists.

You need to invite the user email under **User** tab in admin panel (login in needs htpasswd auth and admin token).

## Snapshots

Domain <https://vaultwarden.maxdsre.com>

### Vault page

![vaultwarden vault page](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/vaultwarden/01_vault_page.png)

### Admin panel

<details><summary>Click to expand login snapshot</summary>

Nginx htpasswd auth

![vaultwarden htpasswd auth](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/vaultwarden/02_htpasswd_auth.png)

Admin token

![vaultwarden admin token](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/vaultwarden/03_admin_token.png)

</details>

Tab *Settings*

![vaultwarden tab setting](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/vaultwarden/04-1_admin_panel_setting.png)

Tab *Users*

![vaultwarden tab user](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/vaultwarden/04-2_admin_panel_user.png)

Tab *Organizations*

<details><summary>Click to expand organization snapshot</summary>

![vaultwarden tab organization](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/vaultwarden/04-3_admin_panel_organization.png)

</details>

Tab *Diagnostics*

<details><summary>Click to expand diagnostic snapshot</summary>

![vaultwarden tab diagnostic](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/vaultwarden/04-4_admin_panel_diagnostic.png)

</details>


## Error Occuring

### upstream response is buffered

> [warn] an upstream response is buffered to a temporary file /var/cache/nginx/proxy_temp/3/11/0000000113 while reading upstream

Solution

```conf
# https://serverfault.com/questions/587386/an-upstream-response-is-buffered-to-a-temporary-file
# position location / {}

proxy_buffers 32 64k;
proxy_buffer_size 64k;
```

### WebSocket Protocol Error

Web interface prompts error info

>WebSocket Protocol Error: Unable to parse WebSocket key.

Log file prompts error info

```txt
[parity_ws::handler][ERROR] WS Error <Protocol>: Unable to parse WebSocket key.
```

Same issue

* [Need explaining websocket and push notifications](https://bitwardenrs.discourse.group/t/need-explaining-websocket-and-push-notifications/87)


## Bibliography

* [Bitwarden: How to setup a self-hosted password manager](https://pieterhollander.nl/post/bitwarden/)


## Change Log

* Mar 04, 2021 12:29 Thu ET
  * First draft
* Mar 05, 2021 14:03 Fri ET
  * optimize Nginx config about log
* Mar 09, 2021 15:26 Tue ET
  * add file *docker-compose.yml*
* Jul 14, 2021 09:22 Wed ET
  * change project name from *bitwarden_rs* to *vaultwarden*
* Oct 03, 2021 18:08 Sun ET
  * change docker compose from V1 to [V2](https://github.com/docker/compose/releases/tag/v2.0.0)


[bitwarden]: https://bitwarden.com "Bitwarden Open Source Password Manager"
[vaultwarden]: https://github.com/dani-garcia/vaultwarden "About Unofficial Bitwarden compatible server written in Rust, formerly known as bitwarden_rs"
[docker]: https://www.docker.com
[docker_compose]:https://docs.docker.com/compose/


<!-- End -->