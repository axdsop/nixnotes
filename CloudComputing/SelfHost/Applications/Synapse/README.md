# Self-hosting Synapse (Matrix)

Matrix is an ambitious new ecosystem for open federated Instant Messaging and VoIP.


## TOC

1. [Official Site](#official-site)  
2. [Deployment](#deployment)  
2.1 [Variables Definition](#variables-definition)  
2.2 [env](#env)  
2.3 [Generating Configs](#generating-configs)  
2.3.1 [homeserver.yaml](#homeserveryaml)  
2.3.2 [turnserver.conf](#turnserverconf)  
2.4 [Firewall Setting](#firewall-setting)  
2.5 [docker-compose.yml](#docker-composeyml)  
2.6 [Running Container](#running-container)  
2.7 [Generating admin user](#generating-admin-user)  
2.8 [Nginx Reverse Proxy](#nginx-reverse-proxy)  
2.8.1 [Conf](#conf)  
3. [Database Operating](#database-operating)
3.1 [Major Version Upgrade](#major-version-upgrade)
4. [Error Occuring](#error-occuring)
5. [Bibliography](#bibliography)
6. [Change Log](#change-log)


## Official Site

Item|Url
---|---
Official Site|https://matrix.org
Official Doc|https://matrix.org/docs/<br/>https://matrix-org.github.io/synapse/latest/
GitHub|https://github.com/matrix-org/synapse
Docker Hub|https://hub.docker.com/r/matrixdotorg/synapse

Supported clients https://matrix.org/clients/

To enable video call, The synapse Matrix homeserver needs to a TURN server

Item|Url
---|---
GitHub|https://github.com/coturn/coturn
Docker Hub|https://hub.docker.com/r/coturn/coturn


## Deployment

Official installation page [Installing Synapse](https://matrix.org/docs/guides/installing-synapse).


### Variables Definition

```sh
service_name='synapse'
service_save_path="$HOME/.docker/${service_name}"
mkdir -p "${service_save_path}"

data_dir="/data/"${service_name}""
config_save_dir="${data_dir}/config"

localhost_ip='127.0.0.1'
domain_for_server="${service_name%%-*}.maxdsre.com"
exposted_http_port='8048'  # default 8008
# EXPOSE 8008/tcp 8009/tcp 8448/tcp

EXPOSED_PORT=${localhost_ip}:${exposted_http_port}

domain_for_turn="turn.${domain_for_server#*.}"  # turn.maxdsre.com

POSTGRES_DB="${service_name}"
POSTGRES_USER='synapse_user'   # "${service_name}"
POSTGRES_PASSWORD=$(openssl rand -hex 64 | head -c 45)

# turn server
TURN_SHARED_SECRET=$(openssl rand -hex 64 | head -c 80)
```

### env

File `.env`

<details><summary>Click to expand env conf</summary>

```sh
cd "${service_save_path}"

tee "${service_save_path}"/.env 1> /dev/null << EOF
# https://github.com/iv-org/invidious/blob/master/config/config.example.yml
# https://docs.invidious.io/configuration/

EXPOSED_PORT=${EXPOSED_PORT}
PROXY_DOMAIN=${domain_for_server}

# - Database
#/var/lib/postgresql/data  PostgreSQL Data

#POSTGRES_HOST  # Hostname of the database server using postgres.
POSTGRES_DB=${POSTGRES_DB}
POSTGRES_USER=${POSTGRES_USER}
POSTGRES_PASSWORD=${POSTGRES_PASSWORD}

# - TURN Server
TURN_SHARED_SECRET=${TURN_SHARED_SECRET}

# - Optional

# End
EOF
```

</details>

### Generating Configs

#### homeserver.yaml

Official doc [Generating a configuration file](https://github.com/matrix-org/synapse/blob/develop/docker/README.md#generating-a-configuration-file)

Save generaged configs to directory `${service_save_path}/configs`.

```sh
# default UID, GID is 991, 991.

# docker run -it --rm \
#     -e SYNAPSE_SERVER_NAME="${domain_for_server}" \
#     -e SYNAPSE_REPORT_STATS=yes \
#     -e SYNAPSE_HTTP_PORT=8008 \
#     -e SYNAPSE_CONFIG_DIR=/data \
#     -v "${service_save_path}/configs":/data \
#     matrixdotorg/synapse:latest generate

docker run -it --rm \
    -e SYNAPSE_SERVER_NAME="${domain_for_server}" \
    -e SYNAPSE_REPORT_STATS=no \
    -e SYNAPSE_HTTP_PORT=8008 \
    -e SYNAPSE_CONFIG_DIR=/data \
    -v "${data_dir}/files":/data \
    matrixdotorg/synapse:latest generate

# path ${data_dir}/files/homeserver.yaml
```

Output

```txt
Setting ownership on /data to 991:991
Creating log config /data/synapse.maxdsre.com.log.config
Generating config file /data/homeserver.yaml
Generating signing key file /data/synapse.maxdsre.com.signing.key
A config file has been generated in '/data/homeserver.yaml' for server name 'synapse.maxdsre.com'. Please review this file and customise it to your needs.
```

Copy a generated config file to dir `${data_dir}`, and mount it via *volumes* detective in *docker-compose.yml*.

```sh
cd "${service_save_path}"
mkdir -pv configs
sudo cp -p "${data_dir}"/files/homeserver.yaml configs/
sudo chown -R $(id -u):$(id -g) configs/
cp -p configs/homeserver.yaml{,.init}
```

Change configs in *homeserver.yaml*

```yml
#database:
#   name: sqlite3
#   args:
#     database: /data/homeserver.db

database:
  name: psycopg2
  txn_limit: 10000
  args:
    user: ${POSTGRES_USER}
    password: ${POSTGRES_PASSWORD}
    database: ${POSTGRES_DB}
    host: synapse_db  # same to service name defined in docker-compose.yml
    port: 5432
    cp_min: 5
    cp_max: 10


# registration
enable_registration: true
enable_registration_without_verification: true

report_stats: false
```

Execute the following commands

```sh
# remove existed database configs
sed -r -i '/^database:$/,/^[[:space:]]+database:.*?homeserver/d' configs/homeserver.yaml

tee -a configs/homeserver.yaml 1> /dev/null << EOF

database:
  name: psycopg2
  txn_limit: 10000
  args:
    user: ${POSTGRES_USER}
    password: ${POSTGRES_PASSWORD}
    database: ${POSTGRES_DB}
    host: synapse_db  # same to service name defined in docker-compose.yml
    port: 5432
    cp_min: 5
    cp_max: 10

# registration
enable_registration: true
enable_registration_without_verification: true

report_stats: false
EOF
```

> You have enabled open registration without any verification. This is a known vector for spam and abuse. If you would like to allow public registration, please consider adding email, captcha, or token-based verification. Otherwise this check can be removed by setting the `enable_registration_without_verification` config option to `true`.


#### turnserver.conf

```sh
cd "${service_save_path}"

curl -fsSL -R -o 'configs/turnserver.conf' https://raw.githubusercontent.com/coturn/coturn/master/examples/etc/turnserver.conf

cp -p configs/turnserver.conf{,.init}
```

Change configs in *turnserver.conf*

```conf
# https://matrix-org.github.io/synapse/latest/setup/turn/coturn.html
# turnserver.conf

fingerprint

use-auth-secret    # lt-cred-mech and use-auth-secret can't use in the same time.
static-auth-secret=TURN_SHARED_SECRET
realm=turn.maxdsre.com
#no-tcp-relay

# consider whether you want to limit the quota of relayed streams per user (or total) to avoid risk of DoS.
user-quota=12  # 4 streams per video call, so 12 streams = 3 simultaneous relayed calls per user.
total-quota=1200
listening-ip=0.0.0.0
listening-port=3478
#no-tcp-relay  # VoIP traffic is all UDP. There is no reason to let users connect to arbitrary TCP endpoints via the relay.

# don't let the relay ever try to connect to private IP address ranges within your network (if any). given the turn server is likely behind your firewall, remember to include any privileged public IPs too.
denied-peer-ip=10.0.0.0-10.255.255.255
denied-peer-ip=192.168.0.0-192.168.255.255
denied-peer-ip=172.16.0.0-172.31.255.255

# https://github.com/coturn/coturn/issues/361
# docker run -ti --rm coturn/coturn:alpine turnadmin -P -p 'eSurBfQpY&RuwqWPK@1P#2ifuhURQJO_o#braqn?3V'
# password 'eSurBfQpY&RuwqWPK@1P#2ifuhURQJO_o#braqn?3V'
cli-password=$5$7502d979bf9e554c$5b28d6c3ce3a8080a0d9a3ac6d74045f0c7a04defbaabbd10a209b9484b71d29

no-software-attribute

no-rfc5780
no-stun-backward-compatibility
response-origin-only-with-rfc5780
```

Synapse homeserver.yml

```yml
# add the following configs

# https://matrix-org.github.io/synapse/latest/turn-howto.html#synapse-setup
# turn server turn.maxdsre.com
turn_uris: [ "turn:turn.maxdsre.com?transport=udp", "turn:turn.maxdsre.com?transport=tcp" ]
turn_shared_secret: "TURN_SHARED_SECRET"
turn_user_lifetime: 86400000
turn_allow_guests: false
```

Update *TURN_SHARED_SECRET* value, make they are identical

```sh
cd "${service_save_path}"

sed -r -i '/^static-auth-secret=/{s@^([^=]+=).*@\1'"${TURN_SHARED_SECRET}"'@g}' ./configs/turnserver.conf
sed -r -i '/^turn_shared_secret:/{s@^([^:]+:[[:space:]]*).*@\1"'"${TURN_SHARED_SECRET}"'"@g}' ./configs/homeserver.yaml
```

### postgres client libraries ?

By default the config will use SQLite. Doc [Using Postgres](https://github.com/matrix-org/synapse/blob/develop/docs/postgres.md) shows how to use Postgres.

Install package *libpq5* on your host system.

```sh
sudo apt-get install -y libpq5 libpq-dev
```

### docker-compose.yml

Official provides a [example](https://github.com/matrix-org/synapse/blob/develop/contrib/docker/docker-compose.yml).

```sh
cd "${service_save_path}"

tee docker-compose.yml 1> /dev/null << EOF
version: '3.8'

services:
  synapse:
    image: matrixdotorg/synapse:latest
    container_name: synapse-server
    # Since synapse does not retry to connect to the database, restart upon failure
    restart: unless-stopped
    networks:
      - synapse_network
    ports:
      - "\${EXPOSED_PORT}:8008/tcp"
    env_file: .env
    # See the readme for a full documentation of the environment settings
    # NOTE: You must edit homeserver.yaml to use postgres, it defaults to sqlite
    environment:
      - SYNAPSE_CONFIG_PATH=/data/homeserver.yaml
    volumes:
      - ${data_dir}/files:/data
      - ./configs/homeserver.yaml:/data/homeserver.yaml:ro
    healthcheck:
      # https://github.com/matrix-org/synapse/blob/develop/docker/README.md#setting-the-healthcheck-in-docker-compose-file
      # disable: true
      test: curl -fSsL http://${EXPOSED_PORT}/health || exit 1
      interval: 30s
      timeout: 5s
      retries: 3
      start_period: 5s
    depends_on:
      - synapse_db
      - synapse_turn

  synapse_db:
    # https://hub.docker.com/_/postgres
    #image: postgres:14-alpine  # version 14, 20210930 ~ 20261112
    image: postgres:15-alpine  # version 15, 20221013 ~ 20271111
    container_name: synapse-db
    restart: unless-stopped
    networks:
      - synapse_network
    env_file: .env
    environment:
      - POSTGRES_DB=\${POSTGRES_DB}
      - POSTGRES_USER=\${POSTGRES_USER}
      - POSTGRES_PASSWORD=\${POSTGRES_PASSWORD}
      # ensure the database gets created correctly
      # https://matrix-org.github.io/synapse/latest/postgres.html#set-up-database
      - POSTGRES_INITDB_ARGS=--encoding=UTF-8 --lc-collate=C --lc-ctype=C
    volumes:
      - ${data_dir}/db:/var/lib/postgresql/data
      # - /etc/localtime:/etc/localtime:ro
    healthcheck:
      test: ["CMD-SHELL", "pg_isready -U \$\$POSTGRES_USER -d \$\$POSTGRES_DB"]

  synapse_turn:
    # turn server is used as a VoIP media traffic NAT  traversal server and gateway.
    # https://github.com/coturn/coturn/tree/master/docker
    image: coturn/coturn:alpine  # tag 'latest' is debian based
    container_name: synapse-turn
    restart: unless-stopped
    networks:
      - synapse_network
    env_file: .env
    environment:
      LOG_FILE: stdout
      PORT: 3478
      #ALT_PORT: 3479
      TLS_PORT: 5349
      #TLS_ALT_PORT: 5350
      JSON_CONFIG: '{"config":["no-auth"]}'
    volumes:
      # https://github.com/coturn/coturn/blob/master/examples/etc/turnserver.conf
      - ./configs/turnserver.conf:/etc/turnserver.conf:ro
      #- ${data_dir}/coturn:/var/lib/coturn
      #- ./configs/coturn_privkey.pem:/etc/ssl/private/privkey.pem:ro
      #- ./configs/coturn_cert.pem:/etc/ssl/certs/cert.pem:ro
    ports:
      ##STUN/TURN
      - 3478:3478
      - 3478:3478/udp
      #- 3479:3479
      #- 3479:3479/udp
      ##STUN/TURN SSL
      #- 5349:5349
      #- 5349:5349/udp
      #- 5350:5350
      #- 5350:5350/udp
      ## Relay Ports
      #- 49160-49200:49160-49200
      #- 49160-49200:49160-49200/udp

networks:
  synapse_network:
    name: synapse_network

# End
EOF
```


### Running Container

```sh
cd "${service_save_path}"

# - Via docker compose (For docker-compose V2)
docker compose up -d
# docker compose --env-file "${service_save_path}/docker-compose.yml" up -d
docker compose ps
docker compose logs
docker compose down --remove-orphans

# - Via docker-compose (For docker-compose V1)
docker-compose up -d
# docker-compose -f "${service_save_path}/docker-compose.yml" up -d
docker-compose ps
# docker-compose down --remove-orphans
```

### Generating admin user

>After synapse is running, you may wish to create a user via `register_new_matrix_user`.
>
>This requires a `registration_shared_secret` to be set in your config file. Synapse must be restarted to pick up this change.

You can then call the script:

```sh
docker exec -it synapse register_new_matrix_user http://localhost:8008 -c /data/homeserver.yaml --help
```

Attention: here *http://localhost:8008* is the port in container, not the port *8048* in host system. Other it'll prompts error info

>requests.exceptions.ConnectionError: HTTPConnectionPool(host='localhost', port=8048): Max retries exceeded with url: /_synapse/admin/v1/register (Caused by NewConnectionError('<urllib3.connection.HTTPConnection object at 0x7f0a70f129d0>: Failed to establish a new connection: [Errno 111] Connection refused'))

<deta<details><summary>Click to expand help info</summary>
ils><summary>Click to expand help info</summary>

```txt
usage: register_new_matrix_user [-h] [-u USER] [-p PASSWORD] [-t USER_TYPE] [-a | --no-admin]
                                (-c CONFIG | -k SHARED_SECRET)
                                [server_url]

Used to register new users with a given homeserver when registration has been disabled. The homeserver must be
configured with the 'registration_shared_secret' option set.

positional arguments:
  server_url            URL to use to talk to the homeserver. Defaults to 'https://localhost:8448'.

optional arguments:
  -h, --help            show this help message and exit
  -u USER, --user USER  Local part of the new user. Will prompt if omitted.
  -p PASSWORD, --password PASSWORD
                        New password for user. Will prompt if omitted.
  -t USER_TYPE, --user_type USER_TYPE
                        User type as specified in synapse.api.constants.UserTypes
  -a, --admin           Register new user as an admin. Will prompt if --no-admin is not set either.
  --no-admin            Register new user as a regular user. Will prompt if --admin is not set either.
  -c CONFIG, --config CONFIG
                        Path to server config file. Used to read in shared secret.
  -k SHARED_SECRET, --shared-secret SHARED_SECRET
                        Shared secret as defined in server config file.
```

</details>


Type|User|Pass
---|---|---
admin|admin|
regular|test1|


```sh
cd "${service_save_path}"

# get container ID
container_id=$(docker ps --filter name="-synapse-" --format "{{.ID}}")  # name synapse-synapse-1

# registration_shared_secret defined in homeserver.yaml
# error: argument -k/--shared-secret: not allowed with argument -c/--config

# create admin user
docker exec -it ${container_id} register_new_matrix_user http://localhost:8008 -c /data/homeserver.yaml --admin -u admin

# create normal user
docker exec -it ${container_id} register_new_matrix_user http://localhost:8008 -c /data/homeserver.yaml --no-admin -u test1

```

### Firewall Setting

>Ensure your firewall allows traffic into the TURN server on the ports you've configured it to listen on (By default: 3478 and 5349 for TURN traffic (remember to allow both TCP and UDP traffic), and ports 49152-65535 for the UDP relay.)

```sh
# For Debian/Ubuntu
# allow port 3478/TCP, 3478/UDP, 49152-65535/UDP

# sudo ufw allow 3478/tcp comment 'matrix turn plain tcp'
sudo ufw allow 3478/udp comment 'matrix turn plain udp'
#sudo ufw allow 5349/tcp comment 'matrix turn tls tcp'
#sudo ufw allow 5349/udp comment 'matrix turn tls udp'
sudo ufw allow 49152:65535/udp comment 'matrix turn udp relay'
```

### Nginx Reverse Proxy

Nginx

```sh
service_name='synapse'
domain_for_server="${service_name%%-*}.maxdsre.com"
domain_for_turn="turn.${domain_for_server#*.}"
exposted_http_port='8048'  # default 8008

backend_private_ip=${backend_private_ip:-'127.0.0.1'}
# backend_private_ip='192.168.8.4'

# Nginx site config dir /etc/nginx/sites-enabled/
nginx_conf_path="/etc/nginx/sites-enabled/${service_name}.conf"
sudo mkdir -p "${nginx_conf_path%/*}"
```

#### Conf

Nginc config sample [Using a reverse proxy with Synapse](https://matrix-org.github.io/synapse/latest/reverse_proxy.html)

<details><summary>Click to expand config info</summary>

```conf
sudo tee "${nginx_conf_path}" 1> /dev/null << EOF
upstream ${service_name}-default {
    server ${backend_private_ip}:${exposted_http_port};
}

upstream turn-default {
    server 127.0.0.1:3478;
}

map \$http_upgrade \$connection_upgrade {
    default upgrade;
    '' close;
}

# - For Turn Server
# Redirect HTTP to HTTPS
server {
    listen 80;
    # listen [::]:80 ipv6only=on;
    server_name ${domain_for_turn};
    return 301 https://\$host\$request_uri;
}

server {
    listen 443 ssl http2;
    # listen [::]:443 ssl http2 ipv6only=on;
    server_name ${domain_for_turn};

    #error_log  /var/log/nginx/${domain_for_server%%.*}_error.log warn;  # [debug|info|notice|warn|error|crit|alert|emerg]
    #access_log /var/log/nginx/${domain_for_server%%.*}_access.log main; # format name 'main' instead of predefined 'combined' format

    include /etc/nginx/conf.d/section/ssl.conf;

    location / {
        #proxy_set_header Host \$host;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto \$scheme;

        # http://nginx.org/en/docs/http/ngx_http_proxy_module.html#proxy_http_version
        proxy_http_version 1.1;
        proxy_set_header Upgrade \$http_upgrade;
        proxy_set_header Connection \$connection_upgrade;
        proxy_set_header Host \$host;
        proxy_set_header Accept-Encoding gzip;

        # https://serverfault.com/questions/587386/an-upstream-response-is-buffered-to-a-temporary-file
        # https://www.cyberciti.biz/faq/nginx-upstream-sent-too-big-header-while-reading-response-header-from-upstream/
        proxy_buffering off;
        #proxy_buffers   4 512k;
        #proxy_buffer_size   256k;
        #proxy_busy_buffers_size   512k;

        proxy_pass http://turn-default;
    }
}


# - For Synapse Server
# Redirect HTTP to HTTPS
server {
    listen 80;
    # listen [::]:80 ipv6only=on;
    server_name ${domain_for_server};
    return 301 https://\$host\$request_uri;
}

server {
    listen 443 ssl http2;
    # listen [::]:443 ssl http2 ipv6only=on;
    server_name ${domain_for_server};

    error_log  /var/log/nginx/${domain_for_server%%.*}_error.log warn;  # [debug|info|notice|warn|error|crit|alert|emerg]
    access_log /var/log/nginx/${domain_for_server%%.*}_access.log main; # format name 'main' instead of predefined 'combined' format

    include /etc/nginx/conf.d/section/ssl.conf;

    location ~ ^(/_matrix|/_synapse/client) {
        #proxy_set_header Host \$host;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto \$scheme;

        # http://nginx.org/en/docs/http/ngx_http_proxy_module.html#proxy_http_version
        proxy_http_version 1.1;
        proxy_set_header Upgrade \$http_upgrade;
        proxy_set_header Connection \$connection_upgrade;
        proxy_set_header Host \$host;
        proxy_set_header Accept-Encoding gzip;

        # https://serverfault.com/questions/587386/an-upstream-response-is-buffered-to-a-temporary-file
        # https://www.cyberciti.biz/faq/nginx-upstream-sent-too-big-header-while-reading-response-header-from-upstream/
        proxy_buffering off;
        #proxy_buffers   4 512k;
        #proxy_buffer_size   256k;
        #proxy_busy_buffers_size   512k;

        # Nginx by default only allows file uploads up to 1M in size
        # Increase client_max_body_size to match max_upload_size defined in homeserver.yaml
        client_max_body_size 50M;

        #add_header Content-Security-Policy "default-src 'none'; child-src 'self'; font-src 'self' data:; connect-src 'self' wss: ws:; media-src 'self' blob: data:; manifest-src 'self'; base-uri 'none'; form-action 'self'; frame-src 'self'; frame-ancestors 'self'; object-src 'none'; worker-src 'self' blob:; script-src 'self'; img-src data: https: http: ; style-src 'unsafe-inline' 'self'" always;

        proxy_pass http://${service_name}-default;
    }
}

# End
EOF
```

</details>

Testing Nginx conf syntax then reload

```sh
sudo -i

nginx -t && nginx -s reload

# nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
# nginx: configuration file /etc/nginx/nginx.conf test is successful
```

## Database Operating

Connect service `synapse_db` to delete data from database

```sh
cd "${service_save_path}"

docker compose exec synapse_db sh

psql -U synapse synapse

# list database list
\l

# connect database 'synapse'
\c synapse
# You are now connected to database "synapse" as user "synapse".

# list tables
\d

# show table info
\d+ access_tokens 

# select action
select * from access_tokens;

#  id | action |  occurred  |     ip     |    subnet     |     metadata
# ----+--------+------------+------------+---------------+------------------
#   1 | login  | 1636403177 | 172.29.0.1 | 172.29.0.1/32 | {"user":"admin"}

select id,ip from oc_bruteforce_attempts;

#  id |     ip
# ----+------------
#   1 | 172.29.0.1

# delete action
delete from access_tokens where id=5;

# change password
\password

# exit
exit
```

### Major Version Upgrade

Lifetime support https://www.postgresql.org/support/versioning/

Version|First Release|Final Release
---|---|---
17 | Sep 26, 2024 | Nov 8, 2029
16 | Sep 14, 2023 | Nov 9, 2028
15 | Oct 13, 2022 | Nov 11, 2027
14 | Sep 30, 2021 | Nov 12, 2026
13 | Sep 24, 2020 | Nov 13, 2025


```sh
# source: https://gabnotes.org/upgrade-postgres-docker/

container_name="synapse_db"
db_user="synapse"
db_name="synapse"

# - Run database container
docker compose down --remove-orphans
docker compose up -d $container_name
# docker compose logs -ft

# - Dump DB & roles
# docker compose exec -it $container_name pg_dumpall -U $db_user > ./db_export/data_backup.sql
docker compose exec -it $container_name \
  pg_dump -Fc -U $db_user $db_name > ./db_export/backup.dump
docker compose exec -it $container_name \
  pg_dumpall --globals-only -U $db_user > ./db_export/roles.sql

# - Shutdown all containers
docker compose down --remove-orphans

# - Move current data dir, don't remove it yet (/data/synapse/db)
cp -p -R /data/synapse/db{,_old}

# - Edit docker-compose.yaml - upgrade psql
vim docker-compose.yaml
# image: postgres:15-alpine 
# /data/synapse/db_15:/var/lib/postgresql/data

# - Check version
docker compose up -d $container_name
docker compose exec -it $container_name psql -U $db_user -c 'select version();'

# - Restore data
docker compose cp ./db_export/roles.sql $container_name:/tmp/roles
docker compose exec -it $container_name psql -U $db_user -d $db_name -f /tmp/roles

docker compose cp ./db_export/backup.dump $container_name:/tmp/backup
docker compose exec -it $container_name pg_restore -U $db_user -d $db_name /tmp/backup

# docker compose cp ./db_export/data_backup.sql $container_name:/tmp/backup_data
# docker compose exec -it $container_name pg_restore -U $db_user -d $db_name /tmp/backup_data


# - Change password (Must) Re-input the old password
# psycopg2.OperationalError: connection to server at "synapse_db" (172.18.0.2), port 5432 failed: FATAL:  password authentication failed for user "synapse"
# https://stackoverflow.com/questions/55038942/fatal-password-authentication-failed-for-user-postgres-postgresql-11-with-pg#69664010
docker compose exec $container_name sh
psql -U synapse synapse   # $db_user $db_name 
\password

# - Verify
docker compose exec -it $container_name psql -U $db_user -d $db_name -c "\dt"

# Launch app again
docker compose up -d
docker compose logs -ft

# - Shutdown all containers
docker compose down --remove-orphans
```


## Error Occuring

>synapse-turn  | CONFIG ERROR: Empty cli-password, and so telnet cli interface is disabled! Please set a non empty cli-password!


## Bibliography

* [GitHub - spantaleev/matrix-docker-ansible-deploy](https://github.com/spantaleev/matrix-docker-ansible-deploy)
* [Self-hosted Discord alternative, Synapse and Element in Docker](https://raddinox.com/self-hosted-discord-alternetive)
* [Matrix Synapse Homeserver](https://comatrix.eu/setup/matrix_synapse_homeserver/)
* [How to Self-Host Matrix and Element (Docker Compose)](https://cyberhost.uk/element-matrix-setup/)
* [Upgrading and containerising Matrix Synapse and PostgreSQL to ensure you have the latest version](https://www.gibiris.org/eo-blog/posts/2022/01/21_containterise-synapse-postgres.html)


## Change Log

* May 26, 2022 14:30 Thu ET
  * First draft


<!-- End -->