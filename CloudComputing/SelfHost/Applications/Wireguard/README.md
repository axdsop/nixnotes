# Self-hosting Wireguard

[Wireguard][wireguard]® is an extremely simple yet fast and modern VPN that utilizes state-of-the-art cryptography.

## TOC

1. [Official Site](#official-site)  
2. [Deployment](#deployment)  
2.1 [Configs](#configs)  
2.1.1 [.env](#env)  
2.1.2 [docker-compose.yml](#docker-composeyml)  
2.2 [Running Container](#running-container)  
3. [Connection](#connection)  
3.1 [Credential](#credential)  
3.1.1 [Via QR codes](#via-qr-codes)  
3.1.2 [Via Peer Config](#via-peer-config)  
3.2 [Statistics](#statistics)  
4. [Issue Occuring](#issue-occuring)  
5. [Bibliography](#bibliography)  
6. [Change Log](#change-log)  


## Official Site

Item|Url
---|---
Official Site|https://www.wireguard.com
Official Doc|https://www.wireguard.com/install/

LinuxServer

Item|Url
---|---
Official site|https://www.linuxserver.io
Official doc|https://docs.linuxserver.io/images/docker-wireguard
GitHub|https://github.com/linuxserver/docker-wireguard
Docker|https://hub.docker.com/r/linuxserver/wireguard

For GNU/Linux

* Arch Linux https://wiki.archlinux.org/title/WireGuard

[Some Unofficial WireGuard Documentation](https://docs.sweeting.me/s/wireguard)


## Deployment

### Configs

Variables Definition

```sh
service_name='wireguard'
service_save_path="$HOME/.docker/${service_name}"
mkdir -p "${service_save_path}"

data_dir="/data/"${service_name}""
config_save_dir="${data_dir}/config"

localhost_ip='127.0.0.1'
domain_for_server="${service_name%%-*}.maxdsre.com"
exposted_http_port='8002'  # default 51820
```

Allow firewall rule

```sh
sudo ufw allow ${exposted_http_port}/udp comment 'wireguard rule'
```

#### .env

File `.env`

<details><summary>Click to expand env conf</summary>

```sh
tee "${service_save_path}"/.env 1> /dev/null << EOF

PROXY_DOMAIN=${domain_for_server}

# - essential
PUID=1000  # id -u
PGID=1000  # id -g
TZ=$(cat /etc/timezone)  # America/New_York from file /etc/timezone

# - optional
SERVERPORT=${exposted_http_port}  # same to host system mapping port, default is 51820
#SERVERURL=${domain_for_server}
INTERNAL_SUBNET=10.16.1.0
# The IPs/Ranges that the peers will be able to reach using the VPN connection. If not specified the default value is: '0.0.0.0/0, ::0/0' This will cause ALL traffic to route through the VPN, if you want split tunneling, set this to only the IPs you would like to use the tunnel AND the ip of the server's WG ip.  # https://www.reddit.com/r/WireGuard/comments/siyqrj/wireguard_docker_container_not_resolving_dns_when/
ALLOWEDIPS='0.0.0.0/0'  # here just allow ipv4,  ::0/0 means ipv6

# Generated QR codes will be displayed in the docker log. Set to false to skip log output.
LOG_CONFS=true

# DNS server set in peer/client configs (can be set as 8.8.8.8). Used in server mode. Defaults to auto, which uses wireguard docker host's DNS via included CoreDNS forward.
# https://kb.adguard.com/en/general/dns-providers
PEERDNS=9.9.9.9,1.1.1.1

# PEERS can be set to a number or a list of strings separated by comma, the container will run in server mode and the necessary server and peer/client confs will be generated.
PEERS=laptop,tablet,mobile,iphone

# End
EOF
```

</details>


#### docker-compose.yml

```sh
cd "${service_save_path}"

tee docker-compose.yml 1> /dev/null << EOF
version: "3.8"

services:
  wireguard:
    image: ghcr.io/linuxserver/wireguard
    container_name: wireguard-server
    restart: unless-stopped
    cap_add:
      - NET_ADMIN
      - SYS_MODULE
    env_file: .env
    environment:
      - PUID=\${PUID}
      - PGID=\${PGID}
      - TZ=\${TZ}
      #- SERVERURL=\${SERVERURL}
      - SERVERPORT=\${SERVERPORT}  # same to host system mapping port ${exposted_http_port}, default is 51820
      - PEERS=\${PEERS}
      - PEERDNS=\${PEERDNS}
      - INTERNAL_SUBNET=\${INTERNAL_SUBNET}
      - ALLOWEDIPS=\${ALLOWEDIPS}
      - LOG_CONFS=\${LOG_CONFS}
    volumes:
      # peers config files path ./config/wireguard-server/peer_*
      - ./config/wireguard-server:/config
      - /lib/modules:/lib/modules:ro
      #- /etc/localtime:/etc/localtime:ro
    ports:
      - ${exposted_http_port}:51820/udp
    # sysctls is required for client mode
    sysctls:
      - net.ipv4.conf.all.src_valid_mark=1

EOF
```

### Running Container

```sh
cd "${service_save_path}"

# - Via docker compose (For docker-compose V2)
docker compose up -d
# docker compose --env-file "${service_save_path}/docker-compose.yml" up -d
docker compose ps
docker compose down --remove-orphans

# - Via docker-compose (For docker-compose V1)
docker-compose up -d
# docker-compose -f "${service_save_path}/docker-compose.yml" up -d
docker-compose ps
# docker-compose down --remove-orphans
```

Save log

```sh
docker compose logs > config/qrcode_log
```

Initialization logs show network interface name is `wg0`

<details><summary>Click to expand output</summary>

```sh
docker compose logs
```


Output

```txt
...
wireguard-server  | **** Server mode is selected ****
wireguard-server  | **** SERVERURL var is either not set or is set to "auto", setting external IP to auto detected value of 204.13.153.189 ****
wireguard-server  | **** External server port is set to 51820. Make sure that port is properly forwarded to port 51820 inside this container ****
wireguard-server  | **** Internal subnet is set to 10.16.1.0 ****
wireguard-server  | **** AllowedIPs for peers 0.0.0.0/0 ****
wireguard-server  | **** Peer DNS servers will be set to 9.9.9.9 ****
wireguard-server  | **** No wg0.conf found (maybe an initial install), generating 1 server and laptop,tablet,mobile,iphone peer/client confs ****
...
...
wireguard-server  | [#] ip link add wg0 type wireguard
wireguard-server  | [#] wg setconf wg0 /dev/fd/63
wireguard-server  | [#] ip -4 address add 10.16.1.1 dev wg0
wireguard-server  | [#] ip link set mtu 1420 up dev wg0
wireguard-server  | [#] ip -4 route add 10.16.1.5/32 dev wg0
wireguard-server  | [#] ip -4 route add 10.16.1.4/32 dev wg0
wireguard-server  | [#] ip -4 route add 10.16.1.3/32 dev wg0
wireguard-server  | [#] ip -4 route add 10.16.1.2/32 dev wg0
wireguard-server  | [#] iptables -A FORWARD -i wg0 -j ACCEPT; iptables -A FORWARD -o wg0 -j ACCEPT; iptables -t nat -A POSTROUTING -o eth+ -j MASQUERADE
wireguard-server  | .:53
wireguard-server  | CoreDNS-1.9.3
```

</details>

To show config details

```sh
# Usage: wg showconf <interface>

# here interface name 'wg0' shows in logs
docker compose exec wireguard wg showconf wg0
```

<details><summary>Click to expand config</summary>

```txt
[Interface]
ListenPort = 51820
PrivateKey = iBLOMu9oTH3DGwl6K2Vot4izTc++VDqfXgOR7sFP0E0=

[Peer]
...

[Peer]
...

# this one is peer 'mobile'
[Peer]
PublicKey = cU0AUvo8hPXlT2Rk6oZ7nXhDXjZnYMdj+3d03N4frwI=
PresharedKey = jwjqhvx0n4PcLRhmP03RoTET6PcyXR6RQvDZ8+W8tRw=
AllowedIPs = 10.16.1.4/32
Endpoint = xx.xx.xx.xx:51820

```

</details>


## Connection

For GNU/Linux, you can follow blog [How to Install WireGuard VPN Client on Ubuntu Linux](https://serverspace.us/support/help/how-to-install-wireguard-vpn-client-on-ubuntu-linux/).

### Credential

#### Via QR codes

If variable *LOG_CONFS* is *true* (default) spcified in file *docker-compose.yml*, generated QR codes will be displayed in the docker log.

```sh
# keep log
docker compose logs
```

You can also manually generate it

```sh
# specify peer name, here is 'mobile' defined in "PEERS=laptop,tablet,mobile,iphone"
peer_name='mobile'
docker compose exec wireguard /app/show-peer "${peer_name}"
```

Just scan via phone camera.

#### Via Peer Config

Peer config files locate at `config/wireguard-server/peer_*/`

For Arch Linux, copy file `config/wireguard-server/peer_laptop/peer_laptop.conf` to your local system, save path is `/etc/wireguard/wg0.conf`.

```sh
# /etc/wireguard/wg0.conf   wg0 is network interface

# - via wg-quick
sudo wg-quick up wg0  # manually up
sudo wg-quick down wg0  # manually down

# - via systemd service
systemctl status wg-quick@wg0  # check service

sudo systemctl start wg-quick@wg0  # service up
sudo systemctl stop wg-quick@wg0   # service down
```

Show connection statistics

```sh
sudo wg show
```

### Statistics

To check the peer connection statistics from container

```sh
docker compose exec wireguard wg
```

<details><summary>Click to expand statistics</summary>

```txt
interface: wg0
  public key: lxOlrRFFEcj3xguWBjDmgm3dC23kajvB0zcfTXxQsQc=
  private key: (hidden)
  listening port: 51820

# this one is peer 'mobile'
peer: cU0AUvo8hPXlT2Rk6oZ7nXhDXjZnYMdj+3d03N4frwI=
  preshared key: (hidden)
  endpoint: xx.xx.xx.xx:51820
  allowed ips: 10.16.1.4/32
  latest handshake: 27 seconds ago
  transfer: 182.65 KiB received, 2.95 MiB sent

...
...
```

</details>


## Issue Occuring

After connect to WireGuard VPN, fail to ssh remote server.


## Bibliography

* [Install Wireguard VPN server with Docker](https://markontech.com/linux/install-wireguard-vpn-server-with-docker/)
* [HOME VPN USING WIREGUARD DOCKER ON A RASPBERRY PI 4](https://www.addictedtotech.net/home-vpn-using-wireguard-docker-on-a-raspberry-pi-4/)

Official blog

* [Routing Docker Host And Container Traffic Through WireGuard](https://www.linuxserver.io/blog/routing-docker-host-and-container-traffic-through-wireguard)


## Change Log

* Jun 13, 2022 14:18 Mon ET
  * First draft


[wireguard]:https://www.wireguard.com "WireGuard: fast, modern, secure VPN tunnel"

<!-- End -->