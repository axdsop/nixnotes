# Wireguard Usage On GNU/Linux


QR codes

If variable *LOG_CONFS* is *true* (default) spcified in file *docker-compose.yml*, generated QR codes will be displayed in the docker log.

```sh
# keep log
docker compose logs > qrcode_log
```

You can also manually generate it

```sh
# specify peer name, here is 'mobile' defined in "PEERS=laptop,tablet,mobile,iphone"
docker compose exec wireguard /app/show-peer mobile
```

To show configuration

```sh
# Usage: wg showconf <interface>

# here interface name 'wg0' shows in logs
docker compose exec wireguard wg showconf wg0
```

<details><summary>Click to expand configuration</summary>

```txt
[Interface]
ListenPort = 51820
PrivateKey = iBLOMu9oTH3DGwl6K2Vot4izTc++VDqfXgOR7sFP0E0=

[Peer]
...

[Peer]
...

# this one is peer 'mobile'
[Peer]
PublicKey = cU0AUvo8hPXlT2Rk6oZ7nXhDXjZnYMdj+3d03N4frwI=
PresharedKey = jwjqhvx0n4PcLRhmP03RoTET6PcyXR6RQvDZ8+W8tRw=
AllowedIPs = 10.16.1.4/32
Endpoint = xx.xx.xx.xx:51820

```

</details>

To check the peer connection statistics

```sh
docker compose exec wireguard wg
```

<details><summary>Click to expand statistics</summary>

```txt
interface: wg0
  public key: lxOlrRFFEcj3xguWBjDmgm3dC23kajvB0zcfTXxQsQc=
  private key: (hidden)
  listening port: 51820

# this one is peer 'mobile'
peer: cU0AUvo8hPXlT2Rk6oZ7nXhDXjZnYMdj+3d03N4frwI=
  preshared key: (hidden)
  endpoint: xx.xx.xx.xx:51820
  allowed ips: 10.16.1.4/32
  latest handshake: 27 seconds ago
  transfer: 182.65 KiB received, 2.95 MiB sent

...
...
```

</details>


For GNU/Linux, please read official page <https://www.wireguard.com/install/>


```sh
/etc/wireguard/wg0.conf

$sudo wg-quick up wg0
[#] ip link add wg0 type wireguard
[#] wg setconf wg0 /dev/fd/63
[#] ip -4 address add 10.16.1.2 dev wg0
[#] ip link set mtu 1420 up dev wg0
[#] resolvconf -a wg0 -m 0 -x
[#] wg set wg0 fwmark 51820
[#] ip -4 route add 0.0.0.0/0 dev wg0 table 51820
[#] ip -4 rule add not fwmark 51820 table 51820
[#] ip -4 rule add table main suppress_prefixlength 0
[#] sysctl -q net.ipv4.conf.all.src_valid_mark=1
[#] iptables-restore -n

sudo wg show

$sudo wg-quick down wg0
[#] ip -4 rule delete table 51820
[#] ip -4 rule delete table main suppress_prefixlength 0
[#] ip link delete dev wg0
[#] resolvconf -d wg0 -f
[#] iptables-restore -n
```

```sh
systemctl status wg-quick@wg0

sudo systemctl start wg-quick@wg0
sudo systemctl stop wg-quick@wg0
```


<!-- [wireguard]:https://www.wireguard.com "WireGuard: fast, modern, secure VPN tunnel" -->

<!-- End -->