#!/usr/bin/env bash

# Date: Jan 11, 2022 Wed 10:35 ET
# Generate Self-host Wireguard Configs Archive (.zip)


# $docker stats --no-stream --format "table {{.Name}}\t{{.CPUPerc}}\t{{.MemUsage}}"
# NAME                       CPU %     MEM USAGE / LIMIT
# wireguard-server           0.20%     17.39MiB / 128MiB
# wireguard-adguard          0.03%     110.2MiB / 512MiB
# wireguard-unbound          0.00%     27.97MiB / 128MiB


host_name=$(sed '/^#/d; /^$/d' /etc/hostname | cut -d- --output-delimiter='_' -f 1,3)
# greencloud-web-sanjose --> greencloud_sanjose
file_name="wg_configs_${host_name}_$(date +'%Y%m%d')"  # %Y%m%d_%H%M

# script absolute path
script_save_path=$(dirname "$(readlink -f -- "$0")")

if [[ ! -f "${script_save_path}/config/wireguard-server/wg0.conf" ]]; then
    echo "No config file 'wireguard-server/wg0.conf' finds."
    
    if [[ -f "${script_save_path}/docker-compose.yml" ]]; then
        cd "${script_save_path}" || exit
        echo "Try to start docker container ..."
        docker compose down --remove-orphans; docker compose pull; docker compose up -d
        sleep 2

        until [[ -f "${script_save_path}/config/wireguard-server/wg0.conf" ]]; do
            sleep 1
        done     
    fi
fi

if [[ ! -d "${script_save_path}/config/wireguard-server" ]]; then
    echo -e "Sorry, fail to find dir 'wireguard-server' in directory ${script_save_path}."
    exit 0
else
    cd /tmp || exit

    cp -p -R "${script_save_path}/config/wireguard-server" "${file_name}"
    [[ -f  "${file_name}.zip" ]] && rm -f "${file_name}.zip"
    zip -q -r "${file_name}.zip" "${file_name}"

    [[ -d "/tmp/${file_name}" ]] && rm -r "/tmp/${file_name}"
    echo -e "\nGenerated config archive path: /tmp/${file_name}.zip\n"
    stat "/tmp/${file_name}.zip"
fi

# Script End