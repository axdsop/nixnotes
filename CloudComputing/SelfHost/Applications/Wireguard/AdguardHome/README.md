# Self-hosting Wireguard With AdGuard Home


## Official Site

AdGuard Home

Item|Url
---|---
Official site|https://adguard.com/en/adguard-home/
GitHub|https://github.com/AdguardTeam/AdGuardHome
GitHub Wiki|https://github.com/AdguardTeam/AdGuardHome/wiki
Docker|https://hub.docker.com/r/adguard/adguardhome

<!-- https://hub.docker.com/r/adguard/adguardhome/dockerfile -->

Wierguard (LinuxServer)

Item|Url
---|---
Official site|https://www.linuxserver.io
Official doc|https://docs.linuxserver.io/images/docker-wireguard
GitHub|https://github.com/linuxserver/docker-wireguard
Docker|https://hub.docker.com/r/linuxserver/wireguard


## Deployment

https://github.com/fnazz/docker-adguard-unbound-wireguard

### Configs

Variables Definition

```sh
service_name='wireguard-adguard'
service_save_path="$HOME/.docker/${service_name}"
mkdir -p "${service_save_path}"

data_dir="/data/"${service_name}""
config_save_dir="${data_dir}/config"

localhost_ip='127.0.0.1'
domain_for_server="${service_name%%-*}.maxdsre.com"
exposted_http_port='8002'  # default 51820
```

Allow firewall rule

```sh
sudo ufw allow ${exposted_http_port}/udp comment 'wireguard rule'
```

#### .env

File `.env`

<details><summary>Click to expand env conf</summary>

```sh
tee "${service_save_path}"/.env 1> /dev/null << EOF

PROXY_DOMAIN=${domain_for_server}

# - essential
PUID=1000  # id -u
PGID=1000  # id -g
TZ=$(cat /etc/timezone)  # America/New_York from file /etc/timezone

# - optional
SERVERPORT=${exposted_http_port}  # same to host system mapping port, default is 51820
#SERVERURL=${domain_for_server}
INTERNAL_SUBNET=10.16.1.0
# The IPs/Ranges that the peers will be able to reach using the VPN connection. If not specified the default value is: '0.0.0.0/0, ::0/0' This will cause ALL traffic to route through the VPN, if you want split tunneling, set this to only the IPs you would like to use the tunnel AND the ip of the server's WG ip.  # https://www.reddit.com/r/WireGuard/comments/siyqrj/wireguard_docker_container_not_resolving_dns_when/
ALLOWEDIPS='0.0.0.0/0'  # here just allow ipv4,  ::0/0 means ipv6

# Generated QR codes will be displayed in the docker log. Set to false to skip log output.
LOG_CONFS=true

# DNS server set in peer/client configs (can be set as 8.8.8.8). Used in server mode. Defaults to auto, which uses wireguard docker host's DNS via included CoreDNS forward.
# https://kb.adguard.com/en/general/dns-providers
PEERDNS=9.9.9.9,1.1.1.1

# PEERS can be set to a number or a list of strings separated by comma, the container will run in server mode and the necessary server and peer/client confs will be generated.
PEERS=laptop,tablet,mobile,iphone

# End
EOF
```

</details>


#### docker-compose.yml

```sh
cd "${service_save_path}"

tee docker-compose.yml 1> /dev/null << EOF
version: "3.8"

services:
  # https://github.com/MatthewVance/unbound-docker
  unbound:
    image: mvance/unbound:latest
    container_name: wireguard-unbound
    restart: unless-stopped
    mem_limit: 128m
    hostname: wireguard-unbound
    volumes:
      #- "./config/unbound:/opt/unbound/etc/unbound/"
      - ./config/unbound.conf:/opt/unbound/etc/unbound/unbound.conf:ro
    networks:
      private_network:
        ipv4_address: 10.2.0.200

  adguard:
    depends_on: [unbound]  # specify service name
    image: adguard/adguardhome:latest
    container_name: wireguard-adguard
    restart: unless-stopped
    mem_limit: 512m
    hostname: wireguard-adguard
    # Volumes store your data between container upgrades
    volumes:
      #- "./config/adguard/work:/opt/adguardhome/work"
      - ${data_dir}/adguard/work:/opt/adguardhome/work
      - "./config/adguard/conf:/opt/adguardhome/conf"
    networks:
      private_network:
        ipv4_address: 10.2.0.100

  wireguard:
    depends_on: [unbound, adguard]  # specify service name
    image: ghcr.io/linuxserver/wireguard
    container_name: wireguard-server
    restart: unless-stopped
    mem_limit: 128m
    cap_add:
      - NET_ADMIN
      # - SYS_MODULE  # As the wireguard module is already active you can remove the SYS_MODULE capability from your container run/compose.
    env_file: .env
    environment:
      - PUID=\${PUID}
      - PGID=\${PGID}
      - TZ=\${TZ}
      #- SERVERURL=\${SERVERURL}
      - SERVERPORT=\${SERVERPORT}  # same to host system mapping port ${exposted_http_port}, default is 51820
      - PEERS=\${PEERS}
      #- PEERDNS=\${PEERDNS}
      - PEERDNS=10.2.0.100  # (Important) Set it to point to adguard home private address
      - INTERNAL_SUBNET=\${INTERNAL_SUBNET}
      - ALLOWEDIPS=\${ALLOWEDIPS}
      #- ALLOWEDIPS=10.2.0.0/24  # optional - split tunnel for web panel and DNS traffic only (just loacl private_network, can't connect internet)
      - LOG_CONFS=\${LOG_CONFS}
    volumes:
      # peers config files path ./config/wireguard-server/peer_*
      - ./config/wireguard-server:/config
      - /lib/modules:/lib/modules:ro
      #- /etc/localtime:/etc/localtime:ro
    ports:
      - ${exposted_http_port}:51820/udp
    # sysctls is required for client mode
    sysctls:
      - net.ipv4.conf.all.src_valid_mark=1
    networks:
      private_network:
        ipv4_address: 10.2.0.10

networks:
  private_network:
    ipam:
      driver: default
      config:
        - subnet: 10.2.0.0/24

EOF
```

### Copying Configs

```sh
cd "${service_save_path}"
mkdir -pv config
```

Copy configs to directory `config/`

* [unbound.conf](./configs/unbound.conf)
* [AdGuardHome.yaml](./configs/AdGuardHome.yaml)

### Running Container

```sh
cd "${service_save_path}"

# - Via docker compose (For docker-compose V2)
docker compose up -d
# docker compose --env-file "${service_save_path}/docker-compose.yml" up -d
docker compose ps
docker compose down --remove-orphans

docker compose up --force-recreate


# - Via docker-compose (For docker-compose V1)
docker-compose up -d
# docker-compose -f "${service_save_path}/docker-compose.yml" up -d
docker-compose ps
# docker-compose down --remove-orphans
```

wireguard server

```sh
[#] ip link add wg0 type wireguard
[#] wg setconf wg0 /dev/fd/63
[#] ip -4 address add 10.16.1.1 dev wg0
[#] ip link set mtu 1420 up dev wg0
[#] ip -4 route add 10.16.1.6/32 dev wg0
[#] ip -4 route add 10.16.1.5/32 dev wg0
[#] ip -4 route add 10.16.1.4/32 dev wg0
[#] ip -4 route add 10.16.1.3/32 dev wg0
[#] ip -4 route add 10.16.1.2/32 dev wg0
[#] iptables -A FORWARD -i wg0 -j ACCEPT; iptables -A FORWARD -o wg0 -j ACCEPT; iptables -t nat -A POSTROUTING -o eth+ -j MASQUERADE
```

## Configuration

```sh
# specify peer name, here is 'mobile' defined in "PEERS=laptop,tablet,mobile,iphone"
peer_name='mobile'
docker compose exec wireguard /app/show-peer "${peer_name}"
```

### Adguard Home

While connected to WireGuard, navigate to <http://10.2.0.100:3000> first to setup AdGuard Home before DNS query and adblocking to work.

Tutorial blog [AdGuard Home 自建 DNS 防污染、去广告教程](https://p3terx.com/archives/use-adguard-home-to-build-dns-to-prevent-pollution-and-remove-ads-2.html)


## Change Log

* Jun 19, 2022 13:52 Sun ET
  * First draft


<!-- End -->