# Self-hosting Authelia

>[Authelia][authelia] is an open-source authentication and authorization server providing two-factor authentication and single sign-on (SSO) for your applications via a web portal. It acts as a companion for reverse proxies by allowing, denying, or redirecting requests.

Following blog <https://matwick.ca/authelia-nginx-sso/> to deploy the service properly.


## Official Site

Item|Url
---|---
Official Site|https://www.authelia.com
Official Doc|https://www.authelia.com/overview/
GitHub|https://github.com/authelia/authelia
Docker Hub|https://hub.docker.com/r/authelia/authelia


Architecture diagram

![](https://raw.githubusercontent.com/authelia/authelia/master/docs/static/images/archi.png "https://github.com/authelia/authelia")


## Deployment

### Variables Definition

```sh
domain_name=${domain_name:-'maxdsre.com'}

service_name='authelia'
service_save_path="$HOME/.docker/${service_name}"
mkdir -p "${service_save_path}"
data_dir="/data/${service_name}"

localhost_ip='127.0.0.1'

domain_name_specify="${service_name}.${domain_name}"
exposted_http_port='8091'  # default 9091
```

### env

File `.env`

<details><summary>Click to expand env conf</summary>

```sh
tee "${service_save_path}"/.env 1> /dev/null << EOF

PUID=1000  # id -u
PGID=1000  # id -g
TZ=$(cat /etc/timezone)  # America/New_York from file /etc/timezone

# - Cache
# The use of Redis is recommended to prevent file locking problems.
#REDIS_HOST (not set by default) Name of Redis container
#REDIS_HOST_PORT (default: 6379) Optional port for Redis, only use for external Redis servers that run on non-standard ports.
REDIS_HOST_PASSWORD=$(openssl rand -hex 64 | head -c 45)

# End
EOF
```

</details>


### docker-compose.yml

```sh
cd "${service_save_path}"

tee docker-compose.yml 1> /dev/null << EOF
version: '3.8'

services:
  authelia:
    # image: authelia/authelia:latest # Latest release date Dec 21, 2022
    image: authelia/authelia:master
    container_name: authelia-server
    volumes:
      - ./config:/config
    networks:
      - authelia_network
    # expose:
    #   - 9091
    ports:
      - ${localhost_ip}:${exposted_http_port}:9091
    depends_on:
      - cache
    restart: unless-stopped
    healthcheck:
      disable: true
    env_file: .env
    # environment:
    #   - TZ=

  cache:
    # https://hub.docker.com/_/redis    # 6-alpine
    image: redis:7-alpine
    container_name: authelia-redis
    restart: unless-stopped
    mem_limit: 32m
    mem_reservation: 8m
    networks:
      - authelia_network
    env_file: .env
    #command: redis-server --requirepass \${REDIS_HOST_PASSWORD}
    environment:
      - TZ=\${TZ}
      - PUID=\${PUID}
      - PGID=\${PGID}
      #- REDIS_HOST_PASSWORD=\${REDIS_HOST_PASSWORD}
    volumes:
      #- /data/authelia/redis:/var/lib/redis
      - /etc/localtime:/etc/localtime:ro
    expose:
      - 6379

networks:
  authelia_network:
    name: authelia_network

# End
EOF
```

### Configs

```sh
mkdir -p "${service_save_path}/config"
cd "${service_save_path}"
```

#### configuration.yml

<details><summary>Click to expand configuration.yml</summary>

```sh
tee config/configuration.yml 1> /dev/null << EOF
# Authelia configuration #
# https://github.com/authelia/authelia/blob/master/docs/configuration/secrets.md
# https://github.com/authelia/authelia/blob/master/examples/compose/lite/authelia/configuration.yml


#default_redirection_url: https://public.maxdsre.com

server:
  host: 0.0.0.0
  port: 9091

log:
  level: info # info, debug, trace
  file_path: /config/authelia.log

jwt_secret: $(openssl rand -hex 64 | head -c 45)
# This secret can also be set using the env variables AUTHELIA_JWT_SECRET_FILE

## Set the default 2FA method, options totp, webauthn, mobile_push.
default_2fa_method: ""

totp:
  disable: true
  issuer: authelia.com
  algorithm: sha1
  digits: 6
  period: 30
  skew: 1
  secret_size: 32

access_control:
  default_policy: deny
  rules:
    # Rules applied to everyone
    - domain:
        - "auth.maxdsre.com"
      policy: bypass
    - domain:
        #- "*.maxdsre.com"
        - "invidious.maxdsre.com"
      policy: one_factor  # Login with username and password.
    - domain:
      - "code.maxdsre.com"
      policy: two_factor  # two_factor:  Login with username, password, and 2FA.

authentication_backend:
  file:
    path: /config/users_database.yml

session:
  name: authelia_session
  # This secret can also be set using the env variables AUTHELIA_SESSION_SECRET_FILE
  secret: $(openssl rand -hex 64 | head -c 45)
  expiration: 12h           # 12 hours
  inactivity: 15m           # 15 minutes
  remember_me_duration: 1M  # 1 month
  domain: ${domain_name}       # Should match whatever your root protected domain is

  # https://www.authelia.com/configuration/session/redis/
  redis:
    host: authelia-redis
    port: 6379
    #username: authelia
    # This secret can also be set using the env variables AUTHELIA_SESSION_REDIS_PASSWORD_FILE
    #password: authelia
    database_index: 0
    maximum_active_connections: 8
    minimum_idle_connections: 0

regulation:
  max_retries: 3
  find_time: 180
  ban_time: 900

storage:
  encryption_key: $(openssl rand -hex 64 | head -c 45)
  local:
    path: /config/db.sqlite3


# Notifications are sent to users when they require a password reset, a Webauthn registration or a TOTP registration.
notifier:
  disable_startup_check: false
  filesystem:
    filename: /config/notification.txt

#   smtp:
#     username: test
#     # This secret can also be set using the env variables AUTHELIA_NOTIFIER_SMTP_PASSWORD_FILE
#     password: password
#     host: mail.example.com
#     port: 25
#     sender: admin@example.com

# End
EOF
```

</details>

Validating your configuration

```sh
# https://www.authelia.com/configuration/prologue/introduction/#validation
docker run -v $PWD/config:/config authelia/authelia:latest authelia validate-config --config /config/configuration.yml

# Configuration parsed and loaded successfully without errors.
```

#### users_database.yml

Note: the following settings are sample.

```sh
tee config/users_database.yml 1> /dev/null << EOF
# https://github.com/authelia/authelia/blob/master/examples/compose/lite/authelia/users_database.yml
# Users Database #
# This file can be used if you do not have an LDAP set up.

# List of users
users:
  authelia:
    disabled: false
    displayname: "Authelia User"
    # Password is Just_Test@2021
    password: "$argon2id$v=19$m=65536,t=1,p=8$bitFM1p0VFpwdEdlQTFUWg$ART2dn/c2ULY5rVKwdq3sh5WD+qXbf+m6+EkchnEWSo"
    email: authelia@authelia.com
    groups:
      - admins
      - dev
  # user 1:
  # user 2:

# End
EOF
```

Using the following mathod to generate hash password

```sh
docker run authelia/authelia:latest authelia hash-password 'Just_Test@2021' | sed -r -n 's@^[^[:space:]]+:[[:space:]]*@@g;p'
# $argon2id$v=19$m=65536,t=3,p=4$YPL2wtQe6pYzJAyikzi0aA$gCMPal2jNSxxi5KZW7bgoXjyEmU6d7YUQeTE8nfdCts
```

### Running Container

```sh
cd "${service_save_path}"

# - Via docker compose (For docker-compose V2)
docker compose up -d
# docker compose --env-file "${service_save_path}/docker-compose.yml" up -d
docker compose ps
docker compose down --remove-orphans

# - Via docker-compose (For docker-compose V1)
docker-compose up -d
# docker-compose -f "${service_save_path}/docker-compose.yml" up -d
docker-compose ps
# docker-compose down --remove-orphans


# - Remove unused docker files
docker system prune -a -f --volumes
```

### Nginx Reverse Proxy

Official doc https://www.authelia.com/docs/deployment/supported-proxies/nginx.html

Variables definition

```sh
domain_name=${domain_name:-'maxdsre.com'}
service_name=${server_name:-'auth'}  # authelia
domain_for_server="${service_name%%-*}.${domain_name}"
exposted_http_port='8091'  # default 9091

backend_private_ip=${backend_private_ip:-'127.0.0.1'}
# backend_private_ip='192.168.8.4'

# Nginx site config dir /etc/nginx/sites-enabled/
nginx_conf_path="/etc/nginx/sites-enabled/${service_name}.conf"
sudo mkdir -p "${nginx_conf_path%/*}"
```

#### Main Conf

<details><summary>Click to expand config info</summary>

```conf
sudo tee "${nginx_conf_path}" 1> /dev/null << EOF
# Nginx conf for authelia

upstream ${service_name}-default {
    server ${backend_private_ip}:${exposted_http_port};
}

server {
    listen 80;
    server_name ${domain_for_server};
    return 301 https://\$server_name\$request_uri;
}

server {
    listen 443 ssl http2;
    server_name ${domain_for_server};

    #error_log  /var/log/nginx/${domain_for_server%%.*}_error.log warn;  # [debug|info|notice|warn|error|crit|alert|emerg]
    #access_log /var/log/nginx/${domain_for_server%%.*}_access.log main; # format name 'main' instead of predefined 'combined' format

    location / {
        #set $upstream_authelia http://${backend_private_ip}:${exposted_http_port};
        #proxy_pass $upstream_authelia;
        proxy_pass http://${service_name}-default;

        client_body_buffer_size 128k;

        #Timeout if the real server is dead
        proxy_next_upstream error timeout invalid_header http_500 http_502     http_503;

        # Advanced Proxy Config
        send_timeout 5m;
        proxy_read_timeout 360;
        proxy_send_timeout 360;
        proxy_connect_timeout 360;

        # Basic Proxy Config
        proxy_set_header Host \$host;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto \$scheme;
        proxy_set_header X-Forwarded-Host \$http_host;
        proxy_set_header X-Forwarded-Uri $request_uri;
        proxy_set_header X-Forwarded-Ssl on;
        proxy_redirect  http://  \$scheme://;
        proxy_http_version 1.1;
        proxy_set_header Connection "";
        proxy_cache_bypass \$cookie_session;
        proxy_no_cache \$cookie_session;
        proxy_buffers 64 256k;

        # If behind reverse proxy, forwards the correct IP
        #set_real_ip_from 10.0.0.0/8;
        #set_real_ip_from 172.0.0.0/8;
        #set_real_ip_from 192.168.0.0/16;
        #set_real_ip_from fc00::/7;
        real_ip_header X-Forwarded-For;
        real_ip_recursive on;
    }
}
EOF
```

</details>

#### Authelia Conf

Save authelia conf under Nginx section conf dir

```sh
conf_section_dir="/etc/nginx/conf.d/section"
mkdir -p "${conf_section_dir}"
```

Conf *authelia.conf*

<details><summary>Click to expand authelia.conf</summary>

```conf
sudo tee "${conf_section_dir}/authelia.conf" 1> /dev/null << EOF
# Virtual endpoint created by nginx to forward auth requests.
location /authelia {
    internal;
    set \$upstream_authelia http://${backend_private_ip}:${exposted_http_port}/api/verify;
    proxy_pass \$upstream_authelia;
    proxy_pass_request_body off;
    proxy_set_header Content-Length "";

    # Timeout if the real server is dead
    proxy_next_upstream error timeout invalid_header http_500 http_502 http_503;

    # [REQUIRED] Needed by Authelia to check authorizations of the resource.
    # Provide either X-Original-URL and X-Forwarded-Proto or
    # X-Forwarded-Proto, X-Forwarded-Host and X-Forwarded-Uri or both.
    # Those headers will be used by Authelia to deduce the target url of the     user.
    # Basic Proxy Config
    client_body_buffer_size 128k;
    proxy_set_header Host \$host;
    proxy_set_header X-Original-URL \$scheme://\$http_host\$request_uri;
    proxy_set_header X-Real-IP \$remote_addr;
    proxy_set_header X-Forwarded-For \$remote_addr; 
    proxy_set_header X-Forwarded-Proto \$scheme;
    proxy_set_header X-Forwarded-Host \$http_host;
    proxy_set_header X-Forwarded-Uri \$request_uri;
    proxy_set_header X-Forwarded-Ssl on;
    proxy_redirect  http://  \$scheme://;
    proxy_http_version 1.1;
    proxy_set_header Connection "";
    proxy_cache_bypass \$cookie_session;
    proxy_no_cache \$cookie_session;
    proxy_buffers 4 32k;

    # Advanced Proxy Config
    send_timeout 5m;
    proxy_read_timeout 240;
    proxy_send_timeout 240;
    proxy_connect_timeout 240;
}
EOF
```

</details>


Conf *authelia_auth.conf*

<details><summary>Click to expand authelia_auth.conf</summary>

```conf
sudo tee "${conf_section_dir}/authelia_auth.conf" 1> /dev/null << EOF
# Basic Authelia Config
# Send a subsequent request to Authelia to verify if the user is authenticated
# and has the right permissions to access the resource.
auth_request /authelia;
# Set the `target_url` variable based on the request. It will be used to build the portal
# URL with the correct redirection parameter.
auth_request_set \$target_url \$scheme://\$http_host\$request_uri;
# Set the X-Forwarded-User and X-Forwarded-Groups with the headers
# returned by Authelia for the backends which can consume them.
# This is not safe, as the backend must make sure that they come from the
# proxy. In the future, it's gonna be safe to just use OAuth.
auth_request_set \$user \$upstream_http_remote_user;
auth_request_set \$groups \$upstream_http_remote_groups;
auth_request_set \$name \$upstream_http_remote_name;
auth_request_set \$email \$upstream_http_remote_email;
proxy_set_header Remote-User \$user;
proxy_set_header Remote-Groups \$groups;
proxy_set_header Remote-Name \$name;
proxy_set_header Remote-Email \$email;
# If Authelia returns 401, then nginx redirects the user to the login portal.
# If it returns 200, then the request pass through to the backend.
# For other type of errors, nginx will handle them as usual.
error_page 401 =302 https://${domain_for_server}/?rd=\$target_url;
EOF
```

</details>


How to add authelia to Nginx site conf

1. include the snippet *authelia.conf* into the main server block;
2. include the snippet *authelia_auth.conf* into any location block you want to protect;

For example


```conf
server {
    ...
    ...

    include /etc/nginx/conf.d/section/authelia.conf;

    include /etc/nginx/conf.d/section/ssl.conf;

    location / {
        ...
        ...

        include /etc/nginx/conf.d/section/authelia_auth.conf;
    }
}
```

Testing Nginx conf syntax then reload

```sh
sudo -i

nginx -t && nginx -s reload

# nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
# nginx: configuration file /etc/nginx/nginx.conf test is successful
```


## Bibliography

* [linuxserver - Setting Up Authelia With SWAG](https://www.linuxserver.io/blog/2020-08-26-setting-up-authelia)
* [Matwick - Self-Hosted SSO with Authelia and NGINX](https://matwick.ca/authelia-nginx-sso/)


## Change Log

* Nov 29, 2022 11:40 Tue ET
  * complete note
* Nov 22, 2021 12:54 Mon ET
  * First draft


[authelia]:https://www.authelia.com "The Single Sign-On Multi-Factor portal for web apps"

<!-- End -->