#!/usr/bin/env bash

# Target: Nextcloud service daily maintance
# Date:
# - JUn 13, 2022 Mon 07:25 ET
# - Nov 29, 2021 Mon 18:38 ET

# get script absolute path
script_absolute_path=$(readlink -nf "$0")
script_absolute_dir=$(dirname "${script_absolute_path}")
compose_absoute_dir=$(dirname "${script_absolute_dir}")
[[ -f "${compose_absoute_dir}/docker-compose.yml" || -L "${compose_absoute_dir}/docker-compose.yml" ]] || exit
cd "${compose_absoute_dir}" || return

if [[ $(docker compose ps -q 2> /dev/null | wc -l) -lt 1 ]]; then
    echo 'No service running.'
    exit 0
fi

# docker compose ps | sed -r -n '/^nextcloud-app/{p}'
# nextcloud-app  "/entrypoint.sh php-…"  app  running  9000/tcp

# - Add to crontab
# https://askubuntu.com/questions/58575/add-lines-to-cron-from-script
# /var/spool/cron/crontabs/$USER
# 05 */8 * * *   # daily_maintence_job.sh
cron_str='nextcloud maintance job'
job_cron=${job_cron:-'25 */3 * * *'}

if [[ -z $(crontab -u "${USER}" -l 2> /dev/null | sed -r -n '/'"${cron_str}"'/{p}') ]]; then
    echo "Add cron task to file /var/spool/cron/crontabs/$USER"
    cron_task_line="\n# ${cron_str} start"
    [[ -f "${script_absolute_path}" ]] && cron_task_line="${cron_task_line}\n${job_cron} /bin/bash ${script_absolute_path}"
    cron_task_line="${cron_task_line}\n# ${cron_str} end\n"

    crontab -u "${USER}" -l &> /dev/null # initialize if not exists
    (crontab -u "${USER}" -l; echo -e "${cron_task_line}" ) | crontab -u "${USER}" -
fi


# - Restart services
# docker compose down --remove-orphans
# docker compose up -d
# sleep 2

# - Essential packages
# install ffmpeg to support thumbnail preview
docker compose exec -d --user=root app sh -c "which apk && (which ffmpeg || apk add --no-cache ffmpeg); which apt-get && (which ffmpeg || (apt-get update; apt-get install -y ffmpeg; rm -rf /var/lib/apt/lists/*)); echo 'ffmpeg check finish'"

# - Maintance Jobs
cd "${compose_absoute_dir}" || return

redis_password=${redis_password:-}
[[ -f .env ]] && redis_password=$(sed -r -n '/REDIS_HOST_PASSWORD=/{s@^[^=]+=@@g;p;q}' .env 2> /dev/null)
docker compose exec cache redis-cli -a "${redis_password}" -h 127.0.0.1 flushall

docker compose exec --user www-data app php occ maintenance:repair 1> /dev/null

#docker compose exec --user www-data app php occ trashbin:cleanup --all-users

docker compose exec --user www-data app php occ app:update --all &> /dev/null

# database repair
docker compose exec --user www-data app php occ db:add-missing-indices 1> /dev/null
docker compose exec --user www-data app php occ db:add-missing-columns 1> /dev/null
docker compose exec --user www-data app php occ db:add-missing-primary-keys 1> /dev/null

# - For scan
# get env file
docker_service_absoute_dir=$(dirname "${script_absolute_path%\/nextcloud\/*}")
env_absolute_path="${docker_service_absoute_dir}/.env"
[[ ! -f "${env_absolute_path}" && -f "${docker_service_absoute_dir}/entry.sh" ]] && bash "${docker_service_absoute_dir}/entry.sh" &> /dev/null
if [[ ! -f "${env_absolute_path}" ]]; then
    echo 'Sorry, fail to find env file'
    exit 0
fi

target_user=${target_user:-}
target_user=$(sed -r -n '/^SCAN_TARGET_USER=/I{s@^[^=]+=@@g;s@[[:space:]]*#+.*$@@g;s@"@@g;s@'\''@@g;s@[[:space:]]*$@@g;p;q}' "${env_absolute_path}" 2> /dev/null)

target_path=${target_path:-}
target_path=$(sed -r -n '/^SCAN_TARGET_PATH=/I{s@^[^=]+=@@g;s@[[:space:]]*#+.*$@@g;s@"@@g;s@'\''@@g;s@[[:space:]]*$@@g;p;q}' "${env_absolute_path}" 2> /dev/null)

# scan files
# --home-only: only scan the home storage, ignoring any mounted external storage or share
docker compose exec --user www-data app php occ files:scan --home-only --all
# [[ -n "${target_user}" ]] && docker compose exec --user www-data app php occ files:scan --home-only "${target_user}"
# [[ -n "${target_user}" && -n "${target_path}" ]] && docker compose exec --user www-data app php occ files:scan --path="${target_path}" "${target_user}"

# preview generate scan (time AM/PM)
# if [[ $(date +'%p') == 'AM' ]]; then
#     [[ -n "${target_user}" ]] && docker compose exec --user www-data app php occ preview:generate-all -q "${target_user}"  # for local dir
#     [[ -n "${target_user}" && -n "${target_path}" ]] && docker compose exec --user www-data app php occ preview:generate-all -q --path="${target_path}" "${target_user}"
# fi


# Script End