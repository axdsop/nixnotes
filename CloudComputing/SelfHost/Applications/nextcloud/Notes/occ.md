# Nextcloud occ Simple Note

Nextcloud’s **occ** command (origins from “ownCloud Console”) is Nextcloud’s command-line interface. You can perform many common server operations with **occ**, such as installing and upgrading Nextcloud, manage users, encryption, passwords, LDAP setting, and more.

## Official Doc

* [Using the occ command](https://docs.nextcloud.com/server/latest/admin_manual/configuration_server/occ_command.html)

### Help Info

Run as apache user *www-data*, see doc [Run occ as your HTTP user](https://docs.nextcloud.com/server/latest/admin_manual/configuration_server/occ_command.html#run-occ-as-your-http-user).

```sh
docker compose exec --user www-data app php occ
```

<details><summary>Click to expand occ help info</summary>

```txt
Nextcloud 22.2.0

Usage:
  command [options] [arguments]

Options:
  -h, --help            Display this help message
  -q, --quiet           Do not output any message
  -V, --version         Display this application version
      --ansi            Force ANSI output
      --no-ansi         Disable ANSI output
  -n, --no-interaction  Do not ask any interactive question
      --no-warnings     Skip global warnings, show command output only
  -v|vv|vvv, --verbose  Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug

Available commands:
  check                                  check dependencies of the server environment
  help                                   Display help for a command
  list                                   List commands
  status                                 show some status information
  upgrade                                run upgrade routines after installation of a new release. The release has to be installed before.
 activity
  activity:send-mails                    Sends the activity notification mails
 app
  app:check-code                         check code to be compliant
  app:disable                            disable an app
  app:enable                             enable an app
  app:getpath                            Get an absolute path to the app directory
  app:install                            install an app
  app:list                               List all available apps
  app:remove                             remove an app
  app:update                             update an app or all apps
 background
  background:ajax                        Use ajax to run background jobs
  background:cron                        Use cron to run background jobs
  background:webcron                     Use webcron to run background jobs
 broadcast
  broadcast:test                         test the SSE broadcaster
 circles
  circles:check                          Checking your configuration
  circles:maintenance                    Clean stuff, keeps the app running
  circles:manage:config                  edit config/type of a Circle
  circles:manage:create                  create a new circle
  circles:manage:destroy                 destroy a circle by its ID
  circles:manage:details                 get details about a circle by its ID
  circles:manage:edit                    edit displayName or description of a Circle
  circles:manage:join                    emulate a user joining a Circle
  circles:manage:leave                   simulate a user joining a Circle
  circles:manage:list                    listing current circles
  circles:members:add                    Add a member to a Circle
  circles:members:details                get details about a member by its ID
  circles:members:level                  Change the level of a member from a Circle
  circles:members:list                   listing Members from a Circle
  circles:members:remove                 remove a member from a circle
  circles:members:search                 Change the level of a member from a Circle
  circles:memberships                    index and display memberships for local and federated users
  circles:remote                         remote features
  circles:shares:files                   listing shares files
  circles:sync                           Sync Circles and Members
  circles:test                           testing some features
 config
  config:app:delete                      Delete an app config value
  config:app:get                         Get an app config value
  config:app:set                         Set an app config value
  config:import                          Import a list of configs
  config:list                            List all configs
  config:system:delete                   Delete a system config value
  config:system:get                      Get a system config value
  config:system:set                      Set a system config value
 dav
  dav:create-addressbook                 Create a dav addressbook
  dav:create-calendar                    Create a dav calendar
  dav:delete-calendar                    Delete a dav calendar
  dav:list-calendars                     List all calendars of a user
  dav:move-calendar                      Move a calendar from an user to another
  dav:remove-invalid-shares              Remove invalid dav shares
  dav:retention:clean-up                 
  dav:send-event-reminders               Sends event reminders
  dav:sync-birthday-calendar             Synchronizes the birthday calendar
  dav:sync-system-addressbook            Synchronizes users to the system addressbook
 db
  db:add-missing-columns                 Add missing optional columns to the database tables
  db:add-missing-indices                 Add missing indices to the database tables
  db:add-missing-primary-keys            Add missing primary keys to the database tables
  db:convert-filecache-bigint            Convert the ID columns of the filecache to BigInt
  db:convert-mysql-charset               Convert charset of MySQL/MariaDB to use utf8mb4
  db:convert-type                        Convert the Nextcloud database to the newly configured one
 encryption
  encryption:change-key-storage-root     Change key storage root
  encryption:decrypt-all                 Disable server-side encryption and decrypt all files
  encryption:disable                     Disable encryption
  encryption:enable                      Enable encryption
  encryption:encrypt-all                 Encrypt all files for all users
  encryption:list-modules                List all available encryption modules
  encryption:migrate-key-storage-format  Migrate the format of the keystorage to a newer format
  encryption:set-default-module          Set the encryption default module
  encryption:show-key-storage-root       Show current key storage root
  encryption:status                      Lists the current status of encryption
 federation
  federation:sync-addressbooks           Synchronizes addressbooks of all federated clouds
 files
  files:cleanup                          cleanup filecache
  files:recommendations:recommend        
  files:repair-tree                      Try and repair malformed filesystem tree structures
  files:scan                             rescan filesystem
  files:scan-app-data                    rescan the AppData folder
  files:transfer-ownership               All files and folders are moved to another user - outgoing shares and incoming user file shares (optionally) are moved as well.
 group
  group:add                              Add a group
  group:adduser                          add a user to a group
  group:delete                           Remove a group
  group:info                             Show information about a group
  group:list                             list configured groups
  group:removeuser                       remove a user from a group
 integrity
  integrity:check-app                    Check integrity of an app using a signature.
  integrity:check-core                   Check integrity of core code using a signature.
  integrity:sign-app                     Signs an app using a private key.
  integrity:sign-core                    Sign core using a private key.
 l10n
  l10n:createjs                          Create javascript translation files for a given app
 log
  log:file                               manipulate logging backend
  log:manage                             manage logging configuration
  log:tail                               Tail the nextcloud logfile
  log:watch                              Watch the nextcloud logfile
 maintenance
  maintenance:data-fingerprint           update the systems data-fingerprint after a backup is restored
  maintenance:mimetype:update-db         Update database mimetypes and update filecache
  maintenance:mimetype:update-js         Update mimetypelist.js
  maintenance:mode                       set maintenance mode
  maintenance:repair                     repair this installation
  maintenance:theme:update               Apply custom theme changes
  maintenance:update:htaccess            Updates the .htaccess file
 notification
  notification:generate                  Generate a notification for the given user
  notification:test-push                 Generate a notification for the given user
 preview
  preview:repair                         distributes the existing previews into subfolders
  preview:reset-rendered-texts           Deletes all generated avatars and previews of text and md files
 security
  security:bruteforce:reset              resets bruteforce attemps for given IP address
  security:certificates                  list trusted certificates
  security:certificates:import           import trusted certificate in PEM format
  security:certificates:remove           remove trusted certificate
 serverinfo
  serverinfo:update-storage-statistics   Triggers an update of the counts related to storages used in serverinfo
 sharing
  sharing:cleanup-remote-storages        Cleanup shared storage entries that have no matching entry in the shares_external table
  sharing:expiration-notification        Notify share initiators when a share will expire the next day.
 tag
  tag:add                                Add new tag
  tag:delete                             delete a tag
  tag:edit                               edit tag attributes
  tag:list                               list tags
 text
  text:reset                             Reset a text document
 theming
  theming:config                         Set theming app config values
 trashbin
  trashbin:cleanup                       Remove deleted files
  trashbin:expire                        Expires the users trashbin
  trashbin:size                          Configure the target trashbin size
 twofactorauth
  twofactorauth:cleanup                  Clean up the two-factor user-provider association of an uninstalled/removed provider
  twofactorauth:disable                  Disable two-factor authentication for a user
  twofactorauth:enable                   Enable two-factor authentication for a user
  twofactorauth:enforce                  Enabled/disable enforced two-factor authentication
  twofactorauth:state                    Get the two-factor authentication (2FA) state of a user
 update
  update:check                           Check for server and app updates
 user
  user:add                               adds a user
  user:add-app-password                  Add app password for the named user
  user:delete                            deletes the specified user
  user:disable                           disables the specified user
  user:enable                            enables the specified user
  user:info                              show user info
  user:lastseen                          shows when the user was logged in last time
  user:list                              list configured users
  user:report                            shows how many users have access
  user:resetpassword                     Resets the password of the named user
  user:setting                           Read and modify user settings
 versions
  versions:cleanup                       Delete versions
  versions:expire                        Expires the users file versions
 workflows
  workflows:list                         Lists configured workflows
```

</details>

Overview

```sh
# nextcloud service name: app

# - get help info
docker compose exec --user www-data app php occ

# --output=json / --output=json_pretty
# This output option is available on all list and list-like commands: status, check, app:list, config:list, encryption:status and encryption:list-modules

# - Nextcloud relevant
# show Nextcloud version
docker compose exec --user www-data app php occ -V  # Nextcloud 22.2.0 / 29.0.4

# show Nextcloud server status
docker compose exec --user www-data app php occ status #--output=json #_pretty

# - APP
# list installed app
docker compose exec --user www-data app php occ app:list

# - Config
docker compose exec --user www-data app php occ config:list

# - Clean trashbin
docker compose exec --user www-data app php occ trashbin:cleanup --all-users

```


### Useful Commands

```sh
cd "${service_save_path}"

# nextcloud service name: app
# redis service name: cache

redis_password=${redis_password:-}
[[ -f .env ]] && redis_password=$(sed -r -n '/REDIS_HOST_PASSWORD=/{s@^[^=]+=@@g;p;q}' .env 2> /dev/null)

# - check redis status
dis-cli -a "${redis_password}" -h 127.0.0.1 monitor

# - update all installed app
docker compose exec --user www-data app php occ app:update --all
# docker compose exec --user www-data app php occ files:scan-app-data
# docker compose exec --user www-data app php occ preview:pre-generate
# - repair
docker compose exec --user www-data app php occ maintenance:repair


# - scan files
docker compose exec cache redis-cli -a "${redis_password}" -h 127.0.0.1 flushall

docker compose exec --user www-data app php occ files:scan --all

docker compose exec --user www-data app php occ trashbin:cleanup --all-users
```

Fix `This Nextcloud instance is currently in maintenance mode, which may take a while.`

```sh
docker compose exec --user www-data app php occ maintenance:mode --off
# docker compose exec --user www-data app php occ upgrade
```

Fix database missing indexes

>The database is missing some indexes. Due to the fact that adding indexes on big tables could take some time they were not added automatically. By running "occ db:add-missing-indices" those missing indexes could be added manually while the instance keeps running. Once the indexes are added queries to those tables are usually much faster.

```sh
# bash /home/chuchu/Documents/Scripts/docker-selfhost/Scripts/service_maintance.sh
docker compose exec --user www-data app php occ db:add-missing-indices
docker compose exec --user www-data app php occ db:add-missing-columns
docker compose exec --user www-data app php occ db:add-missing-primary-keys
```


>One or more mimetype migrations are available. Occasionally new mimetypes are added to better handle certain file types. Migrating the mimetypes take a long time on larger instances so this is not done automatically during upgrades. Use the command `occ maintenance:repair --include-expensive` to perform the migrations.

```sh
docker compose exec --user www-data app php occ maintenance:repair --include-expensive
```

>Server has no maintenance window start time configured. This means resource intensive daily background jobs will also be executed during your main usage time. We recommend to set it to a time of low usage, so users are less impacted by the load caused from these heavy tasks. For more details see the [documentation](https://docs.nextcloud.com/server/30/go.php?to=admin-background-jobs).

```sh
# UTC -> EST (03:00 AM	-> 10:00 PM)
docker compose exec --user www-data app php occ config:system:set maintenance_window_start --type=integer --value=3
```

<!-- End -->