# Self-hosting Nextcloud Server

[Nextcloud][nextcloud] is a famous self-hosted file share and collaboration platform on the web.

[Nextcloud][nextcloud] provides website <https://scan.nextcloud.com> to check the security of your private Nextcloud server.

## TOC

1. [Official Site](#official-site)  
1.1 [Documentation](#documentation)  
2. [Deployment](#deployment)  
2.1 [Variables Definition](#variables-definition)  
2.2 [docker-compose.yml](#docker-composeyml)  
2.2.1 [For Apache](#for-apache)  
2.2.2 [For php-fpm](#for-php-fpm)  
2.3 [Running Container](#running-container)  
2.4 [Nginx Reverse Proxy](#nginx-reverse-proxy)  
2.4.1 [Conf](#conf)  
3. [Customsize Configuration](#customsize-configuration)  
3.1 [Variables Definition](#variables-definition-1)  
3.2 [Thumbnail Preview](#thumbnail-preview)  
3.3 [User Operation](#user-operation)  
3.4 [Apps Operation](#apps-operation)  
3.5 [External Storage Mount](#external-storage-mount)  
3.6 [Scan Files](#scan-files)  
4. [Database Operation](#database-operation)  
4.1 [Database Dump & Import](#database-dump--import)  
4.1.1 [Database Dump](#database-dump)  
4.1.2 [Database Import](#database-import)  
4.2 [PostgreSQL Major Version Upgrade](#postgresql-major-version-upgrade)  
5. [Issues Occuring](#issues-occuring)  
5.1 [Database initialise fail](#database-initialise-fail)  
5.2 [Database Insufficient privilege](#database-insufficient-privilege)  
5.3 [apps dir permission](#apps-dir-permission)  
5.4 [Access through untrusted domain](#access-through-untrusted-domain)  
5.5 [Icons Missing](#icons-missing)  
5.6 [Background Jobs Selector](#background-jobs-selector)  
5.7 [Too Many Requests](#too-many-requests)  
5.8 [External Storage](#external-storage)  
6. [Tutotials](#tutotials)  
7. [Bibliography](#bibliography)  
8. [Change Log](#change-log)  


## Official Site

Item|Url
---|---
Official site|https://nextcloud.com/athome/
Official doc|https://portal.nextcloud.com (Enterprise)<br/>https://docs.nextcloud.com ([User Manual](https://docs.nextcloud.com/server/stable/user_manual/)/[Administration Manual](https://docs.nextcloud.com/server/latest/admin_manual/)/[Developer Manual](https://docs.nextcloud.com/server/stable/developer_manual))
GitHub|https://github.com/nextcloud/docker
Docker|https://hub.docker.com/_/nextcloud/


### Documentation

* [GitHub nextcloud/docker Readme](https://github.com/nextcloud/docker)
* [Installation and server configuration](https://docs.nextcloud.com/server/latest/admin_manual/installation/index.html)
* [Maintenance and Release Schedule](https://github.com/nextcloud/server/wiki/Maintenance-and-Release-Schedule)
* [Compose file version 3 reference](https://docs.docker.com/compose/compose-file/compose-file-v3/)

## Deployment

[Nextcloud][nextcloud]

Nextcloud docker image tag: *stable* (apt-get, include apache 937MB)/ *stable-fpm* (apt-get 926MB) / *stable-fpm-alpine* (apk 548MB smaller)

For database: *mariadb:latest* size 409MB, *postgres:13-alpine* size 192MB.


### Variables Definition

```sh
service_name='nextcloud'
service_save_path="$HOME/.docker/${service_name}"
mkdir -p "${service_save_path}"
data_dir="/data/${service_name}"

localhost_ip='127.0.0.1'

domain_name_specify="${service_name}.maxdsre.com"
exposted_http_port='8300'

upload_size_limit='4096M' # 4GB

nextcloud_admin_user='admin'
nextcloud_admin_pass="Nextcloud@$(date +'%Y')" # initial password e.g.Nextcloud@2021
```

File `.env`

<details><summary>Click to expand env conf</summary>

```sh
tee "${service_save_path}"/.env 1> /dev/null << EOF
# - Nextcloud
NEXTCLOUD_ADMIN_USER="${nextcloud_admin_user}"
NEXTCLOUD_ADMIN_PASSWORD="${nextcloud_admin_pass}"
#NEXTCLOUD_DATA_DIR  # default /var/www/html/data
#NEXTCLOUD_UPDATE   # default 0
# Don't add prefix https?://, otherwise it will prompt error
NEXTCLOUD_TRUSTED_DOMAINS="${domain_name_specify}"


# - Database
#/var/lib/mysql  MySQL / MariaDB Data
#/var/lib/postgresql/data  PostgreSQL Data

#MYSQL_HOST=
MYSQL_USER=nextcloud
MYSQL_DATABASE=nextcloud
MYSQL_PASSWORD=$(openssl rand -hex 64 | head -c 45)
MYSQL_ROOT_PASSWORD=$(openssl rand -hex 64 | head -c 45)

#POSTGRES_HOST  # Hostname of the database server using postgres.
POSTGRES_DB=nextcloud
POSTGRES_USER=nextcloud
POSTGRES_PASSWORD=$(openssl rand -hex 64 | head -c 45)

# - Cache
# The use of Redis is recommended to prevent file locking problems.
#REDIS_HOST (not set by default) Name of Redis container
#REDIS_HOST_PORT (default: 6379) Optional port for Redis, only use for external Redis servers that run on non-standard ports.
REDIS_HOST_PASSWORD=$(openssl rand -hex 64 | head -c 45)

# - PHP
# https://docs.nextcloud.com/server/latest/admin_manual/configuration_files/big_file_upload_configuration.html#configuring-php
PHP_MEMORY_LIMIT="1024M"  # default 521M
PHP_UPLOAD_LIMIT="${upload_size_limit}"  # default 521M This sets the upload limit (post_max_size and upload_max_filesize) for big files.


# - SMTP
# - Object Store (AWS S3/OpenStack Swift)

# End
EOF
```

</details>

### docker-compose.yml

Sample from Github user [ismailyenigul](https://gist.github.com/ismailyenigul/f03b4f5f15e5e61ac5b80905c5d2890a).


#### For Apache

Image *nextcloud:stable* builds in Apache web server.

File `docker-compose-apache.yml`

<details><summary>Click to expand customized docker-compose.yml</summary>

```yml
# Use nextcloud:stable which builds in Apache web server
version: '3.8'

services:
  # db_m:
  #   image: mariadb:latest
  #   container_name: nextcloud-db-m
  #   restart: unless-stopped
  #   networks:
  #     - nextcloud_network
  #   command: --transaction-isolation=READ-COMMITTED --binlog-format=ROW --character-set-server=utf8 --collation-server=utf8_general_ci --innodb-read-only-compressed=OFF
  #   volumes:
  #     - /data/nextcloud/db:/var/lib/mysql
  #     - /etc/localtime:/etc/localtime:ro
  #   env_file: .env
  #   environment:
  #     - MYSQL_ROOT_PASSWORD=${MYSQL_ROOT_PASSWORD}
  #     - MYSQL_PASSWORD=${MYSQL_PASSWORD}
  #     - MYSQL_DATABASE=${MYSQL_DATABASE}
  #     - MYSQL_USER=${MYSQL_USER}

  db_p:
    image: postgres:14-alpine  # v14, alpine is 16 on Aug 12, 2024
    container_name: nextcloud-db-p
    restart: unless-stopped
    networks:
      - nextcloud_network
    volumes:
      - /data/nextcloud/db:/var/lib/postgresql/data
      - /etc/localtime:/etc/localtime:ro
    env_file: .env
    environment:
      - POSTGRES_DB=${POSTGRES_DB}
      - POSTGRES_USER=${POSTGRES_USER}
      - POSTGRES_PASSWORD=${POSTGRES_PASSWORD}

  cache:
    # https://hub.docker.com/_/redis    # 6-alpine
    image: redis:7-alpine
    container_name: nextcloud-redis
    restart: unless-stopped
    # mem_limit: 128m
    # mem_reservation: 16m
    networks:
      - nextcloud_network
    command: redis-server --requirepass ${REDIS_HOST_PASSWORD}
    env_file: .env
    environment:
      - REDIS_HOST_PASSWORD=${REDIS_HOST_PASSWORD}
    volumes:
      - /data/nextcloud/redis:/var/lib/redis
      - /etc/localtime:/etc/localtime:ro
    expose:
      - 6379

  app:
    image: nextcloud:stable  # stable build-in apache, fpm stable-fpm/stable-fpm-alpine (smaller)
    container_name: nextcloud-app
    restart: unless-stopped
    mem_limit: 1024m
    mem_reservation: 256m
    networks:
      - nextcloud_network
    ports:
      - ${localhost_ip}:${exposted_http_port}:80
    depends_on:
      # - db_m
      - db_p
      - cache
    volumes:
      - /data/nextcloud/html:/var/www/html
      # - /data/nextcloud/custom_apps:/var/www/html/custom_apps
      # - /data/nextcloud/config:/var/www/html/config
      - /data/nextcloud/data:/var/www/html/data
      - /etc/localtime:/etc/localtime:ro
    env_file: .env
    environment:
      - OVERWRITEPROTOCOL=https
      - NEXTCLOUD_ADMIN_USER=${NEXTCLOUD_ADMIN_USER}
      - NEXTCLOUD_ADMIN_PASSWORD=${NEXTCLOUD_ADMIN_PASSWORD}
      - NEXTCLOUD_TRUSTED_DOMAINS=${NEXTCLOUD_TRUSTED_DOMAINS}
      - REDIS_HOST=cache
      - REDIS_HOST_PASSWORD=${REDIS_HOST_PASSWORD}
      - POSTGRES_HOST=db_p
      - POSTGRES_DB=${POSTGRES_DB}
      - POSTGRES_USER=${POSTGRES_USER}
      - POSTGRES_PASSWORD=${POSTGRES_PASSWORD}
      # - MYSQL_HOST=db_m
      # - MYSQL_USER=${MYSQL_USER}
      # - MYSQL_PASSWORD=${MYSQL_PASSWORD}
      # - MYSQL_DATABASE=${MYSQL_DATABASE}

  cron:
    # keep the volume: settings same to service 'app'
    image: nextcloud:stable  # stable build-in apache, fpm stable-fpm/stable-fpm-alpine (smaller)
    container_name: nextcloud-cron
    restart: unless-stopped
    networks:
      - nextcloud_network
    volumes:
      - /data/nextcloud/html:/var/www/html
      - /data/nextcloud/data:/var/www/html/data
      # - /data/nextcloud/custom_apps:/var/www/html/custom_apps
      # - /data/nextcloud/config:/var/www/html/config
      - /etc/localtime:/etc/localtime:ro
    entrypoint: /cron.sh  # https://github.com/nextcloud/docker/blob/master/22/fpm-alpine/cron.sh
    depends_on:
      # - db_m
      - db_p
      - cache

networks:
  nextcloud_network:
    name: nextcloud_network

# End
```

</details>

#### For php-fpm

File `docker-compose-phpfpm.yml` add Nginx image as a web server.

<details><summary>Click to expand customized docker-compose.yml</summary>

```yml
# Use nextcloud:stable-fpm-alpine with Nginx container as web server
# Attention: don't use 'container_name:', it'll prompts error 'Additional property container is not allowed'

# Use nextcloud:stable-fpm-alpine with Nginx container as web server
# Attention: don't use 'container_name:', it'll prompts error 'Additional property container is not allowed'
# GitHub Repository https://github.com/nextcloud/docker
# Docker Hub        https://hub.docker.com/_/nextcloud
# https://github.com/nextcloud/server/wiki/Maintenance-and-Release-Schedule

version: '3.8'

services:
  # db_m:
  #   image: mariadb:latest
  #   container_name: nextcloud-db-m
  #   restart: unless-stopped
  #   networks:
  #     - nextcloud_network
  #   command: --transaction-isolation=READ-COMMITTED --binlog-format=ROW --character-set-server=utf8 --collation-server=utf8_general_ci --innodb-read-only-compressed=OFF
  #   volumes:
  #     - /data/nextcloud/db:/var/lib/mysql
  #     - /etc/localtime:/etc/localtime:ro
  #   env_file: .env
  #   environment:
  #     - MYSQL_ROOT_PASSWORD=${MYSQL_ROOT_PASSWORD}
  #     - MYSQL_PASSWORD=${MYSQL_PASSWORD}
  #     - MYSQL_DATABASE=${MYSQL_DATABASE}
  #     - MYSQL_USER=${MYSQL_USER}

  db_p:
    # https://hub.docker.com/_/postgres
    #image: postgres:14-alpine  # version 14, 20210930 ~ 20261112
    image: postgres:15-alpine  # version 15, 20221013 ~ 20271111
    #image: postgres:16-alpine  # version 16, 20230914 ~ 20281109
    container_name: nextcloud-db-p
    restart: unless-stopped
    networks:
      - nextcloud_network
    volumes:
      #- ./sql:/var/lib/postgresql/data/sql:ro  # used to import sql
      - /data/nextcloud/db:/var/lib/postgresql/data
      - /etc/localtime:/etc/localtime:ro
    env_file: .env
    environment:
      - POSTGRES_DB=${POSTGRES_DB}
      - POSTGRES_USER=${POSTGRES_USER}
      - POSTGRES_PASSWORD=${POSTGRES_PASSWORD}

  cache:
    image: redis:7-alpine
    container_name: nextcloud-redis
    restart: unless-stopped
    mem_limit: 200m
    mem_reservation: 16m
    networks:
      - nextcloud_network
    command: redis-server --requirepass ${REDIS_HOST_PASSWORD}
    env_file: .env
    environment:
      - REDIS_HOST_PASSWORD=${REDIS_HOST_PASSWORD}
    volumes:
      - /data/nextcloud/redis:/var/lib/redis
      - /etc/localtime:/etc/localtime:ro
    expose:
      - 6379

  app:
    # stable-fpm (apt-get)/stable-fpm-alpine (apk smaller)
    #image: nextcloud:24-fpm-alpine # for version 24
    image: nextcloud:stable-fpm-alpine # for version 25 since v25.0.2
    container_name: nextcloud-app
    restart: unless-stopped
    mem_limit: 1024m
    mem_reservation: 256m
    networks:
      - nextcloud_network
    # ports:
    #   - ${localhost_ip}:${exposted_http_port}:80
    depends_on:
      # - db_m
      - db_p
      - cache
    volumes:
      - /data/nextcloud/html:/var/www/html
      - /data/nextcloud/data:/var/www/html/data
      # - /data/nextcloud/custom_apps:/var/www/html/custom_apps
      # - /data/nextcloud/config:/var/www/html/config
      - ./configs/web/phpfpm_tweak.conf:/usr/local/etc/php-fpm.d/www.conf:ro
      - /home/cloud_mount:/mnt/cloud_mount:rw  # for local external storage
      - /etc/localtime:/etc/localtime:ro
    env_file: .env
    environment:
      - OVERWRITEPROTOCOL=https
      - NEXTCLOUD_ADMIN_USER=${NEXTCLOUD_ADMIN_USER}
      - NEXTCLOUD_ADMIN_PASSWORD=${NEXTCLOUD_ADMIN_PASSWORD}
      - NEXTCLOUD_TRUSTED_DOMAINS=${NEXTCLOUD_TRUSTED_DOMAINS}
      - REDIS_HOST=cache
      - REDIS_HOST_PASSWORD=${REDIS_HOST_PASSWORD}
      - POSTGRES_HOST=db_p
      - POSTGRES_DB=${POSTGRES_DB}
      - POSTGRES_USER=${POSTGRES_USER}
      - POSTGRES_PASSWORD=${POSTGRES_PASSWORD}
      # - MYSQL_HOST=db_m
      # - MYSQL_USER=${MYSQL_USER}
      # - MYSQL_PASSWORD=${MYSQL_PASSWORD}
      # - MYSQL_DATABASE=${MYSQL_DATABASE}

  cron:
    # keep the volume: settings same to service 'app'
    # stable-fpm (apt-get)/stable-fpm-alpine (apk smaller)
    #image: nextcloud:24-fpm-alpine # for version 24
    image: nextcloud:stable-fpm-alpine # for version 25 since v25.0.2
    container_name: nextcloud-cron
    restart: unless-stopped
    networks:
      - nextcloud_network
    volumes:
      - /data/nextcloud/html:/var/www/html
      - /data/nextcloud/data:/var/www/html/data
      # - /data/nextcloud/custom_apps:/var/www/html/custom_apps
      # - /data/nextcloud/config:/var/www/html/config
      - /etc/localtime:/etc/localtime:ro
    entrypoint: /cron.sh  # https://github.com/nextcloud/docker/blob/master/22/fpm-alpine/cron.sh
    depends_on:
      # - db_m
      - db_p
      - cache

  web:
    image: nginx:alpine
    # build: ./configs/web  # https://git.belmankraul.com/docker/nginx-nextcloud/src/branch/master/Dockerfile
    container_name: nextcloud-web
    restart: unless-stopped
    networks:
      - nextcloud_network
    ports:
      - ${localhost_ip}:${exposted_http_port}:80
    depends_on:
      - app
    volumes:
      - ./configs/web/nginx.conf:/etc/nginx/nginx.conf:ro  # https://github.com/nextcloud/docker/blob/master/.examples/docker-compose/insecure/postgres/fpm/web/nginx.conf
      - /etc/localtime:/etc/localtime:ro
    volumes_from:
      - app

networks:
  nextcloud_network:
    name: nextcloud_network

# End
```

</details>

File `nginx.conf`

<details><summary>Click to expand nginx.conf</summary>

```sh
nginx_conf_path="${service_save_path}/configs/web/nginx.conf"
mkdir -pv "${nginx_conf_path%/*}"

curl -fsSL 'https://raw.githubusercontent.com/nextcloud/docker/master/.examples/docker-compose/insecure/postgres/fpm/web/nginx.conf' > "${nginx_conf_path}"
```

</details>


File `phpfpm_tweak.conf`

<details><summary>Click to expand php-fpm tweak conf</summary>

```sh
# 	./configs/web/phpfpm_tweak.conf:/usr/local/etc/php-fpm.d/www.conf:ro
phpfpm_conf_path="${service_save_path}/configs/web/phpfpm_tweak.conf" # /usr/local/etc/php-fpm.d/www.conf

tee "${phpfpm_conf_path}" 1> /dev/null << EOF
[www]
user = www-data
group = www-data
listen = 127.0.0.1:9000

pm = dynamic
pm.max_children = 120
pm.start_servers = 12
pm.min_spare_servers = 6
pm.max_spare_servers = 18

;https://docs.nextcloud.com/server/latest/admin_manual/installation/server_tuning.html
;https://haefelfinger.ch/posts/2021/2021-03-29-nextcloud-tuning/
;https://help.nextcloud.com/t/why-is-my-nextcloud-frontend-so-slow/75443
;https://www.itsupportwale.com/blog/tuning-nextcloud-for-better-performance/

EOF
```

</details>


### Running Container

Choose compose file

```sh
cd "${service_save_path}"

# for apache
# ln -fs docker-compose-apache.yml docker-compose.yml

# for fpm/fpm-alpine
ln -fs docker-compose-phpfpm.yml docker-compose.yml
```

```sh
cd "${service_save_path}"

# - Via docker compose (For docker-compose V2)
docker compose up -d
# docker compose --env-file "${service_save_path}/docker-compose.yml" up -d
docker compose ps
docker compose down --remove-orphans

# - Via docker-compose (For docker-compose V1)
docker-compose up -d
# docker-compose -f "${service_save_path}/docker-compose.yml" up -d
docker-compose ps
# docker-compose down --remove-orphans


# - Remove unused docker files
docker system prune -a -f --volumes
```

<details><summary>Click to expand container process</summary>

`docker compose` output for *php-fpm*

```sh
~/.docker/nextcloud# docker compose ps
NAME                COMMAND                  SERVICE             STATUS              PORTS
nextcloud-app       "/entrypoint.sh php-…"   app                 running             9000/tcp
nextcloud-cron      "/cron.sh"               cron                running             9000/tcp
nextcloud-db-p      "docker-entrypoint.s…"   db_p                running             5432/tcp
nextcloud-redis     "docker-entrypoint.s…"   cache               running             6379/tcp
nextcloud-web       "/docker-entrypoint.…"   web                 running             127.0.0.1:8300->80/tcp
```

</details>

Please wait at least 30 seconds to make it work, otherwhise it'll prompts 502 error. You can use the following command to test service status.

```sh
docker compose exec --user www-data app php occ status
```

### Nginx Reverse Proxy

Variables definition

```sh
# https://nextcloud.maxdsre.com
service_name='nextcloud'
domain_name_specify="${service_name}.maxdsre.com"

exposted_http_port='8300'
backend_private_ip=${backend_private_ip:-'127.0.0.1'}
# backend_private_ip='192.168.8.4'

# Nginx site config dir /etc/nginx/sites-enabled/
nginx_conf_path="/etc/nginx/sites-enabled/${service_name}.conf"
mkdir -p "${nginx_conf_path%/*}"
```

#### Conf

Official doc [Nextcloud in the webroot of NGINX](https://docs.nextcloud.com/server/stable/admin_manual/installation/nginx.html#nextcloud-in-the-webroot-of-nginx) (GitHub sample [nginx.conf](https://github.com/nextcloud/docker/blob/master/.examples/docker-compose/insecure/postgres/fpm/web/nginx.conf))

<details><summary>Click to expand Nginx conf</summary>

```conf
sudo tee "${nginx_conf_path}" 1> /dev/null << EOF
upstream ${service_name}-default {
    server ${backend_private_ip}:${exposted_http_port};
}

# Redirect HTTP to HTTPS
server {
    listen 80;
    # listen [::]:80 ipv6only=on;
    server_name ${domain_name_specify};
    return 301 https://\$host\$request_uri;
}

#map \$request_uri \$${service_name}_log_ignore {
#    ~^/(libs|images|css|static|colibri-ws)/ 0;
#    ~^/(http-bind|xmpp-websocket) 0;
#    ~\.js 0; #    ~^/manifest 0;
#    default 1;
#}

map \$status \$${service_name}_log_ignore {
    ~^[23]  0;
    default 1;
}

server {
    listen 443 ssl http2;
    # listen [::]:443 ssl http2 ipv6only=on;
    server_name ${domain_name_specify};

    error_log  /var/log/nginx/${service_name}_error.log warn;  # [debug|info|notice|warn|error|crit|alert|emerg]
    access_log /var/log/nginx/${service_name}_access.log main if=\$${service_name}_log_ignore; # format name 'main' instead of predefined 'combined' format
    # access_log off;

    include /etc/nginx/conf.d/section/ssl.conf;

    # set max upload size and increase upload timeout
    # https://docs.nextcloud.com/server/latest/admin_manual/configuration_files/big_file_upload_configuration.html
    client_max_body_size ${upload_size_limit};  # 4GB
    client_body_timeout 300s;
    fastcgi_buffers 64 4K;

    # The settings allows you to optimize the HTTP2 bandwitdth.
    # See https://blog.cloudflare.com/delivering-http-2-upload-speed-improvements/ for tunning hints
    client_body_buffer_size 512k;
    #client_body_buffer_size 100M;
    fastcgi_max_temp_file_size 0;

    # Increase PHP timeouts
    proxy_read_timeout 1800;
    proxy_connect_timeout 1800;
    proxy_send_timeout 1800;
    send_timeout 1800;
    fastcgi_read_timeout 1800;


    # HTTP response headers borrowed from Nextcloud `.htaccess`
    add_header Referrer-Policy                      "no-referrer"   always;
    add_header X-Content-Type-Options               "nosniff"       always;
    add_header X-Download-Options                   "noopen"        always;
    add_header X-Frame-Options                      "SAMEORIGIN"    always;
    add_header X-Permitted-Cross-Domain-Policies    "none"          always;
    add_header X-Robots-Tag                         "none"          always;
    add_header X-XSS-Protection                     "1; mode=block" always;

    # Remove X-Powered-By, which is an information leak
    fastcgi_hide_header X-Powered-By;

    location ^~ /.well-known {
        # https://docs.nextcloud.com/server/latest/admin_manual/issues/general_troubleshooting.html#service-discovery
        location = /.well-known/carddav { return 301 /remote.php/dav/; }
        location = /.well-known/caldav  { return 301 /remote.php/dav/; }

        # https://help.nextcloud.com/t/nextcloud-21-nodeinfo-and-webfinger/109423/
        # https://github.com/nextcloud/documentation/issues/6157
        #location = /.well-known/webfinger   { return 301 /index.php$uri; }
        #location = /.well-known/nodeinfo   { return 301 /index.php$uri; }

        location /.well-known/acme-challenge    { try_files \$uri \$uri/ =404; }
        location /.well-known/pki-validation    { try_files \$uri \$uri/ =404; }

        # Let Nextcloud's API for `/.well-known` URIs handle all other
        # requests by passing them to the front-end controller.
        return 301 /index.php\$request_uri;
    }

    # Rules borrowed from `.htaccess` to hide certain paths from clients
    location ~ ^/(?:build|tests|config|lib|3rdparty|templates|data)(?:$|/)  { return 404; }
    location ~ ^/(?:\.|autotest|occ|issue|indie|db_|console)                { return 404; }

    location / {
        # https://github.com/nextcloud/server/issues/11127#issuecomment-615999723
        add_header Content-Security-Policy "default-src 'self'; frame-ancestors 'self'; style-src 'self' 'unsafe-inline'; script-src 'self' 'unsafe-inline'; img-src 'self' data:; font-src 'self' data:";

        proxy_set_header Host \$host;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto \$scheme;

        proxy_pass http://${service_name}-default;

        # https://serverfault.com/questions/587386/an-upstream-response-is-buffered-to-a-temporary-file
        proxy_buffers 16 1M; 
        proxy_buffer_size 10M;
        proxy_busy_buffers_size 10M;
    }

    # remove errors from favicon from nginx server logs
    location = /favicon.ico {
        return 204;
        log_not_found off;
    }
}
EOF
```

</details>


Testing Nginx conf syntax then reload

```sh
nginx -t && nginx -s reload

# nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
# nginx: configuration file /etc/nginx/nginx.conf test is successful
```

Testing domain if works

```sh
$ curl -I nextcloud.maxdsre.com

HTTP/1.1 301 Moved Permanently
Server: nginx
Date: Sun, 07 Nov 2021 14:59:18 GMT
Content-Type: text/html
Content-Length: 162
Connection: keep-alive
Location: https://nextcloud.maxdsre.com/
```


## Customsize Configuration

### Variables Definition

```sh
new_group='normal'

# example
new_user=${new_user:-'testing'}
new_user_pass=${new_user_pass:-'testing@123'}
```

Test service status

```sh
# show Nextcloud server status
docker compose exec --user www-data app php occ status #--output=json #_pretty
```

```sh
docker compose exec --user www-data app php occ config:system:set overwriteprotocol --value="https"
```


### Thumbnail Preview

Just like issue [No video preview thumbnails](https://github.com/nextcloud/docker/issues/1432). Official doc [Previews configuration](https://docs.nextcloud.com/server/latest/admin_manual/configuration_files/previews_configuration.html) says:

> By default, Nextcloud can generate previews for the following filetypes: Images files, Cover of MP3 files, Text documents

To enable it, set configuration option `'enable_previews' => true` in config file *config.php* (Path in container */var/www/html/config/config.php*)

To restrict file type, add the following configs into file *config.php*

<details><summary>Click to expand configs</summary>

HEIC needs package *imagemagic* (similar [issue 1](https://help.nextcloud.com/t/heic-mov-previews/62973 "HEIC/MOV previews"), [issue 2](https://help.nextcloud.com/t/heic-in-browser/111656 "HEIC in browser"))

```php
  'enable_previews' => true,
  'enabledPreviewProviders' =>
  array (
    0 => 'OC\\Preview\\TXT',
    1 => 'OC\\Preview\\MarkDown',
    2 => 'OC\\Preview\\OpenDocument',
    3 => 'OC\\Preview\\PDF',
    4 => 'OC\\Preview\\MSOffice2003',
    5 => 'OC\\Preview\\MSOfficeDoc',
    6 => 'OC\\Preview\\Image',
    7 => 'OC\\Preview\\Photoshop',
    8 => 'OC\\Preview\\TIFF',
    9 => 'OC\\Preview\\HEIC',
    10 => 'OC\\Preview\\SVG',
    11 => 'OC\\Preview\\Font',
    12 => 'OC\\Preview\\MP3',
    13 => 'OC\\Preview\\Movie',
    14 => 'OC\\Preview\\MKV',
    15 => 'OC\\Preview\\MP4',
    16 => 'OC\\Preview\\AVI',
  )
```

</details>

Here I use shell command to set these configurations

```sh
cd "${service_save_path}"

# - enable enable_previews
docker compose exec --user www-data app php occ config:system:set -q enable_previews --type=boolean --value=true


# - setting enabledPreviewProviders
docker compose exec --user www-data app php occ config:system:delete -q enabledPreviewProviders

# https://github.com/ReinerNippes/nextcloud_on_docker/blob/master/roles/nextcloud_config/defaults/main.yml
preview_type_list='TXT MarkDown OpenDocument PDF MSOffice2003 MSOfficeDoc Image Photoshop TIFF HEIC SVG Font MP3 Movie MKV MP4 AVI'  # also PNG JPEG GIF BMP XBitmap

p_ind=0; sed -n 's@ @\n@g;p' <<< "${preview_type_list}" | while read -r p_val; do
  # https://stackoverflow.com/questions/51616789/exit-code-from-docker-compose-breaking-while-loop
  docker compose exec -d --user www-data app php occ config:system:set enabledPreviewProviders ${p_ind} --type=string --value="OC\\Preview\\${p_val}"
  sleep 1 # must set this, otherwise lost item
  let "p_ind=p_ind+1"
done


# deprecated method
: '
# https://stackoverflow.com/questions/43158140/way-to-create-multiline-comments-in-bash
command_save_path=$(mktemp -t 'XXXXXX.sh')
p_ind=0; echo "${preview_type_list}" | sed -n 's@ @\n@g;p' | while read -r p_val; do echo "docker compose exec --user www-data app php occ config:system:set enabledPreviewProviders ${p_ind} --type=string --value=\"OC\\\\Preview\\\\${p_val}\""; let "p_ind=p_ind+1"; done > "${command_save_path}"
echo "${command_save_path}"
bash "${command_save_path}"
rm -f "${command_save_path}"
'

# get list
docker compose exec --user www-data app php occ config:system:get enabledPreviewProviders
```

Video preview thumbnails alse require utility *ffmpeg** (see [example](https://github.com/nextcloud/docker/tree/master/.examples#ffmpeg))

```sh
# for fpm-alpine  https://github.com/nextcloud/docker/blob/master/22/fpm-alpine/Dockerfile
docker compose exec --user=root app sh -c "which ffmpeg || apk add --no-cache ffmpeg"  # /usr/bin/ffmpeg in container

# for fpm  https://github.com/nextcloud/docker/blob/master/22/fpm/Dockerfile
docker compose exec --user=root app sh -c "which ffmpeg || (apt-get update; apt-get install -y ffmpeg; rm -rf /var/lib/apt/lists/*); which ffmpeg"
```

Adjust chunk size on Nextcloud side (not work?)

```sh
# https://docs.nextcloud.com/server/latest/admin_manual/configuration_files/big_file_upload_configuration.html#adjust-chunk-size-on-nextcloud-side

docker compose exec --user www-data app php occ config:app:set files max_chunk_size --value 52428800  # 50MB
# Config value max_chunk_size for app files set to 52428800

docker compose exec --user www-data app php occ config:app:get files max_chunk_size
```

### User Operation

Create normal user

```sh
# create group 'normal'
docker compose exec --user www-data app php occ group:add "${new_group}"
docker compose exec --user www-data app php occ group:info "${new_group}"
docker compose exec --user www-data app php occ group:list

# create normal user
# https://codereview.stackexchange.com/questions/216335/batch-upload-users-to-nextcloud-on-docker-with-bash-csv
docker compose exec --env OC_PASS="${new_user_pass}" --user www-data app php occ user:add --password-from-env --group="${new_group}" "${new_user}"  # quiet mode
# docker compose exec --user www-data app php occ user:add --group="${new_group}" "${new_user}"  # interactive mode

# check new user info
docker compose exec --user www-data app php occ user:info "${new_user}"

# clean trashbin
docker compose exec --user www-data app php occ trashbin:cleanup "${new_user}"
# docker compose exec --user www-data app php occ trashbin:cleanup --all-users
```

User management

```sh
# list all configured users
docker compose exec --user www-data app php occ user:list

# shows how many users have access
docker compose exec --user www-data app php occ user:report

# reset user password
docker compose exec --user www-data app php occ user:resetpassword <user>
```

### Apps Operation

Official apps store <https://apps.nextcloud.com>

Apps operation

```sh
# https://docs.nextcloud.com/server/latest/admin_manual/configuration_server/occ_command.html#apps-commands

# issue 'the input device is not a TTY'  # https://stackoverflow.com/questions/43099116/error-the-input-device-is-not-a-tty#57565119

# app needs to disable
apps_list_disable='weather_status accessibility survey_client support'
sed -r -n 's@[[:space:]]+@\n@g;p' <<< "${apps_list_disable}" | xargs -I %s docker compose exec -T --user www-data app php occ app:disable %s

# app needs to enable
apps_list_enable='files_external'
sed -r -n 's@[[:space:]]+@\n@g;p' <<< "${apps_list_enable}" | xargs -I %s docker compose exec -T --user www-data app php occ app:enable %s

# app needs to install
apps_list_install='files_markdown files_texteditor previewgenerator bruteforcesettings suspicious_login twofactor_totp groupfolders'
apps_list_install="${apps_list_install} metadata calendar contacts"  # optional

# files_markdown rely on files_texteditor
# twofactor_nextcloud_notification not work in Docker ?
sed -r -n 's@[[:space:]]+@\n@g;p' <<< "${apps_list_install}" | xargs -I %s docker compose exec -T --user www-data app php occ app:install %s
```

List installed apps

```sh
# list all installed apps
docker compose exec --user www-data app php occ app:list
```

Update installed apps

```sh
# update all installed apps
docker compose exec --user www-data app php occ app:update --all
```

### External Storage Mount

Mount external storage via Rclone (optional)

Execute under *root* or via `sudo -i`

```sh
mkdir -p /home/cloud_mount
chown -R 82:82 /home/cloud_mount
chmod -R 0755 /home/cloud_mount

# docker compose volume map
# - /home/cloud_mount:/mnt/cloud_mount:rw

# /mnt/cloud_mount/rw
# /mnt/cloud_mount/ro

fn_Rclone_Mount(){
    local l_remote="${1:-}"
    local l_local="${2:-}"
    local l_action="${3:-0}" # default is 0 rw, 1 is ro (readonly)
    local local_mount_dir='/home/cloud_mount'

    local l_mount_type='rw'
    [[ "${l_action}" -eq 1 ]] && l_mount_type='ro'
    local_mount_dir="${local_mount_dir}/${l_mount_type}"

    if [[ ! -d "${local_mount_dir}" ]]; then
        mkdir -p "${local_mount_dir}"
        chown -R 82:82 "${local_mount_dir}"
        chmod -R 0755 "${local_mount_dir}"
    fi

    local local_mount_point="${local_mount_dir}/${l_local}"

    if [[ ! -d "${local_mount_point}" ]]; then
        mkdir -p "${local_mount_point}"
        chown -R 82:82 "${local_mount_point}"
        chmod -R 0755 "${local_mount_point}"
    fi

    rclone mount ${l_remote} "${local_mount_point}" --daemon --umask=022 --gid 82 --uid 82 --checkers=16 --allow-other --allow-non-empty --cache-dir '/tmp/.rclone/nextdata/' --cache-info-age=60m --dir-cache-time=30m --vfs-cache-mode writes --vfs-cache-max-size=1G --vfs-cache-max-age='60m0s' --vfs-read-chunk-size='128M' --vfs-read-chunk-size-limit=off
}

# Usage
fn_Rclone_Mount 'Remote PATH' 'Local PATH'
```

Open **External storage** setting page (`Setting` -> `Administartion` -> `External Storage`).

`Add storage` choose *Local* from selection menu, `Configuration` fill the path configured in docker compose file, here is */mnt/cloud_mount* (path in container).

Output

```sh
# docker compose exec --user www-data app php occ files_external:list --output=json_pretty
```

If it shows nothing in web page, please restart contianer to make it works.


### Scan Files

```sh
# docker compose exec --user root app sh

# - clean redis cache
# if show file lock error which scan files, this action may solve it.
redis_password=${redis_password:-}
[[ -f .env ]] && redis_password=$(sed -r -n '/REDIS_HOST_PASSWORD=/{s@^[^=]+=@@g;p;q}' .env 2> /dev/null)
docker compose exec cache redis-cli -a "${redis_password}" -h 127.0.0.1 flushall

# - clean trashbin
docker compose exec --user www-data app php occ trashbin:cleanup --all-users

# docker compose exec --user www-data app php occ files:scan --all  # consume too much time for rclone remote

# only scan the home storage, ignoring any mounted external storage or share
docker compose exec --user www-data app php occ files:scan --home-only USER

# for mounted external storage, prefix path is '/USER/files/'
docker compose exec --user www-data app php occ files:scan --unscanned --path='/USER/files/Cloud/onedrive/Examples' USER


docker compose exec --user www-data app php occ preview:generate-all

# update installed apps
docker compose exec --user www-data app php occ app:update --all
# - repair
docker compose exec --user www-data app php occ maintenance:repair
```

Clear activities from database (post [Can I delete the activities list?](https://help.nextcloud.com/t/can-i-delete-the-activities-list/33879))

```sh
docker compose exec db_p sh

psql -U nextcloud nextcloud

# list database list
\l

# connect database 'nextcloud'
\c nextcloud
# You are now connected to database "nextcloud" as user "nextcloud".

# list tables
\d

# show table info
\d+ oc_activity

# show items count
select count(*) from oc_activity;

# delete action
delete from oc_activity;

# exit
exit
```

## Database Operation

### Database Dump & Import

#### Database Dump

https://help.nextcloud.com/t/how-to-upgrade-postgresql-from-v13-to-v14-or-v15/152102/5
https://openterprise.it/2023/05/nextcloud-upgrading-postgresql-database-running-as-docker-container-between-major-versions/


Official doc [Maintenance » Backup](https://docs.nextcloud.com/server/latest/admin_manual/maintenance/backup.html) lists how to backup a Nextcloud installation.

<details><summary>Click to expand commands</summary>

Change to docker compose service dir

```sh
# tutorial https://forum.openmediavault.org/index.php?thread/30925-nextcloud-docker-backup-with-db/

# docker compose exec --user www-data app  # here 'app' is nextcloud service name defined in docker compose file

sql_save_dir=${sql_save_dir:-"$PWD/data/sql"}
[[ -d "${sql_save_dir}" ]] || mkdir -p "${sql_save_dir}"

# https://docs.nextcloud.com/server/latest/admin_manual/maintenance/backup.html#maintenance-mode
# 'maintenance:mode' locks the sessions of logged-in users and prevents new logins in order to prevent inconsistencies of your data.
docker compose exec --user www-data app php occ maintenance:mode --on  # maintenance mode on

# config path in container: /var/www/html/config/config.php
nextcloud_db_info=$(docker compose exec --user www-data app php -r 'include("/var/www/html/config/config.php"); print $CONFIG["dbtype"]."|".$CONFIG["dbhost"]."|".$CONFIG["dbname"]."|".$CONFIG["dbuser"]."|".$CONFIG["dbpassword"];')
# note: docker compose v2.2.3 (Jan 06, 2022) make this command panic https://github.com/docker/compose/releases/tag/v2.2.3

# dbhost is name defined in docker compose file
# dbtype|dbhost|dbname|dbuser|dbpassword
# pgsql|db_p|nextcloud|oc_admin|XXXXXXXXXXXXXX

db_type=$(cut -d\| -f1 <<< "${nextcloud_db_info}")
db_host=$(cut -d\| -f2 <<< "${nextcloud_db_info}") # this is database service name defined in docker compose file

db_name=$(cut -d\| -f3 <<< "${nextcloud_db_info}")
db_pass=$(cut -d\| -f5 <<< "${nextcloud_db_info}")

db_user=$(cut -d\| -f4 <<< "${nextcloud_db_info}") # here 'oc_admin' isn't the exact user name, find it from file '.env'
db_user_search='POSTGRES_USER'
[[ "${db_type}" != 'pgsql' ]] && db_user_search='MYSQL_USER'
[[ "${db_user}" == 'oc_admin' && -f .env ]] && db_user=$(sed -r -n '/^'"${db_user_search}"'=/{s@^[^=]+=@@g;s@[[:space:]]*$@@g;p;q}' .env 2> /dev/null)
[[ -z "${db_user}" ]] && db_user='nextcloud'


sql_save_path="${sql_save_dir}/nextcloud_${db_type}_dump_$(date +'%Y%m%d_%H%M%S').sql"

case "${db_type}" in
    pgsql)
        # clean table oc_activity items
        docker compose exec "${db_host}" sh -c 'PGPASSWORD="'${db_pass}'" psql "'${db_name}'" -h localhost -U "'${db_user}'" -c "delete from oc_activity;select count(*) from oc_activity"' 1> /dev/null # "select count(*) from oc_activity"

        # pg_dump path in container is /usr/local/bin/pg_dump
        # PGPASSWORD="password" pg_dump [db_name] -h [server] -U [username] -f nextcloud-sqlbkp_`date +"%Y%m%d"`.bak  # https://docs.nextcloud.com/server/latest/admin_manual/maintenance/backup.html#postgresql
        docker compose exec "${db_host}" sh -c 'PGPASSWORD="'${db_pass}'" pg_dump "'${db_name}'" -h localhost -U "'${db_user}'"' > "${sql_save_path}"
    ;;
    mysql)
        # MySQL / MariaDB
        echo ''
        # mysqldump --single-transaction -h [server] -u [username] -p[password] [db_name] > nextcloud-sqlbkp_`date +"%Y%m%d"`.bak
        # mysqldump --single-transaction --default-character-set=utf8mb4 -h [server] -u [username] -p[password] [db_name] > nextcloud-sqlbkp_`date +"%Y%m%d"`.bak
    ;;
esac

docker compose exec --user www-data app php occ maintenance:mode --off  # maintenance mode off


# just keep latest 3 versions
# find "${sql_save_path%/*}" -type f -size +2k -name '*.sql' -print | sort -r | sed '1,3d' | xargs -I %s rm -f %s
```

</details>

#### Database Import

Official doc [Maintenance » Restoring backup](https://docs.nextcloud.com/server/latest/admin_manual/maintenance/restore.html)


<details><summary>Click to expand commands</summary>

```sh
# https://docs.nextcloud.com/server/latest/admin_manual/maintenance/restore.html
# tutorial https://forum.openmediavault.org/index.php?thread/30925-nextcloud-docker-backup-with-db/

# config path in container: /var/www/html/config/config.php
nextcloud_db_info=$(docker compose exec --user www-data app php -r 'include("/var/www/html/config/config.php"); print $CONFIG["dbtype"]."|".$CONFIG["dbhost"]."|".$CONFIG["dbname"]."|".$CONFIG["dbuser"]."|".$CONFIG["dbpassword"];')
# note: docker compose v2.2.3 (Jan 06, 2022) make this command panic https://github.com/docker/compose/releases/tag/v2.2.3
# pgsql|db_p|nextcloud|oc_admin|XXXXXXXXXXXXXXXXXXXXXXXXXX

# dbhost is name defined in docker compose file
# dbtype|dbhost|dbname|dbuser|dbpassword
# pgsql|db_p|nextcloud|oc_admin|XXXXXXXXXXXXXX

db_type=$(cut -d\| -f1 <<< "${nextcloud_db_info}")
db_host=$(cut -d\| -f2 <<< "${nextcloud_db_info}") # this is database service name defined in docker compose file

db_name=$(cut -d\| -f3 <<< "${nextcloud_db_info}")
db_pass=$(cut -d\| -f5 <<< "${nextcloud_db_info}")

db_user=$(cut -d\| -f4 <<< "${nextcloud_db_info}") # here 'oc_admin' isn't the exact user name, find it from file '.env'
db_user_search='POSTGRES_USER'
[[ "${db_type}" != 'pgsql' ]] && db_user_search='MYSQL_USER'
[[ "${db_user}" == 'oc_admin' && -f .env ]] && db_user=$(sed -r -n '/^'"${db_user_search}"'=/{s@^[^=]+=@@g;s@[[:space:]]*$@@g;p;q}' .env 2> /dev/null)
[[ -z "${db_user}" ]] && db_user='nextcloud'

# sql_restore_path="$PWD/sql/nextcloud_pgsql_dump.sql"
# sudo cp -p "${sql_restore_path}" /data/nextcloud/db/

# move backup sql to directory './sql/'
# get the latest backup
sql_restore_path=$(find $PWD/sql/ -mindepth 1 -maxdepth 1 -type f -name 'pgsql_dump_*.sql' -print 2> /dev/null | tail -n 1)

case "${db_type}" in
    pgsql)
        # psql path in container is /usr/local/bin/psql

        # Re-create database
        # database 'template1' exists, specify it to solve issue 'ERROR:  cannot drop the currently open database'
        docker compose exec "${db_host}" sh -c 'PGPASSWORD="'${db_pass}'" psql -h localhost -U "'${db_user}'" -d template1 -c "DROP DATABASE \""'${db_name}'"\";"'
        docker compose exec "${db_host}" sh -c 'PGPASSWORD="'${db_pass}'" psql -h localhost -U "'${db_user}'" -d template1 -c "CREATE DATABASE \""'${db_name}'"\";"'

        # Restoring database data
        # put sql file into path '/data/nextcloud/db' in host which map to path '/var/lib/postgresql/data/' in container, defined in docker-compose.yml
        sql_name="${sql_restore_path##*/}"
        docker compose exec "${db_host}" sh -c 'PGPASSWORD="'${db_pass}'" psql -h localhost -U "'${db_user}'" -d "'${db_name}'" -f /var/lib/postgresql/data/sql/'"${sql_name}"''
    ;;
    mysql)
        # MySQL / MariaDB
        echo ''
        # mysql -h [server] -u [username] -p[password] -e "DROP DATABASE nextcloud"
        # mysql -h [server] -u [username] -p[password] -e "CREATE DATABASE nextcloud"
        # mysql -h [server] -u [username] -p[password] [db_name] < nextcloud-sqlbkp.bak
    ;;
esac
```

</details>


If you fail to login use generated totp code, you need to use your backup code to login, then re-enable totp.

```sh
# echo 'twofactor_totp' | xargs -I %s docker compose exec -T --user www-data app php occ app:disable %s
# echo 'twofactor_totp' | xargs -I %s docker compose exec -T --user www-data app php occ app:enable %s
```

```sh
docker compose exec --user www-data app php occ files:scan --home-only --all
```

If prompts error

>Home storage for user XXXX not writable

You need to login every affected users, clean not existed files and directories, then retry.


### PostgreSQL Major Version Upgrade

Major version upgrade

<details><summary>Click to expand</summary>

```sh
# source: https://gabnotes.org/upgrade-postgres-docker/

container_name="db_p"
db_user="nextcloud"
db_name="nextcloud"

# - Run database container
docker compose down --remove-orphans
docker compose up -d $container_name
# docker compose logs -ft

# - Dump DB & roles
mkdir -p db_export
# docker compose exec -it $container_name pg_dumpall -U $db_user > ./db_export/data_backup.sql
docker compose exec -it $container_name \
  pg_dump -Fc -U $db_user $db_name > ./db_export/backup.dump
docker compose exec -it $container_name \
  pg_dumpall --globals-only -U $db_user > ./db_export/roles.sql

# - Shutdown all containers
docker compose down --remove-orphans

# - Move current data dir, don't remove it yet (/data/synapse/db)
cp -p -R /data/synapse/db{,_old}

# - Edit docker-compose.yaml - upgrade psql
vim docker-compose.yaml
# image: postgres:15-alpine 
# /data/synapse/db_15:/var/lib/postgresql/data

# - Check version
docker compose up -d $container_name
docker compose exec -it $container_name psql -U $db_user -c 'select version();'

# - Restore data
docker compose cp ./db_export/roles.sql $container_name:/tmp/roles
docker compose exec -it $container_name psql -U $db_user -d $db_name -f /tmp/roles

docker compose cp ./db_export/backup.dump $container_name:/tmp/backup
docker compose exec -it $container_name pg_restore -U $db_user -d $db_name /tmp/backup

# docker compose cp ./db_export/data_backup.sql $container_name:/tmp/backup_data
# docker compose exec -it $container_name pg_restore -U $db_user -d $db_name /tmp/backup_data


# - Change password (Must) Re-input the old password
# db_user:nextcloud, db_name:nextcloud
docker compose exec $container_name sh
psql -U nextcloud nextcloud
\password

# db_user:oc_admin, db_name:nextcloud
# FATAL:  password authentication failed for user "oc_admin"
docker compose exec --user www-data app php -r 'include("/var/www/html/config/config.php"); print $CONFIG["dbtype"]."|".$CONFIG["dbhost"]."|".$CONFIG["dbname"]."|".$CONFIG["dbuser"]."|".$CONFIG["dbpassword"];'  # dbtype|dbhost|dbname|dbuser|dbpassword
docker compose exec $container_name sh
psql -U oc_admin nextcloud   # $db_user $db_name
\password


# - Verify
docker compose exec -it $container_name psql -U $db_user -d $db_name -c "\dt"

# Launch app again
docker compose up -d
docker compose logs -ft

# - Shutdown all containers
docker compose down --remove-orphans
```

</details>


## Issues Occuring

### Database initialise fail

>Error while trying to initialise the database: An exception occurred while executing a query: SQLSTATE[HY000]: General error: 4047 InnoDB refuses to write tables with ROW_FORMAT=COMPRESSED or KEY_BLOCK_SIZE.

Solution: follow post [New Setup Docker Compose not working](https://help.nextcloud.com/t/new-setup-docker-compose-not-working/115673/9), add parameter *--innodb-read-only-compressed=OFF* in file `docker-compose.yml`.


### Database Insufficient privilege

When update the app **groupfolders**, the system prompts error

>Error: Database error when running migration 19000Date20241029123147 for app groupfolders
>
>An exception occurred while executing a query: SQLSTATE[42501]: Insufficient privilege: 7 ERROR:  permission denied for schema public

Try to install via command line

```sh
docker compose exec --user www-data app php occ app:install groupfolders
```

Failed again.

Database is `postgres:15-alpine`

Similar issue [34617](https://github.com/nextcloud/server/issues/34617#issuecomment-1279257822)

Connect to the database

```sh
docker compose exec db_p sh

psql -U nextcloud nextcloud
```

Then check schema permission

```sql
nextcloud=# \dn+
                                       List of schemas
  Name  |       Owner       |           Access privileges            |      Description
--------+-------------------+----------------------------------------+------------------------
 public | pg_database_owner | pg_database_owner=UC/pg_database_owner+| standard public schema
        |                   | =U/pg_database_owner                   |
(1 row)
```

Following post [PostgreSQL中public schema的权限和安全](https://www.cnblogs.com/abclife/p/16410435.html)

```sql
nextcloud=# grant create on schema public to public;
GRANT
nextcloud=# \dn+
                                       List of schemas
  Name  |       Owner       |           Access privileges            |      Description
--------+-------------------+----------------------------------------+------------------------
 public | pg_database_owner | pg_database_owner=UC/pg_database_owner+| standard public schema
        |                   | =UC/pg_database_owner                  |
(1 row)

```

Then try to install again, it installed successfully.

```sh
# docker compose exec --user www-data app php occ app:install groupfolders
groupfolders 18.0.9 installed
groupfolders enabled
```

Revoke the **PUBLIC** creation permission on the public schema again.

```sql
nextcloud=# \dn+
                                       List of schemas
  Name  |       Owner       |           Access privileges            |      Description
--------+-------------------+----------------------------------------+------------------------
 public | pg_database_owner | pg_database_owner=UC/pg_database_owner+| standard public schema
        |                   | =UC/pg_database_owner                  |
(1 row)

nextcloud=# revoke create on schema public from public;
REVOKE
nextcloud=# \dn+
                                       List of schemas
  Name  |       Owner       |           Access privileges            |      Description
--------+-------------------+----------------------------------------+------------------------
 public | pg_database_owner | pg_database_owner=UC/pg_database_owner+| standard public schema
        |                   | =U/pg_database_owner                   |
(1 row)

nextcloud=#
```

### apps dir permission

Specifing custom path for *custom_apps* (default is */var/www/html/custom_apps*) in volume setting. Pormpt error

>Cannot write into "apps" directory
>
>This can usually be fixed by giving the webserver write access to the apps directory or disabling the App Store in the config file.

Solution is change the owner permission for the directory.

```sh
cd /data/nextcloud
chown -R 82:root custom_apps/
```

### Access through untrusted domain

>Please contact your administrator. If you are an administrator, edit the "trusted_domains" setting in config/config.php like the example in config.sample.php.
>
>Further information how to configure this can be found in the [documentation](https://docs.nextcloud.com/server/22/go.php?to=admin-trusted-domains).

Solution: remove prefix `https://` from parameter *NEXTCLOUD_TRUSTED_DOMAINS* setting in file *.env*.


### Icons Missing

Note: this issue wasted me about 2 days.

Issue like post [Icons in the front-end are not showing](https://help.nextcloud.com/t/icons-in-the-front-end-are-not-showing/98496) which point to Github project issue [Icons not showing/transparent #11127](https://github.com/nextcloud/server/issues/11127).


Similar issue

* [Referrer-Policy and Content-Security-Policy broken on Nginx-only setup](https://help.nextcloud.com/t/solved-referrer-policy-and-content-security-policy-broken-on-nginx-only-setup/49212)

Try to follow post [Content Security Policy Config](https://help.nextcloud.com/t/content-security-policy-config/28146/3)

```sh
find -xdev -type f -name "*.php" -exec grep --with-filename "unsafe-inline" {} \;
```

Find file *./lib/public/AppFramework/Http/ContentSecurityPolicy.php* (Github path [ContentSecurityPolicy.php](https://github.com/nextcloud/server/blob/master/lib/public/AppFramework/Http/ContentSecurityPolicy.php
) and developer doc [Modifying the content security policy](https://docs.nextcloud.com/server/latest/developer_manual/basics/controllers.html#modifying-the-content-security-policy)). But still don't konw how to do it.

Solution is add `Content-Security-Policy` into Nginx config solved.

```conf
# https://docs.nextcloud.com/server/latest/admin_manual/configuration_files/big_file_upload_configuration.html

add_header Content-Security-Policy "default-src 'self';frame-ancestors 'self';style-src 'self' 'unsafe-inline';script-src 'self' 'unsafe-inline' ;img-src 'self' data:;font-src 'self' data:";
```

### Background Jobs Selector

Official doc [Background jobs](https://docs.nextcloud.com/server/latest/admin_manual/configuration_server/background_jobs_configuration.html)

Default is *AJAX* relay on page load, but the recommended method is to use *cron*.

```sh
# https://docs.nextcloud.com/server/latest/admin_manual/configuration_server/occ_command.html#background-jobs-selector

# - change mode to 'ajax' (doesn't work for docker compose ?)
docker compose exec --user www-data app php occ background:ajax
# Set mode for background jobs to 'ajax'

# - manually execute
docker compose exec --user www-data app php -f /var/www/html/cron.php
```

For container, it has issue like

* [NC17 php-fpm:alpine. Cron.php not properly running when using cron.sh as entrypoint](https://github.com/nextcloud/docker/issues/914)
* [[NC20 running with docker-compose] Cron container is not working](https://help.nextcloud.com/t/nc20-running-with-docker-compose-cron-container-is-not-working/97637)

Similar question

* [Docker setup & cron](https://help.nextcloud.com/t/docker-setup-cron/78547)

To automatically execute cron task, the simplest solution is add service *cron* into file *docker-compose.yml* ([sample](https://github.com/nextcloud/docker/blob/master/.examples/docker-compose/insecure/postgres/fpm/docker-compose.yml#L40)). Attention: must keep the volume settings same to service *app*.


### Too Many Requests

>Too many requests
>
>There were too many requests from your network. Retry later or contact your administrator if this is an error.

Issue like [post](https://help.nextcloud.com/t/cannot-login-too-many-requests/100905)

Solution

```sh
docker compose exec --user www-data app php occ security:bruteforce:reset <ip> # 172.29.0.1
```

Connect service `db-p` to delete data from database

```sh
docker compose exec db_p sh

psql -U nextcloud nextcloud

# list schema permission
# \dn+
# SELECT schema_name FROM information_schema.schemata;

# list database list
\l

# connect database 'nextcloud'
\c nextcloud
# You are now connected to database "nextcloud" as user "nextcloud".

# list tables
\d

# show table info
\d+ oc_bruteforce_attempts

# select action
select * from oc_bruteforce_attempts;

#  id | action |  occurred  |     ip     |    subnet     |     metadata
# ----+--------+------------+------------+---------------+------------------
#   1 | login  | 1636403177 | 172.29.0.1 | 172.29.0.1/32 | {"user":"admin"}

select id,ip from oc_bruteforce_attempts;

#  id |     ip
# ----+------------
#   1 | 172.29.0.1

# delete action
delete from oc_bruteforce_attempts;

# exit
exit
```

### External Storage

Note: this issue wasted me about 2 days.

External storage enables you to mount external storage services and devices as secondary Nextcloud storage devices. You may also allow users to mount their own external storage services.

Official doc [Configuring External Storage (GUI)](https://docs.nextcloud.com/server/latest/admin_manual/configuration_files/external_storage_configuration_gui.html).


Similar Issues

* [Unable to add rclone mount as local external storage](https://help.nextcloud.com/t/unable-to-add-rclone-mount-as-local-external-storage/122875)
* [Rclone mount as external storage for Nextcloud?](https://forum.rclone.org/t/rclone-mount-as-external-storage-for-nextcloud/6465)
* [Github - local external storage not working: You don't have permission to upload or create files here](https://github.com/nextcloud/docker/issues/229)


> "smbclient" is not installed. Mounting of "SMB/CIFS", "SMB/CIFS using OC login" is not possible. Please ask your system administrator to install it.

<details><summary>Click to expand smbclient installation</summary>

```sh
# - For fpm-alpine based on Alpine Linux
# https://pkgs.alpinelinux.org/package/edge/main/x86_64/samba-client
docker compose exec --user root app sh -c 'apk add --no-cache samba-client'

# - For apache/fpm based on Debian
# docker compose exec --user root app sh -c 'apt-get update && apt-get -y install smbclient && rm -rf /var/lib/apt/lists/*'
```

</details>

> No external storage configured or you don't have the permission to configure them

Solution see post [External storage in Docker](https://help.nextcloud.com/t/external-storage-in-docker/108145/2).

Attention: user `www-data` and group `www-data` has differents UID/GID on different GNU/Linux distributions.

```sh
# On Debian
#www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin  # /etc/passwd
#www-data:x:33:  # /etc/group

# On Alpine Linux
#www-data:x:82:82:Linux User,,,:/home/www-data:/sbin/nologin  # /etc/passwd
#www-data:x:82:  # /etc/group
```

The problem is my host system is *Debian*, but the container system is *Alpine*. It shows uid as `82` on my host.

```sh
root@debian:/data/nextcloud# ls -lh
total 24K
drwxrwx---  5   82 root 4.0K Nov  8 09:27 data
drwxr-xr-x 14   82 root 4.0K Nov  8 10:58 html
```

Solution is grant target directory ower permission UID `82`, GID `82` to make it can be read by contianer.

```sh
external_mount_path='/mnt/extenral_storage'

mkdir -p "${external_mount_path}"
chown -R 82:82 "${external_mount_path}"
chmod -R 0755 "${external_mount_path}"
```


## Tutotials

* [Nextcloud forum post from user wwe](https://help.nextcloud.com/t/totally-baffled-how-to-get-started-newbie-alert/112181/24)
* [The optimal & fastest Nextcloud-FPM docker setup with Caddy as webserver and https-proxy](https://help.nextcloud.com/t/the-optimal-fastest-nextcloud-fpm-docker-setup-with-caddy-as-webserver-and-https-proxy/110515)
* [Mount Google Drive into NextCloud Using Rclone](https://blog.51sec.org/2020/12/mount-google-drive-into-nextcloud.html)
* [rclone+Google Drive配合docker部署nextcloud](https://blog.stsecurity.moe/index.php/archives/216/)


## Bibliography

* [Nextcloud Administrator Guide](https://support.websoft9.com/docs/nextcloud/)


## Change Log

* Nov 21, 2024 11:30 Thu CST
  * Add postgresql major version upgrade
* Jan 04, 2022 14:00 Tue ET
  * Add database dump and import action
* Nov 25, 2021 11:53 Thu ET
  * Add thumbnail preview configuration
* Nov 03, 2021 19:10 Wed ~ Nov 09, 2021 14:08 Tue ET
  * First draft


[nextcloud]:https://nextcloud.com "Nextcloud is the most deployed on-premises file share and collaboration platform."


<!-- End -->