# Self-hosting Jellyfin

[Jellyfin][jellyfin] is a Free Software Media System that puts you in control of managing and streaming your media. It is an alternative to the proprietary Emby and Plex, to provide media from a dedicated server to end-user devices via multiple apps. [Jellyfin][jellyfin] is descended from Emby's 3.5.2 release and ported to the .NET Core framework to enable full cross-platform support. There are no strings attached, no premium licenses or features, and no hidden agendas: just a team who want to build something better and work together to achieve it.

## TOC

1. [Official Site](#official-site)  
2. [Deployment](#deployment)  
2.1 [Variables Definition](#variables-definition)  
2.2 [env](#env)  
2.3 [docker-compose.yml](#docker-composeyml)  
3. [Deployment](#deployment-1)  
3.1 [Running Container](#running-container)  
3.2 [Nginx Reverse Proxy](#nginx-reverse-proxy)  
3.2.1 [Conf](#conf)  
4. [Bibliography](#bibliography)  
5. [Change Log](#change-log)  


## Official Site

Item|Url
---|---
Official Site|https://jellyfin.org
Official Doc|https://jellyfin.org/docs/
GitHub|https://github.com/jellyfin/jellyfin
Docker Hub|https://hub.docker.com/r/jellyfin/jellyfin/

LinuxServer

Item|Url
---|---
Official site|https://www.linuxserver.io
Official doc|https://docs.linuxserver.io/images/docker-jellyfin
GitHub|https://github.com/linuxserver/docker-jellyfin
Docker|https://hub.docker.com/r/linuxserver/jellyfin


## Deployment

### Variables Definition

```sh
service_name='jellyfin'
service_save_path="$HOME/.docker/${service_name}"
mkdir -p "${service_save_path}"

data_dir="/data/"${service_name}""
config_save_dir="${data_dir}/config"

localhost_ip='127.0.0.1'
domain_for_server="${service_name%%-*}.maxdsre.com"
exposted_http_port='8016'  # default 8096
```

### env

File `.env`

<details><summary>Click to expand env conf</summary>

```sh
tee "${service_save_path}"/.env 1> /dev/null << EOF
# https://github.com/linuxserver/docker-emby/blob/master/README.md#parameters

PUID=1000  # id -u
PGID=1000  # id -g
TZ=$(cat /etc/timezone)  # America/New_York from file /etc/timezone

# JELLYFIN_PublishedServerUrl=  # Set the autodiscovery response domain or IP address.

EXPOSED_PORT=${localhost_ip}:${exposted_http_port}
PROXY_DOMAIN=${domain_for_server}

# - Optional

# End
EOF
```

</details>

### docker-compose.yml

```sh
cd "${service_save_path}"

tee docker-compose.yml 1> /dev/null << EOF
version: '3.8'

services:
  jellyfin:
    image: linuxserver/jellyfin:latest
    container_name: jellyfin-server
    restart: unless-stopped
    networks:
      - jellyfin_server_network
    ports:
      - \${EXPOSED_PORT}:8096 # http webUI
      #- 8920:8920 #optional, https webUI
      #- 7359:7359/udp # optional, allow clients to discover Jellyfin on the local network.
      #- 1900:1900/udp # optional, service discovery used by DNLA and clients
    volumes:
      - ${data_dir}/config:/config
      #- /path/to/movies:/data/movies # add as many as needed
      # - /opt/vc/lib:/opt/vc/lib #optional, for Raspberry Pi OpenMAX libs
      # - /etc/localtime:/etc/localtime:ro
    env_file: .env
    # devices:
    #   - /dev/dri:/dev/dri #optional
    #   - /dev/vchiq:/dev/vchiq #optional
    #   - /dev/video10:/dev/video10 #optional
    #   - /dev/video11:/dev/video11 #optional
    #   - /dev/video12:/dev/video12 #optional

networks:
  jellyfin_server_network:
    name: jellyfin_server_network

# End
EOF
```

### Running Container

```sh
cd "${service_save_path}"

# - Via docker compose (For docker-compose V2)
docker compose up -d
# docker compose --env-file "${service_save_path}/docker-compose.yml" up -d
docker compose ps
docker compose down --remove-orphans

# - Via docker-compose (For docker-compose V1)
docker-compose up -d
# docker-compose -f "${service_save_path}/docker-compose.yml" up -d
docker-compose ps
# docker-compose down --remove-orphans
```


### Nginx Reverse Proxy

Nginx

```sh
# service_name='jellyfin'
# domain_for_server="${service_name%%-*}.maxdsre.com"
# exposted_http_port='8016'  # default 8096

backend_private_ip=${backend_private_ip:-'127.0.0.1'}
# backend_private_ip='192.168.8.4'

# Nginx site config dir /etc/nginx/sites-enabled/
nginx_conf_path="/etc/nginx/sites-enabled/${service_name}.conf"
sudo mkdir -p "${nginx_conf_path%/*}"
```

#### Conf

<details><summary>Click to expand config info</summary>

```conf
sudo tee "${nginx_conf_path}" 1> /dev/null << EOF
upstream ${service_name}-default {
    server ${backend_private_ip}:${exposted_http_port};
}

map \$http_upgrade \$connection_upgrade {
    default upgrade;
    '' close;
}

# - For Sync Server
# Redirect HTTP to HTTPS
server {
    listen 80;
    # listen [::]:80 ipv6only=on;
    server_name ${domain_for_server};
    return 301 https://\$host\$request_uri;
}

server {
    listen 443 ssl http2;
    # listen [::]:443 ssl http2 ipv6only=on;
    server_name ${domain_for_server};

    error_log  /var/log/nginx/${domain_for_server%%.*}_error.log warn;  # [debug|info|notice|warn|error|crit|alert|emerg]
    access_log /var/log/nginx/${domain_for_server%%.*}_access.log main; # format name 'main' instead of predefined 'combined' format

    include /etc/nginx/conf.d/section/ssl.conf;

    location / {
        #proxy_set_header Host \$host;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto \$scheme;

        # http://nginx.org/en/docs/http/ngx_http_proxy_module.html#proxy_http_version
        proxy_http_version 1.1;
        proxy_set_header Upgrade \$http_upgrade;
        proxy_set_header Connection \$connection_upgrade;
        proxy_set_header Host \$host;
        proxy_set_header Accept-Encoding gzip;

        # https://serverfault.com/questions/587386/an-upstream-response-is-buffered-to-a-temporary-file
        # https://www.cyberciti.biz/faq/nginx-upstream-sent-too-big-header-while-reading-response-header-from-upstream/
        proxy_buffering off;
        #proxy_buffers   4 512k;
        #proxy_buffer_size   256k;
        #proxy_busy_buffers_size   512k;

        # https://emby.media/community/index.php?/topic/80952-nginx-reverse-proxy-config-for-emby-website-with-ssl/
        add_header Content-Security-Policy "default-src 'none'; child-src 'self'; font-src 'self' data: https://cdn.ss-cdn.com/assets/webapp-fonts/; connect-src 'self' wss: ws:; media-src 'self' blob: data:; manifest-src 'self'; base-uri 'none'; form-action 'self'; frame-ancestors 'self'; object-src 'none'; worker-src 'self' blob:; script-src 'self' 'unsafe-inline' https://www.gstatic.com; img-src data: https: http: ; style-src 'unsafe-inline' 'self' https://fonts.googleapis.com/css" always;

        proxy_pass http://${service_name}-default;
    }
}

# End
EOF
```

</details>

Testing Nginx conf syntax then reload

```sh
sudo -i

nginx -t && nginx -s reload

# nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
# nginx: configuration file /etc/nginx/nginx.conf test is successful
```

## Bibliography

* [GitHub - Compare Media Servers](https://github.com/Protektor-Desura/Archon/wiki/Compare-Media-Servers)
* [Reddit - Updated Comparison of Jellyfin vs Emby and Plex?](https://www.reddit.com/r/jellyfin/comments/mvcgmw/updated_comparison_of_jellyfin_vs_emby_and_plex/)


## Change Log

* Mar 08, 2022 12:18 Tue ET
  * First draft


[jellyfin]:https://jellyfin.org "Jellyfin: The Free Software Media System"


<!-- End -->