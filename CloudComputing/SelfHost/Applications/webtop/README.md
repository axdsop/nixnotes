# Self-hosting Webtop


[Apache Guacamole][apache_guacamole] is a clientless remote desktop gateway. It supports standard protocols like VNC, RDP, and SSH.

[Webtop][webtop] - Alpine, Ubuntu, Fedora, and Arch based containers containing full desktop environments in officially supported flavors accessible via any modern web browser.


## Official Site

LinuxServer

Item|Url
---|---
Official site|https://www.linuxserver.io
Official doc|https://docs.linuxserver.io/images/docker-webtop
GitHub|https://github.com/linuxserver/docker-webtop
Docker|https://hub.docker.com/r/linuxserver/webtop


## Deployment

By default the user/pass is **abc**/**abc**.

### Configs

Variables Definition

```sh
service_name='webtop'
service_save_path="$HOME/.docker/${service_name}"
mkdir -p "${service_save_path}"

data_dir="/data/"${service_name}""
config_save_dir="${data_dir}/config"

localhost_ip='127.0.0.1'
domain_for_server="${service_name%%-*}.maxdsre.com"
exposted_http_port='8005'  # default 3000
```

#### .env

File `.env`

<details><summary>Click to expand env conf</summary>

```sh
tee "${service_save_path}"/.env 1> /dev/null << EOF
# https://github.com/linuxserver/docker-webtop#parameters

PUID=1000  # id -u
PGID=1000  # id -g
TZ=$(cat /etc/timezone)  # America/New_York from file /etc/timezone

AUTO_LOGIN=false
KEYBOARD=en-us-qwerty

EXPOSED_PORT=${localhost_ip}:${exposted_http_port}
#PROXY_DOMAIN=${domain_for_server}

# - Optional
UMASK=022
SUBFOLDER=/
TITLE=Webtop

# End
EOF
```

</details>

#### docker-compose.yml

>Modern GUI desktop apps (including some flavors terminals) have issues with the latest Docker and syscall compatibility, you can use Docker with the `--security-opt seccomp=unconfined` setting to allow these syscalls or try podman as they have updated their codebase to support them

```sh
cd "${service_save_path}"

tee docker-compose.yml 1> /dev/null << EOF
version: '3.8'

services:
  webtop:
    # tag list https://github.com/linuxserver/docker-webtop#version-tags
    image: linuxserver/webtop:latest  # XFCE Alpine
    container_name: webtop-server
    restart: unless-stopped
    security_opt:
      - seccomp:unconfined  #optional
    networks:
      - webtop_server_network
    ports:
      - \${EXPOSED_PORT}:3000  # http webUI
    volumes:
      - ${data_dir}/config:/config
      # - /var/run/docker.sock:/var/run/docker.sock #optional
      # - /etc/localtime:/etc/localtime:ro
    security_opt:
      - seccomp:unconfined #optional For Docker Engine only, many modern gui apps need this to function as syscalls are unkown to Docker.
    env_file: .env
    # devices:
    #   - /dev/dri:/dev/dri #optional
    #   - /dev/vchiq:/dev/vchiq #optional
    #   - /dev/video10:/dev/video10 #optional
    #   - /dev/video11:/dev/video11 #optional
    #   - /dev/video12:/dev/video12 #optional
    shm_size: "1gb" #optional

networks:
  webtop_server_network:
    name: webtop_server_network

# End
EOF
```

### Running Container

```sh
cd "${service_save_path}"

# - Via docker compose (For docker-compose V2)
docker compose up -d
# docker compose --env-file "${service_save_path}/docker-compose.yml" up -d
docker compose ps
docker compose down --remove-orphans

# - Via docker-compose (For docker-compose V1)
docker-compose up -d
# docker-compose -f "${service_save_path}/docker-compose.yml" up -d
docker-compose ps
# docker-compose down --remove-orphans
```


### Nginx Reverse Proxy

Nginx

```sh
service_name='webtop'
domain_for_server="${service_name%%-*}.maxdsre.com"
exposted_http_port='8005'  # default 3000

backend_private_ip=${backend_private_ip:-'127.0.0.1'}
# backend_private_ip='192.168.8.4'

# Nginx site config dir /etc/nginx/sites-enabled/
nginx_conf_path="/etc/nginx/sites-enabled/${service_name}.conf"
sudo mkdir -p "${nginx_conf_path%/*}"
```

#### Conf

Nginc config samle can find from froum post [NGINX Reverse Proxy Config for Emby + Website With SSL](https://emby.media/community/index.php?/topic/80952-nginx-reverse-proxy-config-for-emby-website-with-ssl/)


<details><summary>Click to expand config info</summary>

```conf
sudo tee "${nginx_conf_path}" 1> /dev/null << EOF
upstream ${service_name}-default {
    server ${backend_private_ip}:${exposted_http_port};
}

map \$http_upgrade \$connection_upgrade {
    default upgrade;
    '' close;
}

# - For Sync Server
# Redirect HTTP to HTTPS
server {
    listen 80;
    # listen [::]:80 ipv6only=on;
    server_name ${domain_for_server};
    return 301 https://\$host\$request_uri;
}

server {
    listen 443 ssl http2;
    # listen [::]:443 ssl http2 ipv6only=on;
    server_name ${domain_for_server};

    error_log  /var/log/nginx/${domain_for_server%%.*}_error.log warn;  # [debug|info|notice|warn|error|crit|alert|emerg]
    access_log /var/log/nginx/${domain_for_server%%.*}_access.log main; # format name 'main' instead of predefined 'combined' format

    include /etc/nginx/conf.d/section/ssl.conf;

    location / {
        #proxy_set_header Host \$host;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto \$scheme;

        # http://nginx.org/en/docs/http/ngx_http_proxy_module.html#proxy_http_version
        proxy_http_version 1.1;
        proxy_set_header Upgrade \$http_upgrade;
        proxy_set_header Connection \$connection_upgrade;
        proxy_set_header Host \$host;
        proxy_set_header Accept-Encoding gzip;

        # https://serverfault.com/questions/587386/an-upstream-response-is-buffered-to-a-temporary-file
        # https://www.cyberciti.biz/faq/nginx-upstream-sent-too-big-header-while-reading-response-header-from-upstream/
        proxy_buffering off;
        #proxy_buffers   4 512k;
        #proxy_buffer_size   256k;
        #proxy_busy_buffers_size   512k;

        add_header Content-Security-Policy "default-src 'none'; child-src 'self'; font-src 'self'; connect-src 'self' wss: ws:; manifest-src 'self'; base-uri 'none'; form-action 'self'; frame-src 'self'; frame-ancestors 'self'; object-src 'none'; worker-src 'self' blob:; script-src 'self'; img-src data: https: http: ; style-src 'unsafe-inline' 'self'" always;

        proxy_pass http://${service_name}-default;
    }
}

# End
EOF
```

</details>

Testing Nginx conf syntax then reload

```sh
sudo -i

nginx -t && nginx -s reload

# nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
# nginx: configuration file /etc/nginx/nginx.conf test is successful
```

## Change Log

* May 25, 2022 10:55 Wed ET
  * First draft


[apache_guacamole]:https://guacamole.apache.org
[webtop]:https://github.com/linuxserver/docker-webtop


<!-- End -->