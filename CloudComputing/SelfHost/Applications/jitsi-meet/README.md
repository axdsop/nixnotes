# Self-hosting Jitsi Meet

[Jitsi][jitsi] is a set of Open Source projects that allows you to easily build and deploy secure videoconferencing solutions.

[Jitsi Meet][jitsi_meet] is a fully encrypted, open source WebRTC JavaScript application that uses [Jitsi Videobridge](https://jitsi.org/jitsi-videobridge/) to provide high quality, scalable video conferences.

Demo site: <https://meet.jit.si>


## TOC

1. [Official Site](#official-site)  
1.1 [Official Doc](#official-doc)  
2. [Deployment](#deployment)  
2.1 [Code Repository](#code-repository)  
2.2 [Configs](#configs)  
2.2.1 [.env](#env)  
2.2.2 [docker-compose.yml](#docker-composeyml)  
2.3 [Running Container](#running-container)  
2.4 [Internal Authentication](#internal-authentication)  
2.5 [Nginx Reverse Proxy](#nginx-reverse-proxy)  
2.5.1 [Conf](#conf)  
3. [Snapshots](#snapshots)  
3.1 [Main Page](#main-page)  
3.2 [Login & Authentication](#login--authentication)  
3.3 [Participants](#participants)  
4. [Issues Occuring](#issues-occuring)  
4.1 [Websocket closed unexcectedly](#websocket-closed-unexcectedly)  
4.2 [No video with more than 2 participants](#no-video-with-more-than-2-participants)  
4.3 [Enable lobby not work](#enable-lobby-not-work)  
5. [Tutorials](#tutorials)  
6. [Change Log](#change-log)  


## Official Site

Item|Url
---|---
Official Site|https://jitsi.org/jitsi-meet/
Official Doc|https://jitsi.github.io/handbook/docs/intro
Official Forum|https://community.jitsi.org
GitHub|https://github.com/jitsi/jitsi-meet<br/>https://github.com/jitsi/docker-jitsi-meet
GitHub Wiki (deprecated)|https://github.com/jitsi/jitsi-meet/wiki
Docker Hub|https://hub.docker.com/u/jitsi/

### Official Doc

[Jitsi Meet Security & Privacy](https://jitsi.org/security/)

[Jitsi Meet Handbook](https://jitsi.github.io/handbook/docs/intro)

1. [User Guide](https://jitsi.github.io/handbook/docs/user-guide/user-guide-start)
2. [Developer Guide](https://jitsi.github.io/handbook/docs/dev-guide/dev-guide-start)
3. [Self-Hosting Guide](https://jitsi.github.io/handbook/docs/devops-guide/devops-guide-start)

Please read official tutorial [Self-Hosting Guide - Docker](https://jitsi.github.io/handbook/docs/devops-guide/devops-guide-docker) first.


## Deployment

[Jitsi Meet][jitsi_meet] server is running as a [Docker][docker] container.

Cloud firewalls define `reverse` droplet can listen TCP port range (8000-9000) exposed by `backend` droplets which running docker service. The `backend` droplet also needs to expose **UDP/10000** to the public Internet.

<details><summary>Click to expand broadcasting infrastructure</summary>

![](https://jitsi.github.io/handbook/docs/assets/docker-jitsi-meet.png "broadcasting infrastructure for recording or streaming a conference")

</details>

I write a note [Secure Nginx With Let's Encrypt Free SSL Certificate](/CyberSecurity/WebServer/Nginx/IssueSSLCert.md) to configure Nginx.

Variables definition

```sh
service_name='jitsi-meet'
service_save_path="$HOME/.docker/${service_name}"
mkdir -p "${service_save_path}"

localhost_ip='127.0.0.1'

domain_name_specify="${service_name%%-*}.maxdsre.com"

# config_save_dir="${service_save_path}/.jitsi-meet-cfg"

data_dir="/data/"${service_name}""
config_save_dir="${data_dir}/jitsi-meet-cfg"

# Create required CONFIG directories
# https://jitsi.github.io/handbook/docs/devops-guide/devops-guide-docker#testing-development-builds
sudo mkdir -p "${config_save_dir}"/{web/crontabs,web/letsencrypt,transcripts,prosody/config,prosody/prosody-plugins-custom,jicofo,jvb,jigasi,jibri}

```

### Code Repository

Download project [jitsi/docker-jitsi-meet](https://github.com/jitsi/docker-jitsi-meet)

```sh
# method 1 - via git
# git clone https://github.com/jitsi/docker-jitsi-meet.git "${service_save_path}"

# method 2 - via tar
curl -fsSL https://github.com/jitsi/docker-jitsi-meet/archive/master.tar.gz | tar zxf - --strip-component=1 -C "${service_save_path}"


```

### Configs

#### .env

<details><summary>Click to expand .env operation</summary>

```sh
# https://github.com/jitsi/docker-jitsi-meet/blob/master/env.example

cd "${service_save_path}"
chmod 640 *.sh  # change shell script permission to 640

rm -r .github  # remove directory .github/

# adjust custom variables here
# https://jitsi.github.io/handbook/docs/devops-guide/devops-guide-docker#configuration
tee "${service_save_path}"/custom_env 1> /dev/null << EOF
# date: $(date)

CONFIG=${config_save_dir}
HTTP_PORT=${localhost_ip}:8400
HTTPS_PORT=${localhost_ip}:8443
TZ=America/New_York
PUBLIC_URL=https://${domain_name_specify}

# - Features configuration (config.js)
ENABLE_LOBBY=1
ENABLE_PREJOIN_PAGE=1
ENABLE_WELCOME_PAGE=1
ENABLE_NOISY_MIC_DETECTION=1

# - Authentication
ENABLE_AUTH=1
ENABLE_GUESTS=1
AUTH_TYPE=internal

# - Advanced JVB options
#JVB_PORT=10000

# deprecated
#JVB_TCP_HARVESTER_DISABLED=true
#JVB_TCP_PORT=8453
#JVB_TCP_MAPPED_PORT=8453

# - Advanced configuration
RESTART_POLICY=unless-stopped
#DISABLE_HTTPS=1
#ENABLE_HSTS=1
#ENABLE_IPV6=1

# default is 'unstable' defined in docker_compose.yml
JITSI_IMAGE_VERSION=stable

# End
EOF

# create file .env from env.example
cp "${service_save_path}"/{env.example,.env}

# change specific directive values in file .env
sed '/^$/d;/^#/d' "${service_save_path}"/custom_env | while IFS="=" read -r var_k var_v; do
    sed -r -i '/^#*[[:space:]]*'"${var_k}"'=/{s@.*@'"${var_k}"'='"${var_v}"'@g}' "${service_save_path}"/.env
done

# sed -r -i '/^ETHERPAD_/{s@^([^=]+=[[:space:]]*)(.*)$@\1"\2"@g}' "${service_save_path}"/.env
# ETHERPAD_DEFAULT_PAD_TEXT, ETHERPAD_SKIN_VARIANTS wrap with '" "'

# change password length 'openssl rand -hex 16' to 'openssl rand -hex 64'
sed -r -i '/openssl.*-hex/{s@[[:digit:]]+$@64@g}' "${service_save_path}"/gen-passwords.sh
# generate/update password
bash "${service_save_path}"/gen-passwords.sh
```

</details>

#### docker-compose.yml

```sh
# https://github.com/jitsi/docker-jitsi-meet/blob/master/docker-compose.yml

cp -p docker-compose.yml{,.origin}

# Disable HTTPS prot (not require, via Nginx reverse proxy)
sed -r -i '/HTTPS_PORT[^:]*:443/{s@-@#&@g}' "${service_save_path}"/docker-compose.yml
```

### Running Container

```sh
cd "${service_save_path}"

# - Via docker compose (For docker-compose V2)
docker compose up -d
# docker compose --env-file "${service_save_path}/docker-compose.yml" up -d
docker compose ps
docker compose down

# - Via docker-compose (For docker-compose V1)
docker-compose up -d
# docker-compose -f "${service_save_path}/docker-compose.yml" up -d
docker-compose ps
# docker-compose down --remove-orphans
```

<details><summary>Click to expand container process</summary>

`docker compose` output

```sh
~/.docker/jitsi-meet# docker compose ps
NAME                   COMMAND             SERVICE             STATUS              PORTS
jitsi-meet-jicofo-1    "/init"             jicofo              running
jitsi-meet-jvb-1       "/init"             jvb                 running             0.0.0.0:10000->10000/udp, 127.0.0.1:8080->8080/tcp, :::10000->10000/udp
jitsi-meet-prosody-1   "/init"             prosody             running             5347/tcp
jitsi-meet-web-1       "/init"             web                 running             127.0.0.1:8400->80/tcp
```

`docker-compose` output

```sh
~/.docker/jitsi-meet# docker-compose ps
        Name           Command   State                        Ports
-----------------------------------------------------------------------------------------
jitsi-meet_jicofo_1    /init     Up
jitsi-meet_jvb_1       /init     Up      0.0.0.0:10000->10000/udp, 0.0.0.0:8453->8453/tcp
jitsi-meet_prosody_1   /init     Up      5222/tcp, 5280/tcp, 5347/tcp
jitsi-meet_web_1       /init     Up      443/tcp, 0.0.0.0:8400->80/tcp
```

</details>

### Internal Authentication

The default authentication mode (*internal*) uses XMPP credentials to authenticate users. To enable it you have to enable authentication with `ENABLE_AUTH` and set `AUTH_TYPE` to *internal*, then configure the settings you can see below.

```sh
# https://jitsi.github.io/handbook/docs/devops-guide/devops-guide-docker#authentication
# https://jitsi.github.io/handbook/docs/devops-guide/secure-domain#create-users-in-prosody-internal-auth

cd "${service_save_path}"

# Register user
docker compose exec prosody prosodyctl --config /config/prosody.cfg.lua register <username> meet.jitsi <password>

# unregister user
docker compose exec prosody prosodyctl --config=/config/prosody.cfg.lua unregister <username> meet.jitsi

# List existed users
docker compose exec prosody find /config/data/meet%2ejitsi/accounts -type f -exec basename {} .dat \;
```

Exercise

```sh
# change to your custom username/password
username='candy'
password='candy@123'

# Register
docker compose exec prosody prosodyctl --config /config/prosody.cfg.lua register "${username}" meet.jitsi "${password}"

# List existed users
docker compose exec prosody find /config/data/meet%2ejitsi/accounts -type f -exec basename {} .dat \;

# unregister
docker compose exec prosody prosodyctl --config=/config/prosody.cfg.lua unregister "${username}" meet.jitsi
```


### Nginx Reverse Proxy

Attention: if you don't expose *UDP/10000* on the public Internet (e.g. via firewall rule), you'll meet issue [No video and audio with more than 2 participants](https://community.jitsi.org/t/no-video-and-audio-with-more-than-2-participants/42641).

Variables definition

```sh
# https://jitsi.maxdsre.com
service_name='jitsi'
domain_name_specify="${service_name}.maxdsre.com"

exposted_http_port=8400
backend_private_ip=${backend_private_ip:-'127.0.0.1'}
# backend_private_ip='192.168.8.4'

# Nginx site config dir /etc/nginx/sites-enabled/
nginx_conf_path="/etc/nginx/sites-enabled/${service_name}.conf"
mkdir -p "${nginx_conf_path%/*}"
```

#### Conf

Nginx conf

<details><summary>Click to expand Nginx conf</summary>

```conf
sudo tee "${nginx_conf_path}" 1> /dev/null << EOF
upstream ${service_name}-web {
    server ${backend_private_ip}:${exposted_http_port};
}

# http://nginx.org/en/docs/stream/ngx_stream_ssl_preread_module.html
# This module is not built by default, it should be enabled with the --with-stream_ssl_preread_module configuration parameter.
# map $ssl_preread_server_name $stream_choose {
#     ~turnrelay  jitsi-turnrelay;
#     default     jitsi-web;
# }

# Redirect HTTP to HTTPS
server {
    listen 80;
    # listen [::]:80 ipv6only=on;
    server_name ${domain_name_specify};
    return 301 https://\$host\$request_uri;
}

#map \$request_uri \$${service_name}_log_ignore {
#    ~^/(libs|images|css|static|colibri-ws)/ 0;
#    ~^/(http-bind|xmpp-websocket) 0;
#    ~\.js 0; #    ~^/manifest 0;
#    default 1;
#}

map \$status \$${service_name}_log_ignore {
    ~^[23]  0;
    default 1;
}

server {
    listen 443 ssl http2;
    # listen [::]:443 ssl http2 ipv6only=on;
    server_name ${domain_name_specify};

    error_log  /var/log/nginx/${service_name}_error.log warn;  # [debug|info|notice|warn|error|crit|alert|emerg]
    access_log /var/log/nginx/${service_name}_access.log main if=\$${service_name}_log_ignore; # format name 'main' instead of predefined 'combined' format
    # access_log off;

    include /etc/nginx/conf.d/section/ssl.conf;

    location / {
        proxy_set_header Host \$host;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto \$scheme;

        proxy_pass http://${service_name}-web;

        # https://serverfault.com/questions/587386/an-upstream-response-is-buffered-to-a-temporary-file
        proxy_buffers 16 1M; 
        proxy_buffer_size 10M;
        proxy_busy_buffers_size 10M;
    }

    # https://github.com/jitsi/jitsi-videobridge/blob/master/doc/web-sockets.md
    location ~ ^/colibri-ws/([a-zA-Z0-9-\.]+)/(.*) {
        tcp_nodelay on;
        # proxy_http_version 1.1;
        proxy_set_header Upgrade \$http_upgrade;
        proxy_set_header Connection "upgrade";

        proxy_pass http://${service_name}-web/colibri-ws/\$1/\$2\$is_args\$args;
    }

    location /xmpp-websocket {
        ssi on;
        tcp_nodelay on;
        # proxy_http_version 1.1;
        proxy_set_header Upgrade \$http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_set_header Host \$host;

        proxy_pass http://${service_name}-web/xmpp-websocket;
    }

    # remove errors from favicon from nginx server logs
    location = /favicon.ico {
        return 204;
        log_not_found off;
    }
}
EOF
```

</details>

Testing Nginx conf syntax then reload

```sh
nginx -t && nginx -s reload

# nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
# nginx: configuration file /etc/nginx/nginx.conf test is successful
```

Testing domain if works

```sh
$ curl -I jitsi.maxdsre.com

HTTP/1.1 301 Moved Permanently
Server: nginx
Date: Wed, 03 Nov 2021 15:33:17 GMT
Content-Type: text/html
Content-Length: 162
Connection: keep-alive
Location: https://jitsi.maxdsre.com/

# Date: Mon, 08 Mar 2021 15:16:50 GMT
```

## Snapshots

### Main Page

Web main page

![jitsi meet mainpage](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/jitsi-meet/01_mainpage.png)

Camera & Microphone permission request

<details><summary>Click to expand camera permission request</summary>

![jitsi meet camera permission](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/jitsi-meet/02_camera_permission.png)

</details>

### Login & Authentication

Login page

![jitsi meet login page](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/jitsi-meet/03_login_page.png)

User authenticate

![jitsi meet hosts prompt](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/jitsi-meet/04_host_prompt.png)

![jitsi meet user authenticate](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/jitsi-meet/05_authentication.png)

Security setting

<details><summary>Click to expand security settings</summary>

![jitsi meet security setting](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/jitsi-meet/06_security_option.png)

![jitsi meet security setting on](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/jitsi-meet/07_security_option_on.png)

</details>

### Participants

Participants

<details><summary>Click to expand participants</summary>

![jitsi meet participant](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/jitsi-meet/08_participant.png)

Meeting with 3 participants

![jitsi meet meeting](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/jitsi-meet/09_meeting.png)

</details>

Message

<details><summary>Click to expand message</summary>

![jitsi meet message](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/jitsi-meet/10_message.png)

</details>


## Issues Occuring

### Websocket closed unexcectedly

Open the web browser, it prompts erros

>You have been discontinued.

Web browser `Developer Tools` shows error

>Websocket closed unexcectedly

Solution is add *xmpp-websocket* section in Nginx config (source [[Jitsi-Docker] Using Jitsi Meet behind Nginx reverse proxy](https://community.jitsi.org/t/jitsi-docker-using-jitsi-meet-behind-nginx-reverse-proxy/65457))

```conf
location /xmpp-websocket {
    ssi on;
    proxy_pass http://jitsi-web/xmpp-websocket;
    # proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "upgrade";
    proxy_set_header Host $host;
    tcp_nodelay on;
}
```

### No video with more than 2 participants

>failed: Error during WebSocket handshake: Unexpected response code: 405

Same issues

* [Error during WebSocket handshake](https://community.jitsi.org/t/error-during-websocket-handshake/82357)
* [No video and audio with more than 2 participants](https://community.jitsi.org/t/no-video-and-audio-with-more-than-2-participants/42641)

It is caused by [UDP port 10000 blocked behind corporate firewall - possible approaches](https://community.jitsi.org/t/udp-port-10000-blocked-behind-corporate-firewall-possible-approaches/28681).

Jitsi meetings in general operate in 2 ways: peer-to-peer (P2P) or via the Jitsi Videobridge (JVB). P2P mode is only used for 1-to-1 meetings.

Solution: expose UDP/10000 to the public Internet. ([Tip: how to check UDP/10000 connectivity](https://community.jitsi.org/t/tip-how-to-check-udp-10000-connectivity/73031/13))


### Enable lobby not work

Setting `ENABLE_LOBBY=1` in file *.env*, but it seems not work. You need to enable it manually.


## Tutorials

* [How to Self Host Jitsi Meet With Docker](https://linuxhandbook.com/self-host-jitsi-meet/)
* [Eigene Jitsi Meet Instanz installieren (Docker/Ubuntu/Nginx)](https://adminforge.de/linux-allgemein/eigene-jitsi-meet-instanz-installieren-docker-ubuntu-nginx/)

Series

* [Meetrix - Jitsi Meet](https://meetrix.io/blog/webrtc/jitsi/meet/installing.html)


## Change Log

* Nov 03, 2021 12:32 Wed ET
  * update configuration, change config dir to path */data*
* Mar 09, 2021 18:41 Tue ET
  * Solve issue cause by *UDP/10000*
* Mar 08, 2021 15:29 Mon ET
  * First draft


<!-- [nginx]: https://www.nginx.com "High Performance Load Balancer, Web Server, & Reverse Proxy" -->
[docker]: https://www.docker.com
<!-- [docker_compose]:https://docs.docker.com/compose/ -->
[jitsi]:https://jitsi.org
[jitsi_meet]:https://jitsi.org/jitsi-meet/ "Free Video Conferencing Solutions"

<!-- End -->