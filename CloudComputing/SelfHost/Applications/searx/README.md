# Self-hosting Searx

[SearXNG][searxng] is a fork of [searx][searx] which is a free internet metasearch engine which aggregates results from various search services and databases. Users are neither tracked nor profiled.


## Official Site

Item|Url
---|---
Official Site|https://searxng.org
Official Doc|https://docs.searxng.org
GitHub|https://github.com/searxng/searxng<br/>https://github.com/searxng/searxng-docker
Docker Hub|https://hub.docker.com/r/searxng/searxng

## Deployment

### Configs

Variables Definition

```sh
service_name='searx'
service_save_path="$HOME/.docker/${service_name}"
mkdir -p "${service_save_path}"

data_dir="/data/"${service_name}""
config_save_dir="${data_dir}/config"

localhost_ip='127.0.0.1'
domain_for_server="${service_name%%-*}.maxdsre.com"
exposted_http_port='8050'  # default 8080
```

#### .env

File `.env`

<details><summary>Click to expand env conf</summary>

```sh
tee "${service_save_path}"/.env 1> /dev/null << EOF

#PUID=1000  # id -u
#PGID=1000  # id -g
TZ=$(cat /etc/timezone)  # America/New_York from file /etc/timezone

EXPOSED_PORT=${localhost_ip}:${exposted_http_port}
PROXY_DOMAIN=${domain_for_server}

# - Cache
# The use of Redis is recommended to prevent file locking problems.
#REDIS_HOST (not set by default) Name of Redis container
#REDIS_HOST_PORT (default: 6379) Optional port for Redis, only use for external Redis servers that run on non-standard ports.
REDIS_HOST_PASSWORD=$(openssl rand -hex 64 | head -c 45)

# - Optional

# End
EOF
```

</details>


#### docker-compose.yml

```sh
cd "${service_save_path}"

tee docker-compose.yml 1> /dev/null << EOF
version: '3.8'

services:
  searxng:
    image: searxng/searxng:latest
    container_name: searxng-server
    restart: unless-stopped
    mem_limit: 1536m  # 1.5GB
    mem_reservation: 128m
    networks:
      - searxng_network
    ports:
     - "\${EXPOSED_PORT}:8080"
    volumes:
      # https://github.com/searxng/searxng/blob/master/searx/settings.yml
      # https://github.com/searxng/searxng/blob/master/dockerfiles/uwsgi.ini
      # ./configs contain file {settings.yml, uwsgi.ini}
      - ./configs:/etc/searxng:rw  # for first time to generate config files
      # - ./configs:/etc/searxng:ro # 'rw' for first time, after tweak configs, change to 'ro'
      - /etc/localtime:/etc/localtime:ro
    # environment:
    #   - SEARXNG_BASE_URL=
    # logging:
    #   driver: "json-file"
    #   options:
    #     max-size: "1m"
    #     max-file: "1"

  redis:
    image: redis:6-alpine
    container_name: searxng-redis
    restart: unless-stopped
    mem_limit: 64m
    mem_reservation: 8m  # Minimum memory reservation allowed is 6MB
    networks:
      - searxng_network
    command: redis-server --requirepass \${REDIS_HOST_PASSWORD} --save "" --appendonly "no"
    env_file: .env
    environment:
      - REDIS_HOST_PASSWORD=\${REDIS_HOST_PASSWORD}
    tmpfs:
      - /var/lib/redis
    volumes:
      #- ${data_dir}/redis:/var/lib/redis
      - /etc/localtime:/etc/localtime:ro
    expose:
      - 6379

networks:
  searxng_network:
    name: searxng_network

# End
EOF
```

### Running Container

```sh
cd "${service_save_path}"

# - Via docker compose (For docker-compose V2)
docker compose up -d
# docker compose --env-file "${service_save_path}/docker-compose.yml" up -d
docker compose ps
docker compose down --remove-orphans

# - Via docker-compose (For docker-compose V1)
docker-compose up -d
# docker-compose -f "${service_save_path}/docker-compose.yml" up -d
docker-compose ps
# docker-compose down --remove-orphans
```

### Nginx Reverse Proxy

Nginx

```sh
service_name='searx'
domain_for_server="${service_name%%-*}.maxdsre.com"
exposted_http_port='8050'  # default 8080

backend_private_ip=${backend_private_ip:-'127.0.0.1'}
# backend_private_ip='192.168.8.4'

# Nginx site config dir /etc/nginx/sites-enabled/
nginx_conf_path="/etc/nginx/sites-enabled/${service_name}.conf"
sudo mkdir -p "${nginx_conf_path%/*}"
```

#### Conf

Nginc config sample can find from froum post [NGINX Reverse Proxy Config for Emby + Website With SSL](https://emby.media/community/index.php?/topic/80952-nginx-reverse-proxy-config-for-emby-website-with-ssl/)


<details><summary>Click to expand config info</summary>

```conf
sudo tee "${nginx_conf_path}" 1> /dev/null << EOF
upstream ${service_name}-default {
    server ${backend_private_ip}:${exposted_http_port};
}

map \$http_upgrade \$connection_upgrade {
    default upgrade;
    '' close;
}

# - For Sync Server
# Redirect HTTP to HTTPS
server {
    listen 80;
    # listen [::]:80 ipv6only=on;
    server_name ${domain_for_server};
    return 301 https://\$host\$request_uri;
}

server {
    listen 443 ssl http2;
    # listen [::]:443 ssl http2 ipv6only=on;
    server_name ${domain_for_server};

    error_log  /var/log/nginx/${domain_for_server%%.*}_error.log warn;  # [debug|info|notice|warn|error|crit|alert|emerg]
    access_log /var/log/nginx/${domain_for_server%%.*}_access.log main; # format name 'main' instead of predefined 'combined' format

    include /etc/nginx/conf.d/section/ssl.conf;

    location / {
        #proxy_set_header Host \$host;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto \$scheme;

        # http://nginx.org/en/docs/http/ngx_http_proxy_module.html#proxy_http_version
        proxy_http_version 1.1;
        proxy_set_header Upgrade \$http_upgrade;
        proxy_set_header Connection \$connection_upgrade;
        proxy_set_header Host \$host;
        proxy_set_header Accept-Encoding gzip;

        # https://serverfault.com/questions/587386/an-upstream-response-is-buffered-to-a-temporary-file
        # https://www.cyberciti.biz/faq/nginx-upstream-sent-too-big-header-while-reading-response-header-from-upstream/
        proxy_buffering off;
        #proxy_buffers   4 512k;
        #proxy_buffer_size   256k;
        #proxy_busy_buffers_size   512k;

        add_header Content-Security-Policy "default-src 'none'; child-src 'self'; font-src 'self' data:; connect-src 'self' wss: ws:; media-src 'self' blob: data:; manifest-src 'self'; base-uri 'none'; form-action 'self'; frame-src 'self'; frame-ancestors 'self'; object-src 'none'; worker-src 'self' blob:; script-src 'self'; img-src data: https: http: ; style-src 'unsafe-inline' 'self'" always;

        proxy_pass http://${service_name}-default;
    }
}

# End
EOF
```

</details>

Testing Nginx conf syntax then reload

```sh
sudo -i

nginx -t && nginx -s reload

# nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
# nginx: configuration file /etc/nginx/nginx.conf test is successful
```

### Running Container

```sh
cd "${service_save_path}"

# - Via docker compose (For docker-compose V2)
docker compose up -d
# docker compose --env-file "${service_save_path}/docker-compose.yml" up -d
docker compose ps
docker compose down --remove-orphans

# - Via docker-compose (For docker-compose V1)
docker-compose up -d
# docker-compose -f "${service_save_path}/docker-compose.yml" up -d
docker-compose ps
# docker-compose down --remove-orphans
```


## Change Log

* Jun 02, 2022 12:50 Thu ET
  * First draft

[searx]:https://github.com/searx/searx
[searxng]:https://github.com/searxng/searxng

<!-- End -->