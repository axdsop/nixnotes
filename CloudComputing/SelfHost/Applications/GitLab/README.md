# Self-hosting GitLab Server

## Official Site

Item|Url
---|---
Official site|https://gitlab.com
Official doc|https://docs.gitlab.com
GitLab|https://gitlab.com/gitlab-org
Docker|[Community Edition](https://hub.docker.com/r/gitlab/gitlab-ce)<br/>[Enterprise Edition](https://hub.docker.com/r/gitlab/gitlab-ee)<br/>[Dockerfile](https://gitlab.com/gitlab-org/omnibus-gitlab/tree/master/docker)

### Documentations

* [GitLab Docker images](https://docs.gitlab.com/ee/install/docker.html)


## Deployment

### Variables Definition

Variables Definition

```sh
service_name='gitlab'
service_save_path="$HOME/.docker/${service_name}"
mkdir -p "${service_save_path}"

data_dir="/data/"${service_name}""

localhost_ip='127.0.0.1'
domain_for_server="${service_name%%-*}.maxdsre.com"
exposted_http_port='8020'  # default 80
exposted_https_port='8021'  # default 443
exposted_ssh_port='8022'  # default 22

domain_for_server="git.maxdsre.com"
backend_private_ip=${backend_private_ip:-'127.0.0.1'}
# backend_private_ip='192.168.8.4'

# Nginx site config dir /etc/nginx/sites-enabled/
nginx_conf_path="/etc/nginx/sites-enabled/${service_name}.conf"
sudo mkdir -p "${nginx_conf_path%/*}"
```


```sh
# https://docs.gitlab.com/ee/install/docker.html#set-up-the-volumes-location
export GITLAB_HOME="${data_dir}"  # /data/gitlab

[[ -f ~/.bashrc ]] && sed -r -i '/^export[[:space:]]+GITLAB_HOME=/d' .bashrc
[[ -f ~/.bashrc ]] && tee -a ~/.bashrc 1> /dev/null << EOF

export GITLAB_HOME='${data_dir}'
EOF
```

### env


File `.env`

<details><summary>Click to expand env conf</summary>

```sh
tee "${service_save_path}"/.env 1> /dev/null << EOF
GITLAB_HOME="${data_dir}"
EOF
```

</details>

### docker-compose.yml

```sh
cd "${service_save_path}"

tee docker-compose.yml 1> /dev/null << EOF
version: '3.8'

services:
  gitlab:
    image: gitlab/gitlab-ce:latest
    container_name: gitlab-ce
    restart: unless-stopped
    networks:
      - gitlab_network
    environment:
      GITLAB_OMNIBUS_CONFIG: |
        #external_url 'http://${localhost_ip}'
        external_url 'http://${domain_for_server}'

        # Add any other gitlab.rb configuration here, each on its own line
        gitlab_rails['gitlab_shell_ssh_port'] = ${exposted_ssh_port}

        # https://docs.gitlab.com/omnibus/settings/nginx.html
        #nginx['enable'] = false
        nginx['listen_https'] = false
        letsencrypt['enable'] = false

        # https://docs.gitlab.com/ee/administration/monitoring/
        gitlab_exporter['enable'] = false
        node_exporter['enable'] = false
        pgbouncer_exporter['enable'] = false
        postgres_exporter['enable'] = false
        prometheus_monitoring['enable'] = false
        puma['exporter_enabled'] = false
        redis_exporter['enable'] = false
        grafana['enable'] = false
        sidekiq['metrics_enabled'] = false

        # memory usage optimization
        # https://docs.gitlab.com/omnibus/settings/memory_constrained_envs.html
        puma['worker_processes'] = 0
        postgresql['shared_buffers'] = "64MB"
        postgresql['max_worker_processes'] = 2
        sidekiq['max_concurrency'] = 2
        sidekiq['concurrency'] = 1
        sidekiq['health_checks_enabled'] = false

        # https://soulteary.com/2021/07/14/gitlab-14-lightweight-operation-solution.html
        gitlab_rails['gitlab_default_projects_features_container_registry'] = false  # L 142
        gitlab_rails['registry_enabled'] = false  # L 796
        registry['enable'] = false  # L 813
        registry_nginx['enable'] = false  # L 1985
        gitlab_rails['packages_enabled'] = false  # L 2577
        gitlab_rails['dependency_proxy_enabled'] = false  # L 2599

        gitlab_pages['enable'] = false  # L 1643
        pages_nginx['enable'] = false  # L 1828

        gitlab_rails['usage_ping_enabled'] = false  # L 481
        gitlab_rails['sentry_enabled'] = false  # L 873
        grafana['reporting_enabled'] = false  # L 2279

        gitlab_ci['gitlab_ci_all_broken_builds'] = false  # L 1837
        gitlab_ci['gitlab_ci_add_pusher'] = false  # L 1838

        gitlab_rails['gitlab_kas_enabled'] = false  # L 1847
        gitlab_kas['enable'] = false  # L 1853
        gitlab_rails['terraform_state_enabled'] = false  # L 424
        gitlab_rails['kerberos_enabled'] = false  # L 2564
        sentinel['enable'] = false  # L 2629
        mattermost['enable'] = false  # L 1918
        mattermost_nginx['enable'] = false  # L 1959

        gitlab_rails['smtp_enable'] = false  # L 85
        gitlab_rails['gitlab_email_enabled'] = false  # L 105
        gitlab_rails['incoming_email_enabled'] = false  # L 265

        consul['enable'] = false  # L 3003
        #gitaly['enable'] = false  # L 2327
        #redis['enable'] = false  # L 1287
 
    ports:
      - '${localhost_ip}:${exposted_http_port}:80'
    # - '${localhost_ip}:${exposted_https_port}:443'
      - '${exposted_ssh_port}:22'
    volumes:
      # https://docs.gitlab.com/ee/install/docker.html#set-up-the-volumes-location
      - '\$GITLAB_HOME/config:/etc/gitlab'
      #- './configs:/etc/gitlab'      
      - '\$GITLAB_HOME/logs:/var/log/gitlab'
      - '\$GITLAB_HOME/data:/var/opt/gitlab'
      - /etc/localtime:/etc/localtime:ro
    shm_size: '256m' # 1gb

#   gitlab-runner:
#     image: gitlab/gitlab-runner:alpine
#     container_name: gitlab-runner    
#     restart: unless-stopped
#     depends_on:
#       - gitlab
#     volumes:
#       - /var/run/docker.sock:/var/run/docker.sock
#       - '\$GITLAB_HOME/gitlab-runner:/etc/gitlab-runner'
#     networks:
#       - gitlab_network

networks:
  gitlab_network:
    name: gitlab_network

# End
EOF
```

### Running Container

```sh
cd "${service_save_path}"

# - Via docker compose (For docker-compose V2)
docker compose up -d
# docker compose --env-file "${service_save_path}/docker-compose.yml" up -d
docker compose ps
docker compose down --remove-orphans

# - Via docker-compose (For docker-compose V1)
docker-compose up -d
# docker-compose -f "${service_save_path}/docker-compose.yml" up -d
docker-compose ps
# docker-compose down --remove-orphans
```

Show running logs

```sh
# docker compose logs gitlab

docker compose logs -f gitlab
```

Service start logs

```txt
gitlab-ce  | [2022-12-04T19:37:25-05:00] INFO: Cinc Client Run complete in 105.903896691 seconds
gitlab-ce  |
gitlab-ce  | Running handlers:
gitlab-ce  | [2022-12-04T19:37:25-05:00] INFO: Running report handlers
gitlab-ce  | Running handlers complete
gitlab-ce  | [2022-12-04T19:37:25-05:00] INFO: Report handlers complete
gitlab-ce  | Infra Phase complete, 202/950 resources updated in 01 minutes 47 seconds
gitlab-ce  |
gitlab-ce  | Notes:
gitlab-ce  | Found old initial root password file at /etc/gitlab/initial_root_password and deleted it.
```

#### Initial user and password

Default user name is `root`, initial password saved in file *$GITLAB_HOME/config/initial_root_password*

```sh
# Path: /data/gitlab/config/initial_root_password
docker compose exec gitlab grep 'Password:' /etc/gitlab/initial_root_password

Password: kMozrecyjfOXryTEkHFD+KRinDTfcHFQKGRqusaat0k=
```


```sh
# back up GitLab
docker compose exec -t <container name> gitlab-backup create

# fix permission issues after update docker images
docker compose exec gitlab update-permissions
docker compose restart gitlab

# reconfigure GitLab to use the new settings
docker compose exec gitlab gitlab-ctl reconfigure
docker compose exec gitlab gitlab-ctl show-config
```

### Nginx Reverse Proxy

Nginx

https://docs.gitlab.com/omnibus/settings/nginx.html

https://gitlab.com/gitlab-org/gitlab-recipes/tree/master/web-server/nginx

https://gitlab.com/gitlab-org/gitlab-foss/tree/master/lib/support/nginx

https://gist.github.com/ryands/ab54fd05507bfbcff850bff771939375

```sh
service_name='gitlab'
domain_for_server="git.maxdsre.com"
exposted_http_port='8020'  # default 80

backend_private_ip=${backend_private_ip:-'127.0.0.1'}
# backend_private_ip='192.168.8.4'

# Nginx site config dir /etc/nginx/sites-enabled/
nginx_conf_path="/etc/nginx/sites-enabled/${service_name}.conf"
sudo mkdir -p "${nginx_conf_path%/*}"
```

Nginx conf

```conf
sudo tee "${nginx_conf_path}" 1> /dev/null << EOF

upstream ${service_name}-workhorse {
  server ${backend_private_ip}:${exposted_http_port};

  # GitLab socket file, for Omnibus this would be: unix:/var/opt/gitlab/gitlab-workhorse/sockets/socket
  # $GITLAB_HOME/data == /data/gitlab/data
  #server unix:/data/gitlab/data/gitlab-workhorse/sockets/socket fail_timeout=0;
}

map \$http_upgrade \$connection_upgrade_gitlab_ssl {
    default upgrade;
    ''      close;
}

map \$request_uri \$log_ignore {
    #~^/users/sign_in 0;
    ~^/assets/favicon- 0;
    ~^/assets/webpack/ 0;
    default 1;
}

## Remove private_token from the request URI
# In:  /foo?private_token=unfiltered&authenticity_token=unfiltered&feed_token=unfiltered&...
# Out: /foo?private_token=[FILTERED]&authenticity_token=unfiltered&feed_token=unfiltered&...
map \$request_uri \$gitlab_ssl_temp_request_uri_1 {
  default \$request_uri;
  ~(?i)^(?<start>.*)(?<temp>[\?&]private[\-_]token)=[^&]*(?<rest>.*)$ "\$start\$temp=[FILTERED]\$rest";
}

## Remove authenticity_token from the request URI
# In:  /foo?private_token=[FILTERED]&authenticity_token=unfiltered&feed_token=unfiltered&...
# Out: /foo?private_token=[FILTERED]&authenticity_token=[FILTERED]&feed_token=unfiltered&...
map \$gitlab_ssl_temp_request_uri_1 \$gitlab_ssl_temp_request_uri_2 {
  default \$gitlab_ssl_temp_request_uri_1;
  ~(?i)^(?<start>.*)(?<temp>[\?&]authenticity[\-_]token)=[^&]*(?<rest>.*)$ "\$start\$temp=[FILTERED]\$rest";
}

## Remove feed_token from the request URI
# In:  /foo?private_token=[FILTERED]&authenticity_token=[FILTERED]&feed_token=unfiltered&...
# Out: /foo?private_token=[FILTERED]&authenticity_token=[FILTERED]&feed_token=[FILTERED]&...
map \$gitlab_ssl_temp_request_uri_2 \$gitlab_ssl_filtered_request_uri {
  default \$gitlab_ssl_temp_request_uri_2;
  ~(?i)^(?<start>.*)(?<temp>[\?&]feed[\-_]token)=[^&]*(?<rest>.*)$ "\$start\$temp=[FILTERED]\$rest";
}

## A version of the referer without the query string
map \$http_referer \$gitlab_ssl_filtered_http_referer {
  default \$http_referer;
  ~^(?<temp>.*)\? \$temp;
}

# Redirect HTTP to HTTPS
server {
    listen 80;
    # listen [::]:80 ipv6only=on;
    server_name ${domain_for_server};
    return 301 https://\$host\$request_uri;
}

server {
    listen 443 ssl http2;
    # listen [::]:443 ssl http2 ipv6only=on;
    server_name ${domain_for_server};

    error_log  /var/log/nginx/${domain_for_server%%.*}_error.log warn;  # [debug|info|notice|warn|error|crit|alert|emerg]
    access_log /var/log/nginx/${domain_for_server%%.*}_access.log main if=\$log_ignore; # format name 'main' instead of predefined 'combined' format

    # if specified SSL cert
    include /etc/nginx/conf.d/section/ssl.conf;

    #client_max_body_size 64M;

    ## Using a Sub Path Config
    # Path to the root of your installation
    location / {
      # if you sepcifiy header 'Content-Security-Policy', the following policy is tweaked for GitLab 
      # https://docs.gitlab.com/omnibus/settings/configuration.html#set-a-content-security-policy
      # set here, but it doesn't work. set it into the container ?
      add_header Content-Security-Policy "default-src 'self'; script-src 'self' 'unsafe-inline'; font-src 'self'; connect-src 'self'; img-src 'self' data: https://www.gravatar.com/avatar/; style-src 'unsafe-inline' 'self'; base-uri 'self'; manifest-src 'self'; form-action 'self'";  # https://content-security-policy.com

      client_max_body_size 0;
      gzip off;

      ## https://github.com/gitlabhq/gitlabhq/issues/694
      ## Some requests take more than 30 seconds.
      proxy_read_timeout      300;
      proxy_connect_timeout   300;
      proxy_redirect          off;

      proxy_http_version 1.1;

      proxy_set_header    Host                \$http_host;
      proxy_set_header    X-Real-IP           \$remote_addr;
      proxy_set_header    X-Forwarded-Ssl     on;
      proxy_set_header    X-Forwarded-For     \$proxy_add_x_forwarded_for;
      proxy_set_header    X-Forwarded-Proto   \$scheme;
      proxy_set_header    Upgrade             \$http_upgrade;
      proxy_set_header    Connection          \$connection_upgrade_gitlab_ssl;

      proxy_pass http://${service_name}-workhorse;

      # https://serverfault.com/questions/587386/an-upstream-response-is-buffered-to-a-temporary-file
      # https://www.cyberciti.biz/faq/nginx-upstream-sent-too-big-header-while-reading-response-header-from-upstream/
      proxy_buffering off;
      #proxy_buffers   4 512k;
      #proxy_buffer_size   256k;
      #proxy_busy_buffers_size   512k;
    }

}
EOF
```

Testing Nginx conf syntax then reload

```sh
nginx -t && nginx -s reload

# nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
# nginx: configuration file /etc/nginx/nginx.conf test is successful
```

### Post-Configuration

### Firewall Setting


```sh
# For Debian/Ubuntu
sudo ufw allow 8022/tcp comment 'gitlab sshd tcp'
```

### SSH

Official doc [Use SSH keys to communicate with GitLab](https://docs.gitlab.com/ee/user/ssh.html)

```sh
ssh -T -p 8022 git@git.maxdsre.com

Welcome to GitLab, @johnny!
```


## Tutorials

* [How to install GitLab using Docker Compose?](https://www.czerniga.it/2021/11/14/how-to-install-gitlab-using-docker-compose/)
* [GitLab 14 轻量化运行方案](https://soulteary.com/2021/07/14/gitlab-14-lightweight-operation-solution.html)


## Change Log

* Mar 18, 2022 10:05 Fri ET
  * First draft


<!-- End -->
