# Self-hosting Standard Notes

[Standard Notes][standard_notes] is free, open-source, and completely encrypted notes app.

You can self-host Standard Notes [server][standalone] and [web][web] application.


## TOC

1. [Official Site](#official-site)  
1.1 [Documentation](#documentation)  
2. [Infrastructure Overview](#infrastructure-overview)  
2.1 [Server](#server)  
2.2 [Web](#web)  
2.3 [File](#file)  
2.3.1 [FileSafe](#filesafe)  
2.3.2 [FileSend](#filesend)  
3. [Prerequisite](#prerequisite)  
3.1 [Essential packages](#essential-packages)  
3.1.1 [Nginx](#nginx)  
3.1.2 [Docker](#docker)  
3.2 [Variables Definition](#variables-definition)  
4. [Configuration Tweaking](#configuration-tweaking)  
4.1 [Syncing Server](#syncing-server)  
4.1.1 [docker-compose.yml](#docker-composeyml)  
4.1.2 [env](#env)  
4.2 [Extension](#extension)  
4.2.1 [docker-compose.yml](#docker-composeyml)  
4.3 [Web app](#web-app)  
4.3.1 [env](#env-1)  
4.3.2 [docker-compose.yml](#docker-composeyml)  
4.4 [FileSafe (optional)](#filesafe-optional)  
4.4.1 [env](#env-2)  
4.4.2 [docker-compose.yml](#docker-composeyml)  
5. [Deployment](#deployment)  
5.1 [Running Containers](#running-containers)  
5.2 [Nginx Reverse Proxy](#nginx-reverse-proxy)  
5.2.1 [Conf](#conf)  
6. [Extensions](#extensions)  
6.1 [Project Code](#project-code)  
6.2 [.env](#env-2)  
6.3 [docker-compose.yml](#docker-composeyml)  
6.4 [Verifying](#verifying)  
6.5 [Extended Activation Code](#extended-activation-code)  
7. [Snapshots](#snapshots)  
7.1 [Verification](#verification)  
7.2 [Desktop Client](#desktop-client)  
7.2.1 [Register & Login](#register--login)  
7.3 [Extensions](#extensions-1)  
7.4 [Editor](#editor)  
7.5 [Web Site](#web-site)  
7.6 [Editor Tabs](#editor-tabs)  
8. [Database Dump](#database-dump)  
8.1 [Importing Database Dump](#importing-database-dump)  
9. [Issues Occuring](#issues-occuring)  
9.1 [Fail to connect database](#fail-to-connect-database)  
9.2 [Waiting for Key](#waiting-for-key)  
9.3 [FileSafe Secret Key](#filesafe-secret-key)  
10. [Tutorials](#tutorials)  
11. [Resources](#resources)  
12. [Change Log](#change-log)  


## Official Site

Item|Url
---|---
Official site|~~https://standardnotes.org~~ (deprecated)<br/>https://standardnotes.com
Official doc|~~https://docs.standardnotes.org~~ (deprecated)<br/>https://docs.standardnotes.com
GitHub|https://github.com/standardnotes/
Docker|https://hub.docker.com/u/standardnotes


### Documentation

* [Help & Support](https://standardnotes.com/help)
* [Getting Started with Self-hosting](https://docs.standardnotes.com/self-hosting/getting-started/)
  * [Self-hosting with Docker](https://docs.standardnotes.com/self-hosting/docker)


## Infrastructure Overview

Official Doc

* [Can I self-host Standard Notes?](https://standardnotes.com/help/47/can-i-self-host-standard-notes)
* [Infrastructure Overview](https://docs.standardnotes.com/self-hosting/infrastructure-overview/)

### Server

Service|Docker|Note
---|---|---
[Syncing Server JS][syncing_server_js]|https://hub.docker.com/r/standardnotes/syncing-server-js|core logic, data operation
[Syncing Server JS][syncing_server_js] Worker||data operation for all asynchronous tasks
[API Gateway][api_gateway]|https://hub.docker.com/r/standardnotes/api-gateway|entry point
[Auth][auth]|https://hub.docker.com/r/standardnotes/auth|authorization and authentication
[Auth][auth] Worker||authorization and authentication for all asynchronous tasks
DB||MySQL database server
Cache||Redis

**Attention**: Project [standardnotes/syncing-server][syncing_server] is deprecated (archived [note](./archives/syncing-server.md)), the new Node.js server is [standardnotes/syncing-server-js][syncing_server_js].

For self-hosting, use [standardnotes/standalone][standalone].

### Web

Official demo: <https://app.standardnotes.com>.

Service|Docker|Note
---|---|---
[Web][web]||https://hub.docker.com/r/standardnotes/web|Standard Notes web app


### File

Still in test

#### FileSafe

Official demo: <https://filesafe.standardnotes.org>

>FileSafe is a platform by Standard Notes that integrates directly with your day-to-day usage of Standard Notes, and serves as an encryption intermediary between you and your favorite cloud provider. -- [FileSafe 101](https://standardnotes.com/help/44/filesafe-101)

The [Bold Editor](https://github.com/standardnotes/bold-editor "standardnotes/bold-editor") is a Standard Notes derived editor that offers text formatting and FileSafe integration.

<details><summary>Click to expand architecture</summary>

![FileSafe](https://standardnotes.com/static/filesafe-graph-b5194e826a57eb72ba7b9910f5531ff9.png "https://standardnotes.com/filesafe")

</details>

Service|Docker|Note
---|---|---
[filesafe-relay][filesafe_relay]|https://hub.docker.com/r/standardnotes/filesafe-relay|communicate between FileSafe client extension and server destination (Dropbox, Google Drive, WebDAV)

Official doc

* [How do I attach encrypted files to my notes?](https://standardnotes.com/help/36/how-do-i-attach-encrypted-files-to-my-notes)

#### FileSend

Official demo: <https://filesend.standardnotes.com>

Service|Docker|Note
---|---|---
[FileSend][filesend]|https://hub.docker.com/r/standardnotes/filesend|encrypted file sharing


## Prerequisite

It needs your machine has >= 2GB of memory to run the infrastructure ([Requirements](https://docs.standardnotes.com/self-hosting/docker#requirements)).

### Essential packages

Install essential packages on host system.

```sh
# For Debian/Ubuntu
sudo apt-get update
sudo apt-get upgrade

# default-libmysqlclient-dev - MySQL database development files (metapackage)
sudo apt-get install -y default-libmysqlclient-dev
# sudo apt-get install -y git # optional
```

#### Nginx

I write a note [Secure Nginx With Let's Encrypt Free SSL Certificate](/CyberSecurity/WebServer/Nginx/IssueSSLCert.md) to configure Nginx.

#### Docker

[Standard Notes][standard_notes] service is running as a [Docker][docker] container.

Install [Docker][docker] and [Docker Compose][docker_compose]. ([Note](/CloudComputing/Containers/Docker/README.md))

```sh
download_command='wget -qO-'
[[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && download_command='curl -fsSL'

# - For Docker CE
${download_command} https://gitlab.com/axdsop/nixnotes/raw/master/ShellScripts/Toolkits/Application/Container/Docker-CE.sh | sudo bash -s -- -h

# - For Docker Compose (v2)
${download_command} https://gitlab.com/axdsop/nixnotes/raw/master/CloudComputing/Containers/Docker/Scripts/docker_compose.sh | sudo bash -s --

# docker compose version  # v2
# docker-compose version  # v1
```

### Variables Definition

```sh
service_name='standardnotes'
data_dir="/data/${service_name}"
service_save_path="$HOME/.docker/${service_name}"
service_envs_dir="${service_save_path}/envs"
mkdir -p "${service_envs_dir}"

service_env_custom_dir="${service_envs_dir}/custom"

localhost_ip='127.0.0.1'

# - For Sync server
server_exposed_port='8200'  # default 3000

# - For extensions (optional)
extensions_exposed_port='8201'
extensions_repo_save_path="${service_save_path}/extensions"
extensions_data_dir="${data_dir}/extensions"

# - For web app (optional)
web_exposed_port='8202'  # default 3001
service_env_web_dir="${service_envs_dir}/web"

# - For filesafe
filesafe_exposed_port='8203'  # default 3000
service_env_filesafe_dir="${service_envs_dir}/filesafe"

```

Domain name

```sh
domain_specify=${domain_specify:-'maxdsre.com'}

domain_for_server="snsync.${domain_specify}"  # https://snsync.maxdsre.com
domain_for_web="sn.${domain_specify}"  # https://sn.maxdsre.com
domain_for_filesafe="snfilesafe.${domain_specify}"  # https://snfilesafe.maxdsre.com
```


## Configuration Tweaking

~~Cloud firewalls define `reverse` droplet can listen TCP port range (8000-9000) exposed by `backend` droplets which running docker service.~~

### Syncing Server

Download project [standardnotes/standalone][standalone] (branch *main*)

```sh
# method 1 - via git
# git clone --single-branch --branch main https://github.com/standardnotes/standalone.git "${service_save_path}"

# method 2 - via tar
curl -fsSL https://github.com/standardnotes/standalone/archive/refs/heads/main.tar.gz | tar zxf - --strip-component=1 -C "${service_save_path}"
```

Change docker compose command from v1 to v2.

```sh
cd "${service_save_path}"

cp -p server.sh{,.sample}
chmod 640 *.sample

# https://github.com/docker/compose/releases/tag/v2.0.0
sed -r -i '/docker-compose/{s@docker-compose@docker compose@g}' server.sh
```

#### docker-compose.yml

Tewaking configuration in file *docker-compose.yml*.

<details><summary>Click to expand operation details</summary>

```sh
cd "${service_save_path}"

cp -p docker-compose.yml{,.sample}

# - change image version
# MySQL 5.6 will reach EOL (“End of Life”) in February 2021. Change to 5.7
# https://www.mysql.com/support/eol-notice.html
db_version='5.7'
sed -r -i '/image.*?mysql:/{s@^(.*?mysql:).*@\1'"${db_version}"'@g}' docker-compose.yml
sed -r -i '/mysql:/,/^$/{/command:/{s@.*@& --tls_version=TLSv1.1,TLSv1.2@g}}' docker-compose.yml

# redis 6.0-alpine to 6-alpine
sed -r -i '/image.*?redis:/{s@^(.*?redis:).*@\16-alpine@g;}' docker-compose.yml


# - change standardnotes' image tag
# For latest version
sed -r -i '/image.*?(syncing-server-js|api-gateway|auth):/{s@^(.*:)[^[:space:]]+[[:space:]]*@\1latest@g;}' docker-compose.yml

# For legacy version
# sed -r -i '/image:.*?\/syncing-server-js:/{s@^(.*?\/syncing-server-js:).*@\11.42.9@g;}' docker-compose.yml  # 1.42.9
# sed -r -i '/image:.*?\/api-gateway:/{s@^(.*?\/api-gateway:).*@\11.26.4@g;}' docker-compose.yml  # 1.26.4
# sed -r -i '/image:.*?\/auth:/{s@^(.*?\/auth:).*@\11.24.4@g;}' docker-compose.yml  # 1.24.4


# - fix port map issue if still exists, otherwise custom exposed port not start properly
# change from '3000:${EXPOSED_PORT}' to '${EXPOSED_PORT}:3000'
sed -r -i '/3000:.*?EXPOSED_PORT/{s@^([^-]+-).*@\1 \${EXPOSED_PORT}:3000@g;}' docker-compose.yml


# - change volume dir
# - ./data/mysql:/var/lib/mysql
# - ./data/import:/docker-entrypoint-initdb.d
# - ./data/redis/:/data

# sed -r -i '/-[[:space:]]*\.\/data\//{s@\./@'"${data_dir}"'/@g}' docker-compose.yml
sed -r -i '/-[[:space:]]*\.\/data\/mysql/{s@\./@'"${data_dir}"'/@g}' docker-compose.yml
sed -r -i '/-[[:space:]]*\.\/data\/redis/{s@\./@'"${data_dir}"'/@g}' docker-compose.yml

# - change container name format 'auth-standalone' -> 'standalone-auth'
sed -r -i '/container_name:.*-standalone$/{s@^([^:]+:[[:space:]]*)(.*?)-(standalone)$@\1\3-\2@g}' docker-compose.yml

# - change network name (optional)
# sed -r -i '/standardnotes_standalone/{s@standardnotes_standalone@standardnotes_newtork@g}' docker-compose.yml
```

</details>

#### env

Create custom enviroment variables file

<details><summary>Click to expand command</summary>

```sh
cd "${service_save_path}"

# - initialize default configuration
./server.sh setup

# Initializing default configuration
# Default configuration files created as .env and docker/*.env files. Feel free to modify values if needed.

[[ -d "${service_env_custom_dir}" ]] || mkdir -p "${service_env_custom_dir}"

# - File .env for Syncing Server JS & Syncing Server Worker
# Set custom env
# https://docs.standardnotes.com/self-hosting/configuration-options/#syncing-server-js--syncing-server-worker
custom_env_path="${service_env_custom_dir}/env"

tee "${custom_env_path}" 1> /dev/null << EOF 
# Basics
LOG_LEVEL=warn # info
NODE_ENV=production # development

# Ports
EXPOSED_PORT=${localhost_ip}:${server_exposed_port}

# Secrets
AUTH_JWT_SECRET=$(openssl rand -hex 64)

# Database
DB_DATABASE=${service_name}
DB_USERNAME=${service_name}
DB_PASSWORD=$(openssl rand -hex 64)

# Email
EMAIL_ATTACHMENT_MAX_BYTE_SIZE=52428800  # 50MB, default is 10485760 (10MB)

# Revisions
REVISIONS_FREQUENCY=180  # seconds, default is 300

# Cache
#REDIS_URL=redis://cache:6379

# New Relic (Optional)
# NEW_RELIC_ENABLED=false

# End
EOF

# change specific directive values in file .env
sed -r -n '/^$/d;/^#/d;s@[[:space:]]+#+[^#]*$@@g;p' "${custom_env_path}" | while IFS="=" read -r var_k var_v; do   
    sed -r -i '/^#*[[:space:]]*'"${var_k}"'=/{s?.*?'"${var_k}"'='"${var_v}"'?g}' "${service_save_path}"/.env
done


# - File docker/auth.env for Auth & Auth Worker
# https://docs.standardnotes.com/self-hosting/configuration-options/#auth--auth-worker
# Set custom env
custom_env_auth_path="${service_env_custom_dir}/auth_env"

tee "${custom_env_auth_path}" 1> /dev/null << EOF 
# Basics
LOG_LEVEL=warn # info
NODE_ENV=production # development

# Secrets
JWT_SECRET=$(openssl rand -hex 64)
PSEUDO_KEY_PARAMS_KEY=$(openssl rand -hex 64)
ENCRYPTION_SERVER_KEY=$(openssl rand -hex 64 | head -c 64)  # ust be a hex string exactly 32 bytes long (length 64)
# AUTH_JWT_TTL=86400
# LEGACY_JWT_SECRET= # You don't need to change this if you are just starting to self-host your setup and do not own a legacy client application.

# Disabling new user registrations
DISABLE_USER_REGISTRATION=false

# Authentication and Authorization
MAX_LOGIN_ATTEMPTS=3
FAILED_LOGIN_LOCKOUT=600  # seconds 10min
ACCESS_TOKEN_AGE=8640000  # seconds 100d
REFRESH_TOKEN_AGE=864000  # seconds 10d
EPHEMERAL_SESSION_AGE=86400  # seconds 1d

# End
EOF

# change specific directive values in file docker/auth.env
sed -r -n '/^$/d;/^#/d;s@[[:space:]]+#+[^#]*$@@g;p' "${custom_env_auth_path}" | while IFS="=" read -r var_k var_v; do   
    sed -r -i '/^#*[[:space:]]*'"${var_k}"'=/{s?.*?'"${var_k}"'='"${var_v}"'?g}' "${service_save_path}"/docker/auth.env
done


# - File docker/api-gateway.env for API Gateway
# https://docs.standardnotes.com/self-hosting/configuration-options/#api-gateway
custom_env_api_gateway_env="${service_env_custom_dir}/api_gateway_env"

tee "${custom_env_api_gateway_env}" 1> /dev/null << EOF 
# Basics
LOG_LEVEL="info"
NODE_ENV="production"

# End
EOF

# change specific directive values in file docker/api-gateway.env
sed -r -n '/^$/d;/^#/d;s@[[:space:]]+#+[^#]*$@@g;p' "${custom_env_api_gateway_env}" | while IFS="=" read -r var_k var_v; do   
    sed -r -i '/^#*[[:space:]]*'"${var_k}"'=/{s?.*?'"${var_k}"'='"${var_v}"'?g}' "${service_save_path}"/docker/api-gateway.env
done
```

</details>


### Extension

#### docker-compose.yml

Add extension web services (optional)

```sh
cd "${service_save_path}"

network_info=$(sed -r -n '/^networks:/,/^$/{p}' docker-compose.yml)
sed -r -i '/^networks:/,$d' docker-compose.yml
sed -r -i '/extension:/,/^$/d' docker-compose.yml
# services:
tee -a docker-compose.yml 1> /dev/null << EOF
  extension:
    image: nginx:alpine
    container_name: standalone_extension
    networks:
      - standardnotes_extension
    restart: unless-stopped
    ports:
      - ${localhost_ip}:${extensions_exposed_port}:80
    volumes:
      - ${extensions_data_dir}/public:/usr/share/nginx/html:ro

${network_info}
  standardnotes_extension:
    name: standardnotes_extension

# End
EOF
```

### Web app

Self-host web app like <https://app.standardnotes.com>.

#### env

```sh
[[ -d "${service_env_web_dir}" ]] || mkdir -p "${service_env_web_dir}"
cd "${service_env_web_dir}"

# https://github.com/standardnotes/web/blob/develop/.env.sample
curl -fsSL https://raw.githubusercontent.com/standardnotes/web/develop/.env.sample > '.env.sample'

cp -p .env.sample .env

custom_env_web_path="${service_env_custom_dir}/web_env"

tee "${custom_env_web_path}" 1> /dev/null << EOF 
# Basics

# Is SECRET_KEY_BASE still necessary? https://github.com/standardnotes/standalone/issues/41
#SECRET_KEY_BASE=test

RAILS_ENV=production # development
RAILS_LOG_LEVEL=WARN

APP_HOST=https://${domain_for_web}  # https://sn.maxdsre.com
DEFAULT_SYNC_SERVER=https://${domain_for_server}  # https://snsync.maxdsre.com

NEW_RELIC_ENABLED=false

# End
EOF

sed -r -n '/^$/d;/^#/d;s@[[:space:]]+#+[^#]*$@@g;p' "${custom_env_web_path}" | while IFS="=" read -r var_k var_v; do   
    sed -r -i '/^#*[[:space:]]*'"${var_k}"'=/{s?.*?'"${var_k}"'='"${var_v}"'?g}' .env  # ./envs/web/.env
done


# For Nginx Web Server
# add_header Content-Security-Policy "default-src 'self';frame-src 'self' snsync.maxdsre.com;style-src 'self' snsync.maxdsre.com 'unsafe-inline';style-src-elem snsync.maxdsre.com 'self' 'unsafe-inline';script-src 'self' 'unsafe-inline' 'unsafe-eval';img-src 'self' data:;connect-src snsync.maxdsre.com 'self' 'unsafe-inline' data:;font-src 'self' data:";
```

#### docker-compose.yml

```sh
cd "${service_save_path}"

network_info=$(sed -r -n '/^networks:/,/^$/{p}' docker-compose.yml)
sed -r -i '/^networks:/,$d' docker-compose.yml
sed -r -i '/web:/,/^$/d' docker-compose.yml

tee -a docker-compose.yml 1> /dev/null << EOF
  web:
    image: standardnotes/web:stable  #latest is develop branch
    container_name: standalone_web
    networks:
      - standardnotes_webapp
    restart: unless-stopped
    ports:
      - ${web_exposed_port}:3001
    env_file:
      - .${service_env_web_dir##*${service_name}}/.env  # ./envs/web/.env
    volumes:
      - /etc/localtime:/etc/localtime:ro

${network_info}
  standardnotes_webapp:
    name: standardnotes_webapp

# End
EOF
```

### FileSafe (optional)

Still in test

#### env

```sh
[[ -d "${service_env_filesafe_dir}" ]] || mkdir -p "${service_env_filesafe_dir}"
cd "${service_env_filesafe_dir}"

# https://github.com/standardnotes/filesafe-relay/blob/develop/.env.sample
curl -fsSL https://raw.githubusercontent.com/standardnotes/filesafe-relay/develop/.env.sample > '.env.sample'

cp -p .env.sample .env

custom_env_filesafe_path="${service_env_custom_dir}/filesafe_env"

tee "${custom_env_filesafe_path}" 1> /dev/null << EOF 
# Basics

RAILS_ENV=production # development

# if choose production, SECRET_KEY_BASE is essential
# https://github.com/standardnotes/filesafe-relay/blob/develop/config/secrets.yml
SECRET_KEY_BASE=$(openssl rand -hex 64)

HOST=https://${domain_for_filesafe}  # https://snfilesafe.maxdsre.com

#RAILS_LOG_TO_STDOUT=false

NEW_RELIC_ENABLED=false

# https://github.com/standardnotes/filesafe-relay/blob/master/app/lib/integrations/dropbox_integration.rb#L16
#DROPBOX_CLIENT_ID=

# https://github.com/standardnotes/filesafe-relay/blob/master/docker/entrypoint.sh#L7
# https://github.com/standardnotes/filesafe-relay/blob/master/app/lib/integrations/google_drive_integration.rb
# GOOGLE_CLIENT_SECRETS=

# End
EOF

sed -r -n '/^$/d;/^#/d;s@[[:space:]]+#+[^#]*$@@g;p' "${custom_env_filesafe_path}" | while IFS="=" read -r var_k var_v; do   
    sed -r -i '/^#*[[:space:]]*'"${var_k}"'=/{s?.*?'"${var_k}"'='"${var_v}"'?g}' .env  # ./envs/filesafe/.env
done

sed -r -i '/^SECRET_KEY_BASE=/d' .env
sed -r -n '/^SECRET_KEY_BASE=/{p}' "${custom_env_filesafe_path}" | tee -a 1> .env
```

#### docker-compose.yml

```sh
cd "${service_save_path}"

network_info=$(sed -r -n '/^networks:/,/^$/{p}' docker-compose.yml)
sed -r -i '/^networks:/,$d' docker-compose.yml
sed -r -i '/filesafe:/,/^$/d' docker-compose.yml

tee -a docker-compose.yml 1> /dev/null << EOF
  filesafe:
    image: standardnotes/filesafe-relay:latest
    container_name: standalone_filesafe
    networks:
      - standardnotes_filesafe
    restart: unless-stopped
    ports:
      - ${filesafe_exposed_port}:3000
    env_file:
      - .${service_env_filesafe_dir##*${service_name}}/.env  # ./envs/filesafe/.env
    volumes:
      - /etc/localtime:/etc/localtime:ro

${network_info}
  standardnotes_filesafe:
    name: standardnotes_filesafe

# End
EOF
```

## Deployment

### Running Containers

```sh
cd "${service_save_path}"

./server.sh start

# check log to debug
# ./server.sh logs
```

<details><summary>Click to expand output info</summary>

```txt
Starting up infrastructure
Creating standardnotes_db_1    ... done
Creating standardnotes_cache_1 ... done
Creating standardnotes_syncing-server-js_1 ... done
Creating standardnotes_syncing-server-js-worker_1 ... done
Creating standardnotes_auth_1                     ... done
Creating standardnotes_auth-worker_1              ... done
Creating standardnotes_api-gateway_1              ... done
Infrastructure started. Give it a moment to warm up. If you wish please run the './server.sh logs' command to see details.
```

</details>

It needs some time to warm up. If you wish please run the './server.sh logs' command to see details.

Now you can check if it works properly via command

```sh
curl http://localhost:${server_exposed_port:-8200}/healthcheck
```

Output *OK*.

<details><summary>Click to expand deprecated method</summary>

Now you can check if it works properly via command `curl 127.0.0.1:${server_exposed_port:-8200}`

<details><summary>Click to expand verification info</summary>

Output

>{"message":"Hi! You're not supposed to be here."}

Log output

>api-gateway_1               | {"meta":{"req":{"url":"/","headers":{"x-real-ip":"127.0.0.1","x-forwarded-for":"127.0.0.1","x-forwarded-proto":"https","connection":"close","user-agent":"curl/7.64.0","accept":"*/*"},"method":"GET","httpVersion":"1.0","originalUrl":"/","query":{}},"res":{"statusCode":200},"responseTime":14},"level":"info","message":"HTTP GET /"}

</details>

</details>


### Nginx Reverse Proxy

Nginx

```sh
# service_name='standardnotes'
# server_exposed_port='8200'  # default 3000
# extensions_exposed_port='8201'
# web_exposed_port='8202'  # default 3001

backend_private_ip=${backend_private_ip:-'127.0.0.1'}
# backend_private_ip='192.168.8.4'

# Nginx site config dir /etc/nginx/sites-enabled/
nginx_conf_path="/etc/nginx/sites-enabled/${service_name}.conf"
sudo mkdir -p "${nginx_conf_path%/*}"
```

#### Conf

<details><summary>Click to expand config info</summary>

```conf
sudo tee "${nginx_conf_path}" 1> /dev/null << EOF
upstream ${service_name}-default {
    server ${backend_private_ip}:${server_exposed_port};
}

upstream ${service_name}-extensions {
    server ${backend_private_ip}:${extensions_exposed_port};
}

upstream ${service_name}-webapp {
    server ${backend_private_ip}:${web_exposed_port};
}

upstream ${service_name}-filesafe {
    server ${backend_private_ip}:${filesafe_exposed_port};
}

# - For Sync Server
# Redirect HTTP to HTTPS
server {
    listen 80;
    # listen [::]:80 ipv6only=on;
    server_name ${domain_for_server};
    return 301 https://\$host\$request_uri;
}

server {
    listen 443 ssl http2;
    # listen [::]:443 ssl http2 ipv6only=on;
    server_name ${domain_for_server};

    error_log  /var/log/nginx/${domain_for_server%%.*}_error.log warn;  # [debug|info|notice|warn|error|crit|alert|emerg]
    access_log /var/log/nginx/${domain_for_server%%.*}_access.log main; # format name 'main' instead of predefined 'combined' format

    include /etc/nginx/conf.d/section/ssl.conf;

    location / {
        proxy_set_header Host \$host;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto \$scheme;

        proxy_pass http://${service_name}-default;

        # https://serverfault.com/questions/587386/an-upstream-response-is-buffered-to-a-temporary-file
        proxy_buffers 16 1M; 
        proxy_buffer_size 10M;
        proxy_busy_buffers_size 10M;
    }

    # https://github.com/iganeshk/standardnotes-extensions#setup-with-nginx
    location ^~ /extensions/ {
        autoindex off;
        #alias /path/to/standardnotes-extensions/public;
        proxy_pass http://${service_name}-extensions/;

        # CORS HEADERS
        if (\$request_method = 'OPTIONS') {
            add_header 'Access-Control-Allow-Origin' '*';
            add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
            add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range';
            # Tell client that this pre-flight info is valid for 20 days
            add_header 'Access-Control-Max-Age' 1728000;
            add_header 'Content-Type' 'text/plain; charset=utf-8';
            add_header 'Content-Length' 0;
            return 204;
		    }
        if (\$request_method = 'POST') {
                add_header 'Access-Control-Allow-Origin' '*';
                add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
                add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range';
                add_header 'Access-Control-Expose-Headers' 'Content-Length,Content-Range';
        }
		    if (\$request_method = 'GET') {
            add_header 'Access-Control-Allow-Origin' '*';
            add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
            add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range';
            add_header 'Access-Control-Expose-Headers' 'Content-Length,Content-Range';
		    }
	  }
}

# - For Web app
# Redirect HTTP to HTTPS
server {
    listen 80;
    # listen [::]:80 ipv6only=on;
    server_name ${domain_for_web};
    return 301 https://\$host\$request_uri;
}

server {
    listen 443 ssl http2;
    # listen [::]:443 ssl http2 ipv6only=on;
    server_name ${domain_for_web};

    error_log  /var/log/nginx/${domain_for_web%%.*}_error.log warn;  # [debug|info|notice|warn|error|crit|alert|emerg]
    access_log /var/log/nginx/${domain_for_web%%.*}_access.log main; # format name 'main' instead of predefined 'combined' format

    include /etc/nginx/conf.d/section/ssl.conf;

    location / {
        proxy_set_header Host \$host;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto \$scheme;

        add_header Content-Security-Policy "default-src 'self';frame-src 'self' ${domain_for_server};style-src 'self' ${domain_for_server} 'unsafe-inline';style-src-elem ${domain_for_server} 'self' 'unsafe-inline';script-src 'self' 'unsafe-inline' 'unsafe-eval';img-src 'self' data:;connect-src ${domain_for_server} 'self' 'unsafe-inline' data:;font-src 'self' data:";

        proxy_pass http://${service_name}-webapp;

        # https://serverfault.com/questions/587386/an-upstream-response-is-buffered-to-a-temporary-file
        proxy_buffers 16 1M; 
        proxy_buffer_size 10M;
        proxy_busy_buffers_size 10M;
    }
}

# - For Filesafe Relay
# Redirect HTTP to HTTPS
server {
    listen 80;
    # listen [::]:80 ipv6only=on;
    server_name ${domain_for_filesafe};
    return 301 https://\$host\$request_uri;
}

server {
    listen 443 ssl http2;
    # listen [::]:443 ssl http2 ipv6only=on;
    server_name ${domain_for_filesafe};

    error_log  /var/log/nginx/${domain_for_filesafe%%.*}_error.log warn;  # [debug|info|notice|warn|error|crit|alert|emerg]
    access_log /var/log/nginx/${domain_for_filesafe%%.*}_access.log main; # format name 'main' instead of predefined 'combined' format

    include /etc/nginx/conf.d/section/ssl.conf;

    location / {
        proxy_set_header Host \$host;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto \$scheme;

        proxy_pass http://${service_name}-filesafe;

        # https://serverfault.com/questions/587386/an-upstream-response-is-buffered-to-a-temporary-file
        proxy_buffers 16 1M; 
        proxy_buffer_size 10M;
        proxy_busy_buffers_size 10M;
    }
}

# End
EOF
```

</details>

Testing Nginx conf syntax then reload

```sh
sudo -i

nginx -t && nginx -s reload

# nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
# nginx: configuration file /etc/nginx/nginx.conf test is successful
```

Checking if domain works

```sh
curl -fSL "${domain_for_server}"/healthcheck && echo ''

# output
OK

# deprecated output
# {"message":"Hi! You're not supposed to be here."}
```

</details>

Now that you own self-hosted Standard Notes.

But if you wanna embrace the power of Standard Notes, you need to install extensions. Luckily, you can also self-host your extensions.

## Extensions

Official doc [Intro to Extensions](https://docs.standardnotes.com/extensions/intro/#sustainability) says:

>Most of our extensions are open-source and available for self-hosting. You can also learn to develop your own extensions by following the guides on this site. However, we encourage you to support the sustainability and future development of this project by [purchasing a subscription](https://standardnotes.com/extensions).

By self-hosting Standard Notes extensions, you're bypassing the need to purchase a license. If possible, please purchase a license to support [Standard Notes][standard_notes].

Github project [iganeshk/standardnotes-extensions](https://github.com/iganeshk/standardnotes-extensions) is a self-host standard notes extensions repository.

**Attention**: From web *v3.9.0* (Nov 10, 2021), standardnotes redesigns its official website and subscription architecture. More etails see <https://github.com/standardnotes/desktop/releases/tag/v3.9.0>

The last legacy version support self-host extension managerment is [v3.8.21](https://github.com/standardnotes/desktop/releases/tag/v3.8.21
).

### Project Code

```sh
mkdir -p "${extensions_repo_save_path}"
# git clone https://github.com/iganeshk/standardnotes-extensions.git "${extensions_repo_save_path}"
curl -fsSL https://github.com/iganeshk/standardnotes-extensions/archive/master.tar.gz | tar zxf - --strip-component=1 -C "${extensions_repo_save_path}"
```

### .env

This repo needs you to generate GitHub token to make its script work.

<details><summary>Click to expand custom env</summary>

```sh
# service_name='standardnotes'

cd "${extensions_repo_save_path}"

# cp env.sample .env

# Please change to you own GitHub token
github_username='XXXXXXX'
github_token='6a3d58a492984f56ed1c3939a4070388147626f0'

tee .env 1> /dev/null << EOF
# https://snsync.maxdsre.com/extensions
domain: https://${domain_for_server}/extensions

github:
  username: ${github_username}
  token: ${github_token}
EOF
```

</details>

### docker-compose.yml

Creating custom *docker-compose.yml*.

Here choose build image based [Dockerfile](https://github.com/iganeshk/standardnotes-extensions/blob/master/Dockerfile), you can also use image [mtoohey/standardnotes-extensions](https://hub.docker.com/r/mtoohey/standardnotes-extensions) provided by project maintainer.

Running via Nginx image which exposes a TCP port.

<details><summary>Click to expand custom docker-compose.yml</summary>

```yml
cd "${extensions_repo_save_path}"

# - Create custom docker-compose.yml
tee docker-compose.yml 1> /dev/null << EOF
version: '3'

services:
  nginx:
    image: nginx:alpine
    container_name: standardnotes_extension_web
    networks:
      - standardnotes_extension
    restart: unless-stopped
    ports:
      - ${extensions_exposed_port}:80
    volumes:
      - ${extensions_data_dir}/public:/usr/share/nginx/html:ro

  standardnotes-extensions:
    build: .
    # image: mtoohey/standardnotes-extensions
    restart: "no"
    volumes:
      - ${extensions_repo_save_path}/.env:/build/.env
      - ${extensions_repo_save_path}/extensions:/build/extensions
      - ${extensions_data_dir}/public:/build/public

networks:
  standardnotes_extension:
    name: standardnotes_extension
EOF

# - Running container
docker compose up -d  # Via docker compose (For docker-compose V2)
# docker-compose up -d  # Via docker-compose (For docker-compose V1)

# WARNING: Image for service standardnotes-extensions was built because it did not already exist. To rebuild this image you must use `docker-compose build` or `docker-compose up --build`.
```

</details>

### Verifying

Verifying the Standard Notes Extensions endpoint

```http
https://snsync.maxdsre.com/extensions/index.json
```

```sh
curl -I https://snsync.maxdsre.com/extensions/index.json

# output
HTTP/2 200
server: nginx
...
...
```

Testing account

Item|Value
---|---
Email|test@maxdsre.com
Password|standard_notes@2021


### Extended Activation Code

To enable self-host extensions, you need activate it via *Extended Activation Code*, here is

>https://snsync.maxdsre.com/extensions/index.json

Just copy this link into the form, then click button `Submit Code`.

<details><summary>Click to expand interface snapshot</summary>

Extended activation code interface

![Extended activation code interface](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/03-1_extension_license_code.png)

Extended activation code submit

![Extended activation code submit](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/03-2_extension_license_code_submit.png)

</details>


## Snapshots

### Verification

<details><summary>Click to expand verification snapshot</summary>

Server service verification

![standard notes verification](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/01-1_verification.png)

Extensions verification

![standard notes extensions verification](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/01-2_extensions_verification.png)


</details>

### Desktop Client

Main interface

<details><summary>Click to expand main interface snapshot</summary>

![desktop main interface](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/02-1_desktop_main_page.png)

</details>

#### Register & Login

Register interface

<details><summary>Click to expand register interface snapshot</summary>

![desktop register interface](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/02-2_desktop_register.png)

![desktop register key generation](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/02-3_desktop_register_key_generating.png)

</details>

Login info

![desktop login info](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/02-4_desktop_login_info.png)

### Extensions

Extended Activation Code

<details><summary>Click to expand snapshot</summary>

Extended activation code interface

![Extended activation code interface](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/03-1_extension_license_code.png)

Extended activation code submit

![Extended activation code submit](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/03-2_extension_license_code_submit.png)

Extensions license activated

![Extensions license activated](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/03-3_extension_license_activated.png)

</details>

Extension activated overview

<details><summary>Click to expand overview snapshot</summary>

![extensions overview](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/04-1_extension_overview.png)

Extension activate welcome info

![Extension activate welcome info](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/04-2_extension_activate.png)

</details>

Extensions installed overview

![extensions installed overview](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/04-3_extensions_installed.png)

### Editor

Editor extension prompt info

<details><summary>Click to expand snapshot</summary>

![Editor extension prompt info](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/05-1_editor_prompt_info.png)

</details>

Editor interface overview

![Editor interface overview](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/06-1_desktop_editor_overview.png)

### Web Site

<details><summary>Click to expand website snapshot</summary>

Web site main page

![Web site main page](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/07-1_web_main_page.png)

Sign in page

![Sign in page](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/07-2_web_signin.png)

Login success

![Login success](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/07-3_web_login_success.png)

Check active sessions

![Check active sessions](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/07-4_web_active_sessions.png)

</details>

Account info overview

![Account info overview](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/07-5_web_account_info.png)

</details>

### Editor Tabs

Editor function tabs

<details><summary>Click to expand snapshot</summary>

Tab - options

![Tab - options](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/08-1_tab_options.png)

Tab - editor

![Tab - editor](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/08-2_tab_editor.png)

Tab - actions

![Tab - actions](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/08-3_tab_actions.png)

Tab - history

![Tab - history](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/08-4_tab_history.png)

</details>


## Database Dump

Official doc [Migrating From Legacy](https://docs.standardnotes.com/self-hosting/legacy-migration#preparing-a-database-dump), but it doesn't work. From issue [Trying to migrate from old self-hosted setup](https://github.com/standardnotes/standalone/issues/9#issuecomment-864453645) find the correct command.


```sh
# change path to docker compose saved dir
sql_save_path=${sql_save_path:-"$PWD/data/import/dbdump_$(date +'%Y%m%d_%H%M%S').sql"}

db_database=$(sed -r -n '/^DB_DATABASE=/{s@^[^=]+=@@g;p;q}' .env 2> /dev/null)
db_password=$(sed -r -n '/^DB_PASSWORD=/{s@^[^=]+=@@g;p;q}' .env 2> /dev/null)

# method 1 - via `docker exec`
db_container_id=$(sed -r -n '/image:[[:space:]]*mysql:/,/^$/{/container_name:/{s@^[^:]+:[[:space:]]*@@g;p;q}}' docker-compose.yml | xargs -I %s docker ps --filter name="%s" --format "{{.ID}}")
docker exec "${db_container_id}" sh -c 'exec mysqldump '${db_database}' -uroot -p"'${db_password}'" 2> /dev/null' > "${sql_save_path}"

# method 2 - via `docker compose exec`
db_service_name=${db_service_name:-'db'}  # db is service name defined in compose file
db_service_name=$(sed -r -n '0,/image:[[:space:]]*mysql:/I{/image:[[:space:]]*mysql:/{x;s@^[[:space:]]*([^:]+).*@\1@g;p}};h' docker-compose.yml)
docker compose exec "${db_service_name}" sh -c 'exec mysqldump '${db_database}' -uroot -p"'${db_password}'" 2> /dev/null' > "${sql_save_path}"

# cp -f "${sql_save_path}" "${sql_save_path%/*}/dbdump.sql"
# find "${sql_save_path%/*}" -type f -size +2k -name '*.sql' -print | sort -r | sed '1,3d' | xargs -I %s rm -f %s  # just keep latest 3 versions

# export create table statements
# sed -r -n '/^CREATE/,/^$/{/\//d;p}' /tmp/sn_dbdump.sql > /tmp/sn_dbdump.sql
```

<details><summary>Click to expand sql statements</summary>

Dump date: Nov 15, 2021 Mon

```sql
CREATE TABLE `extension_settings` (
  `uuid` varchar(36) NOT NULL,
  `extension_id` varchar(255) DEFAULT NULL,
  `mute_emails` tinyint(1) DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`uuid`),
  KEY `index_extension_settings_on_extension_id` (`extension_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `items` (
  `uuid` varchar(36) NOT NULL,
  `duplicate_of` varchar(36) DEFAULT NULL,
  `items_key_id` varchar(255) DEFAULT NULL,
  `content` mediumtext,
  `content_type` varchar(255) NOT NULL,
  `enc_item_key` text,
  `auth_hash` varchar(255) DEFAULT NULL,
  `user_uuid` varchar(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `last_user_agent` text,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `created_at_timestamp` bigint(20) NOT NULL,
  `updated_at_timestamp` bigint(20) NOT NULL,
  PRIMARY KEY (`uuid`),
  KEY `index_items_on_content_type` (`content_type`),
  KEY `index_items_on_user_uuid` (`user_uuid`),
  KEY `index_items_on_deleted` (`deleted`),
  KEY `updated_at_timestamp` (`updated_at_timestamp`),
  KEY `user_uuid_and_updated_at_timestamp_and_created_at_timestamp` (`user_uuid`,`updated_at_timestamp`,`created_at_timestamp`),
  KEY `index_items_on_user_uuid_and_content_type` (`user_uuid`,`content_type`),
  KEY `user_uuid_and_deleted` (`user_uuid`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

CREATE TABLE `offline_settings` (
  `uuid` varchar(36) NOT NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text,
  `server_encryption_version` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` bigint(20) NOT NULL,
  `updated_at` bigint(20) NOT NULL,
  PRIMARY KEY (`uuid`),
  KEY `index_offline_settings_on_name_and_email` (`name`,`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `offline_user_roles` (
  `role_uuid` varchar(36) NOT NULL,
  `offline_user_subscription_uuid` varchar(36) NOT NULL,
  PRIMARY KEY (`role_uuid`,`offline_user_subscription_uuid`),
  KEY `IDX_027ba99043e6902f569d66417f` (`role_uuid`),
  KEY `IDX_cd1b91693f6ee92d5f94ce2775` (`offline_user_subscription_uuid`),
  CONSTRAINT `FK_027ba99043e6902f569d66417f0` FOREIGN KEY (`role_uuid`) REFERENCES `roles` (`uuid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_cd1b91693f6ee92d5f94ce27758` FOREIGN KEY (`offline_user_subscription_uuid`) REFERENCES `offline_user_subscriptions` (`uuid`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `offline_user_subscriptions` (
  `uuid` varchar(36) NOT NULL,
  `email` varchar(255) NOT NULL,
  `plan_name` varchar(255) NOT NULL,
  `ends_at` bigint(20) NOT NULL,
  `created_at` bigint(20) NOT NULL,
  `updated_at` bigint(20) NOT NULL,
  `cancelled` tinyint(1) NOT NULL DEFAULT '0',
  `subscription_id` int(11) NOT NULL,
  PRIMARY KEY (`uuid`),
  KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `permissions` (
  `uuid` varchar(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`uuid`),
  UNIQUE KEY `index_permissions_on_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `revisions` (
  `uuid` varchar(36) NOT NULL,
  `item_uuid` varchar(36) NOT NULL,
  `content` mediumtext,
  `content_type` varchar(255) DEFAULT NULL,
  `items_key_id` varchar(255) DEFAULT NULL,
  `enc_item_key` text,
  `auth_hash` varchar(255) DEFAULT NULL,
  `creation_date` date DEFAULT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `updated_at` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`uuid`),
  KEY `index_revisions_on_creation_date` (`creation_date`),
  KEY `index_revisions_on_created_at` (`created_at`),
  KEY `FK_ab3b92e54701fe3010022a31d90` (`item_uuid`),
  CONSTRAINT `FK_ab3b92e54701fe3010022a31d90` FOREIGN KEY (`item_uuid`) REFERENCES `items` (`uuid`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `revoked_sessions` (
  `uuid` varchar(36) NOT NULL,
  `user_uuid` varchar(36) NOT NULL,
  `received` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`uuid`),
  KEY `index_revoked_sessions_on_user_uuid` (`user_uuid`),
  CONSTRAINT `FK_b357d1397b82bcda5e6cc9b0062` FOREIGN KEY (`user_uuid`) REFERENCES `users` (`uuid`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `role_permissions` (
  `permission_uuid` varchar(36) NOT NULL,
  `role_uuid` varchar(36) NOT NULL,
  PRIMARY KEY (`permission_uuid`,`role_uuid`),
  KEY `IDX_f985b194ff27dde81fb470c192` (`permission_uuid`),
  KEY `IDX_7be6db7b59fb622e6c16ba124c` (`role_uuid`),
  CONSTRAINT `FK_7be6db7b59fb622e6c16ba124c8` FOREIGN KEY (`role_uuid`) REFERENCES `roles` (`uuid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_f985b194ff27dde81fb470c1920` FOREIGN KEY (`permission_uuid`) REFERENCES `permissions` (`uuid`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `roles` (
  `uuid` varchar(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`uuid`),
  UNIQUE KEY `index_roles_on_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `sessions` (
  `uuid` varchar(36) NOT NULL,
  `user_uuid` varchar(255) DEFAULT NULL,
  `hashed_access_token` varchar(255) NOT NULL,
  `hashed_refresh_token` varchar(255) NOT NULL,
  `access_expiration` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `refresh_expiration` datetime NOT NULL,
  `api_version` varchar(255) DEFAULT NULL,
  `user_agent` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`uuid`),
  KEY `index_sessions_on_user_uuid` (`user_uuid`),
  KEY `index_sessions_on_updated_at` (`updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `settings` (
  `uuid` varchar(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text,
  `server_encryption_version` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` bigint(20) NOT NULL,
  `updated_at` bigint(20) NOT NULL,
  `user_uuid` varchar(36) NOT NULL,
  `sensitive` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uuid`),
  KEY `FK_1cc1d030b83d6030795d3e7e63f` (`user_uuid`),
  KEY `index_settings_on_name_and_user_uuid` (`name`,`user_uuid`),
  KEY `index_settings_on_updated_at` (`updated_at`),
  CONSTRAINT `FK_1cc1d030b83d6030795d3e7e63f` FOREIGN KEY (`user_uuid`) REFERENCES `users` (`uuid`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `user_roles` (
  `role_uuid` varchar(36) NOT NULL,
  `user_uuid` varchar(36) NOT NULL,
  PRIMARY KEY (`role_uuid`,`user_uuid`),
  KEY `IDX_0ea82c7b2302d7af0f8b789d79` (`role_uuid`),
  KEY `IDX_2ebc2e1e2cb1d730d018893dae` (`user_uuid`),
  CONSTRAINT `FK_0ea82c7b2302d7af0f8b789d797` FOREIGN KEY (`role_uuid`) REFERENCES `roles` (`uuid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_2ebc2e1e2cb1d730d018893daef` FOREIGN KEY (`user_uuid`) REFERENCES `users` (`uuid`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `user_subscriptions` (
  `uuid` varchar(36) NOT NULL,
  `plan_name` varchar(255) NOT NULL,
  `ends_at` bigint(20) NOT NULL,
  `created_at` bigint(20) NOT NULL,
  `updated_at` bigint(20) NOT NULL,
  `user_uuid` varchar(36) NOT NULL,
  `cancelled` tinyint(4) NOT NULL DEFAULT '0',
  `subscription_id` int(11) NOT NULL,
  PRIMARY KEY (`uuid`),
  KEY `updated_at` (`updated_at`),
  KEY `FK_f44dae0a64c70e6b50de5442d2b` (`user_uuid`),
  CONSTRAINT `FK_f44dae0a64c70e6b50de5442d2b` FOREIGN KEY (`user_uuid`) REFERENCES `users` (`uuid`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `users` (
  `uuid` varchar(36) NOT NULL,
  `version` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `pw_nonce` varchar(255) DEFAULT NULL,
  `kp_created` varchar(255) DEFAULT NULL,
  `kp_origination` varchar(255) DEFAULT NULL,
  `pw_cost` int(11) DEFAULT NULL,
  `pw_key_size` int(11) DEFAULT NULL,
  `pw_salt` varchar(255) DEFAULT NULL,
  `pw_alg` varchar(255) DEFAULT NULL,
  `pw_func` varchar(255) DEFAULT NULL,
  `encrypted_password` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `locked_until` datetime DEFAULT NULL,
  `num_failed_attempts` int(11) DEFAULT NULL,
  `updated_with_user_agent` text,
  `encrypted_server_key` varchar(255) DEFAULT NULL,
  `server_encryption_version` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uuid`),
  KEY `index_users_on_email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```

</details>

### Importing Database Dump

Official doc [Importing your database dump](https://docs.standardnotes.com/self-hosting/legacy-migration#importing-your-database-dump)

>In order to import your data to the database that our Standalone setup will create, just place your *dbdump.sql* file inside the `path-to-your-standalone/data/import` folder. The data will be imported once the setups starts.


## Issues Occuring

### Fail to connect database

After change directive `DB_PASSWORD` in file *.env*, it fails to connect the database while try to start containers.

<details><summary>Click to expand error log</summary>

```txt
syncing-server-js_1         | [Error] PoolCluster : Error: Access denied for user 'std_notes_user'@'172.18.0.2' (using password: YES)
db_1                        | 2021-03-11T15:25:23.595656Z 9 [Note] Access denied for user 'std_notes_user'@'172.18.0.2' (using password: YES)
syncing-server-js_1         | [Error] PoolCluster : Error: Access denied for user 'std_notes_user'@'172.18.0.2' (using password: YES)
syncing-server-js_1         | (node:64) UnhandledPromiseRejectionWarning: Error: Pool does Not exists.
syncing-server-js_1         |     at PoolNamespace.getConnection (/var/www/node_modules/mysql2/lib/pool_cluster.js:36:17)
syncing-server-js_1         |     at /var/www/node_modules/mysql2/lib/pool_cluster.js:43:21
syncing-server-js_1         |     at /var/www/node_modules/mysql2/lib/pool_cluster.js:210:18
syncing-server-js_1         |     at /var/www/node_modules/mysql2/lib/pool.js:60:18
syncing-server-js_1         |     at PoolConnection.<anonymous> (/var/www/node_modules/mysql2/lib/connection.js:739:13)
syncing-server-js_1         |     at Object.onceWrapper (events.js:422:26)
syncing-server-js_1         |     at PoolConnection.emit (events.js:327:22)
syncing-server-js_1         |     at PoolConnection.EventEmitter.emit (domain.js:486:12)
syncing-server-js_1         |     at PoolConnection._notifyError (/var/www/node_modules/mysql2/lib/connection.js:225:12)
syncing-server-js_1         |     at ClientHandshake.<anonymous> (/var/www/node_modules/mysql2/lib/connection.js:114:14)
syncing-server-js_1         | (Use `node --trace-warnings ...` to show where the warning was created)
syncing-server-js_1         | (node:64) UnhandledPromiseRejectionWarning: Unhandled promise rejection. This error originated either by throwing inside of an async function without a catch block, or by rejecting a promise which was not handled with .catch(). To terminate the node process on unhandled promise rejection, use the CLI flag `--unhandled-rejections=strict` (see https://nodejs.org/api/cli.html#cli_unhandled_rejections_mode). (rejection id: 1)
syncing-server-js_1         | (node:64) [DEP0018] DeprecationWarning: Unhandled promise rejections are deprecated. In the future, promise rejections that are not handled will terminate the Node.js process with a non-zero exit code.
syncing-server-js_1         | Done in 1.55s.
standardnotes_syncing-server-js_1 exited with code 0
api-gateway_1               | nc: bad address 'syncing-server-js'
...
...
...
auth_1                      | nc: bad address 'syncing-server-js'
auth_1                      | syncing-server-js:3000 is unavailable - sleeping
api-gateway_1               | nc: bad address 'syncing-server-js'
api-gateway_1               | syncing-server-js:3000 is unavailable - sleeping
syncing-server-js-worker_1  | nc: bad address 'syncing-server-js'
syncing-server-js-worker_1  | syncing-server-js:3000 is unavailable yet - waiting for it to start
...

```

</details>

Solution is keep the origin password, don't change it.


### Waiting for Key

>This note is awaiting its encryption key to be ready. Please wait for syncing to complete for this note to be decrypted.

Don't know how this happens.

### FileSafe Secret Key

Docker log

>An unhandled lowlevel error occurred. The application logs may have details.

Logs

><RuntimeError: Missing `secret_key_base` for 'production' environment, set this value in `config/secrets.yml`>

From project file [secrets.yml](https://github.com/standardnotes/filesafe-relay/blob/master/config/secrets.yml), if you change `RAILS_ENV` from *development* to *production*, you must specify environment parameter `SECRET_KEY_BASE` into docker compose file *.env*.

~~[What's the correct way of defining secret_key_base on Rails 6?](https://stackoverflow.com/questions/60702248/whats-the-correct-way-of-defining-secret-key-base-on-rails-6)~~

Docker log

>ArgumentError (No client_secrets.json filename supplied and/or could not be found in search path.):

Specify enviroment parameter `GOOGLE_CLIENT_SECRETS` into docker compose file *.env*.


## Tutorials

* [How to completely self-host Standard Notes](https://theselfhostingblog.com/posts/how-to-completely-self-host-standard-notes/)
* [Self-Host and Dockerize Standard Notes Extensions with Docker-Compose](https://return2.net/dockerize-standard-notes-extensions/)
* [Standard Notes - Docker self-hosted alternative for all your note needs](https://www.blackvoid.club/standard-notes-docker-self-hosted-alternative/)


## Resources

* [GitHub - Awesome Standard Notes](https://github.com/jonhadfield/awesome-standard-notes)


## Change Log

* Jan 02, 2022 19:40 Sun ET
  * add database dump and import action
* Nov 16, 2021 12:51 Tue ET
  * add web app
* Nov 02, 2021 15:12 Tue ET
  * update contents to make application works
* Oct 03, 2021 18:18 Sun ET
  * change docker compose from V1 to [V2](https://github.com/docker/compose/releases/tag/v2.0.0)
* Jul 15, 2021 06:50 Thu ET
  * project [standardnotes/syncing-server][syncing_server] is deprecated, change to [standardnotes/standalone][standalone]
* Mar 11, 2021 17:45 Thu ET
  * rewrite, add extensions support
* Mar 05, 2021 14:30 Fri ET
  * First draft


[standard_notes]:https://standardnotes.com "A Simple And Private Notes App"
[docker]: https://www.docker.com
[docker_compose]:https://docs.docker.com/compose/
[api_gateway]:https://github.com/standardnotes/api-gateway "router and proxy"
[auth]:https://github.com/standardnotes/auth "authorization and authentication"
[syncing_server_js]:https://github.com/standardnotes/syncing-server-js "core logic"
[syncing_server]:https://github.com/standardnotes/syncing-server
[standalone]:https://github.com/standardnotes/standalone
[web]:https://github.com/standardnotes/web
[filesafe_relay]:https://github.com/standardnotes/filesafe-relay "Relay server used to communicate between FileSafe client extension and server destination (Dropbox, Google Drive, WebDAV)"
[filesend]:https://github.com/standardnotes/FileSend "Simple, encrypted file sharing."

<!-- End -->