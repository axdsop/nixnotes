# Self-hosting Standard Notes (Deprecated)

**Attention**: This note is just for <https://github.com/standardnotes/syncing-server> which has been deprecated.

[Standard Notes][standard_notes] is free, open-source, and completely encrypted notes app. Deme site <https://app.standardnotes.org>.

Client download page <https://standardnotes.org/download>.

Unsolved issue

* [User management](#user-management)
* Android app default font size too tiny


## TOC

1. [Official Site](#official-site)  
1.1 [Documentation](#documentation)  
2. [Prerequisite](#prerequisite)  
2.1 [Essential packages](#essential-packages)  
2.2 [Docker Compose](#docker-compose)  
3. [Deployment](#deployment)  
3.1 [Variables Definition](#variables-definition)  
3.2 [Project Code](#project-code)  
3.3 [Configs](#configs)  
3.3.1 [docker-compose.yml](#docker-composeyml)  
3.3.2 [.env](#env)  
3.4 [Running Containers](#running-containers)  
3.5 [Nginx Reverse Proxy](#nginx-reverse-proxy)  
3.5.1 [Conf](#conf)  
4. [Extensions](#extensions)  
4.1 [Project Code](#project-code-1)  
4.2 [.env](#env)  
4.3 [docker-compose.yml](#docker-composeyml)  
4.4 [Verifying](#verifying)  
4.5 [Extended Activation Code](#extended-activation-code)  
5. [User Management](#user-management)  
5.1 [User Delete (not work)](#user-delete-not-work)  
6. [Snapshots](#snapshots)  
6.1 [Verification](#verification)  
6.2 [Desktop Client](#desktop-client)  
6.2.1 [Register & Login](#register-login)  
6.3 [Extensions](#extensions-1)  
6.4 [Editor](#editor)  
6.5 [Web Site](#web-site)  
6.6 [Editor Tabs](#editor-tabs)  
7. [Issues Occuring](#issues-occuring)  
7.1 [Fail to connect database](#fail-to-connect-database)  
8. [Tutorials](#tutorials)  
9. [Resources](#resources)  
10. [Change Log](#change-log)  


## Official Site

Item|Url
---|---
Official site|https://standardnotes.org
Official doc|https://docs.standardnotes.org/
GitHub|https://github.com/standardnotes/<br/>https://github.com/standardnotes/syncing-server (deprecated)


### Documentation

* [Help & Support](https://standardnotes.org/help)
* [Can I self-host Standard Notes?](https://standardnotes.org/help/47/can-i-self-host-standard-notes)
* [Self-hosting with Docker](https://docs.standardnotes.org/self-hosting/docker)
* [Getting Started with Self-hosting](https://docs.standardnotes.org/self-hosting/getting-started/)


## Prerequisite

### Essential packages

```sh
sudo apt-get update
sudo apt-get upgrade

# default-libmysqlclient-dev - MySQL database development files (metapackage)
sudo apt-get install -y default-libmysqlclient-dev
# sudo apt-get install -y git # optional
```

### Docker Compose

[Docker Compose][docker_compose] is a tool for defining and running multi-container Docker applications. Official install doc [Install Docker Compose](https://docs.docker.com/compose/install/).

I write a Shell script [docker_compose.sh](/CloudComputing/Containers/Docker/Scripts/docker_compose.sh) to install it.

```sh
download_command='wget -qO-'
[[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && download_command='curl -fsSL'

${download_command} https://gitlab.com/axdsop/nixnotes/raw/master/CloudComputing/Containers/Docker/Scripts/docker_compose.sh | sudo bash -s --
```


## Deployment

[Standard Notes][standard_notes] server is running as a [Docker][docker] container.

Cloud firewalls define `reverse` droplet can listen TCP port range (8000-9000) exposed by `backend` droplets which running docker service.

I write a note [Secure Nginx With Let's Encrypt Free SSL Certificate](/CyberSecurity/WebServer/Nginx/IssueSSLCert.md) to configure Nginx.


Following blog [How to completely self-host Standard Notes](https://theselfhostingblog.com/posts/how-to-completely-self-host-standard-notes/)

### Variables Definition

```sh
service_name='standardnotes'
service_save_path="$HOME/.docker/${service_name}"
mkdir -p "${service_save_path}"
data_dir="/data/${service_name}"

exposed_port='8200'  # default 3000

# - For extensions (optional)
extensions_exposed_port='8201'
extensions_repo_save_path="${service_save_path%/*}/standardnotes-extensions"
extensions_data_dir="${data_dir}/extensions"
```

### Project Code

Download project code

```sh
# method 1 - via git
# git clone --single-branch --branch master https://github.com/standardnotes/syncing-server.git "${service_save_path}"

# method 2 - via tar
curl -fsSL https://github.com/standardnotes/syncing-server/archive/master.tar.gz | tar zxf - --strip-component=1 -C "${service_save_path}"
```

[Dockerfile](https://github.com/standardnotes/syncing-server/blob/master/Dockerfile) uses image *ruby:2.6.5-alpine*, if I try to use image *ruby:3-alpine*, it prompts error

>Fetching gem metadata from https://rubygems.org/.........
>listen-3.2.1 requires ruby version >= 2.2.7, ~> 2.2, which is incompatible with the current version, ruby 3.0.0p0


### Configs

#### docker-compose.yml

Tewaking configuration in file *docker-compose.yml*.

<details><summary>Click to expand command</summary>

```sh
cd "${service_save_path}"

# - change image version
# MySQL 5.6 will reach EOL (“End of Life”) in February 2021. Choose 5.7
sed -r -i '/image.*?mysql:/{s@^(.*?mysql:).*@\15.7@g}' docker-compose.yml
# redis 6.0-alpine to 6-alpine
sed -r -i '/image.*?redis:/{s@^(.*?redis:).*@\16-alpine@g;}' docker-compose.yml

# - fix port map issue, otherwise custom exposed port not start properly
# change from '3000:${EXPOSED_PORT}' to '${EXPOSED_PORT}:3000'
sed -r -i '/3000:.*?EXPOSED_PORT/{s@^([^-]+-).*@\1 \${EXPOSED_PORT}:3000@g;}' docker-compose.yml

# - choose image / build
# method 1 - via build (default)
# WARNING: Image for service syncing-server was built because it did not already exist. To rebuild this image you must use `docker-compose build` or `docker-compose up --build`.

# method 2 - via image instead of build
# https://hub.docker.com/r/standardnotes/syncing-server
sed -r -i '/syncing-server:/,/^$/{/build:/{s@build@#&@g}}' docker-compose.yml
sed -r -i '/#build:/a image: standardnotes/syncing-server' docker-compose.yml
sed -r -i '/syncing-server:/,/^$/{/image:/{s@.*@    &@g}}' docker-compose.yml

# - change volume dir
# do not change syncing-server volume '.:/syncing-server', otherwise it will fail to find script './wait-for.sh' in directive 'entrypoint'
sed -r -i '/volumes:/,/-/{/ - /{s@\./@'"${data_dir}"'/@g;}}' docker-compose.yml
```

</details>


#### .env

Create custom enviroment variables file

<details><summary>Click to expand command</summary>

```sh
cd "${service_save_path}"

# - initialize default configuration
./server.sh setup

# Initializing default configuration
# Default configuration files created as .env and docker/*.env files. Feel free to modify values if needed.

# - Set custom env
tee "${service_save_path}"/custom_env 1> /dev/null << EOF 
EXPOSED_PORT=${exposed_port}
RAILS_ENV=production
RAILS_LOG_LEVEL=WARN

#DISABLE_USER_REGISTRATION=false

# Dno't change it, otherwise it will fail to connect database
#DB_PASSWORD=changeme123

SECRET_KEY_BASE=$(openssl rand -hex 64)
PSEUDO_KEY_PARAMS_KEY=$(openssl rand -hex 64)
AUTH_JWT_SECRET=$(openssl rand -hex 64)

# (Optional) Redis Cache for ephemeral sessions
REDIS_URL=redis://cache:6379

# (Optional) Change URLs to Internal DNS
#INTERNAL_DNS_REROUTE_ENABLED=false
# (Optional) User Management Server - registration emails, subscriptions etc.
#USER_MANAGEMENT_SERVER=
EOF

# - change specific directive values in file .env
sed '/^$/d;/^#/d' "${service_save_path}"/custom_env | while IFS="=" read -r var_k var_v; do   
    sed -r -i '/^#*[[:space:]]*'"${var_k}"'=/{s?.*?'"${var_k}"'='"${var_v}"'?g}' "${service_save_path}"/.env
done
```

</details>

### Running Containers

```sh
cd "${service_save_path}"

./server.sh start

# check log to debug
# ./server.sh logs
```

<details><summary>Click to expand output info</summary>

```txt
Starting up infrastructure
Creating standardnotes_syncing-server_1           ... done
Creating standardnotes_syncing-server-js_1        ... done
Creating standardnotes_cache_1                    ... done
Creating standardnotes_syncing-server-js-worker_1 ... done
Creating standardnotes_db_1                       ... done
Creating standardnotes_api-gateway_1              ... done
Creating standardnotes_auth_1                     ... done
Infrastructure started. Give it a moment to warm up. If you wish please run the './server.sh logs' command to see details.
```

</details>

It needs some time to warm up. If you wish please run the './server.sh logs' command to see details.

Now you can check if it works properly via command `curl 127.0.0.1:${exposed_port}`

<details><summary>Click to expand verification info</summary>

Output

>{"message":"Hi! You're not supposed to be here."}

Log output

>api-gateway_1               | {"meta":{"req":{"url":"/","headers":{"x-real-ip":"127.0.0.1","x-forwarded-for":"127.0.0.1","x-forwarded-proto":"https","connection":"close","user-agent":"curl/7.64.0","accept":"*/*"},"method":"GET","httpVersion":"1.0","originalUrl":"/","query":{}},"res":{"statusCode":200},"responseTime":14},"level":"info","message":"HTTP GET /"}

</details>


### Nginx Reverse Proxy

Nginx

```sh
service_name='standardnotes'
exposed_port='8200'
extensions_exposed_port='8201'

domain_name_specify="${service_name}.maxdsre.com"
# https://standardnotes.maxdsre.com
backend_private_ip='192.168.8.4'

# Nginx site config dir /etc/nginx/sites-enabled/
nginx_conf_path="/etc/nginx/sites-enabled/${service_name}.conf"
mkdir -p "${nginx_conf_path%/*}"
```

#### Conf

<details><summary>Click to expand config info</summary>

```conf
sudo tee "${nginx_conf_path}" 1> /dev/null << EOF
upstream ${service_name}-default {
    server ${backend_private_ip}:${exposed_port};
}

upstream ${service_name}-extensions {
    server ${backend_private_ip}:${extensions_exposed_port};
}

# Redirect HTTP to HTTPS
server {
    listen 80;
    # listen [::]:80 ipv6only=on;
    server_name ${domain_name_specify};
    return 301 https://\$host\$request_uri;
}

server {
    listen 443 ssl http2;
    # listen [::]:443 ssl http2 ipv6only=on;
    server_name ${domain_name_specify};

    error_log  /var/log/nginx/${service_name}_error.log warn;  # [debug|info|notice|warn|error|crit|alert|emerg]
    access_log /var/log/nginx/${service_name}_access.log main; # format name 'main' instead of predefined 'combined' format

    include /etc/nginx/conf.d/section/ssl.conf;

    location / {
        proxy_set_header Host \$host;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto \$scheme;

        proxy_pass http://${service_name}-default;

        # https://serverfault.com/questions/587386/an-upstream-response-is-buffered-to-a-temporary-file
        proxy_buffers 16 1M; 
        proxy_buffer_size 10M;
        proxy_busy_buffers_size 10M;
    }

    # https://github.com/iganeshk/standardnotes-extensions#setup-with-nginx
    location ^~ /extensions/ {
        autoindex off;
        #alias /path/to/standardnotes-extensions/public;
        proxy_pass http://${service_name}-extensions/;

        # CORS HEADERS
        if (\$request_method = 'OPTIONS') {
            add_header 'Access-Control-Allow-Origin' '*';
            add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
            add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range';
            # Tell client that this pre-flight info is valid for 20 days
            add_header 'Access-Control-Max-Age' 1728000;
            add_header 'Content-Type' 'text/plain; charset=utf-8';
            add_header 'Content-Length' 0;
            return 204;
		    }
        if (\$request_method = 'POST') {
                add_header 'Access-Control-Allow-Origin' '*';
                add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
                add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range';
                add_header 'Access-Control-Expose-Headers' 'Content-Length,Content-Range';
        }
		    if (\$request_method = 'GET') {
            add_header 'Access-Control-Allow-Origin' '*';
            add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
            add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range';
            add_header 'Access-Control-Expose-Headers' 'Content-Length,Content-Range';
		    }
	  }

}
EOF
```

</details>

Testing Nginx conf syntax then reload

```sh
nginx -t && nginx -s reload

# nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
# nginx: configuration file /etc/nginx/nginx.conf test is successful
```

Checking if domain works

```sh
$ curl "${domain_name_specify}" && echo ''

# output
{"message":"Hi! You're not supposed to be here."}
```

Now that you own self-hosted Standard Notes.

But if you wanna embrace the power of Standard Notes, you need to install extensions. Luckily, you can also self-host your extensions.

## Extensions

Official doc [Intro to Extensions](https://docs.standardnotes.org/extensions/intro/#sustainability) says:

>Most of our extensions are open-source and available for self-hosting. You can also learn to develop your own extensions by following the guides on this site. However, we encourage you to support the sustainability and future development of this project by [purchasing a subscription](https://standardnotes.org/extensions).

By self-hosting Standard Notes extensions, you're bypassing the need to purchase a license. If possible, please purchase a license to support [Standard Notes][standard_notes].

Github project [iganeshk/standardnotes-extensions](https://github.com/iganeshk/standardnotes-extensions) is a self-host standard notes extensions repository.


### Project Code

```sh
mkdir -p "${extensions_repo_save_path}"
# git clone https://github.com/iganeshk/standardnotes-extensions.git "${extensions_repo_save_path}"
curl -fsSL https://github.com/iganeshk/standardnotes-extensions/archive/master.tar.gz | tar zxf - --strip-component=1 -C "${extensions_repo_save_path}"
```

### .env

This repo needs you to generate GitHub token to make its script work.

<details><summary>Click to expand custom env</summary>

```sh
service_name='standardnotes'
domain_name_specify="${service_name}.maxdsre.com"

cd "${extensions_repo_save_path}"

# cp env.sample .env

# Please change to you own GitHub token
github_username='XXXXXXX'
github_token='6a3d58a492984f56ed1c3939a4070388147626f0'

tee .env 1> /dev/null << EOF
# https://standardnotes.maxdsre.com/extensions
domain: https://${domain_name_specify}/extensions

github:
  username: ${github_username}
  token: ${github_token}
EOF
```

</details>

### docker-compose.yml

Creating custom *docker-compose.yml*.

Here choose build image based [Dockerfile](https://github.com/iganeshk/standardnotes-extensions/blob/master/Dockerfile), you can also use image [mtoohey/standardnotes-extensions](https://hub.docker.com/r/mtoohey/standardnotes-extensions) provided by project maintainer.

Running via Nginx image which exposes a TCP port.

<details><summary>Click to expand custom docker-compose.yml</summary>

```yml
cd "${extensions_repo_save_path}"

# - Create custom docker-compose.yml
tee docker-compose.yml 1> /dev/null << EOF
version: '3'

services:
  nginx:
    image: nginx:alpine
    container_name: standardnotes_extensions_nginx
    restart: unless-stopped
    ports:
      - ${extensions_exposed_port}:80
    volumes:
      - ${extensions_data_dir}/public:/usr/share/nginx/html

  standardnotes-extensions:
    build: .
    # image: mtoohey/standardnotes-extensions
    restart: "no"
    volumes:
      - ${extensions_repo_save_path}/.env:/build/.env
      - ${extensions_repo_save_path}/extensions:/build/extensions
      - ${extensions_data_dir}/public:/build/public
EOF

# - Running container
docker-compose up -d

# WARNING: Image for service standardnotes-extensions was built because it did not already exist. To rebuild this image you must use `docker-compose build` or `docker-compose up --build`.
```

</details>

### Verifying

Verifying the Standard Notes Extensions endpoint

```http
https://standardnotes.maxdsre.com/extensions/index.json
```

```sh
curl -I https://standardnotes.maxdsre.com/extensions/index.json

# output
HTTP/2 200
server: nginx
...
...
```

Testing account

Item|Value
---|---
Email|test@maxdsre.com
Password|standard_notes@2021


### Extended Activation Code

To enable self-host extensions, you need activate it via *Extended Activation Code*, here is

>https://standardnotes.maxdsre.com/extensions/index.json

Just copy this link into the form, then click button `Submit Code`.

<details><summary>Click to expand interface snapshot</summary>

Extended activation code interface

![Extended activation code interface](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/03-1_extension_license_code.png)

Extended activation code submit

![Extended activation code submit](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/03-2_extension_license_code_submit.png)

</details>

## User Management

[How can I change my account email?](https://standardnotes.org/help/7/how-can-i-change-my-account-email)

Official doc [Managing options](https://docs.standardnotes.org/self-hosting/managing-options)

### User Delete (not work)

Testing example

<details><summary>Click to expand example details</summary>

```sh
cd "${service_save_path}"

tee -a "${service_save_path}"/.env 1> /dev/null << EOF
# admin api request start
ADMIN_IPS=$(curl -fsL ipinfo.io/ip)
ADMIN_KEY=$(openssl rand -hex 32)
# admin api request end
EOF

# - restart container service
docker-compose restart


target_email='test@maxdsre.com'
admin_key_extract=$(sed -r -n '/ADMIN_KEY/{s@^[^=]*=@@g;p}' .env)

# POST /admin/delete_account?admin_key=theadminkey&email=the-email@domain.com
# Host: your-self-hosted-syncing-server.com

curl -X POST -d "/admin/delete_account?admin_key=${admin_key_extract}&email=email=${target_email}" https://standardnotes.maxdsre.com
```

Prompt error info

```html
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Error</title>
</head>
<body>
<pre>Not Found</pre>
</body>
</html>
```

Log error info

```txt
api-gateway_1               | {"meta":{"req":{"url":"/admin","headers":{"x-real-ip":"217.138.213.126","x-forwarded-for":"217.138.213.126","x-forwarded-proto":"https","connection":"close","cache-control":"max-age=0","dnt":"1","upgrade-insecure-requests":"1","user-agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 11_18_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4183.83 Safari/537.36","accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9","sec-fetch-site":"none","sec-fetch-mode":"navigate","sec-fetch-user":"?1","sec-fetch-dest":"document","accept-encoding":"gzip, deflate, br","accept-language":"en-US,en;q=0.9"},"method":"GET","httpVersion":"1.0","originalUrl":"/admin","query":{}},"res":{"statusCode":404},"responseTime":8},"level":"info","message":"HTTP GET /admin"}
api-gateway_1               | Error: Not Found
api-gateway_1               |     at Request.callback (/var/www/node_modules/superagent/lib/node/index.js:883:15)
api-gateway_1               |     at /var/www/node_modules/superagent/lib/node/index.js:1127:20
api-gateway_1               |     at IncomingMessage.<anonymous> (/var/www/node_modules/superagent/lib/node/parsers/json.js:22:7)
api-gateway_1               |     at Stream.emit (events.js:315:20)
api-gateway_1               |     at Unzip.<anonymous> (/var/www/node_modules/superagent/lib/node/unzip.js:53:12)
api-gateway_1               |     at Unzip.emit (events.js:315:20)
api-gateway_1               |     at endReadableNT (_stream_readable.js:1327:12)
api-gateway_1               |     at processTicksAndRejections (internal/process/task_queues.js:80:21)
```

Is it related to directive `USER_MANAGEMENT_SERVER` defined in file *env* ? I find this directive used in file [app/jobs/registration_job.rb](https://github.com/standardnotes/syncing-server/blob/master/app/jobs/registration_job.rb#L8), set it to *https://standardnotes.maxdsre.com* still not work.

</details>

## Snapshots

### Verification

<details><summary>Click to expand verification snapshot</summary>

Server service verification

![standard notes verification](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/01-1_verification.png)

Extensions verification

![standard notes extensions verification](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/01-2_extensions_verification.png)


</details>

### Desktop Client

Main interface

<details><summary>Click to expand main interface snapshot</summary>

![desktop main interface](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/02-1_desktop_main_page.png)

</details>

#### Register & Login

Register interface

<details><summary>Click to expand register interface snapshot</summary>

![desktop register interface](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/02-2_desktop_register.png)

![desktop register key generation](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/02-3_desktop_register_key_generating.png)

</details>

Login info

![desktop login info](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/02-4_desktop_login_info.png)

### Extensions

Extended Activation Code

<details><summary>Click to expand snapshot</summary>

Extended activation code interface

![Extended activation code interface](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/03-1_extension_license_code.png)

Extended activation code submit

![Extended activation code submit](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/03-2_extension_license_code_submit.png)

Extensions license activated

![Extensions license activated](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/03-3_extension_license_activated.png)

</details>

Extension activated overview

<details><summary>Click to expand overview snapshot</summary>

![extensions overview](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/04-1_extension_overview.png)

Extension activate welcome info

![Extension activate welcome info](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/04-2_extension_activate.png)

</details>

Extensions installed overview

![extensions installed overview](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/04-3_extensions_installed.png)

### Editor

Editor extension prompt info

<details><summary>Click to expand snapshot</summary>

![Editor extension prompt info](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/05-1_editor_prompt_info.png)

</details>

Editor interface overview

![Editor interface overview](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/06-1_desktop_editor_overview.png)

### Web Site

<details><summary>Click to expand website snapshot</summary>

Web site main page

![Web site main page](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/07-1_web_main_page.png)

Sign in page

![Sign in page](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/07-2_web_signin.png)

Login success

![Login success](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/07-3_web_login_success.png)

Check active sessions

![Check active sessions](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/07-4_web_active_sessions.png)

</details>

Account info overview

![Account info overview](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/07-5_web_account_info.png)

</details>

### Editor Tabs

Editor function tabs

<details><summary>Click to expand snapshot</summary>

Tab - options

![Tab - options](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/08-1_tab_options.png)

Tab - editor

![Tab - editor](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/08-2_tab_editor.png)

Tab - actions

![Tab - actions](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/08-3_tab_actions.png)

Tab - history

![Tab - history](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CloudComputing/Selfhosting/standard_notes/08-4_tab_history.png)

</details>


## Issues Occuring

### Fail to connect database

After change directive `DB_PASSWORD` in file *.env*, it fails to connect the database while try to start containers.

<details><summary>Click to expand error log</summary>

```txt
syncing-server-js_1         | [Error] PoolCluster : Error: Access denied for user 'std_notes_user'@'172.18.0.2' (using password: YES)
db_1                        | 2021-03-11T15:25:23.595656Z 9 [Note] Access denied for user 'std_notes_user'@'172.18.0.2' (using password: YES)
syncing-server-js_1         | [Error] PoolCluster : Error: Access denied for user 'std_notes_user'@'172.18.0.2' (using password: YES)
syncing-server-js_1         | (node:64) UnhandledPromiseRejectionWarning: Error: Pool does Not exists.
syncing-server-js_1         |     at PoolNamespace.getConnection (/var/www/node_modules/mysql2/lib/pool_cluster.js:36:17)
syncing-server-js_1         |     at /var/www/node_modules/mysql2/lib/pool_cluster.js:43:21
syncing-server-js_1         |     at /var/www/node_modules/mysql2/lib/pool_cluster.js:210:18
syncing-server-js_1         |     at /var/www/node_modules/mysql2/lib/pool.js:60:18
syncing-server-js_1         |     at PoolConnection.<anonymous> (/var/www/node_modules/mysql2/lib/connection.js:739:13)
syncing-server-js_1         |     at Object.onceWrapper (events.js:422:26)
syncing-server-js_1         |     at PoolConnection.emit (events.js:327:22)
syncing-server-js_1         |     at PoolConnection.EventEmitter.emit (domain.js:486:12)
syncing-server-js_1         |     at PoolConnection._notifyError (/var/www/node_modules/mysql2/lib/connection.js:225:12)
syncing-server-js_1         |     at ClientHandshake.<anonymous> (/var/www/node_modules/mysql2/lib/connection.js:114:14)
syncing-server-js_1         | (Use `node --trace-warnings ...` to show where the warning was created)
syncing-server-js_1         | (node:64) UnhandledPromiseRejectionWarning: Unhandled promise rejection. This error originated either by throwing inside of an async function without a catch block, or by rejecting a promise which was not handled with .catch(). To terminate the node process on unhandled promise rejection, use the CLI flag `--unhandled-rejections=strict` (see https://nodejs.org/api/cli.html#cli_unhandled_rejections_mode). (rejection id: 1)
syncing-server-js_1         | (node:64) [DEP0018] DeprecationWarning: Unhandled promise rejections are deprecated. In the future, promise rejections that are not handled will terminate the Node.js process with a non-zero exit code.
syncing-server-js_1         | Done in 1.55s.
standardnotes_syncing-server-js_1 exited with code 0
api-gateway_1               | nc: bad address 'syncing-server-js'
...
...
...
auth_1                      | nc: bad address 'syncing-server-js'
auth_1                      | syncing-server-js:3000 is unavailable - sleeping
api-gateway_1               | nc: bad address 'syncing-server-js'
api-gateway_1               | syncing-server-js:3000 is unavailable - sleeping
syncing-server-js-worker_1  | nc: bad address 'syncing-server-js'
syncing-server-js-worker_1  | syncing-server-js:3000 is unavailable yet - waiting for it to start
...

```

</details>

Solution is keep the origin password, don't change it.


## Tutorials

* [How to completely self-host Standard Notes](https://theselfhostingblog.com/posts/how-to-completely-self-host-standard-notes/)
* [Self-Host and Dockerize Standard Notes Extensions with Docker-Compose](https://return2.net/dockerize-standard-notes-extensions/)


## Resources

* [GitHub - Awesome Standard Notes](https://github.com/jonhadfield/awesome-standard-notes)


## Change Log

* Mar 05, 2021 14:30 Fri ET
  * First draft
* Mar 11, 2021 17:45 Thu ET
  * rewrite, add extensions support

[standard_notes]:https://standardnotes.org "A Simple And Private Notes App"
[docker]: https://www.docker.com
[docker_compose]:https://docs.docker.com/compose/

<!-- End -->