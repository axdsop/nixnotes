# Standard Notes Extensions Self-Hosted Repository

Official extension data source <https://standardnotes.com/page-data/features/page-data.json> from page [feature](https://standardnotes.com/features).

Origin Github <https://github.com/iganeshk/standardnotes-extensions>

More resource

* [jonhadfield/awesome-standard-notes](https://github.com/jonhadfield/awesome-standard-notes)

I make the following changes:

1. Python script doesn't need GitHub token and package *requests*;
2. Extensions update to the latest GitHub url and version no;


For Github rate limit, details see official doc [Resources in the REST API #Rate limiting](https://docs.github.com/en/rest/overview/resources-in-the-rest-api#rate-limiting)

> For unauthenticated requests, the rate limit allows for up to **60** requests per hour. Unauthenticated requests are associated with the originating IP address, and not the user making requests.
>
>When using the built-in *GITHUB_TOKEN* in GitHub Actions, the rate limit is **1,000** requests per hour per repository.

Here the extensions list is less than 60, then you don't need to use *GITHUB_TOKEN* to bypass github api rate limits.

Execution Python script [build.py](./standardnotes-extensions/build.py), it will download the extensions' latest release version and save them into directory *public/*, then compress then into a *.zip* file (e.g. *public-20210715_0728.zip*).

<details><summary>Click to expand output</summary>

```sh
# python3 build.py
standardnotes/markdown-basic             1.4.0  (updated)
standardnotes/minimal-markdown-editor    1.3.7  (updated)
standardnotes/vim-editor                 1.3.7  (updated)
standardnotes/code-editor                1.3.8  (updated)
standardnotes/secure-spreadsheets        1.4.0  (updated)
standardnotes/bold-editor                1.2.1  (updated)
standardnotes/mfa-link                   1.2.4  (updated)
standardnotes/folders-component          1.3.8  (updated)
standardnotes/markdown-pro               1.3.14 (updated)
standardnotes/simple-task-editor         1.3.7  (updated)
standardnotes/github-push                1.2.4  (updated)
standardnotes/math-editor                1.3.4  (updated)
standardnotes/token-vault                2.0.1  (updated)
standardnotes/action-bar                 1.3.2  (updated)
standardnotes/plus-editor                1.5.0  (updated)
standardnotes/quick-tags                 1.3.2  (updated)
standardnotes/autobiography-theme        1.0.0  (updated)
cameronldn/sn-theme-dracula              1.2.1  (updated)
standardnotes/dynamic-theme              1.0.0  (updated)
standardnotes/focus-theme                1.2.3  (updated)
standardnotes/futura-theme               1.2.2  (updated)
standardnotes/midnight-theme             1.2.2  (updated)
ntran/sn-theme-muteddark                 1.0.9  (updated)
iganeshk/sn-theme-no-distraction-dynamic 1.0.0  (updated)
standardnotes/no-distraction-theme       1.2.2  (updated)
lzambarda/sn-nord-theme                  v0.0.2 (updated)
ceiphr/sn-overcast-theme                 1.2.2  (updated)
christianhans/sn-pure-black-theme        1.0.9  (updated)
standardnotes/solarized-dark-theme       1.2.1  (updated)
standardnotes/titanium-theme             1.2.2  (updated)
hyphone/sn-theme-vscode                  1.0.17 (updated)
Repository Endpoint URL:       https://domain.com/extensions/index.json
```

</details>

The default domain in file *env* is *https://domain.com/extensions*. If you wanna change your own domain for files in directory *public/*, you can execute the following commands

```sh
# unzip public-20210715_0728.zip
cd public/

custom_domain=${custom_domain:-'snsync.maxdsre.com'}
find $PWD -maxdepth 2 -type f -name 'index.json' | xargs -I %s sed -r -i '/https:\/\/domain.com/{s@domain.com@'"${custom_domain}"'@g}' %s
```


<!-- End -->