#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Target: Self-host standard notes extensions.
# Writer: MaxdSre
# Chnage Log
# - Mar 15, 2021 20:25 Mon ET - first draft

# GitHub: https://github.com/iganeshk/standardnotes-extensions   Script build_repo.py needs GitHub token to increase api rate limit from 60 to 1000 per hour, and require module 'requests'

# urllib.e/home/flying/Desktop/standard_extenstion/aaa.pyrror.HTTPError: HTTP Error 403: rate limit exceeded
# For unauthenticated requests, the rate limit allows for up to 60 requests per hour. --- https://docs.github.com/en/rest/overview/resources-in-the-rest-api#rate-limiting

'''
Parse extensions/*.yaml files & build a directory with following structure:
public/
    |-my-extension-1/
    |   |-1.0.0/          <- version (to avoid static file caching issues)
    |   |   |-index.json  <- extension info
    |   |   |-index.html  <- extension entrance (component)
    |   |   |-dist        <- extension resources
    |   |   |-...         <- other files
    |-index.json          <- repo info, contain all extensions' info
'''

# - Import modules
from urllib.request import Request, urlopen
from zipfile import ZipFile
from shutil import make_archive
from datetime import datetime
from time import sleep
import os
import re
import json
import yaml

# - Variables definition
user_agent = 'Mozilla/5.0 (Windows NT 11.0; Win64; x64; rv:99.0) Gecko/20100101 Firefox/99.0'

# - Get absolute file path
# __file__ get the running file path
base_dir = os.path.dirname(os.path.abspath(__file__))

# - Extract domain from file .env (env.sample)
# Pattern  'domain: https://domain.com/extensions'
domain_name = ''
for line in open(os.path.join(base_dir, 'env.sample')):
    for match in re.finditer(re.compile('^domain:'), line):
        domain_name = line.strip('\n').split(': ')[-1].rstrip('/')


extension_dir = os.path.join(base_dir, 'extensions')
public_dir = os.path.join(base_dir, 'public')
if not os.path.exists(public_dir):
    os.makedirs((public_dir))
# os.chdir(public_dir)

extensions_index = []

# Get all extensions, sort extensions alphabetically along by their by type
theme_files = [x for x in sorted(os.listdir(
    extension_dir), reverse=0) if x.startswith('theme') and x.endswith('.yaml')] # x.endswith('theme.yaml')
# ext_files = [x for x in sorted(os.listdir(extension_dir), reverse=0) if not x.endswith('theme.yaml') and x.endswith('.yaml')]
ext_files = [x for x in os.listdir(extension_dir) if x not in theme_files]
ext_files.extend(theme_files)

for ext_file in ext_files:
    with open(os.path.join(extension_dir, ext_file), 'r') as ext_yaml:
        ext_yaml = yaml.load(ext_yaml, Loader=yaml.FullLoader)
        ext_repo_name = ext_yaml['github'].rstrip('/')
        ext_name = ext_repo_name.split('/')[-1]
        ext_save_dir = os.path.join(public_dir, ext_name)

        # - extract release info from GitHub api
        ext_github_api = 'https://api.github.com/repos/' + \
            ext_repo_name + '/releases/latest'

        req = Request(ext_github_api, headers={'User-Agent': user_agent})
        ext_git_info = json.loads(urlopen(req, timeout=1).read().decode())
        ext_git_version = ext_git_info['tag_name']  # ext_yaml['version']

        # - if extension dir with current git version existed, then ignore
        if os.path.exists(os.path.join(ext_save_dir, ext_git_version)):
            print('{:40s} {:6s}\t(already up-to-date)'.format(
                ext_repo_name, ext_git_version))
            sleep(0.15)
        else:
            # - check if extension directory existed
            if not os.path.exists(ext_save_dir):
                os.makedirs(ext_save_dir)

            ext_pack_save_temp_path = os.path.join(
                ext_save_dir, ext_git_version.lstrip('v')) + ".zip"

            if os.path.exists(ext_pack_save_temp_path):
                os.remove(ext_pack_save_temp_path)

            # https://stackoverflow.com/questions/7243750/download-file-from-web-in-python-3
            with open(ext_pack_save_temp_path, "wb") as file:
                # response = urlopen('https://github.com/' + ext_repo_name + '/archive/main.zip')
                # response = urlopen(
                #     'https://github.com/' + ext_repo_name + '/archive/' + ext_git_version + '.zip')
                # file.write(response.read())

                zipball_url = ext_git_info['zipball_url']

                if not zipball_url:
                    zipball_url = 'https://github.com/' + ext_repo_name + '/archive/' + ext_git_version + '.zip'

                response = urlopen(zipball_url)
                file.write(response.read())

            # - decompress ZIP file
            with ZipFile(ext_pack_save_temp_path, 'r') as zipObj:
                zipObj.extractall(ext_save_dir)

            ext_pack_decompress_temp_dir = os.path.join(
                ext_save_dir, ext_name + '-' + ext_git_version.lstrip('v'))

            # - rename decompress dir
            # /PATH/action-bar/action-bar-1.3.1 ==> /PATH/action-bar/1.3.1
            if os.path.exists(ext_pack_decompress_temp_dir):
                os.rename(ext_pack_decompress_temp_dir,
                          os.path.join(ext_save_dir, ext_git_version))

            # - delete ZIP file
            if os.path.exists(ext_pack_save_temp_path):
                os.remove(ext_pack_save_temp_path)

            print('{:40s} {:6s}\t(updated)'.format(
                ext_repo_name, ext_git_version))

        # - build extension info (stateless)
        # https://domain.com/sub-domain/my-extension/index.json
        extension_index = dict(
            identifier=ext_yaml['id'],
            name=ext_yaml['name'],
            content_type=ext_yaml['content_type'],
            area=ext_yaml.get('area', None),
            version=ext_git_version,
            description=ext_yaml.get('description', None),
            marketing_url=ext_yaml.get('marketing_url', None),
            thumbnail_url=ext_yaml.get('thumbnail_url', None),
            valid_until='2030-05-16T18:35:33.000Z',
            url='/'.join([domain_name, ext_name,
                          ext_git_version, ext_yaml['main']]),
            download_url='https://github.com/{}/archive/{}.zip'.format(
                ext_repo_name, ext_git_version),
            latest_url='/'.join([domain_name, ext_name, 'index.json']),
            flags=ext_yaml.get('flags', []),
            dock_icon=ext_yaml.get('dock_icon', {}),
            layerable=ext_yaml.get('layerable', None),
            statusBar=ext_yaml.get('statusBar', None),
        )

        # Strip empty values
        extension_index = {k: v for k, v in extension_index.items() if v}

        # Generate JSON file for each extension
        with open(os.path.join(public_dir, ext_name, 'index.json'),
                  'w') as ext_json:
            json.dump(extension_index, ext_json, indent=4)

        extensions_index.append(extension_index)

        # break

# - Generate the main repository index JSON
# https://domain.com/sub-domain/my-index.json
with open(os.path.join(public_dir, 'index.json'), 'w') as ext_json:
    json.dump(
        dict(
            content_type='SN|Repo',
            valid_until='2030-05-16T18:35:33.000Z',
            packages=extensions_index,
        ),
        ext_json,
        indent=4,
    )

# - Compress public/ directory into .zip
# datetime object containing current date and time
time_now = datetime.now()

os.chdir(base_dir)
make_archive('public-' + time_now.strftime("%Y%m%d_%H%M"), 'zip', public_dir)

print("Repository Endpoint URL: {:6s}{}/index.json".format("", domain_name))


# Script End
