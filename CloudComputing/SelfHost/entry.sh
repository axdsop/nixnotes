#!/usr/bin/env bash

# Entry point for docker self-host services.

# Date: Nov 29, 2021 Mon 11:02 ET

# Code Architecture
# .
# ├── entry.sh
# ├── Scripts/
# └── Services/

# Docker compose choose v2

# To use custom variables in environment config file, add them to file 'custom_env'


# - Variable Definition
data_dir=${data_dir:='/data'}  # default value is /data

download_command='wget -qO-'
[[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && download_command='curl -fsSL'

script_absolute_path=$(readlink -nf "$0")
script_cron=${script_cron:-'10 */2 * * *'}  # every 2h

project_absolute_dir=$(dirname "${script_absolute_path}")
# ~/Documents/Scripts/docker-selfhost/{Scripts,Services}  # find "${project_absolute_dir}" -mindepth 1 -maxdepth 1 -type d -print
project_scripts_dir="${project_absolute_dir}/Scripts"
project_services_dir="${project_absolute_dir}/Services"

# - Add to crontab
# https://askubuntu.com/questions/58575/add-lines-to-cron-from-script
# /var/spool/cron/crontabs/$USER
# 15 23 * * *          # configs_backup.sh
# 25 11,23 * * *       # data_backup.sh
# 15 3,16 * * *        # service_maintance.sh

cron_str='docker self-host maintance'

if [[ -z $(crontab -u "${USER}" -l 2> /dev/null | sed -r -n '/'"${cron_str}"'/{p}') ]]; then
    echo "Add cron task to file /var/spool/cron/crontabs/$USER"

    service_data_backup_path=${service_data_backup_path:-}
    service_data_backup_path=$(find "${project_scripts_dir}" -mindepth 1 -maxdepth 1 -type f -name 'service_data_backup.sh' -print 2> /dev/null)
    service_data_backup_cron=${service_data_backup_cron:-'15 11,22 * * *'}

    service_maintance_path=${configs_backup_path:-}
    service_maintance_path=$(find "${project_scripts_dir}" -mindepth 1 -maxdepth 1 -type f -name 'service_maintance.sh' -print 2> /dev/null)
    service_maintance_cron=${service_maintance_cron:-'15 3,12,17 * * *'}

    cron_task_line="\n# ${cron_str} start"
    cron_task_line="${cron_task_line}\n${script_cron} /bin/bash ${script_absolute_path}"
    [[ -f "${service_data_backup_path}" ]] && cron_task_line="${cron_task_line}\n${service_data_backup_cron} /bin/bash ${service_data_backup_path}"
    [[ -f "${service_maintance_path}" ]] && cron_task_line="${cron_task_line}\n${service_maintance_cron} /bin/bash ${service_maintance_path}"
    cron_task_line="${cron_task_line}\n# ${cron_str} end\n"

    crontab -u "${USER}" -l &> /dev/null # initialize if not exists
    (crontab -u "${USER}" -l; echo -e "${cron_task_line}" ) | crontab -u "${USER}" -
fi

cd "${project_absolute_dir}" || return

# env.sample generation
custom_env='custom_env'  # add custom definition here

[[ -f "${custom_env}" ]] || touch "${custom_env}"
if [[ -f "${custom_env}" ]]; then
    custom_data_dir=${custom_data_dir:-}
    custom_data_dir=$(sed -r -n '/^DATA_DIR=/I{s@^[^=]+=@@g;s@[[:space:]]*#+.*$@@g;s@"@@g;s@'\''@@g;s@[[:space:]]*@@g;p;q}' "${custom_env}" 2> /dev/null)
    [[ -n "${custom_data_dir}" ]] && data_dir="${custom_data_dir}"

    echo "# Custom Variables Definition Stard" > .env
    cat "${custom_env}" 2> /dev/null >> .env
    # sed -r -n '/^#/d;p' "${custom_env}" 2> /dev/null >> .env
    echo -e "# Custom Variables Definition End\n" >> .env

    sed -r -i '/^DATA_DIR=/d' .env 2> /dev/null
fi

tee -a .env 1> /dev/null << EOF
# Detected Variables Start

DATA_DIR="${data_dir}"
SCRIPTS_DIR="${project_scripts_dir}"
SERVICE_DIR="${project_services_dir}"

# Detected Variables End

EOF


# Script End