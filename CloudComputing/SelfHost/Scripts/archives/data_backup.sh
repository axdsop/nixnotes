#!/usr/bin/env bash

# Target: Backup self-host service data
# Change Log
# - Dec 25, 2021 Sat 11:55 ET -  - first draft

# remote save dir: $remote_path/{data,configs}/

# - variables definition
data_dir=${data_dir:-'/data'}
date_format=$(date +'%Y%m%d_%H%M')  # 20211225_1155
temp_save_dir=$(mktemp -d -t 'XXXXXXXX')
remote_path=${remote_path:-}


# - essential utilities detection
if [[ -z $(which zip 2> /dev/null) ]]; then
    echo "Fail to find utility 'zip'"; exit 0
fi

# rclone config detection
if [[ -n $(which rclone 2> /dev/null) ]]; then
    # rclone fail to read var RCLONE_PASSWORD_COMMAND in cron job
    # https://rclone.org/docs/#configuration-encryption
    rclone_config_dir="$HOME/.config/rclone"
    rclone_config_path="${rclone_config_dir}/rclone.conf"

    if [[ -n "$(sed -r  -n '1,4{/RCLONE_ENCRYPT_/{p}}' "${rclone_config_path}" 2> /dev/null)" ]]; then
        rclone_pass=$(find "${rclone_config_dir}" -type f -name 'pass' -print 2> /dev/null | xargs -I %s cat %s | sed '/^$/d;/^#/d')
        export RCLONE_CONFIG_PASS="${rclone_pass}"
    fi
else
    echo "Fail to find utility 'rclone'"; exit 0
fi

# - get script absolute path
script_absolute_path=$(readlink -nf "$0")
script_absolute_dir=$(dirname "${script_absolute_path}")
docker_service_absoute_dir=$(dirname "${script_absolute_dir}")

# get env file
env_absolute_path="${docker_service_absoute_dir}/.env"
[[ ! -f "${env_absolute_path}" && -f "${docker_service_absoute_dir}/entry.sh" ]] && bash "${docker_service_absoute_dir}/entry.sh" &> /dev/null
if [[ ! -f "${env_absolute_path}" ]]; then
    echo 'Sorry, fail to find env file'
    exit 0
fi

[[ -f "${env_absolute_path}" ]] && data_dir=$(sed -r -n '/^DATA_DIR=/I{s@^[^=]+=@@g;s@[[:space:]]*#+.*$@@g;s@"@@g;s@'\''@@g;s@[[:space:]]*$@@g;p;q}' "${env_absolute_path}" 2> /dev/null)

[[ -f "${env_absolute_path}" ]] && remote_path=$(sed -r -n '/^REMOTE_PATH=/I{s@^[^=]+=@@g;s@[[:space:]]*#+.*$@@g;s@"@@g;s@'\''@@g;s@[[:space:]]*$@@g;p;q}' "${env_absolute_path}" 2> /dev/null)

if [[ -z "${remote_path}" ]]; then
    echo "Sorry, fail to extract remote path from env ${env_absolute_path}"
    exit 0
fi

remote_save_path="${remote_path%\/}/data"


# - compress & upload
cd "${data_dir}" || return

# compress all data under dir /data, exclude dirs specified
# zip -r data_$(date +'%Y%m%d_%H%M').zip data/ -x "data/nextcloud/data/*" -x "data/nextcloud/html/*" -x "data/standardnotes/extensions/*"

# /data/nextcloud -> nextcloud
# sed \L to lowercase, \U to uppercase
find "${data_dir}" -mindepth 1 -maxdepth 1 -type d -print | sed -r -n 's@.*\/([^\/]+)$@\L\1@g;p' | while read -r item; do
    save_path="${temp_save_dir}/${item}_${date_format}.zip"

    case "${item}" in
        vaultwarden)
            # exclude icon_cache/
            sudo zip -q -r "${save_path}" "${item}"/ -x "${item}/icon_cache/*"
        ;;
        standardnotes)
            # exclude extensions/
            # sudo zip -q -r "${save_path}" "${item}"/ -x "${item}/extensions/*"

            # don't backup dir, just export database sql
            echo ''
        ;;
        nextcloud)
            # exclude {data,html}/
            sudo zip -q -r "${save_path}" "${item}"/ -x "${item}/data/*" -x "${item}/html/*"
        ;;
        *)
            sudo zip -q -r "${save_path}" "${item}"/
        ;;
    esac

    if [[ -f "${save_path}" && -s "${save_path}" ]]; then
        rclone -P --no-update-modtime copy "${save_path}" "${remote_save_path}" && echo "File ${save_path##*/} upload to remote '${remote_save_path}' successfully."
    fi

done


cd /tmp || return
[[ -d "${temp_save_dir}" && "${temp_save_dir}" =~ ^\/tmp ]] && rm -rf "${temp_save_dir}"

# Script End