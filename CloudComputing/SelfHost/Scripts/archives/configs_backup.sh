#!/usr/bin/env bash

# Target: Backup docker & nginx configs
# Change Log
# - Nov 26, 2021 Fri 16:29 ET -  - first draft

# Issue
# - 'sudo chown' not wokr in corntab

# Crontab '15 23 * * *' # script execute every day 23:15

# remote save dir: $remote_path/{data,configs}/

# variable definition
remote_path=${remote_path:-}
nginx_config_dir=${nginx_config_dir:-'/etc/nginx'}

backup_save_dir="$HOME/Documents/Daily_Backup/Docker_Compose"
config_save_name="docker_configs_$(date +'%Y%m%d_%H%M')"
config_save_dir="${backup_save_dir}/${config_save_name}"

# - get script absolute path
script_absolute_path=$(readlink -nf "$0")
script_absolute_dir=$(dirname "${script_absolute_path}")
docker_service_absoute_dir=$(dirname "${script_absolute_dir}")

# get env file
env_absolute_path="${docker_service_absoute_dir}/.env"
[[ ! -f "${env_absolute_path}" && -f "${docker_service_absoute_dir}/entry.sh" ]] && bash "${docker_service_absoute_dir}/entry.sh" &> /dev/null
if [[ ! -f "${env_absolute_path}" ]]; then
    echo 'Sorry, fail to find env file'
    exit 0
fi

[[ -f "${env_absolute_path}" ]] && remote_path=$(sed -r -n '/^REMOTE_PATH=/I{s@^[^=]+=@@g;s@[[:space:]]*#+.*$@@g;s@"@@g;s@'\''@@g;s@[[:space:]]*$@@g;p;q}' "${env_absolute_path}" 2> /dev/null)

if [[ -z "${remote_path}" ]]; then
    echo "Sorry, fail to extract remote path from env ${env_absolute_path}"
    exit 0
fi

config_remote_path="${remote_path%\/}/configs"

[[ -d "${config_save_dir}" ]] || mkdir -p "${config_save_dir}"
cd "${config_save_dir}" || return

# docker configs
if [[ -d "${docker_service_absoute_dir}" ]]; then
    cp -p -R "${docker_service_absoute_dir}" "${docker_service_absoute_dir##*/}"
fi

# nginx configs
if [[ -d "${nginx_config_dir}" ]]; then
    mkdir -p nginx
    sudo cp -p -R "${nginx_config_dir}"/{nginx.conf,conf.d,sites-enabled,ssl.d} nginx/
    sudo chown -R "$USER":"$USER" nginx/  # not work in crontab
fi

# crontab backup
crontab -u "${USER}" -l > "crontab_$(date +'%Y%m%d_%H%M').txt"


# compress
cd "${backup_save_dir}" || return
zip -r -q "${config_save_name}.zip" "${config_save_name}"
stat "${config_save_name}.zip"

# delete dir
[[ -d "${config_save_dir}" ]] && sudo rm -r "${config_save_dir}"

# upload
if [[ -n $(which rclone 2> /dev/null) ]]; then
    # rclone fail to read var RCLONE_PASSWORD_COMMAND in cron job
    # https://rclone.org/docs/#configuration-encryption
    rclone_config_dir="$HOME/.config/rclone"
    rclone_config_path="${rclone_config_dir}/rclone.conf"

    if [[ -n "$(sed -r  -n '1,4{/RCLONE_ENCRYPT_/{p}}' "${rclone_config_path}" 2> /dev/null)" ]]; then
        rclone_pass=$(find "${rclone_config_dir}" -type f -name 'pass' -print 2> /dev/null | xargs -I %s cat %s | sed '/^$/d;/^#/d')
        export RCLONE_CONFIG_PASS="${rclone_pass}"
    fi

    rclone -P --no-update-modtime copy "${config_save_name}.zip" "${config_remote_path}" && echo "File ${config_save_name}.zip upload to remote '${config_remote_path}' successfully."
fi


# Script End