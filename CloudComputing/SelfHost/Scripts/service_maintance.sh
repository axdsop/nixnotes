#!/usr/bin/env bash

# Target: Docker images & service maintance on GNU/Linux
# Writer: MaxdSre

# Change Log
# - Jan 02, 2021 Sun 20:30 ET - add Standard Notes database sql dump
# - Nov 25, 2021 Thu 13:10 ET - add 'docker compose exec -d' action for specific service
# - Nov 18, 2021 Thu 09:20 ET - first draft

# Docker compose choose v2

# Crontab '15 3 * * *' # script execute every day 03:15


# get env file
file_absolute_dir=$(dirname "$(readlink -nf "$0")")
env_absolute_dir=$(dirname "${file_absolute_dir}")
env_absolute_path="${env_absolute_dir}/.env"
[[ ! -f "${env_absolute_path}" && -f "${env_absolute_dir}/entry.sh" ]] && bash "${env_absolute_dir}/entry.sh" &> /dev/null
if [[ ! -f "${env_absolute_path}" ]]; then
    echo 'Sorry, fail to find env file'
    exit 0
fi

# - variable definition
services_config_dir=${services_config_dir:-}
[[ -f "${env_absolute_path}" ]] && services_config_dir=$(sed -r -n '/^SERVICE_DIR=/{s@^[^=]+=@@g;s@[[:space:]]*#+.*$@@g;s@"@@g;s@'\''@@g;s@[[:space:]]*$@@g;p;q}' "${env_absolute_path}" 2> /dev/null)

if [[ -z "${services_config_dir}" ]]; then
    echo "Sorry, fail to extract service directory from env ${env_absolute_path}"
    exit 0
fi

download_command='wget -qO-'
[[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && download_command='curl -fsSL'


# - Install/Update docker & docker compose
${download_command} https://gitlab.com/axdsop/nixnotes/raw/master/ShellScripts/Toolkits/Application/Container/Docker-CE.sh | sudo bash -s --

echo -e '\n\nDocker Compose'
${download_command} https://gitlab.com/axdsop/nixnotes/raw/master/CloudComputing/Containers/Docker/Scripts/docker_compose.sh | sudo bash -s --

# Service image update

find "${services_config_dir}" -mindepth 1 -maxdepth 1 -type d -print | while read -r line; do
    cd "${line}" || return
    echo -e "\nService ${line##*/}"

    [[ $(docker compose images -q | wc -l) -ge 1 ]] && docker compose pull  # just execute while image existed

    if [[ $(docker compose ps -q | wc -l) -ge 1 ]]; then

        case "${line##*/}" in
            gitlab)
                gitlab_home=${gitlab_home:-'/data/gitlab'}
                [[ -f .env ]] && gitlab_home=$(sed -r -n '/GITLAB_HOME=/{s@^[^=]+=@@g;s@'\''@@g;s@"@@g;p;q}' .env 2> /dev/null)
                export GITLAB_HOME="${gitlab_home}"
            ;;
        esac

        docker compose down --remove-orphans
        sleep 1
        docker compose up -d

        case "${line##*/}" in
            nextcloud)
                sleep 2
                # - install ffmpeg for thumbnail preview
                # here 'app' is service name for image nextcloud: defined in file docker-compose.yml
                # https://stackoverflow.com/questions/51616789/exit-code-from-docker-compose-breaking-while-loop
                docker compose exec -d --user=root app sh -c "which apk && (which ffmpeg || apk add --no-cache ffmpeg); which apt-get && (which ffmpeg || (apt-get update; apt-get install -y ffmpeg; rm -rf /var/lib/apt/lists/*)); echo 'ffmpeg check finish'"
            ;;
        esac

    fi
done

# Delete image with tag <none>
echo -e '\nDelete image with tag <none>'
docker images 2> /dev/null | sed -r -n '/<none>/{s@[[:space:]]{2,}@|@g;p}' | cut -d\| -f3 | xargs -I %s docker rmi -f %s

# Remove unused docker files
echo -e '\nRemove unused docker files'
docker system prune -a -f --volumes

# Memory cache clean
# https://www.blackmoreops.com/2014/10/28/delete-clean-cache-to-free-up-memory-on-your-slow-linux-server-vps/
# https://www.kernel.org/doc/Documentation/sysctl/vm.txt
# 1 free pagecache / 2 free dentries and inodes / 3 free pagecache, dentries and inodes
echo -e '\nMemory pagecache clean'
free -h; sync; sudo bash -c "echo 1 > /proc/sys/vm/drop_caches"
[[ $(sed -r -n '/^SwapTotal:/{s@^[^:]+:[[:space:]]*([[:digit:]]+).*@\1@g;p}' /proc/meminfo 2> /dev/null) -gt 0 ]] && sudo swapoff -a && sudo swapon -a
free -h

# Script End