#!/usr/bin/env bash

# Target: Backup self-host service data (configs, sql, data)

# Change Log
# - Jun 20, 2022 Mon 10:03 ET - add invidious
# - Jun 11, 2022 Sat 16:30 ET - add jellyfin, synapse
# - Jan 05, 2022 Wed 13:00 ET - rewrite script, merge configs, sql, data script into one
# - Dec 25, 2021 Sat 11:55 ET - first draft

# remote save dir: $remote_path/{data,configs}/

# - variables definition
data_dir=${data_dir:-'/data'}
date_format=$(date +'%Y%m%d_%H%M')  # 20211225_1155
remote_path=${remote_path:-}
services_config_dir=${services_config_dir:-}
rclone_exist=${rclone_exist:-1}

readonly project_name='Docker_Selfhost'
local_save_dir="$HOME/Documents/${project_name}_Backup"
docker_service_absoute_dir=${docker_service_absoute_dir:-}

version_keep_num=6  # just keep the latest X versions

fn_Precheck(){
    # - essential utilities detection
    if [[ -z $(which zip 2> /dev/null) ]]; then
        echo "Fail to find utility 'zip'"; exit 0
    fi

    # rclone config detection
    if [[ -n $(which rclone 2> /dev/null) ]]; then
        # rclone fail to read var RCLONE_PASSWORD_COMMAND in cron job
        # https://rclone.org/docs/#configuration-encryption
        rclone_config_dir=${rclone_config_dir:-"$HOME/.config/rclone"}
        rclone_config_path="${rclone_config_dir}/rclone.conf"

        if [[ -n "$(sed -r  -n '1,4{/RCLONE_ENCRYPT_/{p}}' "${rclone_config_path}" 2> /dev/null)" ]]; then
            rclone_pass=$(find "${rclone_config_dir}" -type f -name 'pass' -print 2> /dev/null | xargs -I %s cat %s | sed '/^$/d;/^#/d')
            export RCLONE_CONFIG_PASS="${rclone_pass}"
        fi
    else
        rclone_exist=0
    fi

    # - get script absolute path
    script_absolute_path=$(readlink -nf "$0")
    script_absolute_dir=$(dirname "${script_absolute_path}")
    docker_service_absoute_dir=$(dirname "${script_absolute_dir}")

    # get env file
    env_absolute_path="${docker_service_absoute_dir}/.env"
    [[ ! -f "${env_absolute_path}" && -f "${docker_service_absoute_dir}/entry.sh" ]] && bash "${docker_service_absoute_dir}/entry.sh" &> /dev/null
    if [[ ! -f "${env_absolute_path}" ]]; then
        echo 'Sorry, fail to find env file'; exit 0
    fi

    [[ -f "${env_absolute_path}" ]] && data_dir=$(sed -r -n '/^DATA_DIR=/I{s@^[^=]+=@@g;s@[[:space:]]*#+.*$@@g;s@"@@g;s@'\''@@g;s@[[:space:]]*$@@g;p;q}' "${env_absolute_path}" 2> /dev/null)

    [[ -f "${env_absolute_path}" ]] && remote_path=$(sed -r -n '/^REMOTE_PATH=/I{s@^[^=]+=@@g;s@[[:space:]]*#+.*$@@g;s@"@@g;s@'\''@@g;s@[[:space:]]*$@@g;p;q}' "${env_absolute_path}" 2> /dev/null)
    # e.g: REMOTE_PATH="gdrive_configs:Docker_Selfhost/"

    if [[ -z "${remote_path}" ]]; then
        echo "Attention: no parameter 'REMOTE_PATH' detected from env file ${env_absolute_path}"
    else
        remote_path="${remote_path%%:*}:${project_name}"
    fi

    [[ -f "${env_absolute_path}" ]] && services_config_dir=$(sed -r -n '/^SERVICE_DIR=/{s@^[^=]+=@@g;s@[[:space:]]*#+.*$@@g;s@"@@g;s@'\''@@g;s@[[:space:]]*$@@g;p;q}' "${env_absolute_path}" 2> /dev/null)

}

# Docker_Selfhost
#  - Configs: Nginx configs, docker compose services files
#  - Services
#    - nextcloud/{data,sql}
#    - standardnotes/{data,sql,extension}
#    - bitwarden/{data}


fn_Configs_Backup(){
    echo -e '\nConfigs backup ... '
    local l_remote_save_path
    l_remote_save_path="${remote_path%\/}/Configs"

    local l_config_save_name
    l_config_save_name="docker_configs_${date_format}"
    local l_config_save_dir
    l_config_save_dir="${local_save_dir}/Configs/${l_config_save_name}"

    [[ -d "${l_config_save_dir}" ]] || mkdir -p "${l_config_save_dir}"
    cd "${l_config_save_dir}" || return

    # docker configs
    if [[ -d "${docker_service_absoute_dir}" ]]; then
        cp -p -R "${docker_service_absoute_dir}" "${docker_service_absoute_dir##*/}"
    fi

    # nginx configs
    nginx_config_dir=${nginx_config_dir:-'/etc/nginx'}

    if [[ -d "${nginx_config_dir}" ]]; then
        mkdir -p nginx
        sudo cp -p -R "${nginx_config_dir}"/{nginx.conf,conf.d,sites-enabled,ssl.d} nginx/
        sudo chown -R "$USER":"$USER" nginx/  # not work in crontab
    fi

    # crontab backup
    crontab -u "${USER}" -l > "crontab_${date_format}.txt"

    # compress
    local l_compress_save_path
    l_compress_save_path="${l_config_save_name}.zip"
    cd "${l_config_save_dir%/*}" || return
    zip -r -q -9 "${l_compress_save_path}" "${l_config_save_name}"
    stat "${l_compress_save_path}"

    # delete dir
    [[ -d "${l_config_save_dir}" ]] && sudo rm -r "${l_config_save_dir}"

    # upload
    if [[ "${rclone_exist}" -eq 1 ]]; then
        rclone -P --no-update-modtime copy "${l_compress_save_path}" "${l_remote_save_path}"

        # just keep latest X versions
        rclone lsf "${l_remote_save_path}" | sort -r | sed '1,'"${version_keep_num}"'d' | xargs -I %s rclone deletefile "${l_remote_save_path}/%s"
    fi

    # just keep latest X versions
    find "${l_config_save_dir%/*}" -type f -size +2k -name "*.${l_compress_save_path##*.}" -print | sort -r | sed '1,'"${version_keep_num}"'d' | xargs -I %s rm -f %s
}

fn_Service_SQL_Dump(){
    echo -e '\nService SQL dump ...'

    local l_remote_service_path
    l_remote_service_path="${remote_path%\/}/Services"

    local l_data_save_dir
    l_data_save_dir="${local_save_dir}/Services"

    # not include nextcloud
    find "${services_config_dir}" -mindepth 1 -maxdepth 1 -type d -print | sed '/\/nextclouD$/Id' | while read -r item; do
        cd "${item}" || return

        local l_service_name

        l_service_name="${item##*/}"
        l_service_name="${l_service_name,,}"

        if [[ $(docker compose ps -q 2> /dev/null | wc -l) -ge 1 ]]; then
            echo -e " - service ${l_service_name}"

            sql_dump_dir="${l_data_save_dir}/${l_service_name}/sql"
            [[ -d "${sql_dump_dir}" ]] || mkdir -p "${sql_dump_dir}"

            l_remote_save_path="${l_remote_service_path}/${l_service_name}/sql"
            local l_sql_save_path=''

            case "${l_service_name}" in
                standardnotes)
                    db_name=$(sed -r -n '/^DB_DATABASE=/{s@^[^=]+=@@g;p;q}' .env 2> /dev/null)
                    db_pass=$(sed -r -n '/^DB_PASSWORD=/{s@^[^=]+=@@g;p;q}' .env 2> /dev/null)
                    db_container_id=$(sed -r -n '/image:[[:space:]]*mysql:/,/^$/{/container_name:/{s@^[^:]+:[[:space:]]*@@g;p;q}}' docker-compose.yml | xargs -I %s docker ps --filter name="%s" --format "{{.ID}}")

                    l_sql_save_path="${sql_dump_dir}/dbdump_${date_format}.sql"
                    # via `docker exec`
                    docker exec "${db_container_id}" sh -c 'exec mysqldump '${db_name}' -uroot -p"'${db_pass}'" 2> /dev/null' > "${l_sql_save_path}"

                    # just keep latest X versions
                    find "${l_sql_save_path%/*}" -type f -size +2k -name "*.${l_sql_save_path##*.}" -print | sort -r | sed '1,'"${version_keep_num}"'d' | xargs -I %s rm -f %s
                ;;
                invidious)
                    db_name=$(sed -r -n '/^POSTGRES_DB=/{s@^[^=]+=@@g;p;q}' .env 2> /dev/null)
                    db_pass=$(sed -r -n '/^DB_PASSWORD=/{s@^[^=]+=@@g;p;q}' .env 2> /dev/null)
                    db_user=$(sed -r -n '/^POSTGRES_USER=/{s@^[^=]+=@@g;p;q}' .env 2> /dev/null)
                    db_container_id=$(sed -r -n '/image:[[:space:]]*postgres:/,/^$/{/container_name:/{s@^[^:]+:[[:space:]]*@@g;p;q}}' docker-compose.yml | xargs -I %s docker ps --filter name="%s" --format "{{.ID}}")

                    l_sql_save_path="${sql_dump_dir}/pqsql_dump_${date_format}.sql"
                    # via `docker exec`
                    # - clean table videos items
                    docker exec "${db_container_id}" sh -c 'PGPASSWORD="'${db_pass}'" psql "'${db_name}'" -h localhost -U "'${db_user}'" -c "delete from nonces * where expire < current_timestamp; truncate table videos; truncate table channel_videos;"' 1> /dev/null
                    # - dump database
                    docker exec "${db_container_id}" sh -c 'PGPASSWORD="'${db_pass}'" pg_dump "'${db_name}'" -h localhost -U "'${db_user}'"' > "${l_sql_save_path}"

                    # just keep latest X versions
                    find "${l_sql_save_path%/*}" -type f -size +2k -name "*.${l_sql_save_path##*.}" -print | sort -r | sed '1,'"${version_keep_num}"'d' | xargs -I %s rm -f %s
                ;;
            esac

            # sync to remote path
            if [[ "${rclone_exist}" -eq 1 && -s "${l_sql_save_path}" ]]; then
                rclone -P --no-update-modtime copy "${l_sql_save_path}" "${l_remote_save_path}"

                # just keep latest X versions
                rclone lsf "${l_remote_save_path}" | sort -r | sed '1,'"${version_keep_num}"'d' | xargs -I %s rclone deletefile "${l_remote_save_path}/%s"
            fi
        fi

    done

    # just for nextcloud (it will make while loop exit, don't know why?)
    find "${services_config_dir}" -mindepth 1 -maxdepth 1 -type d -print | sed '/\/nextclouD$/I!d' | while read -r item; do
        cd "${item}" || return

        local l_service_name

        l_service_name="${item##*/}"
        l_service_name="${l_service_name,,}"

        if [[ $(docker compose ps -q 2> /dev/null | wc -l) -ge 1 ]]; then
            echo -e " - service ${l_service_name}"

            sql_dump_dir="${l_data_save_dir}/${l_service_name}/sql"
            [[ -d "${sql_dump_dir}" ]] || mkdir -p "${sql_dump_dir}"

            l_remote_save_path="${l_remote_service_path}/${l_service_name}/sql"
            local l_sql_save_path=''

            case "${l_service_name}" in
                nextcloud)
                    # tutorial https://forum.openmediavault.org/index.php?thread/30925-nextcloud-docker-backup-with-db/

                    # docker compose exec --user www-data app  # here 'app' is nextcloud service name defined in docker compose file

                    # https://docs.nextcloud.com/server/latest/admin_manual/maintenance/backup.html#maintenance-mode
                    # 'maintenance:mode' locks the sessions of logged-in users and prevents new logins in order to prevent inconsistencies of your data.

                    # docker compose exec --user www-data app php occ maintenance:mode --on  # maintenance mode on
                    # PHP Fatal error:  Cannot use OCP\Constants as Constants because the name is already in use in /var/www/html/custom_apps/groupfolders/lib/Command/ACL.php on line 35

                    # config path in container: /var/www/html/config/config.php
                    local l_nextcloud_db_info
                    l_nextcloud_db_info=$(docker compose exec --user www-data app php -r 'include("/var/www/html/config/config.php"); print $CONFIG["dbtype"]."|".$CONFIG["dbhost"]."|".$CONFIG["dbname"]."|".$CONFIG["dbuser"]."|".$CONFIG["dbpassword"];')
                    # note: docker compose v2.2.3 (Jan 06, 2022) make this command panic https://github.com/docker/compose/releases/tag/v2.2.3

                    # dbhost is name defined in docker compose file
                    # dbtype|dbhost|dbname|dbuser|dbpassword
                    # pgsql|db_p|nextcloud|oc_admin|XXXXXXXXXXXXXX

                    local l_db_type
                    l_db_type=$(cut -d\| -f1 <<< "${l_nextcloud_db_info}")
                    local l_db_host
                    l_db_host=$(cut -d\| -f2 <<< "${l_nextcloud_db_info}") # this is database service name defined in docker compose file
                    local l_db_name
                    l_db_name=$(cut -d\| -f3 <<< "${l_nextcloud_db_info}")
                    local l_db_pass
                    l_db_pass=$(cut -d\| -f5 <<< "${l_nextcloud_db_info}")

                    local l_db_user
                    l_db_user=$(cut -d\| -f4 <<< "${l_nextcloud_db_info}") # here 'oc_admin' isn't the exact user name, find it from file '.env'
                    db_user_search='POSTGRES_USER'
                    [[ "${l_db_type}" != 'pgsql' ]] && db_user_search='MYSQL_USER'
                    [[ "${l_db_user}" == 'oc_admin' && -f .env ]] && l_db_user=$(sed -r -n '/^'"${db_user_search}"'=/{s@^[^=]+=@@g;s@[[:space:]]*$@@g;p;q}' .env 2> /dev/null)
                    [[ -z "${l_db_user}" ]] && l_db_user='nextcloud'

                    l_sql_save_path="${sql_dump_dir}/${l_db_type}_dump_${date_format}.sql"

                    if [[ "${l_db_type}" == 'pgsql' ]]; then
                        # clean table oc_activity items
                        docker compose exec "${l_db_host}" sh -c 'PGPASSWORD="'${l_db_pass}'" psql "'${l_db_name}'" -h localhost -U "'${l_db_user}'" -c "delete from oc_activity;select count(*) from oc_activity"' 1> /dev/null # "select count(*) from oc_activity"

                        # pg_dump path in container is /usr/local/bin/pg_dump
                        # PGPASSWORD="password" pg_dump [db_name] -h [server] -U [username] -f nextcloud-sqlbkp_`date +"%Y%m%d"`.bak  # https://docs.nextcloud.com/server/latest/admin_manual/maintenance/backup.html#postgresql
                        docker compose exec "${l_db_host}" sh -c 'PGPASSWORD="'${l_db_pass}'" pg_dump "'${l_db_name}'" -h localhost -U "'${l_db_user}'"' > "${l_sql_save_path}"
                    elif [[ "${l_db_type}" == 'mysql' ]]; then
                        # MySQL / MariaDB
                        echo 'Not deployed for MySQL/MariaDB'
                        # mysqldump --single-transaction -h [server] -u [username] -p[password] [db_name] > nextcloud-sqlbkp_`date +"%Y%m%d"`.bak
                        # mysqldump --single-transaction --default-character-set=utf8mb4 -h [server] -u [username] -p[password] [db_name] > nextcloud-sqlbkp_`date +"%Y%m%d"`.bak
                    fi

                    # docker compose exec --user www-data app php occ maintenance:mode --off  # maintenance mode off

                    # just keep latest X versions
                    find "${l_sql_save_path%/*}" -type f -size +2k -name "*.${l_sql_save_path##*.}" -print | sort -r | sed '1,'"${version_keep_num}"'d' | xargs -I %s rm -f %s
                ;;
            esac

            # sync to remote path
            if [[ "${rclone_exist}" -eq 1 && -s "${l_sql_save_path}" ]]; then
                rclone -P --no-update-modtime copy "${l_sql_save_path}" "${l_remote_save_path}"

                # just keep latest X versions
                rclone lsf "${l_remote_save_path}" | sort -r | sed '1,'"${version_keep_num}"'d' | xargs -I %s rclone deletefile "${l_remote_save_path}/%s"
            fi
        fi

    done
}

fn_Service_Data_Backup(){
    echo -e '\nService Data backup ... '
    local l_remote_save_path
    l_remote_save_path="${remote_path%\/}/Services"

    local l_data_save_dir
    l_data_save_dir="${local_save_dir}/Services"

    # - compress & upload
    cd "${data_dir}" || return

    # compress all data under dir /data, exclude dirs specified
    # zip -r data_$(date +'%Y%m%d_%H%M').zip data/ -x "data/nextcloud/data/*" -x "data/nextcloud/html/*" -x "data/standardnotes/extensions/*"

    # /data/nextcloud -> nextcloud
    # sed \L to lowercase, \U to uppercase
    find "${data_dir}" -mindepth 1 -maxdepth 1 -type d -print | sed -r -n 's@.*\/([^\/]+)$@\L\1@g;p' | while read -r item; do
        save_path="${l_data_save_dir}/${item}/data/${item}_data_${date_format}.zip"
        [[ -d "${save_path%/*}" ]] || mkdir -p "${save_path%/*}"

        case "${item}" in
            vaultwarden)
                # exclude icon_cache/
                sudo zip -q -r -9 "${save_path}" "${item}"/ -x "${item}/icon_cache/*"
            ;;
            standardnotes)
                # exclude extensions/
                # sudo zip -q -r -9 "${save_path}" "${item}"/ -x "${item}/extensions/*"

                # don't backup dir, just export database sql
                echo ''
            ;;
            nextcloud)
                # exclude {data,html}/
                # sudo zip -q -r -9 "${save_path}" "${item}"/ -x "${item}/data/*" -x "${item}/html/*"

                # may be need file html/config/config.php
                echo ''
            ;;
            jellyfin)
                # exclude config/cache, config/data/metadata/
                sudo zip -q -r -9 "${save_path}" "${item}"/ -x "${item}/config/data/metadata/*" -x "${item}/config/cache/*"
            ;;
            synapse)
                # exclude media_store/
                sudo zip -q -r -9 "${save_path}" "${item}"/files/ -x "${item}/media_store/*"
            ;;
            # *)
            #     sudo zip -q -r -9 "${save_path}" "${item}"/
            # ;;
        esac

        # just keep latest X versions
        find "${save_path%/*}" -type f -size +2k -name "*.${save_path##*.}" -print | sort -r | sed '1,'"${version_keep_num}"'d' | xargs -I %s rm -f %s

        if [[ "${rclone_exist}" -eq 1 && -s "${save_path}" ]]; then
            l_remote_save_path_s="${l_remote_save_path}/${item}/data"
            rclone -P --no-update-modtime copy "${save_path}" "${l_remote_save_path_s}"

            # just keep latest X versions
            rclone lsf "${l_remote_save_path_s}" | sort -r | sed '1,'"${version_keep_num}"'d' | xargs -I %s rclone deletefile "${l_remote_save_path_s}/%s"
        fi
    done
}


# Main function
fn_Main(){
    fn_Precheck
    fn_Configs_Backup
    fn_Service_SQL_Dump
    fn_Service_Data_Backup
}

fn_Main


# Script End