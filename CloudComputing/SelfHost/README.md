# Self-hosting Applications

## TOC

1. [Pre-requisite](#pre-requisite)  
2. [Applications List](#applications-list)  
3. [Resources](#resources)  
4. [Change Log](#change-log)  


## Pre-requisite

All applications are runned as [Docker][docker] containers which expose specified TCP port to web server [Nginx][nginx].

[Nginx][nginx] works as a reverse proxy.

Official documentation

Item|Doc Url|Installation
---|---|---
[Nginx][nginx]|https://nginx.org/en/<br/>https://docs.nginx.com|https://nginx.org/en/docs/install.html
[Docker][docker]|https://docs.docker.com|https://docs.docker.com/get-docker/<br/>For distros [Debian](https://docs.docker.com/engine/install/debian/) / [Ubuntu](https://docs.docker.com/engine/install/ubuntu/) / [CentOS](https://docs.docker.com/engine/install/centos/) / [Fedora](https://docs.docker.com/engine/install/fedora/)


[Nginx][nginx] runs on  `reverse` droplet, [Docker][docker] containers are running on `backend` droplets. They communicate via private ip in the same vpc.

Attention: issue [Uncomplicated Firewall (UFW) is not blocking anything when using Docker](https://askubuntu.com/questions/652556/uncomplicated-firewall-ufw-is-not-blocking-anything-when-using-docker#652572).


## Applications List

Application|Description
---|---
[authelia](./Applications/authelia/README.md)|auth server
[vaultwarden](./Applications/vaultwarden/README.md)|password manager
[nextcloud](./Applications/nextcloud/README.md)|file management platform
[joplin server](./Applications/joplin_server/README.md)|note app
[standard notes](./Applications/standard_notes/README.md)|note app
[jitsi meet](./Applications/jitsi-meet/README.md)|video conference
[code server](./Applications/code-server/README.md)|code editor
[emby server](./Applications/emby/README.md)|media system
[Jellyfin server](./Applications/jellyfin/README.md)|media system


### Port Assigned

Service|Port
---|---
authelia|8091 (server)
emby|8006 (server)
jellyfin|8016 (server)
vaultwarden|8000 (server)<br/>8012 (websocket)
code-server|8043 (server)
joplin notes|8100 (server)
standard notes|8200 (api gateway)<br/>8201 (extension)<br/>8202 (webapp)<br/>8203 (filesafe)
nextcloud|8300 (http)
jitsi meet|8400 (http)<br/>8443 (https)
Synapse|8048 (server) ~~8078 (turn)~~
GitLab|8020 (http) 8021 (https) 8022 (ssh)
Webtop|8005 (server)
Invidious|8500 (server)
SearXNG|8050 (server)
Wireguard|8002


## Resources

* [GitHub - awesome-selfhosted](https://github.com/awesome-selfhosted/awesome-selfhosted)
* [The Self-Hosting Blog](https://theselfhostingblog.com)


## Change Log

* Mar 04, 2021 09:05 Thu ET
  * First draft


[nginx]: https://www.nginx.com "High Performance Load Balancer, Web Server, & Reverse Proxy"
[docker]: https://www.docker.com

<!-- End -->