# Cloud Computing

## TOC

1. [Category](#category)  
1.1 [Providers](#providers)  
1.2 [Containers](#containers)  
1.3 [Tools](#tools)  
1.4 [Self-Host](#self-host)  
2. [Resources](#resources)  


## Category

### Providers

* [AWS](./Providers/AWS)
  * [Cli Learning](./Providers/AWS/CLI-Learning/)
  * [Custom Scripts](./Providers/AWS/Scripts)
* [Digital Ocean](./Providers/DigitalOcean)
  * [doctl](./Providers/DigitalOcean/doctl)

### Containers

* [Docker](./Containers/Docker)

### Tools

* [Rclone](./Tools/Rclone)


### Self-Host

Applications [list](./SelfHost/Applications/)

* [bitwarden_rs](./SelfHost/Applications/bitwarden_rs/)
* [joplin server](./SelfHost/Applications/joplin_server/)
* [standard notes](./SelfHost/Applications/standard_notes/)
* [jitsi meet](./SelfHost/Applications/jitsi-meet/)


## Resources

* [sumo logic | Resource Center](https://www.sumologic.com/resources/)

<!-- End -->