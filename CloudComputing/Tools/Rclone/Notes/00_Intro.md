# Rclone Simple Intro


## TOC

1. [Cloud Storage Systems](#cloud-storage-systems)  
1.1 [Supported providers](#supported-providers)  
2. [Rclone Commands](#rclone-commands)  
2.1 [Command Classification](#command-classification)  
3. [Change Log](#change-log)  


## Cloud Storage Systems

[Rclone][rclone] supports over 40 cloud storage products which listed on page [Overview of cloud storage systems](https://rclone.org/overview/).

Features

Item|Value
---|---
Major Features|https://rclone.org/overview/#features
Optional Features|https://rclone.org/overview/#optional-features

### Supported providers

Official page <https://rclone.org/#providers> lists supported provides.

Provider lists

<details><summary>Click to expand provider list</summary>

Provider|Config Page
---|----
[1Fichier](https://1fichier.com/)|https://rclone.org/fichier/
[Alibaba Cloud (Aliyun) Object Storage System (OSS)](https://www.alibabacloud.com/product/oss/)|https://rclone.org/s3/#alibaba-oss
[Amazon Drive](https://www.amazon.com/clouddrive)|https://rclone.org/amazonclouddrive/
[Amazon S3](https://aws.amazon.com/s3/)|https://rclone.org/s3/
[Backblaze B2](https://www.backblaze.com/b2/cloud-storage.html)|https://rclone.org/b2/
[Box](https://www.box.com/)|https://rclone.org/box/
[Ceph](http://ceph.com/)|https://rclone.org/s3/#ceph
[Citrix ShareFile](http://sharefile.com/)|https://rclone.org/sharefile/
[C14](https://www.online.net/en/storage/c14-cold-storage)|https://rclone.org/sftp/#c14
[DigitalOcean Spaces](https://www.digitalocean.com/products/object-storage/)|https://rclone.org/s3/#digitalocean-spaces
[Dreamhost](https://www.dreamhost.com/cloud/storage/)|https://rclone.org/s3/#dreamhost
[Dropbox](https://www.dropbox.com/)|https://rclone.org/dropbox/
[Enterprise File Fabric](https://storagemadeeasy.com/about/)|https://rclone.org/filefabric/
[FTP](https://en.wikipedia.org/wiki/File_Transfer_Protocol)|https://rclone.org/ftp/
[Google Cloud Storage](https://cloud.google.com/storage/)|https://rclone.org/googlecloudstorage/
[Google Drive](https://www.google.com/drive/)|https://rclone.org/drive/
[Google Photos](https://www.google.com/photos/about/)|https://rclone.org/googlephotos/
[HDFS](https://hadoop.apache.org/)|https://rclone.org/hdfs/
[HTTP](https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol)|https://rclone.org/http/
[Hubic](https://hubic.com/)|https://rclone.org/hubic/
[Jottacloud](https://www.jottacloud.com/en/)|https://rclone.org/jottacloud/
[IBM COS S3](http://www.ibm.com/cloud/object-storage)|https://rclone.org/s3/#ibm-cos-s3
[Koofr](https://koofr.eu/)|https://rclone.org/koofr/
[Mail.ru Cloud](https://cloud.mail.ru/)|https://rclone.org/mailru/
[Memset Memstore](https://www.memset.com/cloud/storage/)|https://rclone.org/swift/
[Mega](https://mega.nz/)|https://rclone.org/mega/
[Memory](https://rclone.org/memory/)|https://rclone.org/memory/
[Microsoft Azure Blob Storage](https://azure.microsoft.com/en-us/services/storage/blobs/)|https://rclone.org/azureblob/
[Microsoft OneDrive](https://onedrive.live.com/)|https://rclone.org/onedrive/
[Minio](https://www.minio.io/)|https://rclone.org/s3/#minio
[Nextcloud](https://nextcloud.com/)|https://rclone.org/webdav/#nextcloud
[OVH](https://www.ovh.co.uk/public-cloud/storage/object-storage/)|https://rclone.org/swift/
[OpenDrive](https://www.opendrive.com/)|https://rclone.org/opendrive/
[OpenStack Swift](https://docs.openstack.org/swift/latest/)|https://rclone.org/swift/
[Oracle Cloud Storage](https://cloud.oracle.com/storage-opc)|https://rclone.org/swift/
[ownCloud](https://owncloud.org/)|https://rclone.org/webdav/#owncloud
[pCloud](https://www.pcloud.com/)|https://rclone.org/pcloud/
[premiumize.me](https://premiumize.me/)|https://rclone.org/premiumizeme/
[put.io](https://put.io/)|https://rclone.org/putio/
[QingStor](https://www.qingcloud.com/products/storage)|https://rclone.org/qingstor/
[Rackspace Cloud Files](https://www.rackspace.com/cloud/files)|https://rclone.org/swift/
[rsync.net](https://rsync.net/products/rclone.html)|https://rclone.org/sftp/#rsync-net
[Scaleway](https://www.scaleway.com/object-storage/)|https://rclone.org/s3/#scaleway
[Seafile](https://www.seafile.com/)|https://rclone.org/seafile/
[SFTP](https://en.wikipedia.org/wiki/SSH_File_Transfer_Protocol)|https://rclone.org/sftp/
[StackPath](https://www.stackpath.com/products/object-storage/)|https://rclone.org/s3/#stackpath
[SugarSync](https://sugarsync.com/)|https://rclone.org/sugarsync/
[Tardigrade](https://tardigrade.io/)|https://rclone.org/tardigrade/
[Tencent Cloud Object Storage (COS)](https://intl.cloud.tencent.com/product/cos)|https://rclone.org/s3/#tencent-cos
[Wasabi](https://wasabi.com/)|https://rclone.org/s3/#wasabi
[WebDAV](https://en.wikipedia.org/wiki/WebDAV)|https://rclone.org/webdav/
[Yandex Disk](https://disk.yandex.com/)|https://rclone.org/yandex/
[Zoho WorkDrive](https://www.zoho.com/workdrive/)|https://rclone.org/zoho/
[The local filesystem](https://rclone.org/local/)|https://rclone.org/local/

</details>

If you wanna extract the latest supported platforms, you can use the following commands

<details><summary>Click to expand extract command</summary>

```sh
official_site='https://rclone.org'

curl -fsL "${official_site}" | sed -r -n '/<ul.*list-group[^>]*>/,/<\/ul>/{s@^[[:space:]]*@@g;/^$/d;s@[[:space:]]*\(<a[^)]*>\)[[:space:]]*@@g;/^[^<]+$/{s@.*@&|@g};/href/{s@.*href="([^"]+)".*@\1|@g};p}' | sed ':a;N;$!ba;s@\n@@g' | sed -r -n 's@</li>@&\n@g;p'  | sed -r -n '/<li[^>]*>/{s@[[:space:]]*<[^>]*>[[:space:]]*@@g;s@\|$@@g;p}' | while IFS="|" read -r provider home_link config_link; do
    [[ "${home_link}" =~ ^\/ ]] && home_link="${official_site}${home_link}"
    [[ "${config_link}" =~ ^\/ ]] && config_link="${official_site}${config_link}"
    echo "[${provider}](${home_link})|${config_link}"
done
```

</details>


## Rclone Commands

Official page <https://rclone.org/commands/> lists all commands in rclone.

Command lists

<details><summary>Click to expand commands list</summary>

Command|Description
---|----
[rclone](https://rclone.org/commands/rclone/)|Show help for rclone commands, flags and backends.
[rclone about](https://rclone.org/commands/rclone_about/)|Get quota information from the remote.
[rclone authorize](https://rclone.org/commands/rclone_authorize/)|Remote authorization.
[rclone backend](https://rclone.org/commands/rclone_backend/)|Run a backend specific command.
[rclone cat](https://rclone.org/commands/rclone_cat/)|Concatenates any files and sends them to stdout.
[rclone check](https://rclone.org/commands/rclone_check/)|Checks the files in the source and destination match.
[rclone cleanup](https://rclone.org/commands/rclone_cleanup/)|Clean up the remote if possible.
[rclone config](https://rclone.org/commands/rclone_config/)|Enter an interactive configuration session.
[rclone config create](https://rclone.org/commands/rclone_config_create/)|Create a new remote with name, type and options.
[rclone config delete](https://rclone.org/commands/rclone_config_delete/)|Delete an existing remotename.
[rclone config disconnect](https://rclone.org/commands/rclone_config_disconnect/)|Disconnects user from remote
[rclone config dump](https://rclone.org/commands/rclone_config_dump/)|Dump the config file as JSON.
[rclone config edit](https://rclone.org/commands/rclone_config_edit/)|Enter an interactive configuration session.
[rclone config file](https://rclone.org/commands/rclone_config_file/)|Show path of configuration file in use.
[rclone config password](https://rclone.org/commands/rclone_config_password/)|Update password in an existing remote.
[rclone config providers](https://rclone.org/commands/rclone_config_providers/)|List in JSON format all the providers and options.
[rclone config reconnect](https://rclone.org/commands/rclone_config_reconnect/)|Re-authenticates user with remote.
[rclone config show](https://rclone.org/commands/rclone_config_show/)|Print (decrypted) config file, or the config for a single remote.
[rclone config update](https://rclone.org/commands/rclone_config_update/)|Update options in an existing remote.
[rclone config userinfo](https://rclone.org/commands/rclone_config_userinfo/)|Prints info about logged in user of remote.
[rclone copy](https://rclone.org/commands/rclone_copy/)|Copy files from source to dest, skipping already copied.
[rclone copyto](https://rclone.org/commands/rclone_copyto/)|Copy files from source to dest, skipping already copied.
[rclone copyurl](https://rclone.org/commands/rclone_copyurl/)|Copy url content to dest.
[rclone cryptcheck](https://rclone.org/commands/rclone_cryptcheck/)|Cryptcheck checks the integrity of a crypted remote.
[rclone cryptdecode](https://rclone.org/commands/rclone_cryptdecode/)|Cryptdecode returns unencrypted file names.
[rclone dedupe](https://rclone.org/commands/rclone_dedupe/)|Interactively find duplicate filenames and delete/rename them.
[rclone delete](https://rclone.org/commands/rclone_delete/)|Remove the files in path.
[rclone deletefile](https://rclone.org/commands/rclone_deletefile/)|Remove a single file from remote.
[rclone genautocomplete](https://rclone.org/commands/rclone_genautocomplete/)|Output completion script for a given shell.
[rclone genautocomplete bash](https://rclone.org/commands/rclone_genautocomplete_bash/)|Output bash completion script for rclone.
[rclone genautocomplete fish](https://rclone.org/commands/rclone_genautocomplete_fish/)|Output fish completion script for rclone.
[rclone genautocomplete zsh](https://rclone.org/commands/rclone_genautocomplete_zsh/)|Output zsh completion script for rclone.
[rclone gendocs](https://rclone.org/commands/rclone_gendocs/)|Output markdown docs for rclone to the directory supplied.
[rclone hashsum](https://rclone.org/commands/rclone_hashsum/)|Produces a hashsum file for all the objects in the path.
[rclone link](https://rclone.org/commands/rclone_link/)|Generate public link to file/folder.
[rclone listremotes](https://rclone.org/commands/rclone_listremotes/)|List all the remotes in the config file.
[rclone ls](https://rclone.org/commands/rclone_ls/)|List the objects in the path with size and path.
[rclone lsd](https://rclone.org/commands/rclone_lsd/)|List all directories/containers/buckets in the path.
[rclone lsf](https://rclone.org/commands/rclone_lsf/)|List directories and objects in remote:path formatted for parsing.
[rclone lsjson](https://rclone.org/commands/rclone_lsjson/)|List directories and objects in the path in JSON format.
[rclone lsl](https://rclone.org/commands/rclone_lsl/)|List the objects in path with modification time, size and path.
[rclone md5sum](https://rclone.org/commands/rclone_md5sum/)|Produces an md5sum file for all the objects in the path.
[rclone mkdir](https://rclone.org/commands/rclone_mkdir/)|Make the path if it doesn't already exist.
[rclone mount](https://rclone.org/commands/rclone_mount/)|Mount the remote as file system on a mountpoint.
[rclone move](https://rclone.org/commands/rclone_move/)|Move files from source to dest.
[rclone moveto](https://rclone.org/commands/rclone_moveto/)|Move file or directory from source to dest.
[rclone ncdu](https://rclone.org/commands/rclone_ncdu/)|Explore a remote with a text based user interface.
[rclone obscure](https://rclone.org/commands/rclone_obscure/)|Obscure password for use in the rclone config file.
[rclone purge](https://rclone.org/commands/rclone_purge/)|Remove the path and all of its contents.
[rclone rc](https://rclone.org/commands/rclone_rc/)|Run a command against a running rclone.
[rclone rcat](https://rclone.org/commands/rclone_rcat/)|Copies standard input to file on remote.
[rclone rcd](https://rclone.org/commands/rclone_rcd/)|Run rclone listening to remote control commands only.
[rclone rmdir](https://rclone.org/commands/rclone_rmdir/)|Remove the empty directory at path.
[rclone rmdirs](https://rclone.org/commands/rclone_rmdirs/)|Remove empty directories under the path.
[rclone selfupdate](https://rclone.org/commands/rclone_selfupdate/)|Update the rclone binary.
[rclone serve](https://rclone.org/commands/rclone_serve/)|Serve a remote over a protocol.
[rclone serve dlna](https://rclone.org/commands/rclone_serve_dlna/)|Serve remote:path over DLNA
[rclone serve ftp](https://rclone.org/commands/rclone_serve_ftp/)|Serve remote:path over FTP.
[rclone serve http](https://rclone.org/commands/rclone_serve_http/)|Serve the remote over HTTP.
[rclone serve restic](https://rclone.org/commands/rclone_serve_restic/)|Serve the remote for restic's REST API.
[rclone serve sftp](https://rclone.org/commands/rclone_serve_sftp/)|Serve the remote over SFTP.
[rclone serve webdav](https://rclone.org/commands/rclone_serve_webdav/)|Serve remote:path over webdav.
[rclone settier](https://rclone.org/commands/rclone_settier/)|Changes storage class/tier of objects in remote.
[rclone sha1sum](https://rclone.org/commands/rclone_sha1sum/)|Produces an sha1sum file for all the objects in the path.
[rclone size](https://rclone.org/commands/rclone_size/)|Prints the total size and number of objects in remote:path.
[rclone sync](https://rclone.org/commands/rclone_sync/)|Make source and dest identical, modifying destination only.
[rclone test](https://rclone.org/commands/rclone_test/)|Run a test command
[rclone test histogram](https://rclone.org/commands/rclone_test_histogram/)|Makes a histogram of file name characters.
[rclone test info](https://rclone.org/commands/rclone_test_info/)|Discovers file name or other limitations for paths.
[rclone test makefiles](https://rclone.org/commands/rclone_test_makefiles/)|Make a random file hierarchy in
[rclone test memory](https://rclone.org/commands/rclone_test_memory/)|Load all the objects at remote:path into memory and report memory stats.
[rclone touch](https://rclone.org/commands/rclone_touch/)|Create new file or change file modification time.
[rclone tree](https://rclone.org/commands/rclone_tree/)|List the contents of the remote in a tree like fashion.
[rclone version](https://rclone.org/commands/rclone_version/)|Show the version number.

</details>

If you wanna extract the latest command lists, you can use the following commands

<details><summary>Click to expand extract command</summary>

```sh
official_site='https://rclone.org'

curl -fsL "${official_site}/commands/" | sed -r -n '/<table[^>]*>/,/<\/table>/{s@^[[:space:]]*@@g;/^$/d;p}' | sed ':a;N;$!ba;s@\n@@g' | sed -r -n 's@<\/tr>@&\n@g;p' | sed -r -n '/href=/{s@<\/a>@|@g;s@.*href="([^"]+)"[^>]*>@'"${official_site}"'\1|@g;s@[[:space:]]*<[^>]*>[[:space:]]*@@g;p}' | sed -r -n 's@^([^\|]+)\|([^\|]+)\|(.*)$@[\2](\1)|\3@g;p'  # awk -F\| '{printf("[%s](%s)|%s\n",$2,$1,$3)}'
```

</details>

### Command Classification

Remote Info

Command|Explanation
---|---
`rclone about`|Get quota information from the remote.
`rclone listremotes`|List all the remotes in the config file.

General Commands

Command|Explanation
---|---
`rclone gendocs`|Output markdown docs for rclone to the directory supplied.
`rclone ncdu`|Explore a remote with a text based user interface.
`rclone mount`|Mount the remote as file system on a mountpoint.
`rclone link`|Generate public link to file/folder.

File Operation

Action|File/Dir|Command
---|---|---
Create|Copy url content to dest.|`rclone copyurl`
Delete trash|Clean up the remote if possible.<br/>Empty the trash or delete old file versions.|`rclone cleanup`
Untrash|Untrash files and directories|`rclone backend untrash`
Delete duplicate|Interactively find duplicate filenames and delete/rename them|`rclone dedupe`
Copy|Single File|`rclone copyto`
Delete|Single File|`rclone deletefile`
Create<br/>Change Modify time|Single File|`rclone touch`
Create|Single Dir|`rclone mkdir`
Delete|Removes empty directory|`rclone rmdir`
Delete|Recursively<br/>removes any empty directories|`rclone rmdirs`
Delete|The path and all of its contents|`rclone purge`
Delete|Selectively<br/>just remove the files in path<br/>leaves the directory structure alone|`rclone delete`
Copy|Copy files to the destination<br/>Don't delete files from the destination|`rclone copy`
Sync|Sync files to match source<br/>including deleting files if necessary|`rclone sync`
Move|Move files from source to dest|`rclone move`
Move|Move file or directory from source to dest|`rclone moveto`


List objects

Command|Explanation
---|---
`rclone ls`|List the objects in the path with size and path.
`rclone lsd`|List all directories/containers/buckets in the path.
`rclone lsf`|List directories and objects in remote:path formatted for parsing.
`rclone lsjson`|List directories and objects in the path in JSON format.
`rclone lsl`|List the objects in path with modification time, size and path.
`rclone sha1sum`|Produces an sha1sum file for all the objects in the path.
`rclone md5sum`|Produces an md5sum file for all the objects in the path.
`rclone hashsum`|Produces a hashsum file for all the objects in the path.
`rclone size`|Prints the total size and number of objects in remote:path.
`rclone tree`|List the contents of the remote in a tree like fashion.
`rclone cat`|Concatenates any files and sends them to stdout.


## Change Log

* Mar 22, 2021 16:45 Mon ET
  * First draft

[rclone]:https://rclone.org "rsync for cloud storage"


<!-- End -->