# Rclone Issues Occuring

## TOC

1. [Issue Occuring](#issue-occuring)  
1.1 [fusermount](#fusermount)  
1.2 [out of memory](#out-of-memory)  
1.3 [crypt error](#crypt-error)  
1.4 [onedrive path exceeds maximum length](#onedrive-path-exceeds-maximum-length)  
1.5 [mount error](#mount-error)  
1.6 [pcloud failed to cleanup](#pcloud-failed-to-cleanup)  
2. [Change Log](#change-log)  


## Issue Occuring

### fusermount

>2021/02/06 09:54:28 Fatal error: failed to mount FUSE fs: fusermount: exec: "fusermount": executable file not found in $PATH

Solution

```sh
# Digital Ocean Debian 10
sudo apt install -y cloudsql-proxy
```

### out of memory

>fatal error: runtime: out of memory
>

Official forum:

* [Sync GDrive to Dropbox > fatal error: runtime: out of memory](https://forum.rclone.org/t/sync-gdrive-to-dropbox-fatal-error-runtime-out-of-memory/6014)
* [How to set a memory limit?](https://forum.rclone.org/t/how-to-set-a-memory-limit/10230)

Solution

Add sarameter

```txt
--use-mmap --buffer-size 0M
```

<details><summary>Click to expand memeory usage info</summary>

Before

```sh
$ free -h
              total        used        free      shared  buff/cache   available
Mem:          2.0Gi       1.8Gi        83Mi       2.0Mi        52Mi       105Mi
Swap:            0B          0B          0B

$ rclone --progress --no-update-modtime --transfers=35 --max-transfer 745G --crypt-server-side-across-configs=true --drive-server-side-across-configs=true sync googledrive:XXXXXXX/ onedrive:XXXXXXXXXXXX
```

After

```sh
$ free -h
              total        used        free      shared  buff/cache   available
Mem:          2.0Gi       936Mi       991Mi       2.0Mi        69Mi       1.1Gi
Swap:            0B          0B          0B

$ rclone --progress --no-update-modtime --use-mmap --buffer-size 0M --transfers=35 --max-transfer 745G --crypt-server-side-across-configs=true --drive-server-side-across-configs=true sync googledrive:XXXXXXX/ onedrive:XXXXXXXXXXXX
Transferred:        2.401G / 456.655 GBytes, 1%, 37.744 MBytes/s, ETA 3h25m23s
Checks:               282 / 282, 100%
Transferred:            0 / 10023, 0%
Elapsed time:       1m7.4s
```

</details>

If it prompts error info like

> ERROR : 504 Gateway Timeout: : upload chunks may be taking too long - try reducing --onedrive-chunk-size or decreasing --transfers

Solution is decrease the number of parameter *--transfers*


### crypt error

>failed to authenticate decrypted block - bad password?

Try to mount then copy

>ReadFileHandle.Read error: low level retry 1/10: failed to authenticate decrypted block - bad password?

GitHub issue:

* [BUG: failed to authenticate decrypted block - bad password?](https://github.com/rclone/rclone/issues/2290)
* [failed to authenticate decrypted block - bad password?](https://github.com/rclone/rclone/issues/677)

Solution

See official doc [Crypt/Backing up a crypted remote](https://rclone.org/crypt/#backing-up-a-crypted-remote)

>If you wish to backup a crypted remote, it is recommended that you use `rclone sync` on the encrypted files, and make sure *the passwords are the same in the new encrypted remote*.
>
>For example, let's say you have your original remote at `remote:` with the encrypted version at `eremote:` with path `remote:crypt`. You would then set up the new remote `remote2:` and then the encrypted version `eremote2:` with path `remote2:crypt` using the same passwords as `eremote:`.

### onedrive path exceeds maximum length

Syncing crypted data to Onedrive, prompt errors

>Failed to sync with 980 errors: last error was: invalidRequest: pathIsTooLong: Path exceeds maximum length

Official forum:

* [Max path length for Onedrive + crypt](https://forum.rclone.org/t/max-path-length-for-onedrive-crypt/17011/2)

Official doc [Microsoft OneDrive # Path Length](https://rclone.org/onedrive/#path-length) says

>The entire path, including the file name, must contain fewer than **400** characters for OneDrive, OneDrive for Business and SharePoint Online. *If you are encrypting file and folder names with rclone, you may want to pay attention to this limitation because the encrypted names are typically longer than the original ones*.

Mircosoft doc [Invalid file names and file types in OneDrive and SharePoint](https://support.microsoft.com/en-us/office/invalid-file-names-and-file-types-in-onedrive-and-sharepoint-64883a5d-228e-48f5-b3d2-eb39e07630fa?ui=en-us&rs=en-us&ad=us#filenamepathlengths) says:

>The entire decoded file path, including the file name, can't contain more than **400** characters for OneDrive, OneDrive for work or school and SharePoint in Microsoft 365.

Extracting affected sub dirs from source remote

```sh
# just extract sub dirs
rclone_log='rclone_2021-02-23-0700.log'
source_remote='googledrive_encrypt:'  # Failed to cryptdecode: The remote needs to be of type "crypt"
output_path='/tmp/error_list'

: > "${output_path}" # empty file
sed -r -n '/Failed/{s@.*?ERROR[[:space:]]*:[[:space:]]*([^:]+).*@\1@g;p}' "${rclone_log}" | sed -r -n 's@^(.*)/[^\/]+$@\1@g;p' | sort | uniq | while read -r line; do
    rclone cryptdecode "${source_remote}" "${line}" | sed -r -n 's@^[^[:space:]]+[[:space:]]*@@g;p' >> "${output_path}"
done
```

Attention:

>Any files you delete with rclone will end up in the trash. Microsoft doesn't provide an API to permanently delete files, nor to empty the trash, so you will have to do that with one of Microsoft's apps or via the OneDrive website. -- [Onedrive # Deleting files](https://rclone.org/onedrive/#deleting-files)


### mount error

Using `rclone mount` with parameter *--daemon* prompts error

>unable to read obscured config key and unable to delete the temp file: open /tmp/rclone241234523: no such file or directory

But if not specify parameter *--daemon*, it works.

>Fatal error: failed to mount FUSE fs: fusermount: exec: "fusermount3": executable file not found in $PATH

Solution is install package `fuse3` (See [issue](https://github.com/rclone/rclone/issues/6844#issuecomment-1595317340))

```sh
sudo apt-get install fuse3
```

### pcloud failed to cleanup

[Rclone][rclone] doc about [pcloud # Deleting files](https://rclone.org/pcloud/#deleting-files) says:

>Deleted files will be moved to the trash. Your subscription level will determine how long items stay in the trash. `rclone cleanup` can be used to empty the trash.

But it will prompts error

```txt
ERROR : Attempt 1/3 failed with 1 errors and: pcloud error: Log in required. (1000)
ERROR : Attempt 2/3 failed with 1 errors and: pcloud error: Log in required. (1000)
ERROR : Attempt 3/3 failed with 1 errors and: pcloud error: Log in required. (1000)
Failed to cleanup: pcloud error: Log in required. (1000)
```

Same issue

* [GitHub -  pcloud cleanup error, log in required](https://github.com/rclone/rclone/issues/3853)


## Change Log

* Mar 22, 2021 17:10 Mon ET
  * First draft

[rclone]:https://rclone.org "rsync for cloud storage"


<!-- End -->