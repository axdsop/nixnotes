# Rclone Installation & Configuration

## TOC

1. [Installation](#installation)  
1.1 [Official Method](#official-method)  
1.2 [Custom Script](#custom-script)  
2. [Configuration](#configuration)  
3. [Encryption](#encryption)  
3.1 [For Remote Storage](#for-remote-storage)  
3.2 [For Configuration](#for-configuration)  
4. [Change Log](#change-log)  


## Installation

### Official Method

Latest relase package lists in page <https://github.com/rclone/rclone/releases/latest>

Official page <https://rclone.org/install/>

```sh
# Official Script
curl https://rclone.org/install.sh | sudo bash

# For Arch Linux
pacman -S --noconfirm rclone
```

Generating a shell completion script for [Rclone][rclone] via command [rclone genautocomplete](https://rclone.org/commands/rclone_genautocomplete/).

```sh
# If output_file is "-", then the output will be written to stdout.

# For Bash shell
[[ -d /usr/share/bash-completion/completions ]] && rclone genautocomplete bash - | sudo tee /usr/share/bash-completion/completions/rclone 1> /dev/null
```

### Custom Script

I write a Shell script ([rclone.sh](../Scripts/rclone.sh)) to install/update Rclone and its bash-completion on GNU/Linux.

<details><summary>Click to expand script usage</summary>

```sh
download_command='wget -qO-'
[[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && download_command='curl -fsSL'

${download_command} https://gitlab.com/axdsop/nixnotes/raw/master/CloudComputing/Tools/Rclone/Scripts/rclone.sh | sudo bash -s --

# self update (attention: may clean directory ~/.config/rclone/)
rclone selfupdate
```

</details>


## Configuration

Running command `rclone config` to enter an interactive configuration session. See [rclone config docs](https://rclone.org/docs/) for more details.

Configuation Path

Item|Value
---|---
Default config|`${XDG_CONFIG_HOME}/rclone/rclone.conf`<br/>`$HOME/.config/rclone/rclone.conf`
Custom config|speciy via `--config` (see `rclone help flags`)

You can print decrypted config file from encrypted file via command

```sh
rclone config show [<remote>] [flags]
```


## Encryption

### For Remote Storage

[Rclone][rclone] crypt remotes encrypt and decrypt other remotes. See [Crypt](https://rclone.org/crypt/) for more details.

<details><summary>Click to expand remote crypt example</summary>

```sh
$ rclone config
Current remotes:

Name                 Type
====                 ====
googledrive          drive

e) Edit existing remote
n) New remote
d) Delete remote
r) Rename remote
c) Copy remote
s) Set configuration password
q) Quit config
e/n/d/r/c/s/q> n
name> googledrive_encrypt
Type of storage to configure.
Enter a string value. Press Enter for the default ("").
Choose a number from below, or type in your own value
...
...
11 / Encrypt/Decrypt a remote
   \ "crypt"
...
Storage>   # here choose 11
** See help for crypt backend at: https://rclone.org/crypt/ **

Remote to encrypt/decrypt.
Normally should contain a ':' and a path, e.g. "myremote:path/to/dir",
"myremote:bucket" or maybe "myremote:" (not recommended).
Enter a string value. Press Enter for the default ("").
remote> googledrive:Encrypt    # here input remote name and path
How to encrypt the filenames.
Enter a string value. Press Enter for the default ("standard").
Choose a number from below, or type in your own value
 1 / Encrypt the filenames see the docs for the details.
   \ "standard"
 2 / Very simple filename obfuscation.
   \ "obfuscate"
 3 / Don\'t encrypt the file names.  Adds a ".bin" extension only.
   \ "off"
filename_encryption> 1    # here choose 1
Option to either encrypt directory names or leave them intact.

NB If filename_encryption is "off" then this option will do nothing.
Enter a boolean value (true or false). Press Enter for the default ("true").
Choose a number from below, or type in your own value
 1 / Encrypt directory names.
   \ "true"
 2 / Don\'t encrypt directory names, leave them intact.
   \ "false"
directory_name_encryption> 1    # here choose 1
Password or pass phrase for encryption.
y) Yes type in my own password
g) Generate random password
y/g> g    # here choose generated random password
Password strength in bits.
64 is just about memorable
128 is secure
1024 is the maximum
Bits> 1024
Your password is: 53NsvJcIblXyQLYNn6WzL0NpEQTM2k2D4LEDiR9ubxLhBSCXwO6ybVnxYItvfTL0x2b5RgfS0RXNeYJ3MjcYPeOQ5ase637Cy1TTQyk7Vnk1yWFNs512dTcWOY78t4CuzE4UwDeGc-lNpN5QfbHrA3UG6uTI57_q2qQMlMPhRZY
Use this password? Please note that an obscured version of this
password (and not the password itself) will be stored under your
configuration file, so keep this generated password in a safe place.
y) Yes (default)
n) No
y/n> y
Password or pass phrase for salt. Optional but recommended.
Should be different to the previous password.
y) Yes type in my own password
g) Generate random password
n) No leave this optional password blank (default)
y/g/n> g
Password strength in bits.
64 is just about memorable
128 is secure
1024 is the maximum
Bits> 1024
Your password is: HEZSdu0svwE96ZlnrSYjE0PAvW1phY_S8m0S0PZAQ6TEC_G_wVZrRit-kNuRoOxRmXHNQo1bNzjUV24cbGopQo44hTqZfdtcewDUMpdL-EfcWYo97ab6runMX_eWZsNbs-AZBPw2QF08q-9RJr_j30XXKgrozC9lc-tuSyweKXo
Use this password? Please note that an obscured version of this
password (and not the password itself) will be stored under your
configuration file, so keep this generated password in a safe place.
y) Yes (default)
n) No
y/n> y
Edit advanced config? (y/n)
y) Yes
n) No (default)
y/n> n
Remote config
--------------------
[googledrive_encrypt]
type = crypt
remote = googledrive:Encrypt
filename_encryption = standard
directory_name_encryption = true
password = *** ENCRYPTED ***
password2 = *** ENCRYPTED ***
--------------------
y) Yes this is OK (default)
e) Edit this remote
d) Delete this remote
y/e/d> y
Current remotes:


Name                 Type
====                 ====
googledrive          drive
googledrive_encrypt  crypt

e) Edit existing remote
n) New remote
d) Delete remote
r) Rename remote
c) Copy remote
s) Set configuration password
q) Quit config
e/n/d/r/c/s/q> q
```

</details>

Parameter *--crypt-server-side-across-configs* allow server-side operations (e.g. copy) to work across different crypt configs.

>Normally this option is not what you want, but if you have two crypts pointing to the same backend you can use it.
>
>This can be used, for example, to change file name encryption type without re-uploading all the data. Just make two crypt backends pointing to two different directories with the single changed parameter and use rclone move to move the files between the crypt remotes.

Just like

```sh
# https://rclone.org/crypt/
# --crypt-server-side-across-configs   Allow server-side operations (e.g. copy) to work across different crypt configs.

rclone --progress --no-update-modtime --transfers=50 --max-transfer 745G --crypt-server-side-across-configs=true --drive-server-side-across-configs=true sync googledrive_encrypt: googledrive_encrypt_new:

# Transferred:      452.117G / 508.819 GBytes, 89%, 1.503 GBytes/s, ETA 37s
# Transferred:         1080 / 11140, 10%
```

### For Configuration

If you wanna encrypt you configuration file, you can add a password via command `rclone config`. See [Configuration Encryption](https://rclone.org/docs/#configuration-encryption) for more details.

<details><summary>Click to expand rclone config example</summary>

```sh
$ rclone config
Current remotes:

Name                 Type
====                 ====
googledrive          drive
googledrive_encrypt  crypt

e) Edit existing remote
n) New remote
d) Delete remote
r) Rename remote
c) Copy remote
s) Set configuration password
q) Quit config
e/n/d/r/c/s/q>  # here choose s
```

</details>

You need to supply the password every time you start rclone by default. But if it is safe in your environment, you can set the environment variable `RCLONE_CONFIG_PASS` or `RCLONE_PASSWORD_COMMAND`to contain your password, in which case it will be used for decrypting the configuration.

Example

```sh
rclone_config_pass='p4j>b@X}RQnajjZI%s!Co@TLoztt)w6b@N!(_#g&T_X%QQ4!KB1T}js&SF)e2%lkF)h&&PwG9DA&%?I#%Gr^fb$L9<v@h&VVe){iQfPu)Ijh'

# Method 1 - RCLONE_CONFIG_PASS
tee -a ~/.bashrc 1> /dev/null << EOF
export RCLONE_CONFIG_PASS="${rclone_config_pass}"
EOF

# Method 2 - RCLONE_PASSWORD_COMMAND
echo "${rclone_config_pass}" > $HOME/.config/rclone/pass
tee -a ~/.bashrc 1> /dev/null << EOF
export RCLONE_PASSWORD_COMMAND="cat \$HOME/.config/rclone/pass"
EOF
```


## Change Log

* Mar 22, 2021 16:58 Mon ET
  * First draft

[rclone]:https://rclone.org "rsync for cloud storage"


<!-- End -->