# Rclone Usage Examples

## TOC

1. [Usage Examples](#usage-examples)  
1.1 [Copy url content to dest](#copy-url-content-to-dest)  
1.2 [Delete duplicate files](#delete-duplicate-files)  
1.3 [Untrash cloud drive trash](#untrash-cloud-drive-trash)  
1.4 [Empty cloud drive trash](#empty-cloud-drive-trash)  
2. [Change Log](#change-log)  


## Usage Examples

### Copy url content to dest

Via command `rclone copyurl`

```sh
$ rclone -P copyurl --auto-filename --no-clobber https://mirrors.xtom.com/centos/8/isos/x86_64/CentOS-8.3.2011-x86_64-dvd1.iso googledrive_encrypt:

Transferred:        5.945G / 8.628 GBytes, 69%, 27.778 MBytes/s, ETA 1m38s
Transferred:            0 / 1, 0%
Elapsed time:      3m40.1s
Transferring:
 *               CentOS-8.3.2011-x86_64-dvd1.iso: 68% /8.628G, 27.995M/s, 1m38s

Transferred:        8.630G / 8.630 GBytes, 100%, 26.183 MBytes/s, ETA 0s
Transferred:            1 / 1, 100%
Elapsed time:      5m38.4s

$ rclone lsjson googledrive_encrypt:CentOS-8.3.2011-x86_64-dvd1.iso
[
{"Path":"CentOS-8.3.2011-x86_64-dvd1.iso","Name":"CentOS-8.3.2011-x86_64-dvd1.iso","Size":9264168960,"MimeType":"application/x-iso9660-image","ModTime":"2020-11-18T21:43:47.000Z","IsDir":false,"ID":"1rkJxSMCr6jQ-SwYRUJzUk8qM86gymim5"}
]
```

### Delete duplicate files

Via command `rclone dedupe`

```sh
# rclone dedupe --dedupe-mode list --by-hash googledrive:PDF/ # lists duplicate dirs and files only and changes nothing.

$ rclone dedupe --dedupe-mode newest --by-hash googledrive:PDF/

<5>NOTICE: a3d7785250115bbdbd55917b3e09e1e0: Found 2 files with duplicate MD5 hashes
<5>NOTICE: a3d7785250115bbdbd55917b3e09e1e0: Deleted 1 extra copies
<5>NOTICE: e01c8a2bac450f2e48ff9fdeb865486f: Found 2 files with duplicate MD5 hashes
<5>NOTICE: e01c8a2bac450f2e48ff9fdeb865486f: Deleted 1 extra copies
...

```

### Untrash cloud drive trash

Untrash files and directories via `rclone backend -i untrash`

```sh
# testing preparation
$ rclone about googledrive_encrypt:
Used:    1.605T
Trashed: 0
Other:   0
$ rclone lsjson googledrive_encrypt:rclone_test_20210219.log
[
{"Path":"rclone_test_20210219.log","Name":"rclone_test_20210219.log","Size":83,"MimeType":"application/octet-stream","ModTime":"2021-02-19T20:37:46.776Z","IsDir":false,"ID":"1keYZyLTAEkbxS7d4_e79XkPIZOAYUnJW"}
]
$ rclone deletefile googledrive_encrypt:rclone_test_20210219.log
$ rclone about googledrive_encrypt:
Used:    1.605T
Trashed: 131
Other:   0
$ rclone lsjson googledrive_encrypt:rclone_test_20210219.log
[
2021/02/19 16:41:17 ERROR : : error listing: directory not found
2021/02/19 16:41:17 Failed to lsjson with 2 errors: last error was: error in ListJSON: directory not found
$

# Untrash via `rclone backend untrash`
$ rclone backend -i untrash googledrive:
rclone: restore "Encryption/pkltitmivmvu3fi2uk1rj07bp8bjnth8aekjn5ag0sjbj5g8uus0"?
y) Yes, this is OK (default)
n) No, skip this
s) Skip all restore operations with no more questions
!) Do all restore operations with no more questions
q) Exit rclone now.
y/n/s/!/q> y

rclone about googledrive_encrypt:
Used:    1.605T
Trashed: 0
Other:   0

rclone lsjson googledrive_encrypt:rclone_test_20210219.log
[
{"Path":"rclone_test_20210219.log","Name":"rclone_test_20210219.log","Size":83,"MimeType":"application/octet-stream","ModTime":"2021-02-19T20:37:46.776Z","IsDir":false,"ID":"1keYZyLTAEkbxS7d4_e79XkPIZOAYUnJW"}
]
```

### Empty cloud drive trash

Via command `rclone cleanup`

```sh
$ rclone about onedrive_celeste:
Total:   1.005T
Used:    48.327G
Free:    980.673G
Trashed: 0

$ rclone about googledrive:
Used:    2.011T
Trashed: 406.896G
Other:   0

$ rclone cleanup googledrive:
<5>NOTICE: Google drive root '': Note that emptying the trash happens in the background and can take some time.

$ rclone about googledrive:
Used:    1.614T
Trashed: 0
Other:   0
```


## Change Log

* Mar 22, 2021 17:05 Mon ET
  * First draft

[rclone]:https://rclone.org "rsync for cloud storage"


<!-- End -->