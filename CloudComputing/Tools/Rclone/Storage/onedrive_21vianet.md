# Microsoft Onedrive By 21Vianet China

[Rclone][rclone] officially supports for Onedrive operated by 21vianet in China mainland since v1.54.0 (Feb 02, 2021) [[commit](https://github.com/rclone/rclone/commit/15da53696e73e603ec60dee95d2364a0d0f105e5)].

Office 365 中国版由[世纪互联][21vianet]负责运营、提供和管理此服务的交付 (Microsoft 本身并不运营此服务), 数据中心位于北京、上海，可以提供增值税发票。

世纪互联地址:

1. <https://portal.partner.microsoftonline.cn/>  (Common)
2. <https://portal.azure.cn/>  (Organization)


## TOC

1. [Official Docs](#official-docs)  
2. [Change Default Storage Space](#change-default-storage-space)  
3. [Rclone Configuration](#rclone-configuration)  
3.1 [Generating Client ID and Key](#generating-client-id-and-key)  
3.1.1 [App registrations](#app-registrations)  
3.1.2 [Certificates & secrets](#certificates-secrets)  
3.1.3 [API permissions](#api-permissions)  
3.2 [Rclone Config](#rclone-config)  
4. [Issue Occuring](#issue-occuring)  
5. [Tutorials](#tutorials)  
6. [Change Log](#change-log)  


## Official Docs

Microsoft

* [API endpoints of Office 365 operated by 21Vianet](*https://docs.microsoft.com/en-us/previous-versions/office/office-365-api/api/o365-china-endpoints)
* [由世纪互联运营的 Office 365](https://docs.microsoft.com/zh-cn/office365/servicedescriptions/office-365-platform-service-description/office-365-operated-by-21vianet)
* [比较面向中小企业的中国版本计划和价格](https://www.microsoft.com/zh-cn/microsoft-365/compare-china-global-versions-microsoft-365)

Rclone

* [Microsoft OneDrive - Getting your own Client ID and Key](https://rclone.org/onedrive/#getting-your-own-client-id-and-key)


## Change Default Storage Space

Microsoft official doc [Set the default storage space for OneDrive users](https://docs.microsoft.com/en-US/onedrive/set-default-storage-space)

>For most subscription plans, the default storage space for each user's OneDrive is 1 TB. Depending on your plan and the number of licensed users, you can increase this storage up to 5 TB. For info, see [the OneDrive service description](https://docs.microsoft.com/en-us/office365/servicedescriptions/onedrive-for-business-service-description).

Check user default storage limit

> If you have Office 365 operated by 21Vianet (China), sign in at https://login.partner.microsoftonline.cn/. Then select the `Admin` tile to open the admin center.

Open page [AdminPortal](https://portal.partner.microsoftonline.cn/AdminPortal/Home), then click *Users* --> *Active users* --> *Onedrive* --> *Storage used* (1TB default).

Click *Edit* button under *Storage used*, show *Edit storage limit* page. Choose *Maximum storage for this user* (set 5120GB).

Check storage size

```sh
# default storage size
$rclone about 21vianet:
Total:   1T
Used:    0
Free:    1023.998G
Trashed: 0

# after change storage size
$rclone about 21vianet:
Total:   5T
Used:    0
Free:    5.000T
Trashed: 0
```

## Rclone Configuration

### Generating Client ID and Key

Open official siete <https://portal.azure.cn>

#### App registrations

Open url [App registrations](https://portal.azure.cn/#blade/Microsoft_AAD_IAM/ActiveDirectoryMenuBlade/RegisteredApps), click *New registrations*

Item|Value
---|---
Name|Rclone
Supported account types|Accounts in any organizational directory (Any Azure AD directory - Multitenant)
Redirect URI (optional)<br/>Web|http://localhost:53682/

Generated App Info

Item|Value
---|---
Display name|Rclone
Application (client) ID|b30a9518-f07f-495f-b3c4-35ed030079d8
Directory (tenant) ID|c97ba8d9-756e-4b5e-969c-df1ee0cdf675
Object ID|46ed92e1-1a55-43cf-8bf8-b2a46b67858b

Client ID is *b30a9518-f07f-495f-b3c4-35ed030079d8*

#### Certificates & secrets

Click *Certificates & secrets* --> *New client secret*

Generated Client Secret Info

Item|Value
---|---
Description|Rclone
Expires(2 year)|3/22/2023
Value|T_r6Ed-223UL9Rz9DPt~3_aXPC1iXSfE0v
ID|ab22b3e3-dc60-4c62-addb-fc5b904b21e5

Client Secret is *T_r6Ed-223UL9Rz9DPt~3_aXPC1iXSfE0v*


#### API permissions

Click *API permissions* --> *Add a permissions* --> Choose `Microsoft Graph` --> *Delegated permissions*.

Search and select the following permissions: *Files.Read*, *Files.ReadWrite*, *Files.Read.All*, *Files.ReadWrite.All*, *offline_access*, *User.Read*.


### Rclone Config

Client screts info

Item|Value
---|---
Client ID|b30a9518-f07f-495f-b3c4-35ed030079d8
Client Secret|T_r6Ed-223UL9Rz9DPt~3_aXPC1iXSfE0v

Setting up new remote via command `rclone config`.

<details><summary>Click to expand config procedure</summary>

```sh
$ rclone config
Current remotes:

Name                 Type
====                 ====

e) Edit existing remote
n) New remote
d) Delete remote
r) Rename remote
c) Copy remote
s) Set configuration password
q) Quit config
e/n/d/r/c/s/q> n
name> 21vianet
Type of storage to configure.
Enter a string value. Press Enter for the default ("").
Choose a number from below, or type in your own value
 1 / 1Fichier
   \ "fichier"
...
...
25 / Microsoft Azure Blob Storage
   \ "azureblob"
26 / Microsoft OneDrive
   \ "onedrive"
...
...
41 / premiumize.me
   \ "premiumizeme"
42 / seafile
   \ "seafile"
Storage> 26
** See help for onedrive backend at: https://rclone.org/onedrive/ **

OAuth Client Id
Leave blank normally.
Enter a string value. Press Enter for the default ("").
client_id> b30a9518-f07f-495f-b3c4-35ed030079d8    # input client id
OAuth Client Secret
Leave blank normally.
Enter a string value. Press Enter for the default ("").
client_secret> T_r6Ed-223UL9Rz9DPt~3_aXPC1iXSfE0v    # input client secret
Choose national cloud region for OneDrive.
Enter a string value. Press Enter for the default ("global").
Choose a number from below, or type in your own value
 1 / Microsoft Cloud Global
   \ "global"
 2 / Microsoft Cloud for US Government
   \ "us"
 3 / Microsoft Cloud Germany
   \ "de"
 4 / Azure and Office 365 operated by 21Vianet in China
   \ "cn"
region> 4
Edit advanced config? (y/n)
y) Yes
n) No (default)
y/n> n
Remote config
Make sure your Redirect URL is set to "http://localhost:53682/" in your custom config.
Use auto config?
 * Say Y if not sure
 * Say N if you are working on a remote or headless machine
y) Yes (default)
n) No
y/n> y
If your browser doesn't open automatically go to the following link: http://127.0.0.1:53682/auth?state=vIvzCAV_7KhMtiYsKFmUzA
Log in and authorize rclone for access
Waiting for code...
Got code
Choose a number from below, or type in an existing value
 1 / OneDrive Personal or Business
   \ "onedrive"
 2 / Root Sharepoint site
   \ "sharepoint"
 3 / Sharepoint site name or URL (e.g. mysite or https://contoso.sharepoint.com/sites/mysite)
   \ "url"
 4 / Search for a Sharepoint site
   \ "search"
 5 / Type in driveID (advanced)
   \ "driveid"
 6 / Type in SiteID (advanced)
   \ "siteid"
 7 / Sharepoint server-relative path (advanced, e.g. /teams/hr)
   \ "path"
Your choice> 1
Found 1 drives, please select the one you want to use:
0: OneDrive (business) id=b!rvcofFnMU0mjmQALCo8o4-6ft2bcnltEpRwDiTKqQClbL9B2GfKdSY9uuyZ9gu_1
Chose drive to use:> 0
Found drive 'root' of type 'business', URL: https://*****-my.sharepoint.cn/personal/admin_*****_partner_onmschina_cn/Documents
Is that okay?
y) Yes (default)
n) No
y/n> y
--------------------
[21vianet]
type = onedrive
client_id = b30a9518-f07f-495f-b3c4-35ed030079d8
client_secret = T_r6Ed-223UL9Rz9DPt~3_aXPC1iXSfE0v
region = cn
token = {"access_token":"eyJ0eXAi**********AHOkSxnpsj3q2nbZ9SCw","token_type":"Bearer","refresh_token":"0.AAAA2ah7yW51XkuWnN8e4M32*****MNouXZqcWmjXfrKzNLCsgAA","expiry":"2021-03-22T10:42:49.143531076-04:00"}
drive_id = b!rvcofFnMU0mjmQALCo8o4-6ft2bcnltEpRwDiTKqQClbL9B2GfKdSY9uuyZ9gu_1
drive_type = business
--------------------
y) Yes this is OK (default)
e) Edit this remote
d) Delete this remote
y/e/d> y
Current remotes:

Name                 Type
====                 ====
21vianet             onedrive

e) Edit existing remote
n) New remote
d) Delete remote
r) Rename remote
c) Copy remote
s) Set configuration password
q) Quit config
e/n/d/r/c/s/q> q

```

</details>


## Issue Occuring

>Failed to create file system for "21vianet:": failed to get root: Get "https://microsoftgraph.chinacloudapi.cn/v1.0/drives/b!rvcofFnMU0mjmQALCo8o4-6ft2bcnltEpRwDiTKqQClbL9B2GfKdSY9uuyZ9gu_1/root": dial tcp: lookup microsoftgraph.chinacloudapi.cn: no such host

Solution is change network IP via VPN.


## Tutorials

* [Azure自建Api链接OneDrive用于搭建OLAINDEX等程序](https://aneeo.com/5742.html)
* [世纪互联 OneDrive使用rclone教程](https://www.liujason.com/article/830.html)


## Change Log

* Mar 22, 2021 10:37 Mon ET
  * First draft

[rclone]:https://rclone.org "rsync for cloud storage"
[21vianet]:https://www.21vianet.com

<!-- End -->