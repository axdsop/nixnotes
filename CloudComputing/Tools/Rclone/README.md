# Rclone Simple Tutorials

[Rclone][rclone] ("rsync for cloud storage") is a command line program to sync files and directories to and from different cloud storage providers. It is a feature rich alternative to cloud vendors' web storage interfaces.


## TOC

1. [Official Docs](#official-docs)  
2. [Notes](#notes)  
2.1 [Stroage Platforms](#stroage-platforms)  
3. [Scripts](#scripts)  
4. [Change Log](#change-log)  


## Official Docs

Item|Value
---|---
Official Site|https://rclone.org
GitHub|https://github.com/rclone/rclone
Official Docs|https://rclone.org/docs/
GitHub Wiki|https://github.com/rclone/rclone/wiki

GitHub Wiki

* [Rclone Optimizations](https://github.com/rclone/rclone/wiki/Rclone-Optimizations)
* [Systemd rclone mount](https://github.com/rclone/rclone/wiki/Systemd-rclone-mount)
* [rclone mount helper script](https://github.com/rclone/rclone/wiki/rclone-mount-helper-script)
* [Third Party Integrations with rclone](https://github.com/rclone/rclone/wiki/Third-Party-Integrations-with-rclone)


## Notes

1. [Simple Intro](./Notes/00_Intro.md)
2. [Install & Config](./Notes/01_Install.md)
3. [Usage Examples](./Notes/02_Usage.md)
4. [Issues Occuring](./Notes/03_Issue.md)

### Stroage Platforms

* [Onedrive 21Vianet](./Storage/onedrive_21vianet.md)


## Scripts

Rclone install script

```sh
# curl https://rclone.org/install.sh | sudo bash

download_command='wget -qO-'
[[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && download_command='curl -fsSL'

# rclone install / update
${download_command} https://gitlab.com/axdsop/nixnotes/raw/master/CloudComputing/Tools/Rclone/Scripts/rclone.sh  | sudo bash -s --


# online drive mount
${download_command} https://gitlab.com/axdsop/nixnotes/raw/master/CloudComputing/Tools/Rclone/Scripts/rclone_mount.sh | bash -s --
```


## Change Log

* Jan 14, 2021 13:00 Thu ET
  * First draft
* Feb 19, 2021 13:35 Fri ET
  * Rewrite
* Feb 19, 2021 17:08 Fri ET
  * Add section issue occuring
* Mar 22, 2021 17:21 Mon ET
  * Seperate README into multiple notes


[rclone]:https://rclone.org "rsync for cloud storage"

<!-- End -->