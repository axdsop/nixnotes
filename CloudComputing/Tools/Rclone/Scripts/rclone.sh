#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails

# Target: Install/Update rclone on GNU/Linux
# Write: MaxdSre
# Change Log:
# - Feb 18, 2024 Sun 09:48 CST - fix version comparsion issue
# - Mar 12, 2021 Fri 13:12 ET - first draft

# - Variable definition
os_type=${os_type:-'linux'}
os_arch=${os_arch:-}
binary_save_path=${binary_save_path:-'/usr/bin'} # default path /usr/bin

# https://downloads.rclone.org/version.txt  -->  rclone v1.54.1

# - Custom function
fn_CommandExistIfCheck(){
    # $? -- 0 is find, 1 is not find
    local l_name="${1:-}"
    local l_output=${l_output:-1}
    [[ -n "${l_name}" && -n $(which "${l_name}" 2> /dev/null || command -v "${l_name}" 2> /dev/null) ]] && l_output=0
    return "${l_output}"
}

fn_ExitStatement(){
    local l_str="$*"
    [[ -n "${l_str}" ]] && echo -e "${l_str}\n"
    exit 0
}


# - Check root or sudo privilege
[[ "$UID" -ne 0 ]] && fn_ExitStatement 'Sorry: this script requires superuser privileges (eg. root, su).'

# - Distro info detection
if [[ -f /etc/os-release && $(uname) == 'Linux' ]]; then
    # uname  FreeBSD --> freebsd, NetBSD --> netbsd, OpenBSD --> openbsd
    machine_name=$(uname -m)
    case "${machine_name}" in
        x86_64|amd64|i?86_64) os_arch='amd64' ;;
        i?86|x86 ) os_arch='386' ;;
        aarch64|arm64 ) os_arch='arm64' ;;
        arm* )
            case "${machine_name}" in
                armv7* ) os_arch='arm-v7' ;;
                * ) os_arch='arm' ;;
            esac
        ;;
        * )
            fn_ExitStatement "Sorry, this script doesn't support you system arch '${machine_name}'."
        ;;
    esac
else
    fn_ExitStatement 'Sorry, this script just supports GNU/Linux.'
fi

# - Tool detection
# Download method detection
download_tool='wget -qO-'
fn_CommandExistIfCheck 'curl' && download_tool='curl -fsSL'

# Unzip tool
unzip_tool=${unzip_tool:-}
fn_CommandExistIfCheck 'unzip'  && unzip_tool='unzip'
if [[ -z "${unzip_tool}" ]] && fn_CommandExistIfCheck '7z'; then unzip_tool='7z'; fi
if [[ -z "${unzip_tool}" ]] && fn_CommandExistIfCheck 'busybox'; then unzip_tool='busybox'; fi

if [[ -z "${unzip_tool}" ]]; then
    echo 'Sorry, fail to find the supported tools (unzip/p7zip/busybox) for extracting zip archives'
fi

# - Check Local Version Info
echo -n 'Local version check ... '
local_existed_version=''
fn_CommandExistIfCheck 'rclone' && local_existed_version=$(rclone version 2> /dev/null | sed -r -n '/rclone/I{s@.*v@@g;p}')

if [[ -n "${local_existed_version}" ]]; then
    echo "${local_existed_version}"
else
    echo 'not exist'
fi

# - Extract Latest Online Release Info
latest_release_info=${latest_release_info:-}
latest_release_info=$(${download_tool} https://api.github.com/repos/rclone/rclone/releases/latest 2> /dev/null | sed -r -n '1,/"assets":/{/"(name|published_at)"/{/rclone/{s@.*v@@g};p}}; /"browser_download_url":.*(sha256|linux-'"${os_arch}"'.*.zip)/I{p}' | sed -r -n 's@^[^:]*:[[:space:]]*@@g;s@,$@@g;s@"@@g;s@T[[:digit:]:]+.*Z?$@@g;p' | sed ':a;N;$!ba;s@\n@|@g')
# 1.54.1|2021-03-08|https://github.com/rclone/rclone/releases/download/v1.54.1/rclone-v1.54.1-linux-amd64.zip|https://github.com/rclone/rclone/releases/download/v1.54.1/SHA256SUMS
[[ -n "${latest_release_info}" ]] || fn_ExitStatement 'Sorry, fail to extract latest online release info.'

latest_release_version=${latest_release_version:-}
latest_release_version=$(cut -d\| -f1 <<< "${latest_release_info}")

# - Version Comparasion
# echo -e "Local existed is the latest version ${latest_release_version} ($(cut -d\| -f2 <<< "${latest_release_info}")).\n"
# [[ "${latest_release_version}" == "${local_existed_version}" ]] && fn_ExitStatement ''

if [[ "${latest_release_version}" == "${local_existed_version}" ]]; then
    echo -e "Local existed is the latest version ${latest_release_version} ($(cut -d\| -f2 <<< "${latest_release_info}")).\n"
    fn_ExitStatement ''
else
    echo -e "Local existed is older than version ${latest_release_version} ($(cut -d\| -f2 <<< "${latest_release_info}")).\n"
fi


# - Download SHA256SUMS
pack_download_link=$(cut -d\| -f3 <<< "${latest_release_info}")
sha256sums_link=$(cut -d\| -f4 <<< "${latest_release_info}")
pack_sha256sums=${pack_sha256sums:-}
pack_sha256sums=$(${download_tool} "${sha256sums_link}" 2> /dev/null | sed -r -n '/'"${pack_download_link##*/}"'/{s@^[[:space:]]*([^[:space:]]+).*@\1@g;p}')
# c52cbf3646a2d15765b87cf05fc3b2bca3b1d2782d4922046c597bd979e42720  rclone-v1.54.1-linux-amd64.zip
[[ -n "${pack_sha256sums}" ]] || fn_ExitStatement 'Sorry, fail to extract package sha256sums.'

# - Download Package
temp_save_dir=$(mktemp -t -d rclone_XXXXXX)
pack_save_path="${temp_save_dir}/${pack_download_link##*/}"
echo "Downloading package (${pack_save_path##*/}) ... "
${download_tool} "${pack_download_link}" > "${pack_save_path}"

# - SHA256SUM Verification
echo -n 'SHA256SUM dgst verifing ... '
pack_sha256sum_local=${pack_sha256sum_local:-}
fn_CommandExistIfCheck 'shasum' && pack_sha256sum_local=$(shasum -a 256 "${pack_save_path}" 2>/dev/null | cut -d' ' -f1)
fn_CommandExistIfCheck 'openssl' && pack_sha256sum_local=$(openssl dgst -sha256 "${pack_save_path}" | cut -d' ' -f2)

if [[ "${pack_sha256sums}" == "${pack_sha256sum_local}" ]]; then
    echo 'approve'
else
    [[ -d "${temp_save_dir}" ]] && rm -rf "${temp_save_dir}"
    echo 'fail'
    fn_ExitStatement 'Please try it again.'
fi

# - Decompress Package
echo "Decompressing package via tool '${unzip_tool}' ..."
case "${unzip_tool}" in
    unzip) unzip -q "${pack_save_path}" -d "${temp_save_dir}" ;;
    7z) 7z x "${pack_save_path}" -o"${temp_save_dir}" 1> /dev/null ;;
    busybox) busybox unzip "${pack_save_path}" -d "${temp_save_dir}" 1> /dev/null ;;
esac
# p7zip 7z  -o{Directory} : set Output directory

# - Move binary file to /usr/bin or /usr/local/bin
echo "Moving new binary file to ${binary_save_path} ..."
find "${temp_save_dir}" -type f -size +1M -iname 'rclone' -exec cp -f {} "${binary_save_path}" \;
chmod 755 "${binary_save_path}/rclone"
chown root:root "${binary_save_path}/rclone"


# - Bash Completions
echo 'Bash completions generating ...'
[[ -d /usr/share/bash-completion/completions ]] && rclone genautocomplete bash - | tee /usr/share/bash-completion/completions/rclone 1> /dev/null

echo -e '\nNew installed version:\n'
rclone --version

# echo -e "\nNow run 'rclone config' for setup. Check https://rclone.org/docs/ for more details.\n\n"
[[ -d "${temp_save_dir}" ]] && rm -rf "${temp_save_dir}"
echo ''

# Script End