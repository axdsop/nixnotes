#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails

# Target: Mount/Unmount rclone@.service on GNU/Linux
# Write: MaxdSre

# Change Log:
# - Oct 25, 2021 Mon 18:01 ET - add local mount point detection for mount/remount
# - Oct 18, 2021 Mon 11:08 ET - add help info, service file action
# - Oct 15, 2021 Fri 09:43 ~ 11:25 ET - first draft

# Official Site
# - https://rclone.org
#- https://github.com/rclone/rclone


#########  0-1. Variables Setting  #########
readonly utility_name='Rclone'  # Rclone ("rsync for cloud storage") is a command line program to sync files and directories to and from different cloud storage providers.

readonly rclone_project_url='https://gitlab.com/axdsop/nixnotes/raw/master/CloudComputing/Tools/Rclone'
readonly rclone_service_url="${rclone_project_url}/Service/rclone@.service"
readonly rclone_script_url="${rclone_project_url}/Scripts/rclone.sh"
readonly rclone_service_path="$HOME/.config/systemd/user/rclone@.service"
rclone_config_path=${rclone_config_path:-"$HOME/.config/rclone/rclone.conf"}

force_update=${force_update:-0}
is_uninstall=${is_uninstall:-0}


#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | bash -s -- [options] ...

Mount / Unmount rclone@.service On GNU/Linux!

File path: ${rclone_service_path}

[available option]
    -h    --help, show help info
    -f    --force, force update existed rclone@.service file
    -u    --uninstall, uninstall existed rclone@.service file
\e[0m"
}

while getopts "hfu" option "$@"; do
    case "$option" in
        f ) force_update=1 ;;
        u ) is_uninstall=1 ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done

# ="$OPTARG"


#########  1-2 Preparation Functions  #########
fn_CommandExistIfCheck(){
    # $? -- 0 is find, 1 is not find
    local l_name="${1:-}"
    local l_output=${l_output:-1}
    [[ -n "${l_name}" && -n $(which "${l_name}" 2> /dev/null || command -v "${l_name}" 2> /dev/null) ]] && l_output=0
    return "${l_output}"
}

fn_ExitStatement(){
    local l_str="$*"
    [[ -n "${l_str}" ]] && echo -e "${l_str}\n"
    exit 0
}

fn_Systemctl_Rclone_Mount_Action(){
    local l_name="${1:-}"
    local l_action="${2:-}"

    if [[ -n "${l_name}" ]]; then
        case "${l_action}" in
            1|start) l_action='start' ;;
            0|stop) l_action='stop' ;;
        esac
        
        systemctl --user "${l_action}" rclone@"${l_name}"
        
        if [[ "${l_action}" == 'start' ]]; then
            # systemctl --user is-active --quiet rclone@"${l_name}" && echo 'ok' || echo 'fail'

            local l_output=''
            systemctl --user is-active --quiet rclone@"${l_name}" && l_output='ok' || l_output='fail'

            echo "${l_output}"

            if [[ "${l_output}" == 'ok' ]]; then
                # detect mount point path
                local l_output=''
                [[ -n "${l_name}" ]] && l_output=$(mount 2> /dev/null | column -t | sed -r -n '/^'"${l_name}"':.*rclone[[:space:]]+/{/'"$USER"'/!d;s@^[^[:space:]]+[[:space:]]+[^[:space:]]+[[:space:]]+([^[:space:]]+).*@\1@g;p;q}')
                [[ -n "${l_output}" ]] && echo "Local mount path is ${l_output}"
            fi
        else
            systemctl --user is-active --quiet rclone@"${l_name}" && echo 'fail' || echo 'ok'
        fi
    fi
}


#########  2-0. Initialization Check  #########
fn_Initialization_Check(){
    # - Distro info detection
    [[ -f /etc/os-release && $(uname) == 'Linux' ]] || fn_ExitStatement 'Sorry, this script just supports GNU/Linux.'

    # - Uninstall action
    if [[ "${is_uninstall}" -eq 1 && -f "${rclone_service_path}" ]]; then
        rm -f "${rclone_service_path}"
        systemctl --user daemon-reload
        fn_ExitStatement "File ${rclone_service_path} deletes successfully."
    fi

    # - Download method detection
    download_tool='wget -qO-'
    fn_CommandExistIfCheck 'curl' && download_tool='curl -fsSL'

    # - Utility rclone dtection
    fn_CommandExistIfCheck 'rclone' || fn_ExitStatement "Sorry, please install utility 'rclone' first. You can install it via command\n\n${download_tool} ${rclone_script_url} | sudo bash -s --\n"

    # - Check if rclone config exists
    [[ -f "${rclone_config_path}" ]] || fn_ExitStatement "Sorry, no config find (default path ${rclone_config_path})."

    # - Check if rclone@.service exists in path ~/.config/systemd/user/
    if [[ ! -f "${rclone_service_path}" || "${force_update}" -eq 1 ]]; then
        mkdir -p "${rclone_service_path%/*}"

        ${download_tool} "${rclone_service_url}" > "${rclone_service_path}"
        
        [[ -n $(sed -r -n '/^RCLONE_ENCRYPT_/{p}' "$HOME/.config/rclone/rclone.conf") ]] || sed -i '/RCLONE_PASSWORD_COMMAND/d' "${rclone_service_path}"
        
        systemctl --user daemon-reload
        [[ "${force_update}" -eq 1 ]] && echo "File ${rclone_service_path} updates successfully."
    fi
}

#########  2-1. Core Processing  #########
fn_Core_Processing(){
    # rclone list remotes
    rclone_listremotes=${rclone_listremotes:-}
    rclone_listremotes=$(rclone listremotes 2> /dev/null | sed 's@:$@@g')

    # check running rclone@ service
    running_service_list=${running_service_list:-}
    running_service_list=$(systemctl list-units --user --type=service --state=running 2> /dev/null | sed -r -n '/^[[:space:]]*rclone@/{s@^[[:space:]]*@@g;s@^([^[:space:]]+).*@\1@g;p}') # rclone@onedrive.service

    # - Choose Mount/Unmount Action
    # mount choose menu
    mount_choose_menu=${mount_choose_menu:-}
    [[ -n "${rclone_listremotes}" ]] && mount_choose_menu='Mount'
    [[ -n "${running_service_list}" ]] && mount_choose_menu="Unmount Remount ${mount_choose_menu}"

    if [[ -n "${mount_choose_menu}" ]]; then
        IFS_BAK=${IFS_BAK:-"$IFS"}  # Backup IFS
        IFS="|" # Setting temporary IFS

        mount_choose_menu_list=${mount_choose_menu_list:-}
        mount_choose_menu_list=$(sed -r -n 's@[[:space:]]*$@@g;s@[[:space:]]+@\n@g;p' <<< "${mount_choose_menu}")

        if [[ $(wc -l<<< "${mount_choose_menu_list}") -eq 1 ]]; then
            choose_action="${mount_choose_menu_list}"
        else
            echo -e "\nRclone Mount Available Action List:"
            PS3="Choose action number(e.g. 1, 2,...):"

            select item in $(sed ':a;N;$!ba;s@\n@|@g' <<< "${mount_choose_menu_list}"); do
                choose_action="${item%% *}"
                [[ -n "${choose_action}" ]] && break
            done < /dev/tty

            IFS=${IFS_BAK}  # Restore IFS
            unset IFS_BAK
            unset PS3
        fi

        echo "Action choose is ${choose_action}."
    else
        fn_ExitStatement "No remote finds from 'rclone listremotes'."
    fi


    IFS_BAK=${IFS_BAK:-"$IFS"}  # Backup IFS
    IFS="|" # Setting temporary IFS

    case "${choose_action,,}" in
        unmount|remount)
            local all_choose_str='all_choose'
            echo -e "\nNote: '${all_choose_str}' means choose all mounted remotes.\nMounted remote list:"
            PS3="Choose list number(e.g. 1, 2,...):"

            select item in $(sed -r -n 's/^[^@]*@//g;p' <<< "${running_service_list}" | sed '$a '"${all_choose_str}"'' | sed ':a;N;$!ba;s@\n@|@g'); do
                choose_remote="${item%% *}"
                [[ -n "${choose_remote}" ]] && break
            done < /dev/tty

            if [[ "${choose_remote}" == "${all_choose_str}" ]]; then
                sed -r -n 's/^[^@]*@//g;p' <<< "${running_service_list}" | while read -r line; do
                    echo -e -n "\nStopping rclone@service '${line}' ... "
                    fn_Systemctl_Rclone_Mount_Action "${line%.*}" 'stop'

                    if [[ "${choose_action,,}" == 'remount' ]]; then
                        sleep 1
                        echo -e -n "Starting rclone@service '${line}' ... "
                        fn_Systemctl_Rclone_Mount_Action "${line%.*}" 'start'
                    fi
                done
            else
                echo -e -n "\nStopping rclone@service '${choose_remote}' ... "
                fn_Systemctl_Rclone_Mount_Action "${choose_remote%.*}" 'stop'

                if [[ "${choose_action,,}" == 'remount' ]]; then
                    sleep 1
                    echo -e -n "Starting rclone@service '${choose_remote}' ... "
                    fn_Systemctl_Rclone_Mount_Action "${choose_remote%.*}" 'start'
                fi
            fi
        ;;
        mount)
            echo -e "\nNote: '(m)' means already mounted.\nAvailable remote list:"
            PS3="Choose list number(e.g. 1, 2,...):"

            if [[ -n "${running_service_list}" ]]; then
                running_service_list=$(sed -r -n 's/^[^@]*@//g;s@^([^\.]+).*@\1@g;p' <<< "${running_service_list}")

                rclone_listremotes_new=${rclone_listremotes_new:-}
                rclone_listremotes_new=$(while read -r line; do
                    if [[ -n $(sed -r -n '/^'"${line}"'$/{p}' <<< "${running_service_list}") ]]; then
                        echo "${line} (m)"
                    else
                        echo "${line}"
                    fi
                done <<< "${rclone_listremotes}")

                rclone_listremotes="${rclone_listremotes_new}"
            fi

            select item in $(sed ':a;N;$!ba;s@\n@|@g'<<< "${rclone_listremotes}"); do
                choose_remote="${item}"
                # choose_remote="${item%% *}"
                [[ -n "${choose_remote}" ]] && break
            done < /dev/tty

            if [[ "${choose_remote}" =~ \(m\)$ ]]; then
                choose_remote="${choose_remote%% *}"
                echo -e "\nAttention: remote '${choose_remote}' already mounted."

            else
                echo -e -n "\nStarting rclone@service '${choose_remote}' ... "
                fn_Systemctl_Rclone_Mount_Action "${choose_remote}" 'start'
            fi
        ;;
    esac

    IFS=${IFS_BAK}  # Restore IFS
    unset IFS_BAK
    unset PS3
}



#########  2-1. Main  #########
fn_Initialization_Check
fn_Core_Processing

# Script End