# AWS CLI Learning Notes

GitHub project url: <https://github.com/aws/aws-cli>


The *AWS Command Line Interface* (AWS CLI) is a unified tool that provides a consistent interface for interacting with all parts of AWS.

Preparation note

1. [Introducation](./Preparation/0_introducation.md)
2. [Installation](./Preparation/1_installation.md)
3. [Configuration](./Preparation/2_configuration.md)

Service

1. [EC2](./Service/EC2)

Script [url](./Script/CloudComputing/AWS/)


## TOC

1. [Official Documentation](#official-documentation)  
1.1 [Shell Script](#shell-script)  
2. [Operation](#operation)  
2.1 [Rotating Access Keys](#rotating-access-keys)  
2.2 [SSH Key Pairs Generation](#ssh-key-pairs-generation)  
2.3 [VPC](#vpc)  
2.3.1 [Generating](#generating)  
2.3.2 [Deleting](#deleting)  
2.4 [Amazon EC2 Instance](#amazon-ec2-instance)  
2.4.1 [Creating](#creating)  
2.4.2 [Deleting](#deleting-1)  
3. [Change Log](#change-log)  


## Official Documentation

[AWS Command Line Interface Documentation](https://docs.aws.amazon.com/cli/#lang/en_us)

1. [User Guide](https://docs.aws.amazon.com/cli/latest/userguide/ "Describes all the AWS CLI concepts and provides instructions on using the various features of the CLI.")
    * [What Is the AWS Command Line Interface?](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-welcome.html)
    * [Configuring the AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html)
    * [Using the AWS Command Line Interface](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-using.html)
    * [Using the AWS CLI to Work with AWS Services](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-services.html)
        * [IAM](https://docs.aws.amazon.com/cli/latest/userguide/cli-services-iam.html)
        * [EC2](https://docs.aws.amazon.com/cli/latest/userguide/cli-services-ec2.html)
        * [S3](https://docs.aws.amazon.com/cli/latest/userguide/cli-services-s3.html)
2. [CLI Reference](https://docs.aws.amazon.com/cli/latest/reference/index.html "Describes the AWS CLI in detail and provides basic syntax, options, and usage examples for each operation.")

### Shell Script

I wrote a shell script to download AWS documentation books (format: `pdf`) from [AWS Documentation](https://docs.aws.amazon.com) page directly.

```bash
# curl -fsL / wget -qO-
# if need help info, specify '-h'
wget -qO- https://gitlab.com/axdsop/nixnotes/raw/master/ShellScripts/Toolkits/GNULinux/gnuLinuxOfficialDocumentationDownload.sh | bash -s -- -d aws
```

[![asciicast](https://asciinema.org/a/189219.svg)](https://asciinema.org/a/189219?autoplay=1)


## Operation

### Rotating Access Keys

Official documentation [Rotating Access Keys](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_access-keys.html#Using_RotateAccessKey).

>As a security best practice, we recommend that you regularly rotate (change) IAM user access keys. If your administrator granted you the necessary permissions, you can rotate your own access keys.

Command | Explanation
---|---
`aws iam create-access-key` | create access key
`aws iam update-access-key` | disable or reenable access key
`aws iam list-access-keys` | list a user's access keys
`aws iam get-access-key-last-used` | determine when an access key was most recently used
`aws iam delete-access-key` | delete access key

Code

```bash
# - Get UserName Via Access Key Stores In .~/aws/credentials
# aws iam list-access-keys --query 'AccessKeyMetadata[?AccessKeyId==`'"$(sed -r -n '/aws_access_key_id/{s@^[^=]+=[[:space:]]*(.*)$@\1@g;p}' ~/.aws/credentials)"'`].[UserName]' --output text
existed_access_key_id=$(sed -r -n '/aws_access_key_id/{s@^[^=]+=[[:space:]]*(.*)$@\1@g;p}' $HOME/.aws/credentials)

default_user_name=$(aws iam list-access-keys --query 'AccessKeyMetadata[?AccessKeyId==`'"${existed_access_key_id}"'`].[UserName]' --output text)

aws iam list-access-keys --user-name "${default_user_name}"
# An error occurred (LimitExceeded) when calling the CreateAccessKey operation: Cannot exceed quota for AccessKeysPerUser: 2

# aws iam get-access-key-last-used --access-key-id AKIAJD2AGYCJORHG3U7A

# - Create Newer Access Key
new_access_key_info=$(aws iam create-access-key --user-name "${default_user_name}" --output text | awk 'BEGIN{OFS=" "}{$1=$1;print $2,$4}')
# AccessKeyId / SecretAccessKey
# AKIAIYEZO3TFND2NJBTA XGp8HnJ0zSj0ieGdnf5C/aVSd+5Z3p883Eh2AH7w

new_access_key_id="${new_access_key_info%% *}"
new_secret_access_key="${new_access_key_info##* }"

# aws iam list-access-keys --user-name "${default_user_name}" --query "reverse(sort_by(AccessKeyMetadata,&CreateDate))[:1].[AccessKeyId]" --output text

# - Replace ~/.aws/credentials
aws configure set aws_access_key_id "${new_access_key_id}"
aws configure set aws_secret_access_key "${new_secret_access_key}"
# aws configure set default.region us-east-2
aws configure list

# - Change Old Access Key Status Active/Inactive , then delete older
check_session_refresh(){
    sleep 2
    aws iam list-access-keys --user-name "${default_user_name}" &> /dev/null
    [[ $? -ne 0 ]] && check_session_refresh
}

sleep 3  # An error occurred (InvalidClientTokenId) when calling the ListAccessKeys operation: The security token included in the request is invalid.
check_session_refresh

aws iam list-access-keys --user-name "${default_user_name}" --query 'AccessKeyMetadata[?AccessKeyId!=`'"${new_access_key_id}"'`].[AccessKeyId]' --output text | while read -r line; do
    aws iam update-access-key --user-name "${default_user_name}" --access-key-id "${line}" --status Inactive
    aws iam delete-access-key --user-name "${default_user_name}" --access-key-id "${line}"
done

# - List current user
aws iam list-access-keys --user-name "${default_user_name}"

# aws iam get-access-key-last-used --access-key-id "${new_access_key_id}"
```

### SSH Key Pairs Generation

Official documentation

* [Amazon EC2 Key Pairs](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-key-pairs.html)
* [Using Key Pairs](https://docs.aws.amazon.com/cli/latest/userguide/cli-ec2-keypairs.html)

>Amazon EC2 uses public–key cryptography to encrypt and decrypt login information. Public–key cryptography uses a public key to encrypt a piece of data, such as a password, then the recipient uses the private key to decrypt the data. The public and private keys are known as a *key pair*.
>
>To log in to your instance, you must create a key pair, specify the name of the key pair when you launch the instance, and provide the private key when you connect to the instance. On a Linux instance, the public key content is placed in an entry within ~/.ssh/authorized_keys. This is done at boot time and enables you to securely access your instance using the private key instead of a password.

Command | Explanation
---|---
`aws ec2 create-key-pair` | create a key pair
`aws ec2 import-key-pair` | import a key pair
`aws ec2 describe-key-pairs` | describe key pairs
`aws ec2 delete-key-pair` | delete a key pair


[Verifying Your Key Pair's Fingerprint](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-key-pairs.html#verify-key-pair-fingerprints)

>On the **Key Pairs** page in the Amazon EC2 console, the Fingerprint column displays the fingerprints generated from your key pairs. AWS calculates the fingerprint differently depending on whether the key pair was generated by AWS or a third-party tool.

* If you created the key pair using AWS, the fingerprint is calculated using an **SHA-1** hash function.
* If you created the key pair with a third-party tool and uploaded the public key to AWS, or if you generated a new public key from an existing AWS-created private key and uploaded it to AWS, the fingerprint is calculated using an **MD5** hash function.

```bash
# Via aws-cli create-key-pair
openssl pkcs8 -in $path_to_private_key -inform PEM -outform DER -topk8 -nocrypt | openssl sha1 -c

# Via aws-cli import-key-pair
openssl rsa -in $path_to_private_key -pubout -outform DER | openssl md5 -c
```

Code

```bash
# generate new key pair (aws doesn't support ed25519, so use rsa)
key_pair_generation(){
    local k_name="${1:-'SSH Key'}"
    local k_type="${2:-'rsa'}"
    local k_path="${3:-'~/.ssh/'}"

    case "${k_type,,}" in
        ed25519 ) k_type='ed25519' ;;
        * ) k_type='rsa' ;;
    esac

    if [[ "${k_path}" =~ ^~ ]]; then
        [[ -n "${USER:-}" && -z "${SUDO_USER:-}" ]] && login_user="$USER" || login_user="$SUDO_USER"
        [[ "${login_user}" == 'root' ]] && login_user_home='/root' || login_user_home="/home/${login_user}"
        k_path="${login_user_home}/${k_path#*/}"
    fi

    [[ -d "${k_path}" ]] || mkdir -p "${k_path}"

    local private_key_path
    private_key_path="${k_path%/*}/${k_name// /-}"
    local public_key_path
    public_key_path="${private_key_path}.pub"

    case "${k_type,,}" in
        rsa )
            echo -e 'y\n' | ssh-keygen -q -m PEM -t rsa -b 4096 -f "${private_key_path}" -N '' -C "${k_name}" &> /dev/null
            ;;
        ed25519 )
            echo -e 'y\n' | ssh-keygen -q -m PEM -t ed25519 -f "${private_key_path}" -N '' -C "${k_name}" &> /dev/null
            ;;
    esac

    [[ -f "${private_key_path}" ]] && chmod 600 "${private_key_path}"
    [[ -f "${public_key_path}" ]] && chmod 640 "${public_key_path}"
    echo "${public_key_path}"
}

keypair_name=${keypair_name:-'just_ec2'}
keypair_path=${keypair_path:-"$HOME/.aws/"}
# keypair_path=$(dirname $(readlink -nf "$0"))

# - generate new key pair
pub_key_pair_path=$(key_pair_generation "${keypair_name}" 'rsa' "${keypair_path}")

# aws ec2 describe-key-pairs --output text --query 'KeyPairs[*].[KeyName]'

# - delete existed key pair with the same name
aws ec2 delete-key-pair --key-name "${keypair_name}"

# - import newly generated key pair
aws ec2 import-key-pair --key-name "${keypair_name}" --public-key-material file://"${pub_key_pair_path}"

openssl rsa -in "${pub_key_pair_path%.*}" -pubout -outform DER | openssl md5 -c

aws ec2 describe-key-pairs
# aws ec2 describe-key-pairs --output json
```

### VPC

[Amazon Virtual Private Cloud Documentation](https://docs.aws.amazon.com/vpc/#lang/en_us)

* [User Guide](https://docs.aws.amazon.com/vpc/latest/userguide)
* [Peering Guide](https://docs.aws.amazon.com/vpc/latest/peering)
* [Network Administrator Guide](https://docs.aws.amazon.com/vpc/latest/adminguide)
* [Transit Gateways Guide](https://docs.aws.amazon.com/vpc/latest/tgw)
* [API Reference](https://docs.aws.amazon.com/AWSEC2/latest/APIReference)
* [EC2 section of the AWS CLI Reference](https://docs.aws.amazon.com/cli/latest/reference/ec2/index.html)

[Example: Create an IPv4 VPC and Subnets Using the AWS CLI](https://docs.aws.amazon.com/vpc/latest/userguide/vpc-subnets-commands-example.html)

#### Generating

```bash
common_name=${common_name:-'just_ec2'}
vpc_name=${vpc_name:-"${common_name}"}
subnet_name=${subnet_name:-"${common_name}"}
internet_gateway_name=${internet_gateway_name:-"${common_name}"}
route_table_name=${route_table_name:-"${common_name}"}
sec_group_name=${sec_group_name:-"${common_name}"}

# - Create a VPC
vpc_id_new=$(aws ec2 create-vpc --cidr-block 192.168.0.0/16 --no-amazon-provided-ipv6-cidr-block --instance-tenancy default --output text | sed -r -n '/^VPC/{s@.*?(vpc-[^[:space:]]+).*$@\1@g;p}')

aws ec2 create-tags --resources "${vpc_id_new}" --tags 'Key=Name,Value='"${vpc_name}"''

# Default enableDnsHostnames is false, enableDnsSupport is true
# --no-enable-dns-hostnames, --no-enable-dns-support
aws ec2 modify-vpc-attribute --vpc-id "${vpc_id_new}" --enable-dns-hostnames
aws ec2 modify-vpc-attribute --vpc-id "${vpc_id_new}" --enable-dns-support
# aws ec2 describe-vpc-attribute --vpc-id "${vpc_id_new}" --attribute enableDnsHostnames
# aws ec2 describe-vpc-attribute --vpc-id "${vpc_id_new}" --attribute enableDnsSupport

# aws ec2 describe-vpcs --filters "Name=tag:Name,Values=${vpc_name}"

# - Create Subnet (need VPC)
default_region=$(sed -r -n '/region/{s@^[^=]+=[[:space:]]*([^[:space:]]*)$@\1@g;p}' $HOME/.aws/config)
region_zone_id=$(aws ec2 describe-availability-zones --filter "Name=region-name,Values=${default_region}" --query 'AvailabilityZones[*].ZoneId' --output text | sed -r 's@[[:blank:]]+@\n@g' | sort --random-sort | head -n 1)

subnet_id_new=$(aws ec2 create-subnet --availability-zone-id "${region_zone_id}" --vpc-id "${vpc_id_new}" --cidr-block 192.168.1.0/24 --output text | sed -r -n '/subnet-/{s@.*(subnet-[^[:space:]]+).*$@\1@g;p}')

aws ec2 create-tags --resources "${subnet_id_new}" --tags 'Key=Name,Value='"${subnet_name}"''

# automatically receives a public IP address
# MapPublicIpOnLaunch True/False
# aws ec2 modify-subnet-attribute --subnet-id "${subnet_id_new}" --map-public-ip-on-launch
aws ec2 modify-subnet-attribute --subnet-id "${subnet_id_new}" --no-map-public-ip-on-launch

# aws ec2 describe-subnets --filters "Name=vpc-id,Values=${vpc_id_new}"

# The Following Operations Make Subnet Public
# - Create internet gateway (Need VPC)
internet_gateway_id_new=$(aws ec2 create-internet-gateway --output text | sed -r -n '/igw-/{s@.*(igw-[^[:space:]]+).*$@\1@g;p}')

aws ec2 create-tags --resources "${internet_gateway_id_new}" --tags 'Key=Name,Value='"${internet_gateway_name}"''

aws ec2 attach-internet-gateway --vpc-id ${vpc_id_new} --internet-gateway-id "${internet_gateway_id_new}"
# aws ec2 describe-internet-gateways --internet-gateway-ids "${internet_gateway_id_new}"

# - Create Route Table (Need VPC, Internet gateway)
route_table_id_new=$(aws ec2 create-route-table --vpc-id "${vpc_id_new}" --output text | sed -r -n '/rtb-/{s@.*(rtb-[^[:space:]]+).*$@\1@g;p}')

aws ec2 create-tags --resources "${route_table_id_new}" --tags 'Key=Name,Value='"${route_table_name}"''

aws ec2 create-route --route-table-id "${route_table_id_new}" --destination-cidr-block 0.0.0.0/0 --gateway-id "${internet_gateway_id_new}"
# aws ec2 describe-route-tables --route-table-id "${route_table_id_new}"

# choose which subnet to associate with the custom route table
aws ec2 associate-route-table  --subnet-id "${subnet_id_new}" --route-table-id ${route_table_id_new}

# - Security Groups
sec_group_ip_new=$(aws ec2 create-security-group --group-name "${sec_group_name}" --vpc-id "${vpc_id_new}" --description "Allow SSH Inbound Connection." --output text)

aws ec2 create-tags --resources "${sec_group_ip_new}" --tags 'Key=Name,Value='"${sec_group_name}"''

# - authorize security group
login_ip=$(wget -qO- ipinfo.io/ip 2> /dev/null || curl -fsL ipinfo.io/ip 2> /dev/null)

aws ec2 authorize-security-group-ingress --group-id "${sec_group_ip_new}" --ip-permissions IpProtocol=tcp,FromPort=22,ToPort=22,IpRanges='[{CidrIp='"${login_ip}"'/32,Description="SSH for living room"}]'
# aws ec2 authorize-security-group-ingress --group-id sg-903004f8 --protocol tcp --port 22 --cidr "${login_ip}"/32
```

#### Deleting

VPC delete order

1. `aws ec2 delete-security-group`
2. `aws ec2 delete-subnet`
3. `aws ec2 delete-route-table`
4. `aws ec2 detach-internet-gateway`
5. `aws ec2 delete-vpc`

Code

```bash
vpc_name=${vpc_name:-'just_ec2'}

vpc_id=$(aws ec2 describe-vpcs --filters "Name=tag:Name,Values=${vpc_name}" --query 'Vpcs[].VpcId' --output text)

# - security-group
# sec_group_id_default=$(aws ec2 describe-security-groups --filters "Name=vpc-id,Values=${vpc_id}" "Name=group-name,Values!=default" --query 'SecurityGroups[*].[GroupId]' --output text)
# An error occurred (CannotDelete) when calling the DeleteSecurityGroup operation: the specified group: "sg-0deaa9cd9330d159b" name: "default" cannot be deleted by a user

aws ec2 describe-security-groups --filters "Name=vpc-id,Values=${vpc_id}" --query 'SecurityGroups[?GroupName!=`default`].[GroupId]' --output text | while read -r line; do
    aws ec2 delete-security-group --group-id "${line}"
done

# - subnet
# subnet_id_list=$(aws ec2 describe-subnets --filters "Name=vpc-id,Values=${vpc_id}" --query 'Subnets[*].SubnetId' --output text | sed -r 's@[[:blank:]]+@\n@g')
aws ec2 describe-subnets --filters "Name=vpc-id,Values=${vpc_id}" --query 'Subnets[*].SubnetId' --output text | sed -r 's@[[:blank:]]+@\n@g' | while read -r line; do
    aws ec2 delete-subnet --subnet-id "${line}"
done

# - route-table
# route_table_id_main=$(aws ec2 describe-route-tables --filters "Name=vpc-id,Values=${vpc_id}" "Name=association.main,Values=true" --query 'RouteTables[*].RouteTableId' --output text)
# An error occurred (DependencyViolation) when calling the DeleteRouteTable operation: The routeTable 'rtb-066d7c8bbfc852e42' has dependencies and cannot be deleted.

aws ec2 describe-route-tables --filters "Name=vpc-id,Values=${vpc_id}" --query 'RouteTables[?Associations[0].Main!=`true`].[RouteTableId]' --output text | while read -r line; do
    aws ec2 delete-route-table --route-table-id "${line}"
done

# - internet-gateway
internet_gateway_id=$(aws ec2 describe-internet-gateways --filters "Name=attachment.vpc-id,Values=${vpc_id}" --query 'InternetGateways[*].InternetGatewayId' --output text)
aws ec2 detach-internet-gateway --internet-gateway-id "${internet_gateway_id}" --vpc-id "${vpc_id}"
aws ec2 delete-internet-gateway --internet-gateway-id "${internet_gateway_id}"

# - vpc
aws ec2 delete-vpc --vpc-id "${vpc_id}"
```

### Amazon EC2 Instance

* [Deploying a Development Environment in Amazon EC2 Using the AWS Command Line Interface](https://docs.aws.amazon.com/cli/latest/userguide/tutorial-ec2-ubuntu.html)
  * [Using Security Groups](https://docs.aws.amazon.com/cli/latest/userguide/cli-ec2-sg.html)
  * [Using Amazon EC2 Instances](https://docs.aws.amazon.com/cli/latest/userguide/cli-ec2-launch.html)
* [AWS Identity and Access Management from the AWS Command Line Interface](https://docs.aws.amazon.com/cli/latest/userguide/cli-iam.html)
* [Using Amazon EC2 through the AWS Command Line Interface](https://docs.aws.amazon.com/cli/latest/userguide/cli-using-ec2.html)
  * [Using Key Pairs](https://docs.aws.amazon.com/cli/latest/userguide/cli-ec2-keypairs.html)
  * [Using Security Groups](https://docs.aws.amazon.com/cli/latest/userguide/cli-ec2-sg.html)
  * [Using Amazon EC2 Instances](https://docs.aws.amazon.com/cli/latest/userguide/cli-ec2-launch.html)


#### Creating

```bash
common_name=${common_name:-'just_ec2'}
subnet_name=${subnet_name:-"${common_name}"}
sec_group_name=${sec_group_name:-"${common_name}"}

sec_group_id=$(aws ec2 describe-security-groups --filters "Name=group-name,Values=${sec_group_name}" --query 'SecurityGroups[*].[GroupId]' --output text)
# aws ec2 describe-security-groups --query 'SecurityGroups[?GroupName==`'"${sec_group_name}"'`].[GroupId]' --output text

subnet_id=$(aws ec2 describe-subnets --filters "Name=tag:Name,Values=${subnet_name}" --query 'Subnets[].SubnetId' --output text)
# aws ec2 describe-subnets --query 'Subnets[?Tags[0].Value==`'"${${subnet_name}}"'`].[SubnetId]' --output text

instance_name=${instance_name:-'ec2_free_tier'}
keypair_name=${keypair_name:-'just_ec2'}

# For SUSE, RHEL, Ubuntu
device_path=${device_path:-'/dev/sda1'}
ami_id=${ami_id:-'ami-0c55b159cbfafe1f0'} # Ubuntu Server 18.04 LTS

# For Amazon AMI (amzn)
# ami_id=${ami_id:-'ami-02bcbb802e03574ba'}
# device_path='/dev/xvda'

# device_path=$(aws ec2 describe-images --filter "Name=image-id,Values=${ami_id}" --query 'Images[*].BlockDeviceMappings[0].DeviceName' --output text)

available_zone=$(aws ec2 describe-subnets --filters "Name=subnet-id,Values=${subnet_id}" --query 'Subnets[].AvailabilityZone' --output text)

instance_id_new=$(aws ec2 run-instances --image-id "${ami_id}" --count 1 --instance-type t2.micro --placement AvailabilityZone="${available_zone}" --block-device-mappings DeviceName="${device_path}",Ebs={VolumeSize=30}  --key-name "${keypair_name}" --security-group-ids "${sec_group_id}" --subnet-id "${subnet_id}" --associate-public-ip-address --output text | sed -r -n '/^INSTANCES/{s@.*(i-[^[:space:]]+).*$@\1@g;p}')

aws ec2 create-tags --resources "${instance_id_new}" --tags 'Key=Name,Value='"${instance_name}"''


# The valid values are: 0 (pending), 16 (running), 32 (shutting-down), 48 (terminated), 64 (stopping), and 80 (stopped).
# The state of the instance (pending | running | shutting-down | terminated | stopping | stopped ).

# PublicDnsName
aws ec2 describe-instances --query 'Reservations[].Instances[].[PublicDnsName]' --filter "Name=instance-id,Values=${instance_id_new}" --output text
# InstanceId
aws ec2 describe-instances --query 'Reservations[].Instances[].[InstanceId]' --filters "Name=instance-state-name,Values=running" --output text
# instance status
aws ec2 describe-instances --query 'Reservations[].Instances[].[State.Name]' --filter "Name=instance-id,Values=${instance_id_new}" --output text
```

#### Deleting

```bash
instance_name=${instance_name:-'ec2_free_tier'}

instance_id=$(aws ec2 describe-instances --query 'Reservations[].Instances[].[InstanceId]' --filters "Name=instance-state-name,Values=running" "Name=tag:Name,Values=${instance_name}" --output text)

aws ec2 terminate-instances --instance-ids "${instance_id}"
aws ec2 delete-tags --resources "${instance_id}" --tags Key=Name

aws ec2 describe-instances --filter "Name=instance-id,Values=${instance_id}" --query 'Reservations[].Instances[].[State.Name]' --output text


# - Modidy instance run status
aws ec2 start-instances --instance-ids "${instance_id}"
aws ec2 stop-instances --instance-ids "${instance_id}"
# still show running status
aws ec2 reboot-instances --instance-ids "${instance_id}"
```


## Change Log

* Dec 18, 2018 17:05 Tue ET
  * First draft
* Apr 24, 2019 09:52 Wed ET
  * Solve problem [ssh-keygen does not create RSA private key](https://serverfault.com/questions/939909/ssh-keygen-does-not-create-rsa-private-key#941893)
* Jul 25, 2019 11:01 Thu ET
  * Transfer project to [nixNote](https://gitlab.com/axdsop/nixnotes) (CloudComputing -> AWS -> CLI-Learning)

<!-- End -->
