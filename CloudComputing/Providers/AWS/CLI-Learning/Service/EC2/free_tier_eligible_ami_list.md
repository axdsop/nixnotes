# AWS Free Tier Eligible AMI List

Official page [AWS Free Tier](https://aws.amazon.com/free/ 'Gain free, hands-on experience with the AWS platform, products, and services').

> An AMI is a template that contains the software configuration (operating system, application server, and applications) required to launch your instance.

* Date: Oct 16, 2019 Wed ET
* Data source: `AWS Management Console` --> `Services` --> `EC2` (Compute) --> `Create Instance` (Launch Instance) --> `Choose an Amazon Machine Image (AMI)`


## Latest AMI Id List

Distro | Short Name | Arch (x86) | ImageId
---|---|---|---
Red Hat Enterprise Linux 8 | rhel 8 | x86_64 | ami-0520e698dd500b1d1
SUSE Linux Enterprise Server 15 | sles 15 | x86_64 | ami-052a6e77572eba9a9
Ubuntu Server 18.04 LTS | ubuntu 18.04 | x86_64 | ami-0d5d9d301c853a04a
Ubuntu Server 16.04 LTS | ubuntu 16.04 | x86_64 | ami-0d03add87774b12c5
Amazon Linux 2 AMI | amzn2 | x86_64 | ami-00c03f7f7f2ec15c3
Amazon Linux AMI 2018.03.0 | amzn | x86_64 | ami-0c64dd618a49aeee8


## Old AMI Id

### 2019-10-08

Date: Oct 08, 2019 Tue ET

Distro | Short Name | Arch (x86) | ImageId
---|---|---|---
Red Hat Enterprise Linux 8 | rhel 8 | x86_64 | ami-0520e698dd500b1d1
SUSE Linux Enterprise Server 15 | sles 15 | x86_64 | ami-0e0bae59dc35fe89a
Ubuntu Server 18.04 LTS | ubuntu 18.04 | x86_64 | ami-052a6e77572eba9a9
Ubuntu Server 16.04 LTS | ubuntu 16.04 | x86_64 | ami-0d03add87774b12c5
Amazon Linux 2 AMI | amzn2 | x86_64 | ami-00c03f7f7f2ec15c3
Amazon Linux AMI 2018.03.0 | amzn | x86_64 | ami-0c64dd618a49aeee8


### 2019-09-22

Date: Sep 22, 2019 Sun ET

Distro | Short Name | Arch (x86) | ImageId
---|---|---|---
Red Hat Enterprise Linux 8 | rhel 8 | x86_64 | ami-0520e698dd500b1d1
SUSE Linux Enterprise Server 15 | sles 15 | x86_64 | ami-0e0bae59dc35fe89a
Ubuntu Server 18.04 LTS | ubuntu 18.04 | x86_64 | ami-05c1fa8df71875112
Ubuntu Server 16.04 LTS | ubuntu 16.04 | x86_64 | ami-0f93b5fd8f220e428
Amazon Linux 2 AMI | amzn2 | x86_64 | ami-00c03f7f7f2ec15c3
Amazon Linux AMI 2018.03.0 | amzn | x86_64 | ami-0c64dd618a49aeee8


### 2019-08-05

Date: Aug 05, 2019 Mon ET

Distro | Short Name | Arch (x86) | ImageId
---|---|---|---
Red Hat Enterprise Linux 8 | rhel 8 | x86_64 | ami-0520e698dd500b1d1
SUSE Linux Enterprise Server 15 | sles 15 | x86_64 | ami-0e0bae59dc35fe89a
Ubuntu Server 18.04 LTS | ubuntu 18.04 | x86_64 | ami-05c1fa8df71875112
Ubuntu Server 16.04 LTS | ubuntu 16.04 | x86_64 | ami-0f93b5fd8f220e428
Amazon Linux 2 AMI | amzn2 | x86_64 | ami-0d8f6eb4f641ef691
Amazon Linux AMI 2018.03.0 | amzn | x86_64 | ami-02f706d959cedf892


### 2019-06-25

Date: Jun 25, 2019 Tue ET

Distro | Short Name | Arch (x86) | ImageId
---|---|---|---
Red Hat Enterprise Linux 8 | rhel 8 | x86_64 | ami-05220ffa0e7fce3d1
SUSE Linux Enterprise Server 15 | sles 15 | x86_64 | ami-0eb9f58db22854f8f
Ubuntu Server 18.04 LTS | ubuntu 18.04 | x86_64 | ami-0c55b159cbfafe1f0
Ubuntu Server 16.04 LTS | ubuntu 16.04 | x86_64 | ami-0653e888ec96eab9b
Amazon Linux 2 AMI | amzn2 | x86_64 | ami-00c79db59589996b9
Amazon Linux AMI 2018.03.0 | amzn | x86_64 | ami-02f706d959cedf892


### 2019-06-03

Date: Jun 03, 2019 Mon ET

Distro | Short Name | Arch (x86) | ImageId
---|---|---|---
Red Hat Enterprise Linux 8 | rhel 8 | x86_64 | ami-05220ffa0e7fce3d1
SUSE Linux Enterprise Server 15 | sles 15 | x86_64 | ami-0eb9f58db22854f8f
Ubuntu Server 18.04 LTS | ubuntu 18.04 | x86_64 | ami-0c55b159cbfafe1f0
Ubuntu Server 16.04 LTS | ubuntu 16.04 | x86_64 | ami-0653e888ec96eab9b
Amazon Linux 2 AMI | amzn2 | x86_64 | ami-0ebbf2179e615c338
Amazon Linux AMI 2018.03.0 | amzn | x86_64 | ami-04768381bf606e2b3

### 2019-05-07

Date: May 07, 2019 Tue ET

Distro | Short Name | Arch (x86) | ImageId
---|---|---|---
Red Hat Enterprise Linux 7.6 | rhel 7 | x86_64 | ami-0b500ef59d8335eee
SUSE Linux Enterprise Server 15 | sles 15 | x86_64 | ami-0eb9f58db22854f8f
Ubuntu Server 18.04 LTS | ubuntu 18.04 | x86_64 | ami-0c55b159cbfafe1f0
Ubuntu Server 16.04 LTS | ubuntu 16.04 | x86_64 | ami-0653e888ec96eab9b
Amazon Linux 2 AMI | amzn2 | x86_64 | ami-02bcbb802e03574ba
Amazon Linux AMI 2018.03.0 | amzn | x86_64 | ami-0cd3dfa4e37921605


<!-- End -->
