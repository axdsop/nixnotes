# Amazon Machine Image (AMI) Choose


## TOC

1. [Linux Distribution](#linux-distribution)  
1.1 [AMZN](#amzn)  
1.2 [Ubuntu](#ubuntu)  
1.3 [RHEL](#rhel)  
1.4 [SUSE](#suse)  


## Linux Distribution

### AMZN

amzn OwnerId is `137112412989`

```bash
# list all images with ownerid=137112412989
aws ec2 describe-images --filter "Name=owner-id,Values=137112412989" "Name=image-type,Values=machine" "Name=architecture,Values=x86_64" "Name=state,Values=available" "Name=is-public,Values=true"

# list all images with format table   VolumeType gp2/standard
aws ec2 describe-images --filter "Name=owner-id,Values=137112412989" "Name=image-type,Values=machine" "Name=architecture,Values=x86_64" "Name=state,Values=available" "Name=is-public,Values=true" --query 'Images[*].[Name,ImageId,CreationDate,BlockDeviceMappings[0].Ebs.SnapshotId]' --output table

aws ec2 describe-images --filter "Name=owner-id,Values=137112412989" "Name=image-type,Values=machine" "Name=architecture,Values=x86_64" "Name=state,Values=available" "Name=is-public,Values=true" --query 'Images[?Name!=`null`]|[?starts_with(Name,`amzn2-ami-hvm`)==`true`]|[?contains(BlockDeviceMappings[0].Ebs.VolumeType,`gp2`)==`true`]|[*].[Name,ImageId,CreationDate,BlockDeviceMappings[0].Ebs.SnapshotId]' --output table

# just list name begin with `amzn2-ami-hvm`
aws ec2 describe-images --filter "Name=owner-id,Values=137112412989" "Name=image-type,Values=machine" "Name=architecture,Values=x86_64" "Name=state,Values=available" "Name=is-public,Values=true" --query 'Images[?Name!=`null`]|[?starts_with(Name,`amzn2-ami-hvm`)==`true`]|[?contains(BlockDeviceMappings[0].Ebs.VolumeType,`gp2`)==`true`]|[*].[Name,ImageId,CreationDate,BlockDeviceMappings[0].Ebs.SnapshotId]' --output table

# just list latest release version via sort
aws ec2 describe-images --filter "Name=owner-id,Values=137112412989" "Name=image-type,Values=machine" "Name=architecture,Values=x86_64" "Name=state,Values=available" "Name=is-public,Values=true" --query 'Images[?Name!=`null`]|[?starts_with(Name,`amzn2-ami-hvm`)==`true`]|[?contains(BlockDeviceMappings[0].Ebs.VolumeType,`gp2`)==`true`]|[*].[Name,ImageId,CreationDate,BlockDeviceMappings[0].Ebs.SnapshotId,Tags[?Key==`Name`][] | [0].Value] | sort_by(@, &[2]) | [-1]'
```

### Ubuntu

Ubuntu OwnerId is `099720109477`

```bash
# list all images with ownerid=099720109477
aws ec2 describe-images --filter "Name=owner-id,Values=099720109477" "Name=image-type,Values=machine" "Name=architecture,Values=x86_64" "Name=state,Values=available" "Name=is-public,Values=true" --query 'Images[?starts_with(Name,`ubuntu/images/hvm-ssd/`)==`true`].[Name,ImageId,CreationDate,BlockDeviceMappings[0].Ebs.SnapshotId]'

# just list latest release version via sort which begins with `ubuntu/images/hvm-ssd/ubuntu-bionic`
aws ec2 describe-images --filter "Name=owner-id,Values=099720109477" "Name=image-type,Values=machine" "Name=architecture,Values=x86_64" "Name=state,Values=available" "Name=is-public,Values=true" --query 'Images[?starts_with(Name,`ubuntu/images/hvm-ssd/`)==`true`]|[?contains(Name,`bionic`)==`true`]|[*].[Name,ImageId,CreationDate,BlockDeviceMappings[0].Ebs.SnapshotId,Tags[?Key==`Name`][] | [0].Value] | sort_by(@, &[2]) | [-1]'
```

### RHEL

RHEL OwnerId is `309956199498`

```bash
# list all images with ownerid=309956199498
aws ec2 describe-images --filter "Name=owner-id,Values=309956199498" "Name=image-type,Values=machine" "Name=architecture,Values=x86_64" "Name=state,Values=available" "Name=is-public,Values=true"

# just list general available (GA) version
aws ec2 describe-images --filter "Name=owner-id,Values=309956199498" "Name=image-type,Values=machine" "Name=architecture,Values=x86_64" "Name=state,Values=available" "Name=is-public,Values=true" "Name=name,Values=*HVM_GA*"

# list all images with format table
aws ec2 describe-images --filter "Name=owner-id,Values=309956199498" "Name=image-type,Values=machine" "Name=architecture,Values=x86_64" "Name=state,Values=available" "Name=is-public,Values=true" --query 'Images[*].[Name,ImageId,CreationDate,BlockDeviceMappings[0].Ebs.SnapshotId]' --output table

# list all images with format json
aws ec2 describe-images --filter "Name=owner-id,Values=309956199498" "Name=image-type,Values=machine" "Name=architecture,Values=x86_64" "Name=state,Values=available" "Name=is-public,Values=true" --query 'Images[*].{name:Name,imageid:ImageId,creationdate:CreationDate,snapshotid:BlockDeviceMappings[0].Ebs.SnapshotId}' --output json

# just list latest release version via sort
# sort_by(@, &[0]) is sort by fisrt column, extract the last line
aws ec2 describe-images --filter "Name=owner-id,Values=309956199498" "Name=image-type,Values=machine" "Name=architecture,Values=x86_64" "Name=state,Values=available" "Name=is-public,Values=true" "Name=name,Values=*HVM_GA*" --query 'Images[*].[Name,ImageId,CreationDate,BlockDeviceMappings[0].Ebs.SnapshotId,Tags[?Key==`Name`][] | [0].Value] | sort_by(@, &[0]) | [-1]'
```

### SUSE

SUSE OwnerId is `013907871322`

```bash
# list all images with ownerid=013907871322
aws ec2 describe-images --filter "Name=owner-id,Values=013907871322" "Name=image-type,Values=machine" "Name=architecture,Values=x86_64" "Name=state,Values=available" "Name=is-public,Values=true"

# list all images with format table
aws ec2 describe-images --filter "Name=owner-id,Values=013907871322" "Name=image-type,Values=machine" "Name=architecture,Values=x86_64" "Name=state,Values=available" "Name=is-public,Values=true" --query 'Images[*].[Name,ImageId,CreationDate,BlockDeviceMappings[0].Ebs.SnapshotId]' --output table

# just list name begin with `suse-sles-`
aws ec2 describe-images --filter "Name=owner-id,Values=013907871322" "Name=image-type,Values=machine" "Name=architecture,Values=x86_64" "Name=state,Values=available" "Name=is-public,Values=true" --query 'Images[?starts_with(Name,`suse-sles-`)==`true`]|[?BlockDeviceMappings[0].Ebs.VolumeType==`gp2`]|[*].[Name,ImageId,CreationDate,BlockDeviceMappings[0].Ebs.SnapshotId]' --output table

# just list latest release version via sort
aws ec2 describe-images --filter "Name=owner-id,Values=013907871322" "Name=image-type,Values=machine" "Name=architecture,Values=x86_64" "Name=state,Values=available" "Name=is-public,Values=true" --query 'Images[?starts_with(Name,`suse-sles-`)==`true`]|[?contains(Name,`sap`)==`false`]|[?contains(Name,`-ecs-`)==`false`]|[?BlockDeviceMappings[0].Ebs.VolumeType==`gp2`]|[*].[Name,ImageId,CreationDate,BlockDeviceMappings[0].Ebs.SnapshotId,Tags[?Key==`Name`][] | [0].Value] | sort_by(@, &[0]) | [-1]'
```


<!-- End -->
