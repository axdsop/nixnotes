# AWS CLI Installation

Official documentation [Installing the AWS Command Line Interface](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html)

## TOC

1. [Prerequisites](#prerequisites)  
2. [Python Script](#python-script)  
3. [PIP Installation](#pip-installation)  
3.1 [Method 1](#method-1)  
3.2 [Method 2](#method-2)  
4. [AWS CLI Installation](#aws-cli-installation)  
4.1 [Via Pip](#via-pip)  
4.2 [Via aws-bundle](#via-aws-bundle)  
5. [Enable Command Completion](#enable-command-completion)  
5.1 [For Bash](#for-bash)  
6. [AWS CLI Uninstallation](#aws-cli-uninstallation)  
7. [Change Log](#change-log)  


## Prerequisites

Ways to install the AWS CLI

* [pip](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html#install-tool-pip)
* [Using a virtual environment](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html#install-tool-venv)
* [Using a bundled installer](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html#install-tool-bundled)


Requirements

* Python 2 version 2.6.5+ or Python 3 version 3.3+
* Windows, Linux, macOS, or Unix

System Info

```bash
# lsb_release -a
No LSB modules are available.
Distributor ID:	Debian
Description:	Debian GNU/Linux 9.6 (stretch)
Release:	9.6
Codename:	stretch

# which python3
/usr/bin/python3

# python3 --version
Python 3.5.3
```


## Python Script

I wrote a Python [script](https://gitlab.com/axdsop/nixnotes/blob/master/CloudComputing/AWS/Scripts/awscli_bundle.py) to install/update aws cli bundle on GNU/Linux


```bash
# just support Python 3
download_method='wget -qO-'
[[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && download_method='curl -fsSL'

$download_method https://gitlab.com/axdsop/nixnotes/raw/master/CloudComputing/AWS/Scripts/awscli_bundle.py | python3 -
```


## PIP Installation

[pip][pip] is the package installer for Python. You can use [pip][pip] to install packages from the Python Package Index and other indexes.

Official installation documentation is <https://pip.pypa.io/en/stable/installing/>.

### Method 1

Via python script `get-pip.py`

```bash
curl -fsL https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python3 get-pip.py --force-reinstall --user
# The script wheel is installed in '/home/ubuntu/.local/bin' which is not on PATH.

# or
curl -fsL https://bootstrap.pypa.io/get-pip.py | python3 - --force-reinstall --user
```

>get-pip.py also installs `setuptools` and `wheel` if they are not already. `setuptools` is required to install [source distributions](https://packaging.python.org/glossary/#term-source-distribution-or-sdist). Both are required in order to build a Wheel Cache (which improves installation speed), although neither are required to install pre-built `wheels`.

### Method 2

Via package manager

For `pacman`

```bash
# For Arch Linux
sudo pacman -Syyu --noconfirm
sudo pacman -S --noconfirm python python-pip

which pip
# /usr/bin/pip

pip --version
# pip 19.0.3 from /usr/lib/python3.7/site-packages/pip (python 3.7)

which pip3
# /usr/bin/pip3

pip3 --version
# pip 19.0.3 from /usr/lib/python3.7/site-packages/pip (python 3.7)
```

For `apt`

```bash
# For Debian/Ubuntu
sudo apt-get update
sudo apt-get install python3 -y
sudo apt-get install python3-pip --reinstall -y

# pip3 --version
# pip 9.0.1 from /usr/lib/python3/dist-packages (python 3.5)

# ModuleNotFoundError: No module named 'pip._internal'
# python3 -m pip --version

# create soft link to /usr/local/bin/
[[ -d /usr/local/bin ]] || sudo mkdir -pv /usr/local/bin
sudo ln -fs $(which pip3) /usr/local/bin/pip

# `umask 022` makes directories under /usr/local/lib/python3.x/dist-packages has o+rx permission
# Upgrade pip version -U/--upgrade
(umask 022; sudo pip install pip --upgrade)

# The directory '/home/ubuntu/.cache/pip' or its parent directory is not owned by the current user and caching wheels has been disabled. check the permissions and owner of that directory. If executing pip with sudo, you may want sudo's -H flag.
# sudo -H pip install pip --upgrade

pip --version
# pip 18.1 from /usr/local/lib/python3.5/dist-packages/pip (python 3.5)
```

## AWS CLI Installation

### Via Pip

Official documentation is [Installing the AWS CLI Using pip](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html#install-tool-pip).

Type | Path
--- | ---
For user | `~/.local/bin/`, `~/.local/lib/`
Global | `/usr/local/bin/`, `/usr/local/lib/` <br>or<br> `/usr/bin/`, `/usr/lib/`

**Note**: Path `~/.local/bin/` may be not in environment parameter `$PATH` by default.

For Golbal

```bash
# installing
sudo pip install awscli

# upgrading
sudo pip install awscli --upgrade

# uninstall
sudo pip uninstall awscli -y
```

For User

```bash
# installing
pip install awscli --user
# pip install awscli

# upgrading
pip install awscli --upgrade --user
# The scripts pyrsa-decrypt, pyrsa-decrypt-bigfile, pyrsa-encrypt, pyrsa-encrypt-bigfile, pyrsa-keygen, pyrsa-priv2pub, pyrsa-sign and pyrsa-verify are installed in '/home/XXXXXXX/.local/bin' which is not on PATH. Consider adding this directory to PATH or, if you prefer to suppress this warning, use --no-warn-script-location.

# uninstall
pip uninstall awscli -y
rm -rf ~/.local/bin/
rm -rf ~/.local/lib/python3*
```

Version info

```bash
aws --version
# aws-cli/1.16.211 Python/3.7.4 Linux/5.2.5-arch1-1-ARCH botocore/1.12.201
```


### Via aws-bundle

Official documentation is [Install the AWS CLI Using the Bundled Installer (Linux, macOS, or Unix)](https://docs.aws.amazon.com/cli/latest/userguide/install-bundle.html).

aws-bundle requires [distutils](https://docs.python.org/3/library/distutils.html) to build and install Python modules.

```bash
# Debian/Ubuntu
sudo apt-get install python3-distutils -y
```

Operation command

```bash
download_method='wget -qO-'
[[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && download_method='curl -fsSL'

# Latest release version https://github.com/aws/aws-cli/blob/master/CHANGELOG.rst
$download_method https://raw.githubusercontent.com/aws/aws-cli/master/CHANGELOG.rst | sed -r -n '/^=+/{x;p};h' | sed -r -n '/^[[:digit:]]+/{p;q}'

# https://docs.aws.amazon.com/cli/latest/userguide/install-bundle.html
awscli_bundle_url='https://s3.amazonaws.com/aws-cli/awscli-bundle.zip'
bundle_path="/tmp/${awscli_bundle_url##*/}"

$download_method "${awscli_bundle_url}" > "${bundle_path}"

cd /tmp
unzip "${bundle_path}"

# ./awscli-bundle/install -b ~/bin/aws
# sudo ./awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws
# sudo $(which python3) awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws

install_path=${install_path:-'/opt/aws'}
bashrc_path=${bashrc_path:-"$HOME/.bashrc"}

# need python3, python3-distutils
sudo python3 "${bundle_path%.*}"/install -i "${install_path}" -b /usr/local/bin/aws
# You can now run: /usr/local/bin/aws --version

if [[ "$SHELL" =~ /bin/bash$ && -f "${install_path}/bin/aws_completer" ]]; then
    sed -r -i '/aws_completer/d' "${bashrc_path}"
    echo "complete -C '${install_path}/bin/aws_completer' aws" >> "${bashrc_path}"
fi

rm -rf "${bundle_path%.*}"*

ls -l /usr/local/bin/aws
# lrwxrwxrwx 1 root root 16 Aug  5 16:05 /usr/local/bin/aws -> /opt/aws/bin/aws

/usr/local/bin/aws --version
# aws-cli/1.16.211 Python/3.7.4 Linux/5.2.5-arch1-1-ARCH botocore/1.12.201
```

## Enable Command Completion

The aws-cli package includes a very useful command completion feature. This feature is not automatically installed so you need to configure it manually.

* https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-completion.html
* https://github.com/aws/aws-cli#command-completion


```bash
which aws_completer
# ~/.local/bin/aws_completer
# /usr/local/bin/aws_completer

# make sure path `~/.local/bin` in environment parameter `$PATH`
complete -C '"$(which aws_completer)"' aws
# complete -C '/opt/aws/bin/aws_completer' aws
```

Or add `bin/aws_bash_completer` file under `/etc/bash_completion.d`, `/usr/local/etc/bash_completion.d` or any other `bash_completion.d` location.

For `zsh` use `bin/aws_zsh_completer.sh`.

### For Bash

For bash shell

```bash
source_dir=${source_dir:-"$HOME/.local/bin"}
[[ -s '/usr/local/bin/aws_completer' ]] && source_dir='/usr/local/bin'

aws_completer_source_path="${source_dir}/aws_bash_completer"

aws_completer_target_path='/etc/bash_completion.d/aws_bash_completer'

sudo cp -f "${aws_completer_source_path}" "${aws_completer_target_path}"
sudo chown root:root "${aws_completer_target_path}"
sudo chmod 644 "${aws_completer_target_path}"
```

## AWS CLI Uninstallation

```bash
# for user
echo 'y' | pip uninstall awscli --user

# globally
echo 'y' | sudo pip uninstall awscli

aws_completer_target_path='/etc/bash_completion.d/aws_bash_completer'
[[ -f "${aws_completer_target_path}" ]] && sudo rm -f "${aws_completer_target_path}"
```


## Change Log

* Dec 18, 2018 16:34 Tue -0500
  * First draft
* Aug 05, 2019 16:14 Mon ET
  * Add testing on Arch Linux
* Oct 16, 2019 15:55 Wed ET
  * Add `distutils`


[pip]:https://github.com/pypa/pip "The Python package installer"


<!-- End -->
