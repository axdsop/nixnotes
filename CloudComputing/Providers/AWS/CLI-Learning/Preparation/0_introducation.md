# AWS CLI Introducation

## TOC

1. [Introducation](#introducation)  
2. [AWS SDK](#aws-sdk)  
3. [Change Log](#change-log)  


## Introducation

[What Is the AWS Command Line Interface?](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-welcome.html) says:
>The **AWS CLI** is an open source tool that enables you to interact with AWS services using commands in your command-line shell. With minimal configuration, you can start using functionality equivalent to that provided by the browser-based AWS Management Console from the command prompt in your favorite terminal program.  --

[Command Output](https://github.com/aws/aws-cli#command-output) says:
>The default output for commands is currently JSON. You can use the `--query` option to extract the output elements from this JSON document. For more information on the expression language used for the `--query` argument, you can read the [JMESPath Tutorial](http://jmespath.org/tutorial.html).


## AWS SDK

* [AWS SDK for Go](https://docs.aws.amazon.com/sdk-for-go/api/)
* [AWS SDK for Python (Boto)](https://boto3.amazonaws.com/v1/documentation/api/latest/index.html)
* [AWS SDK for Rust (Rusoto)](https://rusoto.org/)

>**Boto** is the Amazon Web Services (AWS) SDK for Python, which allows Python developers to write software that makes use of Amazon services like S3 and EC2. Boto provides an easy to use, object-oriented API as well as low-level direct service access.


## Change Log

* Dec 18, 2018 16:34 Tue -0500
  * First draft
* Aug 05, 2019 15:17 Mon ET
  * Add aws sdk for rust


<!-- End -->
