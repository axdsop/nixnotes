# AWS CLI Configuration

Official Documents [Using the AWS Command Line Interface](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-using.html).


## TOC

1. [aws configure](#aws-configure)  
1.1 [Example](#example)  
2. [Environment Variables](#environment-variables)  
3. [Using an HTTP Proxy](#using-an-http-proxy)  
4. [Assuming an IAM Role](#assuming-an-iam-role)  
5. [Change Log](#change-log)  


Configuring the AWS CLI

* [Configuring the AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html)
* https://github.com/aws/aws-cli#getting-started


## aws configure

Default configure file is `~/.aws/config`, `~/.aws/credentials`.

Via interacitve mode

```bash
# create / update configuration file
# - default configuration
aws configure

# - add another user configuration   Named Profiles
aws configure --profile user2
```

Via quiet mode

```bash
# details see help info
aws --profile default configure set help
```

### Example

Configuration

```bash
# method 1 - quiet mode
aws configure set aws_access_key_id AKIAJZUTEDFHPKTUIPJA
aws configure set aws_secret_access_key DfLfoYo/mXDmUCBivZwRpIcqEU9BbOEcjmWk4vT/
aws configure set default.region us-east-2
aws configure set default.output json

# method 2 - interacitve mode
aws configure
AWS Access Key ID [None]: AKIAJZUTEDFHPKTUIPJA
AWS Secret Access Key [None]: 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
Default region name [None]: us-east-2
Default output format [None]: json

# cat .aws/config
[default]
region = us-east-2
output = json
# cat .aws/credentials
[default]
aws_secret_access_key = DfLfoYo/mXDmUCBivZwRpIcqEU9BbOEcjmWk4vT/
aws_access_key_id = AKIAJZUTEDFHPKTUIPJA
```

Testing

```bash
aws ec2 describe-regions --query "Regions[].{Endpoint:Endpoint,RegionName:RegionName}" --output table

--------------------------------------------------------
|                    DescribeRegions                   |
+-----------------------------------+------------------+
|             Endpoint              |   RegionName     |
+-----------------------------------+------------------+
|  ec2.eu-north-1.amazonaws.com     |  eu-north-1      |
|  ec2.ap-south-1.amazonaws.com     |  ap-south-1      |
|  ec2.eu-west-3.amazonaws.com      |  eu-west-3       |
|  ec2.eu-west-2.amazonaws.com      |  eu-west-2       |
|  ec2.eu-west-1.amazonaws.com      |  eu-west-1       |
|  ec2.ap-northeast-2.amazonaws.com |  ap-northeast-2  |
|  ec2.ap-northeast-1.amazonaws.com |  ap-northeast-1  |
|  ec2.sa-east-1.amazonaws.com      |  sa-east-1       |
|  ec2.ca-central-1.amazonaws.com   |  ca-central-1    |
|  ec2.ap-southeast-1.amazonaws.com |  ap-southeast-1  |
|  ec2.ap-southeast-2.amazonaws.com |  ap-southeast-2  |
|  ec2.eu-central-1.amazonaws.com   |  eu-central-1    |
|  ec2.us-east-1.amazonaws.com      |  us-east-1       |
|  ec2.us-east-2.amazonaws.com      |  us-east-2       |
|  ec2.us-west-1.amazonaws.com      |  us-west-1       |
|  ec2.us-west-2.amazonaws.com      |  us-west-2       |
+-----------------------------------+------------------+

aws ec2 describe-availability-zones

---------------------------------------------------------
|               DescribeAvailabilityZones               |
+-------------------------------------------------------+
||                  AvailabilityZones                  ||
|+-------------+------------+-----------+--------------+|
|| RegionName  |   State    |  ZoneId   |  ZoneName    ||
|+-------------+------------+-----------+--------------+|
||  us-east-2  |  available |  use2-az1 |  us-east-2a  ||
|+-------------+------------+-----------+--------------+|
||                  AvailabilityZones                  ||
|+-------------+------------+-----------+--------------+|
|| RegionName  |   State    |  ZoneId   |  ZoneName    ||
|+-------------+------------+-----------+--------------+|
||  us-east-2  |  available |  use2-az2 |  us-east-2b  ||
|+-------------+------------+-----------+--------------+|
||                  AvailabilityZones                  ||
|+-------------+------------+-----------+--------------+|
|| RegionName  |   State    |  ZoneId   |  ZoneName    ||
|+-------------+------------+-----------+--------------+|
||  us-east-2  |  available |  use2-az3 |  us-east-2c  ||
|+-------------+------------+-----------+--------------+|

aws iam list-access-keys

--------------------------------------------------------------------------
|                             ListAccessKeys                             |
+------------------------------------------------------------------------+
||                           AccessKeyMetadata                          ||
|+-----------------------+------------------------+---------+-----------+|
||      AccessKeyId      |      CreateDate        | Status  | UserName  ||
|+-----------------------+------------------------+---------+-----------+|
||  AKIAIJC5IT33SNBX2JEQ |  2018-12-18T19:01:01Z  |  Active | aws_learn ||
|+-----------------------+------------------------+---------+-----------+|
```


## Environment Variables

[Environment Variables](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-envvars.html)

Variables | Explanation
---|---
`AWS_ACCESS_KEY_ID` | Specifies an AWS access key associated with an IAM user or role.
`AWS_SECRET_ACCESS_KEY` | Specifies the secret key associated with the access key. This is essentially the 'password' for the access key.
`AWS_SESSION_TOKEN` | Specifies the session token value that is required if you are using temporary security credentials.
`AWS_DEFAULT_REGION` | Specifies the AWS region to send the request to.
`AWS_DEFAULT_OUTPUT` | Specifies the output format to use.
`AWS_PROFILE` | Specifies the name of the CLI profile with the credentials and options to use. <br/>This can be the name of a profile stored in a credentials or config file, or the value default to use the default profile.
`AWS_CA_BUNDLE` | Specifies the path to a certificate bundle to use for HTTPS certificate validation.
`AWS_SHARED_CREDENTIALS_FILE` | Specifies the location of the file that the AWS CLI uses to store access keys (the default is `~/.aws/credentials`).
`AWS_CONFIG_FILE` | Specifies the location of the file that the AWS CLI uses to store configuration profiles (the default is `~/.aws/config`).


```bash
export AWS_ACCESS_KEY_ID=AKIAJZUTEDFHPKTUIPJA
export AWS_SECRET_ACCESS_KEY=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
export AWS_DEFAULT_REGION=us-west-2
```


## Using an HTTP Proxy

[Using an HTTP Proxy](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-proxy.html)

>If you need to access AWS through proxy servers, you can configure the `HTTP_PROXY` and `HTTPS_PROXY` environment variables with the IP addresses and port numbers used by your proxy servers.

```bash
export HTTP_PROXY=http://a.b.c.d:n
export HTTPS_PROXY=http://w.x.y.z:m
```

Authenticating to a Proxy

```bash
export HTTP_PROXY=http://username:password@a.b.c.d:n
export HTTPS_PROXY=http://username:password@w.x.y.z:m
```

## Assuming an IAM Role

* [Assuming an IAM Role](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-role.html)
* [Creating a Role to Delegate Permissions to an IAM User](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_create_for-user.html)

>An [IAM role](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles.html) is a authorization tool that lets an IAM user gain additional (or different) permissions, or get permissions to perform actions in a different AWS account.

Authenticating to AWS with the Credentials File

https://blog.gruntwork.io/authenticating-to-aws-with-the-credentials-file-d16c0fbcbf9e

```bash
# role_arn
aws iam list-roles --query 'Roles[?contains(Arn,`amazonaws.com`)!=`true`].[Arn]' --output text
# aws iam list-roles --query 'Roles[*].[Arn]' --output text

# mfa_serial
# aws iam list-mfa-devices
aws iam list-mfa-devices --query 'MFADevices[*].[SerialNumber]' --output text
```


## Change Log

* Dec 18, 2018 16:34 Tue -0500
  * First draft

<!-- End -->
