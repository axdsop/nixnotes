#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails

# Target: AWS CLI Operation For Free Tier (just for personal user)
# Developer: MaxdSre

# Change Log:
# - Oct 08, 2020 08:47 Thu ET - initialization check method update
# - Oct 24, 2019 10:51 Thu ET - add ec2 run-instances user-data snippets code
# - Oct 16, 2019 15:43 Wed ET - remove deprecated variable 'update_operation'
# - Sep 26, 2019 20:26 Thu ET - change to new base functions
# - Aug 06, 2019 20:11 Tue ET - access key rotate optimization (just for profile default)
# - Jul 25, 2019 ET - change git url to 'https://gitlab.com/axdsop/nixnotes'
# - Jun 07, 2019 17:01 ET - add login ip into security group when list ec2 instances (if login via VPN)
# - Jun 03, 2019 09:43 Mon ET - AMI id update, function 'funcBaseScriptImport' optimization
# - May 24, 2019 21:33 Fri ET - change script url from GitHub to GitLab
# - Apr 24, 2019 09:53 Wed ET - solve problem openssl prompts error 'unable to load Private Key' which is result from ssh-keygen does not create RSA private key
# - Jan 04, 2019 09:55 Fri ET - include base funcions from other file
# - Dec 19, 2018 15:35 Wed ET - first draft


#########  0-1. Singal Setting  #########
trap fnBase_TrapCleanUp INT QUIT

#########  0-2. Variables Setting  #########
# umask 022    # temporarily change umask value to 022
insatll_update_operation=${insatll_update_operation:-0}
rotation_operation=${rotation_operation:-0}

common_name=${common_name:-'free_tier'}
keypair_name=${keypair_name:-"${common_name}"}
keypair_path=${keypair_path:-'~/.aws/key/'}
# keypair_path=$(dirname $(readlink -nf "$0"))
service_name=${action_name:-}
action_name=${action_name:-}
ami_name=${ami_name:-}
user_login_ip=${user_login_ip:-}
readonly raw_git_url='https://gitlab.com/axdsop/nixnotes/raw/master'
ami_list_latest_link=${ami_list_latest_link:-"${raw_git_url}/CloudComputing/AWS/CLI-Learning/Service/EC2/free_tier_eligible_ami_list.md"}
awscli_bundle_python_script_link=${awscli_bundle_python_script_link:-"${raw_git_url}/CloudComputing/AWS/Scripts/awscli_bundle.py"}


#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | bash -s -- [options] ...

AWS CLI Operation For AWS Free Tier On GNU/Linux.

https://aws.amazon.com/free/

[available option]
    -h    --help, show help info
    -n common_name    --specify tag:Name value (default is 'free_tier')
    -s service_name    --specify service name (ec2,vpc)
    -a action_name    --service action (create,delete,list, vpc has recreate), along with '-s'
    -A ami_name --specify ami name (ubuntu/rhel/suse/amzn), default is 'ubuntu'
    -i    --install or update aws cli via bundle package
    -r    --access key rotation (profile 'default'), ssh key pair (re)generation
\e[0m"
}

# http://wiki.bash-hackers.org/howto/getopts_tutorial
# https://www.mkssoftware.com/docs/man1/getopts.1.asp

while getopts "ha:s:n:A:ir" option "$@"; do
    case "$option" in
        a ) action_name="$OPTARG" ;;
        s ) service_name="$OPTARG" ;;
        n ) common_name="$OPTARG" ;;
        A ) ami_name="$OPTARG" ;;
        i ) insatll_update_operation=1 ;;
        r ) rotation_operation=1 ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done


#########  1-2 Preparation Operation  #########
fn_InitializationCheck(){
    local l_user_codebase_dir="$HOME/.config/axdsop"
    local l_base_function_temp_save_path=''
    local l_need_remove=0
    local l_base_function_url='https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/base_functions.sh'
    l_base_function_temp_save_path=$(find "${l_user_codebase_dir}" -type f -name "${l_base_function_url##*/}" -print 2> /dev/null)
    
    if [[ ! -f "${l_base_function_temp_save_path}" ]]; then
        local l_download_tool=${l_download_tool:-'wget -qO-'}
        [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_tool='curl -fsL'
        l_need_remove=1
        l_base_function_temp_save_path=$(mktemp -t XXXXXXX)
        ${l_download_tool} "${l_base_function_url}" > "${l_base_function_temp_save_path}"
    fi

    if [[ -f "${l_base_function_temp_save_path}" && -s "${l_base_function_temp_save_path}" ]]; then
        . "${l_base_function_temp_save_path}"
        [[ "${l_need_remove}" -eq 1 ]] && rm "${l_base_function_temp_save_path}"
    else
        echo "Sorry, fail to get base script path."
        exit
    fi

    # specify 1 means need sudo privilege
    fnBase_RunningEnvironmentCheck '0'

    fnBase_CommandExistCheckPhase 'sed'

    login_user_bashrc=${login_user_bashrc:-}
    [[ -s "${login_user_home}/.bashrc" ]] && login_user_bashrc="${login_user_home}/.bashrc"

    # Login Ip
    # wget -qO- ipinfo.io/ip 2> /dev/null || curl -fsL ipinfo.io/ip 2> /dev/null
    user_login_ip=$(${l_download_tool} ipinfo.io/ip 2> /dev/null || ${l_download_tool} https://ipapi.co/ip/ 2> /dev/null)
}

fn_AWS_ConfigPath_Detection(){
    aws_config_dir=${aws_config_dir:-"${login_user_home}/.aws"}
    aws_credential_path="${aws_config_dir}/credentials"
    [[ -s "${aws_credential_path}" ]] || fnBase_ExitStatement "${c_red}Sorry${c_normal}: fail to find credential ${aws_credential_path}"
}


#########  2 Initialize Operation  #########
fn_AWS_AccessKeys_Deletion(){
    local l_user_name="${1:-}"
    local l_key_id="${2:-}"
    if [[ -n "${l_user_name}" && -n "${l_key_id}" ]]; then
        # aws iam list-access-keys --user-name="${l_user_name}" --query "AccessKeyMetadata[?AccessKeyId=='${l_key_id}']|[*].[AccessKeyId]" --output text
        aws iam update-access-key --user-name "${l_user_name}" --access-key-id "${l_key_id}" --status Inactive
        aws iam delete-access-key --user-name "${l_user_name}" --access-key-id "${l_key_id}"
    fi
}

fn_AWS_RotatingAccessKeys(){
    echo -e '\nRotating Access Keys'
    local existed_access_key_id=''

    # aws configure get default.aws_access_key_id
    # aws configure get aws_access_key_id --profile default
    # aws configure get profile.default.aws_access_key_id
    existed_access_key_id=$(aws configure get default.aws_access_key_id 2> /dev/null)
    [[ -n "${existed_access_key_id}" ]] || existed_access_key_id=$(sed -r -n '/aws_access_key_id/{s@^[^=]+=[[:space:]]*(.*)$@\1@g;p;q}' "${aws_credential_path}")
    [[ -z "${existed_access_key_id}" ]] && fnBase_ExitStatement "${c_red}Attention${c_normal}: value of parameter ${c_red}aws_access_key_id${c_normal} is empty."

    local default_user_name
    default_user_name=$(aws iam list-access-keys --query "AccessKeyMetadata[?AccessKeyId=='${existed_access_key_id}'].[UserName]" --output text)

    # remove existed access key with the same user name
    aws iam list-access-keys --user-name="${default_user_name}" --query "AccessKeyMetadata[?AccessKeyId!='${existed_access_key_id}']|[*].[AccessKeyId]" --output text | while read -r line; do
        fn_AWS_AccessKeys_Deletion "${default_user_name}" "${line}"
    done

    # aws iam list-access-keys --user-name "${default_user_name}"
    # aws iam get-access-key-last-used --access-key-id AKIAJD2AGYCJORHG3U7A

    # - Create Newer Access Key
    local new_access_key_info
    # ACCESSKEY	$AccessKeyId	$DATE	$SecretAccessKey	Active	$UserName
    # ACCESSKEY	AKI3J6LRB2YCRHA54L5Q	2019-03-27T17:53:19Z	bAN5kWTIzATkiz0j2nxv8vPfQ3JjCjOYCI1hJsrl	Active	$UserName
    new_access_key_info=$(aws iam create-access-key --user-name "${default_user_name}" --output text | sed -r -n 's@[[:blank:]]+@ @g;p' | cut -d" " -f2,4)
    # awk 'BEGIN{OFS=" "}{$1=$1;print $2,$4}'

    # AccessKeyId / SecretAccessKey
    # AKI3J6LRB2YCRHA54L5Q bAN5kWTIzATkiz0j2nxv8vPfQ3JjCjOYCI1hJsrl

    local new_access_key_id
    local new_secret_access_key
    new_access_key_id="${new_access_key_info%% *}"
    new_secret_access_key="${new_access_key_info##* }"

    # aws iam list-access-keys --user-name "${default_user_name}" --query "reverse(sort_by(AccessKeyMetadata,&CreateDate))[:1].[AccessKeyId]" --output text

    # - Replace ~/.aws/credentials
    aws configure set aws_access_key_id "${new_access_key_id}"
    aws configure set aws_secret_access_key "${new_secret_access_key}"
    # aws configure set default.region us-east-2
    # aws configure list

    # - Change Old Access Key Status Active/Inactive , then delete older
    check_session_refresh(){
        # new credential needs about 8 seconds to make effect
        local l_count=${1:-0}
        local l_sleep_time=${l_sleep_time:-6}
        [[ "${l_count}" -ge 1 ]] && l_sleep_time=2
        sleep "${l_sleep_time}"
        let l_count+=1
        [[ $(aws ec2 describe-regions --query 'Regions[*].[RegionName]' --output text 2> /dev/null | wc -l) -gt 0 ]] || check_session_refresh "${l_count}"
    }

    check_session_refresh

    aws iam list-access-keys --user-name "${default_user_name}" --query 'AccessKeyMetadata[?AccessKeyId!=`'"${new_access_key_id}"'`].[AccessKeyId]' --output text | while read -r line; do
        fn_AWS_AccessKeys_Deletion "${default_user_name}" "${line}"
    done

    # - List current user
    # aws iam list-access-keys --user-name "${default_user_name}"
    if [[ $(aws iam list-access-keys --user-name "${default_user_name}" --query 'AccessKeyMetadata[*].AccessKeyId' --output text 2> /dev/null) == "${new_access_key_id}" ]]; then
        aws iam list-access-keys --user-name "${default_user_name}"
    else
        fnBase_ExitStatement "${c_red}Error${c_normal}: fail to rotate access key!"
    fi
}

fn_AWS_SSHKeyPair_Generation(){
    local l_key_name="${1:-}"
    local l_key_path="${2:-}"

    key_pair_generation(){
        local k_name="${1:-'SSH Key'}"
        local k_type="${2:-'rsa'}"
        local k_path="${3:-'~/.ssh/'}"

        case "${k_type,,}" in
            ed25519 ) k_type='ed25519' ;;
            * ) k_type='rsa' ;;
        esac

        if [[ "${k_path}" =~ ^~ ]]; then
            [[ -n "${USER:-}" && -z "${SUDO_USER:-}" ]] && login_user="$USER" || login_user="$SUDO_USER"
            [[ "${login_user}" == 'root' ]] && login_user_home='/root' || login_user_home="/home/${login_user}"
            k_path="${login_user_home}/${k_path#*/}"
        fi

        [[ -d "${k_path}" ]] || mkdir -p "${k_path}"

        local private_key_path
        private_key_path="${k_path%/*}/${k_name// /-}"
        local public_key_path
        public_key_path="${private_key_path}.pub"

        case "${k_type,,}" in
            rsa )
                echo -e 'y\n' | ssh-keygen -q -m PEM -t rsa -b 4096 -f "${private_key_path}" -N '' -C "${k_name}" &> /dev/null
                ;;
            ed25519 )
                echo -e 'y\n' | ssh-keygen -q -m PEM -t ed25519 -f "${private_key_path}" -N '' -C "${k_name}" &> /dev/null
                ;;
        esac

        [[ -f "${private_key_path}" ]] && chmod 600 "${private_key_path}"
        [[ -f "${public_key_path}" ]] && chmod 640 "${public_key_path}"
        echo "${public_key_path}"
    }

    echo -e '\nSSH Key Pairs Generation'
    # - generate new key pair
    local pub_key_pair_path
    pub_key_pair_path=$(key_pair_generation "${l_key_name}" 'rsa' "${l_key_path}")
    # aws ec2 describe-key-pairs --output text --query 'KeyPairs[*].[KeyName]'

    # - delete existed key pair with the same name
    aws ec2 delete-key-pair --key-name "${l_key_name}"

    # - import newly generated key pair
    aws ec2 import-key-pair --key-name "${l_key_name}" --public-key-material file://"${pub_key_pair_path}" 1> /dev/null

    # aws ec2 describe-key-pairs
    # ssh-keygen does not create RSA private key    https://serverfault.com/questions/939909/ssh-keygen-does-not-create-rsa-private-key#941893
    if [[ $(aws ec2 describe-key-pairs --filters "Name=key-name,Values=${l_key_name}" --query 'KeyPairs[*].KeyFingerprint' --output text) == $(openssl rsa -in "${pub_key_pair_path%.*}" -pubout -outform DER | openssl md5 -c | sed -r -n '/stdin/{s@^[^=]+=[[:space:]]*([^[:space:]]+).*$@\1@g;p}') ]]; then
        aws ec2 describe-key-pairs --filters "Name=key-name,Values=${l_key_name}"
    else
        fnBase_ExitStatement "${c_red}Error${c_normal}: fail to update ssh key pairs!"
    fi
}


#########  3 Service Operation  #########
fn_AWS_VPC_Deletion(){
    local vpc_id="${1:-}"

    aws ec2 describe-vpcs --filters "Name=vpc-id,Values=${vpc_id}" --query "Vpcs[*].{VpcId:VpcId,Tag_Name:Tags[?Key=='Name'].Value | [0]}" --output table

    # - security-group
    # An error occurred (CannotDelete) when calling the DeleteSecurityGroup operation: the specified group: "sg-0deaa9cd9330d159b" name: "default" cannot be deleted by a user
    aws ec2 describe-security-groups --filters "Name=vpc-id,Values=${vpc_id}" --query 'SecurityGroups[?GroupName!=`default`].[GroupId]' --output text | sed -r 's@[[:blank:]]+@\n@g' | while read -r line; do
        aws ec2 delete-security-group --group-id "${line}"
    done

    # - subnet
    # subnet_id_list=$(aws ec2 describe-subnets --filters "Name=vpc-id,Values=${vpc_id}" --query 'Subnets[*].SubnetId' --output text | sed -r 's@[[:blank:]]+@\n@g')
    aws ec2 describe-subnets --filters "Name=vpc-id,Values=${vpc_id}" --query 'Subnets[*].SubnetId' --output text | sed -r 's@[[:blank:]]+@\n@g' | while read -r line; do
        aws ec2 delete-subnet --subnet-id "${line}"
    done

    # - route-table
    # route_table_id_main=$(aws ec2 describe-route-tables --filters "Name=vpc-id,Values=${vpc_id}" "Name=association.main,Values=true" --query 'RouteTables[*].RouteTableId' --output text)
    # An error occurred (DependencyViolation) when calling the DeleteRouteTable operation: The routeTable 'rtb-066d7c8bbfc852e42' has dependencies and cannot be deleted.

    aws ec2 describe-route-tables --filters "Name=vpc-id,Values=${vpc_id}" --query 'RouteTables[?Associations[0].Main!=`true`].[RouteTableId]' --output text | while read -r line; do
        aws ec2 delete-route-table --route-table-id "${line}"
    done

    # - internet-gateway
    local internet_gateway_id
    aws ec2 describe-internet-gateways --filters "Name=attachment.vpc-id,Values=${vpc_id}" --query 'InternetGateways[*].InternetGatewayId' --output text | sed -r 's@[[:blank:]]+@\n@g' | while read -r line; do
        aws ec2 detach-internet-gateway --internet-gateway-id "${line}" --vpc-id "${vpc_id}"
        aws ec2 delete-internet-gateway --internet-gateway-id "${line}"
    done

    # - vpc
    aws ec2 delete-vpc --vpc-id "${vpc_id}"
}

fn_AWS_VPC_Operation(){
    local vpc_name=${1:-"${common_name}"}
    local vpc_ation="${2:-}"

    case "${vpc_ation,,}" in
        list|l )
            echo 'VPC list'
            aws ec2 describe-vpcs --filters "Name=isDefault,Values=false" --query "Vpcs[*].{VpcId:VpcId,State:State,CidrBlock:CidrBlock,IsDefault:IsDefault,Tag_Name:Tags[?Key=='Name'].Value | [0]}" --output table
            ;;
        create|c )
            echo 'VPC create operation'
            local subnet_name
            subnet_name=${subnet_name:-"${vpc_name}"}
            local internet_gateway_name
            internet_gateway_name=${internet_gateway_name:-"${vpc_name}"}
            local route_table_name
            route_table_name=${route_table_name:-"${vpc_name}"}
            local sec_group_name
            sec_group_name=${sec_group_name:-"${vpc_name}"}

            local vpc_id_exist
            vpc_id_exist=$(aws ec2 describe-vpcs --filters "Name=tag:Name,Values=${vpc_name}" --query 'Vpcs[].VpcId' --output text)
            if [[ -n "${vpc_id_exist}"  ]]; then
                echo "${c_red}Attention${c_normal}: existed VPC with name ${c_red}${vpc_name}${c_normal}."
                aws ec2 describe-vpcs --filters "Name=tag:Name,Values=${vpc_name}" --query "Vpcs[*].{VpcId:VpcId,State:State,CidrBlock:CidrBlock,Tag_Name:Tags[?Key=='Name'].Value | [0]}" --output table
                fnBase_ExitStatement ''
            fi

            # - Create a VPC
            local vpc_id_new
            vpc_id_new=$(aws ec2 create-vpc --cidr-block 192.168.0.0/16 --no-amazon-provided-ipv6-cidr-block --instance-tenancy default --output text | sed -r -n '/^VPC/{s@.*?(vpc-[^[:space:]]+).*$@\1@g;p}')

            # Note: vpc can assign the same tag name:value
            aws ec2 create-tags --resources "${vpc_id_new}" --tags 'Key=Name,Value='"${vpc_name}"''

            # Default enableDnsHostnames is false, enableDnsSupport is true
            # --no-enable-dns-hostnames, --no-enable-dns-support
            aws ec2 modify-vpc-attribute --vpc-id "${vpc_id_new}" --enable-dns-hostnames
            aws ec2 modify-vpc-attribute --vpc-id "${vpc_id_new}" --enable-dns-support
            # aws ec2 describe-vpc-attribute --vpc-id "${vpc_id_new}" --attribute enableDnsHostnames
            # aws ec2 describe-vpc-attribute --vpc-id "${vpc_id_new}" --attribute enableDnsSupport

            # aws ec2 describe-vpcs --filters "Name=tag:Name,Values=${vpc_name}"

            # - Create Subnet (need VPC)
            local default_region
            default_region=$(sed -r -n '/region/{s@^[^=]+=[[:space:]]*([^[:space:]]*)$@\1@g;p}' "${aws_config_dir}/config")
            local region_zone_id
            region_zone_id=$(aws ec2 describe-availability-zones --filter "Name=region-name,Values=${default_region}" --query 'AvailabilityZones[*].ZoneId' --output text | sed -r 's@[[:blank:]]+@\n@g' | sort --random-sort | head -n 1)
            local local
            subnet_id_new=$(aws ec2 create-subnet --availability-zone-id "${region_zone_id}" --vpc-id "${vpc_id_new}" --cidr-block 192.168.1.0/24 --output text | sed -r -n '/subnet-/{s@.*(subnet-[^[:space:]]+).*$@\1@g;p}')

            aws ec2 create-tags --resources "${subnet_id_new}" --tags 'Key=Name,Value='"${subnet_name}"''

            # automatically receives a public IP address
            # MapPublicIpOnLaunch True/False
            # aws ec2 modify-subnet-attribute --subnet-id "${subnet_id_new}" --map-public-ip-on-launch
            aws ec2 modify-subnet-attribute --subnet-id "${subnet_id_new}" --no-map-public-ip-on-launch

            # aws ec2 describe-subnets --filters "Name=vpc-id,Values=${vpc_id_new}"

            # The Following Operations Make Subnet Public
            # - Create internet gateway (Need VPC)
            local internet_gateway_id_new
            internet_gateway_id_new=$(aws ec2 create-internet-gateway --output text | sed -r -n '/igw-/{s@.*(igw-[^[:space:]]+).*$@\1@g;p}')

            aws ec2 create-tags --resources "${internet_gateway_id_new}" --tags 'Key=Name,Value='"${internet_gateway_name}"''

            aws ec2 attach-internet-gateway --vpc-id "${vpc_id_new}" --internet-gateway-id "${internet_gateway_id_new}"
            # aws ec2 describe-internet-gateways --internet-gateway-ids "${internet_gateway_id_new}"

            # - Create Route Table (Need VPC, Internet gateway)
            local route_table_id_new
            route_table_id_new=$(aws ec2 create-route-table --vpc-id "${vpc_id_new}" --output text | sed -r -n '/rtb-/{s@.*(rtb-[^[:space:]]+).*$@\1@g;p}')

            aws ec2 create-tags --resources "${route_table_id_new}" --tags 'Key=Name,Value='"${route_table_name}"''

            aws ec2 create-route --route-table-id "${route_table_id_new}" --destination-cidr-block 0.0.0.0/0 --gateway-id "${internet_gateway_id_new}" 1> /dev/null
            # aws ec2 describe-route-tables --route-table-id "${route_table_id_new}"

            # choose which subnet to associate with the custom route table
            aws ec2 associate-route-table  --subnet-id "${subnet_id_new}" --route-table-id "${route_table_id_new}" 1> /dev/null

            # - Security Groups
            local sec_group_ip_new
            sec_group_ip_new=$(aws ec2 create-security-group --group-name "${sec_group_name}" --vpc-id "${vpc_id_new}" --description "Allow SSH Inbound Connection." --output text)

            aws ec2 create-tags --resources "${sec_group_ip_new}" --tags 'Key=Name,Value='"${sec_group_name}"''

            # - authorize security group
            aws ec2 authorize-security-group-ingress --group-id "${sec_group_ip_new}" --ip-permissions IpProtocol=tcp,FromPort=22,ToPort=22,IpRanges='[{CidrIp='"${user_login_ip}"'/32,Description="SSH for living room"}]'
            # aws ec2 authorize-security-group-ingress --group-id sg-903004f8 --protocol tcp --port 22 --cidr "${user_login_ip}"/32

            aws ec2 describe-vpcs --filters "Name=tag:Name,Values=${vpc_name}"
            ;;
        delete|del|d )
            echo 'VPC delete operation'

            local vpc_id_list
            vpc_id_list=$(aws ec2 describe-vpcs --filters "Name=tag:Name,Values=${vpc_name}" --query 'Vpcs[].VpcId' --output text)
            if [[ -n "${vpc_id_list}" ]]; then
                echo "${vpc_id_list}" | sed -r 's@[[:blank:]]+@\n@g' | while read -r line; do
                    fn_AWS_VPC_Deletion "${line}"
                done
            else
                echo "${c_red}Attention${c_normal}: no VPC with name ${c_red}${vpc_name}${c_normal} find."
                fn_AWS_VPC_Operation "${vpc_name}" 'l'
                # fnBase_ExitStatement ''
            fi
            ;;
        recreate|re|r )
            fn_AWS_VPC_Operation "${vpc_name}" 'd'
            fn_AWS_VPC_Operation "${vpc_name}" 'c'
            ;;
        * )
            fnBase_ExitStatement "${c_red}Error${c_normal}: please specify service action with '-a'!"
            ;;
    esac
}

fn_AWS_EC2_Operation(){
    local ec2_name=${1:-"${common_name}"}
    local ec2_ation="${2:-}"

    local instance_name
    instance_name=${instance_name:-'ec2_free_tier'}

    local instance_id

    case "${ec2_ation}" in
        list|l )
            # echo -e 'EC2 instance (not terminated) list:\n'
            aws ec2 describe-instances --query "Reservations[].Instances[]|[?State.Name!='terminated']|[*].{InstanceId:InstanceId,LaunchTime:LaunchTime,PublicIp:PublicIpAddress,PublicDnsName:PublicDnsName,State:State.Name,Tag_Name:Tags[?Key=='Name'].Value | [0]}"

            # Add login ip into security group if login via VPN
            local sec_group_name
            sec_group_name=${sec_group_name:-"${ec2_name}"}
            local sec_group_id
            sec_group_id=$(aws ec2 describe-security-groups --filters "Name=group-name,Values=${sec_group_name}" --query 'SecurityGroups[*].[GroupId]' --output text)

            if [[ -z $(aws ec2 describe-security-groups --filters "Name=group-id,Values=${sec_group_id}" "Name=ip-permission.protocol,Values=tcp" "Name=ip-permission.from-port,Values=22" "Name=ip-permission.to-port,Values=22" "Name=ip-permission.cidr,Values=${user_login_ip}/32" --query 'SecurityGroups[].GroupId' --output text) ]]; then
               aws ec2 authorize-security-group-ingress --group-id "${sec_group_id}" --ip-permissions IpProtocol=tcp,FromPort=22,ToPort=22,IpRanges='[{CidrIp='"${user_login_ip}"'/32,Description="SSH for login place"}]'
            fi

            ;;
        create|c )
            local instance_state
            instance_state=$(aws ec2 describe-instances --filters "Name=tag:Name,Values=${instance_name}" --query 'Reservations[].Instances[].[State.Name]' --output text)

            if [[ -n "${instance_state}" && "${instance_state}" != 'terminated' ]]; then
                echo "${c_red}Attention${c_normal}: exist instance with name ${c_red}${instance_name}${c_normal}."
                # aws ec2 describe-instances --query "Reservations[].Instances[].[InstanceId,PublicDnsName,State.Name]" --filters "Name=tag:Name,Values=${instance_name}" --output table

                aws ec2 describe-instances --query "Reservations[].Instances[]|[?State.Name!='terminated']|[*].{InstanceId:InstanceId,LaunchTime:LaunchTime,State:State.Name,Tag_Name:Tags[?Key=='Name'].Value | [0]}" --output table
                fnBase_ExitStatement ''
            fi

            local subnet_name
            subnet_name=${subnet_name:-"${ec2_name}"}
            local sec_group_name
            sec_group_name=${sec_group_name:-"${ec2_name}"}

            local subnet_id
            subnet_id=$(aws ec2 describe-subnets --filters "Name=tag:Name,Values=${subnet_name}" --query 'Subnets[].SubnetId' --output text)
            # aws ec2 describe-subnets --query 'Subnets[?Tags[0].Value==`'"${${subnet_name}}"'`].[SubnetId]' --output text

            local sec_group_id
            sec_group_id=$(aws ec2 describe-security-groups --filters "Name=group-name,Values=${sec_group_name}" --query 'SecurityGroups[*].[GroupId]' --output text)
            # aws ec2 describe-security-groups --query 'SecurityGroups[?GroupName==`'"${sec_group_name}"'`].[GroupId]' --output text

            if [[ -z $(aws ec2 describe-security-groups --filters "Name=group-id,Values=${sec_group_id}" "Name=ip-permission.protocol,Values=tcp" "Name=ip-permission.from-port,Values=22" "Name=ip-permission.to-port,Values=22" "Name=ip-permission.cidr,Values=${user_login_ip}/32" --query 'SecurityGroups[].GroupId' --output text) ]]; then
               aws ec2 authorize-security-group-ingress --group-id "${sec_group_id}" --ip-permissions IpProtocol=tcp,FromPort=22,ToPort=22,IpRanges='[{CidrIp='"${user_login_ip}"'/32,Description="SSH for living room"}]'
            #    aws ec2 revoke-security-group-ingress --group-id "${sec_group_id}" --ip-permissions IpProtocol=tcp,FromPort=22,ToPort=22,IpRanges='[{CidrIp='"${user_login_ip}"'/32}]'
            fi

            local device_path=${device_path:-}
            local ami_id=${ami_id:-}

            [[ -n "${ami_name}" ]] || ami_name='u'
            case "${ami_name,,}" in
                rhel|redhat|r )
                    # ami_id='ami-0b500ef59d8335eee' # Red Hat Enterprise Linux 7.6
                    ami_id='ami-0520e698dd500b1d1' # Red Hat Enterprise Linux 8
                    ami_name='rhel'
                    ;;
                sles|suse|s )
                    ami_id='ami-0e0bae59dc35fe89a' # SUSE Linux Enterprise Server 15
                    ami_name='sles'
                    ;;
                amzn2|amzn|amazon|a )
                    ami_id='ami-00c03f7f7f2ec15c3' # Amazon Linux 2 AMI
                    ami_name='amzn'
                    ;;
                ubuntu|u|* )
                    ami_id='ami-052a6e77572eba9a9' # Ubuntu Server 18.04 LTS
                    ami_name='ubuntu'
                    ;;
            esac

            # Get latest free tier eligible ami id
            local l_ami_id_latest=''
            l_ami_id_latest=$($download_method "${ami_list_latest_link}" 2> /dev/null | sed -r -n '/^-+/,${/\|[[:space:]]+'"${ami_name}"'/{s@.*(ami-.*)$@\1@g;p;q}}')
            # sed -r -n '/^-+/,${/\|[[:space:]]+ubuntu/{s@.*(ami-.*)$@\1@g;p;q}}'
            [[ -n "${l_ami_id_latest}" && "${l_ami_id_latest}" =~ ^ami- ]] && ami_id="${l_ami_id_latest}"


            # For SUSE, RHEL, Ubuntu
            # device_path=${device_path:-'/dev/sda1'}
            # ami_id=${ami_id:-'ami-0c55b159cbfafe1f0'} # Ubuntu /dev/sda1
            # For Amazon AMI (amzn) /dev/xvda
            # ami_id=${ami_id:-'ami-02bcbb802e03574ba'}

            # aws ec2 describe-images --filter "Name=image-id,Values=${ami_id}"
            device_path=$(aws ec2 describe-images --filter "Name=image-id,Values=${ami_id}" --query 'Images[*].BlockDeviceMappings[0].DeviceName' --output text)

            local available_zone
            available_zone=$(aws ec2 describe-subnets --filters "Name=subnet-id,Values=${subnet_id}" --query 'Subnets[].AvailabilityZone' --output text)

            # By default, user data scripts and cloud-init directives run only during the boot cycle when you first launch an instance. Scripts entered as user data are executed as the root user, so do not use the sudo command in the script.
            # https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/user-data.html#user-data-api-cli
           # --user-data file://$PAHT

            # normal_user_home=$(find /home -maxdepth 1 -type d ! -path /home -print | head -n1)
            # normal_user=$(sed -r -n '/'"${normal_user_home//\//\\/}"'/{p;q}' /etc/passwd | cut -d: -f1)
            # download_method='wget -qO-'
            # [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && download_method='curl -fsL'
            # https://gitlab.com/axdsop/nixnotes/raw/master/Script/Toolkits/_Base/axdsop_boost_tool.sh

            local instance_id_new
            instance_id_new=$(aws ec2 run-instances --image-id "${ami_id}" --count 1 --instance-type t2.micro --placement AvailabilityZone="${available_zone}" --block-device-mappings DeviceName="${device_path}",Ebs={VolumeSize=30}  --key-name "${keypair_name}" --security-group-ids "${sec_group_id}" --subnet-id "${subnet_id}" --associate-public-ip-address --output text | sed -r -n '/^INSTANCES/{s@.*(i-[^[:space:]]+).*$@\1@g;p}')

            aws ec2 create-tags --resources "${instance_id_new}" --tags 'Key=Name,Value='"${instance_name}"''

            # The valid values are: 0 (pending), 16 (running), 32 (shutting-down), 48 (terminated), 64 (stopping), and 80 (stopped).
            # The state of the instance (pending | running | shutting-down | terminated | stopping | stopped ).

            aws ec2 describe-instances --query 'Reservations[].Instances[].[InstanceId,PublicDnsName,State.Name]' --filters "Name=instance-id,Values=${instance_id_new}" --output table
            ;;
        delete|del|d )
            instance_id=$(aws ec2 describe-instances --query "Reservations[].Instances[]|[?State.Name!='terminated']|[*].[InstanceId]" --filters "Name=tag:Name,Values=${instance_name}" --output text)

            if [[ -n "${instance_id}" ]]; then
                aws ec2 describe-instances --query "Reservations[].Instances[]|[?State.Name!='terminated']|[*].[InstanceId,PublicDnsName,State.Name]" --filters "Name=tag:Name,Values=${instance_name}" --output table
            else
                fnBase_ExitStatement "${c_red}Attention${c_normal}: no running instance with name ${c_red}${instance_name}${c_normal}."
            fi

            aws ec2 terminate-instances --instance-ids "${instance_id}" 1> /dev/null
            aws ec2 delete-tags --resources "${instance_id}" --tags Key=Name

            echo "Instance ${c_red}${instance_id}${c_normal} status is ${c_red}$(aws ec2 describe-instances --filter "Name=instance-id,Values=${instance_id}" --query 'Reservations[].Instances[].[State.Name]' --output text)${c_normal}."
            ;;
        * )
            fnBase_ExitStatement "${c_red}Error${c_normal}: please specify service action with '-a'!"
            ;;
    esac
}


#########  4. Executing Process  #########
fn_ExecutingProcess(){
    fn_InitializationCheck

    if [[ "${insatll_update_operation}" -eq 1 ]]; then
        local sudo_parse=${sudo_parse:-''}
        [[ "${login_user}" != 'root' ]] && sudo_parse='sudo'
        # ${download_method} https://raw.githubusercontent.com/MaxdSre/AWS-CLI-Learning-Notes/master/script/python/awscli_bundle.py | ${sudo_parse} python3 -
        temp_file_path=$(mktemp -t XXXXXX)
        ${download_method} "${awscli_bundle_python_script_link}" > "${temp_file_path}"
        # local l_python_name='python'
        # fnBase_CommandExistIfCheck 'python3' && l_python_name='python3'
        ${sudo_parse} python3 "${temp_file_path}"
        [[ -f "${temp_file_path}" ]] && rm -f "${temp_file_path}"
    fi

    fn_AWS_ConfigPath_Detection

    if [[ "${rotation_operation}" -eq 1 ]]; then
        fn_AWS_RotatingAccessKeys
        fn_AWS_SSHKeyPair_Generation "${keypair_name}" "${keypair_path}"
    fi

    case "${service_name,,}" in
        vpc|v )
            fn_AWS_VPC_Operation "${common_name}" "${action_name}"
            ;;
        ec2|e )
            fn_AWS_EC2_Operation "${common_name}" "${action_name}"
            ;;
    esac
}
fn_ExecutingProcess


#########  5. EXIT Singal Processing  #########
# trap "commands" EXIT # execute command when exit from shell
fn_TrapEXIT(){
    fnBase_UnsetVars
    unset insatll_update_operation
    unset rotation_operation
    unset common_name
    unset keypair_name
    unset keypair_path
    unset service_name
    unset action_name
    unset ami_name
}

trap fn_TrapEXIT EXIT

# Script End
