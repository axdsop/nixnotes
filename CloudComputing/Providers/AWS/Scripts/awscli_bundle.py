#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Usage: Installing/Updating aws cli via bundle package on GNU/Linux.
# Developer: MaxdSre

# Change Log:
# - Oct 19, 2019 21:15 Sat ET - check code style via flake8 lint
# - Oct 16, 2019 16:13 Wed ET - add bashrc path ~/.config/axdsop/bashrc
# - Sep 26, 2019 20:10 Thu ET - add latest release version change log url
# - Apr 24, 2019 10:15 Wed ET - add exit operation if fail to extract online latest release version of aws cli
# - Mar 27, 2019 14:45 Wed ET - add color for output, user privilege check
# - Dec 23, 2018 16:47 Sun ET


import os
from sys import exit
import re
from urllib.request import urlopen, urlretrieve
import subprocess
from shutil import rmtree
from zipfile import ZipFile

aws_cli_release_log = 'https://raw.githubusercontent.com/aws/aws-cli/master/CHANGELOG.rst'
# release page https://github.com/aws/aws-cli/releases
aws_cli_download_link = 'https://s3.amazonaws.com/aws-cli/awscli-bundle.zip'

pack_name = aws_cli_download_link.split('/')[-1]
save_path = '/tmp/' + pack_name
decompress_path = '/tmp/' + pack_name.split('.')[0]
target_path = '/opt/aws'
aws_bin_path = target_path + '/bin/aws'
aws_symbolic_path = '/usr/local/bin/aws'
is_need_continue = True
action_name = 'install'


def version_detection(path=aws_symbolic_path):
    version_info = subprocess.getoutput(path + ' --version')
    version_no = version_info.split(' ')[0].split('/')[-1]
    return version_no


def file_deletion(path=''):
    if path and os.path.exists(path):
        if os.path.isfile(path):
            os.remove(path)
        elif os.path.isdir(path):
            rmtree(path)


# - user privilege check
if os.geteuid() != 0:
    exit('Sorry, please run this script as \x1b[31;1m{}\x1b[0m.'.format('root'))

# - online latest relpylintEnabledease version
raw_data = urlopen(aws_cli_release_log).read().decode()
# https://stackoverflow.com/questions/38579725/return-string-with-first-match-regex#answer-38579883
# latest_release_version = re.findall("^\d+\..*$",raw_data,re.MULTILINE)[0]
latest_release_version = re.search(r"^\d+\..*$", raw_data, re.MULTILINE).group()

# re.match("^\d+\..\d+\..*$",latest_release_version).group()
if re.match(r"^\d+\..\d+\..*$", latest_release_version):
    # print('Latest release version online is \x1b[31;1m{}\x1b[0m.'.format(latest_release_version))

    print('Latest release version online is \x1b[31;1m{}\x1b[0m (\x1b[33m{}\x1b[0m).'.format(latest_release_version, 'https://github.com/aws/aws-cli/blob/develop/CHANGELOG.rst#' + latest_release_version.replace('.', '')))
else:
    exit('Sorry, fail to extract latest release version.')

# - check if need to install/update
if os.path.isdir(target_path) and os.path.isfile(aws_bin_path):
    # version comparasion
    local_version = version_detection(aws_symbolic_path)
    if local_version == latest_release_version:
        is_need_continue = False
        print('\n\x1b[31;1m{}\x1b[0m: local version existed is up to date.'.format('Note'))
    else:
        action_name = 'update'
        print('Local version existed is \x1b[31;1m{}\x1b[0m.'.format(local_version))

if is_need_continue is True:
    # color red
    print('\nOperation type is \x1b[31;3m{}\x1b[0m.'.format(action_name.upper()))
    file_deletion(save_path)
    file_deletion(decompress_path)
    file_deletion(target_path)

    # download package
    print('Procedure - \x1b[34;4m{}\x1b[0m ...'.format('download'))
    urlretrieve(aws_cli_download_link, save_path)

    if os.path.isfile(save_path):
        with ZipFile(save_path, "r") as zip_ref:
            zip_ref.extractall('/tmp')
            print('Procedure - \x1b[34;4m{}\x1b[0m ...'.format('decompress'))

    install_script_path = decompress_path + '/install'

    if os.path.exists(install_script_path) and os.path.isfile(install_script_path):
        print('Procedure - \x1b[34;4m{}\x1b[0m ...'.format(action_name))
        operation = subprocess.Popen(
            ['sudo', 'python3', install_script_path, '-i', target_path, '-b', aws_symbolic_path],
            stdout=subprocess.PIPE,)
        operation_output = operation.communicate()[0].decode('utf-8')
        # operation_output = sudo_operation.stdout
        # operation_output.read()

    if os.path.isfile(aws_bin_path):
        # check newly installed version , comparesion version
        new_install_version = version_detection(aws_bin_path)
        if new_install_version == latest_release_version:
            # ~/.bashrc add 'complete -C'
            aws_completer_path = target_path + '/bin/aws_completer'
            # os.getuid(), os.getlogin()
            user_home = os.getenv('HOME')

            if os.path.isfile(aws_completer_path):
                bashrc_path = user_home + '/.config/axdsop/bashrc'
                if os.path.isfile(bashrc_path):
                    subprocess.call(['sed', '-r', '-i', '/_awscli_operation/{s@^#?[[:space:]]*@@g}', bashrc_path])
                    subprocess.call(['sed', '-r', '-i', '/aws operation start/,/aws operation end/{/complete -C/{s@^#[[:space:]]*@@g;s@AWS_COMPLETER_PATH@' + aws_completer_path + '@g;}}', bashrc_path])
                else:
                    bashrc_path = user_home + '/.bashrc'
                    if os.path.isfile(bashrc_path):
                        # remove existed line
                        subprocess.call(['sed', '-i', '/.*aws_completer.*/d', bashrc_path])
                        # append to the end of file
                        with open(bashrc_path, 'a') as f:
                            f.write("complete -C '" + aws_completer_path + "' aws\n")

            # remove temporary files
            file_deletion(save_path)
            file_deletion(decompress_path)

            print('\nAWS cli is \x1b[31;4m{}\x1b[0m to version \x1b[31;1m{}\x1b[0m.'.format(action_name, latest_release_version))


# Script End
