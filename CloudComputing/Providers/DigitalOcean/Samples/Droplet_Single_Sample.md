# DigitalOcean Single Droplet Deployment Sample

Try to deploy droplet via vpc, cloud firewall, tag.

## TOC

1. [Single Droplet](#single-droplet)  
1.1 [Initialization](#initialization)  
1.2 [Deployment](#deployment)  
1.3 [Check Info](#check-info)  
1.4 [Update firewall rule](#update-firewall-rule)  
1.5 [Delete](#delete)  
1.5.1 [Method 1 - Manually](#method-1---manually)  
1.5.2 [Method 2 - Custom Function](#method-2---custom-function)  
2. [Change Log](#change-log)  


## Single Droplet

I wrote a Shell script [`droplet_single_deploy.sh`](../Scripts/droplet_single_deploy.sh) which implements the latest operating commands.

### Initialization

Variables definition

```sh
prefix_type_name='dev'
region_slug='nyc3'   # doctl compute region list

# VPC
vpc_name="${prefix_type_name}-vpc-${region_slug}"
vpc_description="Development ${prefix_type_name} action on region ${region_slug}."
vpc_ip_range='192.168.8.0/24' # IP size 2^8=256

# Cloud Firewalls
firewall_name="${prefix_type_name}-firewall-${region_slug}"
public_ip=$(curl ipinfo.io/ip 2> /dev/null) # https://ipapi.co/ip
inbound_rule_default="protocol:tcp,ports:22,address:${public_ip}/32" # allow any IPv4 to visit TCP/22
outbound_rule_default='protocol:icmp,address:0.0.0.0/0 protocol:tcp,ports:all,address:0.0.0.0/0 protocol:udp,ports:all,address:0.0.0.0/0' # allow icmp, outbound to any IPv4

# Tag
tag_name="${prefix_type_name}-tag-${region_slug}"

# Droplet
image_slug='ubuntu-20-10-x64'  # doctl compute image list-distribution
size_slug='s-1vcpu-2gb'  # doctl compute size list
# ssh_key_slug='29391948'  # doctl compute ssh-key list
user_data_file=$(mktemp -t doctl_XXXXXXXXX)

# SSH Key
ssh_key_slug=$(doctl compute ssh-key list --no-header --format="ID" | head -n 1)
# doctl compute ssh-key list --no-header | head -n 1 | cut -d' ' -f1
# doctl compute ssh-key list --no-header | head -n 1 | sed -r -n 's@.*[[:space:]]+([^[:space:]]+)[[:space:]]*$@\1@g;p'  # SSH key fingerprints
```

Init script

```sh
sys_init_script_link='https://gitlab.com/axdsop/nixnotes/raw/master/CloudComputing/DigitalOcean/Scripts/doctl_sys_init.sh'
echo -e "#!/usr/bin/env bash\ncurl -fsL ${sys_init_script_link} > /root/sys_init.sh\nbash /root/sys_init.sh" >"${user_data_file}"
# this script consumes time about 5min (14:35:20~14:40:45)
```

### Deployment

Create vpc, cloud firewall, tag, droplet. Applying cloud firewall rules to droplet via tag.

```sh
# - VPC Create
vpc_id=$(doctl vpcs create --name "${vpc_name}" --region "${region_slug}" --description "${vpc_description}" --ip-range "${vpc_ip_range}" --output=json | sed -r -n '/"id"/{s@[^:]*:[[:space:]]*"([^"]*)".*@\1@g;p}')
# change default state from 'false' to 'true', one region just has one active VPC at the same time
doctl vpcs update "${vpc_id}" --default 'true' --output=json # 1> /dev/null
# doctl vpcs get "${vpc_id}" --format="Name,IPRange,Region,Default"

# - Cloud Firewall Create
firewall_uuid=$(doctl compute firewall create --name "${firewall_name}" \
  --inbound-rules "${inbound_rule_default}" \
  --outbound-rules "${outbound_rule_default}" \
  --no-header --format="ID"
  )
# doctl compute firewall get "${firewall_uuid}" --format="ID,Name,Status"

# - Tag Create
doctl compute tag create "${tag_name}"  # --output=json

# - Droplet Create
droplet_name="${region_slug}-${image_slug%%-*}-01" # nyc3-ubuntu-01

droplet_id=$(doctl compute droplet create "${droplet_name}" \
    --region "${region_slug}" \
    --image "${image_slug}" \
    --size "${size_slug}" \
    --ssh-keys "${ssh_key_slug}" \
    --vpc-uuid "${vpc_id}" \
    --user-data-file "${user_data_file}" \
    --no-header --format="ID"
    ) # --wait consumes time about 10s
# --wait   Wait for Droplet creation to complete before returning
# --enable-backups \ --enable-ipv6 \ --enable-monitoring \ --enable-private-networking
# --tag-name string      A tag name to be applied to the Droplet
# --tag-names strings    A list of tag names to be applied to the Droplet
# --user-data string       User-data to configure the Droplet on first boot
# --user-data-file string  The path to a file containing user-data to configure the Droplet on first boot


# doctl compute droplet get "${droplet_id}" --output=json

[[ -f "${user_data_file}" ]] && rm -f "${user_data_file}"

# Get droplet public ip
doctl compute droplet get "${droplet_id}" --no-header --format="PublicIPv4"

# - Tag droplet (optional, also can specify parameter '--tag-names' in 'doctl compute droplet create')
doctl compute droplet tag "${droplet_id}" --tag-name "${tag_name}" # tag a droplet, use droplet id or name

# - Add droplet to Cloud Firewalls
# doctl compute firewall add-droplets "${firewall_uuid}" --droplet-ids "${droplet_id}"  # method 1 - Add droplet to cloud firewall
doctl compute firewall add-tags "${firewall_uuid}" --tag-names "${tag_name}"  # method 2 - Add droplet via tag to cloud firewall
```

### Check Info

```sh
# check vpc info
doctl vpcs get "${vpc_id}" --format="Name,IPRange,Region,Default"

# check tag info
doctl compute tag list

# check cloud firewall info
doctl compute firewall list-by-droplet "${droplet_id}" --output=json

# check droplet info
doctl compute droplet get "${droplet_id}" --format="ID,Name,Public IPv4,Private IPv4,Region,Image,Tags,Status"
```

### Update firewall rule

Update cloud firewall inbound rule

```sh
public_ip_new=$(curl ipinfo.io/ip 2> /dev/null) # https://ipapi.co/ip
inbound_rule_add="protocol:tcp,ports:22,address:${public_ip_new}/32"

# remove old public ip
inbound_rule_remove=$(doctl compute firewall get "${firewall_uuid}" --no-header --format="InboundRules")
doctl compute firewall remove-rules "${firewall_uuid}" \
  --inbound-rules "${inbound_rule_remove}" --output=json

# add new public ip
doctl compute firewall add-rules "${firewall_uuid}" \
  --inbound-rules "${inbound_rule_add}" --output=json

# doctl compute firewall get "${firewall_uuid}" --output=json
```

### Delete

Delete resources created

#### Method 1 - Manually

```sh
# Delete order: droplet, firewall, tag, vpc

# - Delete a droplet
# doctl compute droplet list --format="ID,Name,Public IPv4,Region,Image,Status"
doctl compute droplet delete "${droplet_id}"
# Warning: Are you sure you want to delete this Droplet? (y/N) ?

# - Delete a cloud firewall
# doctl compute firewall remove-droplets "${firewall_uuid}" --droplet-ids "${droplet_id}"  # method 1 - remove droplet from cloud firewall 
doctl compute firewall remove-tags "${firewall_uuid}" --tag-names "${tag_name}"    # method 2 - remove droplet via tag from cloud firewall 

doctl compute firewall delete "${firewall_uuid}"
# Warning: Are you sure you want to delete this firewall? (y/N) ? y

# - Delete a tag
doctl compute tag delete "${tag_name}"
# doctl compute tag list

# - Delete a vpc
# mark default vpc as true
origin_default_vpc_id=$(doctl vpcs list --no-header --format="ID,Name,Region,Default" | sed -r -n '/default-'"${region_slug}"'/{s@^([^[:space:]]+).*@\1@g;p;q}')
doctl vpcs update "${origin_default_vpc_id}" --default true --output=json 1> /dev/null


# doctl vpcs get $vpc_id --format="Name,IPRange,Region,Default"
doctl vpcs delete "${vpc_id}"
# Warning: Are you sure you want to delete this VPC? (y/N) ?

doctl vpcs list --format="Name,IPRange,Region,Default"
```

#### Method 2 - Custom Function

Via Custom function

<details><summary>Click to expand function code</summary>

```sh
fn_Droplet_Single_Remove(){
    local l_droplet_name=${1:-}
    local l_droplet_info=''
    echo 'Existed droplets detecting...'
    l_droplet_info=$(doctl compute droplet list --no-header --format="ID,Name,VPCUUID,Tags,Region" | sed -r -n 's@[[:space:]]+@\|@g;p')
    # 228101056|nyc3-ubuntu-01|d8da895d-c5ca-4b37-99c8-b58390909c97|dev-tag-nyc3|ny3
    local l_droplet_count=0
    l_droplet_count=$(sed '/^$/d' <<< "${l_droplet_info}" | wc -l)

    if [[ "${l_droplet_count}" -gt 0 ]]; then
        # - Check if specified droplet name existed
        [[ -n "${l_droplet_name}" && -n $(sed -r -n '/[[:space:]]+'"${l_droplet_name}"'[[:space:]]+/{p}' <<< "${l_droplet_info}") ]] || l_droplet_name=''

        # - If just has one droplet, extract droplet name instead of via selection menu
        [[ "${l_droplet_count}" -eq 1 ]] && l_droplet_name=$(cut -d\| -f 2  <<< "${l_droplet_info}")

        # - Without droplet name, via selection menu
        if [[ -z "${l_droplet_name}" ]]; then
            local IFS_BAK=${IFS_BAK:-"$IFS"}  # Backup IFS
            IFS="|" # Setting temporary IFS

            echo -e "\nAvailable Droplet List: "
            PS3="Choose droplet number(e.g. 1, 2,...): "

            select item in $(cut -d\| -f 2,5  <<< "${l_droplet_info}" | sed -r -n 's@([^\|]+)\|(.*)@\1 (\2)@g;p' | sort -r | sed ':a;N;$!ba;s@\n@|@g'); do
                l_droplet_name="${item}"
                [[ -n "${l_droplet_name}" ]] && break
            done < /dev/tty

            IFS=${IFS_BAK}  # Restore IFS
            unset IFS_BAK
            unset PS3
            printf "\nDroplet you choose is %s.\n\n" "${l_droplet_name}"
            l_droplet_name="${l_droplet_name%% *}"
        fi

        # -Droplet resources delete action
        if [[ -n "${l_droplet_name}" ]]; then
            # doctl compute droplet list --no-header --format="ID,Name,VPCUUID,Tags" | sed -r -n '/[[:space:]]+'"${l_droplet_name}"'[[:space:]]+/{s@[[:space:]]+@\|@g;p}'
            sed -r -n '/\|'"${l_droplet_name}"'\|/{p}' <<< "${l_droplet_info}" | while IFS="|" read -r droplet_id droplet_name vpc_id tag_name region_name; do
                # droplet_id=$(cut -d\| -f1 <<< "${droplet_info}")
                # droplet_name=$(cut -d\| -f2 <<< "${droplet_info}")
                # vpc_id=$(cut -d\| -f3 <<< "${droplet_info}")
                # tag_name=$(cut -d\| -f4 <<< "${droplet_info}")

                firewall_uuid=$(doctl compute firewall list-by-droplet "${droplet_id}" --no-header --format="ID")

                # Delete order: droplet, firewall, tag, vpc

                # - Delete a droplet
                # doctl compute droplet list --format="ID,Name,Public IPv4,Region,Image,Status"
                echo -e 'Droplet deleting ...'
                doctl compute droplet delete "${droplet_id}" --force
                # Warning: Are you sure you want to delete this Droplet? (y/N) ?

                # - Delete a cloud firewall
                echo -e 'Cloud firewall deleting ...'
                # doctl compute firewall remove-droplets "${firewall_uuid}" --droplet-ids "${droplet_id}"  # method 1 - remove droplet from cloud firewall
                doctl compute firewall remove-tags "${firewall_uuid}" --tag-names "${tag_name}"    # method 2 - remove droplet via tag from cloud firewall
                doctl compute firewall delete "${firewall_uuid}" --force
                # Warning: Are you sure you want to delete this firewall? (y/N) ? y

                # - Delete a tag
                echo -e 'Tag deleting ...'
                doctl compute tag delete "${tag_name}" --force
                # doctl compute tag list

                # - Delete a vpc
                echo -e 'VPC deleting ...'
                # mark default vpc as true
                local l_origin_default_vpc_info=${l_origin_default_vpc_info:-}
                # origin_default_vpc_id=$(doctl vpcs list --no-header --format="ID,Name,Region,Default" | sed -r -n '/default-'"${region_name}"'/{s@^([^[:space:]]+).*@\1@g;p;q}')
                l_origin_default_vpc_info=$(doctl vpcs list --no-header --format="ID,Name,Region,Default" | sed -r -n 's@[[:space:]]{2,}@\|@g;p' | sed -r -n '/default[^\|]+\|'"${region_name}"'\|/{p;q}')

                if [[ -n "${l_origin_default_vpc_info}" ]]; then
                    [[ $(cut -d\| -f 4 <<< "${l_origin_default_vpc_info}") != 'true' ]] && doctl vpcs update $(cut -d\| -f 1 <<< "${l_origin_default_vpc_info}") --default true --output=json 1> /dev/null
                fi
                # doctl vpcs get $vpc_id --format="Name,IPRange,Region,Default"
                sleep 2  # Error: DELETE https://api.digitalocean.com/v2/vpcs/1ed6d4b6-42e1-4a27-b380-99100baa8ffb: 403 (request "21c24c71-08c9-43a3-bf15-da20a156b319") Can not delete VPC with members
                doctl vpcs delete "${vpc_id}" --force
                # Warning: Are you sure you want to delete this VPC? (y/N) ?

                # doctl vpcs list --format="Name,IPRange,Region,Default"
            done
        fi
    else
        echo 'No droplet detects.'
    fi
}
```

</details>

Invoking custom function, specify droplet name manually or choose from selection menu.

```sh
# specify droplet name
fn_Droplet_Single_Remove "${droplet_name}"  # nyc3-ubuntu-01

# via selection menu
fn_Droplet_Single_Remove
```

<details><summary>Click to expand selection menu</summary>

```sh
Available Droplet List:
1) nyc3-ubuntu-01 (nyc3)
Choose droplet number(e.g. 1, 2,...): 1

Droplet you choose is nyc3-ubuntu-01 (nyc3).
```

</details>


## Change Log

* Jan 24, 2021 14:58 Sun ET
  * First draft
* Jan 24, 2021 21:00 Sun ET
  * Add droplet remove custom function
* Jan 25, 2021 16:32 Mon ET
  * single deployment script rewrite, add help info and parameter specification
* Feb 01, 2021 12:48 Mon ET
  * fix variables initial issue, optimize vpc create/remove operating procedure


<!-- End -->