# Trinary Droplets Deployment Sample

Setting up a jumping server, a web server, a backend server as reverse proxy.

## TOC

1. [Design](#design)  
1.1 [Shell Script](#shell-script)  
2. [Preparation](#preparation)  
2.1 [Variables Initialization](#variables-initialization)  
2.2 [VPC](#vpc)  
3. [Jump Server](#jump-server)  
3.1 [Variables Definition](#variables-definition)  
3.2 [Deploying](#deploying)  
4. [Web Server](#web-server)  
4.1 [Variables Definition](#variables-definition-1)  
4.2 [Deploying](#deploying-1)  
5. [Backend](#backend)  
5.1 [Variables Definition](#variables-definition-2)  
5.2 [Deploying](#deploying-2)  
6. [Delete Operation](#delete-operation)  
6.1 [Custom Function](#custom-function)  
7. [Change Log](#change-log)  


## Design

1. Jump server
    * CloudFirewall 1
        * Inbound rule: just allow SSH from controler's IP
    * CloudFirewall 2
        * Inbound rule: just allow SSH/ICMP from private IP of jump server (via tag)

2. Web server (frontend)

    * Add floating IP
    * CloudFirewall 1
        * Inbound rule: allow TCP port `80`, `443` from any IP
    * CloudFirewall 2 is jump server's CloudFirewall 2 (attach)

3. Backend Server (backend)
    * CloudFirewall
        * Outbound rule: just allow specific TCP port to *Web server*


### Shell Script

I wrote a Shell script [`droplet_trinary_deploy.sh`](../Scripts/droplet_trinary_deploy.sh) which implements the latest operating commands.


## Preparation

### Variables Initialization

Variables definition

```sh
ssh_key_directory="$HOME/Documents/KeyManagement/SSH/DigitalOcean"
sys_init_script_link='https://gitlab.com/axdsop/nixnotes/raw/master/CloudComputing/Providers/DigitalOcean/Scripts/doctl_sys_init.sh'
script_save_path="/root/.doctl_sys_init.sh"
# echo -e "#!/usr/bin/env bash\nwget -qO- ${sys_init_script_link} > ${script_save_path} || curl -fsL ${sys_init_script_link} > ${script_save_path}\nbash ${script_save_path}" > "${user_data_file}"

prefix_type_name='web'
suffix_for_jump='jump'
suffix_for_jump_control='control'
suffix_for_webserver='reverse'
suffix_for_backend='backend'

region_slug='nyc1'   # doctl compute region list

# Droplet
image_slug='debian-10-x64'  # doctl compute image list-distribution
size_slug='s-1vcpu-2gb'  # doctl compute size list
user_data_file=$(mktemp -t doctl_XXXXXXXXX)

# - VPC
vpc_name="${prefix_type_name}-${region_slug}-vpc"
vpc_description="VPC for ${prefix_type_name} on region ${region_slug}."
vpc_ip_range='192.168.8.0/24' # IP size 2^8=256
vpc_default_ip_range='10.118.0.0/20'

# - Cloud Firewalls
firewall_name="${prefix_type_name}-${region_slug}-firewall"

# - Tag
tag_name="${prefix_type_name}-${region_slug}-tag"

# SSH Key
ssh_name_jump='SSH Jump'
ssh_name_backend='SSH Backend'
```

### VPC

Creting VPC

```sh
# - check if default VPC for specific region exists
vpc_default_id=${vpc_default_id:-}
vpc_default_id=$(doctl vpcs list --no-header --format="ID,Name,Region,Default" | sed -r -n 's@[[:space:]]{2,}@\|@g;p' | sed -r -n '/\|default[^\|]+\|'"${region_slug}"'\|/{s@^([^\|]+).*@\1@g;p}')

# if not exists, create default VPC
if [[ -z "${vpc_default_id}" ]]; then
    doctl vpcs create --name "default-${region_slug}" --region "${region_slug}" --ip-range "${vpc_default_ip_range}" 1> /dev/null
fi

# - create specific VPC
vpc_id=${vpc_id:-}
vpc_id=$(doctl vpcs list --no-header --format="Name,ID" | sed -r -n 's@[[:space:]]{2,}@|@g; /^'"${vpc_name}"'\|/{s@^[^\|]+\|@@g;p}')
[[ -n "${vpc_id}" ]] || vpc_id=$(doctl vpcs create --name "${vpc_name}" --region "${region_slug}" --description "${vpc_description}" --ip-range "${vpc_ip_range}" --output=json | sed -r -n '/"id"/{s@[^:]*:[[:space:]]*"([^"]*)".*@\1@g;p}')
# solve error info: a VPC with the same name already exists in your account

# - Update default state of specific VPC
# change default state from 'false' to 'true', one region just has one active VPC at the same time
doctl vpcs update "${vpc_id}" --default 'true' --output=json 1> /dev/null
echo -e "\nVPC Info:"
doctl vpcs get "${vpc_id}" --format="Name,IPRange,Region,Default"
```

## Jump Server

To make ping working (ICMP), *jump* server outbound rule needs to add *protocol:icmp,address:0.0.0.0/0,address:::/0*, and *jump* control inbound rule needs to add *protocol:icmp,tag:${tag_name_jump}* or *protocol:icmp,address:0.0.0.0/0,address:::/0*.

### Variables Definition

Variables preparation

For *jump* server itself

```sh
tag_name_jump="${tag_name}-${suffix_for_jump}"
firewall_name_jump="${firewall_name}-${suffix_for_jump}"

public_ip=$(curl ipinfo.io/ip 2> /dev/null || curl http://ip-api.com/line/?fields=query 2> /dev/null) # https://ipapi.co/ip
inbound_rule_jump="protocol:tcp,ports:22,address:${public_ip}/32" # allow specific IPv4 to visit TCP/22
outbound_rule_jump='protocol:icmp,address:0.0.0.0/0,address:::/0 protocol:tcp,ports:all,address:0.0.0.0/0,address:::/0 protocol:udp,ports:all,address:0.0.0.0/0,address:::/0' # allow TCP/UDP outbound to any IPv4/IPv6
```

For servers controled by *jump* server (SSH/ICMP)

```sh
tag_name_jump_control="${tag_name_jump}-${suffix_for_jump_control}"
firewall_name_jump_control="${firewall_name_jump}-${suffix_for_jump_control}"
inbound_rule_jump_control="protocol:icmp,tag:${tag_name_jump} protocol:tcp,ports:22,tag:${tag_name_jump}" # allow specific IPv4 to visit ICMP & TCP/22 via tag
```

### Deploying

For *jump* server itself

```sh
# - Create Tag
doctl compute tag create "${tag_name_jump}" 1> /dev/null # --output=json
# doctl compute tag get "${tag_name_jump}"

# - Create Cloud firewall
doctl compute firewall list --no-header --format="ID,Name" | sed -r -n 's@[[:space:]]{2,}@|@g; /\|'"${firewall_name_jump}"'$/{s@^([^\|]+).*@\1@g;p}' | xargs -I % doctl compute firewall delete % -f  # delete existed firewall with same name

# Method 1
doctl compute firewall create --name "${firewall_name_jump}" \
--inbound-rules "${inbound_rule_jump}" \
--outbound-rules "${outbound_rule_jump}" \
--tag-names "${tag_name_jump}" \
--format="Name,Status,Tags,Created"

# Method 2
# firewall_uuid_jump=$(doctl compute firewall create --name "${firewall_name_jump}" \
# --inbound-rules "${inbound_rule_jump}" \
# --outbound-rules "${outbound_rule_jump}" \
# --no-header --format="ID"
# )
# # doctl compute firewall get "${firewall_uuid_jump}" --format="Name,ID,Status"
# doctl compute firewall add-tags "${firewall_uuid_jump}" --tag-names "${tag_name_jump}"  # add tag to Cloud Firewalls
```

For servers controled by *jump* server (SSH/ICMP)

```sh
# - Create Tag
doctl compute tag create "${tag_name_jump_control}" 1> /dev/null # --output=json
# doctl compute tag get "${tag_name_jump_control}"

# - Create Cloud firewall
doctl compute firewall create --name "${firewall_name_jump_control}" \
--inbound-rules "${inbound_rule_jump_control}" \
--tag-names "${tag_name_jump_control}" \
--format="Name,Status,Tags,Created"
```

Droplet deployment

```sh
# - SSH Key Choose
ssh_key_slug_jump=$(doctl compute ssh-key list --no-header --format="ID,Name" | sed -r -n 's@[[:space:]]{2,}@|@g; /\|'"${ssh_name_jump}"'$/{s@^([^\|]+).*@\1@g;p}')

# - User Data File
# echo -e "#!/usr/bin/env bash\nwget -qO- ${sys_init_script_link} > ${script_save_path} || curl -fsL ${sys_init_script_link} > ${script_save_path}\nbash ${script_save_path}" > "${user_data_file}"
# echo -e '#!/usr/bin/env bash\nwget -qO- '"${sys_init_script_link}"' > '"${script_save_path}"' || curl -fsL '"${sys_init_script_link}"' > '"${script_save_path}"'\nbash '"${script_save_path}"'' > "${user_data_file}" # can execute successfully in command line interface

echo -e '#!/usr/bin/env bash' > "${user_data_file}"
ssh_key_backend_data=$(cat ${ssh_key_directory}/id_ed25519_backend)
ssh_key_backend_data_public=$(cat ${ssh_key_directory}/id_ed25519_backend.pub)
echo -e '\nmkdir /root/.ssh\necho '"'${ssh_key_backend_data}'"' > /root/.ssh/id_ed25519\nchmod 600 /root/.ssh/id_ed25519\necho '"'${ssh_key_backend_data_public}'"' > /root/.ssh/id_ed25519.pub\nchmod 644 /root/.ssh/id_ed25519.pub' >> "${user_data_file}"
echo -e "\nwget -qO- ${sys_init_script_link} > ${script_save_path} || curl -fsL ${sys_init_script_link} > ${script_save_path}\nbash ${script_save_path}" >> "${user_data_file}"

# - Create Droplet
droplet_name_jump="${prefix_type_name}-${region_slug}-${image_slug%%-*}-${suffix_for_jump}"

droplet_id_jump=$(doctl compute droplet create "${droplet_name_jump}" \
    --region "${region_slug}" \
    --image "${image_slug}" \
    --size "${size_slug}" \
    --ssh-keys "${ssh_key_slug_jump}" \
    --vpc-uuid "${vpc_id}" \
    --user-data-file "${user_data_file}" \
    --no-header --format="ID"
    ) # --wait consumes time about 10s
# --tag-names strings           A list of tag names to be applied to the Droplet

# Tag droplet (optional, also can specify parameter '--tag-names' in 'doctl compute droplet create')
doctl compute droplet tag "${droplet_id_jump}" --tag-name "${tag_name_jump}" # tag a droplet, use droplet id or name
```

Droplet info

```sh
# doctl compute droplet get "${droplet_id_jump}" --format="ID,Name,PublicIPv4,PrivateIPv4,Region,Status"

droplet_info_jump=$(doctl compute droplet get "${droplet_id_jump}" --no-header --format="ID,PublicIPv4,PrivateIPv4,Region,Status" | sed -r -n 's@[[:space:]]{2,}@\|@g;p')
# 234414875|167.99.225.211|192.168.8.2|nyc1|active
droplet_private_ip_jump=$(cut -d\| -f3 <<< "${droplet_info_jump}")
```

Extracting private IP of *Jump* droplet.

```sh
droplet_private_ip_jump=${droplet_private_ip_jump:-}
droplet_private_ip_jump=$(doctl compute droplet list --no-header --format="ID,Name,Private IPv4,Region,Status" | sed -r -n 's@[[:space:]]{2,}@\|@g; /-'"${suffix_for_jump}"'\|/{p}' | cut -d\| -f3)  # web-nyc1-debian-jump

if [[ -n "${droplet_private_ip_jump}" ]]; then
    echo "Jump server private ip: ${droplet_private_ip_jump}"
else
    echo 'No jump droplet find.'
    exit
fi
```

## Web Server

Deploying a droplet along with floating ip as web server (Nginx). I write a note [Secure Nginx With Let's Encrypt Free SSL Certificate](/CyberSecurity/WebServer/Nginx/IssueSSLCert.md) to configure Nginx.

### Variables Definition

Variables preparation

```sh
tag_name_webserver="${tag_name}-${suffix_for_webserver}"

firewall_name_webserver="${firewall_name}-${suffix_for_webserver}"
inbound_rule_webserver="protocol:tcp,ports:80,address:0.0.0.0/0,address:::/0 protocol:tcp,ports:443,address:0.0.0.0/0,address:::/0" # allow any IPv4 to visit TCP/{80,443}
outbound_rule_webserver='protocol:tcp,ports:all,address:0.0.0.0/0,address:::/0 protocol:udp,ports:all,address:0.0.0.0/0,address:::/0' # allow outbound to any IPv4/IPv6
```

### Deploying

Deployment

```sh
# - Create Tag
doctl compute tag create "${tag_name_webserver}" 1> /dev/null # --output=json
# doctl compute tag get "${tag_name_webserver}"

# - Create Cloud firewall
doctl compute firewall list --no-header --format="ID,Name" | sed -r -n 's@[[:space:]]{2,}@|@g; /\|'"${firewall_name_webserver}"'$/{s@^([^\|]+).*@\1@g;p}' | xargs -I % doctl compute firewall delete % -f  # delete existed firewall with same name
# Method 1
doctl compute firewall create --name "${firewall_name_webserver}" \
--inbound-rules "${inbound_rule_webserver}" \
--outbound-rules "${outbound_rule_webserver}" \
--tag-names "${tag_name_webserver}" \
--format="Name,Status,Tags,Created"

# Method 2
# firewall_uuid_webserver=$(doctl compute firewall create --name "${firewall_name_webserver}" \
# --inbound-rules "${inbound_rule_webserver}" \
# --outbound-rules "${outbound_rule_webserver}" \
# --no-header --format="ID"
# )
# # doctl compute firewall get "${firewall_uuid_webserver}" --format="Name,ID,Status"
# # Add tag to Cloud Firewalls
# doctl compute firewall add-tags "${firewall_uuid_webserver}" --tag-names "${tag_name_webserver}"

# - SSH Key Choose
ssh_key_slug_backend=$(doctl compute ssh-key list --no-header --format="ID,Name" | sed -r -n 's@[[:space:]]{2,}@|@g; /\|'"${ssh_name_backend}"'$/{s@^([^\|]+).*@\1@g;p}')

# - User Data File
# echo -e "#!/usr/bin/env bash\nwget -qO- ${sys_init_script_link} > ${script_save_path} || curl -fsL ${sys_init_script_link} > ${script_save_path}\nbash ${script_save_path}" > "${user_data_file}"
# echo -e '#!/usr/bin/env bash\nwget -qO- '"${sys_init_script_link}"' > '"${script_save_path}"' || curl -fsL '"${sys_init_script_link}"' > '"${script_save_path}"'\nbash '"${script_save_path}"'' > "${user_data_file}" # can execute successfully in command line interface
echo -e '#!/usr/bin/env bash\n' > "${user_data_file}"
echo -e "wget -qO- ${sys_init_script_link} > ${script_save_path} || curl -fsL ${sys_init_script_link} > ${script_save_path}\nbash ${script_save_path} -w" >> "${user_data_file}" # install web server (Nginx) via '-w'

# - Create Droplet
droplet_name_webserver="${prefix_type_name}-${region_slug}-${image_slug%%-*}-${suffix_for_webserver}-01"  # 422 Only valid hostname characters are allowed. (a-z, A-Z, 0-9, . and -)

# droplet_id_webserver=$(doctl compute droplet create "${droplet_name_webserver}" \
#     --region "${region_slug}" \
#     --image "${image_slug}" \
#     --size "${size_slug}" \
#     --ssh-keys "${ssh_key_slug_backend}" \
#     --vpc-uuid "${vpc_id}" \
#     --user-data-file "${user_data_file}" \
#     --no-header --format="ID"
#     ) # --wait consumes time about 10s
# # --tag-names strings           A list of tag names to be applied to the Droplet

# # Tag droplet (optional, also can specify parameter '--tag-names' in 'doctl compute droplet create')
# doctl compute droplet tag "${droplet_id_webserver}" --tag-name "${tag_name_webserver}" # tag a droplet, use droplet id or name
# doctl compute droplet tag "${droplet_id_webserver}" --tag-name "${tag_name_jump_control}" # tag a droplet, use droplet id or name

droplet_id_webserver=$(doctl compute droplet create "${droplet_name_webserver}" \
    --region "${region_slug}" \
    --image "${image_slug}" \
    --size "${size_slug}" \
    --ssh-keys "${ssh_key_slug_backend}" \
    --vpc-uuid "${vpc_id}" \
    --user-data-file "${user_data_file}" \
    --tag-names "${tag_name_jump_control},${tag_name_webserver}" \
    --no-header --format="ID"
    ) # --wait consumes time about 10s
# --tag-names strings           A list of tag names to be applied to the Droplet

# - For Floating IPs
floating_ip=${floating_ip:-}
floating_ip=$(doctl compute floating-ip list --no-header --format="IP,Region,DropletID" | sed -r -n 's@[[:space:]]{2,}@|@g; /\|'"${region_slug}"'\|$/{s@^([^\|]+).*@\1@g;p}') # check if exist floating ip not allocate
[[ -n "${floating_ip}" ]] || floating_ip=$(doctl compute floating-ip create --region "${region_slug}" --no-header --format="IP")
sleep 2  # error: 422 Droplet already has a pending event.
doctl compute floating-ip-action assign "${floating_ip}" "${droplet_id_webserver}" # --output=json
# --format="ID,Status,Type,StartedAt,CompletedAt,ResourceID,ResourceType,Region"
```

Droplet info

```sh
echo -e "Floating IP: ${floating_ip}\n"

doctl compute droplet get "${droplet_id_webserver}" --format="ID,Name,PublicIPv4,PrivateIPv4,Region,Status,Tags" # --output=json
```

Extracting private IP of *webserver* droplet.

```sh
droplet_private_ip_webserver=${droplet_private_ip_webserver:-}
droplet_private_ip_webserver=$(doctl compute droplet list --no-header --format="ID,Name,Private IPv4,Region,Status" | sed -r -n 's@[[:space:]]{2,}@\|@g; /-'"${suffix_for_webserver}"'-/{p}' | cut -d\| -f3)  # web-nyc1-debian-webserver-01

if [[ -n "${droplet_private_ip_webserver}" ]]; then
    echo "Webserver server private ip: ${droplet_private_ip_webserver}"
else
    echo 'No webserver droplet find.'
    exit
fi
```

To check if remote host port is open, you can use command `nc` provided by utility `netcat`

```sh
# check if remote host port is open
sudo apt-get install -y netcat

nc -n -zv 192.168.8.4 8000 -w 2
# (UNKNOWN) [192.168.8.4] 8000 (?) open
# (UNKNOWN) [192.168.8.4] 8001 (?) : Connection refused
# (UNKNOWN) [192.168.8.4] 22 (ssh) : Connection timed out
```


## Backend

### Variables Definition

Variables preparation

```sh
tag_name_backend="${tag_name}-${suffix_for_backend}"

firewall_name_backend="${firewall_name}-${suffix_for_backend}"

# tag tag_name_jump_control has defined partial inbound rules
inbound_rule_backend="protocol:tcp,ports:8000-9000,tag:${tag_name_webserver}"
outbound_rule_backend='protocol:tcp,ports:all,address:0.0.0.0/0,address:::/0 protocol:udp,ports:all,address:0.0.0.0/0,address:::/0' # allow icmp, outbound to any IPv4/IPv6
```

### Deploying

Deployment

```sh
# - Create Tag
doctl compute tag create "${tag_name_backend}" 1> /dev/null # --output=json
# doctl compute tag get "${tag_name_backend}"

# - Create Cloud firewall
doctl compute firewall list --no-header --format="ID,Name" | sed -r -n 's@[[:space:]]{2,}@|@g; /\|'"${firewall_name_backend}"'$/{s@^([^\|]+).*@\1@g;p}' | xargs -I % doctl compute firewall delete % -f  # delete existed firewall with same name

firewall_uuid_backend=$(doctl compute firewall create --name "${firewall_name_backend}" \
--inbound-rules "${inbound_rule_backend}" \
--outbound-rules "${outbound_rule_backend}" \
--tag-names "${tag_name_jump_control},${tag_name_backend}" \
--no-header --format="ID"
)
# doctl compute firewall get "${firewall_uuid_backend}" --format="Name,ID,Status,Tags"

# - SSH Key Choose
ssh_key_slug_backend=$(doctl compute ssh-key list --no-header --format="ID,Name" | sed -r -n 's@[[:space:]]{2,}@|@g; /\|'"${ssh_name_backend}"'$/{s@^([^\|]+).*@\1@g;p}')

# - User Data File
# echo -e "#!/usr/bin/env bash\nwget -qO- ${sys_init_script_link} > ${script_save_path} || curl -fsL ${sys_init_script_link} > ${script_save_path}\nbash ${script_save_path}" > "${user_data_file}"
# echo -e '#!/usr/bin/env bash\nwget -qO- '"${sys_init_script_link}"' > '"${script_save_path}"' || curl -fsL '"${sys_init_script_link}"' > '"${script_save_path}"'\nbash '"${script_save_path}"'' > "${user_data_file}" # can execute successfully in command line interface
echo -e '#!/usr/bin/env bash\n' > "${user_data_file}"
echo -e "wget -qO- ${sys_init_script_link} > ${script_save_path} || curl -fsL ${sys_init_script_link} > ${script_save_path}\nbash ${script_save_path} -d" >> "${user_data_file}"  # install Docker via parameter '-d'

# - Create Droplet
droplet_name_backend="${prefix_type_name}-${region_slug}-${image_slug%%-*}-${suffix_for_backend}-01"  # 422 Only valid hostname characters are allowed. (a-z, A-Z, 0-9, . and -)

# size_slug_new="${size_slug}"
size_slug_new='s-2vcpu-4gb' # more memeory to run Docker

droplet_id_backend=$(doctl compute droplet create "${droplet_name_backend}" \
    --region "${region_slug}" \
    --image "${image_slug}" \
    --size "${size_slug_new}" \
    --ssh-keys "${ssh_key_slug_backend}" \
    --vpc-uuid "${vpc_id}" \
    --user-data-file "${user_data_file}" \
    --tag-names "${tag_name_jump_control},${tag_name_backend}" \
    --no-header --format="ID"
    ) # --wait consumes time about 10s

# Tag droplet (optional, also can specify parameter '--tag-names' in 'doctl compute droplet create')
# doctl compute droplet tag "${droplet_id_backend}" --tag-name "${tag_name_backend}" # tag a droplet, use droplet id or name

# droplet info
doctl compute droplet get "${droplet_id_backend}" --format="ID,Name,PublicIPv4,PrivateIPv4,Region,Status,Tags" # --output=json
```

Extracting private IP of *backend* droplet.

```sh
droplet_private_ip_backend=${droplet_private_ip_backend:-}
droplet_private_ip_backend=$(doctl compute droplet list --no-header --format="ID,Name,Private IPv4,Region,Status" | sed -r -n 's@[[:space:]]{2,}@\|@g; /-'"${suffix_for_backend}"'-/{p}' | cut -d\| -f3)  # web-nyc1-debian-backend-01

if [[ -n "${droplet_private_ip_backend}" ]]; then
    echo -e "Backend server private ip:\n${droplet_private_ip_backend}"
else
    echo 'No backend droplet find.'
    exit
fi
```


## Delete Operation

List all droplets

```sh
doctl compute droplet list --format="ID,Name,PublicIPv4,PrivateIPv4,Region,Status" # --output=json
```

### Custom Function

<details><summary>Click to expand custom function</summary>

```sh
# Keep floating ip by default

fn_Droplet_Delete(){
    local l_droplet_suffix="${1:-}"
    local l_delete_floatingip="${2:-0}" # 0 or 1

    # local l_is_continue=0

    # case "${l_droplet_suffix,,}" in
    #     webserver|jump|) l_is_continue=1 ;;
    # esac

    # - Getting droplet info
    droplet_infos=$(doctl compute droplet list --no-header --format="Region,ID,Name,VPCUUID,Tags,Volumes" 2> /dev/null | sed -r -n 's@[[:space:]]{2,}@|@g;p')
    # nyc1|234415989|web-nyc1-debian-webserver-01|d8f884f7-cf14-403b-a6d2-6a3ee6ed92a8|web-nyc1-tag-webserver|

    awk -F\| '$3~/-'"${l_droplet_suffix}"'-?([^\|]*)?/{print}' <<< "${droplet_infos}" | while read -r droplet_info; do
        region_name=$(cut -d\| -f1 <<< "${droplet_info}")
        droplet_id=$(cut -d\| -f2 <<< "${droplet_info}")
        droplet_name=$(cut -d\| -f3 <<< "${droplet_info}")
        vpc_id=$(cut -d\| -f4 <<< "${droplet_info}")
        tag_names=$(cut -d\| -f5 <<< "${droplet_info}")
        volume_ids=$(cut -d\| -f6 <<< "${droplet_info}")

        echo "=== Droplet ${droplet_name} Operation ==="

        # Delete order: volume, droplet, firewall, tag, floating_ip, [vpc]

        # - Detach & Delete block storage volume
        if [[ -n "${volume_ids}" ]]; then
            echo -e 'Block Storage Volume detaching ...'
            sed -r -n 's@,@\n@g;p' <<< "${volume_ids}" | while read -r volume_id; do
                doctl compute volume-action detach "${volume_id}" "${droplet_id}" --wait &> /dev/null # --output=json
                sleep 1
                echo '- deleting ...'
                doctl compute volume delete "${volume_id}" --force
            done
        fi

        floating_ip=${floating_ip:-}
        if [[ "${l_delete_floatingip}" -eq 1 ]]; then
            floating_ip=$(doctl compute floating-ip list --no-header --format="DropletID,IP" | sed -r -n 's@[[:space:]]{2,}@|@g; /^'"${droplet_id}"'\|/{s@^[^\|]+\|@@g;p}')
        fi

        # - Delete a droplet
        echo -e 'Droplet deleting ...'
        doctl compute droplet delete "${droplet_id}" --force
        # Warning: Are you sure you want to delete this Droplet? (y/N) ?
        sleep 1

        # - Delete tags
        # if tag dropletcount is 0, then delete it
        sed -r -n 's@,@\n@g;p' <<< "${tag_names}" | while read -r tag_name; do
            if [[ $(doctl compute tag get "${tag_name}" --no-header --format="DropletCount") -eq 0 ]]; then
                echo -e 'Tag deleting ...'
                doctl compute tag delete "${tag_name}" --force
                # doctl compute tag list
                sleep 1
            fi
        done

        doctl compute tag list --no-header --format="Name,DropletCount" | sed -r -n 's@[[:space:]]{2,}@|@g; /\|0$/{s@^([^\|]+).*@\1@g;p}' | xargs -I % doctl compute tag delete % -f

        # - Delete a cloud firewall
        doctl compute firewall list --no-header --format="ID,Name,Tags" | sed -r -n 's@[[:space:]]{2,}@|@g;p' | while IFS="|" read -r firewall_uuid firewall_name firewall_tags; do
            if [[ -z "${firewall_tags}" ]]; then
                echo -e 'Cloud firewall deleting ...'
                doctl compute firewall delete "${firewall_uuid}" --force
            fi
        done

        # - Delete Floating IP
        if [[ "${l_delete_floatingip}" -eq 1 ]]; then            
            # Error info: 422 Droplet already has a pending event.
            if [[ -n "${floating_ip}" ]]; then
                while true; do
                    if [[ -n $(doctl compute floating-ip get "${floating_ip}" --no-header --format="DropletID" 2> /dev/null) ]]; then
                        sleep 1
                    else
                        break
                    fi
                done

                doctl compute floating-ip delete "${floating_ip}" --force
                # Warning: Are you sure you want to delete this firewall? (y/N) ? y
            fi
        fi

        # - Delete VPC
        if [[ $(doctl compute droplet list --no-header --format="ID,Region,VPCUUID" | sed -r -n 's@[[:space:]]{2,}@|@g; /\|'"${vpc_id}"'$/{p}' | sed '/^$/d' | wc -l) -lt 1 ]]; then
            echo -e 'VPC deleting ...'
            # mark default vpc as true
            origin_default_vpc_info=${origin_default_vpc_info:-}
            origin_default_vpc_info=$(doctl vpcs list --no-header --format="ID,Name,Region,Default" | sed -r -n 's@[[:space:]]{2,}@\|@g; /default[^\|]+\|'"${region_name}"'\|/{p;q}')
            # a31b76b2-d161-40a4-abbc-034a8f164983|default-nyc1|nyc1|false

            if [[ -n "${origin_default_vpc_info}" ]]; then
                [[ $(cut -d\| -f 4 <<< "${origin_default_vpc_info}") != 'true' ]] && doctl vpcs update $(cut -d\| -f 1 <<< "${origin_default_vpc_info}") --default true --output=json 1> /dev/null
            fi
            # doctl vpcs get $vpc_id --format="Name,IPRange,Region,Default"
            sleep 2  # Error: DELETE https://api.digitalocean.com/v2/vpcs/1ed6d4b6-42e1-4a27-b380-99100baa8ffb: 403 (request "21c24c71-08c9-43a3-bf15-da20a156b319") Can not delete VPC with members
            doctl vpcs delete "${vpc_id}" --force
            # Warning: Are you sure you want to delete this VPC? (y/N) ?

            # doctl vpcs list --format="Name,IPRange,Region,Default"
        fi

    done
}
```

</details>

Execute example

```sh
# Supported suffix: jump, webserver, backend

# For jump
fn_Droplet_Delete 'jump'

# For webserver
fn_Droplet_Delete 'reverse'
# fn_Droplet_Delete 'reverse' 1  # remove floating ip

# For backend
fn_Droplet_Delete 'backend'
```


## Change Log

* Mar 01, 2021 09:33 ~ 21.15 Mon ET
  * First draft
* Mar 04, 2021 11:59 Thu ET
  * optimize droplet delete function
* Mar 27, 2021 21:50 Sat ET
  * Add deployment script


<!-- End -->