# Digital Ocean Tutorials

[DigitalOcean][digitalocean], Inc. is an American cloud infrastructure provider headquartered in New York City with data centers worldwide.

[doctl][doctl] is the official command line interface for the [DigitalOcean API][doapi].


## TOC

1. [Official Doc](#official-doc)  
1.1 [Configuation Path](#configuation-path)  
2. [Limits](#limits)  
3. [Architecture](#architecture)  
3.1 [Notes](#notes)  
3.2 [Samples](#samples)  
3.3 [Customize Scripts](#customize-scripts)  
4. [Simple Usage](#simple-usage)  
4.1 [Account Related](#account-related)  
5. [Issue Occuring](#issue-occuring)  
5.1 [cannot assign requested address](#cannot-assign-requested-address)  
5.2 [account locked after payment](#account-locked-after-payment)  
6. [Tutorials](#tutorials)  
7. [Change Log](#change-log)  


## Official Doc

Item|Value
---|---
GitHub|https://github.com/digitalocean/doctl
Doc|https://www.digitalocean.com/docs/apis-clis/doctl/


### Configuation Path

Item|Value
---|---
Default config|`${XDG_CONFIG_HOME}/doctl/config.yaml`<br/>`$HOME/.config/doctl/config.yaml`
Custom config|speciy via `--config`, `-c`


## Limits

1. [doctl][doctl] doesn't support the DigitalOcean Spaces API.

However, because the Spaces API is S3-compatible, you can use S3-compatible command line tools like s3cmd to manage Spaces.

Supported characters for service name

>422 Only valid hostname characters are allowed. (a-z, A-Z, 0-9, . and -)

## Architecture

### Notes

Preparation

* [Initialization](./Notes/0_Initialization.md)

Networking

* [Virtual Private Cloud (VPC)](./Notes/Networking/VPC.md)
* [Cloud Firewalls](./Notes/Networking/CloudFirewalls.md)

Compute

* [Droplet](./Notes/Compute/Droplet.md)

Storage

* [Block Storage Volume](./Notes/Storage/BlockStorageVolumes.md)

### Samples

* ~~[Unary Droplet Deployment](./Samples/Droplet_Single_Sample.md)~~ **deprecated**
* [Trinary Droplets Deployment](./Samples/Droplet_Trinary_Sample.md)


### Customize Scripts

Install

* [doctl install/update](./Scripts/doctl_install.sh)

Sample

* [system initialization](./Scripts/doctl_sys_init.sh)
* [trinary droplet deployment](./Scripts/droplet_trinary_deploy.sh)
* ~~[single droplet deployment](./Scripts/droplet_single_deploy.sh)~~ **deprecated**


## Simple Usage

Output json format via *--output json*, or format like *--format "ID,Name,PublicIPv4"*.

### Account Related

Account details

```sh
# Retrieve account profile details
doctl account get

# Retrieve your API usage and the remaining quota
doctl account ratelimit
```

Billing history

```sh
# Retrieve a paginated billing history for a user
doctl billing-history list
```

Account balance

```sh
# Retrieve your account balance
doctl balance get
```

Account invoice

```sh
# List all of the invoices for your account (get $invoice_id)
doctl invoice list

# Get a summary of an invoice (inclue user name, email, billing address info)
doctl invoice summary $invoice_id # --output=json
# doctl invoice summary preview --output=json

# Retrieve a list of all the items on an invoice
doctl invoice get ${invoice_id} #  --output=json

# Download a PDF file of an invoice
doctl invoice pdf ${invoice_id} <output-file.pdf>
```

## Issue Occuring

### cannot assign requested address

Package downloaded from GitHub official [release](https://github.com/digitalocean/doctl/releases/). It prompts error while validating token via command `doctl auth init`

>Error: Unable to use supplied token to access API: Get "https://api.digitalocean.com/v2/account": dial tcp: lookup api.digitalocean.com on [::1]:53: dial udp [::1]:53: connect: cannot assign requested address

But if I install [package](https://archlinux.org/packages/community/x86_64/doctl/) from Arch Linux package manager, it works properly.


### account locked after payment

Search keywords *digitalocean locked account after paypal payment* in search engine, there are many similar cases, e.g.

* [DigitalOcean - Account Locked Immediately After Payment?](https://www.digitalocean.com/community/questions/account-locked-immediately-after-payment)
* [DigitalOcean - I AM NOT CRIMINAL FOR HAVING 2 ACCOUNT](https://www.digitalocean.com/community/questions/i-am-not-criminal-for-having-2-account)
* [Reddit - Account got locked right after payment](https://www.reddit.com/r/digital_ocean/comments/ej5hji/account_got_locked_right_after_payment/)


## Tutorials

* [How To Use Doctl, the Official DigitalOcean Command-Line Client](https://www.digitalocean.com/community/tutorials/how-to-use-doctl-the-official-digitalocean-command-line-client)


## Change Log

* Jan 22, 2021 16:45 Fri ET
  * First draft
* Mar 01, 2021 21:22 Mon ET
  * Add *samples* section
* Mar 23, 2021 09:25 Tue ET
  * Add *issue occuring* section


[digitalocean]:https://www.digitalocean.com "DititalOcean - The developer cloud"
[doctl]:https://github.com/digitalocean/doctl
[doapi]:https://developers.digitalocean.com/documentation/v2/

<!-- End -->