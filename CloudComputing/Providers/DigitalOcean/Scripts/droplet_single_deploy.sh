#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails

# Official Site
# - https://www.digitalocean.com/docs/apis-clis/doctl/
# - https://github.com/digitalocean/doctl

# Target: Install/Update doctl On GNU/Linux
# Developer: MaxdSre

# Change Log:
# - Mar 01, 2021 Mon 09:18 ET - change name format to 'unary-nyc1-{vpc,tag,firewall}'
# - Feb 21, 2021 Sun 14:25 ET - add support for existed droplet to attach block storage volume
# - Feb 18, 2021 Thu 13:05 ET - add prefix name restriction for droplet info extracting
# - Feb 06, 2021 Sat 11:10 ET - fix SSH inbound rule update if exists multiple SSH rules
# - Feb 01, 2021 Mon 10:26 ET - fix variables initial issue, optimize vpc create/remove operating procedure
# - Jan 26, 2021 Tue 09:21 ET - fix download method check issue (wget, curl)
# - Jan 25, 2021 Mon 14:39 ET - code rewrite, add help info and parameter specification
# - Jan 24, 2021 Sun 21:00 ET - first draft


# Attention:
# Debian default has wget, no curl


#########  0-1. Variables Setting  #########
is_delete=0
is_customsize=0
is_firewall_update=0
is_purge_cache=0
is_block_storage_volume=0

metadata_cache_path="$HOME/.config/doctl/.metadata_single_deploy"
# development_type=${development_type:-'dev'}
# prefix_name_specify=${prefix_name_specify:-"unary-${development_type}"}
readonly prefix_name_specify='unary'
region_slug=${region_slug:-'nyc3'}   # doctl compute region list

# VPC
readonly vpc_default_ip_range='10.118.0.0/20'

# Droplet
image_slug=${image_slug:-'ubuntu-20-04-x64'}  # doctl compute image list-distribution
size_slug=${size_slug:-'s-1vcpu-2gb'}  # doctl compute size list
readonly sys_init_script_link='https://gitlab.com/axdsop/nixnotes/raw/master/CloudComputing/Providers/DigitalOcean/Scripts/doctl_sys_init.sh'

# SSH Key
ssh_key_slug=${ssh_key_slug:-}  # doctl compute ssh-key list  # key fingerprint or ID
ssh_port=${ssh_port:-22}  # default SSH port is 22

# Block Storage Volume
volume_size='100GiB'  # required, range [1, 16,384] GiB (16TiB)
volume_fs_type='xfs'  # ext4 / xfs


#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | bash -s -- [options] ...

Create/Delete Just One DigitalOcean Droplet Resources Via doctl On GNU/Linux.

Prefix name is '${prefix_name_specify}'

Details see https://gitlab.com/axdsop/nixnotes/tree/master/CloudComputing/Providers/DigitalOcean/Samples/Droplet_Single_Sample.md

[available option]
    -h    --help, show help info
    -c    --customsize droplet parmeter, default (region '${region_slug}', image '${image_slug}', image size '${size_slug}')
    -a    --create & attach block storage volume (100GiB) to existed droplet, not use with '-c'
    -d    --delete, delete created droplet resources, default is create
    -f    --update cloud firewall inbound SSH rule (IPv4 port ${ssh_port})
    -p    --purge cached resource metedata (available region, image, image size, ssh key list), optional along with '-c'
\e[0m"
}

while getopts "hacdfp" option "$@"; do
    case "$option" in
        a ) is_block_storage_volume=1 ;;
        c ) is_customsize=1 ;;
        d ) is_delete=1 ;;
        f ) is_firewall_update=1 ;;
        p ) is_purge_cache=1 ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done


#########  1-2 Preparation Operation  #########
fn_CommandExistIfCheck(){
    # $? -- 0 is find, 1 is not find
    local l_name="${1:-}"
    local l_output=${l_output:-1}
    [[ -n "${l_name}" && -n $(which "${l_name}" 2> /dev/null || command -v "${l_name}" 2> /dev/null) ]] && l_output=0
    return "${l_output}"
}

fn_Public_IP_Detection(){
    local l_output=''

    local l_download_method='wget -qO-'
    [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_method='curl -fsL'
    l_output=$(${l_download_method} ipinfo.io/ip 2> /dev/null || ${l_download_method} http://ip-api.com/line/?fields=query 2> /dev/null) # https://ipapi.co/ip
    echo "${l_output}"
}


#########  2-0. Initialization Check  #########
fn_Initialization_Check(){
    if fn_CommandExistIfCheck 'doctl'; then
        if [[ ! -f $HOME/.config/doctl/config.yaml ]]; then
            echo -e "Sorry, no config file find.\nYou can authorize token via 'doctl auth init'."
            exit
        fi
    else
        echo 'Sorry, no binary file doctl finds.'
        exit
    fi
}


#########  2-1. Variables Initialization   #########
fn_Variables_Predefinition(){
    # need ssh_key_slug, region_slug, image_slug, size_slug
    # VPC
    vpc_name="${prefix_name_specify}-${region_slug}-vpc"
    vpc_description="VPC for ${prefix_name_specify} on region ${region_slug}."
    vpc_ip_range='192.168.8.0/24' # IP size 2^8=256

    # Cloud Firewalls
    firewall_name="${prefix_name_specify}-${region_slug}-firewall"
    inbound_rule_default="protocol:tcp,ports:${ssh_port},address:0.0.0.0/0" # allow any IPv4 to visit TCP/22. if want to allow ICMP, add 'protocol:icmp,address:0.0.0.0/0'
    outbound_rule_default='protocol:icmp,address:0.0.0.0/0 protocol:tcp,ports:all,address:0.0.0.0/0 protocol:udp,ports:all,address:0.0.0.0/0' # allow icmp, outbound to any IPv4

    # Tag
    tag_name="${prefix_name_specify}-${region_slug}-tag"
    
    # Block Storage Volume
    volume_name="${prefix_name_specify}-${region_slug}-volume-01"
    volume_description="Volume for ${prefix_name_specify} on region ${region_slug}."
}

# Global variable $metadata_cache_path
fn_Metadata_Operation(){
    local l_name="${1:-}"
    local l_output=''

    if [[ ! -f "${metadata_cache_path}" ]]; then
        echo '' > "${metadata_cache_path}"
        echo -e '\nssh_key_avail_list' >> "${metadata_cache_path}"
        doctl compute ssh-key list --no-header --format="ID,Name" | sed -r -n 's@[[:space:]]{2,}@|@g;s@^([^\|]+)\|(.*)@\1 (\2)@g;p' >> "${metadata_cache_path}"

        echo -e '\nregion_avail_list' >> "${metadata_cache_path}"
        doctl compute region list --no-header --format="Available,Slug,Name" | sed -r -n 's@[[:space:]]{2,}@\|@g;/false/d;p' | sort | cut -d\| -f2,3 | sed -r -n 's@^([^\|]+)\|(.*)$@\1 \(\2\)@g;p' >> "${metadata_cache_path}"

        echo -e '\nimage_avail_list' >> "${metadata_cache_path}"
        doctl compute image list-distribution --public --no-header --format="Slug,Distribution,Name" | sort -t- -k1,1 -k2,2rh | sed -r -n '/ubuntu-(16|18)/d;/debian-(9)/d;/fedora-(32)/d;/freebsd-(11|12-[[:digit:]]+)/d;s@[[:space:]]{2,}@\|@g;s@^([^\|]+)\|([^\|]+)\|(.*)@\1 [\2 \3]@g;p' >> "${metadata_cache_path}"

        echo -e '\nimage_size_avail_list' >> "${metadata_cache_path}"
        doctl compute size list --no-header --format="Slug,Price Monthly" | sed -r -n 's@[[:space:]]{2,}@|@g;s@^([^\|]+)\|(.*)@\1 ($\2/m)@g;p' >> "${metadata_cache_path}"
        echo '' >> "${metadata_cache_path}"
    fi

    if [[ -n "${l_name}" ]]; then
        l_output=$(sed -r -n '/^'"${l_name}"'$/,/^$/{/^$/d;/^'"${l_name}"'$/d;p}' "${metadata_cache_path}")
        echo "${l_output}"
    fi
}

fn_Variables_Initialization(){
    # this script just creates 1 droplet 
    if [[ "${is_delete}" -eq 0 ]]; then
        local l_existed_droplet_id=${l_existed_droplet_id:-}
        l_existed_droplet_id=$(doctl compute droplet list --no-header --format="ID,Name,Tags" | sed -r -n '/[[:space:]]{2,}'"${prefix_name_specify}"'[^[:space:]]+/{s@^([^[:space:]]+).*@\1@g;p}')

        if [[ -n "${l_existed_droplet_id}" ]]; then
            echo -e "Attention, find one existed ${prefix_name_specify} droplet:\n"
            doctl compute droplet get "${l_existed_droplet_id}" --format="ID,Name,Public IPv4,Private IPv4,Region,Image,VPCUUID,Tags,Status,Volumes"
            exit
        fi
       
    #    if [[ $(doctl compute droplet list --no-header --format="ID,Name,Tags" | sed -r -n '/[[:space:]]+'"${prefix_name_specify}"'[[:space:]]*/{p}' | wc -l) -eq 1 ]]; then
    #         echo -e "Attention, find one existed ${prefix_name_specify} droplet:\n"
    #         doctl compute droplet list --format="ID,Name,Public IPv4,Private IPv4,Region,Image,VPCUUID,Tags,Status"
    #         exit
    #    fi

    fi

    local IFS_BAK=${IFS_BAK:-"$IFS"}  # Backup IFS
    IFS="|" # Setting temporary IFS
    PS3="Choose item number(e.g. 1, 2,...): "

    if [[ "${is_purge_cache}" -eq 1 || ! -f "${metadata_cache_path}" ]]; then
        [[ -f "${metadata_cache_path}" ]] && rm -f "${metadata_cache_path}"
        echo 'Metadata Initialing...'
        fn_Metadata_Operation
    fi

    if [[ "${is_customsize}" -eq 0 ]]; then
        # ssh_key_slug=$(doctl compute ssh-key list --no-header --format="ID" | head -n 1)
        # doctl compute ssh-key list --no-header | head -n 1 | cut -d' ' -f1
        # doctl compute ssh-key list --no-header | head -n 1 | sed -r -n 's@.*[[:space:]]+([^[:space:]]+)[[:space:]]*$@\1@g;p'  # SSH key fingerprints

        ssh_key_avail_list=$(fn_Metadata_Operation 'ssh_key_avail_list')

        if [[ $(sed '/^$/d' <<< "${ssh_key_avail_list}" | wc -l) -eq 1 ]]; then
            ssh_key_slug="${ssh_key_avail_list%% *}"
        else
            echo -e "\nAvailable SSH key List: "
            select item in $(sed ':a;N;$!ba;s@\n@|@g' <<< "${ssh_key_avail_list}"); do
                ssh_key_slug="${item}"
                [[ -n "${ssh_key_slug}" ]] && break
            done < /dev/tty
            ssh_key_slug="${ssh_key_slug%% *}"
        fi
    else
        # - ssh key
        # ssh_key_avail_list=$(doctl compute ssh-key list --no-header --format="ID,Name" | sed -r -n 's@[[:space:]]{2,}@|@g;s@^([^\|]+)\|(.*)@\1 (\2)@g;p')
        ssh_key_avail_list=$(fn_Metadata_Operation 'ssh_key_avail_list')
        
        echo -e "\nAvailable SSH key List: "
        select item in $(sed ':a;N;$!ba;s@\n@|@g' <<< "${ssh_key_avail_list}"); do
            ssh_key_slug="${item}"
            [[ -n "${ssh_key_slug}" ]] && break
        done < /dev/tty
        ssh_key_slug="${ssh_key_slug%% *}"

        # - region choose
        # doctl compute region list --no-header --format="Available,Slug,Name" | sed -r -n 's@[[:space:]]{2,}@\|@g;/false/d;p' | sort | cut -d\| -f2,3 | sed -r -n 's@^([^\|]+)\|(.*)$@\1 \(\2\)@g;p'
        # region_avail_list="ams3 (Amsterdam 3)|blr1 (Bangalore 1)|fra1 (Frankfurt 1)|lon1 (London 1)|nyc1 (New York 1)|nyc3 (New York 3)|sfo2 (San Francisco 2)|sfo3 (San Francisco 3)|sgp1 (Singapore 1)|tor1 (Toronto 1)"
        region_avail_list=$(fn_Metadata_Operation 'region_avail_list')
        
        echo -e "\nAvailable Region List: "
        select item in $(sed ':a;N;$!ba;s@\n@|@g' <<< "${region_avail_list}"); do
            region_slug="${item}"
            [[ -n "${region_slug}" ]] && break
        done < /dev/tty
        region_slug="${region_slug%% *}"

        # - image choose
        # doctl compute image list-distribution --public --no-header --format="Slug,Distribution,Name" | sort -r | sed -r -n '/ubuntu-(16|18)/d;/debian-(9)/d;/fedora-(32)/d;/freebsd-(11|12-[[:digit:]]+)/d;s@[[:space:]]{2,}@\|@g;s@^([^\|]+)\|([^\|]+)\|(.*)@\1 [\2 \3]@g;p'
        # image_avail_list="centos-8-x64 [CentOS 8.3 x64]|centos-7-x64 [CentOS 7.6 x64]|debian-10-x64 [Debian 10 x64]|fedora-33-x64 [Fedora 33 x64]|freebsd-12-x64-ufs [FreeBSD 12.2 ufs x64]|freebsd-12-x64-zfs [FreeBSD 12.2 zfs x64]|rancheros [RancherOS 1.5.8 x64]|ubuntu-20-04-x64 [Ubuntu 20.04 (LTS) x64]|ubuntu-20-10-x64 [Ubuntu 20.10 x64]"
        image_avail_list=$(fn_Metadata_Operation 'image_avail_list')

        echo -e "\nAvailable Image List: "
        select item in $(sed ':a;N;$!ba;s@\n@|@g' <<< "${image_avail_list}"); do
            image_slug="${item}"
            [[ -n "${image_slug}" ]] && break
        done < /dev/tty
        image_slug="${image_slug%% *}"

        # - image size choose
        # doctl compute size list --no-header --format="Slug,Price Monthly" | sed -r -n 's@[[:space:]]{2,}@|@g;s@^([^\|]+)\|(.*)@\1 ($\2/m)@g;p'
        # image_size_avail_list="s-1vcpu-1gb (\$5.00/m)|s-1vcpu-2gb (\$10.00/m)|s-2vcpu-2gb (\$15.00/m)|s-2vcpu-4gb (\$20.00/m)"
        # image_size_avail_list="s-1vcpu-1gb ($5.00/m)|s-1vcpu-1gb-amd ($6.00/m)|s-1vcpu-1gb-intel ($6.00/m)|s-1vcpu-2gb ($10.00/m)|s-1vcpu-2gb-amd ($12.00/m)|s-1vcpu-2gb-intel ($12.00/m)|s-2vcpu-2gb ($15.00/m)|s-2vcpu-2gb-amd ($18.00/m)|s-2vcpu-2gb-intel ($18.00/m)|s-2vcpu-4gb ($20.00/m)"
        image_size_avail_list=$(fn_Metadata_Operation 'image_size_avail_list')

        echo -e "\nAvailable Image Size List: "
        select item in $(sed ':a;N;$!ba;s@\n@|@g' <<< "${image_size_avail_list}"); do
            size_slug="${item}"
            [[ -n "${size_slug}" ]] && break
        done < /dev/tty
        size_slug="${size_slug%% *}"


        IFS=${IFS_BAK}  # Restore IFS
        unset IFS_BAK
        unset PS3
    fi

    echo -e "\n\nDroplet parameter:\nRegion '${region_slug}', Image '${image_slug}', Image size '${size_slug}', SSH Key '${ssh_key_slug}'"

    fn_Variables_Predefinition

    local l_public_ip=''
    l_public_ip=$(fn_Public_IP_Detection)
    inbound_rule_default="protocol:tcp,ports:${ssh_port},address:${l_public_ip}/32" # just allow my public IPv4 to visit TCP/22

    user_data_file=$(mktemp -t doctl_XXXXXXXXX)
    script_save_path="/root/.${prefix_name_specify//-/_}_sys_init.sh"
    echo -e '#!/usr/bin/env bash' > "${user_data_file}"
    echo -e "wget -qO- ${sys_init_script_link} > ${script_save_path} || curl -fsL ${sys_init_script_link} > ${script_save_path}\nbash ${script_save_path}" >> "${user_data_file}"
    # echo -e '#!/usr/bin/env bash\nwget -qO- '"${sys_init_script_link}"' > '"${script_save_path}"' || curl -fsL '"${sys_init_script_link}"' > '"${script_save_path}"'\nbash ${script_save_path}' > "${user_data_file}" # can execute successfully in command line interface

    # this script consumes time about 5min (14:35:20~14:40:45) for Ubuntu 20.10, about 30s~50s for Debian 10
}

#########  2-2. Droplet Resources Create  #########
fn_Droplet_Single_Create(){
    # - VPC Create
    # Check if default VPC exists
    local l_vpc_default_id=${l_vpc_default_id:-}
    l_vpc_default_id=$(doctl vpcs list --no-header --format="ID,Name,Region,Default" | sed -r -n 's@[[:space:]]{2,}@\|@g;p' | sed -r -n '/\|default[^\|]+\|'"${region_slug}"'\|/{s@^([^\|]+).*@\1@g;p}')

    if [[ -z "${l_vpc_default_id}" ]]; then
        doctl vpcs create --name "default-${region_slug}" --region "${region_slug}" --ip-range "${vpc_default_ip_range}" 1> /dev/null
    fi

    vpc_id=$(doctl vpcs create --name "${vpc_name}" --region "${region_slug}" --description "${vpc_description}" --ip-range "${vpc_ip_range}" --output=json | sed -r -n '/"id"/{s@[^:]*:[[:space:]]*"([^"]*)".*@\1@g;p}')
    # change default state from 'false' to 'true', one region just has one active VPC at the same time
    doctl vpcs update "${vpc_id}" --default 'true' --output=json 1> /dev/null
    echo -e "\nVPC Info:"
    doctl vpcs get "${vpc_id}" --format="Name,IPRange,Region,Default"

    # - Cloud Firewall Create
    firewall_uuid=$(doctl compute firewall create --name "${firewall_name}" \
    --inbound-rules "${inbound_rule_default}" \
    --outbound-rules "${outbound_rule_default}" \
    --no-header --format="ID"
    )
    echo -e "\nCloud Firewall Info:"
    doctl compute firewall get "${firewall_uuid}" --format="Name,ID,Status"

    # - Tag Create
    doctl compute tag create "${tag_name}" 1> /dev/null # --output=json
    echo -e "\nTag Info:"
    doctl compute tag get "${tag_name}"

    # - Droplet Create
    droplet_name="${prefix_name_specify}-${region_slug}-${image_slug%%-*}" # unary-nyc3-ubuntu

    droplet_id=$(doctl compute droplet create "${droplet_name}" \
        --region "${region_slug}" \
        --image "${image_slug}" \
        --size "${size_slug}" \
        --ssh-keys "${ssh_key_slug}" \
        --vpc-uuid "${vpc_id}" \
        --user-data-file "${user_data_file}" \
        --no-header --format="ID"
        ) # --wait consumes time about 10s

    # --user-data not work ???
    # --wait   Wait for Droplet creation to complete before returning
    # --enable-backups \ --enable-ipv6 \ --enable-monitoring \ --enable-private-networking
    # --volumes strings             A list of block storage volume IDs to attach to the Droplet
    # --tag-name string      A tag name to be applied to the Droplet
    # --tag-names strings    A list of tag names to be applied to the Droplet
    # --user-data string       User-data to configure the Droplet on first boot
    # --user-data-file string  The path to a file containing user-data to configure the Droplet on first boot

    [[ -f "${user_data_file}" ]] && rm -f "${user_data_file}"

    # Get droplet public ip
    # echo -n 'Public ip is: '
    # doctl compute droplet get "${droplet_id}" --no-header --format="PublicIPv4"

    # - Tag droplet (optional, also can specify parameter '--tag-names' in 'doctl compute droplet create')
    doctl compute droplet tag "${droplet_id}" --tag-name "${tag_name}" # tag a droplet, use droplet id or name

    # - Add droplet to Cloud Firewalls
    # doctl compute firewall add-droplets "${firewall_uuid}" --droplet-ids "${droplet_id}"  # method 1 - Add droplet to cloud firewall
    doctl compute firewall add-tags "${firewall_uuid}" --tag-names "${tag_name}"  # method 2 - Add droplet via tag to cloud firewall

    # - Block Storage Volume Create & Attach
    # if [[ "${is_block_storage_volume}" -eq 1 ]]; then
    #     # Create
    #     volume_id=$(doctl compute volume create "${volume_name}" \
    #         --desc "${volume_description}" \
    #         --region "${region_slug}" \
    #         --size "${volume_size}" \
    #         --fs-type "${volume_fs_type}" \
    #         --no-header --format="ID"
    #         )
    #     echo -e "\nBlock Storage Volume Info:"
    #     doctl compute volume get "${volume_id}" --format="ID,Name,Size,Region,Tags"

    #     # Attach
    #     doctl compute volume-action attach "${volume_id}" "${droplet_id}" 1> /dev/null  # --output=json

    #     sleep 2
    # fi

    echo -e "\nDroplet Info\n:"
    doctl compute droplet get "${droplet_id}" --output=json
    # doctl compute droplet get "${droplet_id}" --format="ID,Name,Public IPv4,Private IPv4,Region,Image,VPCUUID,Tags,Status"
}


#########  3-1. Droplet Resources Operation  #########
# - Droplet delete
# - Block Storage Volume create & attach

fn_Droplet_Single_Operation(){
    local l_droplet_name=${1:-}
    local l_droplet_info=''
    echo "Existed ${prefix_name_specify} droplets detecting..."
    # l_droplet_info=$(doctl compute droplet list --no-header --format="ID,Name,VPCUUID,Tags,Region" | sed -r -n 's@[[:space:]]+@\|@g;p')
    l_droplet_info=$(doctl compute droplet list --no-header --format="ID,Name,VPCUUID,Tags,Region,Volumes" | sed -r -n '/[[:space:]]{2,}'"${prefix_name_specify}"'[^[:space:]]+/{s@[[:space:]]{2,}@\|@g;p}')
    
    # 232992431|nyc1-debian-01|b0a68ff1-6c52-4dce-9cd1-84a574c75a9b|unary-dev-tag-nyc1|nyc1|e0047bb1-7473-11eb-bd9d-0a58ac144893

    local l_droplet_count=0
    l_droplet_count=$(sed '/^$/d' <<< "${l_droplet_info}" | wc -l)

    if [[ "${l_droplet_count}" -gt 0 ]]; then
        # - Check if specified droplet name existed
        [[ -n "${l_droplet_name}" && -n $(sed -r -n '/[[:space:]]+'"${l_droplet_name}"'[[:space:]]+/{p}' <<< "${l_droplet_info}") ]] || l_droplet_name=''

        # - If just has one droplet, extract droplet name instead of via selection menu
        [[ "${l_droplet_count}" -eq 1 ]] && l_droplet_name=$(cut -d\| -f 2  <<< "${l_droplet_info}")

        # - Without droplet name, via selection menu
        if [[ -z "${l_droplet_name}" ]]; then
            local IFS_BAK=${IFS_BAK:-"$IFS"}  # Backup IFS
            IFS="|" # Setting temporary IFS

            echo -e "\nAvailable Droplet List: "
            PS3="Choose droplet number(e.g. 1, 2,...): "

            select item in $(cut -d\| -f 2,5  <<< "${l_droplet_info}" | sed -r -n 's@([^\|]+)\|(.*)@\1 (\2)@g;p' | sort -r | sed ':a;N;$!ba;s@\n@|@g'); do
                l_droplet_name="${item}"
                [[ -n "${l_droplet_name}" ]] && break
            done < /dev/tty

            IFS=${IFS_BAK}  # Restore IFS
            unset IFS_BAK
            unset PS3
            printf "\nDroplet you choose is %s.\n\n" "${l_droplet_name}"
            l_droplet_name="${l_droplet_name%% *}"
        fi

        # -Droplet resources delete action
        if [[ -n "${l_droplet_name}" ]]; then
            # doctl compute droplet list --no-header --format="ID,Name,VPCUUID,Tags" | sed -r -n '/[[:space:]]+'"${l_droplet_name}"'[[:space:]]+/{s@[[:space:]]+@\|@g;p}'

            sed -r -n '/\|'"${l_droplet_name}"'\|/{p}' <<< "${l_droplet_info}" | while read -r droplet_info; do
                droplet_id=$(cut -d\| -f1 <<< "${droplet_info}")
                droplet_name=$(cut -d\| -f2 <<< "${droplet_info}")
                vpc_id=$(cut -d\| -f3 <<< "${droplet_info}")
                tag_name=$(cut -d\| -f4 <<< "${droplet_info}")
                region_name=$(cut -d\| -f5 <<< "${droplet_info}")
                volume_id=$(cut -d\| -f6 <<< "${droplet_info}")

                # - Block Storage Volume Create & Attach
                if [[ "${is_block_storage_volume}" -eq 1 ]]; then
                    # Create
                    volume_name="${prefix_name_specify}-volume-${region_name}-01"
                    volume_description="Volume for ${prefix_name_specify} on region ${region_name}."

                    volume_id=$(doctl compute volume create "${volume_name}" \
                        --desc "${volume_description}" \
                        --region "${region_name}" \
                        --size "${volume_size}" \
                        --fs-type "${volume_fs_type}" \
                        --no-header --format="ID"
                        )
                    echo -e "\nBlock Storage Volume Info:"
                    doctl compute volume get "${volume_id}" --format="ID,Name,Size,Region,Tags"

                    # Attach
                    doctl compute volume-action attach "${volume_id}" "${droplet_id}" 1> /dev/null  # --output=json

                    echo -e "\nDroplet Info\n:"
                    # doctl compute droplet get "${droplet_id}" --output=json
                    doctl compute droplet get "${droplet_id}" --format="ID,Name,Public IPv4,Region,Image,Status,Volumes"

                # - delete action
                elif [[ "${is_delete}" -eq 1 ]]; then

                    firewall_uuid=$(doctl compute firewall list-by-droplet "${droplet_id}" --no-header --format="ID")

                    # Delete order: volume,droplet, firewall, tag, vpc

                    # - Detach & Delete block storage volume
                    if [[ -n "${volume_id}" ]]; then
                        echo -n -e 'Block Storage Volume detaching '
                        doctl compute volume-action detach "${volume_id}" "${droplet_id}" --wait &> /dev/null # --output=json
                        sleep 1
                        echo '& deleting ...'
                        doctl compute volume delete "${volume_id}" --force
                    fi

                    # - Delete a droplet
                    # doctl compute droplet list --format="ID,Name,Public IPv4,Region,Image,Status"
                    echo -e 'Droplet deleting ...'
                    doctl compute droplet delete "${droplet_id}" --force
                    # Warning: Are you sure you want to delete this Droplet? (y/N) ?

                    # - Delete a cloud firewall
                    echo -e 'Cloud firewall deleting ...'
                    # doctl compute firewall remove-droplets "${firewall_uuid}" --droplet-ids "${droplet_id}"  # method 1 - remove droplet from cloud firewall
                    doctl compute firewall remove-tags "${firewall_uuid}" --tag-names "${tag_name}"    # method 2 - remove droplet via tag from cloud firewall
                    doctl compute firewall delete "${firewall_uuid}" --force
                    # Warning: Are you sure you want to delete this firewall? (y/N) ? y

                    # - Delete a tag
                    echo -e 'Tag deleting ...'
                    doctl compute tag delete "${tag_name}" --force
                    # doctl compute tag list
                    sleep 1

                    # - Delete a vpc
                    echo -e 'VPC deleting ...'
                    # mark default vpc as true
                    local l_origin_default_vpc_info=${l_origin_default_vpc_info:-}
                    # origin_default_vpc_id=$(doctl vpcs list --no-header --format="ID,Name,Region,Default" | sed -r -n '/default-'"${region_name}"'/{s@^([^[:space:]]+).*@\1@g;p;q}')
                    l_origin_default_vpc_info=$(doctl vpcs list --no-header --format="ID,Name,Region,Default" | sed -r -n 's@[[:space:]]{2,}@\|@g; /default[^\|]+\|'"${region_name}"'\|/{p;q}')

                    if [[ -n "${l_origin_default_vpc_info}" ]]; then
                        [[ $(cut -d\| -f 4 <<< "${l_origin_default_vpc_info}") != 'true' ]] && doctl vpcs update "$(cut -d\| -f 1 <<< "${l_origin_default_vpc_info}")" --default true --output=json 1> /dev/null
                    fi
                    # doctl vpcs get $vpc_id --format="Name,IPRange,Region,Default"
                    sleep 2  # Error: DELETE https://api.digitalocean.com/v2/vpcs/1ed6d4b6-42e1-4a27-b380-99100baa8ffb: 403 (request "21c24c71-08c9-43a3-bf15-da20a156b319") Can not delete VPC with members
                    doctl vpcs delete "${vpc_id}" --force
                    # Warning: Are you sure you want to delete this VPC? (y/N) ?

                    # doctl vpcs list --format="Name,IPRange,Region,Default"
                fi

            done
        fi
    else
        echo 'No droplet detects.'
    fi

    exit
}


#########  4-1. Cloud Firewall Inbound Rule Update  #########
fn_Firewall_Inbound_SSH_Rule_Update(){
    local l_droplet_name=${1:-}
    local l_droplet_info=''
    l_droplet_info=$(doctl compute droplet list --no-header --format="ID,Name,VPCUUID,Tags,Region" | sed -r -n 's@[[:space:]]+@\|@g;p')
    # 228101056|nyc3-ubuntu-01|d8da895d-c5ca-4b37-99c8-b58390909c97|dev-tag-nyc3|ny3
    local l_droplet_count=0
    l_droplet_count=$(sed '/^$/d' <<< "${l_droplet_info}" | wc -l)

    if [[ "${l_droplet_count}" -gt 0 ]]; then

        # check if specified droplet name existed
        [[ -n "${l_droplet_name}" && -n $(sed -r -n '/[[:space:]]+'"${l_droplet_name}"'[[:space:]]+/{p}' <<< "${l_droplet_info}") ]] || l_droplet_name=''

        # If just has one droplet, extract droplet name instead of via selection menu
        [[ "${l_droplet_count}" -eq 1 ]] && l_droplet_name=$(cut -d\| -f 2  <<< "${l_droplet_info}")

        # - Without droplet name, via selection menu
        if [[ -z "${l_droplet_name}" ]]; then
            local IFS_BAK=${IFS_BAK:-"$IFS"}  # Backup IFS
            IFS="|" # Setting temporary IFS

            echo -e "\nAvailable Droplet List: "
            PS3="Choose droplet number(e.g. 1, 2,...): "

            select item in $(cut -d\| -f 2,5  <<< "${l_droplet_info}" | sed -r -n 's@([^\|]+)\|(.*)@\1 (\2)@g;p' | sort -r | sed ':a;N;$!ba;s@\n@|@g'); do
                l_droplet_name="${item}"
                [[ -n "${l_droplet_name}" ]] && break
            done < /dev/tty

            IFS=${IFS_BAK}  # Restore IFS
            unset IFS_BAK
            unset PS3
            printf "\nDroplet you choose is %s.\n\n" "${l_droplet_name}"
            l_droplet_name="${l_droplet_name%% *}"
        fi

        # -Droplet resources delete action
        if [[ -n "${l_droplet_name}" ]]; then
            # doctl compute droplet list --no-header --format="ID,Name,VPCUUID,Tags" | sed -r -n '/[[:space:]]+'"${l_droplet_name}"'[[:space:]]+/{s@[[:space:]]+@\|@g;p}'
            sed -r -n '/\|'"${l_droplet_name}"'\|/{p}' <<< "${l_droplet_info}" | while read -r droplet_info; do
                droplet_id=$(cut -d\| -f1 <<< "${droplet_info}")
                # droplet_name=$(cut -d\| -f2 <<< "${droplet_info}")
                # vpc_id=$(cut -d\| -f3 <<< "${droplet_info}")
                # tag_name=$(cut -d\| -f4 <<< "${droplet_info}")

                firewall_uuid=$(doctl compute firewall list-by-droplet "${droplet_id}" --no-header --format="ID")

                public_ip_new=$(fn_Public_IP_Detection)
                inbound_rule_add="protocol:tcp,ports:${ssh_port},address:${public_ip_new}/32"

                # remove old public ip
                inbound_rule_remove=$(doctl compute firewall get "${firewall_uuid}" --no-header --format="InboundRules" | sed -r -n 's@[[:space:]]+@\n@g;p' | sed -r '/ports:'"${ssh_port}"'[^[:digit:]]+/!d' | sed ':a;N;$!ba;s@\n@ @g')
                doctl compute firewall remove-rules "${firewall_uuid}" \
                --inbound-rules "${inbound_rule_remove}" --output=json
                echo "Old inbound SSH rule: ${inbound_rule_remove}"

                # add new public ip
                doctl compute firewall add-rules "${firewall_uuid}" \
                --inbound-rules "${inbound_rule_add}" --output=json
                echo -e -n "\nNew inbound SSH rule: "
                doctl compute firewall list-by-droplet "${droplet_id}" --no-header --format="Inbound Rules" |  sed -r -n 's@[[:space:]]+@\n@g;p' | sed -r '/ports:'"${ssh_port}"'[^[:digit:]]+/!d'
            done
        fi
    else
        echo 'No droplet detects.'
    fi

    exit
}


#########  5. Executing Process  #########
fn_Initialization_Check

[[ "${is_firewall_update}" -eq 1 ]] && fn_Firewall_Inbound_SSH_Rule_Update "$@"

if [[ "${is_delete}" -eq 1 || "${is_block_storage_volume}" -eq 1 ]]; then
    fn_Droplet_Single_Operation "$@"
else
    fn_Variables_Initialization
    fn_Droplet_Single_Create
fi

# Script End