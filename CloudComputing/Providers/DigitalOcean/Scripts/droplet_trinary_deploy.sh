#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails

# Official Site
# - https://www.digitalocean.com/docs/apis-clis/doctl/
# - https://github.com/digitalocean/doctl

# Target: Deploy/Delete Digital Ocean droplet via doctl On GNU/Linux
# Developer: MaxdSre

# Change Log:
# - Mar 29, 2021 Mon 19?38 ET - add normal user name specify option
# - Mar 27, 2021 Sat 12:23 ET - first draft


#########  0-1. Variables Setting  #########
is_delete=0
is_customsize=0
is_purge_cache=0
is_firewall_update=0
username_specify=${username_specify:-'maxdsre'}

metadata_cache_path="$HOME/.config/doctl/.metadata_single_deploy"
readonly sys_init_script_link='https://gitlab.com/axdsop/nixnotes/raw/master/CloudComputing/Providers/DigitalOcean/Scripts/doctl_sys_init.sh'
user_data_file=$(mktemp -t doctl_XXXXXXXXX)
readonly script_save_path="/tmp/.doctl_sys_init.sh"

droplet_index_num=${droplet_index_num:-1}

readonly prefix_type_name='web'
readonly suffix_for_jump='jump'  # number 1
readonly suffix_for_jump_control='control'
readonly suffix_for_webserver='reverse'  # number 1
readonly suffix_for_backend='backend'  # number [1,]

droplet_usage_type_list="${suffix_for_jump}|${suffix_for_webserver}|${suffix_for_backend}"
droplet_usage_type=${droplet_usage_type:-}

readonly region_slug_default='nyc1'   # doctl compute region list
readonly image_slug_default='debian-10-x64'  # doctl compute image list-distribution
readonly size_slug_default='s-1vcpu-2gb'  # doctl compute size list

# SSH Key
ssh_port=${ssh_port:-22}  # default SSH port is 22
ssh_name_jump=${ssh_name_jump:-'SSH Jump'}
ssh_name_backend=${ssh_name_backend:-'SSH Backend'}


#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | bash -s -- [options] ...

Create/Delete DigitalOcean Droplet Resources Via doctl On GNU/Linux.

Details see https://gitlab.com/axdsop/nixnotes/tree/master/CloudComputing/Providers/DigitalOcean/Samples/Droplet_Trinary_Sample.md


[available option]
    -h    --help, show help info
    -c    --customsize droplet parameter, default (region '${region_slug_default}', image '${image_slug_default}', image size '${size_slug_default}')
    -d    --delete, delete created droplet resources, default is create
    -f    --update cloud firewall inbound SSH rule (IPv4 port ${ssh_port}) for '${suffix_for_jump}' droplet
    -p    --purge cached resource metedata (available region, image, image size, ssh key list), optional along with '-c'
    -u USERNAME    --specify normal user name (default '${username_specify}') with sudo permission
\e[0m"
}

while getopts "hcdfpu:" option "$@"; do
    case "$option" in
        h|\? ) fn_HelpInfo && exit ;;
        c ) is_customsize=1 ;;
        d ) is_delete=1 ;;
        f ) is_firewall_update=1 ;;
        p ) is_purge_cache=1 ;;
        u ) username_specify="$OPTARG" ;;
    esac
done


#########  1-2 Preparation Functions  #########
fn_CommandExistIfCheck(){
    # $? -- 0 is find, 1 is not find
    local l_name="${1:-}"
    local l_output=${l_output:-1}
    [[ -n "${l_name}" && -n $(which "${l_name}" 2> /dev/null || command -v "${l_name}" 2> /dev/null) ]] && l_output=0
    return "${l_output}"
}

fn_ExitStatement(){
    local l_str="$*"
    [[ -n "${l_str}" ]] && echo -e "${l_str}\n"
    exit
}

fn_Public_IP_Detection(){
    local l_output=''
    local l_download_method='wget -qO-'
    [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_method='curl -fsL'
    l_output=$(${l_download_method} ipinfo.io/ip 2> /dev/null || ${l_download_method} http://ip-api.com/line/?fields=query 2> /dev/null) # https://ipapi.co/ip
    echo "${l_output}"
}


#########  2-0. Initialization Check  #########
fn_Initialization_Check(){
    # - Binary utility detection
    if fn_CommandExistIfCheck 'doctl'; then
        # [[ -n $(doctl auth list 2> /dev/null) ]] || fn_ExitStatement "No authentication context finds, please initialize doctl with token via command 'doctl auth init'"

        [[ -f $HOME/.config/doctl/config.yaml ]] || fn_ExitStatement "Sorry, no config file find in $HOME/.config/doctl/.\nYou can authorize token via 'doctl auth init'."

        # account status (active)
        # [[ $(doctl account get --no-header --format="Status" 2> /dev/null) == 'active' ]] || fn_ExitStatement "Attention: account status is not 'active'"

        # config file: $HOME/.config/doctl/config.yaml
        [[ -d "$HOME/.config/doctl" ]] || mkdir -p "$HOME/.config/doctl"
    else
        fn_ExitStatement "Sorry, no binary file doctl finds in \$PATH"
    fi

    # - SSH Key (Digital Ocean) save path detection
    ssh_key_directory=${ssh_key_directory:-}
    ssh_key_directory=$(find "$HOME/Documents/" -type d -iname 'DigitalOcean' -print 2> /dev/null | sed '/ssh/I!d;q')
    [[ -z "${ssh_key_directory}" ]] && ssh_key_directory="$HOME/Documents/KeyManagement/SSH/DigitalOcean"
    [[ -d "${ssh_key_directory}" ]] || mkdir -p "${ssh_key_directory}"
}


#########  2-1. Variables Initialization  #########
# Global variable $metadata_cache_path
fn_Metadata_Operation(){
    local l_name="${1:-}"
    local l_output=''

    if [[ ! -f "${metadata_cache_path}" ]]; then
        echo -n 'Metadata Initialing ... '

        echo '' > "${metadata_cache_path}"

        echo -e '\nssh_key_avail_list' >> "${metadata_cache_path}"
        doctl compute ssh-key list --no-header --format="ID,Name" | sed -r -n 's@[[:space:]]{2,}@|@g;s@^([^\|]+)\|(.*)@\1 (\2)@g;p' >> "${metadata_cache_path}"

        echo -e '\nregion_avail_list' >> "${metadata_cache_path}"
        doctl compute region list --no-header --format="Available,Slug,Name" | sed -r -n 's@[[:space:]]{2,}@\|@g;/false/d;p' | sort | cut -d\| -f2,3 | sed -r -n 's@^([^\|]+)\|(.*)$@\1 \(\2\)@g;p' >> "${metadata_cache_path}"

        echo -e '\nimage_avail_list' >> "${metadata_cache_path}"
        doctl compute image list-distribution --public --no-header --format="Slug,Distribution,Name" | sort -t- -k1,1 -k2,2rh -k3,3rh | awk -F- '{if ($1 in arr){if(arr[$1]==$2){print}} else{arr[$1]=$2; print}}' | sed -r -n 's@[[:space:]]{2,}@\|@g;s@^([^\|]+)\|([^\|]+)\|(.*)@\1 [\2 \3]@g;p' >> "${metadata_cache_path}"

        echo -e '\nimage_size_avail_list' >> "${metadata_cache_path}"
        doctl compute size list --no-header --format="Slug,Price Monthly" | sed -r -n 's@[[:space:]]{2,}@|@g;s@^([^\|]+)\|(.*)@\1 ($\2/m)@g;p' >> "${metadata_cache_path}"
        # default droplet limit is 10 for user pay with Paypal ($5)
        [[ $(doctl account get --no-header --format="DropletLimit") -le 10 ]] && sed -r -i '/image_size_avail_list/,/^$/{/-(amd|intel)/Id}' "${metadata_cache_path}"

        echo '' >> "${metadata_cache_path}"
        echo '[ok]'
    fi

    if [[ -n "${l_name}" ]]; then
        l_output=$(sed -r -n '/^'"${l_name}"'$/,/^$/{/^$/d;/^'"${l_name}"'$/d;p}' "${metadata_cache_path}")
        echo "${l_output}"
    fi
}

fn_SSH_Key_Generation(){
    echo -n 'SSH Key Re-generating & Uploading ... '
    local l_key_save_path=${1:-''}

    # ssh_name_jump='SSH Jump'
    # ssh_name_backend='SSH Backend'

    # - Delete
    # echo 'Deleting existed SSH keys from Digital Ocean ...'
    # doctl compute ssh-key list # --output=json
    doctl compute ssh-key list --no-header --format="ID,Name" | sed -r -n 's@[[:space:]]{2,}@|@g; /\|('"${ssh_name_jump}"'|'"${ssh_name_backend}"')/{s@^([^\|]+).*@\1@g;p}' | xargs -I % doctl compute ssh-key delete % -f

    # - Create
    # Jump Key
    # echo -e "\nGeneraring key ${ssh_name_jump}"
    echo 'y' | ssh-keygen -t ed25519 -N '' -C "DigitalOcean ${ssh_name_jump}" -f "${l_key_save_path}"/id_ed25519_jump 1> /dev/null
    doctl compute ssh-key import "${ssh_name_jump}" --public-key-file "${l_key_save_path}"/id_ed25519_jump.pub 1> /dev/null # --no-header --format="ID"

    # Backend Key
    # echo -e "\nGeneraring key ${ssh_name_backend}"
    echo 'y' | ssh-keygen -t ed25519 -N '' -C "DigitalOcean ${ssh_name_backend}" -f "${l_key_save_path}"/id_ed25519_backend 1> /dev/null
    doctl compute ssh-key import "${ssh_name_backend}" --public-key-file "${l_key_save_path}"/id_ed25519_backend.pub 1> /dev/null # --no-header --format="ID"

    # change file permission
    find "${l_key_save_path}" -type f -name 'id_*' -exec chmod 600 {} \;
    find "${l_key_save_path}" -type f -name 'id_*.pub' -exec chmod 644 {} \;

    # - List
    # echo -e '\nListing new SSH keys'
    # doctl compute ssh-key list --output=json
    echo '[ok]'
}

fn_Droplet_Paras_Configuration(){
    local IFS_BAK=${IFS_BAK:-"$IFS"}  # Backup IFS
    IFS="|" # Setting temporary IFS
    PS3="Choose item number(e.g. 1, 2,...): "

    # - purege metadata cache
    if [[ "${is_purge_cache}" -eq 1 || ! -f "${metadata_cache_path}" ]]; then
        [[ -f "${metadata_cache_path}" ]] && rm -f "${metadata_cache_path}"
        [[ $(doctl compute droplet list --no-header --format="ID,Region" 2> /dev/null | sed '/^$/d' |wc -l) -eq 0 ]] && fn_SSH_Key_Generation "${ssh_key_directory}"
        fn_Metadata_Operation
    fi

    # - region choose
    if [[ "${is_customsize}" -eq 0 ]]; then
        region_slug="${region_slug_default}"
    else
        region_avail_list=$(fn_Metadata_Operation 'region_avail_list')
        
        echo -e "\nAvailable Region List: "
        select item in $(sed ':a;N;$!ba;s@\n@|@g' <<< "${region_avail_list}"); do
            region_slug="${item}"
            [[ -n "${region_slug}" ]] && break
        done < /dev/tty
        region_slug="${region_slug%% *}"
    fi

    # - droplet usage type (jump|reverse|backend)
    echo -e "\nAvailable droplet usage type List: "
    select item in $(sed ':a;N;$!ba;s@\n@|@g' <<< "${droplet_usage_type_list}"); do
        droplet_usage_type="${item}"
        [[ -n "${droplet_usage_type}" ]] && break
    done < /dev/tty
    droplet_usage_type="${droplet_usage_type%% *}"

    local l_region_droplets_info=''
    l_region_droplets_info=$(doctl compute droplet list --no-header --format="Region,Name" 2> /dev/null | sed -r -n 's@[[:space:]]{2,}@\|@g; /^'"${region_slug}"'\|/{p}')

    if [[ -n "${l_region_droplets_info}" ]]; then
        local l_type_droplet_num=''
        l_type_droplet_num=$(sed -r -n '/^'"${region_slug}"'\|.*-'"${droplet_usage_type}"'(-[[:digit:]]+)?$/{p}' <<< "${l_region_droplets_info}" 2> /dev/null | sed '/^$/d' | wc -l)

        case "${droplet_usage_type}" in
            jump|reverse) [[ "${l_type_droplet_num}" -eq 1 ]] && fn_ExitStatement "\nAttention: region ${region_slug} exists a '${droplet_usage_type}' droplet." ;;
        esac

        # droplet index add preffix '0' if number in [1,9]
        if [[ "${l_type_droplet_num}" -gt 0 ]]; then
            ((l_type_droplet_num++))
            droplet_index_num="${l_type_droplet_num}"
        fi

        if [[ "${droplet_index_num}" -gt 0 && "${droplet_index_num}" -lt 10 ]]; then
            droplet_index_num="0${droplet_index_num}"
        fi

    fi

    # jump droplet use 'SSH Jump'
    # reverse/backend droplet use 'SSH Backend', they only can be connected by jump droplet via SSH

    ssh_key_avail_list=$(fn_Metadata_Operation 'ssh_key_avail_list')

    local l_ssh_key_name=''

    case "${droplet_usage_type}" in
        jump) l_ssh_key_name="${ssh_name_jump}" ;;
        *) l_ssh_key_name="${ssh_name_backend}" ;;
    esac

    ssh_key_slug=${ssh_key_slug:-}
    ssh_key_slug=$(sed -r -n '/'"${l_ssh_key_name}"'/{s@^[[:space:]]*([^[:space:]]+).*@\1@g;p;q}' <<< "${ssh_key_avail_list}")

    # - droplet parameter choose via selection menu
    if [[ "${is_customsize}" -eq 0 ]]; then
        # region_slug="${region_slug_default}"
        image_slug="${image_slug_default}"
        size_slug="${size_slug_default}"
    else
        # - ssh key
        # ssh_key_avail_list=$(fn_Metadata_Operation 'ssh_key_avail_list')
        
        # echo -e "\nAvailable SSH key List: "
        # select item in $(sed ':a;N;$!ba;s@\n@|@g' <<< "${ssh_key_avail_list}"); do
        #     ssh_key_slug="${item}"
        #     [[ -n "${ssh_key_slug}" ]] && break
        # done < /dev/tty
        # ssh_key_slug="${ssh_key_slug%% *}"

        # - image choose
        image_avail_list=$(fn_Metadata_Operation 'image_avail_list')

        echo -e "\nAvailable Image List: "
        select item in $(sed ':a;N;$!ba;s@\n@|@g' <<< "${image_avail_list}"); do
            image_slug="${item}"
            [[ -n "${image_slug}" ]] && break
        done < /dev/tty
        image_slug="${image_slug%% *}"

        # - image size choose
        image_size_avail_list=$(fn_Metadata_Operation 'image_size_avail_list')

        echo -e "\nAvailable Image Size List: "
        select item in $(sed ':a;N;$!ba;s@\n@|@g' <<< "${image_size_avail_list}"); do
            size_slug="${item}"
            [[ -n "${size_slug}" ]] && break
        done < /dev/tty
        size_slug="${size_slug%% *}"

    fi

    IFS=${IFS_BAK}  # Restore IFS
    unset IFS_BAK
    unset PS3
    
    echo -e "\n\nDroplet parameter:\nUsage type '${droplet_usage_type}', Region '${region_slug}', Image '${image_slug}', Image size '${size_slug}', SSH Key '${ssh_key_slug}'\n"
}

fn_Variables_Initialization(){
    # - Variable pre-definition
    # VPC
    vpc_name="${prefix_type_name}-${region_slug}-vpc"
    vpc_description="VPC for ${prefix_type_name} on region ${region_slug}."
    vpc_ip_range='192.168.8.0/24' # IP size 2^8=256
    vpc_default_ip_range='10.118.0.0/20'
    # Cloud Firewalls
    firewall_name="${prefix_type_name}-${region_slug}-firewall"
    # Tag
    tag_name="${prefix_type_name}-${region_slug}-tag"


    # - droplet 'jump'
    tag_name_jump="${tag_name}-${suffix_for_jump}"
    firewall_name_jump="${firewall_name}-${suffix_for_jump}"

    if [[ "${droplet_usage_type}" == 'jump' ]]; then
        local l_public_ip=${l_public_ip:-}
        l_public_ip=$(fn_Public_IP_Detection)
        inbound_rule_jump="protocol:tcp,ports:${ssh_port},address:${l_public_ip}/32" # allow specific IPv4 to visit TCP/22
        outbound_rule_jump='protocol:icmp,address:0.0.0.0/0,address:::/0 protocol:tcp,ports:all,address:0.0.0.0/0,address:::/0 protocol:udp,ports:all,address:0.0.0.0/0,address:::/0' # allow TCP/UDP outbound to any IPv4/IPv6
    fi

    # - droplets connected by droplet 'jump'
    tag_name_jump_control="${tag_name_jump}-${suffix_for_jump_control}"
    firewall_name_jump_control="${firewall_name_jump}-${suffix_for_jump_control}"
    inbound_rule_jump_control="protocol:icmp,tag:${tag_name_jump} protocol:tcp,ports:${ssh_port},tag:${tag_name_jump}" # allow specific IPv4 to visit ICMP & TCP/22 via tag
  
    if [[ "${droplet_usage_type}" != 'jump' ]]; then
        # - droplet 'reverse' (webserver)
        tag_name_webserver="${tag_name}-${suffix_for_webserver}"
        firewall_name_webserver="${firewall_name}-${suffix_for_webserver}"
        inbound_rule_webserver="protocol:tcp,ports:80,address:0.0.0.0/0,address:::/0 protocol:tcp,ports:443,address:0.0.0.0/0,address:::/0" # allow any IPv4 to visit TCP/{80,443}
        outbound_rule_webserver='protocol:tcp,ports:all,address:0.0.0.0/0,address:::/0 protocol:udp,ports:all,address:0.0.0.0/0,address:::/0' # allow outbound to any IPv4/IPv6

        # - droplet 'backend'
        tag_name_backend="${tag_name}-${suffix_for_backend}"
        firewall_name_backend="${firewall_name}-${suffix_for_backend}"
        # tag tag_name_jump_control has defined partial inbound rules
        inbound_rule_backend="protocol:tcp,ports:8000-9000,tag:${tag_name_webserver}"
        outbound_rule_backend='protocol:tcp,ports:all,address:0.0.0.0/0,address:::/0 protocol:udp,ports:all,address:0.0.0.0/0,address:::/0' # allow icmp, outbound to any IPv4/IPv6
    fi
}


#########  2-2. VPC Initialization  #########
fn_VPC_Initialization(){
    # - check if default VPC for specific region exists
    vpc_default_id=${vpc_default_id:-}
    vpc_default_id=$(doctl vpcs list --no-header --format="ID,Name,Region,Default" | sed -r -n 's@[[:space:]]{2,}@\|@g;p' | sed -r -n '/\|default[^\|]+\|'"${region_slug}"'\|/{s@^([^\|]+).*@\1@g;p}')

    # if not exists, create default VPC
    if [[ -z "${vpc_default_id}" ]]; then
        doctl vpcs create --name "default-${region_slug}" --region "${region_slug}" --ip-range "${vpc_default_ip_range}" 1> /dev/null
    fi

    # - create specific VPC
    vpc_id=${vpc_id:-}
    vpc_id=$(doctl vpcs list --no-header --format="Name,ID" | sed -r -n 's@[[:space:]]{2,}@|@g; /^'"${vpc_name}"'\|/{s@^[^\|]+\|@@g;p}')
    [[ -n "${vpc_id}" ]] || vpc_id=$(doctl vpcs create --name "${vpc_name}" --region "${region_slug}" --description "${vpc_description}" --ip-range "${vpc_ip_range}" --output=json | sed -r -n '/"id"/{s@[^:]*:[[:space:]]*"([^"]*)".*@\1@g;p}')
    # solve error info: a VPC with the same name already exists in your account

    # - Update default state of specific VPC
    # change default state from 'false' to 'true', one region just has one active VPC at the same time
    doctl vpcs update "${vpc_id}" --default 'true' --output=json 1> /dev/null
    echo -e "\nVPC Info:"
    doctl vpcs get "${vpc_id}" --format="Name,IPRange,Region,Default"
}

#########  3-1. Drople Create Operation  #########
fn_Droplet_Jump_Control(){
    # - Jump control tag & firewall
    # Create Tag
    doctl compute tag create "${tag_name_jump_control}" 1> /dev/null # --output=json
    # doctl compute tag get "${tag_name_jump_control}"

    # Create Cloud firewall
    doctl compute firewall list --no-header --format="ID,Name" | sed -r -n 's@[[:space:]]{2,}@|@g; /\|'"${firewall_name_jump_control}"'$/{s@^([^\|]+).*@\1@g;p}' | xargs -I % doctl compute firewall delete % -f  # delete existed firewall with same name

    doctl compute firewall create --name "${firewall_name_jump_control}" \
    --inbound-rules "${inbound_rule_jump_control}" \
    --tag-names "${tag_name_jump_control}" \
    --format="Name,Status,Tags,Created" 1> /dev/null
}

fn_Droplet_Create_For_JUMP(){
    # - Create Tag
    doctl compute tag create "${tag_name_jump}" 1> /dev/null # --output=json
    # doctl compute tag get "${tag_name_jump}"

    # - Create Cloud firewall
    doctl compute firewall list --no-header --format="ID,Name" | sed -r -n 's@[[:space:]]{2,}@|@g; /\|'"${firewall_name_jump}"'$/{s@^([^\|]+).*@\1@g;p}' | xargs -I % doctl compute firewall delete % -f  # delete existed firewall with same name

    # Method 1
    doctl compute firewall create --name "${firewall_name_jump}" \
    --inbound-rules "${inbound_rule_jump}" \
    --outbound-rules "${outbound_rule_jump}" \
    --tag-names "${tag_name_jump}" \
    --format="Name,Status,Tags,Created" 1> /dev/null

    # - SSH Key Choose
    # ssh_key_slug_jump=$(doctl compute ssh-key list --no-header --format="ID,Name" | sed -r -n 's@[[:space:]]{2,}@|@g; /\|'"${ssh_name_jump}"'$/{s@^([^\|]+).*@\1@g;p}')
    ssh_key_slug_jump="${ssh_key_slug}"

    # - User Data File
    # echo -e "#!/usr/bin/env bash\nwget -qO- ${sys_init_script_link} > ${script_save_path} || curl -fsL ${sys_init_script_link} > ${script_save_path}\nbash ${script_save_path}" > "${user_data_file}"
    # echo -e '#!/usr/bin/env bash\nwget -qO- '"${sys_init_script_link}"' > '"${script_save_path}"' || curl -fsL '"${sys_init_script_link}"' > '"${script_save_path}"'\nbash '"${script_save_path}"'' > "${user_data_file}" # can execute successfully in command line interface

    echo -e '#!/usr/bin/env bash' > "${user_data_file}"
    ssh_key_backend_data=$(cat "${ssh_key_directory}"/id_ed25519_backend)
    ssh_key_backend_data_public=$(cat "${ssh_key_directory}"/id_ed25519_backend.pub)
    echo -e '\nmkdir /root/.ssh\necho '"'${ssh_key_backend_data}'"' > /root/.ssh/id_ed25519\nchmod 600 /root/.ssh/id_ed25519\necho '"'${ssh_key_backend_data_public}'"' > /root/.ssh/id_ed25519.pub\nchmod 644 /root/.ssh/id_ed25519.pub' >> "${user_data_file}"
    echo -e "\nwget -qO- ${sys_init_script_link} > ${script_save_path} || curl -fsL ${sys_init_script_link} > ${script_save_path}\nbash ${script_save_path} -u ${username_specify}" >> "${user_data_file}"

    # - Create Droplet
    droplet_name_jump="${prefix_type_name}-${region_slug}-${suffix_for_jump}"  # ${image_slug%%-*}

    droplet_id_jump=$(doctl compute droplet create "${droplet_name_jump}" \
        --region "${region_slug}" \
        --image "${image_slug}" \
        --size "${size_slug}" \
        --ssh-keys "${ssh_key_slug_jump}" \
        --vpc-uuid "${vpc_id}" \
        --user-data-file "${user_data_file}" \
        --no-header --format="ID"
        ) # --wait consumes time about 10s
    # --tag-names strings           A list of tag names to be applied to the Droplet

    # Tag droplet (optional, also can specify parameter '--tag-names' in 'doctl compute droplet create')
    doctl compute droplet tag "${droplet_id_jump}" --tag-name "${tag_name_jump}" # tag a droplet, use droplet id or name

    doctl compute droplet get "${droplet_id_jump}" --format="ID,Name,PublicIPv4,PrivateIPv4,Region,Status"

    fn_Droplet_Jump_Control
}

fn_Droplet_Create_For_REVERSE(){
    fn_Droplet_Jump_Control

    # - Create Tag
    doctl compute tag create "${tag_name_webserver}" 1> /dev/null # --output=json
    # doctl compute tag get "${tag_name_webserver}"

    # - Create Cloud firewall
    doctl compute firewall list --no-header --format="ID,Name" | sed -r -n 's@[[:space:]]{2,}@|@g; /\|'"${firewall_name_webserver}"'$/{s@^([^\|]+).*@\1@g;p}' | xargs -I % doctl compute firewall delete % -f  # delete existed firewall with same name
    # Method 1
    doctl compute firewall create --name "${firewall_name_webserver}" \
    --inbound-rules "${inbound_rule_webserver}" \
    --outbound-rules "${outbound_rule_webserver}" \
    --tag-names "${tag_name_webserver}" \
    --format="Name,Status,Tags,Created" 1> /dev/null

    # Method 2
    # firewall_uuid_webserver=$(doctl compute firewall create --name "${firewall_name_webserver}" \
    # --inbound-rules "${inbound_rule_webserver}" \
    # --outbound-rules "${outbound_rule_webserver}" \
    # --no-header --format="ID"
    # )
    # # doctl compute firewall get "${firewall_uuid_webserver}" --format="Name,ID,Status"
    # # Add tag to Cloud Firewalls
    # doctl compute firewall add-tags "${firewall_uuid_webserver}" --tag-names "${tag_name_webserver}"

    # - SSH Key Choose
    # ssh_key_slug_backend=$(doctl compute ssh-key list --no-header --format="ID,Name" | sed -r -n 's@[[:space:]]{2,}@|@g; /\|'"${ssh_name_backend}"'$/{s@^([^\|]+).*@\1@g;p}')
    ssh_key_slug_backend="${ssh_key_slug}"

    # - User Data File
    # echo -e "#!/usr/bin/env bash\nwget -qO- ${sys_init_script_link} > ${script_save_path} || curl -fsL ${sys_init_script_link} > ${script_save_path}\nbash ${script_save_path}" > "${user_data_file}"
    # echo -e '#!/usr/bin/env bash\nwget -qO- '"${sys_init_script_link}"' > '"${script_save_path}"' || curl -fsL '"${sys_init_script_link}"' > '"${script_save_path}"'\nbash '"${script_save_path}"'' > "${user_data_file}" # can execute successfully in command line interface
    echo -e '#!/usr/bin/env bash\n' > "${user_data_file}"
    echo -e "wget -qO- ${sys_init_script_link} > ${script_save_path} || curl -fsL ${sys_init_script_link} > ${script_save_path}\nbash ${script_save_path} -w -u ${username_specify}" >> "${user_data_file}" # install web server (Nginx) via '-w'

    # - Create Droplet
    droplet_name_webserver="${prefix_type_name}-${region_slug}-${suffix_for_webserver}-${droplet_index_num}"  # 422 Only valid hostname characters are allowed. (a-z, A-Z, 0-9, . and -)

    # droplet_id_webserver=$(doctl compute droplet create "${droplet_name_webserver}" \
    #     --region "${region_slug}" \
    #     --image "${image_slug}" \
    #     --size "${size_slug}" \
    #     --ssh-keys "${ssh_key_slug_backend}" \
    #     --vpc-uuid "${vpc_id}" \
    #     --user-data-file "${user_data_file}" \
    #     --no-header --format="ID"
    #     ) # --wait consumes time about 10s
    # # --tag-names strings           A list of tag names to be applied to the Droplet

    # # Tag droplet (optional, also can specify parameter '--tag-names' in 'doctl compute droplet create')
    # doctl compute droplet tag "${droplet_id_webserver}" --tag-name "${tag_name_webserver}" # tag a droplet, use droplet id or name
    # doctl compute droplet tag "${droplet_id_webserver}" --tag-name "${tag_name_jump_control}" # tag a droplet, use droplet id or name

    droplet_id_webserver=$(doctl compute droplet create "${droplet_name_webserver}" \
        --region "${region_slug}" \
        --image "${image_slug}" \
        --size "${size_slug}" \
        --ssh-keys "${ssh_key_slug_backend}" \
        --vpc-uuid "${vpc_id}" \
        --user-data-file "${user_data_file}" \
        --tag-names "${tag_name_jump_control},${tag_name_webserver}" \
        --no-header --format="ID" \
        --wait
        ) # --wait consumes time about 10s
    # --tag-names strings           A list of tag names to be applied to the Droplet

    doctl compute droplet get "${droplet_id_webserver}" --format="ID,Name,PublicIPv4,PrivateIPv4,Region,Status"

    # - For Floating IPs
    echo -e '\nFloating IP Info:'

    floating_ip=${floating_ip:-}
    floating_ip=$(doctl compute floating-ip list --no-header --format="IP,Region,DropletID" | sed -r -n 's@[[:space:]]{2,}@|@g; /\|'"${region_slug}"'\|$/{s@^([^\|]+).*@\1@g;p}') # check if exist floating ip not allocate
    [[ -n "${floating_ip}" ]] || floating_ip=$(doctl compute floating-ip create --region "${region_slug}" --no-header --format="IP")
    sleep 1  # use '--wait' to solve  Error: could not assign IP to droplet: POST https://api.digitalocean.com/v2/floating_ips/138.197.227.220/actions: 422 Droplet already has a pending event.
    doctl compute floating-ip-action assign "${floating_ip}" "${droplet_id_webserver}" 1> /dev/null # --output=json
    # --format="ID,Status,Type,StartedAt,CompletedAt,ResourceID,ResourceType,Region"
    sleep 1
    doctl compute floating-ip get "${floating_ip}" 
}

fn_Droplet_Create_For_BACKEND(){
    fn_Droplet_Jump_Control

    # - Create Tag
    doctl compute tag create "${tag_name_backend}" 1> /dev/null # --output=json
    # doctl compute tag get "${tag_name_backend}"

    # - Create Cloud firewall
    doctl compute firewall list --no-header --format="ID,Name" | sed -r -n 's@[[:space:]]{2,}@|@g; /\|'"${firewall_name_backend}"'$/{s@^([^\|]+).*@\1@g;p}' | xargs -I % doctl compute firewall delete % -f  # delete existed firewall with same name

    firewall_uuid_backend=$(doctl compute firewall create --name "${firewall_name_backend}" \
    --inbound-rules "${inbound_rule_backend}" \
    --outbound-rules "${outbound_rule_backend}" \
    --tag-names "${tag_name_jump_control},${tag_name_backend}" \
    --no-header --format="ID"
    )
    # doctl compute firewall get "${firewall_uuid_backend}" --format="Name,ID,Status,Tags"

    # - SSH Key Choose
    # ssh_key_slug_backend=$(doctl compute ssh-key list --no-header --format="ID,Name" | sed -r -n 's@[[:space:]]{2,}@|@g; /\|'"${ssh_name_backend}"'$/{s@^([^\|]+).*@\1@g;p}')
    ssh_key_slug_backend="${ssh_key_slug}"

    # - User Data File
    # echo -e "#!/usr/bin/env bash\nwget -qO- ${sys_init_script_link} > ${script_save_path} || curl -fsL ${sys_init_script_link} > ${script_save_path}\nbash ${script_save_path}" > "${user_data_file}"
    # echo -e '#!/usr/bin/env bash\nwget -qO- '"${sys_init_script_link}"' > '"${script_save_path}"' || curl -fsL '"${sys_init_script_link}"' > '"${script_save_path}"'\nbash '"${script_save_path}"'' > "${user_data_file}" # can execute successfully in command line interface
    echo -e '#!/usr/bin/env bash\n' > "${user_data_file}"
    echo -e "wget -qO- ${sys_init_script_link} > ${script_save_path} || curl -fsL ${sys_init_script_link} > ${script_save_path}\nbash ${script_save_path} -d -u ${username_specify}" >> "${user_data_file}"  # install Docker via parameter '-d'

    # - Create Droplet
    droplet_name_backend="${prefix_type_name}-${region_slug}-${suffix_for_backend}-${droplet_index_num}"  # 422 Only valid hostname characters are allowed. (a-z, A-Z, 0-9, . and -)

    size_slug_new="${size_slug}"
    # size_slug_new='s-2vcpu-4gb' # more memeory to run Docker

    droplet_id_backend=$(doctl compute droplet create "${droplet_name_backend}" \
        --region "${region_slug}" \
        --image "${image_slug}" \
        --size "${size_slug_new}" \
        --ssh-keys "${ssh_key_slug_backend}" \
        --vpc-uuid "${vpc_id}" \
        --user-data-file "${user_data_file}" \
        --tag-names "${tag_name_jump_control},${tag_name_backend}" \
        --no-header --format="ID"
        ) # --wait consumes time about 10s

    # Tag droplet (optional, also can specify parameter '--tag-names' in 'doctl compute droplet create')
    # doctl compute droplet tag "${droplet_id_backend}" --tag-name "${tag_name_backend}" # tag a droplet, use droplet id or name

    # droplet info
    doctl compute droplet get "${droplet_id_backend}" --format="ID,Name,PublicIPv4,PrivateIPv4,Region,Status,Tags" # --output=json
}


fn_Droplet_Create(){
    fn_Droplet_Paras_Configuration
    fn_Variables_Initialization
    fn_VPC_Initialization

    echo -e "\nDroplet Info (${droplet_usage_type}):"
    fn_Droplet_Create_For_"${droplet_usage_type^^}"
    echo -e "\nNormal user name: ${username_specify}\n"

    [[ -f "${user_data_file}" ]] && rm -f "${user_data_file}"
}


#########  3-2. Drople Delete Operation  #########
fn_Droplet_Delete(){
    # - Getting droplets info
    droplet_infos=$(doctl compute droplet list --no-header --format="Region,ID,Name,VPCUUID,Tags,Volumes" 2> /dev/null | sed -r -n 's@[[:space:]]{2,}@|@g;p')
    # nyc1|234415989|web-nyc1-debian-webserver-01|d8f884f7-cf14-403b-a6d2-6a3ee6ed92a8|web-nyc1-tag-webserver|
    [[ -z "${droplet_infos}" ]] && fn_ExitStatement '\nAttention: no droplet finds in your account.'

    local IFS_BAK=${IFS_BAK:-"$IFS"}  # Backup IFS
    IFS="|" # Setting temporary IFS
    PS3="Choose item number(e.g. 1, 2,...): "

    local l_region_list=''
    l_region_list=$(cut -d\| -f1 <<< "${droplet_infos}" | sort | uniq)
    local l_region_specified=''
    
    if [[ $(sed '/^$/d' <<< "${l_region_list}" | wc -l) -eq 1 ]]; then
        l_region_specified="${l_region_list}"
    else
        echo -e "\nAvailable Region List: "
        select item in $(sed ':a;N;$!ba;s@\n@|@g' <<< "${l_region_list}"); do
            l_region_specified="${item}"
            [[ -n "${l_region_specified}" ]] && break
        done < /dev/tty
    fi

    local l_droplet_type_specified=''
    echo -e "\nAvailable Droplet Type List (region ${l_region_specified}): "
    # rely on droplet name format
    select item in $(sed -r -n '/^'"${l_region_specified}"'\|/{p}' <<< "${droplet_infos}" | cut -d\| -f3 | cut -d- -f3 | sort | uniq | sed ':a;N;$!ba;s@\n@|@g'); do
        l_droplet_type_specified="${item}"
        [[ -n "${l_droplet_type_specified}" ]] && break
    done < /dev/tty

    local l_droplet_specified=''
    echo -e "\nAvailable Droplet List (${l_region_specified}): "
    select item in $(sed -r -n '/^'"${l_region_specified}"'\|[^\|]+\|[^\|]+-'"${l_droplet_type_specified}"'-?/{p}' <<< "${droplet_infos}" | cut -d\| -f3 | sed ':a;N;$!ba;s@\n@|@g'); do
        l_droplet_specified="${item}"
        [[ -n "${l_droplet_specified}" ]] && break
    done < /dev/tty

    IFS=${IFS_BAK}  # Restore IFS
    unset IFS_BAK
    unset PS3

    sed -r -n '/^'"${l_region_specified}"'\|[^\|]+\|'"${l_droplet_specified}"'\|/{p}' <<< "${droplet_infos}" | while read -r droplet_info; do
        region_name=$(cut -d\| -f1 <<< "${droplet_info}")
        droplet_id=$(cut -d\| -f2 <<< "${droplet_info}")
        droplet_name=$(cut -d\| -f3 <<< "${droplet_info}")
        vpc_id=$(cut -d\| -f4 <<< "${droplet_info}")
        tag_names=$(cut -d\| -f5 <<< "${droplet_info}")
        volume_ids=$(cut -d\| -f6 <<< "${droplet_info}")

        echo -e "\nDroplet ${droplet_name} Delete Operation"
        # Delete order: volume, droplet, firewall, tag, floating_ip, [vpc]

        # - Detach & Delete block storage volume
        if [[ -n "${volume_ids}" ]]; then
            echo -e '- Block Storage Volume detaching ...'
            sed -r -n 's@,@\n@g;p' <<< "${volume_ids}" | while read -r volume_id; do
                doctl compute volume-action detach "${volume_id}" "${droplet_id}" --wait &> /dev/null # --output=json
                sleep 1
                echo '- deleting ...'
                doctl compute volume delete "${volume_id}" --force
            done
        fi

        floating_ip=${floating_ip:-}
        [[ "${droplet_name}" =~ -"${suffix_for_webserver}"-[0-9]+ ]] && floating_ip=$(doctl compute floating-ip list --no-header --format="DropletID,IP" | sed -r -n 's@[[:space:]]{2,}@|@g; /^'"${droplet_id}"'\|/{s@^[^\|]+\|@@g;p}')

        # - Delete a droplet
        echo -e '- Droplet deleting ...'
        doctl compute droplet delete "${droplet_id}" --force
        # Warning: Are you sure you want to delete this Droplet? (y/N) ?
        sleep 1

        # - Delete tags
        # if tag dropletcount is 0, then delete it
        sed -r -n 's@,@\n@g;p' <<< "${tag_names}" | while read -r tag_name; do
            if [[ $(doctl compute tag get "${tag_name}" --no-header --format="DropletCount") -eq 0 ]]; then
                echo -e '- Tag deleting ...'
                doctl compute tag delete "${tag_name}" --force
                # doctl compute tag list
                sleep 1
            fi
        done

        doctl compute tag list --no-header --format="Name,DropletCount" | sed -r -n 's@[[:space:]]{2,}@|@g; /\|0$/{s@^([^\|]+).*@\1@g;p}' | xargs -I % doctl compute tag delete % -f

        # - Delete a cloud firewall
        doctl compute firewall list --no-header --format="ID,Name,Tags" | sed -r -n 's@[[:space:]]{2,}@|@g;p' | while IFS="|" read -r firewall_uuid firewall_name firewall_tags; do
            if [[ -z "${firewall_tags}" ]]; then
                echo -e '- Cloud firewall deleting ...'
                doctl compute firewall delete "${firewall_uuid}" --force
            fi
        done

        # - Delete Floating IP
        # Error info: 422 Droplet already has a pending event.
        if [[ -n "${floating_ip}" ]]; then
            echo -e '- Floating IP deleting ...'

            while true; do
                if [[ -n $(doctl compute floating-ip get "${floating_ip}" --no-header --format="DropletID" 2> /dev/null) ]]; then
                    sleep 1
                else
                    break
                fi
            done

            doctl compute floating-ip delete "${floating_ip}" --force
            # Warning: Are you sure you want to delete this firewall? (y/N) ? y
        fi

        # - Delete VPC
        if [[ $(doctl compute droplet list --no-header --format="ID,Region,VPCUUID" | sed -r -n 's@[[:space:]]{2,}@|@g; /\|'"${vpc_id}"'$/{p}' | sed '/^$/d' | wc -l) -lt 1 ]]; then
            echo -e '- VPC deleting ...'
            # mark default vpc as true
            origin_default_vpc_info=${origin_default_vpc_info:-}
            origin_default_vpc_info=$(doctl vpcs list --no-header --format="ID,Name,Region,Default" | sed -r -n 's@[[:space:]]{2,}@\|@g; /default[^\|]+\|'"${region_name}"'\|/{p;q}')
            # a31b76b2-d161-40a4-abbc-034a8f164983|default-nyc1|nyc1|false

            if [[ -n "${origin_default_vpc_info}" ]]; then
                [[ $(cut -d\| -f 4 <<< "${origin_default_vpc_info}") != 'true' ]] && doctl vpcs update "$(cut -d\| -f 1 <<< "${origin_default_vpc_info}")" --default true --output=json 1> /dev/null
            fi
            # doctl vpcs get $vpc_id --format="Name,IPRange,Region,Default"
            sleep 2  # Error: DELETE https://api.digitalocean.com/v2/vpcs/1ed6d4b6-42e1-4a27-b380-99100baa8ffb: 403 (request "21c24c71-08c9-43a3-bf15-da20a156b319") Can not delete VPC with members
            doctl vpcs delete "${vpc_id}" --force
            # Warning: Are you sure you want to delete this VPC? (y/N) ?

            # doctl vpcs list --format="Name,IPRange,Region,Default"
        fi

    done

    exit 0
}


#########  4-1. Cloud Firewall Inbound Rule Update  #########
fn_Jump_SSH_Inbound_IP_Update(){
    # - Getting droplets info
    droplet_infos=$(doctl compute droplet list --no-header --format="Region,ID,Name" 2> /dev/null | sed -r -n 's@[[:space:]]{2,}@|@g; /^[^\|]+\|[^\|]+\|[^\|]+-'"${suffix_for_jump}"'-?/{p}')
    # nyc1|234415989|web-nyc1-debian-webserver-01|d8f884f7-cf14-403b-a6d2-6a3ee6ed92a8|web-nyc1-tag-webserver|
    [[ -z "${droplet_infos}" ]] && fn_ExitStatement "\nAttention: no '${suffix_for_jump}' droplet finds in your account."

    sed '/^$/d' <<< "${droplet_infos}" |  while read -r droplet_info; do
        region_name=$(cut -d\| -f1 <<< "${droplet_info}")
        droplet_id=$(cut -d\| -f2 <<< "${droplet_info}")
        droplet_name=$(cut -d\| -f3 <<< "${droplet_info}")

        echo -e "\nDroplet ${droplet_name} (${region_name}) SSH IP update"

        firewall_uuid=$(doctl compute firewall list-by-droplet "${droplet_id}" --no-header --format="ID")

        public_ip_new=$(fn_Public_IP_Detection)
        inbound_rule_add="protocol:tcp,ports:${ssh_port},address:${public_ip_new}/32"

        # remove old public ip
        inbound_rule_remove=$(doctl compute firewall get "${firewall_uuid}" --no-header --format="InboundRules" | sed -r -n 's@[[:space:]]+@\n@g;p' | sed -r '/ports:'"${ssh_port}"'[^[:digit:]]+/!d' | sed ':a;N;$!ba;s@\n@ @g')
        doctl compute firewall remove-rules "${firewall_uuid}" \
        --inbound-rules "${inbound_rule_remove}" --output=json
        echo "- Old inbound SSH rule: ${inbound_rule_remove}"

        # add new public ip
        doctl compute firewall add-rules "${firewall_uuid}" \
        --inbound-rules "${inbound_rule_add}" --output=json
        echo -e -n "- New inbound SSH rule: "
        doctl compute firewall list-by-droplet "${droplet_id}" --no-header --format="Inbound Rules" |  sed -r -n 's@[[:space:]]+@\n@g;p' | sed -r '/ports:'"${ssh_port}"'[^[:digit:]]+/!d'

    done

    exit
}

#########  5. Executing Process  #########
fn_Initialization_Check

[[ "${is_firewall_update}" -eq 1 ]] && fn_Jump_SSH_Inbound_IP_Update
[[ "${is_delete}" -eq 1 ]] && fn_Droplet_Delete
fn_Droplet_Create

# Script End