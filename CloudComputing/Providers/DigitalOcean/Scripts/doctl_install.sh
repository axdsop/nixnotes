#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails

# Official Site
# - https://www.digitalocean.com/docs/apis-clis/doctl/
# - https://github.com/digitalocean/doctl

# Target: Install/Update doctl On GNU/Linux
# Developer: MaxdSre

# Change Log:
# - Jan 23, 2021 Sat 11:08 ET - code rewrite
# - Jan 22, 2021 Fri 10:45 ET - first draft


#########  0-1. Variables Setting  #########
readonly binary_save_dir='/usr/local/bin'
sys_arch=${sys_arch:-'amd64'} # amd64, arm64, i386
version_check=${version_check:-0}
is_uninstall=${is_uninstall:-0}
sudo_command=${sudo_command:-''}


#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | [sudo] bash -s -- [options] ...

Install/Update DigitalOcean cli utility doctl On GNU/Linux.

Details see https://gitlab.com/axdsop/nixnotes/tree/master/CloudComputing/Providers/DigitalOcean/doctl

[available option]
    -h    --help, show help info
    -c    --check, check latest release version
    -u    --uninstall, uninstall doctl installed
\e[0m"
}

while getopts "hcu" option "$@"; do
    case "$option" in
        c ) version_check=1 ;;
        u ) is_uninstall=1 ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done



#########  1-2 Preparation Operation  #########
fn_CommandExistIfCheck(){
    # $? -- 0 is find, 1 is not find
    local l_name="${1:-}"
    local l_output=${l_output:-1}
    [[ -n "${l_name}" && -n $(which "${l_name}" 2> /dev/null || command -v "${l_name}" 2> /dev/null) ]] && l_output=0
    return "${l_output}"
}

fn_ExitStatement(){
    local l_str="$*"
    if [[ -n "${l_str}" ]]; then
        echo -e "${l_str}\n"
    fi
    exit
}


#########  2-0 Initialization Check  #########
fn_InitializationCheck(){
    # [[ "$UID" -eq 0 ]] && fn_ExitStatement "Sorry: please run this script as normal user whthout root privilege."
    [[ "$UID" -ne 0 ]] && sudo_command='sudo'

    # - Download method
    if fn_CommandExistIfCheck 'curl'; then
        download_method='curl -fsL'
    elif fn_CommandExistIfCheck 'wget'; then
        download_method='wget -qO-'
    else
        fn_ExitStatement "Please install curl or wget firstly."
    fi

    # system arch
    case "$(uname -m)" in
        x86_64) sys_arch='amd64' ;;
        aarch64) sys_arch='arm64' ;;
        i686)
            sys_arch='386' # i386
        ;;
    esac
}

#########  2-1. Uninstalling  #########
fn_UninstallOperation(){
    if [[ "${is_uninstall}" -eq 1 ]]; then
        if fn_CommandExistIfCheck 'doctl'; then
            echo -e 'Uninstalling'
            ${sudo_command} rm -f "${binary_save_dir}/doctl"
            [[ -d /usr/share/bash-completion/completions ]] && ${sudo_command} find /usr/share/bash-completion/completions -type f -name 'doctl' -exec rm -f {} \;
        else
            echo "Sorry, no doctl installed."
        fi

        exit
    fi
}

#########  2-2. Latest & Local Version Check  #########
# Global variables
# - doctl_release_json
# - release_info

fn_VersionComparasion(){
    # get latest release info
    doctl_release_json=$(${download_method} https://api.github.com/repos/digitalocean/doctl/releases/latest 2> /dev/null)
    release_info=$(sed -r -n '/(tag_name|published_at|browser_download_url)/{/browser_download_url/{/linux-'"${sys_arch}"'/!d};s@[^:]*:[[:space:]]*"([^"]*)".*@\1@g;p}' <<< "${doctl_release_json}" | sed ':a;N;$!ba;s@\n@|@g;')
    # v1.55.0|2021-01-21T18:05:46Z|https://github.com/digitalocean/doctl/releases/download/v1.55.0/doctl-1.55.0-linux-amd64.tar.gz
    # v1.96.1|2023-05-29T20:20:40Z|https://github.com/digitalocean/doctl/releases/download/v1.96.1/doctl-1.96.1-linux-amd64.tar.gz

    online_release_version=$(cut -d\| -f1 <<< "${release_info}")
    online_release_date=$(cut -d\| -f2 <<< "${release_info}" | date +'%b %d, %Y' -f - 2> /dev/null)
    echo "Latest release version is ${online_release_version} (${online_release_date})."

    # check if installed
    local l_is_continue=1

    if fn_CommandExistIfCheck 'doctl'; then
        local l_installed_version=''
        l_installed_version=$(doctl version 2> /dev/null | sed -r -n '/version/{s@.*[[:space:]]+([^-]+)-(release|dev).*@\1@g;p}')
        # 0.0.0-dev --> 0.0.0     1.55.0-release --> 1.55.0

        # doctl version 0.0.0-dev
        # release 1.55.0 is available, check it out!

        if [[ "${l_installed_version}" == 0.0.0 ]]; then
            echo "Local doctl existed, but it's outdated."
        elif [[ "${online_release_version}" =~ ^v?"${l_installed_version}"$ ]]; then
            echo "Local existed version is the latest version (${l_installed_version})."
            l_is_continue=0
        fi
    fi

    [[ "${version_check}" -eq 1 || "${l_is_continue}" -eq 0 ]] && fn_ExitStatement ''

    # v1.55.0|2021-01-21T18:05:46Z|https://github.com/digitalocean/doctl/releases/download/v1.55.0/doctl-1.55.0-linux-amd64.tar.gz|439e1d6e0f8f191030e196c662eede59ec1d1f1bbbca75b902c7fe2fc75c6b4a
}


#########  2-3. Download & Decompress  #########
fn_Deployment(){
    temp_save_dir=$(mktemp -d -t XXXXXXXXX)
    pack_save_name="${temp_save_dir}/${release_info##*/}"

    echo "Downloading package ${pack_save_name##*/}"
    ${download_method} "$(cut -d\| -f3 <<< "${release_info}")" > "${pack_save_name}"

    # SHA256 checksum verification
    sha256_checksum=$(sed -r -n '/browser_download_url/{/checksum/!d;s@[^:]*:[[:space:]]*"([^"]*)".*@\1@g;p}' <<< "${doctl_release_json}" | while read -r line; do ${download_method} "${line}" | sed -r -n '/'"${release_info##*/}"'/{s@^[[:space:]]*([^[:space:]]+).*@\1@g;p}'; done) # 439e1d6e0f8f191030e196c662eede59ec1d1f1bbbca75b902c7fe2fc75c6b4a

    if [[ $(sha256sum "${pack_save_name}" 2> /dev/null | sed -r -n 's@^[[:space:]]*([^[:space:]]+).*@\1@g;p') != "${sha256_checksum}" ]]; then
        echo 'Sorry, SHA256 checksums check fails.'
    else
        echo 'SHA256 checksums check approves.'

        echo "Decompressing package to ${temp_save_dir}"
        tar xf "${pack_save_name}" -C "${temp_save_dir}"
        # --strip-components=1 2> /dev/null

        if [[ -f "${temp_save_dir}/doctl" ]]; then
            echo "Copying binary file to ${binary_save_dir}"
            ${sudo_command} mv "${temp_save_dir}/doctl" "${binary_save_dir}"
            ${sudo_command} chown root:root "${binary_save_dir}/doctl"

            bash_completion_dir='/usr/share/bash-completion/completions'
            if [[ -d "${bash_completion_dir}" ]]; then
                echo 'Generating bash completion'
                # https://www.digitalocean.com/docs/apis-clis/doctl/reference/completion/   $SHELL
                doctl completion bash | ${sudo_command} tee "${bash_completion_dir}/doctl" 1> /dev/null
                source "${bash_completion_dir}/doctl"
            fi
        fi

    fi

    echo "Remove temporary directory ${temp_save_dir}"
    [[ -d "${temp_save_dir}" ]] && rm -rf "${temp_save_dir}"

    echo ''
    ${binary_save_dir}/doctl version
}


#########  3. Executing Process  #########
fn_InitializationCheck

fn_UninstallOperation
fn_VersionComparasion
fn_Deployment

# Script End