#!/usr/bin/env bash

# Target: DigitalOcean droplet post-installation system initialization
# Developer: MaxdSre

# Change Log:
# - Mar 29, 2021 Mon 16:58 ET - add new normal user configuration
# - Mar 01, 2021 Mon 19:50 ET - add help info, Nginx install
# - Feb 21, 2021 Feb 15:04 ET - solve dpkg lock issue
# - Jan 25, 2021 Mon 10:15 ET - first draft


# Prefer Ubuntu/Debian

#########  0. Variables Setting  #########
is_docker_install=${is_docker_install:-0}
is_nginx_install=${is_nginx_install:-0}
pack_manager_tool=''
init_log_path="/var/log/doctl-init-$(date +'%Y%m%d').log"
username_specify=${username_specify:-'doctl'}
username_password="Doctl@$(date +'%Y')" # Doctl@2021


#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | bash -s -- [options] ...

DigitalOcean droplet (Debian/Ubuntu) system initialization.

[available option]
    -h    --help, show help info
    -d    --install Docker, default is not install
    -w    --install web server (Nginx), default is not install
    -u USERNAME    --specify normal user name (default '${username_specify}'), init password '${username_password}'
\e[0m"
}

while getopts "hdwu:" option "$@"; do
    case "$option" in
        d ) is_docker_install=1 ;;
        w ) is_nginx_install=1 ;;
        u ) username_specify="$OPTARG" ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done


#########  2. Custom Functions  #########
fn_Distro_Detection(){
    # DigitalOcean supports Ubuntu/Debian, CentOS/Fedora, CoreOS, FreeBSD
    # https://www.digitalocean.com/products/droplets/

    if [[ -s /etc/os-release ]]; then
        local l_distro_release_info=''
        l_distro_release_info=$(sed -r -n 's@=@|@g;s@"@@g;/^$/d;p' /etc/os-release)

        local l_distro_name=''
        l_distro_name=$(sed -r -n '/^ID\|/{s@^.*?\|(.*)$@\L\1@g;p}' <<< "${l_distro_release_info}")

        # DNF is currently used in Fedora, Red Hat Enterprise Linux 8 (RHEL), CentOS 8, OEL 8 and Mageia 6/7.
        # YUM is currently used in Red Hat Enterprise Linux 6/7 (RHEL), CentOS 6/7, OEL 6/7.

        case "${l_distro_name}" in
            ubuntu|debian) pack_manager_tool='apt-get' ;;
            fedora) pack_manager_tool='dnf' ;;
            centos)
                pack_manager_tool='yum'
                [[ -n $(which dnf 2> /dev/null || command -v dnf 2> /dev/null) ]] && pack_manager_tool='dnf'
            ;;
        esac
    fi
}

fn_Normal_User_Create(){
    # - Add normal user into group sudo/wheel without prompt password
    local l_sudo_config_path=${l_sudo_config_path:-'/etc/sudoers'}
    # disable sudo su - / sudo su root
    if [[ "${pack_manager_tool}" == 'apt-get' ]]; then
        sed -r -i 's@#*[[:space:]]*(%sudo[[:space:]]+ALL=\(ALL:ALL\)[[:space:]]+ALL)@# \1@;/%sudo ALL=NOPASSWD:ALL/d;/group sudo/a %sudo ALL=NOPASSWD:ALL,!/bin/su' "${l_sudo_config_path}"
    else
        sed -r -i 's@#*[[:space:]]*(%wheel[[:space:]]+ALL=\(ALL\)[[:space:]]+ALL)@# \1@;s@#*[[:space:]]*(%wheel[[:space:]]+ALL=\(ALL\)[[:space:]]+NOPASSWD: ALL).*@\1,!/bin/su@' "${l_sudo_config_path}"
    fi

    # - Create new normal user
    local l_login_shell=${l_login_shell:-'/bin/bash'}
    # Debian/Ubuntu: sudo      RHEL/OpenSUSE: wheel
    local l_sudo_group_name=${l_sudo_group_name:-'wheel'}
    [[ "${pack_manager_tool}" == 'apt-get' ]] && l_sudo_group_name='sudo'

    useradd -mN -G "${l_sudo_group_name}" "${username_specify}" &> /dev/null
    # change login shell
    usermod -s "${l_login_shell}" "${username_specify}" &> /dev/null

    # Debian/SUSE not support --stdin
    case "${pack_manager_tool}" in
        # https://debian-administration.org/article/668/Changing_a_users_password_inside_a_script
        apt-get ) echo "${username_specify}:${username_password}" | chpasswd &> /dev/null ;;
        dnf|yum ) echo "${username_password}" | passwd --stdin "${username_specify}" &> /dev/null ;;
    esac

    # chage -d0 "${username_specify}" &> /dev/null  # new created user have to change passwd when first login

    # - Copy SSH key from user root
    local l_login_user=''
    local l_login_user_home=''
    [[ "$UID" -eq 0 || "$HOME" =~ ^\/root ]] && l_login_user='root'
    [[ -z "${l_login_user}" ]] && l_login_user=$(logname 2> /dev/null)
    [[ -z "${l_login_user}" ]] && l_login_user=$(id -u -n 2> /dev/null)
    l_login_user_home=$(sed -r -n '/^'"${l_login_user}"':/{p}' /etc/passwd | cut -d: -f6)

    if [[ -f "${l_login_user_home}/.ssh/authorized_keys" ]]; then
        local l_newuser_home=''
        l_newuser_home=$(sed -r -n '/^'"${username_specify}"':/{p}' /etc/passwd | cut -d: -f6)
        # (umask 077; [[ -d "${l_newuser_home}/.ssh" ]] || mkdir -p "${l_newuser_home}/.ssh"; cat "${l_login_user_home}/.ssh/authorized_keys" >> "${l_newuser_home}/.ssh/authorized_keys"; chown -R "${username_specify}" "${l_newuser_home}/.ssh"; chgrp -R users "${l_newuser_home}/.ssh" 2> /dev/null)
        (umask 077; cp -R -p "${l_login_user_home}/.ssh" "${l_newuser_home}/" 2> /dev/null; chown -R "${username_specify}" "${l_newuser_home}/.ssh"; chgrp -R users "${l_newuser_home}/.ssh" 2> /dev/null)
    fi
}

fn_Init_Operation(){
    timedatectl set-timezone America/New_York
    echo "Start time: $(date)" >> "${init_log_path}"

    [[ -f /etc/systemd/resolved.conf ]] && sed -r -i '/^#*(LLMNR|DNSStubListener)=/{s@^#*([^=]+=).*@\1no@g}' /etc/systemd/resolved.conf


    local l_pack_manager_tool=''
    case "${pack_manager_tool}" in
        apt|apt-get)
            # fetch package information
            # Solve error
            # E: Could not get lock /var/lib/dpkg/lock-frontend - open (11: Resource temporarily unavailable)
            # E: Unable to acquire the dpkg frontend lock (/var/lib/dpkg/lock-frontend), is another process using it?
            while true; do
                if [[ -n $(ps -eo pid,comm | sed -r -n '/[[:space:]]*(apt-get|apt:)$/{p}') ]]; then
                    sleep 3
                else
                    break
                fi
            done

            apt-get -yq update
            apt-get -yq upgrade
            apt-get -yq dist-upgrade

            # firewall -   using DigitalOcean cloud firewall
            # apt-get install -y ufw
            # ufw default deny incoming
            # ufw default allow outgoing
            # ufw limit ssh  # or ufw allow ssh
            # ufw --force enable # non-interactive
            # systemctl enable ufw.service
            # systemctl is-enabled ufw.service
            # ufw status verbose

            # essential packages
            apt-get install -y apt-transport-https bash bash-completion curl unzip gawk sed gnupg2 vim tmux jq
            apt-get install -y vnstat
            apt-get install -y p7zip-full

            # Nginx
            if [[ "${is_nginx_install}" -eq 1 ]]; then
                # https://www.nginx.com/resources/wiki/start/topics/tutorials/install/
                distro_name=$(sed -r -n '/^ID=/{s@^[^=]*=@@g;p}' /etc/os-release)
                code_name=$(sed -r -n '/^VERSION_CODENAME=/{s@^[^=]*=@@g;p}' /etc/os-release)
                echo -e "deb https://nginx.org/packages/${distro_name}/ ${code_name} nginx\ndeb-src https://nginx.org/packages/${distro_name}/ ${code_name} nginx" > /etc/apt/sources.list.d/nginx.list
                apt-key adv --keyserver keyserver.ubuntu.com --recv-keys ABF5BD827BD9BF62
                apt-get -yq update
                apt-get install -y nginx
                systemctl enable nginx
            fi

        ;;
        dnf)
            dnf -yq makecache
            dnf -yq upgrade
            # dnf -yq autoremove

            # essential packages
            dnf -yq install bash bash-completion curl unzip gawk sed gnupg2 vim tmux jq
            dnf -yq install vnstat
            dnf -yq install p7zip
        ;;
        yum)
            yum -y -q makecache fast
            yum -y -q install yum-utils

            # There are unfinished transactions remaining. You might consider running yum-complete-transaction, or "yum-complete-transaction --cleanup-only" and "yum history redo last", first to finish them. If those don't work you'll have to try removing/installing packages by hand (maybe package-cleanup can help).

            # Delta RPMs disabled because /usr/bin/applydeltarpm not installed.
            yum -y -q install deltarpm

            # essential packages
            yum -y -q install bash bash-completion curl unzip gawk sed gnupg2 vim tmux 

            # add EPEL Repository
            yum -y -q install epel-release
            yum -y -q install jq vnstat

            yum -y -q install p7zip

            yum -y -q update  # almost 4minutes
            yum -y -q upgrade
        ;;
    esac

    [[ -f /usr/share/bash-completion/completions/tmux ]] || curl -fsSL -o /usr/share/bash-completion/completions/tmux https://raw.githubusercontent.com/imomaliev/tmux-bash-completion/master/completions/tmux

    # Docker
    if [[ "${is_docker_install}" -eq 1 ]]; then
        # https://docs.docker.com/engine/install/debian/#install-using-the-convenience-script
        curl -fsSL https://get.docker.com -o /tmp/get-docker.sh
        bash /tmp/get-docker.sh

        curl -fsSL https://gitlab.com/axdsop/nixnotes/raw/master/CloudComputing/Containers/Docker/Scripts/docker_compose.sh | bash -s --
    fi

    curl -fsL https://rclone.org/install.sh | bash
    # rclone genautocomplete bash rclone.bash_completion
    # mv rclone.bash_completion /usr/share/bash-completion/completions/rclone
    [[ -d /usr/share/bash-completion/completions ]] && rclone genautocomplete bash - | tee /usr/share/bash-completion/completions/rclone 1> /dev/null

    # Fatal error: failed to mount FUSE fs: fusermount: exec: "fusermount": executable file not found in $PATH
    case "${pack_manager_tool}" in
        apt|apt-get) apt-get install -y cloudsql-proxy ;;
    esac

    # Enable TCP BBR
    # Arch Linux just has directory /etc/sysctl.d/
    # https://wiki.archlinux.org/index.php/Sysctl#Enable_BBR
    # https://bbs.archlinux.org/viewtopic.php?id=223879#p1863975
    local l_kernel_version=${l_kernel_version:-}
    l_kernel_version=$(uname -r | sed -r -n 's@^([[:digit:]]+.[[:digit:]]+)..*$@\1@g;p')
    local l_kernel_major=${l_kernel_version%%.*}
    local l_kernel_minor=${l_kernel_version##*.}
    local l_bbr_continue=0

    if [[ "${l_kernel_major}" -gt 4 ]]; then
        l_bbr_continue=1
    elif [[ "${l_kernel_major}" -eq 4 && "${l_kernel_minor}" -ge 9 ]]; then
        l_bbr_continue=1
    fi

    if [[ "${l_bbr_continue}" -eq 1 ]]; then
        # Load the BBR kernel module.
        [[ -d /etc/modules-load.d/ ]] && echo "tcp_bbr" > /etc/modules-load.d/bbr.conf
        # Set the default congestion algorithm to BBR.
        echo "net.core.default_qdisc=fq" > /etc/sysctl.d/bbr.conf
        echo "net.ipv4.tcp_congestion_control=bbr" >> /etc/sysctl.d/bbr.conf
    fi
       
    echo "Finish time: $(date)" >> "${init_log_path}"

    # reboot system
    reboot
}


#########  3. Execute Process  #########
fn_Distro_Detection
fn_Normal_User_Create
fn_Init_Operation


# Script End