# DigitalOcean Block Storage Volumes

Block storage volumes are network-based block devices that provide additional data storage for Droplets. You can move them between Droplets and resize them at any time.

## TOC

1. [Official Resources](#official-resources)  
2. [Introduction](#introduction)  
2.1 [Plans and Pricing](#plans-and-pricing)  
2.2 [Limits](#limits)  
2.3 [Automatically Format & Mount](#automatically-format-mount)  
3. [doctl compute volume](#doctl-compute-volume)  
3.1 [Create](#create)  
3.2 [List](#list)  
3.3 [Get](#get)  
3.4 [Snapshot](#snapshot)  
3.4.1 [doctl compute snapshot](#doctl-compute-snapshot)  
3.4.1.1 [List](#list-1)  
3.4.1.2 [Get](#get-1)  
3.4.1.3 [Delete](#delete)  
3.5 [Delete](#delete-1)  
4. [doctl compute volume-action](#doctl-compute-volume-action)  
4.1 [Attach](#attach)  
4.2 [List](#list-2)  
4.3 [Get](#get-2)  
4.4 [Resize](#resize)  
4.5 [Detach](#detach)  
5. [Issue Occuring](#issue-occuring)  
5.1 [Grub update error for mounted volume](#grub-update-error-for-mounted-volume)  
6. [Change Log](#change-log)  


## Official Resources

* [Block Storage Volumes Overview](https://www.digitalocean.com/docs/volumes/)
* [Volume How-Tos](https://www.digitalocean.com/docs/volumes/how-to/)


## Introduction

### Plans and Pricing

1. Volumes cost $0.10/GiB per month
2. Volumes size range [1, 16,384] GiB (16TiB)
3. Charges accrue hourly for as long as the volume exists

### Limits

1. Volumes are region-specific resources.

    1.1  You cannot rename volumes.

    1.2  You can only move them between Droplets in the same datacenter.

    1.3  By default, users can create [1, 100] volumes and up to a total of 16 TiB of disk space per region.

2. You can only attach a volume to one Droplet at a time.

    2.1 You can attach [1, 7] volumes to any one node or Droplet, and this limit cannot be changed.

3. FreeBSD, CoreOS, RancherOS, and one-click apps do not support automatic formatting and mounting.
4. Volumes are not included in Droplet backups.
5. Tags for volumes are only supported via the API
6. Volumes are available in *NYC1*, *NYC3*, *SFO2*, *FRA1*, *SGP1*, *TOR1*, *BLR1*, *LON1*, and *AMS3*.

    >**NYC3** contains some legacy hardware that does not support block storage volumes. Attach volumes to **NYC3** Droplets during creation to guarantee volume support.


### Automatically Format & Mount

Official doc [Automatically Format & Mount](https://www.digitalocean.com/docs/volumes/how-to/create/#automatically-format--mount) says:

>When you automatically format and mount a volume, you also choose a filesystem. *ext4* is the default because of its stability, backwards compatibility, and mature support and tooling. *XFS*, which specializes in performance for large data files, is also available.
>
>Automatic mounting uses [systemd](https://www.freedesktop.org/software/systemd/man/systemd.mount.html) on distributions that support it. The mount unit files are `/etc/systemd/system/mnt-volume_*.mount`. The udev rules that control the configuration are in `/etc/udev/rules.d/99-digitalocean-automount.rules`.
>
>On non-systemd distributions, automatic mounting uses `fstab`, following the same commands and options provided in the control panel's instructions for manual formatting and mounting.
>
>Volumes are auto-mounted into the `/mnt` directory with the options *defaults,nofail,discard,noatime*.


## doctl compute volume

[doctl][doctl] provides command [doctl compute volume](https://www.digitalocean.com/docs/apis-clis/doctl/reference/compute/volume/) to operate block storage volumes.

Command|Description
---|---
doctl compute volume|Display commands to manage block storage volumes
doctl compute volume create|Create a block storage volume
doctl compute volume delete|Delete a block storage volume
doctl compute volume get|Retrieve an existing block storage volume
doctl compute volume list|List block storage volumes by ID
doctl compute volume snapshot|Create a block storage volume snapshot

Variables definition

```sh
volume_region='nyc1'   # doctl compute region list
volume_name="dev-volume-${volume_region}-01"
volume_description="Personal development testing block storage volumes on region ${volume_region}."
volume_size='100GiB'  # required, range [1, 16,384] GiB (16TiB)
volume_fs_type='xfs'  # ext4 / xfs

volume_tag="tag-for-volume-${volume_region}"

volume_snapshot_desc="Snapshot of volume ${volume_name}"
volume_snapshot_name="${volume_name}-snapshot"
volume_snapshot_tag="tag-for-snapshot-${volume_name}"
```

### Create

Creating a block storage volume on your account. Applying tags to the volume is optional.

```sh
doctl compute tag create "${volume_tag}" --output=json

doctl compute volume create "${volume_name}" \
    --desc "${volume_description}" \
    --region "${volume_region}" \
    --size "${volume_size}" \
    --fs-type "${volume_fs_type}" \
    --tag "${volume_tag}" \
    --output=json

# --size is required

# --fs-label string   Volume filesystem label
# --tag --tag         Tags to apply to the volume; comma separate or repeat --tag to add multiple tags at once

doctl compute tag get "${volume_tag}" --output=json
```

<details><summary>Click to expand json output</summary>

```json
# create tag for volume
[
  {
    "name": "tag-for-volume-nyc1",
    "resources": {
      "count": 0,
      "droplets": {},
      "images": {},
      "volumes": {},
      "volume_snapshots": {},
      "databases": {}
    }
  }
]

# create volume
[
  {
    "id": "82b12c42-745b-11eb-b236-0a58ac1457d9",
    "region": {
      "slug": "nyc1",
      "name": "New York 1",
      "sizes": [
        "s-1vcpu-1gb",
        "s-1vcpu-2gb",
        "s-2vcpu-2gb",
        "s-2vcpu-4gb",
        "s-4vcpu-8gb",
        "c-2",
        "c2-2vcpu-4gb",
        "g-2vcpu-8gb",
        "gd-2vcpu-8gb",
        "s-8vcpu-16gb",
        "m-2vcpu-16gb",
        "c-4",
        "c2-4vcpu-8gb",
        "m3-2vcpu-16gb",
        "g-4vcpu-16gb",
        "so-2vcpu-16gb",
        "m6-2vcpu-16gb",
        "gd-4vcpu-16gb",
        "so1_5-2vcpu-16gb",
        "m-4vcpu-32gb",
        "c-8",
        "c2-8vcpu-16gb",
        "m3-4vcpu-32gb",
        "g-8vcpu-32gb",
        "so-4vcpu-32gb",
        "m6-4vcpu-32gb",
        "gd-8vcpu-32gb",
        "so1_5-4vcpu-32gb",
        "m-8vcpu-64gb",
        "c-16",
        "c2-16vcpu-32gb",
        "m3-8vcpu-64gb",
        "g-16vcpu-64gb",
        "so-8vcpu-64gb",
        "m6-8vcpu-64gb",
        "gd-16vcpu-64gb",
        "so1_5-8vcpu-64gb",
        "m-16vcpu-128gb",
        "c-32",
        "c2-32vcpu-64gb",
        "m3-16vcpu-128gb",
        "m-24vcpu-192gb",
        "g-32vcpu-128gb",
        "so-16vcpu-128gb",
        "m6-16vcpu-128gb",
        "gd-32vcpu-128gb",
        "m3-24vcpu-192gb",
        "so1_5-16vcpu-128gb",
        "m-32vcpu-256gb",
        "so-24vcpu-192gb",
        "m6-24vcpu-192gb",
        "m3-32vcpu-256gb",
        "so1_5-24vcpu-192gb",
        "so-32vcpu-256gb",
        "m6-32vcpu-256gb"
      ],
      "available": true,
      "features": [
        "backups",
        "ipv6",
        "metadata",
        "install_agent",
        "storage",
        "image_transfer"
      ]
    },
    "name": "dev-volume-nyc1-01",
    "size_gigabytes": 100,
    "description": "Personal development testing block storage volumes on region nyc1.",
    "droplet_ids": null,
    "created_at": "2021-02-21T15:43:14Z",
    "filesystem_type": "xfs",
    "filesystem_label": "",
    "tags": [
      "tag-for-volume-nyc1"
    ]
  }
]
# if not specify tag, just show '"tags": null'


# re-check tag info
[
  {
    "name": "tag-for-volume-nyc1",
    "resources": {
      "count": 1,
      "last_tagged_uri": "https://api.digitalocean.com/v2/volumes/82b12c42-745b-11eb-b236-0a58ac1457d9",
      "droplets": {},
      "images": {},
      "volumes": {
        "count": 1,
        "last_tagged_uri": "https://api.digitalocean.com/v2/volumes/82b12c42-745b-11eb-b236-0a58ac1457d9"
      },
      "volume_snapshots": {},
      "databases": {}
    }
  }
]
```

</details>


### List

```sh
doctl compute volume list  # --output=json  # show all volumes
# specify --region= to just show volumes belongs to specific region 

doctl compute volume list --region="${volume_region}" --format="ID,Name,Size,Region,DropletIDs,Tags"
# `Filesystem Type`, `Filesystem Label` prompt errors   Error: unknown column "FilesystemType"
```

<details><summary>Click to expand output</summary>

```txt
ID                                      Name                  Size       Region    Droplet IDs    Tags
82b12c42-745b-11eb-b236-0a58ac1457d9    dev-volume-nyc1-01    100 GiB    nyc1                     tag-for-volume-nyc1
```

</details>

### Get

You can retrieve information about a block storage volume using its ID.

```sh
volume_id='82b12c42-745b-11eb-b236-0a58ac1457d9'

doctl compute volume get "${volume_id}" --format="ID,Size,Region,DropletIDs,Tags"
```

<details><summary>Click to expand output</summary>

```txt
ID                                      Size       Region    Droplet IDs    Tags
82b12c42-745b-11eb-b236-0a58ac1457d9    100 GiB    nyc1                     tag-for-volume-nyc1
```

</details>


### Snapshot

You can create a snapshot of a block storage volume by ID.


```sh
doctl compute tag create "${volume_snapshot_tag}" --output=json

# without output
doctl compute volume snapshot "${volume_id}" \
    --snapshot-name "${volume_snapshot_name}" \
    --snapshot-desc "${volume_snapshot_desc}" \
    --tag "${volume_snapshot_tag}"

# --snapshot-name is required

# --tag --tag    Tags to apply to the snapshot; comma separate or repeat --tag to add multiple tags at once

doctl compute tag get "${volume_snapshot_tag}" --output=json
doctl compute tag list --output=json
```

<details><summary>Click to expand json output</summary>

```json
# create volume snapshot tag
[
  {
    "name": "tag-for-snapshot-dev-volume-nyc1-01",
    "resources": {
      "count": 0,
      "droplets": {},
      "images": {},
      "volumes": {},
      "volume_snapshots": {},
      "databases": {}
    }
  }
]

# create volume snapshot
without output

# re-check volume snapshot tag info
[
  {
    "name": "tag-for-snapshot-dev-volume-nyc1-01",
    "resources": {
      "count": 1,
      "last_tagged_uri": "https://api.digitalocean.com/v2/snapshots/ebb933f6-745b-11eb-bd9d-0a58ac144893",
      "droplets": {},
      "images": {},
      "volumes": {},
      "volume_snapshots": {
        "count": 1,
        "last_tagged_uri": "https://api.digitalocean.com/v2/snapshots/ebb933f6-745b-11eb-bd9d-0a58ac144893"
      },
      "databases": {}
    }
  }
]

# tag list
[
  {
    "name": "tag-for-snapshot-dev-volume-nyc1-01",
    "resources": {
      "count": 1,
      "last_tagged_uri": "https://api.digitalocean.com/v2/snapshots/ebb933f6-745b-11eb-bd9d-0a58ac144893",
      "droplets": {},
      "images": {},
      "volumes": {},
      "volume_snapshots": {
        "count": 1,
        "last_tagged_uri": "https://api.digitalocean.com/v2/snapshots/ebb933f6-745b-11eb-bd9d-0a58ac144893"
      },
      "databases": {}
    }
  },
  {
    "name": "tag-for-volume-nyc1",
    "resources": {
      "count": 1,
      "last_tagged_uri": "https://api.digitalocean.com/v2/volumes/82b12c42-745b-11eb-b236-0a58ac1457d9",
      "droplets": {},
      "images": {},
      "volumes": {
        "count": 1,
        "last_tagged_uri": "https://api.digitalocean.com/v2/volumes/82b12c42-745b-11eb-b236-0a58ac1457d9"
      },
      "volume_snapshots": {},
      "databases": {}
    }
  }
]
```

</details>


#### doctl compute snapshot

##### List

```sh
doctl compute snapshot list --region="${volume_region}" # --output=json
# --format="ID,Name,ResourceId,Regions,ResourceType,MinDiskSize,CreatedAt,Size,Tags"
```

<details><summary>Click to expand json output</summary>

```json
[
  {
    "id": "ebb933f6-745b-11eb-bd9d-0a58ac144893",
    "name": "dev-volume-nyc1-01-snapshot",
    "resource_id": "82b12c42-745b-11eb-b236-0a58ac1457d9",
    "resource_type": "volume",
    "regions": [
      "nyc1"
    ],
    "min_disk_size": 100,
    "created_at": "2021-02-21T15:46:11Z",
    "tags": [
      "tag-for-snapshot-dev-volume-nyc1-01"
    ]
  }
]
# if not specify tag, without key '"tags": []'
```

</details>

##### Get

```sh
volume_snapshot_id='ebb933f6-745b-11eb-bd9d-0a58ac144893'

doctl compute snapshot get "${volume_snapshot_id}" --format="ID,Name,ResourceId,Regions,ResourceType,MinDiskSize,Tags,CreatedAt"
```


##### Delete

```sh
doctl compute snapshot delete "${volume_snapshot_id}" # -f

# -f, --force       Delete the snapshot without confirmation

# doctl compute snapshot list --region="${volume_region}" --output=json # --format="ID,Name,ResourceId,Regions,ResourceType"

# delete volume snapshot tag
doctl compute tag delete "${volume_snapshot_tag}" # -f
```

### Delete

Deleting a block storage volume by ID, destroying all of its data and removing it from your account.

```sh
doctl compute volume delete "${volume_id}" # -f

# -f, --force   Force volume delete

# doctl compute volume list --region="${volume_region}" --output=json # --format="ID,Name,Size,Region,DropletIDs,Tags"

# Delete volume tag
doctl compute tag delete "${volume_tag}" # -f
doctl compute tag list --output=json
```


## doctl compute volume-action

[doctl][doctl] provides command [doctl compute volume-action](https://www.digitalocean.com/docs/apis-clis/doctl/reference/compute/volume/) to operate block storage volumes.

Command|Description
---|---
doctl compute volume-action|Display commands to perform actions on a volume
doctl compute volume-action attach|Attach a volume to a Droplet
doctl compute volume-action detach|Detach a volume from a Droplet
doctl compute volume-action detach-by-droplet-id|(Deprecated) Detach a volume. Use `detach` instead.
doctl compute volume-action get|Retrieve the status of a volume action
doctl compute volume-action list|Retrieve a list of actions taken on a volume
doctl compute volume-action resize|Resize the disk of a volume

For command `detach-by-droplet-id`

>This command detaches a volume. This command is deprecated. Use `doctl compute volume-action detach` instead.

### Attach

Command `doctl compute volume-action attach` help info

>Each volume can be attached to only one Droplet at a time. However, up to five volumes may be attached to a single Droplet.
>
>When you attach a pre-formatted volume to Ubuntu, Debian, Fedora, Fedora Atomic, and CentOS Droplets created on or after April 26, 2018, the volume will be automatically mounted. On older Droplets, additional configuration is required. Visit https://www.digitalocean.com/docs/volumes/how-to/format-and-mount/#mounting-the-filesystems for details

To attach a volume to a droplet, you have 2 methods

1. Attaching volume via parameter `--volumes` while creating droplet (`doctl compute droplet create`)
2. Attaching volume to the existed droplet via command `doctl compute volume-action attach`.

Here choose method 2, using my Shell script [droplet_single_deploy.sh](../../Scripts/droplet_single_deploy.sh) to create a droplet.

<details><summary>Click to expand droplet json info (without volume)</summary>

```json
# droplet info
[
  {
    "id": 232972592,
    "name": "nyc1-debian-01",
    "memory": 4096,
    "vcpus": 2,
    "disk": 80,
    "region": {
      "slug": "nyc1",
      "name": "New York 1",
      "sizes": [
        "s-1vcpu-1gb",
        "s-1vcpu-2gb",
        "s-2vcpu-2gb",
        "s-2vcpu-4gb",
        "s-4vcpu-8gb",
        "c-2",
        "c2-2vcpu-4gb",
        "g-2vcpu-8gb",
        "gd-2vcpu-8gb",
        "s-8vcpu-16gb",
        "m-2vcpu-16gb",
        "c-4",
        "c2-4vcpu-8gb",
        "m3-2vcpu-16gb",
        "g-4vcpu-16gb",
        "so-2vcpu-16gb",
        "m6-2vcpu-16gb",
        "gd-4vcpu-16gb",
        "so1_5-2vcpu-16gb",
        "m-4vcpu-32gb",
        "c-8",
        "c2-8vcpu-16gb",
        "m3-4vcpu-32gb",
        "g-8vcpu-32gb",
        "so-4vcpu-32gb",
        "m6-4vcpu-32gb",
        "gd-8vcpu-32gb",
        "so1_5-4vcpu-32gb",
        "m-8vcpu-64gb",
        "c-16",
        "c2-16vcpu-32gb",
        "m3-8vcpu-64gb",
        "g-16vcpu-64gb",
        "so-8vcpu-64gb",
        "m6-8vcpu-64gb",
        "gd-16vcpu-64gb",
        "so1_5-8vcpu-64gb",
        "m-16vcpu-128gb",
        "c-32",
        "c2-32vcpu-64gb",
        "m3-16vcpu-128gb",
        "m-24vcpu-192gb",
        "g-32vcpu-128gb",
        "so-16vcpu-128gb",
        "m6-16vcpu-128gb",
        "gd-32vcpu-128gb",
        "m3-24vcpu-192gb",
        "so1_5-16vcpu-128gb",
        "m-32vcpu-256gb",
        "so-24vcpu-192gb",
        "m6-24vcpu-192gb",
        "m3-32vcpu-256gb",
        "so1_5-24vcpu-192gb",
        "so-32vcpu-256gb",
        "m6-32vcpu-256gb"
      ],
      "available": true,
      "features": [
        "backups",
        "ipv6",
        "metadata",
        "install_agent",
        "storage",
        "image_transfer"
      ]
    },
    "image": {
      "id": 69440038,
      "name": "10 x64",
      "type": "base",
      "distribution": "Debian",
      "slug": "debian-10-x64",
      "public": true,
      "regions": [
        "nyc3",
        "nyc1",
        "sfo1",
        "nyc2",
        "ams2",
        "sgp1",
        "lon1",
        "ams3",
        "fra1",
        "tor1",
        "sfo2",
        "blr1",
        "sfo3"
      ],
      "min_disk_size": 15,
      "size_gigabytes": 2.15,
      "created_at": "2020-09-02T19:51:15Z",
      "description": "Debian 10 x86 image",
      "status": "available"
    },
    "size": {
      "slug": "s-2vcpu-4gb",
      "memory": 4096,
      "vcpus": 2,
      "disk": 80,
      "price_monthly": 20,
      "price_hourly": 0.02976,
      "regions": [
        "ams2",
        "ams3",
        "blr1",
        "fra1",
        "lon1",
        "nyc1",
        "nyc2",
        "nyc3",
        "sfo1",
        "sfo3",
        "sgp1",
        "tor1"
      ],
      "available": true,
      "transfer": 4
    },
    "size_slug": "s-2vcpu-4gb",
    "features": [
      "private_networking"
    ],
    "status": "new",
    "networks": {
      "v4": [
        {
          "ip_address": "192.168.8.2",
          "netmask": "255.255.255.0",
          "type": "private"
        },
        {
          "ip_address": "157.245.89.42",
          "netmask": "255.255.240.0",
          "gateway": "157.245.80.1",
          "type": "public"
        }
      ]
    },
    "created_at": "2021-02-21T15:51:27Z",
    "tags": [
      "unary-dev-tag-nyc1"
    ],
    "volume_ids": [],
    "vpc_uuid": "2b817195-1249-4a06-809a-f2055f2cdb87"
  }
]
```

</details>


```sh
droplet_id='232972592'

doctl compute volume-action attach "${volume_id}" "${droplet_id}" # --output=json
# --wait   Wait for volume to attach
```

The status of the volume action. This will be either `in-progress`, `completed`, or `errored`.

<details><summary>Click to expand output (in-progress)</summary>

```txt
ID            Status         Type             Started At                       Completed At    Resource ID    Resource Type    Region
1145085300    in-progress    attach_volume    2021-02-21 16:00:51 +0000 UTC    <nil>           0              backend          nyc1
```

</details>

### List

```sh
volume_id='82b12c42-745b-11eb-b236-0a58ac1457d9'

doctl compute volume-action list "${volume_id}" # --output=json

# Get droplet info
doctl compute droplet get "${droplet_id}" --output=json
```

<details><summary>Click to expand output (completed)</summary>

```txt
ID            Status       Type             Started At                       Completed At                     Resource ID    Resource Type    Region
1145085300    completed    attach_volume    2021-02-21 16:00:52 +0000 UTC    2021-02-21 16:00:55 +0000 UTC    0              backend          nyc1
```

</details>

<details><summary>Click to expand json output (completed)</summary>

```json
[
  {
    "id": 1145085300,
    "status": "completed",
    "type": "attach_volume",
    "started_at": "2021-02-21T16:00:52Z",
    "completed_at": "2021-02-21T16:00:55Z",
    "resource_id": 0,
    "resource_type": "backend",
    "region": {
      "slug": "nyc1",
      "name": "New York 1",
      "sizes": [
        "s-1vcpu-1gb",
        "s-1vcpu-2gb",
        "s-2vcpu-2gb",
        "s-2vcpu-4gb",
        "s-4vcpu-8gb",
        "c-2",
        "c2-2vcpu-4gb",
        "g-2vcpu-8gb",
        "gd-2vcpu-8gb",
        "s-8vcpu-16gb",
        "m-2vcpu-16gb",
        "c-4",
        "c2-4vcpu-8gb",
        "m3-2vcpu-16gb",
        "g-4vcpu-16gb",
        "so-2vcpu-16gb",
        "m6-2vcpu-16gb",
        "gd-4vcpu-16gb",
        "so1_5-2vcpu-16gb",
        "m-4vcpu-32gb",
        "c-8",
        "c2-8vcpu-16gb",
        "m3-4vcpu-32gb",
        "g-8vcpu-32gb",
        "so-4vcpu-32gb",
        "m6-4vcpu-32gb",
        "gd-8vcpu-32gb",
        "so1_5-4vcpu-32gb",
        "m-8vcpu-64gb",
        "c-16",
        "c2-16vcpu-32gb",
        "m3-8vcpu-64gb",
        "g-16vcpu-64gb",
        "so-8vcpu-64gb",
        "m6-8vcpu-64gb",
        "gd-16vcpu-64gb",
        "so1_5-8vcpu-64gb",
        "m-16vcpu-128gb",
        "c-32",
        "c2-32vcpu-64gb",
        "m3-16vcpu-128gb",
        "m-24vcpu-192gb",
        "g-32vcpu-128gb",
        "so-16vcpu-128gb",
        "m6-16vcpu-128gb",
        "gd-32vcpu-128gb",
        "m3-24vcpu-192gb",
        "so1_5-16vcpu-128gb",
        "m-32vcpu-256gb",
        "so-24vcpu-192gb",
        "m6-24vcpu-192gb",
        "m3-32vcpu-256gb",
        "so1_5-24vcpu-192gb",
        "so-32vcpu-256gb",
        "m6-32vcpu-256gb"
      ],
      "available": true,
      "features": [
        "backups",
        "ipv6",
        "metadata",
        "install_agent",
        "storage",
        "image_transfer"
      ]
    },
    "region_slug": "nyc1"
  }
]
```

</details>


Droplet info

<details><summary>Click to expand droplet json output (with volume)</summary>

```json
[
  {
    "id": 232972592,
    "name": "nyc1-debian-01",
    "memory": 4096,
    "vcpus": 2,
    "disk": 80,
    "region": {
      "slug": "nyc1",
      "name": "New York 1",
      "sizes": [
        "s-1vcpu-1gb",
        "s-1vcpu-2gb",
        "s-2vcpu-2gb",
        "s-2vcpu-4gb",
        "s-4vcpu-8gb",
        "c-2",
        "c2-2vcpu-4gb",
        "g-2vcpu-8gb",
        "gd-2vcpu-8gb",
        "s-8vcpu-16gb",
        "m-2vcpu-16gb",
        "c-4",
        "c2-4vcpu-8gb",
        "m3-2vcpu-16gb",
        "g-4vcpu-16gb",
        "so-2vcpu-16gb",
        "m6-2vcpu-16gb",
        "gd-4vcpu-16gb",
        "so1_5-2vcpu-16gb",
        "m-4vcpu-32gb",
        "c-8",
        "c2-8vcpu-16gb",
        "m3-4vcpu-32gb",
        "g-8vcpu-32gb",
        "so-4vcpu-32gb",
        "m6-4vcpu-32gb",
        "gd-8vcpu-32gb",
        "so1_5-4vcpu-32gb",
        "m-8vcpu-64gb",
        "c-16",
        "c2-16vcpu-32gb",
        "m3-8vcpu-64gb",
        "g-16vcpu-64gb",
        "so-8vcpu-64gb",
        "m6-8vcpu-64gb",
        "gd-16vcpu-64gb",
        "so1_5-8vcpu-64gb",
        "m-16vcpu-128gb",
        "c-32",
        "c2-32vcpu-64gb",
        "m3-16vcpu-128gb",
        "m-24vcpu-192gb",
        "g-32vcpu-128gb",
        "so-16vcpu-128gb",
        "m6-16vcpu-128gb",
        "gd-32vcpu-128gb",
        "m3-24vcpu-192gb",
        "so1_5-16vcpu-128gb",
        "m-32vcpu-256gb",
        "so-24vcpu-192gb",
        "m6-24vcpu-192gb",
        "m3-32vcpu-256gb",
        "so1_5-24vcpu-192gb",
        "so-32vcpu-256gb",
        "m6-32vcpu-256gb"
      ],
      "available": true,
      "features": [
        "backups",
        "ipv6",
        "metadata",
        "install_agent",
        "storage",
        "image_transfer"
      ]
    },
    "image": {
      "id": 69440038,
      "name": "10 x64",
      "type": "base",
      "distribution": "Debian",
      "slug": "debian-10-x64",
      "public": true,
      "regions": [
        "nyc3",
        "nyc1",
        "sfo1",
        "nyc2",
        "ams2",
        "sgp1",
        "lon1",
        "ams3",
        "fra1",
        "tor1",
        "sfo2",
        "blr1",
        "sfo3"
      ],
      "min_disk_size": 15,
      "size_gigabytes": 2.15,
      "created_at": "2020-09-02T19:51:15Z",
      "description": "Debian 10 x86 image",
      "status": "available"
    },
    "size": {
      "slug": "s-2vcpu-4gb",
      "memory": 4096,
      "vcpus": 2,
      "disk": 80,
      "price_monthly": 20,
      "price_hourly": 0.02976,
      "regions": [
        "ams2",
        "ams3",
        "blr1",
        "fra1",
        "lon1",
        "nyc1",
        "nyc2",
        "nyc3",
        "sfo1",
        "sfo3",
        "sgp1",
        "tor1"
      ],
      "available": true,
      "transfer": 4
    },
    "size_slug": "s-2vcpu-4gb",
    "features": [
      "private_networking"
    ],
    "status": "active",
    "networks": {
      "v4": [
        {
          "ip_address": "192.168.8.2",
          "netmask": "255.255.255.0",
          "type": "private"
        },
        {
          "ip_address": "157.245.89.42",
          "netmask": "255.255.240.0",
          "gateway": "157.245.80.1",
          "type": "public"
        }
      ]
    },
    "created_at": "2021-02-21T15:51:27Z",
    "tags": [
      "unary-dev-tag-nyc1"
    ],
    "volume_ids": [
      "82b12c42-745b-11eb-b236-0a58ac1457d9"
    ],
    "vpc_uuid": "2b817195-1249-4a06-809a-f2055f2cdb87"
  }
]
```

</details>


<details><summary>Click to expand droplet mount info (`/mnt/`)</summary>

```sh
root@nyc1-debian-01:~# lsblk /dev/sda
NAME MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda    8:0    0  100G  0 disk /mnt/dev_volume_nyc1_01

root@nyc1-debian-01:~# ls /etc/systemd/system/mnt-*
/etc/systemd/system/mnt-dev_volume_nyc1_01.mount
root@nyc1-debian-01:~# cat /etc/systemd/system/mnt-dev_volume_nyc1_01.mount
[Unit]
Description=Mount DO Volume dev-volume-nyc1-01

[Mount]
What=/dev/disk/by-uuid/6b552564-0e32-4fa8-9530-dc641090af5e
Where=/mnt/dev_volume_nyc1_01
Options=defaults,nofail,discard,noatime
Type=xfs

[Install]
WantedBy = multi-user.target

root@nyc1-debian-01:~# systemctl status mnt-dev_volume_nyc1_01.mount
● mnt-dev_volume_nyc1_01.mount - Mount DO Volume dev-volume-nyc1-01
   Loaded: loaded (/etc/systemd/system/mnt-dev_volume_nyc1_01.mount; enabled; vendor preset: enabled)
   Active: active (mounted) since Sun 2021-02-21 11:01:07 EST; 25min ago
    Where: /mnt/dev_volume_nyc1_01
     What: /dev/sda
    Tasks: 0 (limit: 4718)
   Memory: 92.0K
   CGroup: /system.slice/mnt-dev_volume_nyc1_01.mount

Feb 21 11:01:07 nyc1-debian-01 systemd[1]: Mounting Mount DO Volume dev-volume-nyc1-01...
Feb 21 11:01:07 nyc1-debian-01 systemd[1]: Mounted Mount DO Volume dev-volume-nyc1-01.
root@nyc1-debian-01:~#
```

</details>


### Get

```sh
volume_action_id='1145085300'

doctl compute volume-action get "${volume_id}" --action-id "${volume_action_id}" # --output=json # --format="ID,Status,Type,Region,ResourceID,ResourceType,StartedAt,CompletedAt"

# --action-id is (required)
```

<details><summary>Click to expand json output</summary>

```json
[
  {
    "id": 1145085300,
    "status": "completed",
    "type": "attach_volume",
    "started_at": "2021-02-21T16:00:52Z",
    "completed_at": "2021-02-21T16:00:55Z",
    "resource_id": 0,
    "resource_type": "backend",
    "region": {
      "slug": "nyc1",
      "name": "New York 1",
      "sizes": [
        "s-1vcpu-1gb",
        "s-1vcpu-2gb",
        "s-2vcpu-2gb",
        "s-2vcpu-4gb",
        "s-4vcpu-8gb",
        "c-2",
        "c2-2vcpu-4gb",
        "g-2vcpu-8gb",
        "gd-2vcpu-8gb",
        "s-8vcpu-16gb",
        "m-2vcpu-16gb",
        "c-4",
        "c2-4vcpu-8gb",
        "m3-2vcpu-16gb",
        "g-4vcpu-16gb",
        "so-2vcpu-16gb",
        "m6-2vcpu-16gb",
        "gd-4vcpu-16gb",
        "so1_5-2vcpu-16gb",
        "m-4vcpu-32gb",
        "c-8",
        "c2-8vcpu-16gb",
        "m3-4vcpu-32gb",
        "g-8vcpu-32gb",
        "so-4vcpu-32gb",
        "m6-4vcpu-32gb",
        "gd-8vcpu-32gb",
        "so1_5-4vcpu-32gb",
        "m-8vcpu-64gb",
        "c-16",
        "c2-16vcpu-32gb",
        "m3-8vcpu-64gb",
        "g-16vcpu-64gb",
        "so-8vcpu-64gb",
        "m6-8vcpu-64gb",
        "gd-16vcpu-64gb",
        "so1_5-8vcpu-64gb",
        "m-16vcpu-128gb",
        "c-32",
        "c2-32vcpu-64gb",
        "m3-16vcpu-128gb",
        "m-24vcpu-192gb",
        "g-32vcpu-128gb",
        "so-16vcpu-128gb",
        "m6-16vcpu-128gb",
        "gd-32vcpu-128gb",
        "m3-24vcpu-192gb",
        "so1_5-16vcpu-128gb",
        "m-32vcpu-256gb",
        "so-24vcpu-192gb",
        "m6-24vcpu-192gb",
        "m3-32vcpu-256gb",
        "so1_5-24vcpu-192gb",
        "so-32vcpu-256gb",
        "m6-32vcpu-256gb"
      ],
      "available": true,
      "features": [
        "backups",
        "ipv6",
        "metadata",
        "install_agent",
        "storage",
        "image_transfer"
      ]
    },
    "region_slug": "nyc1"
  }
]
```

</details>

### Resize

You resize a block storage volume.

Attention: new size is number in GiB. E.g. new size 188GiB, then value specified is *188*, not *188GiB*. Otherwise it will prompts error info

>invalid argument "188GiB" for "--size" flag: strconv.ParseInt: parsing "188GiB": invalid syntax

```sh
volume_size_new='188' # GiB 

doctl compute volume-action resize "${volume_id}" --region "${volume_region}" --size "${volume_size_new}" --output=json

# --region, --size are required
# --wait            Wait for volume to resize
```

<details><summary>Click to expand volume action json output</summary>

```json
[
  {
    "id": 1145100166,
    "status": "done",
    "type": "resize_volume",
    "started_at": "2021-02-21T16:29:58Z",
    "completed_at": "2021-02-21T16:29:58Z",
    "resource_id": 0,
    "resource_type": "backend",
    "region": {
      "slug": "nyc1",
      "name": "New York 1",
      "sizes": [
        "s-1vcpu-1gb",
        "s-1vcpu-2gb",
        "s-2vcpu-2gb",
        "s-2vcpu-4gb",
        "s-4vcpu-8gb",
        "c-2",
        "c2-2vcpu-4gb",
        "g-2vcpu-8gb",
        "gd-2vcpu-8gb",
        "s-8vcpu-16gb",
        "m-2vcpu-16gb",
        "c-4",
        "c2-4vcpu-8gb",
        "m3-2vcpu-16gb",
        "g-4vcpu-16gb",
        "so-2vcpu-16gb",
        "m6-2vcpu-16gb",
        "gd-4vcpu-16gb",
        "so1_5-2vcpu-16gb",
        "m-4vcpu-32gb",
        "c-8",
        "c2-8vcpu-16gb",
        "m3-4vcpu-32gb",
        "g-8vcpu-32gb",
        "so-4vcpu-32gb",
        "m6-4vcpu-32gb",
        "gd-8vcpu-32gb",
        "so1_5-4vcpu-32gb",
        "m-8vcpu-64gb",
        "c-16",
        "c2-16vcpu-32gb",
        "m3-8vcpu-64gb",
        "g-16vcpu-64gb",
        "so-8vcpu-64gb",
        "m6-8vcpu-64gb",
        "gd-16vcpu-64gb",
        "so1_5-8vcpu-64gb",
        "m-16vcpu-128gb",
        "c-32",
        "c2-32vcpu-64gb",
        "m3-16vcpu-128gb",
        "m-24vcpu-192gb",
        "g-32vcpu-128gb",
        "so-16vcpu-128gb",
        "m6-16vcpu-128gb",
        "gd-32vcpu-128gb",
        "m3-24vcpu-192gb",
        "so1_5-16vcpu-128gb",
        "m-32vcpu-256gb",
        "so-24vcpu-192gb",
        "m6-24vcpu-192gb",
        "m3-32vcpu-256gb",
        "so1_5-24vcpu-192gb",
        "so-32vcpu-256gb",
        "m6-32vcpu-256gb"
      ],
      "available": true,
      "features": [
        "backups",
        "ipv6",
        "metadata",
        "install_agent",
        "storage",
        "image_transfer"
      ]
    },
    "region_slug": "nyc1"
  }
]
```

</details>

New volume action id is `1145100166`.

Re-checking volume info

```sh
volume_action_resize_id='1145100166'
doctl compute volume-action get "${volume_id}" --action-id "${volume_action_resize_id}" # --output=json # --format="ID,Status,Type,Region,ResourceID,ResourceType,StartedAt,CompletedAt"

# check volume info
doctl compute volume get "${volume_id}" --format="ID,Size,Region,DropletIDs,Tags"
```

<details><summary>Click to expand volume info output</summary>

```sh
# Volume info after resize
ID                                      Size       Region    Droplet IDs    Tags
82b12c42-745b-11eb-b236-0a58ac1457d9    188 GiB    nyc1      [232972592]    tag-for-volume-nyc1

# Volume info before resize
ID                                      Size       Region    Droplet IDs    Tags
82b12c42-745b-11eb-b236-0a58ac1457d9    100 GiB    nyc1                     tag-for-volume-nyc1
```

</details>

Droplet info

<details><summary>Click to expand droplet mount info (`/mnt/`)</summary>

```sh
# New
root@nyc1-debian-01:~# lsblk /dev/sda
NAME MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda    8:0    0  188G  0 disk /mnt/dev_volume_nyc1_01

# Old
root@nyc1-debian-01:~# lsblk /dev/sda
NAME MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda    8:0    0  100G  0 disk /mnt/dev_volume_nyc1_01
```

</details>

Volume size in droplet has changed without reboot.


### Detach

doctl compute volume-action detach

```sh
doctl compute volume-action detach "${volume_id}" "${droplet_id}" --output=json

# --wait   Wait for volume to attach
```

<details><summary>Click to expand volume detach action json output</summary>

```json
[
  {
    "id": 1145106834,
    "status": "in-progress",
    "type": "detach_volume",
    "started_at": "2021-02-21T16:44:18Z",
    "completed_at": null,
    "resource_id": 0,
    "resource_type": "backend",
    "region": {
      "slug": "nyc1",
      "name": "New York 1",
      "sizes": [
        "s-1vcpu-1gb",
        "s-1vcpu-2gb",
        "s-2vcpu-2gb",
        "s-2vcpu-4gb",
        "s-4vcpu-8gb",
        "c-2",
        "c2-2vcpu-4gb",
        "g-2vcpu-8gb",
        "gd-2vcpu-8gb",
        "s-8vcpu-16gb",
        "m-2vcpu-16gb",
        "c-4",
        "c2-4vcpu-8gb",
        "m3-2vcpu-16gb",
        "g-4vcpu-16gb",
        "so-2vcpu-16gb",
        "m6-2vcpu-16gb",
        "gd-4vcpu-16gb",
        "so1_5-2vcpu-16gb",
        "m-4vcpu-32gb",
        "c-8",
        "c2-8vcpu-16gb",
        "m3-4vcpu-32gb",
        "g-8vcpu-32gb",
        "so-4vcpu-32gb",
        "m6-4vcpu-32gb",
        "gd-8vcpu-32gb",
        "so1_5-4vcpu-32gb",
        "m-8vcpu-64gb",
        "c-16",
        "c2-16vcpu-32gb",
        "m3-8vcpu-64gb",
        "g-16vcpu-64gb",
        "so-8vcpu-64gb",
        "m6-8vcpu-64gb",
        "gd-16vcpu-64gb",
        "so1_5-8vcpu-64gb",
        "m-16vcpu-128gb",
        "c-32",
        "c2-32vcpu-64gb",
        "m3-16vcpu-128gb",
        "m-24vcpu-192gb",
        "g-32vcpu-128gb",
        "so-16vcpu-128gb",
        "m6-16vcpu-128gb",
        "gd-32vcpu-128gb",
        "m3-24vcpu-192gb",
        "so1_5-16vcpu-128gb",
        "m-32vcpu-256gb",
        "so-24vcpu-192gb",
        "m6-24vcpu-192gb",
        "m3-32vcpu-256gb",
        "so1_5-24vcpu-192gb",
        "so-32vcpu-256gb",
        "m6-32vcpu-256gb"
      ],
      "available": true,
      "features": [
        "backups",
        "ipv6",
        "metadata",
        "install_agent",
        "storage",
        "image_transfer"
      ]
    }
  }
]
```

</details>

Droplet info

<details><summary>Click to expand droplet mount info</summary>

```sh
# After detach
root@nyc1-debian-01:~# lsblk
NAME   MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
vda    254:0    0   80G  0 disk
├─vda1 254:1    0   80G  0 part /
└─vda2 254:2    0    2M  0 part
vdb    254:16   0  470K  1 disk
root@nyc1-debian-01:~# lsblk /dev/sda
lsblk: /dev/sda: not a block device
root@nyc1-debian-01:~#

# After resize
root@nyc1-debian-01:~# lsblk /dev/sda
NAME MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda    8:0    0  188G  0 disk /mnt/dev_volume_nyc1_01

# After attach
root@nyc1-debian-01:~# lsblk /dev/sda
NAME MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda    8:0    0  100G  0 disk /mnt/dev_volume_nyc1_01
```

</details>


## Issue Occuring

### Grub update error for mounted volume

I attach block storage volume for a droplet, and it is automaticall format & mount. But when I try to update system via package manager, it prompts errors

>Configuring grub-pc
>
>GRUB failed to install to the following devices:
>
>/dev/sda
>
>Do you want to continue anyway? If you do, your computer may not start up properly.
>
>Writing GRUB to boot device failed continue?

Cloud init log */var/log/cloud-init-output.log* shows

>grub-install: error: hostdisk//dev/sda appears to contain a xfs filesystem which isn't known to reserve space for DOS-style boot.  Installing GRUB there could result in FILESYSTEM DESTRUCTION if valuable data is overwritten by grub-setup (--skip-fs-probe disables this check, use at your own risk).

Try to choose *ext4* as filesystem still prompts the *Configuring grub-pc* error info.

The prefered solution is attach block storage volume after the droplet has runned the system update commands.


## Change Log

* Feb 20, 2021 16:55 Sat ET
  * First draft
* Feb 21, 2021 11:48 Sun ET
  * Add section *volume-action*, update example


[digitalocean]:https://www.digitalocean.com "DititalOcean - The developer cloud"
[doctl]:https://github.com/digitalocean/doctl

<!-- End -->