# DigitalOcean Droplets Opeation

DigitalOcean Droplets are Linux-based virtual machines (VMs) that run on top of virtualized hardware. Each Droplet you create is a new server you can use, either standalone or as part of a larger, cloud-based infrastructure.

Official documentation <https://www.digitalocean.com/docs/droplets/> lists its limits and features:

* [Bandwidth](https://www.digitalocean.com/docs/droplets/#bandwidth)
* [Regional Availability](https://www.digitalocean.com/docs/droplets/#regional-availability)
* [Limits](https://www.digitalocean.com/docs/droplets/#limits)

If you wanna operate in the web interface, just following

* [Droplet Quickstart](https://www.digitalocean.com/docs/droplets/quickstart/)
* [Droplet How-Tos](https://www.digitalocean.com/docs/droplets/how-to/)
* [Droplet Resources](https://www.digitalocean.com/docs/droplets/resources/)


But here I use official command-line interface [doctl][doctl] to operate Droplets actions.

## TOC

1. [Preparation](#preparation)  
1.1 [VPCs & Cloud Firewalls](#vpcs-cloud-firewalls)  
1.2 [Tag](#tag)  
1.3 [SSH Key](#ssh-key)  
1.3.1 [Example](#example)  
2. [Compute Meta Data](#compute-meta-data)  
3. [Droplet Operation](#droplet-operation)  
3.1 [Deploying](#deploying)  
3.2 [droplet-action](#droplet-action)  
3.2.1 [Power Related](#power-related)  
3.2.2 [Miscellaneous](#miscellaneous)  
4. [SSH](#ssh)  
5. [Change Log](#change-log)  


## Preparation

### VPCs & Cloud Firewalls

`VPC`, `Cloud Firewall` can enhance network security.

About `Cloud Firewall`, see note [CloudFirewalls.md](../Networking/CloudFirewalls.md);

About `VPC`, see note [VPC.md](../Networking/VPC.md);

### Tag

`Tag` can make resources management more easy. [doctl][doctl] provides command *doctl compute tag* to operate tags.

Command|Description
---|---
doctl compute tag|Display commands to manage tags
doctl compute tag create|Create a tag
doctl compute tag delete|Delete a tag
doctl compute tag get|Retrieve information about a tag
doctl compute tag list|List all tags

### SSH Key

Supported key types by DigitalOcean are *ssh-rsa*, *ssh-dss*, *ecdsa-sha2-nistp256*, *ecdsa-sha2-nistp384*, *ecdsa-sha2-nistp521*, or *ssh-ed25519*.

OpenSSH supports key types are *dsa*, *rsa*, *ed25519*, *ed25519-sk*, *ecdsa*, *ecdsa-sk*.

Command|Description
---|---
doctl compute ssh-key|Display commands to manage SSH keys on your account
doctl compute ssh-key create|Create a new SSH key on your account
doctl compute ssh-key delete|Permanently delete an SSH key from your account
doctl compute ssh-key get|Retrieve information about an SSH key on your account
doctl compute ssh-key import|Import an SSH key from your computer to your account
doctl compute ssh-key list|List all SSH keys on your account
doctl compute ssh-key update|Update an SSH key's name

```sh
ssh_key_name='test_ssh_key'

# - Method 1 Import existed SSH key
ssh_key_public_path="$HOME/.ssh/id_ed25519.pub"
ssh_key_slug=$(doctl compute ssh-key import "${ssh_key_name}" --public-key-file "${ssh_key_public_path}" --no-header --format="ID")

# - Method 2 Generate new SSH key
ssh_key_dir=$(mktemp -d -t XXXXXXX)
ssh-keygen -t ed25519  -N '' -C 'doctl ssh key' -f ${ssh_key_dir}/id_ed25519
ssh_key_public_str=$(cat ${ssh_key_dir}/id_ed25519.pub)
ssh_key_slug=$(doctl compute ssh-key create "${ssh_key_name}" --public-key "${ssh_key_public_str}" --no-header --format="ID")  # ID or FingerPrint

# - List
doctl compute ssh-key list

# - Get
doctl compute ssh-key get "${ssh_key_slug}" # --output=json
doctl compute ssh-key get "${ssh_key_slug}" --format="Name,FingerPrint"

# - Update (change SSH key's name)
$doctl compute ssh-key update "${ssh_key_slug}" --key-name "${ssh_key_name}_update" --no-header --format="Name"

# - Delete
doctl compute ssh-key delete "${ssh_key_slug}"
# -f, --force   Delete the key without a confirmation prompt

# doctl compute ssh-key list
```

Deleting all existed ssh keys

```sh
doctl compute ssh-key list --no-header --format="ID" | xargs -I % doctl compute ssh-key delete % -f
```

#### Example

Deleting existed SSH keys, then generating new SSH keys.

<details><summary>Click to expand script code</summary>

```sh
#!/usr/bin/env bash

# Target: Generating SSH Key for Digital Ocean
# Date: Mar 01, 2021 Mon 13:32 ET


ssh_name_jump='SSH Jump'
ssh_name_backend='SSH Backend'

# - Delete
echo 'Deleting existed SSH keys...'
doctl compute ssh-key list # --output=json
doctl compute ssh-key list --no-header --format="ID,Name" | sed -r -n 's@[[:space:]]{2,}@|@g; /\|('"${ssh_name_jump}"'|'"${ssh_name_backend}"')/{s@^([^\|]+).*@\1@g;p}' | xargs -I % doctl compute ssh-key delete % -f

# - Create
# Jump Key
echo -e "\nGeneraring key ${ssh_name_jump}"
echo 'y' | ssh-keygen -t ed25519 -N '' -C "DigitalOcean ${ssh_name_jump}" -f $PWD/id_ed25519_jump 1> /dev/null
doctl compute ssh-key import "${ssh_name_jump}" --public-key-file $PWD/id_ed25519_jump.pub # --no-header --format="ID"

# Backend Key
echo -e "\nGeneraring key ${ssh_name_backend}"
echo 'y' | ssh-keygen -t ed25519 -N '' -C "DigitalOcean ${ssh_name_backend}" -f $PWD/id_ed25519_backend 1> /dev/null
doctl compute ssh-key import "${ssh_name_backend}" --public-key-file $PWD/id_ed25519_backend.pub # --no-header --format="ID"

# - List
echo -e '\nListing new SSH keys'
doctl compute ssh-key list --output=json


# Script End
```

</details>

Output

<details><summary>Click to expand output</summary>

```txt
Deleting existed SSH keys...
ID          Name           FingerPrint
29718205    SSH Backend    bf:6c:d2:9d:e2:d7:3a:69:9a:d3:b9:9a:41:75:1a:4b
29718204    SSH Jump       ee:16:e4:52:4a:35:f8:32:fd:d9:2a:dd:60:c8:92:65

Generaring key SSH Jump
ID          Name        FingerPrint
29718223    SSH Jump    85:8e:a8:11:75:16:e7:3e:51:6f:5c:b5:51:fa:c5:3b

Generaring key SSH Backend
ID          Name           FingerPrint
29718224    SSH Backend    62:a7:9c:38:26:cb:16:51:96:ae:3d:43:47:b7:26:95

Listing new SSH keys
[
  {
    "id": 29718224,
    "name": "SSH Backend",
    "fingerprint": "62:a7:9c:38:26:cb:16:51:96:ae:3d:43:47:b7:26:95",
    "public_key": "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBvhc5nSpPLi1Zrjta4FOSRqRjPiyQJfb0+h72Xw8sXL DigitalOcean SSH Backend"
  },
  {
    "id": 29718223,
    "name": "SSH Jump",
    "fingerprint": "85:8e:a8:11:75:16:e7:3e:51:6f:5c:b5:51:fa:c5:3b",
    "public_key": "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBbhsbuc4Kx2fCkhsHOvKedB20eSzK0tWrh7qN44WzH6 DigitalOcean SSH Jump"
  }
]
```

</details>


## Compute Meta Data

`doctl compute` is the most used commands.

```sh
# List Droplets on your account
doctl compute droplet list
# doctl compute droplet list --format="ID,Name,Public IPv4,Private IPv4,Region,Image,Status"
```

Creating a droplet via

```sh
doctl compute droplet create $droplet_name --region $region_slug --image $image_slug --size $size_slug --ssh-keys $size_slug

# parameter --region, --image, --size are required
```

<!-- --user-data string    User-data to configure the Droplet on first boot
--user-data-file string    The path to a file containing user-data to configure the Droplet on first boot -->


List datacenter regions

```sh
doctl compute region list
```

<details><summary>Click to expand</summary>

```txt
Slug    Name               Available
nyc1    New York 1         true
sfo1    San Francisco 1    false
nyc2    New York 2         false
ams2    Amsterdam 2        false
sgp1    Singapore 1        true
lon1    London 1           true
nyc3    New York 3         true
ams3    Amsterdam 3        true
fra1    Frankfurt 1        true
tor1    Toronto 1          true
sfo2    San Francisco 2    true
blr1    Bangalore 1        true
sfo3    San Francisco 3    true
```

</details>


List available distribution images

```sh
doctl compute image list-distribution
```

<details><summary>Click to expand</summary>

```txt
ID          Name                 Type        Distribution    Slug                    Public    Min Disk
54203610    16.04.6 (LTS) x32    snapshot    Ubuntu          ubuntu-16-04-x32        true      20
59416024    12.1 ufs x64         snapshot    FreeBSD         freebsd-12-x64          true      20
65416372    v1.5.6               snapshot    RancherOS       rancheros               true      20
69440038    10 x64               snapshot    Debian          debian-10-x64           true      15
69440042    9 x64                snapshot    Debian          debian-9-x64            true      15
69452245    11.4 zfs x64         snapshot    FreeBSD         freebsd-11-x64-zfs      true      15
69500386    11.4 ufs x64         snapshot    FreeBSD         freebsd-11-x64-ufs      true      15
69535713    7.6 x64              snapshot    CentOS          centos-7-x64            true      20
70639049    32 x64               snapshot    Fedora          fedora-32-x64           true      15
72061309    18.04 (LTS) x64      snapshot    Ubuntu          ubuntu-18-04-x64        true      15
72067660    20.04 (LTS) x64      snapshot    Ubuntu          ubuntu-20-04-x64        true      15
72067667    16.04 (LTS) x64      snapshot    Ubuntu          ubuntu-16-04-x64        true      15
72181180    20.10 x64            snapshot    Ubuntu          ubuntu-20-10-x64        true      15
72465092    33 x64               snapshot    Fedora          fedora-33-x64           true      15
72855737    12.1 ufs x64         snapshot    FreeBSD         freebsd-12-1-x64-ufs    true      20
72903235    12.1 zfs x64         snapshot    FreeBSD         freebsd-12-1-x64-zfs    true      20
74885442    8.3 x64              snapshot    CentOS          centos-8-x64            true      15
76527379    12.2 ufs x64         snapshot    FreeBSD         freebsd-12-x64-ufs      true      20
76530147    12.2 zfs x64         snapshot    FreeBSD         freebsd-12-x64-zfs      true      15
```

</details>


List available Droplet sizes

```sh
doctl compute size list
```

<details><summary>Click to expand</summary>

Old

```txt
Slug           Memory    VCPUs    Disk    Price Monthly    Price Hourly
s-1vcpu-1gb    1024      1        25      5.00             0.007440
s-1vcpu-2gb    2048      1        50      10.00            0.014880
s-2vcpu-2gb    2048      2        60      15.00            0.022320
s-2vcpu-4gb    4096      2        80      20.00            0.029760
```

New

```txt
Slug                 Memory    VCPUs    Disk    Price Monthly    Price Hourly
s-1vcpu-1gb          1024      1        25      5.00             0.007440
s-1vcpu-1gb-amd      1024      1        25      6.00             0.008930
s-1vcpu-1gb-intel    1024      1        25      6.00             0.008930
s-1vcpu-2gb          2048      1        50      10.00            0.014880
s-1vcpu-2gb-amd      2048      1        50      12.00            0.017860
s-1vcpu-2gb-intel    2048      1        50      12.00            0.017860
s-2vcpu-2gb          2048      2        60      15.00            0.022320
s-2vcpu-2gb-amd      2048      2        60      18.00            0.026790
s-2vcpu-2gb-intel    2048      2        60      18.00            0.026790
s-2vcpu-4gb          4096      2        80      20.00            0.029760
```

</details>


Other non-essential commands

```sh
# List all SSH keys on your account
doctl compute ssh-key list

# List all tags
doctl compute tag list

# List block storage volumes by ID
doctl compute volume list

# List the cloud firewalls on your account
doctl compute firewall list
```


## Droplet Operation

### Deploying

>doctl compute droplet create $droplet_name --region $region_slug --image $image_slug --size $size_slug --ssh-keys $size_slug

You can specify user-data via `--user-data string` or `--user-data-file string` to configure the Droplet on first boot. After login the droplet, you can retrieve the user data via

```sh
curl http://169.254.169.254/metadata/v1/user-data
```

More details see [Retrieve User Data](https://www.digitalocean.com/docs/droplets/how-to/provide-user-data/#retrieve-user-data).

I write a sample note [Droplet_Single_Sample.md](../../Samples/Droplet_Single_Sample.md).


### droplet-action

#### Power Related

```sh
# https://www.digitalocean.com/docs/apis-clis/doctl/reference/compute/droplet-action/

# Reboot a droplet
doctl compute droplet-action reboot $droplet_id

# Power off a Droplet
doctl compute droplet-action power-off $droplet_id

# Shut down a Droplet
# doctl compute droplet-action shutdown $droplet_id

# Power on a Droplet
doctl compute droplet-action power-on $droplet_id

# Reboot a Droplet
doctl compute droplet-action reboot $droplet_id
```

#### Miscellaneous

Item|Value
---|---
Change a Droplet's kernel|doctl compute droplet-action change-kernel $droplet_id
Disable backups on a Droplet|doctl compute droplet-action disable-backups $droplet_id
Enable backups on a Droplet|doctl compute droplet-action enable-backups $droplet_id
Enable IPv6 on a Droplet|doctl compute droplet-action enable-ipv6 $droplet_id
Enable private networking on a Droplet|doctl compute droplet-action enable-private-networking $droplet_id
Retrieve a specific Droplet action|doctl compute droplet-action get $droplet_id
Reset the root password for a Droplet|doctl compute droplet-action password-reset $droplet_id
Powercycle a Droplet|doctl compute droplet-action power-cycle $droplet_id
Rebuild a Droplet|doctl compute droplet-action rebuild $droplet_id
Rename a Droplet|doctl compute droplet-action rename $droplet_id
Resize a Droplet|doctl compute droplet-action resize $droplet_id
Restore a Droplet from a backup|doctl compute droplet-action restore $droplet_id
Take a Droplet snapshot|doctl compute droplet-action snapshot $droplet_id


## SSH

<!-- doctl compute ssh-key

doctl compute ssh --ssh-command
doctl compute ssh "${droplet_name}" -->


## Change Log

* Jan 22, 2021 20:12 Fri ET
  * First draft
* Jan 24, 2021 13:12 Sun ET
  * Add `doctl compute ssh-key` operation
* Mar 01, 2021 13:45 Mon ET
  * Add example for section *ssh-key*


[doctl]:https://github.com/digitalocean/doctl

<!-- End -->