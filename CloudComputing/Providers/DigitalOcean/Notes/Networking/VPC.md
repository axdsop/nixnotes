# DigitalOcean Virtual Private Cloud (VPC)

A Virtual Private Cloud (VPC) is a private network interface for collections of DigitalOcean resources. VPC networks provide a more secure connection between resources because the network is inaccessible from the public internet and other VPC networks. **Traffic within a VPC network doesn’t count against bandwidth usage.**

More details in official doc https://www.digitalocean.com/docs/networking/vpc/


## TOC

1. [Official Resources](#official-resources)  
2. [Introduction](#introduction)  
2.1 [Plans and Pricing](#plans-and-pricing)  
2.2 [Limits](#limits)  
3. [IP ranges](#ip-ranges)  
3.1 [Available](#available)  
3.2 [Reserved](#reserved)  
4. [doctl vpcs](#doctl-vpcs)  
4.1 [Create](#create)  
4.2 [List](#list)  
4.3 [Update](#update)  
4.4 [Get](#get)  
4.5 [Delete](#delete)  
5. [Usage](#usage)  
5.1 [Droplet Create](#droplet-create)  
6. [Change Log](#change-log)  


## Official Resources

* [VPC How-Tos](https://www.digitalocean.com/docs/networking/vpc/how-to)
* [An Overview of DigitalOcean VPC and Networking](https://www.digitalocean.com/community/tech_talks/an-overview-of-digitalocean-vpc-and-networking)
* [Build Secure Apps on DigitalOcean with VPC and a Trustworthy Foundation](https://www.digitalocean.com/blog/vpc-trust-platform/)
* [How to Plan Your Custom VPC Network][plan_custom_vpc_network]
* [VPC Best Practices](https://www.digitalocean.com/docs/networking/vpc/resources/best-practices/)


## Introduction

### Plans and Pricing

1. VPC is available in all regions.
2. VPC is available at no additional cost.
3. Traffic between resources inside of a VPC network does not count against your bandwidth billing transfer allowance.

    3.1 Traffic accross VPC networks uses a public interface and does count against your bandwidth allowance.

### Limits

1. Not support VPC networks between resources in different datacenter regions.
2. *Load balancers* or *Kubernetes clusters* doesn't support migrate between VPC networks.

    2.1 *Droplets* can be migrated between networks using snapshots

    2.2 databases can be directly migrated in their Settings tab
3. VPC network ranges cannot overlap with the ranges of other networks in the same account. The IP ranges available for VPC networks are the same as those outlined in RFC 1918.
4. VPCs do not support multicast or broadcast.
5. Resources do not currently support multiple private network interfaces and cannot be placed in multiple VPC networks.

## IP ranges

VPC networks can vary in size from */28* (16 IP) up to */16* (65536 IP). The default size is */20* (4096 usable IP addresses).


### Available

From [How to Plan Your Custom VPC Network][plan_custom_vpc_network], DigitalOcean VPC provides several IP address ranges that have been allocated for private networking purposes ([RFC 1918](https://tools.ietf.org/html/rfc1918)):

* 10.0.0.0 – 10.255.255.255
* 172.16.0.0 – 172.31.255.255
* 192.168.0.0 – 192.168.255.255

Network Size (CIDR Suffix)|Number of Usable IP Addresses
---|---
/16|65536
/17|32768
/18|16384
/19|8192
/20|4096
/21|2048
/22|1024
/23|512
/24|256
/28|16

### Reserved

From [VPC #limits](https://www.digitalocean.com/docs/networking/vpc/#limits), DigitalOcean reserves a few addresses in each VPC network and subnet for backend operations, including the network ID and the broadcast ID.

* 10.244.0.0/16
* 10.245.0.0/16
* 10.246.0.0/24

DigitalOcean also reserve the following IP ranges in the these regions:

<details><summary>Click to expand</summary>

Region|Name|Reserved Range
---|---|---
AMS1|Amsterdam 1|10.11.0.0/16
AMS2|Amsterdam 2|10.14.0.0/16
AMS3|Amsterdam 3|10.18.0.0/16
BLR1|Bangalore 1|10.47.0.0/16
FRA1|Frankfurt 1|10.19.0.0/16
LON1|London 1|10.16.0.0/16
NYC1|New York 1|10.10.0.0/16
NYC2|New York 2|10.13.0.0/16
NYC3|New York 3|10.17.0.0/16
SFO1|San Francisco 1|10.12.0.0/16
SFO2|San Francisco 2|10.46.0.0/16
SFO3|San Francisco 3|10.48.0.0/16
SGP1|Singapore 1|10.15.0.0/16
TOR1|Toronto 1|10.20.0.0/16

</details>

If you wanna plan your custom VPN network size, see [How to Plan Your Custom VPC Network][plan_custom_vpc_network].


## doctl vpcs

[doctl][doctl] provides command *doctl vpcs* to operate vpc.

Command|Description
---|---
doctl vpcs | Display commands that manage VPCs
doctl vpcs create | Create a new VPC
doctl vpcs delete | Permanently delete a VPC
doctl vpcs get | Retrieve a VPC via VPC id
doctl vpcs list | List VPCs
doctl vpcs update | Update a VPC's configuration

Variables definition

```sh
vpc_region='nyc3'   # doctl compute region list
vpc_name="dev-${vpc_region}"
vpc_description="Personal development testing on region ${vpc_region}."
vpc_ip_range='192.168.8.0/24' # IP size 2^8=256, available 256-2=254
```

### Create

```sh
doctl vpcs create --name "${vpc_name}" --region "${vpc_region}" --description "${vpc_description}" --ip-range "${vpc_ip_range}"  # --output=json | jq -r '.[]? | .id'

# --name, --region are required.
```

Example

```sh
doctl vpcs create --name "${vpc_name}" --region "${vpc_region}" --description "${vpc_description}" --ip-range "${vpc_ip_range}" --output=json
```

<details><summary>Click to expand json output</summary>

```json
[
  {
    "id": "0b3419ae-f4fa-44b7-b8dc-357fd52a94c3",
    "urn": "do:vpc:0b3419ae-f4fa-44b7-b8dc-357fd52a94c3",
    "name": "dev-nyc3",
    "description": "Personal development testing on region nyc3.",
    "ip_range": "192.168.8.0/24",
    "region": "nyc3",
    "created_at": "2021-01-23T19:39:38.790371286Z"
  }
]
```

</details>


### List

```sh
doctl vpcs list
# doctl vpcs list --format="ID,Name,IP Range,Region,Default"

doctl vpcs list --output=json
```

<details><summary>Click to expand json output</summary>

```json
[
  {
    "id": "0b3419ae-f4fa-44b7-b8dc-357fd52a94c3",
    "urn": "do:vpc:0b3419ae-f4fa-44b7-b8dc-357fd52a94c3",
    "name": "dev-nyc3",
    "description": "Personal development testing on region nyc3.",
    "ip_range": "192.168.8.0/24",
    "region": "nyc3",
    "created_at": "2021-01-23T19:39:39Z"
  },
  {
    "id": "a31b76b2-d161-40a4-abbc-034a8f164983",
    "urn": "do:vpc:a31b76b2-d161-40a4-abbc-034a8f164983",
    "name": "default-nyc1",
    "ip_range": "10.116.0.0/20",
    "region": "nyc1",
    "created_at": "2021-01-19T00:14:26Z",
    "default": true
  },
  {
    "id": "be9ae0ae-c17f-464e-b093-3877929c658b",
    "urn": "do:vpc:be9ae0ae-c17f-464e-b093-3877929c658b",
    "name": "default-nyc3",
    "ip_range": "10.108.0.0/20",
    "region": "nyc3",
    "created_at": "2021-01-14T17:47:06Z",
    "default": true
  }
]
```

</details>

### Update

You can change a VPC's configuration (default state, description, name) via VPC id.

```sh
vpc_id='0b3419ae-f4fa-44b7-b8dc-357fd52a94c3'

doctl vpcs update "${vpc_id}" --name "test vpc"  # --output=json

doctl vpcs update "${vpc_id}" --description "test vpc description update" --output=json

doctl vpcs update "${vpc_id}" --default 'true' # --output=json | jq -r '.[]? | .default'   # VPC's default state, one region just has one active VPC at the same time
```

<details><summary>Click to expand json output</summary>

```json
# - update VPC name
[
  {
    "id": "0b3419ae-f4fa-44b7-b8dc-357fd52a94c3",
    "urn": "do:vpc:0b3419ae-f4fa-44b7-b8dc-357fd52a94c3",
    "name": "test-vpc",
    "description": "Personal development testing on region nyc3.",
    "ip_range": "192.168.8.0/24",
    "region": "nyc3",
    "created_at": "2021-01-23T19:39:39Z"
  }
]

# - updaet VPC description
[
  {
    "id": "0b3419ae-f4fa-44b7-b8dc-357fd52a94c3",
    "urn": "do:vpc:0b3419ae-f4fa-44b7-b8dc-357fd52a94c3",
    "name": "test-vpc",
    "description": "test vpc description update",
    "ip_range": "192.168.8.0/24",
    "region": "nyc3",
    "created_at": "2021-01-23T19:39:39Z"
  }
]

# - updaet VPC default state
[
  {
    "id": "0b3419ae-f4fa-44b7-b8dc-357fd52a94c3",
    "urn": "do:vpc:0b3419ae-f4fa-44b7-b8dc-357fd52a94c3",
    "name": "test-vpc",
    "description": "test vpc description update",
    "ip_range": "192.168.8.0/24",
    "region": "nyc3",
    "created_at": "2021-01-23T19:39:39Z",
    "default": true
  }
]

# And VPC 'default-nyc3' becomes to state 'false'
[
  {
    "id": "be9ae0ae-c17f-464e-b093-3877929c658b",
    "urn": "do:vpc:be9ae0ae-c17f-464e-b093-3877929c658b",
    "name": "default-nyc3",
    "ip_range": "10.108.0.0/20",
    "region": "nyc3",
    "created_at": "2021-01-14T17:47:06Z"
  },
  {
    "id": "0b3419ae-f4fa-44b7-b8dc-357fd52a94c3",
    "urn": "do:vpc:0b3419ae-f4fa-44b7-b8dc-357fd52a94c3",
    "name": "test-vpc",
    "description": "test vpc description update",
    "ip_range": "192.168.8.0/24",
    "region": "nyc3",
    "created_at": "2021-01-23T19:39:39Z",
    "default": true
  },
  {
    "id": "a31b76b2-d161-40a4-abbc-034a8f164983",
    "urn": "do:vpc:a31b76b2-d161-40a4-abbc-034a8f164983",
    "name": "default-nyc1",
    "ip_range": "10.116.0.0/20",
    "region": "nyc1",
    "created_at": "2021-01-19T00:14:26Z",
    "default": true
  }
]
```

</details>

### Get

```sh
vpc_id='0b3419ae-f4fa-44b7-b8dc-357fd52a94c3'

doctl vpcs get ${vpc_id} --format="Name,IPRange,Region,Default"
```

<details><summary>Click to expand output</summary>

```txt
Name        IP Range          Region    Default
test-vpc    192.168.8.0/24    nyc3      true
```

</details>

### Delete

Before you delete a VPC, please make sure it isn't the default VPC for region. Otherwise it'll prompts error

>Error: DELETE https://api.digitalocean.com/v2/vpcs/0b3419ae-f4fa-44b7-b8dc-357fd52a94c3: 403 (request "1613e23e-7fde-4813-a2b1-9febd3071f99") Can not delete default VPCs

```sh
vpc_id='0b3419ae-f4fa-44b7-b8dc-357fd52a94c3'

default_vpc_id='be9ae0ae-c17f-464e-b093-3877929c658b'
# default vpc name format 'default-nyc3'
# default_vpc_id=$(doctl vpcs list --output=json | jq -r '.[]? | select (.region == "'${vpc_region}'") | select(.name? | match("^default-")) | .id')

doctl vpcs update ${default_vpc_id} --default true

# doctl vpcs get $vpc_id --format="Name,IPRange,Region,Default"

doctl vpcs delete ${vpc_id}
# Warning: Are you sure you want to delete this VPC? (y/N) ?
```


## Usage

### Droplet Create

```sh
doctl compute droplet create <droplet-name>... [flags]

Flags:
      --vpc-uuid string             The UUID of a non-default VPC to create the Droplet in

# Get VPC id
# doctl vpcs list --format="ID,Name,IP Range,Region,Default"
```

Here `--vpc-uuid` is field *ID* in `doctl vpcs list` output.


## Change Log

* Jun 03, 2023 09:01 Sat ET
  * Add `jq` command to extract value
* Jan 23, 2021 15:09 Sat ET
  * First draft


[doctl]:https://github.com/digitalocean/doctl
[plan_custom_vpc_network]:https://www.digitalocean.com/docs/networking/vpc/resources/plan-your-network/ "How to Plan Your Custom VPC Network"

<!-- End -->