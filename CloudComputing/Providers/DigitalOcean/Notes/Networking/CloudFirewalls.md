# DigitalOcean Cloud Firewalls

DigitalOcean Cloud Firewalls are a network-based, stateful firewall service for Droplets provided at no additional cost. It blocks all traffic that isn't expressly permitted by a rule before it reaches the server.

You can apply cloud firewall rules to individual Droplets, but a more powerful option is to use tags. Tags are custom labels that you can apply to Droplets and other DigitalOcean resources. When you add a tag to a firewall, any Droplets with that tag are automatically included in the firewall configuration.

## TOC

1. [Introducation](#introducation)  
1.1 [Limits](#limits)  
2. [doctl compute firewall](#doctl-compute-firewall)  
2.1 [Attention](#attention)  
3. [Usage Example](#usage-example)  
3.1 [Create](#create)  
3.2 [Update](#update)  
3.3 [Get](#get)  
3.4 [Rule Add/Remove](#rule-addremove)  
3.5 [Miscellaneous](#miscellaneous)  
3.5.1 [Droplets](#droplets)  
3.5.2 [Tags](#tags)  
3.6 [Delete](#delete)  
4. [Tutorials](#tutorials)  
5. [Change Log](#change-log)  


## Introducation

1. Cloud Firewalls are available at no additional cost.
2. Cloud firewalls are available in every region. A cloud firewall's rules can include Droplets from any data center.

### Limits

1. At most 10 Droplets per firewall and 5 tags per firewall.

    * If you have more than 10 Droplets that need the same firewall, tag the Droplets, then add that tag to the firewall.

2. Each firewall can have up to 50 total incoming and outgoing rules.
3. Firewalls affect both public and VPC network traffic. Rules specific to either must specify the public or private IP range.
4. Firewalls support only ICMP, TCP, and UDP.
5. Firewalls block traffic at the network layer before that traffic reaches your resources. Because of this, traffic logs are not available.


## doctl compute firewall

[doctl][doctl] provides command *doctl compute firewall* to operate DigitalOcean Cloud Firewalls.

Command|Description
---|---
doctl compute firewall|Display commands to manage cloud firewalls
doctl compute firewall list|List the cloud firewalls on your account
doctl compute firewall create|Create a new cloud firewall
doctl compute firewall get|Retrieve information about a cloud firewall
doctl compute firewall update|Update a cloud firewall's configuration
doctl compute firewall delete|Permanently delete a cloud firewall
doctl compute firewall add-droplets|Add Droplets to a cloud firewall
doctl compute firewall remove-droplets|Remove Droplets from a cloud firewall
doctl compute firewall add-rules|Add inbound or outbound rules to a cloud firewall
doctl compute firewall remove-rules|Remove inbound or outbound rules from a cloud firewall
doctl compute firewall add-tags|Add tags to a cloud firewall
doctl compute firewall remove-tags|Remove tags from a cloud firewall
doctl compute firewall list-by-droplet|List firewalls by Droplet

### Attention

For create command:

1. must specify at least one rule, otherwise it'll prompts error.
2. `--name` need use `-` instead of `_` (e.g. *web-firewall* ok, but *web_firewall* will prompts *invalid name*).


```sh
doctl compute firewall create --name ${firewall_name}

# --name is required

# optional
# --droplet-ids 123,456
# --inbound-rules protocol:tcp,ports:22,droplet_id:123
# --name string
# --outbound-rules protocol:tcp,ports:22,address:0.0.0.0/0
# --tag-names frontend,backend
```

For rules definition, details see official doc [How to Configure Firewall Rules](https://www.digitalocean.com/docs/networking/firewalls/how-to/configure-rules/).

```sh
# port range (6000~8000)
--inbound-rules "protocol:tcp,ports:6000-8000,address:0.0.0.0/0,address:::/0"

# droplet ($droplet_id_1)
--inbound-rules "protocol:tcp,ports:8000-8080,droplet_id:${droplet_id_1}"

# multiple droplet ($droplet_id_1, $droplet_id_2)
--inbound-rules "protocol:tcp,ports:8000-8080,droplet_id:${droplet_id_1},droplet_id:${droplet_id_2}"
```


## Usage Example

Example follows official tutorial [How To Secure Web Server Infrastructure With DigitalOcean Cloud Firewalls Using Doctl](https://www.digitalocean.com/community/tutorials/how-to-secure-web-server-infrastructure-with-digitalocean-cloud-firewalls-using-doctl).

Variables definition

```sh
vpc_region='nyc3'
firewall_name="test-firewall-${vpc_region}"
inbound_rule_default='protocol:tcp,ports:22,address:0.0.0.0/0' # allow any IPv4 to visit TCP/22
outbound_rule_default='protocol:icmp,address:0.0.0.0/0 protocol:tcp,ports:all,address:0.0.0.0/0 protocol:udp,ports:all,address:0.0.0.0/0' # allow icmp, outbound to any IPv4

# IPv4 address:0.0.0.0/0
# IPv6 address:::/0
```

### Create

```sh
doctl compute firewall create --name "${firewall_name}" \
  --inbound-rules "${inbound_rule_default}" \
  --outbound-rules "${outbound_rule_default}" --output=json

# List the cloud firewalls on your account
doctl compute firewall list
```

<details><summary>Click to expand json output</summary>

```json
[
  {
    "id": "f9fc672e-9193-4ec7-a40e-12d2f99f5971",
    "name": "test-firewall-nyc3",
    "status": "succeeded",
    "inbound_rules": [
      {
        "protocol": "tcp",
        "ports": "22",
        "sources": {
          "addresses": [
            "0.0.0.0/0"
          ]
        }
      }
    ],
    "outbound_rules": [
      {
        "protocol": "icmp",
        "ports": "0",
        "destinations": {
          "addresses": [
            "0.0.0.0/0"
          ]
        }
      },
      {
        "protocol": "tcp",
        "ports": "0",
        "destinations": {
          "addresses": [
            "0.0.0.0/0"
          ]
        }
      },
      {
        "protocol": "udp",
        "ports": "0",
        "destinations": {
          "addresses": [
            "0.0.0.0/0"
          ]
        }
      }
    ],
    "droplet_ids": [],
    "tags": [],
    "created_at": "2021-01-24T14:09:32Z",
    "pending_changes": []
  }
]
```

</details>

New generated cloud firewall id is *f9fc672e-9193-4ec7-a40e-12d2f99f5971*.

```sh
firewall_uuid='f9fc672e-9193-4ec7-a40e-12d2f99f5971'
```

### Update

Update a cloud firewall's configuration, just like *Create* operation, can be used to update cloud firewall's name.

```sh
doctl compute firewall update "${firewall_uuid}" --name 

# --name is required

# optional
# --droplet-ids 123,456
# --inbound-rules protocol:tcp,ports:22,droplet_id:123
# --outbound-rules protocol:tcp,ports:22,address:0.0.0.0/0
# --tag-names frontend,backend
```

### Get

```sh
doctl compute firewall get "${firewall_uuid}" --format="ID,Name,Status"

# --format=    Columns for output in a comma-separated list. Possible values: ID, `Name`, `Status`, `Created`, `InboundRules`, `OutboundRules`, `DropletIDs`, `Tags`, `PendingChanges`
```

Output

```txt
ID                                      Name                  Status
f9fc672e-9193-4ec7-a40e-12d2f99f5971    test-firewall-nyc3    succeeded
```

### Rule Add/Remove

Nothing outputs except error occuring.

```sh
inbound_rule_add='protocol:tcp,ports:80,address:0.0.0.0/0' # add TCP/80 inbound
outbound_rule_remove='protocol:icmp,address:0.0.0.0/0' # remove icmp outbound

# add rule
doctl compute firewall add-rules "${firewall_uuid}" \
  --inbound-rules "${inbound_rule_add}" --output=json

# remove rule
doctl compute firewall remove-rules "${firewall_uuid}" \
  --outbound-rules "${outbound_rule_remove}" --output=json

# check
doctl compute firewall get "${firewall_uuid}" --output=json
```

<details><summary>Click to expand json output</summary>

```json
# after add rule
[
  {
    "id": "f9fc672e-9193-4ec7-a40e-12d2f99f5971",
    "name": "test-firewall-nyc3",
    "status": "succeeded",
    "inbound_rules": [
      {
        "protocol": "tcp",
        "ports": "22",
        "sources": {
          "addresses": [
            "0.0.0.0/0"
          ]
        }
      },
      # inbound rule with TCP/80
      {
        "protocol": "tcp",
        "ports": "80",
        "sources": {
          "addresses": [
            "0.0.0.0/0"
          ]
        }
      }
    ],
    "outbound_rules": [
      {
        "protocol": "icmp",
        "ports": "0",
        "destinations": {
          "addresses": [
            "0.0.0.0/0"
          ]
        }
      },
      {
        "protocol": "tcp",
        "ports": "0",
        "destinations": {
          "addresses": [
            "0.0.0.0/0"
          ]
        }
      },
      {
        "protocol": "udp",
        "ports": "0",
        "destinations": {
          "addresses": [
            "0.0.0.0/0"
          ]
        }
      }
    ],
    "droplet_ids": [],
    "tags": [],
    "created_at": "2021-01-24T14:09:32Z",
    "pending_changes": []
  }
]

# after remove rule
[
  {
    "id": "f9fc672e-9193-4ec7-a40e-12d2f99f5971",
    "name": "test-firewall-nyc3",
    "status": "succeeded",
    "inbound_rules": [
      {
        "protocol": "tcp",
        "ports": "22",
        "sources": {
          "addresses": [
            "0.0.0.0/0"
          ]
        }
      },
      {
        "protocol": "tcp",
        "ports": "80",
        "sources": {
          "addresses": [
            "0.0.0.0/0"
          ]
        }
      }
    ],
    # outbound rule without icmp
    "outbound_rules": [
      {
        "protocol": "tcp",
        "ports": "0",
        "destinations": {
          "addresses": [
            "0.0.0.0/0"
          ]
        }
      },
      {
        "protocol": "udp",
        "ports": "0",
        "destinations": {
          "addresses": [
            "0.0.0.0/0"
          ]
        }
      }
    ],
    "droplet_ids": [],
    "tags": [],
    "created_at": "2021-01-24T14:09:32Z",
    "pending_changes": []
  }
]
```

</details>


```sh
doctl compute firewall add-droplets c7b39b43-4fcc-4594-88f2-160a64aaddd4 \
    --droplet-ids 51146959
```

### Miscellaneous

List firewalls by Droplet

```sh
doctl compute firewall list-by-droplet "${droplet_id}"

# --format ID   Columns for output in a comma-separated list. Possible values: ID, `Name`, `Status`, `Created`, `InboundRules`, `OutboundRules`, `DropletIDs`, `Tags`, `PendingChanges`
```

#### Droplets

Droplets add/remove

```sh
droplet_ids='111,222' # or just 111

# add droplets
doctl compute firewall add-droplets "${firewall_uuid}" \
  --droplet-ids "${droplet_ids}"

# remove droplets
doctl compute firewall remove-droplets "${firewall_uuid}" \
  --droplet-ids "${droplet_ids}"

# --droplet-ids 123,456   A comma-separated list of Droplet IDs to place behind the cloud firewall, e.g.: 123,456
```

#### Tags

Tags add/remove, example see [Using Tags](https://www.digitalocean.com/community/tutorials/how-to-secure-web-server-infrastructure-with-digitalocean-cloud-firewalls-using-doctl#step-5-%E2%80%94-using-tags)

```sh
tag_name='aaa'
doctl compute tag create "${tag_name}"
doctl compute droplet tag "${droplet_id}" --tag-name "${tag_name}" # tag a droplet, use droplet id or name

tag_names='aaa' # or aaa,bbb

# add tags to cloud firewall
doctl compute firewall add-tags "${firewall_uuid}" \
  --tag-names "${tag_names}"

# remove tags from cloud firewall
doctl compute firewall remove-tags "${firewall_uuid}" \
  --tag-names "${tag_names}"

# --tag-names frontend,backend   A comma-separated list of tag names to apply to the cloud firewall, e.g.: frontend,backend
```

### Delete

Permanently delete a cloud firewall

```sh
doctl compute firewall delete "${firewall_uuid}"
# Warning: Are you sure you want to delete this firewall? (y/N) ? y

# -f, --force   Delete firewall without confirmation prompt
```


## Tutorials

* [How To Configure a Secure Web App Infrastructure with DigitalOcean Cloud Firewalls](https://www.digitalocean.com/community/tutorials/how-to-configure-a-secure-web-app-infrastructure-with-digitalocean-cloud-firewalls)


## Change Log

* Jan 24, 2021 10:05 Sun ET
  * First draft


[doctl]:https://github.com/digitalocean/doctl

<!-- End -->