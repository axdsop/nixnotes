# Domains and DNS (Not complete)

You can add a domain you own to your [DigitalOcean][digitalocean] account which lets you manage the domain's DNS records with the control panel and API.

Domains you manage on DigitalOcean also integrate with DigitalOcean *Load Balancers* and *Spaces* to streamline automatic SSL certificate management.

## Introducation

Plans and Pricing

* DNS management is available at no additional cost.

Regional Availability

* DNS management is available for DigitalOcean resources in all regions as well as non-DigitalOcean resources.

Features

* [DigitalOcean][digitalocean] currently supports `A`, `AAAA`, `CAA`, `CNAME`, `MX`, `NS`, `TXT`, and `SRV` records. More details see official doc [Supported Record Types](https://www.digitalocean.com/docs/networking/dns/how-to/manage-records/).
* [DigitalOcean][digitalocean] name servers are *ns1.digitalocean.com*, *ns2.digitalocean.com*, and *ns3.digitalocean.com*.

Fully Managed SSL Certificates

Managing domains on DigitalOcean also enables our Let's Encrypt integration for fully managed SSL certificates, which work with [custom Spaces CDN endpoints](https://www.digitalocean.com/docs/spaces/how-to/customize-cdn-endpoint/) and [SSL termination on DigitalOcean Load Balancers](https://www.digitalocean.com/docs/networking/load-balancers/how-to/ssl-termination/#use-lets-encrypt).

### Limits

1. DigitalOcean doesn't provide domain registration services.
2. By default, you can add up to 50 domains.
3. All DNS records require a minimum TTL value of 30 seconds.
4. Records created at a hostname covered by a wildcard record will stop wildcard resolution for that hostname.

    >For example, if you have an A wildcard record at *.example.com, and you add an MX record at the hostname email.example.com, the A wildcard record will no longer be served at email.example.com. However, you can still add an explicit A record to the hostname email.example.com if your use case requires it.
5. DigitalOcean DNS does not support tags.
6. DigitalOcean's terms of service prohibit adding country code top-level domains (ccTLDs) from OFAC-sanctioned countries.


## doctl compute domain

[doctl][doctl] provides command *doctl compute domain* to manage domains you have purchased from a domain name registrar that you are managing through the DigitalOcean DNS interface.

Command|Description
---|---
doctl compute domain|Display commands that manage domains
doctl compute domain create|Add a domain to your account
doctl compute domain delete|Permanently delete a domain from your account
doctl compute domain get|Retrieve information about a domain
doctl compute domain list|List all domains on your account
doctl compute domain records|Manage DNS records
doctl compute domain records create|Create a DNS record
doctl compute domain records delete|Delete a DNS record
doctl compute domain records list|List the DNS records for a domain
doctl compute domain records update|Update a DNS record

## Usage Example

Update your domain's NS records to point to [DigitalOcean][digitalocean]'s name servers *ns1.digitalocean.com*, *ns2.digitalocean.com*, and *ns3.digitalocean.com*. More details see official tutorial [How To Point to DigitalOcean Nameservers From Common Domain Registrars](https://www.digitalocean.com/community/tutorials/how-to-point-to-digitalocean-nameservers-from-common-domain-registrars).

Variables definition

```sh
domain_name='domtest.xyz'
```

### Create

Add a domain to your [DigitalOcean][digitalocean] account.

```sh
doctl compute domain create ${domain_name}

# optional
# --ip-address string   Creates an A record when an IPv4 address is provided

# apt-get install -yq dnsutils
dig @ns1.digitalocean.com $domain_name NS
```

<details><summary>Click to expand output</summary>

```txt
Domain         TTL
domtest.xyz    0
```

Verifing DNS server info

```sh
$ dig @ns1.digitalocean.com $domain_name NS

; <<>> DiG 9.11.5-P4-5.1+deb10u3-Debian <<>> @ns1.digitalocean.com domtest.xyz NS
; (2 servers found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 27098
;; flags: qr aa rd; QUERY: 1, ANSWER: 3, AUTHORITY: 0, ADDITIONAL: 1
;; WARNING: recursion requested but not available

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;domtest.xyz.                   IN      NS

;; ANSWER SECTION:
domtest.xyz.            1800    IN      NS      ns1.digitalocean.com.
domtest.xyz.            1800    IN      NS      ns3.digitalocean.com.
domtest.xyz.            1800    IN      NS      ns2.digitalocean.com.

;; Query time: 7 msec
;; SERVER: 173.245.58.51#53(173.245.58.51)
;; WHEN: Fri Feb 26 19:16:01 EST 2021
;; MSG SIZE  rcvd: 107
```

</details>

### List

Retrieving a list of domains on your account.

```sh
doctl compute domain list --output=json
```

<details><summary>Click to expand json output</summary>

```json
[
  {
    "name": "domtest.xyz",
    "ttl": 1800,
    "zone_file": "$ORIGIN domtest.xyz.\n$TTL 1800\ndomtest.xyz. IN SOA ns1.digitalocean.com. hostmaster.domtest.xyz. 1614384744 10800 3600 604800 1800\ndomtest.xyz. 1800 IN NS ns1.digitalocean.com.\ndomtest.xyz. 1800 IN NS ns2.digitalocean.com.\ndomtest.xyz. 1800 IN NS ns3.digitalocean.com.\n"
  }
]
```

</details>

### Get

Retrieving information about the specified domain on your account.

```sh
doctl compute domain get ${domain_name} # --output=json
```

### Delete

Deleting a domain from your account (irreversible).

```sh
doctl compute domain delete ${domain_name}

# -f, --force   Delete domain without confirmation prompt
```

### Domain Records

Use the subcommands of `doctl compute domain records` to manage the DNS records for domains.

Supported records `A`, `AAAA`, `CAA`, `CNAME`, `MX`, `NS`, `TXT`, and `SRV`.

Official docs

* [How to Create, Edit, and Delete DNS Records](https://www.digitalocean.com/docs/networking/dns/how-to/manage-records/)
* [How to Manage CAA Records](https://www.digitalocean.com/docs/networking/dns/how-to/create-caa-records/)

#### Create

```sh
doctl compute domain records create ${domain_name} [flags]

# optional flags
# --record-type string    The type of DNS record
# --record-data string    Record data; varies depending on record type
# --record-name string    The host name, alias, or service being defined by the record
# --record-priority int   Record priority
# --record-ttl int        The record\'s Time To Live value, in seconds (default 1800)
# --record-flags int      An unsigned integer between 0-255 used for CAA records
# --record-tag issue      The parameter tag for CAA records. Valid values are issue, `issuewild`, or `iodef`
# --record-port int       The port value for an SRV record
# --record-weight int     The weight value for an SRV record

# https://www.digitalocean.com/docs/networking/dns/how-to/create-caa-records/
```

Certificate Authority Authorization (CAA) is a standard designed to prevent bad actors from creating unauthorized SSL/TLS certificates. CAA records specify which certificate authorities (CAs) can issue certificates.


```sh
doctl compute domain records create ${domain_name} --record-type='CAA' --record-tag='issuewild'  --record-data ''

# --record-flags int      An unsigned integer between 0-255 used for CAA records
# --record-tag issue      The parameter tag for CAA records. Valid values are issue, `issuewild`, or `iodef`

doctl compute domain records create ${domain_name} --record-type='A' --record-data 'public IP addr'
```

https://letsencrypt.org/docs/client-options/

https://github.com/acmesh-official/acme.sh


#### List

#### Update

#### Delete


## Change Log

* Feb 25, 2021 17:05 Thu ET
  * First draft


[digitalocean]:https://www.digitalocean.com "DititalOcean - The developer cloud"
[doctl]:https://github.com/digitalocean/doctl

<!-- End -->