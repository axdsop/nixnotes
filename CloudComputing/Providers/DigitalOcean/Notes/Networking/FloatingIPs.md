# Floating IPs

The Floating IP service is now called Reserved IPs. The Reserved IP service retains the same functionality as the prior service. (2023/06/03)

>We have renamed the Floating IP product to Reserved IPs. The Reserved IP service retains the same functionality as the prior service. (Source https://docs.digitalocean.com/release-notes/#2022-06-16)

[DigitalOcean][digitalocean] floating IPs are publicly-accessible static IP addresses that you can assign to Droplets and instantly remap between other Droplets in the same datacenter. Implement a failover mechanism with floating IPs to build a high availability infrastructure.

Official doc [Floating IPs](https://www.digitalocean.com/docs/networking/floating-ips/).


## TOC

1. [Introduction](#introduction)  
1.1 [Limits](#limits)  
2. [doctl compute floating-ip](#doctl-compute-floating-ip)  
2.1 [Create](#create)  
2.1.1 [floating-ip-action](#floating-ip-action)  
2.1.1.1 [Assign](#assign)  
2.1.1.2 [Get](#get)  
2.1.1.3 [Unassign](#unassign)  
2.2 [List](#list)  
2.3 [Get](#get-1)  
2.4 [Delete](#delete)  
3. [Change Log](#change-log)  


## Introduction

Floating IPs are publicly-accessible static IP addresses that can be mapped to one of your Droplets. Floating IPs are bound to a specific region.

Plans and Pricing

* Floating IPs are free when assigned to a Droplet.
* Floating IPs cost $4/month (roughly $0.006/hour) when reserved but not assigned to a Droplet, due to the shortage of available IPv4 addresses. You are not billed unless you accrue $1 or more per floating IP.

Regional Availability

* Floating IPs are available in all datacenters.

  >They are region-specific resources and can only be assigned to Droplets within the same datacenter.

Features

* Floating IPs let you redirect network traffic between any of your Droplets within the same datacenter.

  >Assigning a floating IP to a Droplet doesn’t replace or change its original public IP address.

### Limits

1. By default, you can reserve three floating IPs per account.
2. Floating IPs cannot be assigned to more than one Droplet at a time.
3. Floating IPs do not support PTR (rDNS) records.
4. All floating IPs are IPv4.
5. Floating IPs don't support for DigitalOcean Kubernetes worker nodes.
6. A Droplet must have an anchor IP before you can assign a floating IP to it.
7. Floating IPs do not support SMTP traffic.


## doctl compute floating-ip

[doctl][doctl] provides command *floating-ip* to manage DigitalOcean floating IP addresses.

Official tutorial [Working with Floating IPs](https://www.digitalocean.com/community/tutorials/how-to-use-doctl-the-official-digitalocean-command-line-client#working-with-floating-ips).

Command|Description
---|---
doctl compute floating-ip|Display commands to manage floating IP addresses
doctl compute floating-ip create|Create a new floating IP address
doctl compute floating-ip delete|Permanently delete a floating IP address
doctl compute floating-ip get|Retrieve information about a floating IP address
doctl compute floating-ip list|List all floating IP addresses on your account
doctl compute floating-ip-action|Display commands to associate floating IP addresses with Droplets
doctl compute floating-ip-action assign|Assign a floating IP address to a Droplet
doctl compute floating-ip-action get|Retrieve the status of a floating IP action
doctl compute floating-ip-action unassign|Unassign a floating IP address from a Droplet

Variables preparation

```sh
region_slug='nyc1'  # doctl compute region list
```

### Create

```sh
doctl compute floating-ip create --region "${region_slug}"


# --droplet-id --region   The ID of the Droplet to assign the floating IP to (mutually exclusive with --region).
# --region --droplet-id   Region where to create the floating IP address. (mutually exclusive with --droplet-id)
```

<details><summary>Click to expand output</summary>

```txt
IP                Region    Droplet ID    Droplet Name
167.172.15.100    nyc1
```

</details>

New floating IP

```sh
floating_ip='167.172.15.100'
```

#### floating-ip-action

Here choose method 2, using my Shell script [droplet_single_deploy.sh](../../Scripts/droplet_single_deploy.sh) to create a droplet.

<details><summary>Click to expand droplet json info</summary>

```json
[
  {
    "id": 233956260,
    "name": "nyc1-debian-01",
    "memory": 2048,
    "vcpus": 2,
    "disk": 60,
    "region": {
      "slug": "nyc1",
      "name": "New York 1",
      "sizes": [
        "s-1vcpu-1gb",
        "s-1vcpu-1gb-intel",
        "s-1vcpu-2gb",
        "s-1vcpu-2gb-intel",
        "s-2vcpu-2gb",
        "s-2vcpu-2gb-intel",
        "s-2vcpu-4gb",
        "s-2vcpu-4gb-intel",
        "s-4vcpu-8gb",
        "c-2",
        "c2-2vcpu-4gb",
        "s-4vcpu-8gb-intel",
        "g-2vcpu-8gb",
        "gd-2vcpu-8gb",
        "s-8vcpu-16gb",
        "m-2vcpu-16gb",
        "c-4",
        "c2-4vcpu-8gb",
        "s-8vcpu-16gb-intel",
        "m3-2vcpu-16gb",
        "g-4vcpu-16gb",
        "so-2vcpu-16gb",
        "m6-2vcpu-16gb",
        "gd-4vcpu-16gb",
        "so1_5-2vcpu-16gb",
        "m-4vcpu-32gb",
        "c-8",
        "c2-8vcpu-16gb",
        "m3-4vcpu-32gb",
        "g-8vcpu-32gb",
        "so-4vcpu-32gb",
        "m6-4vcpu-32gb",
        "gd-8vcpu-32gb",
        "so1_5-4vcpu-32gb",
        "m-8vcpu-64gb",
        "c-16",
        "c2-16vcpu-32gb",
        "m3-8vcpu-64gb",
        "g-16vcpu-64gb",
        "so-8vcpu-64gb",
        "m6-8vcpu-64gb",
        "gd-16vcpu-64gb",
        "so1_5-8vcpu-64gb",
        "m-16vcpu-128gb",
        "c-32",
        "c2-32vcpu-64gb",
        "m3-16vcpu-128gb",
        "m-24vcpu-192gb",
        "g-32vcpu-128gb",
        "so-16vcpu-128gb",
        "m6-16vcpu-128gb",
        "gd-32vcpu-128gb",
        "m3-24vcpu-192gb",
        "g-40vcpu-160gb",
        "so1_5-16vcpu-128gb",
        "m-32vcpu-256gb",
        "gd-40vcpu-160gb",
        "so-24vcpu-192gb",
        "m6-24vcpu-192gb",
        "m3-32vcpu-256gb",
        "so1_5-24vcpu-192gb",
        "so-32vcpu-256gb",
        "m6-32vcpu-256gb"
      ],
      "available": true,
      "features": [
        "backups",
        "ipv6",
        "metadata",
        "install_agent",
        "storage",
        "image_transfer"
      ]
    },
    "image": {
      "id": 69440038,
      "name": "10 x64",
      "type": "base",
      "distribution": "Debian",
      "slug": "debian-10-x64",
      "public": true,
      "regions": [
        "nyc3",
        "nyc1",
        "sfo1",
        "nyc2",
        "ams2",
        "sgp1",
        "lon1",
        "ams3",
        "fra1",
        "tor1",
        "sfo2",
        "blr1",
        "sfo3"
      ],
      "min_disk_size": 15,
      "size_gigabytes": 2.15,
      "created_at": "2020-09-02T19:51:15Z",
      "description": "Debian 10 x86 image",
      "status": "available"
    },
    "size": {
      "slug": "s-2vcpu-2gb",
      "memory": 2048,
      "vcpus": 2,
      "disk": 60,
      "price_monthly": 15,
      "price_hourly": 0.02232,
      "regions": [
        "ams2",
        "ams3",
        "blr1",
        "fra1",
        "lon1",
        "nyc1",
        "nyc2",
        "nyc3",
        "sfo1",
        "sfo3",
        "sgp1",
        "tor1"
      ],
      "available": true,
      "transfer": 3,
      "description": "Legacy Basic"
    },
    "size_slug": "s-2vcpu-2gb",
    "features": [
      "private_networking"
    ],
    "status": "new",
    "networks": {
      "v4": [
        {
          "ip_address": "192.168.8.2",
          "netmask": "255.255.255.0",
          "type": "private"
        },
        {
          "ip_address": "134.209.125.245",
          "netmask": "255.255.240.0",
          "gateway": "134.209.112.1",
          "type": "public"
        }
      ]
    },
    "created_at": "2021-02-26T23:32:33Z",
    "tags": [
      "unary-dev-tag-nyc1"
    ],
    "volume_ids": [],
    "vpc_uuid": "66f1711e-d9c7-44b6-a087-1a04252aba4f"
  }
]
```

</details>

Droplet id

```sh
droplet_id='233956260'  # default public IP is 134.209.125.245
```


##### Assign

Assigning a floating IP address to a Droplet by specifying the `droplet_id`.

```sh
doctl compute floating-ip-action assign "${floating_ip}" "${droplet_id}" --output=json

# --format="ID,Status,Type,StartedAt,CompletedAt,ResourceID,ResourceType,Region"

# Status: in-progress, completed, errored
```

<details><summary>Click to expand droplet json info</summary>

```json
[
  {
    "id": 1149583216,
    "status": "in-progress",
    "type": "assign_ip",
    "started_at": "2021-02-26T23:48:47Z",
    "completed_at": null,
    "resource_id": 2813071204,
    "resource_type": "floating_ip",
    "region": {
      "slug": "nyc1",
      "name": "New York 1",
      "sizes": [
        "c-2",
        "c-4",
        "g-2vcpu-8gb",
        "m-2vcpu-16gb",
        "m3-2vcpu-16gb",
        "m6-2vcpu-16gb",
        "gd-2vcpu-8gb",
        "s-8vcpu-16gb",
        "c2-2vcpu-4gb",
        "c2-4vcpu-8gb",
        "c2-8vcpu-16gb",
        "so-2vcpu-16gb",
        "c2-16vcpu-32gb",
        "c2-32vcpu-64gb",
        "so1_5-2vcpu-16gb",
        "so-4vcpu-32gb",
        "so1_5-4vcpu-32gb",
        "so-8vcpu-64gb",
        "so1_5-8vcpu-64gb",
        "s-1vcpu-1gb-intel",
        "s-1vcpu-2gb-intel",
        "s-2vcpu-2gb-intel",
        "s-2vcpu-4gb-intel",
        "s-4vcpu-8gb-intel",
        "s-8vcpu-16gb-intel",
        "s-1vcpu-1gb",
        "s-1vcpu-2gb",
        "s-2vcpu-2gb",
        "s-2vcpu-4gb",
        "s-4vcpu-8gb",
        "so-16vcpu-128gb",
        "so1_5-16vcpu-128gb",
        "so-24vcpu-192gb"
      ],
      "available": true,
      "features": [
        "private_networking",
        "backups",
        "ipv6",
        "metadata",
        "install_agent",
        "storage",
        "image_transfer"
      ]
    },
    "region_slug": "nyc1"
  }
]
```

</details>


<details><summary>Click to expand droplet IP info</summary>

```sh
$ ip addr
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 4a:b4:21:c2:84:a8 brd ff:ff:ff:ff:ff:ff
    inet 134.209.125.245/20 brd 134.209.127.255 scope global eth0
       valid_lft forever preferred_lft forever
    inet 10.10.0.5/16 brd 10.10.255.255 scope global eth0:1
       valid_lft forever preferred_lft forever
    inet6 fe80::48b4:21ff:fec2:84a8/64 scope link
       valid_lft forever preferred_lft forever
3: eth1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 2e:5f:a1:d8:cd:a1 brd ff:ff:ff:ff:ff:ff
    inet 192.168.8.2/24 brd 192.168.8.255 scope global eth1
       valid_lft forever preferred_lft forever
    inet6 fe80::2c5f:a1ff:fed8:cda1/64 scope link
       valid_lft forever preferred_lft forever
```

</details>

Floating IP action id

```sh
floating_ip_action_id='1149583216'
```

##### Get

Retrieving the status of a floating IP action.

```sh

doctl compute floating-ip-action get "${floating_ip}" "${floating_ip_action_id}" --output=json

# --format="ID,Status,Type,StartedAt,CompletedAt,ResourceID,ResourceType,Region"
```

<details><summary>Click to expand droplet json info</summary>

```json
[
  {
    "id": 1149583216,
    "status": "completed",
    "type": "assign_ip",
    "started_at": "2021-02-26T23:48:47Z",
    "completed_at": "2021-02-26T23:48:49Z",
    "resource_id": 2813071204,
    "resource_type": "floating_ip",
    "region": {
      "slug": "nyc1",
      "name": "New York 1",
      "sizes": [
        "c-2",
        "c-4",
        "g-2vcpu-8gb",
        "m-2vcpu-16gb",
        "m3-2vcpu-16gb",
        "m6-2vcpu-16gb",
        "gd-2vcpu-8gb",
        "s-8vcpu-16gb",
        "c2-2vcpu-4gb",
        "c2-4vcpu-8gb",
        "c2-8vcpu-16gb",
        "so-2vcpu-16gb",
        "c2-16vcpu-32gb",
        "c2-32vcpu-64gb",
        "so1_5-2vcpu-16gb",
        "so-4vcpu-32gb",
        "so1_5-4vcpu-32gb",
        "so-8vcpu-64gb",
        "so1_5-8vcpu-64gb",
        "s-1vcpu-1gb-intel",
        "s-1vcpu-2gb-intel",
        "s-2vcpu-2gb-intel",
        "s-2vcpu-4gb-intel",
        "s-4vcpu-8gb-intel",
        "s-8vcpu-16gb-intel",
        "s-1vcpu-1gb",
        "s-1vcpu-2gb",
        "s-2vcpu-2gb",
        "s-2vcpu-4gb",
        "s-4vcpu-8gb",
        "so-16vcpu-128gb",
        "so1_5-16vcpu-128gb",
        "so-24vcpu-192gb"
      ],
      "available": true,
      "features": [
        "private_networking",
        "backups",
        "ipv6",
        "metadata",
        "install_agent",
        "storage",
        "image_transfer"
      ]
    },
    "region_slug": "nyc1"
  }
]
```

</details>

##### Unassign

Unassigning a floating IP address from a Droplet. The floating IP address will be reserved in the region but not assigned to a Droplet.

```sh
doctl compute floating-ip-action unassign "${floating_ip}" --format="ID,Status,Type,Region"

# --format="ID,Status,Type,StartedAt,CompletedAt,ResourceID,ResourceType,Region"

# Status: in-progress, completed, errored
```

### List

Listing all the floating IP addresses on your account.

```sh
doctl compute floating-ip list

doctl compute floating-ip list --region "${region_slug}" # --output=json

# --region string   The region the floating IP address resides in
```

<details><summary>Click to expand json output</summary>

Before assign floating IP


```json
[
  {
    "region": {
      "slug": "nyc1",
      "name": "New York 1",
      "sizes": [
        "c-2",
        "c-4",
        "g-2vcpu-8gb",
        "m-2vcpu-16gb",
        "m3-2vcpu-16gb",
        "m6-2vcpu-16gb",
        "gd-2vcpu-8gb",
        "s-8vcpu-16gb",
        "c2-2vcpu-4gb",
        "c2-4vcpu-8gb",
        "c2-8vcpu-16gb",
        "so-2vcpu-16gb",
        "c2-16vcpu-32gb",
        "c2-32vcpu-64gb",
        "so1_5-2vcpu-16gb",
        "so-4vcpu-32gb",
        "so1_5-4vcpu-32gb",
        "so-8vcpu-64gb",
        "so1_5-8vcpu-64gb",
        "s-1vcpu-1gb-intel",
        "s-1vcpu-2gb-intel",
        "s-2vcpu-2gb-intel",
        "s-2vcpu-4gb-intel",
        "s-4vcpu-8gb-intel",
        "s-8vcpu-16gb-intel",
        "s-1vcpu-1gb",
        "s-1vcpu-2gb",
        "s-2vcpu-2gb",
        "s-2vcpu-4gb",
        "s-4vcpu-8gb",
        "so-16vcpu-128gb",
        "so1_5-16vcpu-128gb",
        "so-24vcpu-192gb",
        "so1_5-24vcpu-192gb",
        "so-32vcpu-256gb"
      ],
      "available": true,
      "features": [
        "private_networking",
        "backups",
        "ipv6",
        "metadata",
        "install_agent",
        "storage",
        "image_transfer"
      ]
    },
    "droplet": null,
    "ip": "167.172.15.100"
  }
]
```

After assign floating IP

```json
[
  {
    "region": {
      "slug": "nyc1",
      "name": "New York 1",
      "sizes": [
        "c-2",
        "c-4",
        "g-2vcpu-8gb",
        "m-2vcpu-16gb",
        "m3-2vcpu-16gb",
        "m6-2vcpu-16gb",
        "gd-2vcpu-8gb",
        "s-8vcpu-16gb",
        "c2-2vcpu-4gb",
        "c2-4vcpu-8gb",
        "c2-8vcpu-16gb",
        "so-2vcpu-16gb",
        "c2-16vcpu-32gb",
        "c2-32vcpu-64gb",
        "so1_5-2vcpu-16gb",
        "so-4vcpu-32gb",
        "so1_5-4vcpu-32gb",
        "so-8vcpu-64gb",
        "so1_5-8vcpu-64gb",
        "s-1vcpu-1gb-intel",
        "s-1vcpu-2gb-intel",
        "s-2vcpu-2gb-intel",
        "s-2vcpu-4gb-intel",
        "s-4vcpu-8gb-intel",
        "s-8vcpu-16gb-intel",
        "s-1vcpu-1gb",
        "s-1vcpu-2gb",
        "s-2vcpu-2gb",
        "s-2vcpu-4gb",
        "s-4vcpu-8gb",
        "so-16vcpu-128gb",
        "so1_5-16vcpu-128gb",
        "so-24vcpu-192gb"
      ],
      "available": true,
      "features": [
        "private_networking",
        "backups",
        "ipv6",
        "metadata",
        "install_agent",
        "storage",
        "image_transfer"
      ]
    },
    "droplet": {
      "id": 233956260,
      "name": "nyc1-debian-01",
      "memory": 2048,
      "vcpus": 2,
      "disk": 60,
      "region": {
        "slug": "nyc1",
        "name": "New York 1",
        "sizes": [
          "c-2",
          "c-4",
          "g-2vcpu-8gb",
          "m-2vcpu-16gb",
          "m3-2vcpu-16gb",
          "m6-2vcpu-16gb",
          "gd-2vcpu-8gb",
          "s-8vcpu-16gb",
          "c2-2vcpu-4gb",
          "c2-4vcpu-8gb",
          "c2-8vcpu-16gb",
          "so-2vcpu-16gb",
          "c2-16vcpu-32gb",
          "c2-32vcpu-64gb",
          "so1_5-2vcpu-16gb",
          "so-4vcpu-32gb",
          "so1_5-4vcpu-32gb",
          "so-8vcpu-64gb",
          "so1_5-8vcpu-64gb",
          "s-1vcpu-1gb-intel",
          "s-1vcpu-2gb-intel",
          "s-2vcpu-2gb-intel",
          "s-2vcpu-4gb-intel",
          "s-4vcpu-8gb-intel",
          "s-8vcpu-16gb-intel",
          "s-1vcpu-1gb",
          "s-1vcpu-2gb",
          "s-2vcpu-2gb",
          "s-2vcpu-4gb",
          "s-4vcpu-8gb",
          "so-16vcpu-128gb",
          "so1_5-16vcpu-128gb",
          "so-24vcpu-192gb"
        ],
        "available": true,
        "features": [
          "private_networking",
          "backups",
          "ipv6",
          "metadata",
          "install_agent",
          "storage",
          "image_transfer"
        ]
      },
      "image": {
        "id": 69440038,
        "name": "10 x64",
        "type": "snapshot",
        "distribution": "Debian",
        "slug": "debian-10-x64",
        "public": true,
        "regions": [
          "nyc1",
          "sfo1",
          "nyc2",
          "ams2",
          "sgp1",
          "lon1",
          "nyc3",
          "ams3",
          "fra1",
          "tor1",
          "sfo2",
          "blr1",
          "sfo3"
        ],
        "min_disk_size": 15,
        "size_gigabytes": 2.15,
        "created_at": "2020-09-02T19:51:15Z",
        "description": "Debian 10 x86 image",
        "status": "available"
      },
      "size": {
        "slug": "s-2vcpu-2gb",
        "memory": 2048,
        "vcpus": 2,
        "disk": 60,
        "price_monthly": 15,
        "price_hourly": 0.02232,
        "regions": [
          "ams2",
          "ams3",
          "blr1",
          "fra1",
          "lon1",
          "nyc1",
          "nyc2",
          "nyc3",
          "sfo1",
          "sfo2",
          "sfo3",
          "sgp1",
          "tor1"
        ],
        "available": true,
        "transfer": 3
      },
      "size_slug": "s-2vcpu-2gb",
      "status": "active",
      "networks": {
        "v4": [
          {
            "ip_address": "134.209.125.245",
            "netmask": "255.255.240.0",
            "gateway": "134.209.112.1",
            "type": "public"
          }
        ]
      },
      "created_at": "2021-02-26T23:32:33Z",
      "tags": [
        "unary-dev-tag-nyc1"
      ],
      "volume_ids": []
    },
    "ip": "167.172.15.100"
  }
]
```

</details>

### Get

Retrieving detailed information about a floating IP address.

```sh
doctl compute floating-ip get "${floating_ip}" # --output=json
```

<details><summary>Click to expand output</summary>

```txt
IP                Region    Droplet ID    Droplet Name
167.172.15.100    nyc1      233956260     nyc1-debian-01
```

</details>

### Delete

Permanently delete a floating IP address (irreversible).

```sh
doctl compute floating-ip delete "${floating_ip}"

# -f, --force   Force floating IP delete
```


## Change Log

* Feb 25, 2021 18:05 Thu ET
  * First draft


[digitalocean]:https://www.digitalocean.com "DititalOcean - The developer cloud"
[doctl]:https://github.com/digitalocean/doctl

<!-- End -->