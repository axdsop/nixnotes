# doctl Initialization


## TOC

1. [Installation](#installation)  
2. [Authentication](#authentication)  
3. [Change Log](#change-log)  


## Installation

Following official documentations

* [How to Install and Configure doctl](https://www.digitalocean.com/docs/apis-clis/doctl/how-to/install/)
* [doctl#installing-doctl](https://github.com/digitalocean/doctl#installing-doctl)

For package manager

```sh
# For Arch Linux
pacman -S --noconfirm doctl
```

Or manually install/update via Shell script [doctl_install.sh](../Scripts/doctl_install.sh)

```sh
# curl -fsL / wget -qO-
curl -fsL https://gitlab.com/axdsop/nixnotes/raw/master/CloudComputing/Providers/DigitalOcean/Scripts/doctl_install.sh | bash -s --
```

Version check

```sh
# doctl version

doctl version 1.55.0-release
Git commit hash: b85cb28
```

## Authentication

Following official documentation

* [doctl auth](https://www.digitalocean.com/docs/apis-clis/doctl/reference/auth/).
* [How to Create a Personal Access Token](https://www.digitalocean.com/docs/apis-clis/api/create-personal-access-token/)


The `doctl auth` commands allow you to authenticate doctl for use with your DigitalOcean account using tokens that you generate in the control panel at <https://cloud.digitalocean.com/account/api/tokens>.

After executing *doctl auth* command, it prompts info

>Please authenticate doctl for use with your DigitalOcean account. You can generate a token in the control panel at https://cloud.digitalocean.com/account/api/tokens


If you just have one DigitalOcean account

```sh
doctl auth init

# config.yaml format
# access-token: <token>
```

If you have multiple DigitalOcean accounts

```sh
doctl auth init --context <name>

# config.yaml format
# auth-contexts:
#   <name>: <token>

# To switch between accounts, use doctl auth switch --context <name>.
doctl auth switch --context <name>
```

You need to add *--context <name>* to switch between accounts. For example, retrieve account details

```sh
# For single account configs
doctl account get

# For multiple account configs
# - method 1 - via context
doctl account get --context <name>
doctl --context <name> account get

# - method 2 - change to default
doctl auth switch --context <name> # Now using context [<name>] by default
doctl account get

# config.yaml format
# context: <name>
```

## Config Path

Default *config.yaml* path

OS|Default Path
---|---
MacOS | ${HOME}/Library/Application Support/doctl/config.yaml
Linux | ${XDG_CONFIG_HOME}/doctl/config.yaml
Windows | %APPDATA%\doctl\config.yaml


## Change Log

* Jan 22, 2021 18:55 Fri ET
  * First draft


<!-- End -->