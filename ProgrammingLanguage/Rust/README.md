# Rust Programming Language Learning Notes

[Rust][rust] is a multi-paradigm system programming language focused on safety, especially safe concurrency. [Rust][rust] is syntactically similar to C++, but is designed to provide better memory safety while maintaining high performance.

This repository records my [Rust][rust] programming language learning process.


## TOC

1. [Notes](#notes)  
1.1 [Preparation](#preparation)  
2. [About Rust Logo](#about-rust-logo)  
3. [Change Log](#change-log)  


## Notes

* [Learning Resources](./Preparation/LearningResources.md)

### Preparation

* [Release Version List](./Preparation/0-ReleaseVersionList.md)
* [Getting Rust](./Preparation/1-GettingRust.md)


## About Rust Logo

![rust logo](https://raw.githubusercontent.com/rust-lang/rust-artwork/master/logo/rust-logo-128x128.png)

[Rust][rust] logo can find from

* [Officially provide the Rust logo #11562](https://github.com/rust-lang/rust/issues/11562)
* [rust-lang/rust-artwork](https://github.com/rust-lang/rust-artwork)

This artwork is distributed under the terms of the [Creative Commons Attribution license (CC-BY)](https://creativecommons.org/licenses/by/4.0/).


## Change Log

* Jul 10, 2019 Wed 10:45 ET
  * First draft
* May 24, 2020 Sun 10:20 ET
  * seperate release into file [Release Version List](./Preparation/0-ReleaseVersionList.md).


[rust]:https://www.rust-lang.org/ "A language empowering everyone
to build reliable and efficient software."
[rustup]:https://github.com/rust-lang/rustup.rs

<!-- End -->