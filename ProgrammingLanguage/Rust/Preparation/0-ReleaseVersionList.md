# Rust Release Version List

[Rust][rust] release version info lists in its GitHub repo [RELEASES.md](https://github.com/rust-lang/rust/blob/master/RELEASES.md).

Official [blog](https://blog.rust-lang.org/) shows release announcing info.

## Extracing Method

Extracting release info via Shell script.

<details><summary>Click to expand script code</summary>

```bash
official_blog_site='https://blog.rust-lang.org'

download_method='wget -qO-'
[[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && download_method='curl -fsL'

release_version_list=$($download_method 'https://raw.githubusercontent.com/rust-lang/rust/master/RELEASES.md' | sed -r -n '/^Version/{s@[\(\)]@@g;s@^Version[[:space:]]*([^[:space:]]+)[[:space:]]*(.*)@\1|\2@g;p}')
# 1.51.0|2021-03-25

release_version_blog_list=$($download_method "${official_blog_site}" | sed -r -n '/Rust 1.1 stable/{s@.*href="([^"]+)".*@\1|1.1.0@g;p}; /Announcing Rust [[:digit:].]+/I{/beta/Id;s@[[:space:]]*[[:punct:]]*and[[:space:]]+[^<]+@@g;s@.*href="([^"]+)"[^[:digit:]]+([^<]+).*$@\1|\2@g;s@[[:space:]]+alpha@.0.alpha@Ig;s@.(alpha)@-\1@g;s@\|[[:digit:]]+.[[:digit:]]+$@&.0@g;p}')
# /2020/05/07/Rust.1.43.1.html|1.43.1

awk -F\| -v blog_site="${official_blog_site}" 'BEGIN{OFS="|"; printf("Release Version|Release Date|Official Blog\n---|---|---\n")} NR==FNR {a[$2]=$1; next}{if (a[$1]) {print $1,$2,blog_site a[$1]}else{print $0}}' <(echo "${release_version_blog_list}") <(echo "${release_version_list}")
# 1.51.0|2021-03-25|https://blog.rust-lang.org/2021/03/25/Rust-1.51.0.html

```

</details>


## Release Version Info

First release version **0.1** (2012-01-20).

Release Version|Release Date|Official Blog
---|---|---
1.51.0|2021-03-25|https://blog.rust-lang.org/2021/03/25/Rust-1.51.0.html
1.50.0|2021-02-11|https://blog.rust-lang.org/2021/02/11/Rust-1.50.0.html
1.49.0|2020-12-31|https://blog.rust-lang.org/2020/12/31/Rust-1.49.0.html
1.48.0|2020-11-19|https://blog.rust-lang.org/2020/11/19/Rust-1.48.html
1.47.0|2020-10-08|https://blog.rust-lang.org/2020/10/08/Rust-1.47.html
1.46.0|2020-08-27|https://blog.rust-lang.org/2020/08/27/Rust-1.46.0.html
1.45.2|2020-08-03|https://blog.rust-lang.org/2020/08/03/Rust-1.45.2.html
1.45.1|2020-07-30|https://blog.rust-lang.org/2020/07/30/Rust-1.45.1.html
1.45.0|2020-07-16|https://blog.rust-lang.org/2020/07/16/Rust-1.45.0.html
1.44.1|2020-06-18|https://blog.rust-lang.org/2020/06/18/Rust.1.44.1.html
1.44.0|2020-06-04|https://blog.rust-lang.org/2020/06/04/Rust-1.44.0.html
1.43.1|2020-05-07|https://blog.rust-lang.org/2020/05/07/Rust.1.43.1.html
1.43.0|2020-04-23|https://blog.rust-lang.org/2020/04/23/Rust-1.43.0.html
1.42.0|2020-03-12|https://blog.rust-lang.org/2020/03/12/Rust-1.42.html
1.41.1|2020-02-27|https://blog.rust-lang.org/2020/02/27/Rust-1.41.1.html
1.41.0|2020-01-30|https://blog.rust-lang.org/2020/01/30/Rust-1.41.0.html
1.40.0|2019-12-19|https://blog.rust-lang.org/2019/12/19/Rust-1.40.0.html
1.39.0|2019-11-07|https://blog.rust-lang.org/2019/11/07/Rust-1.39.0.html
1.38.0|2019-09-26|https://blog.rust-lang.org/2019/09/26/Rust-1.38.0.html
1.37.0|2019-08-15|https://blog.rust-lang.org/2019/08/15/Rust-1.37.0.html
1.36.0|2019-07-04|https://blog.rust-lang.org/2019/07/04/Rust-1.36.0.html
1.35.0|2019-05-23|https://blog.rust-lang.org/2019/05/23/Rust-1.35.0.html
1.34.2|2019-05-14|https://blog.rust-lang.org/2019/05/14/Rust-1.34.2.html
1.34.1|2019-04-25|https://blog.rust-lang.org/2019/04/25/Rust-1.34.1.html
1.34.0|2019-04-11|https://blog.rust-lang.org/2019/04/11/Rust-1.34.0.html
1.33.0|2019-02-28|https://blog.rust-lang.org/2019/02/28/Rust-1.33.0.html
1.32.0|2019-01-17|https://blog.rust-lang.org/2019/01/17/Rust-1.32.0.html
1.31.1|2018-12-20|https://blog.rust-lang.org/2018/12/20/Rust-1.31.1.html
1.31.0|2018-12-06|https://blog.rust-lang.org/2018/12/06/Rust-1.31-and-rust-2018.html
1.30.1|2018-11-08|https://blog.rust-lang.org/2018/11/08/Rust-1.30.1.html
1.30.0|2018-10-25|https://blog.rust-lang.org/2018/10/25/Rust-1.30.0.html
1.29.2|2018-10-11|https://blog.rust-lang.org/2018/10/12/Rust-1.29.2.html
1.29.1|2018-09-25|https://blog.rust-lang.org/2018/09/25/Rust-1.29.1.html
1.29.0|2018-09-13|https://blog.rust-lang.org/2018/09/13/Rust-1.29.html
1.28.0|2018-08-02|https://blog.rust-lang.org/2018/08/02/Rust-1.28.html
1.27.2|2018-07-20|https://blog.rust-lang.org/2018/07/20/Rust-1.27.2.html
1.27.1|2018-07-10|https://blog.rust-lang.org/2018/07/10/Rust-1.27.1.html
1.27.0|2018-06-21|https://blog.rust-lang.org/2018/06/21/Rust-1.27.html
1.26.2|2018-06-05|https://blog.rust-lang.org/2018/06/05/Rust-1.26.2.html
1.26.1|2018-05-29|https://blog.rust-lang.org/2018/05/29/Rust-1.26.1.html
1.26.0|2018-05-10|https://blog.rust-lang.org/2018/05/10/Rust-1.26.html
1.25.0|2018-03-29|https://blog.rust-lang.org/2018/03/29/Rust-1.25.html
1.24.1|2018-03-01|https://blog.rust-lang.org/2018/03/01/Rust-1.24.1.html
1.24.0|2018-02-15|https://blog.rust-lang.org/2018/02/15/Rust-1.24.html
1.23.0|2018-01-04|https://blog.rust-lang.org/2018/01/04/Rust-1.23.html
1.22.1|2017-11-22
1.22.0|2017-11-22|https://blog.rust-lang.org/2017/11/22/Rust-1.22.html
1.21.0|2017-10-12|https://blog.rust-lang.org/2017/10/12/Rust-1.21.html
1.20.0|2017-08-31|https://blog.rust-lang.org/2017/08/31/Rust-1.20.html
1.19.0|2017-07-20|https://blog.rust-lang.org/2017/07/20/Rust-1.19.html
1.18.0|2017-06-08|https://blog.rust-lang.org/2017/06/08/Rust-1.18.html
1.17.0|2017-04-27|https://blog.rust-lang.org/2017/04/27/Rust-1.17.html
1.16.0|2017-03-16|https://blog.rust-lang.org/2017/03/16/Rust-1.16.html
1.15.1|2017-02-09|https://blog.rust-lang.org/2017/02/09/Rust-1.15.1.html
1.15.0|2017-02-02|https://blog.rust-lang.org/2017/02/02/Rust-1.15.html
1.14.0|2016-12-22|https://blog.rust-lang.org/2016/12/22/Rust-1.14.html
1.13.0|2016-11-10|https://blog.rust-lang.org/2016/11/10/Rust-1.13.html
1.12.1|2016-10-20|https://blog.rust-lang.org/2016/10/20/Rust-1.12.1.html
1.12.0|2016-09-29|https://blog.rust-lang.org/2016/09/29/Rust-1.12.html
1.11.0|2016-08-18|https://blog.rust-lang.org/2016/08/18/Rust-1.11.html
1.10.0|2016-07-07|https://blog.rust-lang.org/2016/07/07/Rust-1.10.html
1.9.0|2016-05-26|https://blog.rust-lang.org/2016/05/26/Rust-1.9.html
1.8.0|2016-04-14|https://blog.rust-lang.org/2016/04/14/Rust-1.8.html
1.7.0|2016-03-03|https://blog.rust-lang.org/2016/03/02/Rust-1.7.html
1.6.0|2016-01-21|https://blog.rust-lang.org/2016/01/21/Rust-1.6.html
1.5.0|2015-12-10|https://blog.rust-lang.org/2015/12/10/Rust-1.5.html
1.4.0|2015-10-29|https://blog.rust-lang.org/2015/10/29/Rust-1.4.html
1.3.0|2015-09-17|https://blog.rust-lang.org/2015/09/17/Rust-1.3.html
1.2.0|2015-08-07|https://blog.rust-lang.org/2015/08/06/Rust-1.2.html
1.1.0|2015-06-25|https://blog.rust-lang.org/2015/06/25/Rust-1.1.html
1.0.0|2015-05-15|https://blog.rust-lang.org/2015/05/15/Rust-1.0.html
1.0.0-alpha.2|2015-02-20|https://blog.rust-lang.org/2015/02/20/Rust-1.0-alpha2.html
1.0.0-alpha|2015-01-09|https://blog.rust-lang.org/2015/01/09/Rust-1.0-alpha.html
0.12.0|2014-10-09
0.11.0|2014-07-02
0.10|2014-04-03
0.9|2014-01-09
0.8|2013-09-26
0.7|2013-07-03
0.6|2013-04-03
0.5|2012-12-21
0.4|2012-10-15
0.3|2012-07-12
0.2|2012-03-29
0.1|2012-01-20


## Change Log

* May 24, 2020 Sun 09:56 ET
  * First draft
* Apr 14, 2021 Wed 08:43 ET
  * Update version info


[rust]:https://www.rust-lang.org/ "A language empowering everyone"

<!-- End -->