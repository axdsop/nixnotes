# Getting Rust (Install/Update/Uninstall)

[Rust][rust]'s official document [Install Rust](https://www.rust-lang.org/tools/install) recommands to use [rustup.rs][rustup]. It is a tool that manages multiple Rust toolchains in a consistent way across all platforms Rust supports.

[Rust][rust] also provides [Other Rust Installation Methods](https://forge.rust-lang.org/other-installation-methods.html) such as [standalone installers](https://forge.rust-lang.org/other-installation-methods.html#standalone), [source code](https://forge.rust-lang.org/other-installation-methods.html#source) and so on.


## TOC

1. [Release Info](#release-info)  
2. [Introduction](#introduction)  
2.1 [rustup](#rustup)  
2.2 [Components](#components)  
2.3 [Cargo](#cargo)  
3. [Install](#install)  
3.1 [PATH environment variable](#path-environment-variable)  
3.2 [Tab Completion](#tab-completion)  
4. [Update](#update)  
5. [Uninstall](#uninstall)  
6. [Configuration](#configuration)  
6.1 [Network Proxies](#network-proxies)  
7. [Change Log](#change-log)  


## Release Info

Note [Release Version List](./0-ReleaseVersionList.md) lists release version lists and extraction method.

Latest release version info (2021-03-25)

Release Version|Release Date|Official Blog
---|---|---
1.51.0|2021-03-25|https://blog.rust-lang.org/2021/03/25/Rust-1.51.0.html


## Introduction

### rustup

>*rustup* installs `rustc`, `cargo`, `rustup` and other standard tools to Cargo's bin directory. On Unix it is located at *$HOME/.cargo/bin*. This is the same directory that *cargo install* will install Rust programs and Cargo plugins. -- [rustup.rs#installation](https://github.com/rust-lang/rustup.rs#installation)

How rustup works

>`rustup` is a *toolchain multiplexer*. It installs and manages many Rust toolchains and presents them all through a single set of tools installed to `~/.cargo/bin`. The `rustc` and `cargo` installed to `~/.cargo/bin` are *proxies* that delegate to the real toolchain. `rustup` then provides mechanisms to easily change the active toolchain by reconfiguring the behavior of the proxies.
>
>So when `rustup` is first installed running `rustc` will run the proxy in `$HOME/.cargo/bin/rustc`, which in turn will run the stable compiler. If you later *change the default toolchain* to nightly with `rustup default nightly`, then that same proxy will run the `nightly` compiler instead. -- [rustup.rs#how-rustup-works](https://github.com/rust-lang/rustup.rs#how-rustup-works)


[Environment variables](https://github.com/rust-lang/rustup.rs#environment-variables) list rustup environment variables, such as *RUSTUP_HOME*,*RUSTUP_TOOLCHAIN*.

[Rust][rust] support many editors which are listed in [First-class editor support](https://www.rust-lang.org/tools#tools-write-ide-prose).


### Components

```sh
# List available components
rustup component list

# List only installed components
rustup component list --installed
# rustup component list | grep '('
```

[Installing Rust](https://www.rust-lang.org/learn/get-started#installing-rust) says

>You can install a code formatting tool (Rustfmt) with `rustup component add rustfmt`, and a linting tool (Clippy) with `rustup component add clippy`.


[Velocity through automation](https://www.rust-lang.org/tools#tools-debug) says
>Rust’s industry-grade tools make collaboration fearless, allowing teams to focus on the tasks that matter.

```sh
# code formatting tool
# https://github.com/rust-lang/rustfmt
rustup component add rustfmt

# linting tool (Clippy)
# https://github.com/rust-lang/rust-clippy
rustup component add clippy
```


### Cargo

Cargo: the Rust build tool and package manager

When you install Rustup you�ll also get the latest stable version of the Rust build tool and package manager, also known as Cargo. Cargo does lots of things:

Command|Comment
---|---
`cargo --version`|test if Rust and Cargo installed
`cargo build`|build your project
`cargo run`|run your project
`cargo test`|test your project
`cargo doc`|build documentation for your project
`cargo publish`|publish a library to crates.io


## Install

Official installing method

```sh
# - for *nix
curl --proto '=https' --tlsv1.2 -sSf | sh
# curl https://sh.rustup.rs -sSf | sh -s --

# - list help info
curl https://sh.rustup.rs -sSf | sh -s -- --help
```

<details><summary>Click to expand help info</summary>

```txt
rustup-init 1.23.1 (fb4d10153 2020-12-01)
The installer for rustup

USAGE:
    rustup-init [FLAGS] [OPTIONS]

FLAGS:
    -v, --verbose           Enable verbose output
    -q, --quiet             Disable progress output
    -y                      Disable confirmation prompt.
        --no-modify-path    Don't configure the PATH environment variable
    -h, --help              Prints help information
    -V, --version           Prints version information

OPTIONS:
        --default-host <default-host>              Choose a default host triple
        --default-toolchain <default-toolchain>    Choose a default toolchain to install
        --default-toolchain none                   Do not install any toolchains
        --profile [minimal|default|complete]       Choose a profile
    -c, --component <components>...                Component name to also install
    -t, --target <targets>...                      Target name to also install
```

</details>


<details><summary>Click to expand installation procedure</summary>

```txt
info: downloading installer
Warning: Not enforcing strong cipher suites for TLS, this is potentially less secure
Warning: Not enforcing TLS v1.2, this is potentially less secure

Welcome to Rust!

This will download and install the official compiler for the Rust
programming language, and its package manager, Cargo.

Rustup metadata and toolchains will be installed into the Rustup
home directory, located at:

  /home/maxdsre/.rustup

This can be modified with the RUSTUP_HOME environment variable.

The Cargo home directory located at:

  /home/maxdsre/.cargo

This can be modified with the CARGO_HOME environment variable.

The cargo, rustc, rustup and other commands will be added to
Cargo's bin directory, located at:

  /home/maxdsre/.cargo/bin

This path will then be added to your PATH environment variable by
modifying the profile files located at:

  /home/maxdsre/.profile
  /home/maxdsre/.bash_profile
  /home/maxdsre/.bashrc

You can uninstall at any time with rustup self uninstall and
these changes will be reverted.

Current installation options:


   default host triple: x86_64-unknown-linux-gnu
     default toolchain: stable (default)
               profile: default
  modify PATH variable: yes

1) Proceed with installation (default)
2) Customize installation
3) Cancel installation
>1

info: profile set to 'default'
info: default host triple is x86_64-unknown-linux-gnu
info: syncing channel updates for 'stable-x86_64-unknown-linux-gnu'
info: latest update on 2021-03-25, rust version 1.51.0 (2fd73fabe 2021-03-23)
info: downloading component 'cargo'
  6.0 MiB /   6.0 MiB (100 %)   2.2 MiB/s in  2s ETA:  0s
info: downloading component 'clippy'
  2.4 MiB /   2.4 MiB (100 %)   2.2 MiB/s in  1s ETA:  0s
info: downloading component 'rust-docs'
 14.9 MiB /  14.9 MiB (100 %)   1.9 MiB/s in  8s ETA:  0s
info: downloading component 'rust-std'
 24.9 MiB /  24.9 MiB (100 %)   2.2 MiB/s in 11s ETA:  0s
info: downloading component 'rustc'
 50.4 MiB /  50.4 MiB (100 %)   2.1 MiB/s in 24s ETA:  0s
info: downloading component 'rustfmt'
  3.6 MiB /   3.6 MiB (100 %)   2.3 MiB/s in  1s ETA:  0s
info: installing component 'cargo'
info: using up to 500.0 MiB of RAM to unpack components
info: installing component 'clippy'
info: installing component 'rust-docs'
 14.9 MiB /  14.9 MiB (100 %)   6.2 MiB/s in  2s ETA:  0s
info: installing component 'rust-std'
 24.9 MiB /  24.9 MiB (100 %)   9.3 MiB/s in  3s ETA:  0s
info: installing component 'rustc'
 50.4 MiB /  50.4 MiB (100 %)  11.0 MiB/s in  4s ETA:  0s
info: installing component 'rustfmt'
info: default toolchain set to 'stable-x86_64-unknown-linux-gnu'

  stable-x86_64-unknown-linux-gnu installed - rustc 1.51.0 (2fd73fabe 2021-03-23)


Rust is installed now. Great!

To get started you need Cargo's bin directory ($HOME/.cargo/bin) in your PATH
environment variable. Next time you log in this will be done
automatically.

To configure your current shell, run:
source $HOME/.cargo/env
```

</details>


### PATH environment variable

In the [Rust][rust] development environment, all tools are installed to the *~/.cargo/bin* directory, and this is where you will find the Rust toolchain, including `rustc`, `cargo`, and `rustup`.

Make environment parameter `$PATH` effect in current shell

```sh
# $HOME/.profile, $HOME/.bash_profile,$HOME/.bashrc

tail -n 1 $HOME/.profile
# source "$HOME/.cargo/env" ==> export PATH="$HOME/.cargo/bin:$PATH"

source $HOME/.profile
# source $HOME/.cargo/env
```

Version check

```sh
rustc --version
# rustc 1.51.0 (2fd73fabe 2021-03-23)
```

Show the active and installed toolchains

```sh
rustup show
```

<details><summary>Click to expand output</summary>

```txt
Default host: x86_64-unknown-linux-gnu
rustup home:  /home/maxdsre/.rustup

stable-x86_64-unknown-linux-gnu (default)
rustc 1.51.0 (2fd73fabe 2021-03-23)
```

</details>

### Tab Completion

[rustup][rustup] now supports generating completion scripts for Bash, Fish, Zsh, and PowerShell.

[rustup][rustup] can also generate a completion script for `cargo`. The script output by `rustup` will source the completion script distributed with your default toolchain.

More details see command `rustup help completions`.

<details><summary>Click to expand Shell script code</summary>

```sh
# $SHELL /etc/shells

# - For bash
if [[ -n $(which brew 2> /dev/null || command -v brew 2> /dev/null) ]]; then
    # Bash (macOS/Homebrew)
    mkdir -p $(brew --prefix)/etc/bash_completion.d
    rustup completions bash > $(brew --prefix)/etc/bash_completion.d/rustup.bash-completion
else
    # https://github.com/scop/bash-completion/blob/master/README.md#faq
    # Bash
    [[ -d ~/.local/share/bash-completion/completions ]] || mkdir -p ~/.local/share/bash-completion/completions
    rustup completions bash > ~/.local/share/bash-completion/completions/rustup
    # cargo support
    rustup completions bash cargo >> ~/.local/share/bash-completion/completions/cargo
fi

# - For zsh
[[ -d ~/.zfunc ]] || mkdir -p ~/.zfunc
# Then add the line to your `.zshrc` just before `compinit`
[[ -f ~/.zshrc ]] && sed -r -i 'fpath.*zfunc/d; /compinit/i fpath+=~/.zfunc' ~/.zshrc
rustup completions zsh > ~/.zfunc/_rustup
# cargo support
rustup completions zsh cargo > ~/.zfunc/_cargo

# - For fish
[[ -d ~/.config/fish/completions ]] || mkdir -p ~/.config/fish/completions
rustup completions fish > ~/.config/fish/completions/rustup.fish
```

</details>

You may need to restart your shell in order for the changes to take effect.


## Update

>rustup will automatically update itself at the end of any toolchain installation as well. You can prevent this automatic behaviour by passing the *--no-self-update* argument when running `rustup update` or `rustup toolchain install`. -- [rustup.rs#keeping-rustup-up-to-date](https://github.com/rust-lang/rustup.rs#keeping-rustup-up-to-date)


Toolchain name has *stable*, *beta*, and *nightly*, default is *stable*

```sh
# Set the default toolchain to 'nightly'
rustup default nightly
```

Update Rust toolchains and rustup

```sh
rustup update

# rustup update stable  # update Rust toolchains 'stable' and rustup
# rustup self update  #just update rustup itself
```

<details><summary>Click to expand output</summary>

```txt
info: syncing channel updates for 'stable-x86_64-unknown-linux-gnu'
info: checking for self-updates

  stable-x86_64-unknown-linux-gnu unchanged - rustc 1.51.0 (2fd73fabe 2021-03-23)

info: cleaning up downloads & tmp directories
```

</details>


## Uninstall

[rustup.rs#installation](https://github.com/rust-lang/rustup.rs#installation) says

>If you decide Rust isn't your thing, you can completely remove it from your system by running `rustup self uninstall`.

```sh
# uninstall rustup
rustup self uninstall
```

<details><summary>Click to expand output</summary>

```txt
Thanks for hacking in Rust!

This will uninstall all Rust toolchains and data, and remove
$HOME/.cargo/bin from your PATH environment variable.

Continue? (y/N) y

info: removing rustup home
info: removing cargo home
info: removing rustup binaries
info: rustup is uninstalled
```

</details>


## Configuration

### Network Proxies

[Working with network proxies](https://github.com/rust-lang/rustup.rs#working-with-network-proxies) says
>Enterprise networks often don't have direct outside HTTP access, but enforce the use of proxies. If you're on such a network, you can request that rustup uses a proxy by setting its URL in the environment. In most cases, setting *https_proxy* should be sufficient.

On a Unix-like system with a shell like bash or zsh, you could use:

```sh
export https_proxy=socks5://proxy.example.com:1080 # or http://proxy.example.com:8080
```


## Change Log

* Jul 10, 2019 Wed 10:05 ET
  * First draft
* Oct 02, 2019 Wed 10:37 ET
  * Upgrade version to `1.38.0`
* Apr 08, 2020 Wed 08:53 ET
  * Upgrade version to `1.42.0`
* May 24, 2020 Sun 10:05 ET
  * Upgrade version to `1.43.1`, seperate release into file [Release Version List](./0-ReleaseVersionList.md).
* Apr 14, 2021 Wed 09:33 ET
  * Add output info toggle via `<details></details>`


[rust]:https://www.rust-lang.org/ "A language empowering everyone
to build reliable and efficient software."
[rustup]:https://github.com/rust-lang/rustup.rs

<!-- End -->