# Rust Learning Resources

[Rust][rust] has many social accouts, such as [GitHub](https://github.com/rust-lang), [Twitter](https://twitter.com/rustlang), [Discord](https://discordapp.com/invite/rust-lang), [Youtube](https://www.youtube.com/channel/UCaYhcUwRBNscFNUKTjgPFiA).

[Rust][rust] provides a web interactive editor [Rust Playground](https://play.rust-lang.org).

Page [Learn Rust](https://www.rust-lang.org/learn) lists many learn resources.

## TOC

1. [Browser Reading](#browser-reading)  
2. [Learn Rust](#learn-rust)  
2.1 [Get started with Rust](#get-started-with-rust)  
2.2 [Grow with Rust](#grow-with-rust)  
2.3 [Master Rust](#master-rust)  
3. [Bibliography](#bibliography)  
4. [Change Log](#change-log)  


## Browser Reading

>All of this documentation is also available locrustup docally using the `rustup doc` command, which will open up these resources for you in your browser without requiring a network connection!


```bash
#The Rust core allocation and collections library
rustup doc --alloc

#The Rust Programming Language book
rustup doc --book

#The Cargo Book
rustup doc --cargo

#The Rust Core Library
rustup doc --core

#The Rust Edition Guide
rustup doc --edition-guide

#The Embedded Rust Book
rustup doc --embedded-book

#The Dark Arts of Advanced and Unsafe Rust Programming
rustup doc --nomicon

#Only print the path to the documentation
rustup doc --path

#A support library for macro authors when defining new macros
rustup doc --proc_macro

#The Rust Reference
rustup doc --reference

#A collection of runnable examples that illustrate various Rust concepts and standard libraries
rustup doc --rust-by-example

#The compiler for the Rust programming language
rustup doc --rustc

#Generate documentation for Rust projects
rustup doc --rustdoc

#Standard library API documentation
rustup doc --std

#Support code for rustc's built in unit-test and micro-benchmarking framework
rustup doc --test

#The Unstable Book
rustup doc --unstable-book
```

## Learn Rust

### Get started with Rust

* [The Rust Programming Language](https://doc.rust-lang.org/book/)
* [rustlings](https://github.com/rust-lang/rustlings/ "Small exercises to get you used to reading and writing Rust code!")
* [Rust By Example](https://doc.rust-lang.org/stable/rust-by-example/)

### Grow with Rust

Read the core documentation

* [The Standard Libary](https://doc.rust-lang.org/std/index.html "Comprehensive guide to the Rust standard library APIs.")
* [Edition Guide](https://doc.rust-lang.org/edition-guide/index.html "Guide to the Rust editions.")
* [Cargo Book](https://doc.rust-lang.org/stable/cargo/ "A book on Rust’s package manager and build system.")
* [Rustdoc Book](https://doc.rust-lang.org/rustdoc/index.html "Learn how to make awesome documentation for your crate.")
* [Rustc Book](https://doc.rust-lang.org/rustc/index.html "Familiarize yourself with the knobs available in the Rust compiler.")
* [Complete Error Index](https://doc.rust-lang.org/error-index.html "In-depth explanations of the errors you may see from the Rust compiler.")

Build your skills in an application domain

* [Command Line Book](https://rust-lang-nursery.github.io/cli-wg/ "Learn how to build effective command line applications in Rust.")
* [WebAssembly Book](https://rustwasm.github.io/docs/book/ "Use Rust to build browser-native libraries through WebAssembly.")
* [Embedded Book](https://rust-embedded.github.io/book/ "Become proficient with Rust for Microcontrollers and other embedded systems.")

### Master Rust

* [The Rust Reference](https://doc.rust-lang.org/reference/index.html)
* [The Rustonomicon](https://doc.rust-lang.org/nomicon/index.html "The Dark Arts of Advanced and Unsafe Rust Programming")
* [The Rust Unstable Book](https://doc.rust-lang.org/unstable-book/index.html)


## Bibliography

* [This Rust Cookbook](https://rust-lang-nursery.github.io/rust-cookbook/)


## Change Log

* Jul 10, 2019 Wed 10:40 ET
  * First draft
* Oct 17, 2019 Thu 15:59 ET
  * Add bibliography


[rust]:https://www.rust-lang.org/ "A language empowering everyone
to build reliable and efficient software."
[rustup]:https://github.com/rust-lang/rustup.rs


<!-- End -->