# Software Release Version Detection

Extracting software release version info from its official site via shell script on GNU/Linux.

## TOC

1. [Python](#python)  
1.1 [Command](#command)  
1.1.1 [Shell Bash](#shell-bash)  
1.1.2 [bashrc](#bashrc)  
1.2 [Output](#output)  
2. [Linux Kernel](#linux-kernel)  
2.1 [Command](#command-1)  
2.1.1 [Shell Bash](#shell-bash-1)  
2.1.2 [bashrc](#bashrc-1)  
2.2 [Output](#output-1)  

## Python

### Command

#### Shell Bash

```bash
download_tool='wget -qO-'
[[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && download_tool='curl -fsSL'

${download_tool} https://www.python.org/downloads/ | sed -r -n '/list-row-container/,/<\/ol>/{s@^[[:space:]]*@@g;s@</?(span|ol)[^>]*>@@g;/^$/d;p}' | sed ':a;N;$!ba;s@\n@@g;s@<\/li>@&\n@g;' | sed -r -n '/^$/d;s@^.*?Python[[:space:]]*@@g;s@>[[:space:]]*(Download|Release)[^<]*@>@g;s@<a[[:space:]]*href="([^>]*)"[^>]*>@|\1|@g;s@\|?<\/a>\|?@|@g;s@<[^>]*>@@g;s@\|$@@g;p' | sed '/\/release\//!d' | awk -F\| '{ver=gensub(/[^[:digit:]]*([[:digit:]]{1}).*/,"\\1","g",$1); if (arr[ver]==""){arr[ver]=$0}}END{for(i in arr) print arr[i]}' | sort -r | while IFS="|" read -r p_ver p_date p_release_note p_change_log; do
    p_release_note="https://www.python.org/${p_release_note#/}"
    printf "\n\033[31;1m%s\033[0m\n---------\n\033[33m%14s\033[0m %s\n\033[33m%14s\033[0m %s\n\033[33m%14s\033[0m %s\n" "Python ${p_ver}" 'Release date:' "${p_date}" 'Release note:' "${p_release_note}" 'Change log:' "${p_change_log}"
    ${download_tool} "${p_release_note}" | sed -r -n '/>(XZ|Gzipped)/,/<\/tr>/{s@^[[:space:]]*@@g;s@<\/tr>@---@g;/<td>/{/[[:digit:]]+/!d};s@.*href="([^"]*)".*@\1@g;s@<[^>]*>@@g;p}' | sed ':a;N;$!ba;s@\n@|@g;s@|*---|*@\n@g' | sed '/^$/d' | awk -F\| '{printf("\n\033[33m%14s\033[0m %s\n\033[33m%14s\033[0m %s\n\033[33m%14s\033[0m %s\n\033[33m%14s\033[0m %s\n","Download link:",$1,"MD5 Checksum:",$2,"Size:",$3,"GPG:",$4)}'
    echo ''
done
```

#### bashrc

```bash
download_tool='wget -qO-'
# download_tool='curl -fsL'

alias axdsop_release_python="${download_tool} https://www.python.org/downloads/ | sed -r -n '/list-row-container/,/<\/ol>/{s@^[[:space:]]*@@g;s@</?(span|ol)[^>]*>@@g;/^\$/d;p}' | sed ':a;N;\$!ba;s@\n@@g;s@<\/li>@&\n@g;' | sed -r -n '/^\$/d;s@^.*?Python[[:space:]]*@@g;s@>[[:space:]]*(Download|Release)[^<]*@>@g;s@<a[[:space:]]*href=\"([^>]*)\"[^>]*>@|\1|@g;s@\|?<\/a>\|?@|@g;s@<[^>]*>@@g;s@\|\$@@g;p' | sed '/\/release\//!d' | awk -F\| '{ver=gensub(/[^[:digit:]]*([[:digit:]]{1}).*/,\"\\\\1\",\"g\",\$1); if (arr[ver]==\"\"){arr[ver]=\$0}}END{for(i in arr) print arr[i]}' | sort -r | while IFS=\"|\" read -r p_ver p_date p_release_note p_change_log; do p_release_note=\"https://www.python.org/\${p_release_note#/}\"; printf \"\n\033[31;1m%s\033[0m\n---------\n\033[33m%14s\033[0m %s\n\033[33m%14s\033[0m %s\n\033[33m%14s\033[0m %s\n\" \"Python \${p_ver}\" 'Release date:' \"\${p_date}\" 'Release note:' \"\${p_release_note}\" 'Change log:' \"\${p_change_log}\"; ${download_tool} \"\${p_release_note}\" | sed -r -n '/>(XZ|Gzipped)/,/<\/tr>/{s@^[[:space:]]*@@g;s@<\/tr>@---@g;/<td>/{/[[:digit:]]+/!d};s@.*href=\"([^\"]*)\".*@\1@g;s@<[^>]*>@@g;p}' | sed ':a;N;\$!ba;s@\n@|@g;s@|*---|*@\n@g' | sed '/^\$/d' | awk -F\| '{printf(\"\n\033[33m%14s\033[0m %s\n\033[33m%14s\033[0m %s\n\033[33m%14s\033[0m %s\n\033[33m%14s\0Python 3.10.9 Dec. 6, 2022  Download Release Notes
33[0m %s\n\",\"Download link:\",\$1,\"MD5 Checksum:\",\$2,\"Size:\",\$3,\"GPG:\",\$4)}'; echo ''; done"
```

### Output

Date: 2023/01

```txt
Python 3.11.1
---------
 Release date: Dec. 6, 2022
 Release note: https://www.python.org/downloads/release/python-3111/
   Change log: https://docs.python.org/release/3.11.1/whatsnew/changelog.html#python-3-11-1

Download link: https://www.python.org/ftp/python/3.11.1/Python-3.11.1.tgz
 MD5 Checksum: 5c986b2865979b393aa50a31c65b64e8
         Size: 26394378
          GPG: https://www.python.org/ftp/python/3.11.1/Python-3.11.1.tgz.asc

Download link: https://www.python.org/ftp/python/3.11.1/Python-3.11.1.tar.xz
 MD5 Checksum: 4efe92adf28875c77d3b9b2e8d3bc44a
         Size: 19856648
          GPG: https://www.python.org/ftp/python/3.11.1/Python-3.11.1.tar.xz.asc


Python 2.7.18
---------
 Release date: April 20, 2020
 Release note: https://www.python.org/downloads/release/python-2718/
   Change log:

Download link: https://www.python.org/ftp/python/2.7.18/Python-2.7.18.tgz
 MD5 Checksum: 38c84292658ed4456157195f1c9bcbe1
         Size: 17539408
          GPG: https://www.python.org/ftp/python/2.7.18/Python-2.7.18.tgz.asc

Download link: https://www.python.org/ftp/python/2.7.18/Python-2.7.18.tar.xz
 MD5 Checksum: fd6cc8ec0a78c44036f825e739f36e5a
         Size: 12854736
          GPG: https://www.python.org/ftp/python/2.7.18/Python-2.7.18.tar.xz.asc

```

Date: 2019/10

```txt
Python 3.8.0
---------
 Release date: Oct. 14, 2019
 Release note: https://www.python.org/downloads/release/python-380/
   Change log: https://docs.python.org/3.8/whatsnew/changelog.html#python-3-8-0-final


Download link: https://www.python.org/ftp/python/3.8.0/Python-3.8.0.tgz
 MD5 Checksum: e18a9d1a0a6d858b9787e03fc6fdaa20
         Size: 23949883
          GPG: https://www.python.org/ftp/python/3.8.0/Python-3.8.0.tgz.asc

Download link: https://www.python.org/ftp/python/3.8.0/Python-3.8.0.tar.xz
 MD5 Checksum: dbac8df9d8b9edc678d0f4cacdb7dbb0
         Size: 17829824
          GPG: https://www.python.org/ftp/python/3.8.0/Python-3.8.0.tar.xz.asc


Python 2.7.16
---------
 Release date: March 4, 2019
 Release note: https://www.python.org/downloads/release/python-2716/
   Change log: https://raw.githubusercontent.com/python/cpython/v2.7.16/Misc/NEWS.d/2.7.16.rst

Download link: https://www.python.org/ftp/python/2.7.16/Python-2.7.16.tgz
 MD5 Checksum: f1a2ace631068444831d01485466ece0
         Size: 17431748
          GPG: https://www.python.org/ftp/python/2.7.16/Python-2.7.16.tgz.asc

Download link: https://www.python.org/ftp/python/2.7.16/Python-2.7.16.tar.xz
 MD5 Checksum: 30157d85a2c0479c09ea2cbe61f2aaf5
         Size: 12752104
          GPG: https://www.python.org/ftp/python/2.7.16/Python-2.7.16.tar.xz.asc

```

## Linux Kernel

### Command

#### Shell Bash

```bash
download_tool='wget -qO-'
[[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && download_tool='curl -fsSL'

${download_tool} https://www.kernel.org | sed -r -n '/id="releases"/,/<\/table>/{s@^[[:space:]]*@@g;/(^$|<\/?table[^>]*>)/d;s@<\/tr>@---@g;/<tr[^>]*>/d;s@.*href="([^"]*)".*@\1@g;s@:<@<@g;p}' | sed ':a;N;$!ba;s@\n@|@g;s@|*---|*@\n@g' | sed -r -n '/^$/d;s@[[:space:]]*<[^>]*>[[:space:]]*@@g;p' | awk -F\| '{if(arr[$1]==""){arr[$1]=$0}}END{for(i in arr) print arr[i]}' | awk -F\| '{printf("\033[31;1m%s\033[0m\n---------\n\033[33m%16s\033[0m %s\n\033[33m%16s\033[0m %s\n\033[33m%16s\033[0m %s\n\033[33m%16s\033[0m %s\n\033[33m%16s\033[0m %s\n\n",$1,"Release Version:",$2,"Release Date:",$3,"Download link:",$4,"GPG:",$5,"Change Log:",$10);}'
```

#### bashrc

```bash
alias axdsop_release_kernel="${download_tool} https://www.kernel.org | sed -r -n '/id=\"releases\"/,/<\/table>/{s@^[[:space:]]*@@g;/(^\$|<\/?table[^>]*>)/d;s@<\/tr>@---@g;/<tr[^>]*>/d;s@.*href=\"([^\"]*)\".*@\1@g;s@:<@<@g;p}' | sed ':a;N;\$!ba;s@\n@|@g;s@|*---|*@\n@g' | sed -r -n '/^\$/d;s@[[:space:]]*<[^>]*>[[:space:]]*@@g;p' | awk -F\| '{if(arr[\$1]==\"\"){arr[\$1]=\$0}}END{for(i in arr) print arr[i]}' | awk -F\| '{printf(\"\033[31;1m%s\033[0m\n---------\n\033[33m%16s\033[0m %s\n\033[33m%16s\033[0m %s\n\033[33m%16s\033[0m %s\n\033[33m%16s\033[0m %s\n\033[33m%16s\033[0m %s\n\n\",\$1,\"Release Version:\",\$2,\"Release Date:\",\$3,\"Download link:\",\$4,\"GPG:\",\$5,\"Change Log:\",\$10);}'"
```

### Output

Date: 2023/01

```txt
stable
---------
Release Version: 6.1.4
   Release Date: 2023-01-07
  Download link: https://cdn.kernel.org/pub/linux/kernel/v6.x/linux-6.1.4.tar.xz
            GPG: https://cdn.kernel.org/pub/linux/kernel/v6.x/linux-6.1.4.tar.sign
     Change Log: https://cdn.kernel.org/pub/linux/kernel/v6.x/ChangeLog-6.1.4

mainline
---------
Release Version: 6.2-rc3
   Release Date: 2023-01-08
  Download link: https://git.kernel.org/torvalds/t/linux-6.2-rc3.tar.gz
            GPG:
     Change Log:

longterm
---------
Release Version: 5.15.86
   Release Date: 2022-12-31
  Download link: https://cdn.kernel.org/pub/linux/kernel/v5.x/linux-5.15.86.tar.xz
            GPG: https://cdn.kernel.org/pub/linux/kernel/v5.x/linux-5.15.86.tar.sign
     Change Log: https://cdn.kernel.org/pub/linux/kernel/v5.x/ChangeLog-5.15.86

linux-next
---------
Release Version: next-20230110
   Release Date: 2023-01-10
  Download link:
            GPG:
     Change Log:

```

Date: 2019/10

```txt
stable
---------
Release Version: 5.3.6
   Release Date: 2019-10-11
  Download link: https://cdn.kernel.org/pub/linux/kernel/v5.x/linux-5.3.6.tar.xz
            GPG: https://cdn.kernel.org/pub/linux/kernel/v5.x/linux-5.3.6.tar.sign
     Change Log: https://cdn.kernel.org/pub/linux/kernel/v5.x/ChangeLog-5.3.6

mainline
---------
Release Version: 5.4-rc3
   Release Date: 2019-10-13
  Download link: https://git.kernel.org/torvalds/t/linux-5.4-rc3.tar.gz
            GPG:
     Change Log:

longterm
---------
Release Version: 4.19.79
   Release Date: 2019-10-11
  Download link: https://cdn.kernel.org/pub/linux/kernel/v4.x/linux-4.19.79.tar.xz
            GPG: https://cdn.kernel.org/pub/linux/kernel/v4.x/linux-4.19.79.tar.sign
     Change Log: https://cdn.kernel.org/pub/linux/kernel/v4.x/ChangeLog-4.19.79

linux-next
---------
Release Version: next-20191015
   Release Date: 2019-10-15
  Download link:
            GPG:
     Change Log:

```

<!-- End -->
