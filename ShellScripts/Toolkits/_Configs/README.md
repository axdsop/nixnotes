# Toolkits - Configs


## Application

Application Script|Config
---|---
auditd|[config](./Application/auditd/)
[Elastic](ShellScripts/Toolkits/Application/Monitor/ElasticStack.sh)|[config](./Application/elastic/)
[Grafana](ShellScripts/Toolkits/Application/Monitor/Grafana.sh)|[config](./Application/grafana/)
[MongoDB](ShellScripts/Toolkits/Application/Database/MongoDB.sh)|[config](./Application/mongodb/)
[MySQL](ShellScripts/Toolkits/Application/Database/MySQLVariants.sh)|[config](./Application/mongodb/)
[Nginx](ShellScripts/Toolkits/Application/WebServer/NginxWebServer.sh)|[config](./Application/mongodb/)
[Prometheus](ShellScripts/Toolkits/Application/Monitor/Prometheus.sh)|[config](./Application/mongodb/)
[Redis](ShellScripts/Toolkits/Application/Database/Redis.sh)|[config](./Application/mongodb/)
[Visual Studio Code](ShellScripts/Toolkits/Application/TextEditor/VisualStudioCode.sh)|[config](./Application/mongodb/)


## Source

* [GNU/Linux Distros Release List](./Source/gnulinuxDistroReleaseDateList.txt)
* [MySQL/Percona/MariaDB Version & Linux Distros Relation Table](./Source/mysqlVariantsVersionAndLinuxDistroRelationTable.txt)
* [PostgreSQL Version & Linux Distros Relation Table](./Source/PostgreSQLVersionAndLinuxDistroRelationTable.txt)


<!-- End -->
