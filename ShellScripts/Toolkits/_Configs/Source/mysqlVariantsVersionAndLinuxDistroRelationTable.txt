MySQL|bionic|5.7 8.0
MySQL|buster|5.7 8.0
MySQL|el6|8.0 5.7 5.6 5.5|rhel centos
MySQL|el7|8.0 5.7 5.6 5.5|rhel centos
MySQL|el8|8.0|rhel centos
MySQL|fc31|8.0 5.7|fedora
MySQL|fc32|8.0|fedora
MySQL|fc33|8.0|fedora
MySQL|focal|8.0
MySQL|groovy|8.0
MySQL|sl15|8.0|sles
MySQL|sles12|8.0 5.7 5.6|sles
MySQL|stretch|5.6 5.7 8.0
MySQL|xenial|5.7 8.0
MySQL NDB Cluster|bionic|7.5 7.6 8.0
MySQL NDB Cluster|buster|7.5 7.6 8.0
MySQL NDB Cluster|el6|8.0 7.6 7.5|rhel centos
MySQL NDB Cluster|el7|8.0 7.6 7.5|rhel centos
MySQL NDB Cluster|el8|8.0|rhel centos
MySQL NDB Cluster|focal|8.0
MySQL NDB Cluster|groovy|8.0
MySQL NDB Cluster|sl15|8.0|sles
MySQL NDB Cluster|sles12|8.0 7.6 7.5|sles
MySQL NDB Cluster|stretch|7.5 7.6 8.0
MySQL NDB Cluster|xenial|7.5 7.6 8.0
MariaDB|fedora32|10.5
MariaDB|xenial|10.5 10.4 10.3 10.2
MariaDB|trusty|10.2
MariaDB|stretch|10.5 10.4 10.3 10.2
MariaDB|sles15|10.5 10.4 10.3 10.2|sles
MariaDB|sles12|10.5 10.4 10.3 10.2|sles
MariaDB|sid|10.5 10.4 10.3 10.2|debian
MariaDB|rhel8|10.5 10.4 10.3|rhel
MariaDB|rhel7|10.5 10.4 10.3 10.2|rhel
MariaDB|rhel6|10.3 10.2|rhel
MariaDB|opensuse15|10.5 10.4 10.3 10.2|opensuse
MariaDB|jessie|10.5 10.4 10.3 10.2
MariaDB|groovy|10.5 10.4 10.3
MariaDB|focal|10.5 10.4 10.3
MariaDB|centos8|10.5 10.4 10.3|centos
MariaDB|centos7|10.5 10.4 10.3 10.2|centos
MariaDB|centos6|10.3 10.2|centos
MariaDB|buster|10.5 10.4 10.3
MariaDB|bionic|10.5 10.4 10.3 10.2
Percona Server|xenial|8.0 5.7 5.6
Percona Server|stretch|8.0 5.7 5.6
Percona Server|rhel8|8.0 5.7|rhel centos
Percona Server|rhel7|8.0 5.7 5.6|rhel centos
Percona Server|rhel6|8.0 5.7 5.6|rhel centos
Percona Server|focal|8.0 5.7
Percona Server|buster|8.0 5.7 5.6
Percona Server|bionic|8.0 5.7 5.6
Percona XtraDB Cluster|xenial|8.0 5.7 5.6
Percona XtraDB Cluster|stretch|8.0 5.7 5.6
Percona XtraDB Cluster|rhel8|8.0 5.7|rhel centos
Percona XtraDB Cluster|rhel7|8.0 5.7 5.6|rhel centos
Percona XtraDB Cluster|rhel6|5.7 5.6|rhel centos
Percona XtraDB Cluster|focal|8.0 5.7
Percona XtraDB Cluster|buster|8.0 5.7
Percona XtraDB Cluster|bionic|8.0 5.7 5.6