#! /usr/bin/env bash

# chkconfig: 2345 80 05
# description: Redis Server Daemon
# processname: redis
# pidfile: /var/run/redis-server-${redis_port}.pid

### Initialization Info Start
# Creator:           MaxdSre
# Required-Start:    $all
# Required-Stop:     $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Start redis server daemon at boot time
### Initialization Info End

#  tested on
#  1. New lsb that define start-stop-daemon
#  3. Centos with initscripts package installed

### Parameters Default Configuraton Start
NAME=redis
DESC='Advanced key-value store'
USER=root
GROUP=root
DAEMON_DIR=/opt/Redis/bin
DAEMON=${DAEMON_DIR}/redis-server
CLIEXEC=${DAEMON_DIR}/redis-cli
PORT=6379
CONF_FILE=/etc/redis/redis.conf
SYS_CONFIG=/etc/sysconfig/${NAME}
LOG_DIR=/var/log/redis
LOG_FILE=${LOG_DIR}/${NAME}.log
PID_FILE=/var/run/redis-server-${PORT}.pid
LOCK_FILE=/var/lock/subsys/${NAME}
### Parameters Default Configuraton End

# Check daemon if exists or executable
if [ ! -x ${DAEMON} ]; then
  echo "Program not installed or not executable"
  exit 5
fi

# init.d / servicectl compatibility (openSUSE)
if [[ -f /etc/rc.status ]]; then
    . /etc/rc.status
    rc_reset
fi

# Source function library. (status/pidofproc/...)
[[ -f /etc/rc.d/init.d/functions ]] && . /etc/rc.d/init.d/functions

# Overwrite settings from default file
[[ -e ${SYS_CONFIG} ]] && . ${SYS_CONFIG}

function check_status() {
    status -p ${PID_FILE} redis-server > /dev/null 2>&1
}

function check_is_root() {
    if [[ $(id -u) -ne 0 ]]; then
        echo "You need root privileges to run this script"
        exit 4
    fi
}

function perparation() {
    if [[ ! -d "${LOG_DIR}" ]]; then
        mkdir -p "${LOG_DIR}"
        touch "${LOG_FILE}"
        chown -R "${USER}":"${GROUP}" "${LOG_DIR}"
        chmod 750 "${LOG_DIR}"
        chmod 640 "${LOG_FILE}"
    fi
}

function start() {
    check_is_root
    check_status
    if [[ $? -eq 0 ]]; then
      echo "${NAME} is already running."
      exit 0
    fi
    perparation

    # Start Daemon
    cd ${DAEMON_DIR}
    echo -n $"Starting ${NAME} ..."
	${DAEMON} ${CONF_FILE}
	echo ''
    $0 status
}

function stop() {
    check_is_root
    if [[ -f "${PID_FILE}" ]]; then
        PID=$(cat "${PID_FILE}")
        echo -n "Stopping $NAME ..."
        AUTH_PASS=$(sed -r -n '/^requirepass/{s@^requirepass[[:space:]]*(.*)$@\1@g;p}' "${CONF_FILE}")
        if [[ -n "${AUTH_PASS}" ]]; then
            ${CLIEXEC} -p ${PORT} -a ${AUTH_PASSWORD} shutdown
        else
            ${CLIEXEC} -p ${PORT} shutdown
        fi
        while [ -x /proc/${PID} ]
        do
            sleep 1
        done
        rm -f "${PID_FILE}" "${LOCK_FILE}"
        echo ''
        $0 status
    else
        echo "${NAME} is not running"
    fi
}


case "$1" in
    status )
        status -p ${PID_FILE} ${NAME}
        exit $?
        ;;
    start )
        start
        ;;
    stop )
        stop
        ;;
    restart|force-reload )
        if [[ -f "${PID_FILE}" ]]; then
            $0 stop
            usleep 15000
        fi
        $0 start
        ;;
    * )
        echo "Usage: $0 {start|stop|restart|force-reload|reload|status}"
        exit 3
        ;;
esac

# Script End
