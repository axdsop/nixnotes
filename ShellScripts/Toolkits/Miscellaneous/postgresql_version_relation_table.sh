#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails


# Official Site:
# - https://www.postgresql.org/download/
# - https://zypp.postgresql.org/
# - https://wiki.postgresql.org/wiki/Apt


# Target: Extract GNU/Linux distribution supported by PostgreSQL along with specific version list
# Developer: MaxdSre

# Change Log:
# - Feb 13, 2021 10:07 Sat ET - optimize release version extracting method, just list supported release version number
# - Oct 08, 2020 09:11 Thu ET - initialization check method update
# - Sep 27, 2019 15:02 Fri ET - change to new base functions
# - Apr 27, 2019 21:45 Sat ET - As of 15 April 2019, there is only one repository RPM per distro, and it includes repository information for all available PostgreSQL releases
# - Jun 18, 2018 13:30 Mon ET
# - Jan 01, 2018 20:10 Tue ET - code reconfiguration, include base funcions from other file


#########  0-1. Singal Setting  #########
trap fnBase_TrapCleanUp INT QUIT

#########  0-2. Variables Setting  #########
# umask 022    # temporarily change umask value to 022
proxy_server_specify=${proxy_server_specify:-''}


#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | sudo bash -s -- [options] ...

Extract GNU/Linux distributions supported by PostgreSQL along with specific version list!

[available option]
    -h    --help, show help info
    -p [protocol:]ip:port    --proxy host (http|https|socks4|socks5), default protocol is http
\e[0m"
}

while getopts "hp:" option "$@"; do
    case "$option" in
        p ) proxy_server_specify="$OPTARG" ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done

#########  1-2 Preparation Operation  #########
fn_InitializationCheck(){
    local l_user_codebase_dir="$HOME/.config/axdsop"
    local l_base_function_temp_save_path=''
    local l_need_remove=0
    local l_base_function_url='https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/base_functions.sh'
    l_base_function_temp_save_path=$(find "${l_user_codebase_dir}" -type f -name "${l_base_function_url##*/}" -print 2> /dev/null)
    
    if [[ ! -f "${l_base_function_temp_save_path}" ]]; then
        local l_download_tool=${l_download_tool:-'wget -qO-'}
        [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_tool='curl -fsL'
        l_need_remove=1
        l_base_function_temp_save_path=$(mktemp -t XXXXXXX)
        ${l_download_tool} "${l_base_function_url}" > "${l_base_function_temp_save_path}"
    fi

    if [[ -f "${l_base_function_temp_save_path}" && -s "${l_base_function_temp_save_path}" ]]; then
        . "${l_base_function_temp_save_path}"
        [[ "${l_need_remove}" -eq 1 ]] && rm "${l_base_function_temp_save_path}"
    else
        echo "Sorry, fail to get base script path."
        exit
    fi

    # specify 1 means need sudo privilege
    fnBase_RunningEnvironmentCheck '0'

    fnBase_CommandExistCheckPhase 'gawk'
    fnBase_CommandExistCheckPhase 'sed'
    # fnBase_CommandExistCheckPhase 'parallel'

    # Arch Linux utility name is rpmextract
    fnBase_CommandExistCheckPhase 'rpm2cpio' # Arch Linux: rpmextract
    fnBase_CommandExistCheckPhase 'cpio'
    # fnBase_CommandExistCheckPhase 'ar'
}

#########  2-1. RPM relationship  #########
fn_RelationshipForRPM_old(){
    local l_target_url=${l_target_url:-'https://yum.postgresql.org/repopackages.php'}
    # As of 15 April 2019, there is only one repository RPM per distro, and it includes repository information for all available PostgreSQL releases.
    $download_method "${l_target_url}" | sed -r -n '/Available Repository RPMs/,/d releases/{/(Amazon|Oracle|Scientific)/d;s@Red Hat Enterprise Linux@rhel@g;s@CentOS@centos@g;s@Fedora@fedora@g;s@.*<a href="([^"]+)">([^-]+).*@\2|\1@g;s@[[:space:]]*@@g;/x86_64/!d;p}' | awk -F\| 'BEGIN{OFS="|"}{item=gensub(/.*repos\/[^[:digit:]]+([^\/]+).*/,"\\1","g",$2);print item,$1,$2}' | awk -F\| -v suffix='rpm' 'BEGIN{OFS="|"}{if ($2 in arr){val_old=arr[$2];val_new=val_old" "$1;arr[$2]=val_new}else{arr[$2]=$1}}END{PROCINFO["sorted_in"]="@ind_str_desc"; for (i in arr) print suffix,i,arr[i]}' | sort -b -f -t"|" -k 2r,2
}

# Page says: As of 15 April 2019, there is only one repository RPM per distro, and it includes repository information for all available PostgreSQL releases

# https://yum.postgresql.org/news/non-free-reporpms-v1/   PostgreSQL RPM Repository now includes packages like oracle_fdw and db2_fdw, which are actually OSS, but depend on non-free software. This repo requires a new repository RPM.

fn_PgdgRPMVersionList(){
    local l_source="${1:-}"

    if [[ -n "${l_source}" ]]; then
        local l_link="${l_source%%|*}"
        local l_distro="${l_source##*|}"

        local file_path="/tmp/$RANDOM_${l_link##*/}"
        sleep 1
        $download_method "${l_link}" > "${file_path}"

        if [[ -f "${file_path}" && -s "${file_path}" ]]; then
            extract_dir=$(mktemp -d -t 'pgdg_XXXXXX')
            # rpm2cpio pgdg-redhat-repo-latest.noarch.rpm | cpio -idmv
            # ${extract_dir}/etc/yum.repos.d/pgdg-redhat-all.repo
            cd "${extract_dir}" && rpm2cpio "${file_path}" | cpio -idm 2> /dev/null
            extract_target_path=$(find "${extract_dir}/etc/" -type f -name *.repo)
            version_list=$(sed -r -n '/^[pgdg[[:digit:]]+]/,/^$/{/^name/{s@^[^[:digit:]]+([[:digit:].]+).*$@\1@g;p}}' "${extract_target_path}" | sed ':a;N;$!ba;s@\n@ @g')
            [[ -d "${extract_dir}" ]] && rm -rf "${extract_dir}"
            [[ -f "${file_path}" ]] && rm -f "${file_path}"

            echo "rpm|${l_distro// /}|${version_list}"
        fi
    fi
}

fn_RelationshipForRPM(){
    # For yum, supported release version
    local l_supported_releases=''
    l_supported_releases=$($download_method https://yum.postgresql.org 2> /dev/null | sed -r -n '/available PostgreSQL releases/I,/<\/ul>/{/<li[^>]*>/{/\(/d;s@[[:space:]]*<[^>]*>[[:space:]]*@@g;p}}' | sed ':a;N;$!ba;s@\n@|@g')
    #pg94 9.5 (no longer maintained by upstream)
    #pg14 v14 (PACKAGES ARE FOR ALPHA TESTING ONLY! USE v13 PACKAGES FOR PRODUCTION)

    local l_target_url=${l_target_url:-'https://yum.postgresql.org/repopackages.php'}
    # redirect to  https://yum.postgresql.org/repopackages/

    # - via gnu parallel
    # export -f fn_PgdgRPMVersionList
    # export download_tool="$download_method"
    # $download_method "${l_target_url}" | sed -r -n '/id="pgContentWrap"/,/EOL'\''d releases/{/<li>.*x86_64/{/nonfree/d; /(Amazon|Oracle|Scientific)/d; s@Red Hat Enterprise Linux@rhel@g; s@CentOS@centos@g; s@Fedora@fedora@g; s@.*href="([^"]+)"[^>]*>(.*?)[[:space:]]*-.*@\1|\2@g;p}}' | parallel -k -j 0 fn_PgdgRPMVersionList | sort -t"|" -k 2,2r

    # - via while loop
    $download_method "${l_target_url}" | sed -r -n '/id="pgContentWrap"/,/EOL'\''d releases/{/<li>.*x86_64/{/nonfree/d; /(Amazon|Oracle|Scientific)/d; s@Red Hat Enterprise Linux@rhel@g; s@CentOS@centos@g; s@Fedora@fedora@g; s@.*href="([^"]+)"[^>]*>(.*?)[[:space:]]*-.*@\1|\2@g;p}}' | while IFS="|" read -r pack_download_link distro; do
        # https://download.postgresql.org/pub/repos/yum/reporpms/EL-8-x86_64/pgdg-redhat-repo-latest.noarch.rpm|rhel 8
        file_path="/tmp/${pack_download_link##*/}"
        $download_method "${pack_download_link}" > "${file_path}"

        if [[ -f "${file_path}" && -s "${file_path}" ]]; then
            extract_dir=$(mktemp -d -t 'pgdg_XXXXXX')
            # rpm2cpio pgdg-redhat-repo-latest.noarch.rpm | cpio -idmv
            # ${extract_dir}/etc/yum.repos.d/pgdg-redhat-all.repo
            # On Arch Linux rpm2cpio (rpmextract) not work properly
            cd "${extract_dir}" && rpm2cpio "${file_path}" | cpio -idm 2> /dev/null
            extract_target_path=$(find "${extract_dir}/etc/" -type f -name *.repo 2> /dev/null)
            if [[ -f "${extract_target_path}" ]]; then
                version_list=$(sed -r -n '/^[pgdg[[:digit:]]+]/,/^$/{/^name/{s@^[^[:digit:]]+([[:digit:].]+).*$@\1@g;p}}' "${extract_target_path}")
                [[ -n "${l_supported_releases}" ]] && version_list=$(sed -r -n '/('"${l_supported_releases}"')/{p}' <<< "${version_list}")
                version_list=$(sed ':a;N;$!ba;s@\n@ @g' <<< "${version_list}")
                echo "rpm|${distro// /}|${version_list}"
            fi

            [[ -d "${extract_dir}" ]] && rm -rf "${extract_dir}"
            [[ -f "${file_path}" ]] && rm -f "${file_path}"
            # sleep 1
        fi
    done | sort -t"|" -k 2,2r
}


#########  2-2. DEB distro&codename relationship  #########
# https://wiki.postgresql.org/wiki/Apt

fn_PgdgDEBVersionList(){
    local l_source="${1:-}"
    local l_codename="${l_source%%|*}"
    local l_link="${l_source##*|}"
    local l_list
    # l_list=$($download_method "${l_link}" | sed -r -n '/<body[^>]*>/,/<\/body>/{/href=/!d;s@.*href="([^"]+)".*@\1@g;/^[[:digit:]]/!d;s@\/@@g;p}' | sort -t. -k1,1rn -k2,2rn -k3,3rn | sed ':a;N;$!ba;s@\n@ @g')
    l_list=$($download_method "${l_link}" | sed -r -n '/<body[^>]*>/,/<\/body>/{/href=/!d;s@.*href="([^"]+)".*@\1@g;/^[[:digit:]]/!d;s@\/@@g;p}' | sort -t. -k1,1rn -k2,2rn -k3,3rn)
    [[ -n "${deb_supported_releases}" ]] && l_list=$(sed -r -n '/('"${deb_supported_releases}"')/{p}' <<< "${l_list}")
    l_list=$(sed ':a;N;$!ba;s@\n@ @g' <<< "${l_list}")
    echo "deb|${l_codename}|${l_list}"
}

fn_RelationshipForDEB(){
    # https://wiki.postgresql.org/wiki/Apt
    local deb_supported_releases=''
    deb_supported_releases=$($download_method https://wiki.postgresql.org/wiki/Apt 2> /dev/null | sed -r -n '/PostgreSQL packages for Debian and Ubuntu/I,/<h2[^>]*>/{/<li>.*PostgreSQL/I{s@.*?PostgreSQL[[:space:]]*([^<]*).*@\1@Ig;s@,[[:space:]]*@\|@g;p}}')

    local l_target_url=${l_target_url:-'https://apt.postgresql.org/pub/repos/apt/dists/'}
    local l_codename_table=''
    l_codename_table=$($download_method "${l_target_url}" | sed -r -n '/<body[^>]*>/,/<\/table>/{/href=/!d;/testing/d;/pgdg/!d;s@.*href="([^"]+)".*@\1@g;p}' | awk -v site="${l_target_url}" 'BEGIN{OFS="|"}{codename=gensub(/^([^-]+).*/,"\\1","g",$0); print codename,site$1}')
    # bionic|https://apt.postgresql.org/pub/repos/apt/dists/bionic-pgdg/

    if fnBase_CommandExistIfCheck 'parallell'; then
        export -f fn_PgdgDEBVersionList
        export download_method="${download_method}"
        export deb_supported_releases="${deb_supported_releases}"
        echo "${l_codename_table}" | parallel -k -j 0 fn_PgdgDEBVersionList
    else
        echo "${l_codename_table}" | while read -r line; do
            fn_PgdgDEBVersionList "${line}"
        done
    fi
}

#########  2-3. ZYPPER distro&codename relationship  #########
fn_RelationshipForZYPPER(){
    # https://zypp.postgresql.org/
    # https://zypp.postgresql.org/howtozypp.php
    # https://download.postgresql.org/pub/repos/zypp/repo/
    local l_target_url='https://zypp.postgresql.org/howtozypp.php'
}

#########  2-4. Central Operation  #########
fn_CentralOperation(){
    fn_RelationshipForRPM
    fn_RelationshipForDEB
}


#########  3. Executing Process  #########
fn_InitializationCheck
fn_CentralOperation


#########  4. EXIT Singal Processing  #########
# trap "commands" EXIT # execute command when exit from shell
fn_TrapEXIT(){
    fnBase_UnsetVars
    unset proxy_server_specify
}

trap fn_TrapEXIT EXIT

# Script End
