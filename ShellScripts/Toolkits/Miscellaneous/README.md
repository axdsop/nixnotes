# Toolkits - Miscellaneous

## List

* [Rename image/video via exiftool](./rename_image_video_via_exiftool.sh)
* [PostgreSQL Variants Version Relation Table](./postgresql_version_relation_table.sh)
* [MySQL Variants Version Relation Table](./mysql_variants_version_relation_table.sh)


<!-- End -->

