#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails

# Target: Rename Images & Videos Based On Timestamps
# Developer: MaxdSre
# Change Log:
# - Sep 27, 2019 Fri 09:42 ET - change custom function name format
# - Aug 04, 2019 Sun 20:55 ET - first draft


# https://superuser.com/questions/595177/how-to-retrieve-video-file-information-from-command-line-under-linux#1035178
# sudo pacman -S perl-image-exiftool


#########  0-1. Variables Setting  #########
umask 022
source_path=${source_path:-}
destnation_path=${destnation_path:-}


#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | bash -s -- [options] ...

Rename images/videos based on timestamps via tool exiftool on GNU/Linux.

File name format example: $(date +'%Y%m%d-%H%M%S')_4032x3024_iPhone6s.jpg

[available option]
    -h    --help, show help info
    -s source_path    --specify source path contains images or videos
    -d destnation_path    --specify destnation directory to save renamed file
\e[0m"
}

while getopts "hs:d:" option "$@"; do
    case "$option" in
        s ) source_path="$OPTARG" ;;
        d ) destnation_path="$OPTARG" ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done


#########  2-1 Variables Verification  #########
fn_VariablesVerification(){
    if [[ -z $(which exiftool 2> /dev/null || command -v exiftool 2> /dev/null) ]]; then
        echo "Sorry, this script need command exiftool."
        exit
    fi

    if [[ -z "${source_path}" ]]; then
        echo "Please specify source path by first.";
        exit
    fi

    if [[ -n "${destnation_path}" ]]; then
        destnation_path="${destnation_path%/}"
    else
        echo "Please specify destnation dir by first."
        exit
    fi

    # [[ -d "${destnation_path}" ]] || (echo "Attention: destnation path must be a directory."; exit)
}


#########  3-1 Operation Procedure  #########
fn_ExitInfoExtraction(){
    local l_path="${1:-}"
    local l_output=''
    if [[ -n "${l_path}" && -f "${l_path}" ]]; then
        # https://superuser.com/questions/595177/how-to-retrieve-video-file-information-from-command-line-under-linux#1035178
        # sudo pacman -S perl-image-exiftool
        local l_exitinfo=''
        l_exitinfo=$(exiftool "${l_path}" 2> /dev/null)

        if [[ -n "${l_exitinfo}" ]]; then
            local l_mime_type='' # video/quicktime, image/jpeg
            l_mime_type=$(echo "${l_exitinfo}" | sed -r -n '/^MIME Type[[:space:]]+:/{s@.*:[[:space:]]*([^\/]+).*@\1@g;p}')

            local l_file_extension=''
            local l_camera_model_name=''
            local l_create_date=''
            local l_resolution=''

            l_file_extension=$(echo "${l_exitinfo}" | sed -r -n '/^(File Type Extension)[[:space:]]+:/{s@^[^:]+:[[:space:]]*(.*)@\1@g;p}')

            l_camera_model_name=$(echo "${l_exitinfo}" | sed -r -n '/^(Camera Model Name)[[:space:]]+:/{s@^[^:]+:[[:space:]]*(.*)@\1@g;p}')
            [[ -z "${l_camera_model_name}" ]] && l_camera_model_name=$(echo "${l_exitinfo}" | sed -r -n '/^(Model)[[:space:]]+:/{s@^[^:]+:[[:space:]]*(.*)@\1@g;p}')

            case "${l_mime_type}" in
                image )
                    l_create_date=$(echo "${l_exitinfo}" | sed -r -n '/^(Date\/Time Original)[[:space:]]+:/{/Date\/Time Original/{/\./d};s@^[^:]+:[[:space:]]*(.*)@\1@g;p}')
                    [[ -z "${l_create_date}" ]] && l_create_date=$(echo "${l_exitinfo}" | sed -r -n '/^(File Modification Date\/Time)[[:space:]]+:/{s@^[^:]+:[[:space:]]*(.*)@\1@g;p}')
                    ;;
                video )
                    l_create_date=$(echo "${l_exitinfo}" | sed -r -n '/^(Creation Date)[[:space:]]+:/{s@^[^:]+:[[:space:]]*(.*)@\1@g;p}')
                    [[ -z "${l_create_date}" ]] && l_create_date=$(echo "${l_exitinfo}" | sed -r -n '/^(Media Create Date)[[:space:]]+:/{s@^[^:]+:[[:space:]]*(.*)@\1@g;p}')
                    [[ -z "${l_create_date}" ]] && l_create_date=$(echo "${l_exitinfo}" | sed -r -n '/^(File Modification Date\/Time)[[:space:]]+:/{s@^[^:]+:[[:space:]]*(.*)@\1@g;p}')
                    ;;
            esac

            [[ -n "${l_create_date}" ]] && l_create_date=$(echo "${l_create_date}" | sed -r -n '{s@-.*$@@g;s@:@@g;s@[[:blank:]]+@-@g;p}')

            l_resolution=$(echo "${l_exitinfo}" | sed -r -n '/^(Image Size)[[:space:]]+:/{s@^[^:]+:[[:space:]]*(.*)@\1@g;p}')

            l_output="${l_create_date}_${l_resolution}"
            [[ -n "${l_camera_model_name}" ]] && l_output="${l_output}_${l_camera_model_name// /}"
            l_output="${l_output}.${l_file_extension}"

            [[ -n "${l_output}" ]] && l_output="${l_mime_type}|${l_output}"
        fi
    fi

    echo "${l_output}"
}

fn_FileRenameOperation(){
    local l_file_path="${1:-}"
    local l_save_dir=${2:-"${destnation_path}"}
    local l_mime_info=''

    if [[ -f "${l_file_path}" ]]; then
        l_mime_info=$(fn_ExitInfoExtraction "${l_file_path}")

        if [[ -n "${l_mime_info}" ]]; then
            echo "${l_mime_info}" | while IFS="|" read -r mime_type file_new_name; do
                [[ -d "${l_save_dir}/${mime_type}" ]] || mkdir -p "${l_save_dir}/${mime_type}"

                local l_new_save_path="${l_save_dir}/${mime_type}/${file_new_name}"
                if [[ -f "${l_new_save_path}" ]]; then
                    echo "Attention: save path ${l_new_save_path} existed."
                else
                    echo "Old name is ${l_file_path##*/},new name is ${file_new_name}, destnation is ${l_save_dir}/${mime_type}"
                    cp "${l_file_path}" "${l_new_save_path}"
                fi
            done
        fi
    fi
}


#########  4-1 Operation Procedure  #########
fn_MainProcess(){
    fn_VariablesVerification

    if [[ -n $(which parallel 2> /dev/null || command -v parallel 2> /dev/null) ]]; then
        export -f fn_ExitInfoExtraction
        export -f fn_FileRenameOperation
        export destnation_path="${destnation_path}"

        find "${source_path}" -type f -print | parallel -k -j 0 fn_FileRenameOperation 2> /dev/null
    else
        find "${source_path}" -type f -print | while read -r line; do
            fn_FileRenameOperation "${line}"
        done
    fi
}

fn_MainProcess

# Script End
