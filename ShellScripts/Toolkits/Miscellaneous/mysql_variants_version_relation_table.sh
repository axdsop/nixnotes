#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails

#Official Site:
# - https://dev.mysql.com/
# - https://www.percona.com/
# - https://mariadb.org/

# Target: Extract GNU/Linux distribution supported by MySQL/MariaDB/Percona along with specific version list
# Developer: MaxdSre

# Change Log:
# - Feb 11, 2021 17:10 Thu ET - fix Percona release info extraction issue
# - Oct 08, 2020 09:11 Thu ET - initialization check method update
# - Mar 17, 2020 10:42 Tue ET - fix MySQL RPM/APT release info extraction issue
# - Oct 15, 2019 12:03 Tue ET - change custom script url
# - Oct 02, 2019 09:31 Wed ET - change custom shellscript url 'custom_shellscript_url', 'relation_script_path'
# - Oct 01, 2019 13:52 Tue ET - change to new base functions
# - Jan 01, 2019 19:53 Tue ET - code reconfiguration, include base funcions from other file
# - Nov 29, 2018 11:14 Thu ET - MySQL deb package transfer control.tar.gz to control.tar.xz
# - Oct 07, 2018 09:59 Sun +0800 - Percona page code update
# - Nov 15, 2017 19:35 Wed +0800
# - Oct 24, 2017 15:40 Tue +0800
# - Aug 30, 2017 19:35 Wed +0800
# - Aug 29, 2017 13:06 Tue +0800


#########  0-1. Singal Setting  #########
trap fnBase_TrapCleanUp INT QUIT

#########  0-2. Variables Setting  #########
# umask 022    # temporarily change umask value to 022
readonly custom_shellscript_url='https://gitlab.com/axdsop/nixnotes/raw/master/Script/Toolkits'
readonly relation_script_path="${custom_shellscript_url}/GNULinux/gnuLinuxLifeCycleInfo.sh"
dbname_choose=${dbname_choose:-'all'}
proxy_server_specify=${proxy_server_specify:-''}


#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | sudo bash -s -- [options] ...

Extract GNU/Linux distribution supported by MySQL/MariaDB/Percona along with specific version list!

[available option]
    -h    --help, show help info
    -d db_name    -- choose database type (MySQL/MariaDB/Percona), default is all
    -p [protocol:]ip:port    --proxy host (http|https|socks4|socks5), default protocol is http
\e[0m"
}

while getopts "hd:p:" option "$@"; do
    case "$option" in
        d ) dbname_choose="$OPTARG" ;;
        p ) proxy_server_specify="$OPTARG" ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done


#########  1-2 Preparation Operation  #########
fn_GetoptsVariableChecking(){
    case "${dbname_choose,,}" in
        MySQL|mysql|my|m ) dbname_choose='mysql' ;;
        MariaDB|mariadb|ma ) dbname_choose='mariadb' ;;
        Percona|percona|p ) dbname_choose='percona' ;;
        * ) dbname_choose='all' ;;
    esac
}

fn_InitializationCheck(){
    local l_user_codebase_dir="$HOME/.config/axdsop"
    local l_base_function_temp_save_path=''
    local l_need_remove=0
    local l_base_function_url='https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/base_functions.sh'
    l_base_function_temp_save_path=$(find "${l_user_codebase_dir}" -type f -name "${l_base_function_url##*/}" -print 2> /dev/null)
    
    if [[ ! -f "${l_base_function_temp_save_path}" ]]; then
        local l_download_tool=${l_download_tool:-'wget -qO-'}
        [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_tool='curl -fsL'
        l_need_remove=1
        l_base_function_temp_save_path=$(mktemp -t XXXXXXX)
        ${l_download_tool} "${l_base_function_url}" > "${l_base_function_temp_save_path}"
    fi

    if [[ -f "${l_base_function_temp_save_path}" && -s "${l_base_function_temp_save_path}" ]]; then
        . "${l_base_function_temp_save_path}"
        [[ "${l_need_remove}" -eq 1 ]] && rm "${l_base_function_temp_save_path}"
    else
        echo "Sorry, fail to get base script path."
        exit
    fi

    # tput clear  # Echo the clear-screen sequence for the current terminal.
    fnBase_RunningEnvironmentCheck '0'

    fnBase_CommandExistCheckPhase 'gawk'
    fnBase_CommandExistCheckPhase 'sed'
    fnBase_CommandExistCheckPhase 'parallel'

    case "${dbname_choose}" in
        mysql|all )
            fnBase_CommandExistCheckPhase 'rpm2cpio' # Arch Linux: rpmextract
            fnBase_CommandExistCheckPhase 'ar' 'binutils'
            ;;
    esac
}


#########  2-0. Debian/Ubuntu distro&codename relationship  #########
fn_DebianUbuntuCodeRelationTable(){
    debian_codename_ralation=$(mktemp -t "${mktemp_format}")

    $download_method "${relation_script_path}" | bash -s -- -c ubuntu | awk -F\| -v distro='ubuntu' 'BEGIN{OFS="|"}NR>1{codename=gensub(/^([[:alpha:]]+).*/,"\\1","1",tolower($2));a[codename]=$2}END{for(i in a) print distro,tolower(i)}' >> "${debian_codename_ralation}"

    $download_method "${relation_script_path}" | bash -s -- -c debian | awk -F\| -v distro='debian' 'BEGIN{OFS="|"}NR>1{a[$2]++}END{for(i in a) print distro,tolower(i)}' >> "${debian_codename_ralation}"
}

#########  2-1. Percona Operation  #########
# rhel6 ==> rhel6, centos6
# - version|distro_list
fn_PerconaDistroListForPerVersion(){
    # $1 = Percona XtraDB Cluster|5.7|https://www.percona.com/downloads/Percona-XtraDB-Cluster-LATEST/

    local item=${1:-}
    if [[ -n "${item}" ]]; then
        local db_name=${db_name:-}
        local db_version=${db_version:-}
        local db_version_url=${db_version_url:-}
        local distro_list=${distro_list:-}

        echo "${item}" | while IFS="|" read -r item1 item2 item3; do
            db_name="${item1}"
            db_version="${item2}"
            db_version_url="${item3}"

            distro_list=$($download_method "${db_version_url}" | sed -r -n '/Select Software Platform/{s@redhat/@rhel@g;s@<\/option>@\n@g;p}' | sed -r -n 's@.*/([^"]*)"[[:space:]]*>.*@\1@g;/binary|source|select|^$/d;p' | sed -r ':a;N;$!ba;s@\n@ @g;')
            echo "${db_name}|${db_version}|${distro_list}" >> "${version_list_info}"
            # Percona Server|5.7|wheezy jessie stretch rhel6 rhel7 trusty xenial yakkety zesty
        done
    fi
}

# - db_name|distro|version_list
fn_PerconaVersionListPerDistro(){
    local distro=${1:-}
    local version_list=${version_list:-}

    awk -F\| '{!arr[$1]++}END{for(i in arr) print i}' "${version_list_info}" | while IFS="" read -r db_name; do
        version_list=$(awk -F\| 'match($1,/^'"${db_name}"'$/)&&match($NF,/[[:space:]]*'"${distro}"'[[:space:]]*/){a[$2]=$3}END{PROCINFO["sorted_in"]="@ind_str_desc";for(i in a) print i}' "${version_list_info}" | sed -r ':a;N;$!ba;s@\n@ @g;')

        local distro_list=${distro_list:-}

        if [[ -n "${version_list}" ]]; then
            if [[ "${distro}" =~ ^rhel ]]; then
                distro_list='rhel centos'
            else
                [[ -s "${debian_codename_ralation}" ]] && distro_list=$(awk -F\| 'match($2,/^'"${distro}"'$/){print $1}' "${debian_codename_ralation}")
            fi

            [[ -z "${distro_list}" ]] || distro_list="|${distro_list}"
            echo "${db_name}|${distro}|${version_list}${distro_list}"
        fi

    done
}

fn_PerconaOperation(){
    version_list_info=$(mktemp -t "${mktemp_format}")
    local official_site=${official_site:-'https://www.percona.com'}
    local download_page=${download_page:-"${official_site}/downloads/"}

    # - step 1: version|distro_list
    export version_list_info="${version_list_info}"
    export -f fn_PerconaDistroListForPerVersion
    export download_method="${download_method}"

    $download_method "${download_page}" | sed -r -n '/<h4>[[:space:]]*Percona.*(Server|XtraDB).*<\/h4>/,/<\/div>/{/<\/(p|div)>/d;/for MongoDB/,$d;/href=/{s@.* href="([^"]*)"[^>]*>Download ([[:digit:].]+).*@\2|'"${official_site}"'\1@g;};/<h4>/d;/Percona-Server/s@.*@Percona Server|&@g;/Percona-XtraDB-Cluster/s@.*@Percona XtraDB Cluster|&@g;p}' | parallel -k -j 0 fn_PerconaDistroListForPerVersion 2> /dev/null

    # - step 2: db_name|distro|version_list
    export version_list_info="${version_list_info}"
    export debian_codename_ralation="${debian_codename_ralation}"
    export -f fn_PerconaVersionListPerDistro

    sed -r 's@.*\|@@g' "${version_list_info}" | sed -r ':a;N;$!ba;s@\n@ @g;s@ @\n@g' | awk '{a[$0]++}END{PROCINFO["sorted_in"]="@ind_str_asc";for (i in a) print i}' | parallel -k -j 0 fn_PerconaVersionListPerDistro | sort -b -f -t"|" -k 1,1 -k 2r,2 2> /dev/null

    [[ -f "${version_list_info}" ]] && rm -f "${version_list_info}"
    unset version_list_info
}



#########  2-2. MySQL Operation  #########
# el5 ==> rhel5, centos5
# fc26 ==> fedora26
fn_MySQLOperationForRPM(){
    # https://dev.mysql.com/downloads/file/?id=470281|mysql57-community-release-el7-11.noarch.rpm|c070b754ce2de9f714ab4db4736c7e05|
    local item=${1:-}
    if [[ -n "${item}" ]]; then
        local download_page=${download_page:-}
        local pack_name=${pack_name:-}
        local md5_official=${md5_official:-}

        item="${item%|}"
        download_page="${item%%|*}"
        item=${item#*|}
        pack_name="${item%%|*}"
        md5_official="${item##*|}"

        # download_page=$(echo "${item}" | awk -F\| '{print $1}')
        # pack_name=$(echo "${item}" | awk -F\| '{print $2}')
        # md5_official=$(echo "${item}" | awk -F\| '{print $3}')

        # el7
        distro=$(echo "${pack_name}" | sed -r -n 's@.*release-([[:alnum:]]+).*@\1@g;p')
        pack_download_link=$($download_method "${download_page}" | sed -r -n '/thanks/{s@.*"(.*)".*@https://dev.mysql.com\1@g;p}')
        pack_download_link="https://dev.mysql.com/get/${pack_name}"

        file_path="/tmp/${pack_name}"
        [[ -f "${file_path}" ]] && rm -f "${file_path}"
        $download_method "${pack_download_link}" > "${file_path}"
        md5_checksum=$(md5sum "${file_path}" | awk '{print $1}')
        # md5_checksum=$(openssl dgst -md5 "${file_path}" | awk '{print $NF}')
        if [[ "${md5_official}" != "${md5_checksum}" ]]; then
            echo "Attention: file ${pack_name} no approved MD5 ${md5_checksum} check."
        else
            extract_dir=$(mktemp -d -t 'mysql_XXXXXX')

            # rpm2cpio mysql57-community-release-el7.rpm | cpio -idmv
            cd "${extract_dir}" && rpm2cpio "${file_path}" | cpio -idm 2> /dev/null
            extract_target_path="${extract_dir}${target_path}"

            if [[ -s "${extract_target_path}" ]]; then
                # version_and_cluster_list_info=$(mktemp -t "${mktemp_format}")
                version_and_cluster_list_info=$(mktemp -t "${mktemp_format}")
                awk 'match($1,/^\[mysql[[:digit:]]/)||match($1,/^\[mysql-cluster-[[:digit:]]/){getline;val=gensub(/.*MySQL[[:space:]]*(.*[[:digit:]]).*/,"\\1","g",$0);gsub("Cluster ","cluster-",val);a[val]=val}END{PROCINFO["sorted_in"]="@ind_str_desc";for(i in a) print i}' "${extract_target_path}" >> "${version_and_cluster_list_info}"

                local cluster_str=${cluster_str:-}
                local common_str=${common_str:-}

                while read -r line; do
                    if [[ ${line} =~ ^cluster ]]; then
                        line=${line##*-}
                        if [[ -z "${cluster_str}" ]]; then
                            cluster_str="${line}"
                        else
                            cluster_str="${cluster_str} ${line}"
                        fi
                    else
                        if [[ -z "${common_str}" ]]; then
                            common_str="${line}"
                        else
                            common_str="${common_str} ${line}"
                        fi
                    fi
                done < "${version_and_cluster_list_info}"

                local db_name
                # sles11,sles12,sl15

                if [[ -n "${cluster_str}" ]]; then
                    db_name="MySQL NDB Cluster"
                    echo "${cluster_str}" | sed 's@.*@'"${db_name}|${distro}"'|&@' | awk -F\| '{if($2~/^el/){printf("%s|%s\n",$0,"rhel centos")}else if($2~/^fc/){printf("%s|%s\n",$0,"fedora")}else if($2~/^(sles|sl)/){printf("%s|%s\n",$0,"sles")}else{print}}'

                fi

                if [[ -n "${common_str}" ]]; then
                    db_name="MySQL"
                    echo "${common_str}" | sed 's@.*@'"${db_name}|${distro}"'|&@' | awk -F\| '{if($2~/^el/){printf("%s|%s\n",$0,"rhel centos")}else if($2~/^fc/){printf("%s|%s\n",$0,"fedora")}else if($2~/^(sles|sl)/){printf("%s|%s\n",$0,"sles")}else{print}}'
                fi

            fi

            [[ -d "${extract_dir}" ]] && rm -rf "${extract_dir}"
            [[ -f "${file_path}" ]] && rm -f "${file_path}"
            [[ -f "${version_and_cluster_list_info}" ]] && rm -f "${version_and_cluster_list_info}"
        fi

    fi
}

fn_MySQLForRPM(){
    rpm_type="${1:-}"
    local repo_page=${repo_page:-}
    local target_path=${target_path:-}
    case "${rpm_type,,}" in
        yum|y )
            repo_page='https://dev.mysql.com/downloads/repo/yum/'
            target_path='/etc/yum.repos.d/mysql-community.repo'
            ;;
        suse|sles|s)
            repo_page='https://dev.mysql.com/downloads/repo/suse/'
            target_path='/etc/zypp/repos.d/mysql-community.repo'
            ;;
    esac

    export download_method="${download_method}"
    export -f fn_MySQLOperationForRPM
    export target_path="${target_path}"
    export mktemp_format="${mktemp_format}"
    curl -fsL "${repo_page}" | sed -r -n '/<table/,/<\/table>/{/(button03|sub-text|md5)/!d;/style=/d;s@^[^<]*@@g;s@.*href="(.*)".*@https://dev.mysql.com\1@g;s@.*\((.*)\).*@\1@g;s@[[:space:]]*<\/td>@\n@g;s@<\/[^>]*>@&\n@;s@<[^>]*>@@g;p}' | awk '{if($0!~/^$/){ORS="|";print $0}else{printf "\n"}}' | parallel -k -j 0 fn_MySQLOperationForRPM 2> /dev/null
}

fn_MySQLForDEB(){
    local repo_page=${repo_page:-'https://dev.mysql.com/downloads/repo/apt/'}

    # https://dev.mysql.com/downloads/file/?id=482263|mysql-apt-config_0.8.11-1_all.deb|c9f3d46804d339a7797e14018bd44104|
    $download_method "${repo_page}" | sed -r -n '/<table/,/<\/table>/{/(button03|sub-text|md5)/!d;/style=/d;s@^[^<]*@@g;s@.*href="(.*)".*@https://dev.mysql.com\1@g;s@.*\((.*)\).*@\1@g;s@[[:space:]]*<\/td>@\n@g;s@<\/[^>]*>@&\n@;s@<[^>]*>@@g;p}' | awk '{if($0!~/^$/){ORS="|";print $0}else{printf "\n"}}' | while IFS="|" read -r download_page pack_name md5_official; do
        # https://dev.mysql.com/get/mysql-apt-config_0.8.11-1_all.deb
        pack_download_link=$($download_method "${download_page}" | sed -r -n '/thanks/{s@.*"(.*)".*@https://dev.mysql.com\1@g;p}')
        pack_download_link="https://dev.mysql.com/get/${pack_name}"

        file_path="/tmp/${pack_name}"
        [[ -f "${file_path}" ]] && rm -f "${file_path}"
        $download_method "${pack_download_link}" > "${file_path}"
        md5_checksum=$(md5sum "${file_path}" | awk '{print $1}')
        # md5_checksum=$(openssl dgst -md5 "${file_path}" | awk '{print $NF}')
        if [[ "${md5_official}" != "${md5_checksum}" ]]; then
            echo "Attention: file ${pack_name} no approved MD5 ${md5_checksum} check."
        else
            extract_dir=$(mktemp -d -t 'mysql_XXXXXX')

            # https://www.cyberciti.biz/faq/how-to-extract-a-deb-file-without-opening-it-on-debian-or-ubuntu-linux/
            cd "${extract_dir}" && ar -x "${file_path}"
            if [[ -f "${extract_dir}/control.tar.gz" ]]; then
                tar xf "${extract_dir}/control.tar.gz"
            elif [[ -f "${extract_dir}/control.tar.xz" ]]; then
                tar xf "${extract_dir}/control.tar.xz"
            fi

            local db_name=${db_name:-'MySQL'}

            # default order
            # [[ -s "${extract_dir}/config" ]] && sed -r -n '/case/,/esac/{s@^[[:space:]]*@@g;s@\)@@g;/(;;|case|esac)/d;s@^[^"]*"@@g;s@(mysql-|preview)@@g;s@,?[[:space:]]*cluster.*$@@g;s@[[:space:]]*"?@@g;/(^$|\*)/d;p}' "${extract_dir}/config" | sed 'N;s@\n@ @g;' | awk -v db_name="${db_name}" 'BEGIN{OFS="|"}{gsub(/,/," ",$NF);print db_name,$1,$NF}'

            version_and_cluster_list_info=$(mktemp -t "${mktemp_format}")

            # reverse order
            # jessie 5.6,5.7,cluster-7.5,cluster-7.6
            # stretch 5.6,5.7,8.0,cluster-7.5,cluster-7.6
            # trusty 5.6,5.7,cluster-7.5,cluster-7.6
            # xenial 5.7,8.0,cluster-7.5,cluster-7.6
            # bionic 5.7,8.0,cluster-7.5,cluster-7.6
            # cosmic 5.7,8.0,cluster-7.5,cluster-7.6

            [[ -s "${extract_dir}/config" ]] && sed -r -n '/case/,/esac/{s@^[[:space:]]*@@g;s@\)@@g;/(;;|case|esac)/d;s@^[^"]*"@@g;s@(mysql-|preview)@@g;s@[[:space:]]*"?@@g;/(^$|\*)/d;p}' "${extract_dir}/config" | sed 'N;s@\n@ @g;' | while IFS=" " read -r codename version_list; do
                echo "${version_list}" | sed 's@,@\n@g' | sed -r -n 's@.*@'"${codename}"' &@g;p' | awk -v name='Cluster' 'BEGIN{OFS="|"}{if($2~/cluster-/){
                    if(($1,cluster) in arr){item=arr[$1,cluster];item=item" "$2;arr[$1,cluster]=item}else{arr[$1,cluster]=$2}
                    }else{
                        if(($1) in arr){item=arr[$1];item=item" "$2;arr[$1]=item}else{arr[$1]=$2}
                    }}
                    END{for(i in arr) {split(i,j,SUBSEP); if(arr[i]~/cluster/){gsub("cluster-","",arr[i]);print j[1],arr[i],name}else{print j[1],arr[i]} }}' >> "${version_and_cluster_list_info}"
                # wheezy|5.6 5.7
                # wheezy|7.5 7.6|Cluster
            done

            sort -b -f -t"|" -k 3,3 "${version_and_cluster_list_info}" | while IFS="|" read -r codename version_list cluster_flag; do
                [[ -n "${cluster_flag}" ]] && db_name="MySQL NDB Cluster"
                local distro_list=${distro_list:-}
                if [[ -n "${version_list}" ]]; then
                    [[ -s "${debian_codename_ralation}" ]] && distro_list=$(awk -F\| 'match($2,/^'"${codename}"'$/){print $1}' "${debian_codename_ralation}")

                    [[ -z "${distro_list}" ]] || distro_list="|${distro_list}"
                    echo "${db_name}|${codename}|${version_list}${distro_list}"
                fi
                # unset distro_list
            done

            [[ -d "${extract_dir}" ]] && rm -rf "${extract_dir}"
            [[ -f "${file_path}" ]] && rm -f "${file_path}"
            [[ -f "${version_and_cluster_list_info}" ]] && rm -f "${version_and_cluster_list_info}"
        fi

    done
}

fn_MySQLOperation(){
    mysql_list_info=$(mktemp -t "${mktemp_format}")
    # invoking custom function
    # fn_MySQLForRPM 'yum' >> "${mysql_list_info}"
    # fn_MySQLForRPM 'suse' >> "${mysql_list_info}"
    { fn_MySQLForRPM 'yum'; fn_MySQLForRPM 'suse'; } >> "${mysql_list_info}"
    fn_MySQLForDEB >> "${mysql_list_info}"
    sort -b -f -t"|" -k 1,1 "${mysql_list_info}"
    [[ -f "${mysql_list_info}" ]] && rm -f "${mysql_list_info}"
}


#########  2-3. MariaDB Operation  #########
# fedora26 ==> fedora26
# opensuse42 ==> opensuse 42

# all supported distribution lists for every MariaDB release version
# curl -fsL https://downloads.mariadb.org/mariadb/repositories | sed -r -n '/Choose a Version/,/Choose a Mirror/{s@^[[:space:]]*@@g;/^<[^(\/?li)]/d;p}' | awk '{if($0!~/^<\/li>/){ORS=" ";print $0}else{printf "\n"}}' | sed -r -n '/class=""/d;s@.* data-value="([^"]*)".*class="[[:space:]]*([^"]*)".*>([[:digit:].]+)[[:space:]]*\[(.*)\]@\L\3|\4|\2@g;/^[[:digit:]]/!d;p'

fn_MariaDBOperation(){
    local mariadb_repositories_page=${mariadb_repositories_page:-'https://downloads.mariadb.org/mariadb/repositories'}
    page_source=$(mktemp -t "${mktemp_format}")
    distro_lists_per_mariadb_version=$(mktemp -t "${mktemp_format}")
    local db_name=${db_name:-'MariaDB'}

    [[ -s "${page_source}" ]] || $download_method "${mariadb_repositories_page}" > "${page_source}"

    [[ -f "${distro_lists_per_mariadb_version}" ]] && echo '' > "${distro_lists_per_mariadb_version}"

    sed -r -n '/Choose a Version/,/Choose a Mirror/{s@^[[:space:]]*@@g;/^<[^(\/?li)]/d;p}' "${page_source}" | awk '{if($0!~/^<\/li>/){ORS=" ";print $0}else{printf "\n"}}' | sed -r -n '/class=""/d;s@.* data-value="([^"]*)".*class="[[:space:]]*([^"]*)".*>([[:digit:].]+)[[:space:]]*\[(.*)\]@\L\3|\4|\2@g;/^[[:digit:]]/!d;p' | while IFS="|" read -r version types distro;do
        lists=$(echo "$distro" | sed 's@ @\n@g' | awk -F- '{a[$1]++}END{for(i in a) printf("%s ",i)}' | sed -r 's@^[[:space:]]*@@g;s@[[:space:]]*$@\n@g')
        echo "${version}|${types}|${lists}" >> "${distro_lists_per_mariadb_version}"
    done

    # all support distribution by MariaDB
    # cat "${page_source}" | sed -r -n '/Choose a Release/,/Choose a Version/{/<\/(li|ul|div)>/d;s@^[[:space:]]*@@g;s@.*data-value="([^"]*)".*@\1@g;/^(<|[[:upper:]])/d;s@^$@@g;p}' | sed '/^$/d'

    sed -r -n '/Choose a Release/,/Choose a Version/{/<\/(li|ul|div)>/d;s@^[[:space:]]*@@g;s@.*data-value="([^"]*)".*@\1@g;/^(<|[[:upper:]])/d;s@^$@@g;p}' "${page_source}" | awk -F- '!match($0,/^$/){a[$1]++}END{PROCINFO["sorted_in"]="@ind_str_desc";for(i in a) print i}' | while read -r line; do
        lists=$(awk -F\| 'match($NF,/'"${line}"'/){a[$1]++}END{PROCINFO["sorted_in"]="@ind_num_desc";for(i in a) printf("%s ",i)}' "${distro_lists_per_mariadb_version}" | sed -r 's@^[[:space:]]*@@g;s@[[:space:]]*$@\n@g')

        local distro_list=${distro_list:-}

        if [[ "${line}" =~ ^rhel ]]; then
            distro_list='rhel'
        elif [[ "${line}" =~ ^centos ]]; then
            distro_list='centos'
        elif [[ "${line}" =~ ^fedora ]]; then
            distro_list='fedora'
        elif [[ "${line}" =~ ^opensuse ]]; then
            distro_list='opensuse'
        elif [[ "${line}" =~ ^(sles|sl) ]]; then
            distro_list='sles'
        else
            if [[ -n "${lists}" ]]; then
                [[ -s "${debian_codename_ralation}" ]] && distro_list=$(awk -F\| 'match($2,/^'"${line}"'$/){print $1}' "${debian_codename_ralation}")
            fi
        fi

        # The code name for Debian's development distribution is "sid", aliased to "unstable".
        # https://www.debian.org/releases/sid/
        # https://wiki.debian.org/DebianUnstable
        if [[ -z "${distro_list}" ]]; then
            [[ "${line}" == 'sid' ]] && distro_list="|debian"
        else
            distro_list="|${distro_list}"
        fi

        echo "${db_name}|${line}|${lists}${distro_list}"
        unset distro_list
    done

    [[ -f "${page_source}" ]] && rm -f "${page_source}"
    [[ -f "${distro_lists_per_mariadb_version}" ]] && rm -f "${distro_lists_per_mariadb_version}"
}


#########  2-4. Central Operation  #########
fn_CentralOperation(){
    fn_DebianUbuntuCodeRelationTable

    case "${dbname_choose,,}" in
        MySQL|mysql|my|m )
            fn_MySQLOperation
            ;;
        MariaDB|mariadb|ma )
            fn_MariaDBOperation
            ;;
        Percona|percona|p )
            fn_PerconaOperation
            ;;
        * )
            fn_MySQLOperation
            fn_MariaDBOperation
            fn_PerconaOperation
            ;;
    esac
}


#########  3. Executing Process  #########
fn_GetoptsVariableChecking
fn_InitializationCheck
fn_CentralOperation


#########  4. EXIT Singal Processing  #########
# trap "commands" EXIT # execute command when exit from shell
fn_TrapEXIT(){
    fnBase_UnsetVars
    unset dbname_choose
    unset proxy_server_specify
    unset debian_codename_ralation
}

trap fn_TrapEXIT EXIT

# Script End
