#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails

# Target: Firewall(ufw/SuSEfirewall2/firewalld/iptables) Rule Configuration On GNU/Linux
# Developer: MaxdSre

# Change Log:
# - Oct 08, 2020 09:11 Thu ET - initialization check method update
# - Oct 15, 2019 11:50 Tue ET - change custom script url
# - Oct 02, 2019 09:35 Wed ET - change to new base functions
# - Jul 11, 2019 11:27 Thu ET - add support for Arch Linux using ufw
# - Jan 01, 2019 20:40 Tue ET - code reconfiguration, include base funcions from other file
# - Jan 30, 2018 19:04 Tue +0800 - add icmp configuation, disable icmp except localhost
# - Nov 09, 2017 16:58 Thu +0800


#########  0-1. Singal Setting  #########
trap fnBase_TrapCleanUp INT QUIT

#########  0-2. Variables Setting  #########
# umask 022    # temporarily change umask value to 022
readonly custom_shellscript_url='https://gitlab.com/axdsop/nixnotes/raw/master/Script/Toolkits'
readonly os_check_script="${custom_shellscript_url}/GNULinux/gnuLinuxMachineInfoDetection.sh"

sshd_existed=${sshd_existed:-0}
list_rule=${list_rule:-0}
port_num_specified=${port_num_specified:-}
port_specified_legal=${port_specified_legal:-0}    # check if port_num_specified is legal
proto_type=${proto_type:-}
allow_inbound=${allow_inbound:-1}
deny_inbound=${deny_inbound:-0}
masquerade_enable=${masquerade_enable:-0}
delete_rule=${delete_rule:-}
host_ip_specified=${host_ip_specified:-}
restrict_ssh_login=${restrict_ssh_login:-0}
silent_output=${silent_output:-0}

pack_manager=${pack_manager:-}
firewall_type=${firewall_type:-}
readonly ssh_port_default=22
ssh_port=${ssh_port:-}
ssh_connection_ip=${ssh_connection_ip:-}


#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | sudo bash -s -- [options] ...

Installing / Configuring Firewall(ufw/SuSEfirewall2/firewalld/iptables) Rule On GNU/Linux, currently just support IPv4!
This script requires superuser privileges (eg. root, su).
Default disable ICMP except local host.

[available option]
    -h    --help, show help info
    -l    --list curent firewall rules
    -p port    --specify port number, e.g. SSH 22, Apache/Nginx 80/443
    -t proto_type    --specify port proto type (tcp|udp), default is tcp, along with -p
    -a    --allow inbound (default), along with -p
    -d    --deny inbound, along with -p
    -m    --enable masquerade mode (routed), default is disable, just support ufw,firewalld,SuSEfirewall2 currently
    -H host_ip    --specify single host ip or ip range for port specified by -p
    -R    --restrict remote login via SSH (be careful if your host is under proxy, it uses proxy ip)
    -D    --delete rule, along with -p or -H
    -s    --silent output, default output firewall configuration detail
\e[0m"
}

while getopts "hlp:t:admDH:Rs" option "$@"; do
    case "$option" in
        l ) list_rule=1 ;;
        p ) port_num_specified="$OPTARG" ;;
        t ) proto_type="$OPTARG" ;;
        a ) allow_inbound=1 ;;
        d ) deny_inbound=1 ;;
        m ) masquerade_enable=1 ;;
        D ) delete_rule=1 ;;
        H ) host_ip_specified="$OPTARG" ;;
        R ) restrict_ssh_login=1 ;;
        s ) silent_output=1 ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done



#########  1-2 Preparation Operation  #########
fn_LinuxDistroSupportCheck(){
    # - specified for RHEL/Debian/SLES/Amazon Linux
    local l_check_approved=${l_check_approved:-0}
    if [[ -f '/etc/redhat-release' || -f '/etc/debian_version' || -f '/etc/SuSE-release' || -f '/etc/arch-release' ]]; then
        l_check_approved=1
    elif [[ -f '/etc/os-release' ]]; then
        [[ $(sed -r -n '/^ID=/s@.*="?([^"]*)"?@\L\1@p' /etc/os-release) == 'amzn' ]] && l_check_approved=1
    fi

    if [[ "${l_check_approved}" -ne 1 ]]; then
        echo -e "${c_red}Sorry${c_normal}: this script just support RHEL/CentOS/Debian/Ubuntu/OpenSUSE/Arch Linux derivates!"
        exit
    fi
}

fn_InitializationCheck(){
    local l_user_codebase_dir="$HOME/.config/axdsop"
    local l_base_function_temp_save_path=''
    local l_need_remove=0
    local l_base_function_url='https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/base_functions.sh'
    l_base_function_temp_save_path=$(find "${l_user_codebase_dir}" -type f -name "${l_base_function_url##*/}" -print 2> /dev/null)
    
    if [[ ! -f "${l_base_function_temp_save_path}" ]]; then
        local l_download_tool=${l_download_tool:-'wget -qO-'}
        [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_tool='curl -fsL'
        l_need_remove=1
        l_base_function_temp_save_path=$(mktemp -t XXXXXXX)
        ${l_download_tool} "${l_base_function_url}" > "${l_base_function_temp_save_path}"
    fi

    if [[ -f "${l_base_function_temp_save_path}" && -s "${l_base_function_temp_save_path}" ]]; then
        . "${l_base_function_temp_save_path}"
        [[ "${l_need_remove}" -eq 1 ]] && rm "${l_base_function_temp_save_path}"
    else
        echo "Sorry, fail to get base script path."
        exit
    fi

    # specify 1 means need sudo privilege
    fnBase_RunningEnvironmentCheck '1'
    fnBase_CommandExistCheckPhase 'sed'
}


#########  2-1. Essential Operation  #########
# root privilege, pack manager, firewall type, ssh port, ssh host ip ...
fn_VitalInfoVerification(){
    local l_json_osinfo=${l_json_osinfo:-}
    l_json_osinfo=$(fnBase_OSInfoRetrieve "${os_check_script}")
    # [[ -z "${l_json_osinfo}" ]] && fnBase_ExitStatement "${c_red}Fatal${c_normal}: fail to extract os info!"
    firewall_type=${firewall_type:-}
    firewall_type=$(fnBase_OSInfoDirectiveValExtraction 'distro_firewall_type' "${l_json_osinfo}")

    pack_manager=${pack_manager:-}
    pack_manager=$(fnBase_OSInfoDirectiveValExtraction 'distro_pack_manager' "${l_json_osinfo}")


    # 3 - check sshd daemon if is existed
    fnBase_CommandExistIfCheck 'sshd' && sshd_existed=1

    # 4 - check if host_ip_specified is legal
    if [[ -n "${host_ip_specified}" ]]; then
        if [[ "${host_ip_specified}" =~ / ]]; then
            local l_ip_subnet=${l_ip_subnet:-}
            l_ip_subnet="${host_ip_specified##*/}"
            [[ "${l_ip_subnet}" -gt 0 && "${l_ip_subnet}" -le 32 ]] || fnBase_ExitStatement "${c_red}Fatat${c_normal}: ip ${c_yellow}${host_ip_specified}${c_normal} is illegal."
        fi
    fi

    # 5 - check if port_num_specified is legal
    port_num_specified="${port_num_specified// /}"
    if [[ "${port_num_specified}" =~ ^[1-9]{1}[0-9]{1,4}$ ]]; then
        [[ "${port_num_specified}" -lt 65536 ]] && port_specified_legal=1
    elif [[ "${port_num_specified}" =~ ^[1-9]{1}[0-9]{1,4}[,:-]{1}[1-9]{1}[0-9]{1,4}$ ]]; then
        local l_port_start=${l_port_start:-}
        local l_port_end=${l_port_end:-}

        # port range: firewalld 2000-2010, iptables/ufw/SuSEfirewall2 2000:2010
        case "${firewall_type}" in
            firewalld )
                port_num_specified="${port_num_specified//:/-}"
                if [[ "${port_num_specified}" =~ - ]]; then
                    l_port_start="${port_num_specified%%-*}"
                    l_port_end="${port_num_specified##*-}"
                fi
                ;;
            * )
                port_num_specified="${port_num_specified//-/:}"
                if [[ "${port_num_specified}" =~ : ]]; then
                    l_port_start="${port_num_specified%%:*}"
                    l_port_end="${port_num_specified##*:}"
                fi
                ;;
        esac

        if [[ -n "${l_port_start}" && -n "${l_port_end}" ]]; then
            if [[ "${l_port_start}" -lt "${l_port_end}" && "${l_port_end}" -le 65535 ]]; then
                port_specified_legal=1
            else
                fnBase_ExitStatement "${c_red}Fatat${c_normal}: port ${c_yellow}${port_num_specified}${c_normal} is illegal."
            fi
        fi

        if [[ "${port_num_specified}" =~ , ]]; then
            local l_port_check_approved=${l_port_check_approved:-}
            l_port_check_approved=$( echo "${port_num_specified}" | sed -r -n 's@,@\n@g;p' | while read -r line; do if [[ "${line}" -gt 65535 ]]; then echo '0'; break; else continue; fi; done
            )
            if [[ -n "${l_port_check_approved}" && "${l_port_check_approved}" -eq 0 ]]; then
                fnBase_ExitStatement "${c_red}Fatat${c_normal}: port ${c_yellow}${port_num_specified}${c_normal} is illegal."
            else
                port_specified_legal=1
            fi
        fi

    fi

    # 6 - proto type
    case "${proto_type,,}" in
        udp|u ) proto_type='udp' ;;
        tcp|t|* ) proto_type='tcp' ;;
    esac

    # 7 - Remote Login User Information
    # $SSH_CLIENT    # 158.65.125.120 58624 22
    # $SSH_CONNECTION    # 158.65.125.120 58624 192.168.9.8 22
    local l_ssh_connection_ip=${l_ssh_connection_ip:-}
    # local l_ssh_connecting_port=${l_ssh_connecting_port:-}

    # SSH_CLIENT, SSH_CONNECTION will be failed to read if run this script with sudo privilege
    if [[ "${sshd_existed}" -eq 1 ]]; then
        # ssh port specified in sshd config file
        ssh_port=$(sed -r -n '/^Port/s@^Port[[:space:]]*(.*)@\1@p' /etc/ssh/sshd_config  2> /dev/null)
        [[ -z "${ssh_port}" ]] && ssh_port="${ssh_port_default}"

        if [[ "${restrict_ssh_login}" -eq 1 ]]; then
            if [[ -n "${SSH_CLIENT:-}" ]]; then
                l_ssh_connection_ip="${SSH_CLIENT%% *}"
                # l_ssh_connecting_port="${SSH_CLIENT##* }"
            elif [[ -n "${SSH_CONNECTION:-}" ]]; then
                l_ssh_connection_ip="${SSH_CONNECTION%% *}"
                # l_ssh_connecting_port="${SSH_CONNECTION##* }"
            else
                l_ssh_connection_ip=$(who | sed -r -n '$s@.*\(([^\)]+)\).*@\1@gp')
                if [[ ! "${l_ssh_connection_ip}" =~ ^([0-9]{1,3}.){3}[0-9]{1,3}$ ]]; then
                    if fnBase_CommandExistIfCheck 'ping'; then
                        l_ssh_connection_ip=$(ping -c1 -n "${l_ssh_connection_ip}" 2> /dev/null | sed -r -n '1{s@^[^\(]*\(([^\)]*).*@\1@gp}')
                    elif fnBase_CommandExistIfCheck 'host'; then
                        l_ssh_connection_ip=$(host "${l_ssh_connection_ip}" 2> /dev/null | sed -r -n 's@.*address[[:space:]]*(.*)$@\1@g;p')
                    elif fnBase_CommandExistIfCheck 'dig'; then
                        l_ssh_connection_ip=$(dig +short "${l_ssh_connection_ip}" 2> /dev/null)
                    else
                        l_ssh_connection_ip=''
                    fi
                fi
                [[ "${l_ssh_connection_ip}" == ":0" ]] && l_ssh_connection_ip='127.0.0.1'
            fi

            ssh_connection_ip="${l_ssh_connection_ip}"
        elif [[ -n "${host_ip_specified}" && "${port_num_specified}" =~ ^[1-9]{1}[0-9]{1,4}$ && "${port_num_specified}" -eq "${ssh_port}" ]]; then
            ssh_connection_ip="${host_ip_specified}"
        fi

    fi    # enf if sshd
}

# Just list firewall rules
fn_ListRules(){
    if [[ "${list_rule}" -eq 1 ]]; then
        case "${firewall_type}" in
            ufw ) fnBase_CommandExistIfCheck 'ufw' && ufw status verbose 2> /dev/null ;;
            SuSEfirewall2 ) fnBase_CommandExistIfCheck 'SuSEfirewall2' && yast2 firewall services show detailed ;;
            firewalld ) fnBase_CommandExistIfCheck 'firewalld' && firewall-cmd --permanent --list-all ;;
            iptables ) fnBase_CommandExistIfCheck 'iptables' && iptables -nL --line-num ;;
        esac
        exit
    fi
}

#########  2-2. Fierwall Installation  #########
fn_FirewallInstallation(){
    case "${firewall_type}" in
        ufw )
            fnBase_CommandExistIfCheck 'ufw' || fnBase_PackageManagerOperation 'install' "ufw"
            ;;
        SuSEfirewall2 )
            fnBase_CommandExistIfCheck 'SuSEfirewall2' || fnBase_PackageManagerOperation 'install' "SuSEfirewall2"
            fnBase_PackageManagerOperation 'install' "yast2-firewall"
            ;;
        firewalld )
            fnBase_CommandExistIfCheck 'firewalld' || fnBase_PackageManagerOperation 'install' "firewalld"
            ;;
        iptables )
            fnBase_CommandExistIfCheck 'iptables' || fnBase_PackageManagerOperation 'install' "iptables iptables-services"
            ;;
    esac
}

# Block Top 10 Known-bad IPs
# $download_method https://isc.sans.edu/top10.html | sed -r -n '/ipdetails.html/{s@.*?ip=([^"]+)".*@\1@g;s@^0+@@g;s@\.0+@.@g;p}'

#########  3-1. Fierwall - Iptables  #########
fn_IptablesRuleAdd(){
    local l_type="${1:-}"    # input/output
    local l_action="${2:-}"    # accept/drop
    local l_port="${3:-}"
    local l_host="${4:-}"

    local insert_pos=${insert_pos:-}
    insert_pos=$(iptables -L INPUT -n --line-num | awk '$2="ACCEPT"&&$3=="all"&&$0~/RELATED,ESTABLISHED/{print $1;exit}')

    if [[ -n "${insert_pos}" ]]; then
        # insert
        if [[ -n "${l_host}" ]]; then
            iptables -I "${l_type}" "${insert_pos}" -s "${l_host}" -m state --state NEW -m multiport -p "${proto_type}" --dport "${l_port}" -j "${l_action}"
        else
            iptables -I "${l_type}" "${insert_pos}" -m state --state NEW -m multiport -p "${proto_type}" --dport "${l_port}" -j "${l_action}"
        fi
    else
        # append
        if [[ -n "${l_host}" ]]; then
            iptables -A "${l_type}" -s "${l_host}" -m state --state NEW -m multiport -p "${proto_type}" --dport "${l_port}" -j "${l_action}"
        else
            iptables -A "${l_type}" -m state --state NEW -m multiport -p "${proto_type}" --dport "${l_port}" -j "${l_action}"
        fi
    fi    # end if insert_pos

}

fn_IptablesRulesOperation(){
    local l_type="${1:-'input'}"    # input/output
    local l_action="${2:-'accept'}"    # accept/drop
    local l_port="${3:-}"
    local l_host="${4:-}"

    case "${l_type,,}" in
        output|o ) l_type='OUTPUT' ;;
        input|i|* ) l_type='INPUT' ;;
    esac

    case "${l_action,,}" in
        drop|d ) l_action='DROP' ;;
        accept|a|* ) l_action='ACCEPT' ;;
    esac

    if [[ -n "${l_port}" ]]; then
        local line_num_arr=()
        if [[ -n "${l_host}" ]]; then
            line_num_arr=( $(iptables -L INPUT -n --line-num | awk 'match($0,/'"${l_action}"'.*:?'"${l_port}"'[[:blank:]]*$/){if($0~/[[:space:]]+'"${l_host}"'[[:space:]]+/) print $1}' | sort -rh) )
        else
            line_num_arr=( $(iptables -L INPUT -n --line-num | awk 'match($0,/'"${l_action}"'.*:?'"${l_port}"'[[:blank:]]*$/){print $1}' | sort -rh) )
        fi

        local arr_item_count=${arr_item_count:-0}
        arr_item_count="${#line_num_arr[@]}"

        if [[ "${arr_item_count}" -eq 0 ]]; then
            [[ "${delete_rule}" -eq 1 ]] || fn_IptablesRuleAdd "${l_type}" "${l_action}" "${l_port}" "${l_host}"
        else

            for (( i = 0; i < "${arr_item_count}"; i++ )); do
                [[ "${line_num_arr[$i]}" -gt 0 ]] && iptables -D INPUT "${line_num_arr[$i]}"
            done

            if [[ "${delete_rule}" -eq 1 ]]; then
                if [[ -n "${ssh_port}" && "${ssh_port}" -eq "${port_num_specified}" ]]; then
                    local ssh_rule_count=${ssh_rule_count:-0}
                    ssh_rule_count=$(iptables -L INPUT -n --line-num | awk 'BEGIN{count=0}match($0,/:?'"${l_port}"'[[:blank:]]*$/){count++}END{print count}')

                    [[ "${ssh_rule_count}" -eq 0 ]] && fn_IptablesRuleAdd "${l_type}" "${l_action}" "${l_port}" "${l_host}"
                fi
            else
                fn_IptablesRuleAdd "${l_type}" "${l_action}" "${l_port}" "${l_host}"
            fi
        fi

    fi    # end if l_port
}

fn_Firewall_iptables(){
    # https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/security_guide/sect-security_guide-iptables
    # https://github.com/ismailtasdelen/Anti-DDOS
    local is_newly_installed=${is_newly_installed:-1}

    # start iptables servic first time will prompt "iptables: No config file." add a rule first
    if [[ -f /etc/sysconfig/iptables ]]; then
        is_newly_installed=0
    else
        # write temporarily rule to create this configuration file
        iptables -A INPUT -m state --state NEW -m multiport -p "${proto_type}" --dports 22 -j ACCEPT
        service iptables save 1> /dev/null
        fnBase_SystemServiceManagement 'iptables' 'enable'
    fi

    # iptables -nL --line-num
    # iptables -L -n -v
    # iptables -L INPUT -n --line-num

    if [[ "${is_newly_installed}" -eq 1 ]]; then
        iptables -P INPUT ACCEPT
        iptables -F    # -F Flush the selected chain
        iptables -X    # -X Delete the optional user-defined chain specified.
        iptables -Z    # -Z Zero the packet and byte counters in chains
        service iptables save 1> /dev/null

        # ALL <==> FIN,SYN,RST,PSH,ACK,URG
        # - drop invalid packet
        iptables -A INPUT -m state --state INVALID -j DROP

        # - anti syn-flood attack
        # iptables -A INPUT -p tcp ! --syn -m state --state NEW -m limit --limit 5/m --limit-burst 7 -j LOG --log-level 4 --log-prefix "Drop Syn"
        iptables -A INPUT -p tcp ! --syn -m state --state NEW -j DROP

        # - Drop fragments in all chains
        # iptables -A INPUT -f  -m limit --limit 5/m --limit-burst 7 -j LOG --log-level 4 --log-prefix "Drop Fragments"
        iptables -A INPUT -f -j DROP

        # Block Packets With Bogus TCP Flags
        # iptables -A INPUT -p tcp --tcp-flags ACK,FIN FIN -j LOG --log-level 4 --log-prefix "FIN scan: "
        iptables -A INPUT -p tcp --tcp-flags ACK,FIN FIN -j DROP
        # iptables -A INPUT -p tcp --tcp-flags ACK,PSH PSH -j LOG --log-level 4 --log-prefix "PSH scan: "
        iptables -A INPUT -p tcp --tcp-flags ACK,PSH PSH -j DROP
        # iptables -A INPUT -p tcp --tcp-flags ACK,URG URG -j LOG --log-level 4 --log-prefix "URG scan: "
        iptables -A INPUT -p tcp --tcp-flags ACK,URG URG -j DROP
        # iptables -A INPUT -p tcp --tcp-flags ALL URG,PSH,FIN -j LOG --log-level 4 --log-prefix "NMAP-XMAS-SCAN: "
        iptables -A INPUT -p tcp --tcp-flags ALL URG,PSH,FIN -j DROP
        # iptables -A INPUT -p tcp --tcp-flags ALL ALL -j LOG --log-level 4 --log-prefix "XMAS scan: "
        iptables -A INPUT -p tcp --tcp-flags ALL ALL -j DROP
        # iptables -A INPUT -p tcp --tcp-flags ALL NONE -j LOG --log-level 4 --log-prefix "NULL scan: "
        iptables -A INPUT -p tcp --tcp-flags ALL NONE -j DROP
        # iptables -A INPUT -p tcp --tcp-flags SYN,RST SYN,RST -j LOG --log-level 4 --log-prefix "SYN-RST scan: "
        iptables -A INPUT -p tcp --tcp-flags SYN,RST SYN,RST -j DROP
        # iptables -A INPUT -p tcp --tcp-flags SYN,FIN SYN,FIN -j LOG --log-level 4 --log-prefix "SYN-FIN scan: "
        iptables -A INPUT -p tcp --tcp-flags SYN,FIN SYN,FIN -j DROP
        # iptables -A INPUT -p tcp --tcp-flags FIN,RST FIN,RST -j LOG --log-level 4 --log-prefix "FIN-RST 2 scan: "
        iptables -A INPUT -p tcp --tcp-flags FIN,RST FIN,RST -j DROP
        # iptables -A INPUT -p tcp --tcp-flags ALL SYN,RST,ACK,FIN,URG -j LOG --log-level 4 --log-prefix "pscan: "
        iptables -A INPUT -p tcp --tcp-flags ALL SYN,RST,ACK,FIN,URG -j DROP
        # iptables -A INPUT -p tcp --tcp-flags ALL SYN,FIN -j LOG --log-level 4 --log-prefix "SYNFIN-SCAN: "
        iptables -A INPUT -p tcp --tcp-flags ALL SYN,FIN -j DROP
        # iptables -A INPUT -p tcp --tcp-flags ALL FIN -j LOG --log-level 4 --log-prefix "nmap FIN scan: "
        iptables -A INPUT -p tcp --tcp-flags ALL FIN -j DROP
        # iptables -A INPUT -p tcp --tcp-flags ALL URG,PSH,SYN,FIN -j LOG --log-level 4 --log-prefix "NMAP-ID: "
        iptables -A INPUT -p tcp --tcp-flags ALL URG,PSH,SYN,FIN -j DROP

        # Block Packets From Private Subnets (Spoofing)
        # - reserved ip
        # iptables -t mangle -A PREROUTING -s 169.254.0.0/16 -j DROP
        # iptables -t mangle -A PREROUTING -s 127.0.0.0/8 ! -i lo -j DROP
        # - priviate ip
        # iptables -t mangle -A PREROUTING -s 10.0.0.0/8 -j DROP
        # iptables -t mangle -A PREROUTING -s 192.168.0.0/16 -j DROP
        # iptables -t mangle -A PREROUTING -s 172.16.0.0/12 -j DROP
        # iptables -t mangle -A PREROUTING -s 0.0.0.0/8 -j DROP

        # https://javapipe.com/ddos/blog/iptables-ddos-protection/
        # Block Packets With Bogus TCP Flags

        # Allow loopback interface to do anything.
        iptables -A INPUT -i lo -j ACCEPT
        iptables -A OUTPUT -o lo -j ACCEPT

        # Just allow localhost use ping
        # iptables -A INPUT -p icmp -m icmp --icmp-type 8 -m limit --limit 1/s --limit-burst 2 -j ACCEPT
        iptables -A INPUT -p icmp -m icmp --icmp-type 8 -s 127.0.0.1 -d 0/0 -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT
        iptables -A INPUT -p icmp --icmp-type 0 -s 0/0 -d 127.0.0.1 -m state --state ESTABLISHED,RELATED -j ACCEPT

        ## open http/https server port to all ##
        # iptables -A INPUT -m state --state NEW -p tcp -m multiport --dports 80,443 -j ACCEPT
        if [[ "${sshd_existed}" -eq 1 && -n "${ssh_port}" ]]; then
            if [[ -n "${ssh_connection_ip}" ]]; then
                fn_IptablesRulesOperation 'input' 'accept' "${ssh_port}" "${ssh_connection_ip}"
            else
                fn_IptablesRulesOperation 'input' 'accept' "${ssh_port}"
            fi
        fi
    fi    # end if is_newly_installed

    if [[ "${port_specified_legal}" -eq 1 ]]; then
        if [[ -n "${host_ip_specified}" ]]; then
            [[ "${allow_inbound}" -eq 1 ]] && fn_IptablesRulesOperation 'input' 'accept' "${port_num_specified}" "${host_ip_specified}"
            [[ "${deny_inbound}" -eq 1 ]] && fn_IptablesRulesOperation 'input' 'drop' "${port_num_specified}" "${host_ip_specified}"
        else
            [[ "${allow_inbound}" -eq 1 ]] && fn_IptablesRulesOperation 'input' 'accept' "${port_num_specified}"
            [[ "${deny_inbound}" -eq 1 ]] && fn_IptablesRulesOperation 'input' 'drop' "${port_num_specified}"
        fi
    fi

    if [[ "${is_newly_installed}" -eq 1 ]]; then
        # Allow incoming connections related to existing allowed connections.
        iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
        # Allow outgoing connections EXCEPT invalid
        iptables -A OUTPUT -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT
        iptables -P INPUT DROP
    fi

    # iptables -A INPUT -p tcp --dport 22 -j LOG --log-prefix "Someone knocked on port 22"

    service iptables save 1> /dev/null
    fnBase_SystemServiceManagement 'iptables' 'restart'

    [[ "${silent_output}" -eq 1 ]] || iptables -nL --line-num
}

#########  3-2. Fierwall - firewalld  #########
fn_Firewall_firewalld(){
    # https://www.certdepot.net/rhel7-get-started-firewalld/
    # https://www.digitalocean.com/community/tutorials/how-to-set-up-a-firewall-using-firewalld-on-centos-7

    # firewall-cmd --get-default-zone
    # firewall-cmd --zone=public --permanent --list-ports
    # firewall-cmd --zone=public --permanent --list-rich-rule
    # firewall-cmd --zone=public --list-services --permanent

    # firewall-cmd --permanent --zone=public --add-rich-rule='rule family="ipv4" source address="192.168.100.26" destination address=192.168.0.10/32 port port=8080-8090 protocol=tcp accept'

    # $(systemctl is-active firewalld) == 'inactive'
    [[ $(firewall-cmd --state 2>&1) == 'running' ]] || fnBase_SystemServiceManagement 'firewalld' 'enable'
    # set default zone
    local l_default_zone=${l_default_zone:='public'}
    [[ $(firewall-cmd --get-default-zone) == 'public' ]] || firewall-cmd --set-default-zone="${l_default_zone}" &> /dev/null

    # If zone is omitted, default zone will be used.
    # --add-masquerade / --remove-masquerade / --query-masquerade
    if [[ "${masquerade_enable}" -eq 1 && $(firewall-cmd --query-masquerade 2>&1) != 'yes' ]]; then
        firewall-cmd --add-masquerade &> /dev/null
        firewall-cmd --permanent --add-masquerade &> /dev/null
    fi

    # lockdown Rules
    # /etc/firewalld/firewalld.conf  Lockdown=yes
    # firewall-cmd --query-lockdown
    # firewall-cmd --lockdown-on/--lockdown-off
    [[ $(firewall-cmd --query-lockdown) == 'no' ]] && firewall-cmd --lockdown-on &> /dev/null

    # disable ping
    # firewall-cmd --add-icmp-block=echo-request --permanent &> /dev/null
    # firewall-cmd --get-icmptypes
    # firewall-cmd --list-icmp-blocks
    # firewall-cmd --zone=public --add-icmp-block=echo-reply
    # firewall-cmd --zone=public --query-icmp-block=echo-reply
    # https://serverfault.com/questions/677084/block-icmp-timestamp-timestamp-reply-with-firewalld
    firewall-cmd --zone="${l_default_zone}" --remove-icmp-block={echo-request,echo-reply,timestamp-reply,timestamp-request} --permanent &> /dev/null

    firewall-cmd --zone="${l_default_zone}" --add-icmp-block={echo-request,echo-reply,timestamp-reply,timestamp-request} --permanent &> /dev/null

    # firewall-cmd --zone=public --permanent --add-service=ssh &> /dev/null
    firewall-cmd --zone="${l_default_zone}" --permanent --remove-service=ssh &> /dev/null

    # for ssh remote login
    if [[ "${sshd_existed}" -eq 1 && -n "${ssh_port}" ]]; then
        if [[ -n "${ssh_connection_ip}" ]]; then
            [[ "${restrict_ssh_login}" -eq 1 ]] && firewall-cmd --zone="${l_default_zone}" --permanent --add-rich-rule='rule family="ipv4" source address="'"${ssh_connection_ip}"'" port port='"${ssh_port}"' protocol="'${proto_type}'" accept' &> /dev/null
        else
            firewall-cmd --zone="${l_default_zone}" --permanent --add-port="${ssh_port}/${proto_type}" &> /dev/null
        fi
    fi


    # accept|reject|drop|mark
    local l_action=${l_action:-'accept'}
    [[ "${allow_inbound}" -eq 1 ]] && l_action='accept'
    [[ "${deny_inbound}" -eq 1 ]] && l_action='drop'

    if [[ "${delete_rule}" -eq 1 ]]; then
        # - delete via ip
        if [[ -n "${host_ip_specified}" ]]; then
            [[ "${host_ip_specified}" =~ / ]] && host_ip_specified="${host_ip_specified//\//\\/}"

            firewall-cmd --zone="${l_default_zone}" --permanent --list-rich-rule | awk 'match($0,/source address="'"${host_ip_specified}"'"/){ipinfo=gensub(/.*address="([^"]+)".*port="([^"]+)".*/,"\\1|\\2","g",$0);print ipinfo}' | while IFS="|" read -r ip_num port_num; do
                firewall-cmd --zone="${l_default_zone}" --permanent --remove-rich-rule='rule family="ipv4" source address="'"${ip_num}"'" port port='"${port_num}"' protocol="'${proto_type}'" '"${l_action}"'' 1> /dev/null
            done
        fi

        # - delete via port no.
        if [[ "${port_specified_legal}" -eq 1 ]]; then
            firewall-cmd --zone="${l_default_zone}" --permanent --list-rich-rule | awk 'match($0,/port="'"${port_num_specified}"'"/){ipinfo=gensub(/.*address="([^"]+)".*/,"\\1","g",$0);print ipinfo}' | while read -r line; do
                firewall-cmd --zone="${l_default_zone}" --permanent --remove-rich-rule='rule family="ipv4" source address="'"${line}"'" port port='"${port_num_specified}"' protocol="'${proto_type}'" '"${l_action}"'' &> /dev/null
            done

            [[ "${port_num_specified}" =~ ^[1-9]{1}[0-9]{1,4}$ ]] && firewall-cmd --zone="${l_default_zone}" --permanent --remove-port="${port_num_specified}/${proto_type}"  &> /dev/null
        fi

        # must keep allow at least ssh remote connection
        if [[ -n "${ssh_port}" ]]; then
            if [[ -n "${ssh_connection_ip}" ]]; then
                firewall-cmd --zone="${l_default_zone}" --permanent --add-rich-rule='rule family="ipv4" source address="'"${ssh_connection_ip}"'" port port='"${port_num_specified}"' protocol="'${proto_type}'" '"${l_action}"'' &> /dev/null
            else
                firewall-cmd --zone="${l_default_zone}" --permanent --add-port="${port_num_specified}/tcp" &> /dev/null
            fi
        fi

    else
        if [[ -n "${host_ip_specified}" ]]; then
            firewall-cmd --zone="${l_default_zone}" --permanent --add-rich-rule='rule family="ipv4" source address="'"${host_ip_specified}"'" port port='"${port_num_specified}"' protocol="'${proto_type}'" '"${l_action}"'' &> /dev/null
        else
            firewall-cmd --zone="${l_default_zone}" --permanent --add-port="${port_num_specified}/${proto_type}"  &> /dev/null
        fi
    fi

    firewall-cmd --reload 1> /dev/null

    [[ "${silent_output}" -eq 1 ]] || firewall-cmd --permanent --list-all
}

#########  3-3. Fierwall - ufw  #########
fn_Firewall_ufw(){
    # https://help.ubuntu.com/community/UFW
    # https://www.digitalocean.com/community/tutorials/ufw-essentials-common-firewall-rules-and-commands
    # https://www.digitalocean.com/community/tutorials/how-to-set-up-a-firewall-with-ufw-on-ubuntu-16-04
    # ufw status numbered
    # ufw allow ssh / ufw delete allow ssh
    # ufw allow 6660:6670/tcp

    # ufw [delete] allow/deny from 192.168.100.0/24 1> /dev/null
    # ufw [delete] deny/allow in on eth0 [from 192.168.100.0/24] to any port 80 1> /dev/null
    # ufw [delete] allow/limit from 192.168.100.106/32 to any port 22 proto tcp 1> /dev/null
    # ufw [delete] allow 80,443/tcp 1> /dev/null   #ufw allow http/https, ufw deny 80/tcp
    # ufw [delete] allow proto tcp from any to any port 80,443 1> /dev/null
    # ufw allow from <target> to <destination> port <port number>

    # Disable ipv6
    [[ -f /etc/default/ufw ]] && sed -r -i '/^IPV6=/{s@(IPV6=).*@\1no@g;}' /etc/default/ufw 2> /dev/null
    fnBase_SystemServiceManagement 'ufw' 'enable'

    # https://serverfault.com/questions/790143/ufw-enable-requires-y-prompt-how-to-automate-with-bash-script
    # man ufw | sed -r -n '/REMOTE MANAGEMENT/,/APPLICATION INTEGRATION/p'
    # When running `ufw enable` or starting ufw via its initscript, ufw will flush its chains. This is required so ufw can maintain a consistent state, but it may drop existing connections (eg ssh).
    # This can be disabled by using 'ufw --force enable'.
    # Command may disrupt existing ssh connections. Proceed with operation (y|n)?

    if [[ "${sshd_existed}" -eq 1 && $(ufw status 2> /dev/null | sed -r -n '/^Status:/{s@.*:[[:space:]]*(.*)@\1@g;p}') == 'inactive' ]]; then
        # this rule will be flushed, but the ssh port still be open after enabling the firewall
        local l_restrict_host=${l_restrict_host:-'any'}
        [[ -n "${ssh_connection_ip}" ]] && l_restrict_host="${ssh_connection_ip}"
        ufw allow proto "${proto_type}" from "${l_restrict_host}" to any port 22 1> /dev/null
        ufw allow proto "${proto_type}" from "${l_restrict_host}" to any port "${ssh_port}" 1> /dev/null
        ufw --force enable 1> /dev/null
    fi

    # # Just allow localhost use ping
    local before_rule_path=${before_rule_path:-'/etc/ufw/before.rules'}
    if [[ -f "${before_rule_path}" && $(sed -r -n '/ufw-before-input.*echo-request -j ACCEPT/{p}' "${before_rule_path}" | wc -l) -gt 0 ]]; then
        # input
        sed -r -i '/ufw-before-input.*echo-request -j ACCEPT/{s@(.* -j).*$@\1 DROP@g;}' "${before_rule_path}" 2> /dev/null
        sed -r -i '/ufw-before-input.*echo-request -j/i -A ufw-before-input -p icmp --icmp-type echo-request -s 127.0.0.1 -m state --state ESTABLISHED -j ACCEPT' "${before_rule_path}" 2> /dev/null

        # output
        sed -r -i '/ok icmp code for FORWARD/i # ok icmp codes for OUTPUT\n-A ufw-before-output -p icmp -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT\n-A ufw-before-output -p icmp -m state --state ESTABLISHED,RELATED -j ACCEPT\n' "${before_rule_path}" 2> /dev/null
    fi

    local user_rule_path=${user_rule_path:-'/etc/ufw/user.rules'}
    [[ -s /lib/ufw/user.rules ]] && user_rule_path='/lib/ufw/user.rules'

    # - no existed rules, initialization
    if [[ $(sed -r -n '/^#+[[:space:]]*RULES/,/^#+[[:space:]]*END RULES/{/^#/d;/^$/d;p}' "${user_rule_path}" | wc -l) -eq 0 ]]; then
        echo 'y' | ufw reset 1> /dev/null

        # default allow|deny|reject DIRECTION
        # DIRECTION: incoming, outgoing, routed
        # /etc/default/ufw
        # DEFAULT_INPUT_POLICY <--> incoming
        # DEFAULT_OUTPUT_POLICY <--> outgoing
        # DEFAULT_FORWARD_POLICY <--> routed

        # setting default rule (deny incoming, allow outgoing)
        ufw default deny incoming 1> /dev/null
        ufw default allow outgoing 1> /dev/null
        if [[ "${masquerade_enable}" -eq 1 ]]; then
            ufw default allow routed 1> /dev/null
        fi
        ufw logging on 1> /dev/null

        # for ssh remote login
        ufw delete limit ssh &> /dev/null
        ufw delete allow ssh &> /dev/null

        if [[ "${sshd_existed}" -eq 1 && -n "${ssh_port}" ]]; then
            if [[ -n "${ssh_connection_ip}" ]]; then
                # ufw limit
                ufw allow from "${ssh_connection_ip}" to any port "${ssh_port}" proto "${proto_type}" 1> /dev/null
            else
                ufw allow "${ssh_port}/${proto_type}" 1> /dev/null
            fi
        fi
    fi

    # - add / delete rule
    local l_action=${l_action:-'allow'}
    [[ "${allow_inbound}" -eq 1 ]] && l_action='allow'
    [[ "${deny_inbound}" -eq 1 ]] && l_action='deny'

    # delete rule
    if [[ "${delete_rule}" -eq 1 ]]; then
        # - delete via ip
        if [[ -n "${host_ip_specified}" ]]; then
            sed -r -i '/^#+[[:space:]]*RULES/,/^#+[[:space:]]*END RULES/{/^#+[[:space:]]*tuple.*'"${l_action}"'[[:space:]]*(tcp|udp|any)[[:space:]]*.*'"${host_ip_specified}"'[[:space:]]*in[[:space:]]*/,/^$/{d}}' "${user_rule_path}" 2> /dev/null

        fi
        # - delte via port
        if [[ "${port_specified_legal}" -eq 1 ]]; then
            sed -r -i '/^#+[[:space:]]*RULES/,/^#+[[:space:]]*END RULES/{/^#+[[:space:]]*tuple.*'"${l_action}"'[[:space:]]*(tcp|udp|any)[[:space:]]*'"${port_num_specified}"'[[:space:]]+/,/^$/{d}}' "${user_rule_path}" 2> /dev/null

        fi    # end if host_ip_specified

        # must keep allow at least ssh remote connection
        if [[ -n "${ssh_port}" && $(ufw status 2> /dev/null | sed -r -n 's@.*@\L&@g;p' | sed -r -n '/^'"${ssh_port}"'\/tcp.*allow/{p}' | wc -l) -eq 0 ]]; then
            if [[ -n "${ssh_connection_ip}" ]]; then
                ufw allow from "${ssh_connection_ip}" to any port "${ssh_port}" proto tcp &> /dev/null
            else
                ufw allow "${ssh_port}/tcp" &> /dev/null
            fi
        fi
    # add rule
    else
        if [[ -n "${host_ip_specified}" ]]; then
            ufw ${l_action} from "${host_ip_specified}" to any port "${port_num_specified}" proto "${proto_type}" &> /dev/null
        else
            ufw ${l_action} "${port_num_specified}/${proto_type}" &> /dev/null
        fi
    fi

    ufw reload 1> /dev/null
    [[ "${sshd_existed}" -eq 1 ]] || ufw --force enable 1> /dev/null
    [[ "${silent_output}" -eq 1 ]] || ufw status verbose 2> /dev/null
}

#########  3-4. Fierwall - SuSEfirewall2  #########
fn_Yast2FirewallOperation(){
    local l_port="${1:-}"
    local l_proto=${2:-"${proto_type}"}
    local l_host="${3:-}"
    local l_config_path=${l_config_path:-'/etc/sysconfig/SuSEfirewall2'}

    local l_pattern_info=${l_pattern_info:-''}

    if [[ -n "${l_host}" ]]; then
        l_pattern_info="${l_host},${l_proto},${l_port}"
    else
        l_pattern_info="${l_proto},${l_port}"
        yast2 firewall services remove zone=EXT tcpport="${l_port}" 1> /dev/null
    fi

    local l_service_left=${l_service_left:-}
    l_service_left=$(sed -r -n '/^FW_SERVICES_ACCEPT_EXT=/{s@.*="([^"]*)".*@\1@g;s@[[:space:]]+@\n@g;p}' "${l_config_path}" | sed '/'"${l_pattern_info}"'$/d;/^$/d' | awk '!arr[$0]++' | sed ':a;N;$!ba;s@\n@ @g')

    sed -r -i '/^FW_SERVICES_ACCEPT_EXT=/{s@(.*=")[^"]*(")@\1'" ${l_service_left}"'\2@g;s@[[:space:]]*(")[[:space:]]*@\1@g}' "${l_config_path}"

    # add new
    local add_rule_flag=${add_rule_flag:-0}
    if [[ "${delete_rule}" -ne 1 ]]; then
        add_rule_flag=1
    else
        if [[ -n "${ssh_port}" && "${l_port}" -eq "${ssh_port}" ]]; then
            add_rule_flag=1
        fi
    fi    # end if delete_rule

    if [[ "${add_rule_flag}" -eq 1 ]]; then
        if [[ -n "${l_host}" ]]; then
            sed -r -i '/^FW_SERVICES_ACCEPT_EXT=/{s@^(.*="[^"]*)(")$@\1'" ${l_pattern_info}"'\2@g;s@[[:space:]]*(")[[:space:]]*@\1@g}' "${l_config_path}"
        else
            yast2 firewall services add zone=EXT tcpport="${l_port}" 1> /dev/null
        fi
    fi
}

fn_Firewall_SuSEfirewall2(){
    # https://knowledgelayer.softlayer.com/procedure/configure-software-firewall-sles
    # https://release-8-16.about.gitlab.com/downloads/#opensuse421

    # INT - Internal Zone  |  DMZ - Demilitarized Zone  |  EXT - External Zone
    # /etc/sysconfig/SuSEfirewall2

    # yast2 firewall summary
    # yast2 firewall services show detailed
    # yast2 firewall interfaces/logging/startup show

    # yast2 firewall interfaces add interface=`ip a s dev eth0 | awk '/ether/{printf "eth-id-%s", $2}'` zone=INT
    # yast2 firewall interfaces add interface=`ip a s dev eth1 | awk '/ether/{printf "eth-id-%s", $2}'` zone=EXT

    # list in $(yast2 firewall services list)
    # FW_CONFIGURATIONS_EXT
    # yast2 firewall services add zone=EXT service=service:sshd

    # FW_SERVICES_EXT_TCP / FW_SERVICES_EXT_UDP
    # yast2 firewall services add zone=EXT tcpport=22
    # yast2 firewall services add zone=EXT udpport=53
    # yast2 firewall services add tcpport=80,443,22,25,465,587 udpport=80,443,22,25,465,587 zone=EXT

    # FW_SERVICES_ACCEPT_EXT
    #Custome Rule, space separated list of <source network>[,<protocol>,<destination port>,<source port>,<options>]
    # FW_SERVICES_ACCEPT_EXT="116.228.89.242,tcp,777 192.168.92.123,tcp,567,789 192.168.45.145,tcp,,85"

    # yast2 firewall startup atboot/manual
    # yast2 firewall startup manual

    # rcSuSEfirewall2 status/start/stop/restart

    local susefirewall2=${susefirewall2:-'/etc/sysconfig/SuSEfirewall2'}
    [[ -f "${susefirewall2}${bak_suffix}" ]] || cp -fp "${susefirewall2}" "${susefirewall2}${bak_suffix}"

    if [[ -n "${ssh_port}" && "${ssh_port}" -ne "${port_num_specified}" ]]; then
        if [[ -n "${ssh_connection_ip}" ]]; then
            fn_Yast2FirewallOperation "${ssh_port}" "${proto_type}" "${ssh_connection_ip}"
        else
            fn_Yast2FirewallOperation "${ssh_port}" "${proto_type}"
        fi
    fi

    if [[ "${port_specified_legal}" -eq 1 ]]; then
        if [[ -n "${host_ip_specified}" ]]; then
            fn_Yast2FirewallOperation "${port_num_specified}" "${proto_type}" "${host_ip_specified}"
        else
            fn_Yast2FirewallOperation "${port_num_specified}" "${proto_type}"
        fi
    fi

    if [[ "${masquerade_enable}" -eq 1 ]]; then
        # show/enable/disable/help/verbose
        yast2 firewall masquerade enable 2> /dev/null

        # /etc/sysconfig/SuSEfirewall2
        # FW_ROUTE="yes"    FW_MASQUERADE="yes"
    fi

    yast2 firewall startup atboot 2> /dev/null
    yast2 firewall enable 1> /dev/null
    rcSuSEfirewall2 start 1> /dev/null

    if [[ "${silent_output}" -ne 1 ]]; then
        yast2 firewall services show
        sed -r -n '/^FW_SERVICES_ACCEPT_EXT=/p' /etc/sysconfig/SuSEfirewall2
    fi
}

#########  4. Executing Process  #########
fn_LinuxDistroSupportCheck
fn_InitializationCheck

fn_VitalInfoVerification
fn_ListRules
fn_FirewallInstallation
fn_Firewall_"${firewall_type}"


#########  4. EXIT Singal Processing  #########
fn_TrapEXIT(){
    fnBase_UnsetVars
    unset sshd_existed
    unset list_rule
    unset port_num_specified
    unset proto_type
    unset allow_inbound
    unset deny_inbound
    unset masquerade_enable
    unset delete_rule
    unset host_ip_specified
    unset restrict_ssh_login
    unset silent_output
    unset pack_manager
    unset codename
    unset ssh_port
    unset firewall_type
}

trap fn_TrapEXIT EXIT

# Script End
