#!/usr/bin/env bash
# shellcheck disable=SC2016,SC2003,SC1001
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails

#Docker Script https://get.docker.com/
#Gitlab Script https://packages.gitlab.com/gitlab/gitlab-ce/install

# Harden Security
# https://wiki.mozilla.org/Security
# https://www.cyberciti.biz/tips/linux-security.html

# Benchmark
# https://github.com/haydenjames/bench-scripts

# Security auditing - Lynis
# https://cisofy.com/lynis/
# https://github.com/CISOfy/Lynis

# Common administrative commands in Red Hat Enterprise Linux 5, 6, and 7
# https://access.redhat.com/articles/1189123

# Red Hat Enterprise Linux technology capabilities and limits
# https://access.redhat.com/articles/rhel-limits


# Target: Post Initialization Setting On Freshly Installed GNU/Linux (RHEL/CentOS/Fedora/Debian/Ubuntu/OpenSUSE and variants)
# Developer: MaxdSre

# Change Log:
# - Oct 08, 2020 09:11 Thu ET - initialization check method update
# - Oct 15, 2019 11:35 Tue ET - change custom script url
# - Oct 02, 2019 15:45 Wed ET - change to new base functions
# - May 08, 2019 08:45 Wed ET - add disable sudo hint in /etc/bash.bashrc which create file $HOME/.sudo_as_admin_successful
# - Jan 01, 2019 21:44 Tue ET - code reconfiguration, include base funcions from other file
# - Dec 26, 2018 21:24 Wed ET - Add Docker container install operation
# - Jul 11, 2018 17:04 Wed ET - AWS EC2 ubuntu remove .gz file in /usr/share/man/ by default which makes `man ssh_config` not work
# - Jul 02, 2018 09:51 Mon ET - Optimization, add TCP forward control
# - Feb 27, 2018 13:55 Fri ET - solve cracklib_dict.pwd error while change user passwd
# - Feb 22, 2018 16:33 Thu ET - Optimization, add MegaCli, Fail2Ban
# - Feb 19, 2018 16:22 Mon ET - Optimization, remove bc, replace expr with bc
# - Feb 02, 2018 15:47 Fri +0800 - Optimization, security enhancement, add USB block, umask determining...
# - Feb 01, 2018 18:04 Thu +0800 - Optimization, security enhancement, add cron task for rkhunter, aide, clamav
# - Jan 28, 2018 12:39 Sun +0800 - THP optimization, ~/.bashrc add alias fro system update, older kernel remove
# - Jan 24, 2018 19:28 Wed +0800 - ssh directive configuation optimization
# - Jan 23, 2018 16:43 Tue +0800 - Add timezone autodetection, disable 'sudo su -', add readonly-removables rule
# - Jan 19, 2018 18:45 Fri +0800 - Auditd tunning, add custom rules
# - Jan 18, 2018 18:21 Thu +0800 - Auditd tunning, add compress command
# - Jan 17, 2018 15:51 Wed +0800 - SELinux tunning under SUSE/OpenSUSE
# - Jan 16, 2018 16:02 Tue +0800 - Optimization, add tripwire, nikto
# - Jan 08, 2018 16:09 Mon +0800 - sysstat optimization, add SELinux
# - Jan 05, 2018 18:59 Fri +0800 - Optimization, add cron task
# - Jan 02, 2018 14:51 Tue +0800 - Optimization, add security/audit utilities
# - Dec 25, 2017 13:47 Mon +0800 - Change output style
# - Dec 05, 2017 11:05 Fri +0800
# - Nov 24, 2017 17:36 Fri +0800
# - Nov 14, 2017 15:30 Tue +0800
# - Nov 09, 2017 18:23 Thu +0800
# - Oct 26, 2017 19:10 Thu +0800
# - Oct 17, 2017 11:50 Tue +0800
# - Sep 05, 2017 13:49 Tue +0800
# - Aug 16, 2017 18:25 Wed +0800
# - July 27, 2017 17:26 Thu +0800
# - July 11, 2017 13:12 Tue ~ July 12, 2017 16:33 Wed +0800


#########  0-1. Singal Setting  #########
trap fnBase_TrapCleanUp INT QUIT

#########  0-2. Variables Setting  #########
# umask 022    # temporarily change umask value to 022
pass_change_minday=${pass_change_minday:-0}    # minimum days need for a password change
pass_change_maxday=${pass_change_maxday:-90}   # maximum days the password is valid
pass_change_warnningday=${pass_change_warnningday:-7}  # password expiry advanced warning days
readonly umask_default='027'
readonly custom_shellscript_url='https://gitlab.com/axdsop/nixnotes/raw/master/Script/Toolkits'
readonly vim_url="${custom_shellscript_url}/_Configs/vimrc"
readonly sysctl_url="${custom_shellscript_url}/_Configs/sysctl.conf"
readonly os_check_script="${custom_shellscript_url}/GNULinux/gnuLinuxMachineInfoDetection.sh"
readonly docker_script="${custom_shellscript_url}/Application/Container/Docker-CE.sh"
readonly port_generation_script="${custom_shellscript_url}/Network/random_available_port_generate.sh"
readonly firewall_configuration_script="${custom_shellscript_url}/GNULinux/gnuLinuxFirewallRuleConfiguration.sh"
readonly auditd_custom_rule="${custom_shellscript_url}/_Configs/Application/auditd/custom.rules"
readonly ssh_custom_config="${custom_shellscript_url}/_Configs/ssh_config"
readonly bashrc_custom="${custom_shellscript_url}/_Configs/bashrc"


enforcing_selinux=${enforcing_selinux:-0}
readonly default_timezone='Asia/Singapore'
readonly default_grub_timeout=3
disable_ssh_root=${disable_ssh_root:-0}
readonly ssh_port_default=22
ssh_port=${ssh_port:-"${ssh_port_default}"}
automatic_update=${automatic_update:-0}
email_address=${email_address:-}
repository_change=${repository_change:-0}
container_install=${container_install:-0}
ssh_keygen_only=${ssh_keygen_only:-0}
restrict_remote_login=${restrict_remote_login:-0}
forward_disable=${forward_disable:-0}
grub_timeout=${grub_timeout:-2}
hostname_specify=${hostname_specify:-}
username_specify=${username_specify:-}
timezone_specify=${timezone_specify:-}
ssh_port_specify=${ssh_port_specify:-}
install_ssh_server=${install_ssh_server:-1}
tmpfs_enable=${tmpfs_enable:-0}
sudo_mode_enable=${sudo_mode_enable:-0}
log_user_session=${log_user_session:-0}
administrator_utility=${administrator_utility:-0}
security_enhance=${security_enhance:-0}
unwanted_utility_service_keep=${unwanted_utility_service_keep:-0}
cron_task=${cron_task:-0}
kernel_upgrade=${kernel_upgrade:-0}
selinux_setting=${selinux_setting:-0}
boost_enable=${boost_enable:-0}
proxy_server_specify=${proxy_server_specify:-}
current_year=${current_year:-"$(date +'%Y')"}


#########  1-1 getopts Operation  #########
funcHelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | sudo bash -s -- [options] ...

Post Installation Configuring RHEL/CentOS/Fedora/Amazon Linux/Debian/Ubuntu/SLES/OpenSUSE!
This script requires superuser privileges (eg. root, su).

[available option]
    -h    --help, show help info
    -a    --enable automatic update, default is disable
    -e email_address    --specify Email for alerting
    -R    --repository source configuration
    -C    --instll Docker container, default is not install
    -u username    --add user, create new normal user, password is 'Username@year', e.g. user maxdsre, this year is ${current_year}, then password is 'Maxdsre@${current_year}'
    -S    --sudo, grant user sudo privilege which is specified by '-u'
    -H hostname    --hostname, set static hostname
    -t timezone    --timezone, set timezone (eg. America/New_York, Asia/Hong_Kong), default detect timezone via public ip
    -P port    --specify ssh port num or port type (d/r/n/e/h), 'd' is default port 22, 'r' is system ports (0,1023], 'n' is user ports [1024,32767], 'e' is regular ephemeral port [32768,49151], 'h' is high ephemeral port [49152,65535]. If '-P' is specified, script will install sshd service (server side)
    -d    --disable root user remoting login (eg: via ssh)
    -k    --keygen, sshd service only allow ssh keygen, disable password
    -r    --restrict remote login from specific ip (current login host), use with caution
    -f    --forward disable (TCP), default is enable
    -g time    --grub timeout, set timeout num (second), default is 3s, the max is 6s
    -T    --enable tmpfs filesystem for dir '/tmp' provided by systemd, only if physical memory size > 7GB, default allocate 1GB, (16,32) 2GB, [32,64) 3GB, [64,128) 4GB, >=128 8GB
    -l    --enable login user session log via built command 'script'
    -A    --install system administration utility, e.g. iproute, procps, dstat, dnsutils ...
    -E    --install security and audit utilities
    -s    --Keep unwanted utilities (default is remove) and service (default is disable)
    -c    --add cron task, e.g. system update, anti-virus sacn, intrusion detection scan
    -K    --kernel upgrade, install latest mailline kernel version
    -Z selinux_type    --SELinux configuation(0/permissive, 1/enforcing, 2/disabled), default is 0/Permissive, not suggest deploy on Debian/Ubuntu, it may cause problems
    -b    --enable boost tool 'axdsop' stores in ~/.bashrc
    -p [protocol:]ip:port    --proxy host (http|https|socks4|socks5), default protocol is http
\e[0m"
}

while getopts "hRCae:u:SH:t:P:dkrfg:TlAEscKZ:bp:" option "$@"; do
    case "$option" in
        R ) repository_change=1 ;;
        C ) container_install=1 ;;
        a ) automatic_update=1 ;;
        e ) email_address="$OPTARG" ;;
        u ) username_specify="$OPTARG" ;;
        S ) sudo_mode_enable=1 ;;
        H ) hostname_specify="$OPTARG" ;;
        t ) timezone_specify="$OPTARG" ;;
        P ) ssh_port_specify="$OPTARG" ;;
        d ) disable_ssh_root=1 ;;
        k ) ssh_keygen_only=1 ;;
        r ) restrict_remote_login=1 ;;
        f ) forward_disable=1 ;;
        g ) grub_timeout="$OPTARG" ;;
        T ) tmpfs_enable=1 ;;
        l ) log_user_session=1 ;;
        A ) administrator_utility=1 ;;
        E ) security_enhance=1 ;;
        s ) unwanted_utility_service_keep=1 ;;
        c ) cron_task=1 ;;
        K ) kernel_upgrade=1 ;;
        Z ) selinux_setting="$OPTARG" ;;
        b ) boost_enable=1 ;;
        p ) proxy_server_specify="$OPTARG" ;;
        h|\? ) funcHelpInfo && exit;;
    esac
done


#########  1-2 Preparation Operation  #########
fn_InitializationCheck(){
    local l_user_codebase_dir="$HOME/.config/axdsop"
    local l_base_function_temp_save_path=''
    local l_need_remove=0
    local l_base_function_url='https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/base_functions.sh'
    l_base_function_temp_save_path=$(find "${l_user_codebase_dir}" -type f -name "${l_base_function_url##*/}" -print 2> /dev/null)
    
    if [[ ! -f "${l_base_function_temp_save_path}" ]]; then
        local l_download_tool=${l_download_tool:-'wget -qO-'}
        [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_tool='curl -fsL'
        l_need_remove=1
        l_base_function_temp_save_path=$(mktemp -t XXXXXXX)
        ${l_download_tool} "${l_base_function_url}" > "${l_base_function_temp_save_path}"
    fi

    if [[ -f "${l_base_function_temp_save_path}" && -s "${l_base_function_temp_save_path}" ]]; then
        . "${l_base_function_temp_save_path}"
        [[ "${l_need_remove}" -eq 1 ]] && rm "${l_base_function_temp_save_path}"
    else
        echo "Sorry, fail to get base script path."
        exit
    fi

    tput clear  # Echo the clear-screen sequence for the current terminal.
    fnBase_RunningEnvironmentCheck '0'

    # fnBase_CommandExistCheckPhase 'gawk'

    # - total physical memory size
    mem_totoal_size=${mem_totoal_size:-0}
    if [[ -s '/proc/meminfo' ]]; then
        mem_totoal_size=$(sed -r -n '/MemTotal/{s@^[^[:digit:]]*([[:digit:]]+).*$@\1@g;p}' /proc/meminfo)
    elif fnBase_CommandExistIfCheck 'free'; then
        mem_totoal_size=$(free -k | sed -r -n '/^Mem:/{s@^[^[:digit:]]*([[:digit:]]+).*@\1@g;p}')
    fi
}


fn_OSInfoDetection(){
    local l_json_osinfo=${l_json_osinfo:-}
    l_json_osinfo=$(fnBase_OSInfoRetrieve "${os_check_script}")
    # [[ -z "${l_json_osinfo}" ]] && funcExitStatement "${c_red}Fatal${c_normal}: fail to extract os info!"

    distro_fullname=${distro_fullname:-}
    distro_fullname=$(fnBase_OSInfoDirectiveValExtraction 'distro_fullname' "${l_json_osinfo}")

    local distro_is_eol=${distro_is_eol:-0}
    distro_is_eol=$(fnBase_OSInfoDirectiveValExtraction 'distro_is_eol' "${l_json_osinfo}")
    [[ "${distro_is_eol}" -eq 1 ]] && funcExitStatement "${c_red}Sorry${c_normal}: your system ${c_blue}${distro_fullname}${c_normal} is obsoleted!"

    distro_name=${distro_name:-}
    distro_name=$(fnBase_OSInfoDirectiveValExtraction 'distro_name' "${l_json_osinfo}")
    distro_name="${distro_name%%-*}"    # rhel/centos/fedora/debian/ubuntu/sles/opensuse

    codename=${codename:-}
    codename=$(fnBase_OSInfoDirectiveValExtraction 'distro_codename' "${l_json_osinfo}")

    version_id=${version_id:-}
    version_id=$(fnBase_OSInfoDirectiveValExtraction 'distro_version_id' "${l_json_osinfo}")

    ip_local=${ip_local:-}
    ip_local=$(fnBase_OSInfoDirectiveValExtraction 'ip_local' "${l_json_osinfo}")

    ip_public=${ip_public:-}
    ip_public=$(fnBase_OSInfoDirectiveValExtraction 'ip_public' "${l_json_osinfo}")

    ip_public_locate=${ip_public_locate:-}
    ip_public_locate=$(fnBase_OSInfoDirectiveValExtraction 'ip_public_locate' "${l_json_osinfo}")

    ip_public_country_code=${ip_public_country_code:-}
    ip_public_country_code=$(fnBase_OSInfoDirectiveValExtraction 'ip_public_country_code' "${l_json_osinfo}")

    ip_public_timezone=${ip_public_timezone:-}
    ip_public_timezone=$(fnBase_OSInfoDirectiveValExtraction 'ip_public_timezone' "${l_json_osinfo}")

    ip_public_as=${ip_public_as:-}
    ip_public_as=$(fnBase_OSInfoDirectiveValExtraction 'ip_public_as' "${l_json_osinfo}")

    pack_manager=${pack_manager:-}
    pack_manager=$(fnBase_OSInfoDirectiveValExtraction 'distro_pack_manager' "${l_json_osinfo}")

    # Output
    fnBase_CentralOutputTitle 'Machine Information'
    fnBase_OutputControl 'Manufacturer' "$(fnBase_OSInfoDirectiveValExtraction 'product_manufacturer' "${l_json_osinfo}")"
    fnBase_OutputControl 'Family' "$(fnBase_OSInfoDirectiveValExtraction 'product_family' "${l_json_osinfo}")"
    fnBase_OutputControl 'Product Name' "$(fnBase_OSInfoDirectiveValExtraction 'product_name' "${l_json_osinfo}")"
    fnBase_OutputControl 'Version' "$(fnBase_OSInfoDirectiveValExtraction 'product_version' "${l_json_osinfo}")"
    fnBase_OutputControl 'Serial Number' "$(fnBase_OSInfoDirectiveValExtraction 'product_serial_num' "${l_json_osinfo}")"
    fnBase_OutputControl 'UUID' "$(fnBase_OSInfoDirectiveValExtraction 'product_uuid' "${l_json_osinfo}")"
    fnBase_OutputControl 'Wake-up Type' "$(fnBase_OSInfoDirectiveValExtraction 'product_wakeup_type' "${l_json_osinfo}")"
    fnBase_OutputControl 'Virtualization' "$(fnBase_OSInfoDirectiveValExtraction 'distro_virtualization' "${l_json_osinfo}")"
    # fnBase_OutputControl 'Chassis' "$(fnBase_OSInfoDirectiveValExtraction 'distro_virtualization' "${distro_chassis}")"
    fnBase_OutputControl 'Icon name' "$(fnBase_OSInfoDirectiveValExtraction 'distro_icon_name' "${l_json_osinfo}")"

    fnBase_CentralOutputTitle 'GNU/Linux Disto Information'
    fnBase_OutputControl 'Full Name' "${distro_fullname}"
    fnBase_OutputControl 'Distro Name' "${distro_name}"
    fnBase_OutputControl 'Version ID' "${version_id}"
    fnBase_OutputControl 'Code Name' "${codename}"
    fnBase_OutputControl 'Kernel Version' "$(fnBase_OSInfoDirectiveValExtraction 'distro_kernel_version' "${l_json_osinfo}")"
    fnBase_OutputControl 'Release Date' "$(fnBase_OSInfoDirectiveValExtraction 'distro_release_date' "${l_json_osinfo}")"
    fnBase_OutputControl 'EOL Date' "$(fnBase_OSInfoDirectiveValExtraction 'distro_eol_date' "${l_json_osinfo}")"
    fnBase_OutputControl 'Official Site' "$(fnBase_OSInfoDirectiveValExtraction 'distro_official_site' "${l_json_osinfo}")"

    if [[ -n "${ip_public}" ]]; then
        [[ -n "${ip_local}" && "${ip_public}" != "${ip_local}" ]] && fnBase_OutputControl 'Internal IP' "${ip_local}"
        [[ -n "${ip_public}" ]] && fnBase_OutputControl 'External IP' "${ip_public} (${ip_public_country_code}.${ip_public_locate})"
        fnBase_OutputControl 'External IP AS Info' "${ip_public_as}"
        fnBase_OutputControl 'External IP ISP Info' "$(fnBase_OSInfoDirectiveValExtraction 'ip_public_isp' "${l_json_osinfo}")"
    fi

    version_id=${version_id%%.*}
}


#########  2-0. Variables Preprocessing  #########
fn_VariablesPreprocessing(){
    # - Email
    [[ -n "${email_address}" && "${email_address}" =~ ^[A-Za-z1-9]+[A-Za-z0-9._%+-]+@[A-Za-z0-9_-]+\.[A-Za-z]{2,4}$ ]] || email_address=''
}


#########  2-1. Package Repository Setting & System Update  #########
fn_RepositorySourceOperation(){
    if [[ -n "${ip_public_as}" ]]; then
        if [[ "${ip_public_as}" =~ Amazon.com ]]; then
            # use default repository setting
            repository_change=0
        # elif [[ "${ip_public_as}" =~ 'Google LLC' ]]; then
        # elif [[ "${ip_public_as}" =~ 'Microsoft Corporation' ]]; then
        fi
    fi

    case "${pack_manager}" in
        yum )
            local repo_dir=${repo_dir:-'/etc/yum.repos.d'}
            if [[ "${repository_change}" -eq 1 && "${distro_name}" == 'centos' && "${ip_public_country_code}" == 'CN' ]]; then
                local repo_dir_backup="${repo_dir}${bak_suffix}"
                if [[ ! -d "${repo_dir_backup}" ]]; then
                    mkdir -p "${repo_dir_backup}"
                    mv -f "${repo_dir}"/CentOS*.repo "${repo_dir_backup}"
                fi

                # https://mirrors.163.com/.help/centos.html
                local repo_savename="${repo_dir}/CentOS-Base.repo"
                [[ -f "${repo_savename}" ]] || $download_method "https://mirrors.163.com/.help/CentOS${version_id}-Base-163.repo" > "$repo_savename"
            fi

            # Installing EPEL Repository
            local l_epel_repo_path=${l_epel_repo_path:-"${repo_dir}/epel.repo"}
            if [[ ! -f "${l_epel_repo_path}" ]]; then
                local rpm_gpg_dir='/etc/pki/rpm-gpg/'
                [[ -f "${rpm_gpg_dir}RPM-GPG-KEY-EPEL-${version_id}" ]] || $download_method "https://dl.fedoraproject.org/pub/epel/RPM-GPG-KEY-EPEL-${version_id}" > "${rpm_gpg_dir}RPM-GPG-KEY-EPEL-${version_id}"
                fnBase_PackageManagerOperation 'install' "epel-release"
                # https://support.rackspace.com/how-to/install-epel-and-additional-repositories-on-centos-and-red-hat/
                if [[ ! -f "${l_epel_repo_path}" ]]; then
                    if [[ "${distro_name}" != 'amzn' ]]; then
                        fnBase_PackageManagerOperation 'install' "https://dl.fedoraproject.org/pub/epel/epel-release-latest-${version_id}.noarch.rpm"
                    else
                        # Amazon Linux 2 offers long-term support until June 30, 2023.
                        # https://aws.amazon.com/premiumsupport/knowledge-center/ec2-enable-epel/
                        # Amazon Linux 2.0 (2017.12) LTS Release Candidate has no epel.repo, need to install manually
                        fnBase_PackageManagerOperation 'install' 'https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm'

                        # epel-release is available in Amazon Linux Extra topic "epel"
                        # sudo amazon-linux-extras install epel
                        # https://aws.amazon.com/amazon-linux-2/faqs/#Amazon_Linux_Extras
                    fi
                    [[ "${distro_name}" == 'amzn' ]] && version_id=7
                fi
            fi

            [[ -s "${l_epel_repo_path}" ]] && sed -r -i '/^\[epel\]$/,/^\[/{s@^(enabled=).*@\11@g;}' "${l_epel_repo_path}"
            [[ -f "${repo_dir}/epel-testing.repo" ]] && rm -f "${repo_dir}/epel-testing.repo"

            # Yum plugin which chooses fastest repository from a mirrorlist
            [[ -z $(rpm -qa yum-plugin-fastestmirror 2> /dev/null) ]] && fnBase_PackageManagerOperation 'install' 'yum-plugin-fastestmirror'
            ;;
        dnf )
            if [[ "${repository_change}" -eq 1 && "${distro_name}" == 'fedora' && "${ip_public_country_code}" == 'CN' ]]; then
                local repo_dir=${repo_dir:-'/etc/yum.repos.d/'}
                local repo_dir_backup="${repo_dir}${bak_suffix}"
                if [[ ! -d "${repo_dir_backup}" ]]; then
                    mkdir -p "${repo_dir_backup}"
                    mv -f "${repo_dir}*.repo" "${repo_dir_backup}"
                fi

                # https://mirrors.163.com/.help/fedora.html
                local repo_fedora_savename="${repo_dir}Fedora.repo"
                [[ -f "${repo_fedora_savename}" ]] || $download_method "https://mirrors.163.com/.help/fedora-163.repo" > "$repo_fedora_savename"

                local repo_fedora_updates_savename="${repo_dir}Fedora-Updates.repo"
                [[ -f "${repo_fedora_updates_savename}" ]] || $download_method "https://mirrors.163.com/.help/fedora-updates-163.repo" > "$repo_fedora_updates_savename"
            fi
            ;;
        apt-get )
            local repo_path=${repo_path:-'/etc/apt/sources.list'}
            [[ -f "${repo_path}${bak_suffix}" ]] || cp -fp "${repo_path}" "${repo_path}${bak_suffix}"
            sed -r -i '/cdrom/d' "${repo_path}"

            if [[ "${repository_change}" -eq 1 ]]; then
                local l_repo_site
                local l_security_site
                local l_backports_site

                case "${distro_name}" in
                    debian )
                        if [[ "${ip_public_country_code}" == 'CN' ]]; then
                            l_repo_site='https://mirrors.163.com'
                            l_security_site="${l_repo_site}"
                            l_backports_site="${l_repo_site}"
                        else
                            # https://deb.debian.org/

                            # https://wiki.debian.org/SourcesList
                            # https://deb.debian.org/
                            # https://backports.debian.org/Instructions/

			    # https://whydoesaptnotusehttps.com/
                            # versions of apt before 1.5 (i.e. Debian stretch and earlier) you must first install the apt-transport-https package.
                            # l_repo_site='https://deb.debian.org'    # http://cdn-fastly.deb.debian.org
			    l_repo_site='http://deb.debian.org'
                            l_security_site='http://security.debian.org'    # http://cdn-fastly.deb.debian.org
                            l_backports_site='http://ftp.debian.org'
                        fi

                        echo -e "deb ${l_repo_site}/${distro_name} ${codename} main contrib non-free\ndeb-src ${l_repo_site}/${distro_name} ${codename} main contrib non-free\n\ndeb ${l_repo_site}/${distro_name} ${codename}-updates main contrib non-free\ndeb-src ${l_repo_site}/${distro_name} ${codename}-updates main contrib non-free\n\ndeb ${l_security_site}/${distro_name}-security/ ${codename}/updates main contrib non-free\ndeb-src ${l_security_site}/${distro_name}-security/ ${codename}/updates main contrib non-free\n" > "${repo_path}"
                        echo -e "deb ${l_backports_site}/${distro_name} ${codename}-backports main contrib non-free\ndeb-src ${l_backports_site}/${distro_name} ${codename}-backports main contrib non-free" >> "${repo_path}"
                        ;;
                    ubuntu )
                        # for china mainland only
                        if [[ "${ip_public_country_code}" == 'CN' ]]; then
                            l_repo_site='https://mirrors.163.com'
                            echo -e "deb ${l_repo_site}/${distro_name}/ ${codename} main restricted universe multiverse\ndeb-src ${l_repo_site}/${distro_name}/ ${codename} main restricted universe multiverse\n\ndeb ${l_repo_site}/${distro_name}/ ${codename}-security main restricted universe multiverse\ndeb-src ${l_repo_site}/${distro_name}/ ${codename}-security main restricted universe multiverse\n\ndeb ${l_repo_site}/${distro_name}/ ${codename}-updates main restricted universe multiverse\ndeb-src ${l_repo_site}/${distro_name}/ ${codename}-updates main restricted universe multiverse\n\n" > "${repo_path}"
                            echo -e "deb ${l_repo_site}/${distro_name}/ ${codename}-proposed main restricted universe multiverse\ndeb-src ${l_repo_site}/${distro_name}/ ${codename}-proposed main restricted universe multiverse\n\ndeb ${l_repo_site}/${distro_name}/ ${codename}-backports main restricted universe multiverse\ndeb-src ${l_repo_site}/${distro_name}/ ${codename}-backports main restricted universe multiverse\n" >> "${repo_path}"
                        fi
                        ;;
                esac
            fi
            ;;
        zypper )
            if [[ "${repository_change}" -eq 1 && "${distro_name}" == 'opensuse' ]]; then
                # for i in $(zypper lr | awk -F\| 'match($1,/[[:digit:]]/){a=gensub(/[[:blank:]]/,"","g",$1);arr[a]=a}END{PROCINFO["sorted_in"]="@val_num_desc";for (i in arr) print arr[i]}'); do zypper rr "$i" &> /dev/null ; done

                for i in $(zypper lr | sed -r -n '/^[[:space:]]*[[:digit:]]+[[:space:]]*\|/{s@^[[:space:]]*([[:digit:]]+)[[:space:]]*.*@\1@g;1!G;h;$!d;p}' | sed '/^$/d'); do zypper rr "$i" &> /dev/null ; done

                local repo_keyword=${repo_keyword:-"${version_id}"}
                [[ "${version_id%%.*}" -ge 42 ]] && repo_keyword="leap/${version_id}"

                local repo_url=${repo_url:-'https://download.opensuse.org'}
                local repo_alias=${repo_alias:-'OpenSUSE'}

                if [[ "${ip_public_country_code}" == 'CN' ]]; then
                    repo_url="https://mirrors.ustc.edu.cn/${distro_name}"
                    repo_alias='USTC'
                fi

                zypper ar -fcg "${repo_url}/distribution/${repo_keyword}/repo/oss" "${repo_alias}:${repo_keyword##*/}:OSS" &> /dev/null
                zypper ar -fcg "${repo_url}/distribution/${repo_keyword}/repo/non-oss" "${repo_alias}:${repo_keyword##*/}:NON-OSS" &> /dev/null
                zypper ar -fcg "${repo_url}/update/${repo_keyword}/oss" "${repo_alias}:${repo_keyword##*/}:UPDATE-OSS" &> /dev/null
                zypper ar -fcg "${repo_url}/update/${repo_keyword}/non-oss" "${repo_alias}:${repo_keyword##*/}:UPDATE-NON-OSS" &> /dev/null

                # For selinux-policy
                # https://software.opensuse.org/download.html?project=security%3ASELinux&package=selinux-policy
                local selinux_distro_version=${selinux_distro_version:-"${version_id}"}

                # just list 42.2, not list 42.3, treat 42.3 as 42.2
                [[ $(expr "${version_id}" \>= 42.2) -eq 1 ]] && selinux_distro_version='42.2'

                [[ $(expr "${version_id%%.*}" \>= 42) -eq 1 ]] && zypper ar -fcg https://download.opensuse.org/repositories/security:/SELinux/openSUSE_Leap_"${selinux_distro_version}"/ OpenSUSE:"${selinux_distro_version}":SELinux

                # zypper ar -fcg https://mirrors.ustc.edu.cn/opensuse/distribution/leap/42.3/repo/oss USTC:42.3:OSS
                # zypper ar -fcg https://mirrors.ustc.edu.cn/opensuse/distribution/leap/42.3/repo/non-oss USTC:42.3:NON-OSS
                # zypper ar -fcg https://mirrors.ustc.edu.cn/opensuse/update/leap/42.3/oss USTC:42.3:UPDATE-OSS
                # zypper ar -fcg https://mirrors.ustc.edu.cn/opensuse/update/leap/42.3/non-oss USTC:42.3:UPDATE-NON-OSS

                # zypper ar -fcg https://download.opensuse.org/distribution/leap/42.3/repo/oss/ OpenSUSE:42.3:OSS
                # zypper ar -fcg https://download.opensuse.org/distribution/leap/42.3/repo/non-oss/ OpenSUSE:42.3:NON-OSS
                # zypper ar -fcg https://download.opensuse.org/update/leap/42.3/oss/ OpenSUSE:42.3:UPDATE-OSS
                # zypper ar -fcg https://download.opensuse.org/update/leap/42.3/non-oss/ OpenSUSE:42.3:UPDATE-NON-OSS
                # zypper ar -dcg https://download.opensuse.org/source/distribution/leap/42.3/repo/oss/ OpenSUSE:42.3:SOURCE-OSS
                # zypper ar -dcg https://download.opensuse.org/distribution/leap/42.3/repo/non-oss/ OpenSUSE:42.3:SOURCE-NON-OSS
                # zypper ar -dcg https://download.opensuse.org/debug/distribution/leap/42.3/repo/oss/ OpenSUSE:42.3:DEBUG-OSS
                # zypper ar -dcg https://download.opensuse.org/debug/update/leap/42.3/oss/ OpenSUSE:42.3:DEBUG-UPDATE-OSS
            fi
            ;;
    esac
}

fn_PackageManagerConfOptimization(){
    fnBase_OperationProcedureStatement 'Package manager conf optimiztion'
    local l_packmanager_conf_path=${l_packmanager_conf_path:-}
    local l_installonly_limit=${l_installonly_limit:-'2'}
    case "${pack_manager}" in
        yum )
            l_packmanager_conf_path='/etc/yum.conf'
            if [[ -s "${l_packmanager_conf_path}" ]]; then
                sed -r -i '/[[:space:]]*metadata_expire[[:space:]]*=/{s@^#?[[:space:]]*([^=]*=).*@\13d@g;}' "${l_packmanager_conf_path}" 2> /dev/null
                sed -r -i '/[[:space:]]*installonly_limit[[:space:]]*=/{s@^#?[[:space:]]*([^=]*=).*@\1'"${l_installonly_limit}"'@g;}' "${l_packmanager_conf_path}" 2> /dev/null
                sed -r -i '/[[:space:]]*keepcache[[:space:]]*=/{s@^#?[[:space:]]*([^=]*=).*@\11@g;}' "${l_packmanager_conf_path}" 2> /dev/null
                sed -r -i '/[[:space:]]*groupremove_leaf_only[[:space:]]*=/{s@^#?[[:space:]]*([^=]*=).*@\11@g;}' "${l_packmanager_conf_path}" 2> /dev/null
                sed -r -i '/[[:space:]]*obsoletes[[:space:]]*=/{s@^#?[[:space:]]*([^=]*=).*@\11@g;}' "${l_packmanager_conf_path}" 2> /dev/null
            fi
            ;;
        dnf )
            l_packmanager_conf_path='/etc/dnf/dnf.conf'
            if [[ -s "${l_packmanager_conf_path}" ]]; then
                sed -r -i '/[[:space:]]*installonly_limit[[:space:]]*=/{s@^#?[[:space:]]*([^=]*=).*@\1'"${l_installonly_limit}"'@g;}' "${l_packmanager_conf_path}" 2> /dev/null
                # sed -r -i '/[[:space:]]*clean_requirements_on_remove[[:space:]]*=/{s@^#?[[:space:]]*([^=]*=).*@\1True@g;}' "${l_packmanager_conf_path}" 2> /dev/null
            fi
            ;;
        # zypper )
        #     # /etc/zypp/zypp.conf
        #     # /etc/zypp/zypper.conf
        #     ;;
        # apt-get )
        #     # /etc/apt/apt.conf.d
        #     ;;
    esac

    fnBase_OperationProcedureResult "${l_packmanager_conf_path}"
}

fn_AutomaticPackageUpdate(){
    fnBase_OperationProcedureStatement 'Automatic package update'

    local l_update_method=${l_update_method:-}

    case "${pack_manager}" in
        apt-get )
            # https://www.howtoforge.com/tutorial/how-to-setup-automatic-security-updates-on-ubuntu-1604/
            # https://linoxide.com/ubuntu-how-to/enable-disable-unattended-upgrades-ubuntu-16-04/
            # https://www.vultr.com/docs/how-to-set-up-unattended-upgrades-on-debian-9-stretch
            # unattended-upgrades - automatic installation of security upgrades
            fnBase_CommandExistIfCheck 'unattended-upgrades' || fnBase_PackageManagerOperation 'install' 'unattended-upgrades'
            # unattended-upgrades -v
            # unattended-upgrade -v -d --dry-run
            if fnBase_CommandExistIfCheck 'unattended-upgrades'; then
                l_update_method='unattended-upgrades'
                fnBase_CommandExistIfCheck 'dpkg-reconfigure' && DEBIAN_FRONTEND=noninteractive dpkg-reconfigure -plow unattended-upgrades 2> /dev/null

                # /etc/apt/apt.conf.d/10periodic
                [[ -s /etc/apt/apt.conf.d/20auto-upgrades ]] && echo -e "APT::Periodic::Update-Package-Lists \"1\";\nAPT::Periodic::Unattended-Upgrade \"1\";\nAPT::Periodic::Download-Upgradeable-Packages \"1\";\n# AutocleanInterval: Auto clean packages every X days.\nAPT::Periodic::AutocleanInterval \"2\";" > /etc/apt/apt.conf.d/20auto-upgrades

                local l_unattended_upgrade_path=${l_unattended_upgrade_path:-'/etc/apt/apt.conf.d/50unattended-upgrades'}
                if [[ -s "${l_unattended_upgrade_path}" ]]; then
                    case "${distro_name}" in
                        ubuntu ) sed -r -i '/\$\{distro_codename\}-(security|updates)/{s@^//*@@g;}' "${l_unattended_upgrade_path}" 2> /dev/null ;;
                        debian ) sed -r -i '/o=Debian,a=/{s@^\/*@@g;}' "${l_unattended_upgrade_path}" 2> /dev/null ;;
                    esac

                    sed -r -i '/AutoFixInterruptedDpkg/{s@^\/*(.*")[^"]*(".*)$@\1true\2@g;}' "${l_unattended_upgrade_path}" 2> /dev/null
                    sed -r -i '/InstallOnShutdown/{s@^\/*(.*")[^"]*(".*)$@\1false\2@g;}' "${l_unattended_upgrade_path}" 2> /dev/null
                    sed -r -i '/Remove-Unused-Dependencies/{s@^\/*(.*")[^"]*(".*)$@\1true\2@g;}' "${l_unattended_upgrade_path}" 2> /dev/null
                    sed -r -i '/Automatic-Reboot/{s@^\/*(.*")[^"]*(".*)$@\1false\2@g;}' "${l_unattended_upgrade_path}" 2> /dev/null
                    sed -r -i '/Automatic-Reboot-WithUsers/{s@^\/*(.*")[^"]*(".*)$@\1false\2@g;}' "${l_unattended_upgrade_path}" 2> /dev/null
                    # sed -r -i '/Automatic-Reboot-Time/{s@^\/*(.*")[^"]*(".*)$@\103:00\2@g;}' "${l_unattended_upgrade_path}" 2> /dev/null

                    if [[ -n "${email_address}" ]]; then
                        sed -r -i '/^\/*Unattended-Upgrade::Mail[[:space:]]+/{s#^\/*(.*")[^"]*(".*)$#\1'"${email_address}"'\2#g;}' "${l_unattended_upgrade_path}" 2> /dev/null
                        sed -r -i '/Unattended-Upgrade::MailOnlyOnError/{s@^\/*(.*")[^"]*(".*)$@\1false\2@g;}' "${l_unattended_upgrade_path}" 2> /dev/null
                    fi    # end if email_address
                fi    # end if l_unattended_upgrade_path
            fi

            # https://askubuntu.com/questions/800479/ubuntu-16-04-slow-boot-apt-daily-service
            if fnBase_CommandExistIfCheck 'systemctl'; then
                if [[ -n $(systemctl list-unit-files 2> /dev/null | sed -r -n '/apt-daily.timer/{p}') ]]; then
                    [[ -d /etc/systemd/system/apt-daily.timer.d/ ]] || mkdir -p /etc/systemd/system/apt-daily.timer.d/
                    echo -e "# apt-daily timer configuration override\n[Timer]\nOnBootSec=15min\nOnUnitActiveSec=1d\nAccuracySec=1h\nRandomizedDelaySec=30min" > /etc/systemd/system/apt-daily.timer.d/override.conf
                fi
            fi
            ;;
        yum )
            # https://linuxaria.com/howto/enabling-automatic-updates-in-centos-7-and-rhel-7
            # https://linuxaria.com/pills/enabling-automatic-updates-in-centos-6-and-red-hat-6

            # yum-cron - Files needed to run yum updates as a cron job
            fnBase_CommandExistIfCheck 'yum-cron' || fnBase_PackageManagerOperation 'install' 'yum-cron'
            l_update_method='yum-cron'

            # CentOS 7: /etc/yum/yum-cron.conf    /etc/yum/yum-cron-hourly.conf
            # CentOS 6: /etc/sysconfig/yum-cron

            local l_yum_cron_path

            if fnBase_CommandExistIfCheck 'systemctl'; then
                l_yum_cron_path='/etc/yum/yum-cron.conf'
                if [[ -s "${l_yum_cron_path}" ]]; then
                    sed -r -i '/^[[:space:]]*update_cmd[[:space:]]*=/{s@^[[:space:]]*([^=]*=[[:space:]]*).*$@\1security@g;}' "${l_yum_cron_path}" 2> /dev/null
                    sed -r -i '/^[[:space:]]*update_messages[[:space:]]*=/{s@^[[:space:]]*([^=]*=[[:space:]]*).*$@\1yes@g;}' "${l_yum_cron_path}" 2> /dev/null
                    sed -r -i '/^[[:space:]]*update_messages[[:space:]]*=/{s@^[[:space:]]*([^=]*=[[:space:]]*).*$@\1yes@g;}' "${l_yum_cron_path}" 2> /dev/null
                    sed -r -i '/^[[:space:]]*update_messages[[:space:]]*=/{s@^[[:space:]]*([^=]*=[[:space:]]*).*$@\1yes@g;}' "${l_yum_cron_path}" 2> /dev/null

                    if [[ -n "${email_address}" ]]; then
                        sed -r -i '/^[[:space:]]*emit_via[[:space:]]*=/{s@^[[:space:]]*([^=]*=[[:space:]]*).*$@\1email@g;}' "${l_yum_cron_path}" 2> /dev/null
                        sed -r -i '/^[[:space:]]*email_to[[:space:]]*=/{s/^[[:space:]]*([^=]*=[[:space:]]*).*$/\1'"${email_address}"'/g;}' "${l_yum_cron_path}" 2> /dev/null
                    fi    # end if email_address
                fi    # end if l_yum_cron_path
            else
                l_yum_cron_path='/etc/sysconfig/yum-cron'
                if [[ -s "${l_yum_cron_path}" ]]; then
                    # seperate download & installation
                    sed -r -i '/^[[:space:]]*CHECK_ONLY[[:space:]]*=/{s@^[[:space:]]*([^=]*=[[:space:]]*).*$@\1yes@g;}' "${l_yum_cron_path}" 2> /dev/null
                    sed -r -i '/^[[:space:]]*CHECK_FIRST[[:space:]]*=/{s@^[[:space:]]*([^=]*=[[:space:]]*).*$@\1yes@g;}' "${l_yum_cron_path}" 2> /dev/null
                    sed -r -i '/^[[:space:]]*DOWNLOAD_ONLY[[:space:]]*=/{s@^[[:space:]]*([^=]*=[[:space:]]*).*$@\1yes@g;}' "${l_yum_cron_path}" 2> /dev/null

                    if [[ -n "${email_address}" ]]; then
                        sed -r -i '/^[[:space:]]*MAILTO[[:space:]]*=/{s/^[[:space:]]*([^=]*=[[:space:]]*).*$/\1'"${email_address}"'/g;}' "${l_yum_cron_path}" 2> /dev/null
                    fi    # end if email_address
                fi    # end if l_yum_cron_path
            fi

            fnBase_CommandExistIfCheck 'yum-cron' && fnBase_SystemServiceManagement 'yum-cron' 'enable'
            ;;
        dnf )
            # https://major.io/2015/05/11/automatic-package-updates-with-dnf/
            # dnf-automatic - Alternative CLI to "dnf upgrade" suitable for automatic, regular execution.

            fnBase_CommandExistIfCheck 'dnf-automatic' || fnBase_PackageManagerOperation 'install' 'dnf-automatic'
            l_update_method='dnf-automatic'

            local l_dnf_automatic_path=${l_dnf_automatic_path:-'/etc/dnf/automatic.conf'}

            if [[ -s "${l_dnf_automatic_path}" ]]; then
                sed -r -i '/^[[:space:]]*upgrade_type[[:space:]]*=/{s@^[[:space:]]*([^=]*=[[:space:]]*).*$@\1security@g;}' "${l_dnf_automatic_path}" 2> /dev/null
                sed -r -i '/^[[:space:]]*upgrade_type[[:space:]]*=/{s@^[[:space:]]*([^=]*=[[:space:]]*).*$@\1security@g;}' "${l_dnf_automatic_path}" 2> /dev/null
                sed -r -i '/^[[:space:]]*download_updates[[:space:]]*=/{s@^[[:space:]]*([^=]*=[[:space:]]*).*$@\1yes@g;}' "${l_dnf_automatic_path}" 2> /dev/null
                sed -r -i '/^[[:space:]]*apply_updates[[:space:]]*=/{s@^[[:space:]]*([^=]*=[[:space:]]*).*$@\1no@g;}' "${l_dnf_automatic_path}" 2> /dev/null

                if [[ -n "${email_address}" ]]; then
                    sed -r -i '/^[[:space:]]*emit_via[[:space:]]*=/{s@^[[:space:]]*([^=]*=[[:space:]]*).*$@\1email@g;}' "${l_dnf_automatic_path}" 2> /dev/null
                    sed -r -i '/^[[:space:]]*email_to[[:space:]]*=/{s/^[[:space:]]*([^=]*=[[:space:]]*).*$/\1'"${email_address}"'/g;}' "${l_yum_cron_path}" 2> /dev/null
                fi    # end if email_address

            fi    # end if l_dnf_automatic_path
            ;;
        zypper )
            # https://www.hiroom2.com/2016/12/19/opensuse-13-auto-update/
            l_update_method='PackageKit'

            # /etc/cron.daily/packagekit-background.cron
            local l_packagekit_path=${l_packagekit_path:-'/etc/sysconfig/packagekit-background'}
            [[ -s "${l_packagekit_path}" ]] || fnBase_PackageManagerOperation 'install' 'PackageKit'

            if [[ -s "${l_packagekit_path}" ]]; then
                sed -r -i '/^[[:space:]]*ENABLED[[:space:]]*=/{s@^[[:space:]]*([^=]*=[[:space:]]*).*$@\1yes@g;}' "${l_packagekit_path}" 2> /dev/null
                sed -r -i '/^[[:space:]]*CHECK_ONLY[[:space:]]*=/{s@^[[:space:]]*([^=]*=[[:space:]]*).*$@\1no@g;}' "${l_packagekit_path}" 2> /dev/null

                if [[ -n "${email_address}" ]]; then
                    sed -r -i '/^[[:space:]]*MAILTO[[:space:]]*=/{s/^[[:space:]]*([^=]*=[[:space:]]*).*$/\1'"${email_address}"'/g;}' "${l_packagekit_path}" 2> /dev/null
                fi
            fi    # end if l_packagekit_path

            [[ -s "${l_packagekit_path}" ]] && fnBase_SystemServiceManagement 'packagekit' 'enable'
            ;;
    esac

    fnBase_OperationProcedureResult "${l_update_method}"
}

fn_KernelLatestRelease(){
    case "${pack_manager}" in
        apt-get )
            case "${distro_name}" in
                ubuntu )
                    # linux-generic-hwe-16.04
                    local l_hwe_name
                    #l_hwe_name=$(apt-cache search linux-generic-hwe 2> /dev/null | sed -r -n '1{s@^([^[:space:]]+).*$@\1@g;p}')
                    l_hwe_name=$(apt-cache search linux-generic-hwe 2> /dev/null | sed -r -n '/'"${version_id}"'/{s@^([^[:space:]]+).*$@\1@g;p;q}')
                    [[ -n "${l_hwe_name}" ]] && fnBase_PackageManagerOperation 'install' "${l_hwe_name}"
                    ;;
                debian )
                    # http://jensd.be/818/linux/install-a-newer-kernel-in-debian-9-stretch-stable
                    # need add ${codename}-backports to source.list first
                    fnBase_CommandExistIfCheck 'apt-get' && apt-get -t "${codename}"-backports -y -q upgrade &> /dev/null
                    ;;
            esac
            ;;
        zypper )
            # http://pvdm.xs4all.nl/wiki/index.php/How_to_have_the_latest_kernel_in_openSUSE
            if [[ "${distro_name}" == 'opensuse' ]]; then
                if [[ -s '/etc/zypp/zypp.conf' ]]; then
                    # multiversion = provides:multiversion(kernel)
                    # multiversion.kernels = latest,latest-1,running
                    zypper ar -fcg https://download.opensuse.org/repositories/Kernel:/HEAD/standard/ kernel-repo &> /dev/null
                    zypper dup -r kernel-repo &> /dev/null
                fi
            fi
            ;;
        yum )
            # https://www.tecmint.com/install-upgrade-kernel-version-in-centos-7/
            # ELRepo
            if [[ ! -f '/etc/yum.repos./elrepo.repo' ]]; then
                local l_elrepo_info
                l_elrepo_info=$($download_method https://elrepo.org/tiki/tiki-index.php | sed -r -n '/rpm (--import|-Uvh)/{s@<[^>]*>@@g;s@.*(http.*)$@\1@g;p}' | sed ':a;N;$!ba;s@\n@|@g;')
                # https://www.elrepo.org/RPM-GPG-KEY-elrepo.org|http://www.elrepo.org/elrepo-release-7.0-3.el7.elrepo.noarch.rpm|http://www.elrepo.org/elrepo-release-6-8.el6.elrepo.noarch.rpm
                rpm --import "${l_elrepo_info%%|*}" 2> /dev/null
                l_elrepo_info="${l_elrepo_info#*|}"
                case "${version_id}" in
                    7 ) l_elrepo_info="${l_elrepo_info%%|*}" ;;
                    6 ) l_elrepo_info="${l_elrepo_info##*|}" ;;
                esac

                fnBase_PackageManagerOperation 'install' "${l_elrepo_info}"

                # Long term support kernel package name is kernel-lt version
                # Mainline stable kernel package name is kernel-ml version

                # yum --disablerepo='*' --enablerepo='elrepo-kernel' list available
                # yum --disablerepo='*' --enablerepo='elrepo-kernel' -y -q install kernel-lt
                yum --disablerepo='*' --enablerepo='elrepo-kernel' -y -q install kernel-ml &> /dev/null

                case "${version_id}" in
                    7 )
                        # egrep ^menuentry /etc/grub2.cfg | cut -f 2 -d \'
                        fnBase_CommandExistIfCheck 'grub2-set-default' && grub2-set-default 0 2> /dev/null
                    ;;
                    6 )
                        [[ -s '/etc/grub.conf' ]] && sed -r -i '/default=/{s@^([^+]+=).*$@\10@g;}' /etc/grub.conf
                    ;;
                esac
            fi
            ;;
    esac
}

fn_RepositoryAndPackagesOperation(){
    fnBase_OperationPhaseStatement 'Package Management'
    # Repository Setting
    fnBase_OperationProcedureStatement 'Repository source configuration'
    fn_RepositorySourceOperation
    fnBase_OperationProcedureResult "${pack_manager^^}"

    # package manager config file optimization
    fn_PackageManagerConfOptimization

    # automatic update
    [[ "${automatic_update}" -eq 1 ]] && fn_AutomaticPackageUpdate

    # package update
    fnBase_OperationProcedureStatement 'Packages update'
    fnBase_PackageManagerOperation
    fnBase_PackageManagerOperation 'upgrade'
    fnBase_OperationProcedureResult

    # install latest kernel
    if [[ "${kernel_upgrade}" -eq 1 ]]; then
        fnBase_OperationProcedureStatement 'Kernel latest release installation'
        fn_KernelLatestRelease
        fnBase_OperationProcedureResult
    fi
}


#########  2-2. SELinux Configuration #########
fn_SELinuxConfiguration(){
    fnBase_OperationPhaseStatement "Mandatory Access Control - SELinux"
    # Permissive SELinux defaultly
    # 0 - Permissive / 1 - Enforcing / 2 - Disabled

    # https://wiki.debian.org/SELinux/Setup
    # https://giunchi.net/how-to-enable-selinux-on-debian-stretch-9

    # SELinux Operating Mode
    local l_selinux_mode=''
    case "${selinux_setting,,}" in
        2|disabled|d ) l_selinux_mode='disabled' ;;
        1|enforcing|e ) l_selinux_mode='enforcing' ;;
        0|permissive|p|* ) l_selinux_mode='permissive' ;;
    esac

    local l_selinux_config=${l_selinux_config:-'/etc/selinux/config'}

    # if ! fnBase_CommandExistIfCheck 'getenforce'; then
    if [[ ! -f "${l_selinux_config}" && "${l_selinux_mode}" != 'disabled' ]]; then
        # in enforcing mode, debian/ubuntu not works well, temporarily just allow permissive mode
        [[ "${l_selinux_mode}" == 'enforcing' && "${pack_manager}" != 'apt-get' ]] && enforcing_selinux=1

        # - install packages
        fnBase_OperationProcedureStatement "SELinux utilities installation"
        local l_selinux_pack_list=''
        case "${pack_manager}" in
            apt-get )
                # https://debian-handbook.info/browse/stable/sect.selinux.html
                # https://wiki.debian.org/SELinux/Setup
                # https://www.lowendtalk.com/discussion/105063/debian-9-stretch-frozen-getting-selinux
                # https://notesoncybersecurity.wordpress.com/2017/07/16/how-to-install-selinux-in-debian-stretch/

                # selinuxenabled   # It exits with status 0 if SELinux is enabled and 1 if it is not enabled.

                # selinux - Security-Enhanced Linux runtime support
                # selinux-basics - SELinux basic support
                # selinux-policy-default - Strict and Targeted variants of the SELinux policy
                # selinux-policy-mls - MLS (Multi Level Security) variant of the SELinux policy
                # selinux-policy-src - Source of the SELinux reference policy for customization
                # selinux-utils - SELinux utility programs
                # setools - tools for Security Enhanced Linux policy analysis

                l_selinux_pack_list='selinux-basics selinux-policy-default selinux-policy-mls  selinux-policy-src'
                [[ "${distro_name}" == 'ubuntu' ]] && l_selinux_pack_list="${l_selinux_pack_list} selinux"
                ;;
            dnf|yum )
                # selinux-policy-minimum selinux-policy-mls
                l_selinux_pack_list='policycoreutils policycoreutils-python selinux-policy selinux-policy-targeted libselinux-utils setroubleshoot-server setools setools-console mcstrans'

                # setroubleshoot-server
                # sealert -a /var/log/audit/audit.log
                ;;
            zypper )
                # https://en.opensuse.org/SDB:SELinux
                # https://doc.opensuse.org/documentation/leap/security/html/book.security/cha.selinux.html
                l_selinux_pack_list='selinux-policy selinux-policy-minimum selinux-policy-devel libselinux1 libselinux-devel libselinux-devel-static selinux-tools selinux-doc python-selinux checkpolicy policycoreutils policycoreutils-python mcstrans'
                ;;
        esac

        [[ -n "${l_selinux_pack_list}" ]] && fnBase_PackageManagerOperation 'install' "${l_selinux_pack_list}"
        fnBase_OperationProcedureResult

        # - preconditioning
        fnBase_OperationProcedureStatement "SELinux preconditioning"
        case "${pack_manager}" in
            apt-get )
                fnBase_CommandExistIfCheck 'selinux-policy-upgrade' && selinux-policy-upgrade &> /dev/null
                if [[ "${l_selinux_mode}" != 'disabled' ]]; then
                    fnBase_CommandExistIfCheck 'selinux-activate' && selinux-activate &> /dev/null
                fi

                # then reboot, default is 'permissive' mode, if wanna set 'enforcing' mode, after reboot, executing command 'selinux-config-enforcing enforcing'
                # selinux-config-enforcing - change /etc/selinux/config to set enforcing or permissive modes
                # Prompt:  Configured enforcing mode in /etc/selinux/config for the next boot. This can be overridden by "enforcing=0" on the kernel command line.
                # selinux-config-enforcing enforcing

                # audit2allow - generate SELinux policy allow/dontaudit rules from logs of denied operations
                # audit2why - translates SELinux audit messages into a description of why the access was denied (audit2allow -w)
                # audit2why -al
                ;;
            dnf|yum )
                [[ -f '/.autorelabel' ]] || touch /.autorelabel

                # http://www.chrisumbel.com/article/selinux_amazon_aws_ec2_ami_linux
                # https://cloudacademy.com/blog/selinux-aws-ec2-security/
                # change /boot/grub/grub.conf does not take affect
                [[ "${distro_name}" == 'amzn' && "${l_selinux_mode}" != 'disabled' && -s '/boot/grub/menu.lst' ]] && sed -r -i '/\/boot\/vmlinuz/{s@[[:space:]]*(selinux|security|permissive|enforcing)=[^[:space:]]+@@g;s@^(.*console=[^[:space:]]+[[:space:]]*)(.*)$@\1selinux=1 security=selinux '"${l_selinux_mode}"'=1 \2@g;}' /boot/grub/menu.lst
                ;;
            zypper )
                # selinux-policy-minimum
                # /etc/selinux/minimum/contexts/files/file_contexts
                if [[ "${l_selinux_mode}" != 'disabled' ]]; then
                    local l_grub=${l_grub:-'/etc/default/grub'}
                    if [[ -s "${l_grub}" ]]; then
                        # yast2 bootloader
                        # System › Boot Loader › Kernel Parameters. (Alt + P), add the following parameters to the Optional Kernel Command Line Parameters: 'security=selinux selinux=1 enforcing=0'
                        sed -r -i '/GRUB_CMDLINE_LINUX_DEFAULT=/d' "${l_grub}"
                        sed -r -i '$a GRUB_CMDLINE_LINUX_DEFAULT="security=selinux selinux=1 enforcing=0"' "${l_grub}"

                        # via function fn_GRUBConfiguring
                        # grub2-mkconfig -o /boot/grub2/grub.cfg &> /dev/null
                    fi

                    # Note that you cannot start restorecond until you boot a kernel with the parameters mentioned above, so let's just enable it for now, without starting.
                    fnBase_CommandExistIfCheck 'systemctl' && systemctl enable restorecond &> /dev/null
                fi

                # after reboot, execute the following command , file system labeling
                # fnBase_CommandExistIfCheck 'restorecon' && restorecon -Rp /
                # selinux check
                # selinux-ready
                ;;
        esac
        fnBase_OperationProcedureResult
    fi

    # - policy modification
    if [[ -f "${l_selinux_config}" && -s "${l_selinux_config}" ]]; then
        # 0 - Permissive / 1 - Enforcing / 2 - Disabled
        fnBase_OperationProcedureStatement "SELinux directives configuration"
        [[ -f "${l_selinux_config}${bak_suffix}" ]] || cp -fp "${l_selinux_config}" "${l_selinux_config}${bak_suffix}"

        local l_selinux_type=${l_selinux_type:-'targeted'}

        case "${pack_manager}" in
            yum|dnf )
                # minimum / mls / targeted
                l_selinux_type='targeted'
                ;;
            zypper )
                # ☆ SUSE/OpenSUSE just use SELINUXTYPE=minimum because package selinux-policy-minimum
                [[ "${l_selinux_mode}" != 'disabled' ]] && l_selinux_mode='permissive'
                l_selinux_type='minimum'
                ;;
            apt-get )
                # ☆ Default is 'permissive' mode, if wanna set 'enforcing' mode, after reboot, executing command 'selinux-config-enforcing enforcing'
                # 'enforcing' mode makes system not work well
                [[ "${l_selinux_mode}" != 'disabled' ]] && l_selinux_mode='permissive'
                # default / mls / src
                l_selinux_type='default'
                ;;
        esac

        sed -r -i '/^SELINUX=/{s@^[[:space:]]*([^=]+=).*@\1'"${l_selinux_mode}"'@g;}' "${l_selinux_config}"
        sed -r -i '/^SELINUXTYPE=/{s@^[[:space:]]*([^=]+=).*@\1'"${l_selinux_type}"'@g;}' "${l_selinux_config}"

        fnBase_OperationProcedureResult "${l_selinux_mode^}"
    fi
}

#########  2-3. GRUB Configuring  #########
fn_GRUBConfiguring(){
    fnBase_OperationPhaseStatement 'GRUB Configuration'
    # cat /proc/cmdline

    # GRUB_GFXMODE resolution / font size
    # https://askubuntu.com/questions/103516/grub2-use-maximum-detected-resolution

    # https://askubuntu.com/questions/1076178/plymouth-taking-a-lot-of-time-during-boot
    # GRUB_CMDLINE_LINUX_DEFAULT="quiet splash" ==> GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"

    fnBase_OperationProcedureStatement 'GRUB_TIMEOUT'

    local grub_regexp='^[-+]?[0-9]{1,}(\.[0-9]*)?$'
    if [[ "${grub_timeout}" =~ $grub_regexp ]]; then
        grub_timeout=${grub_timeout/[-+]}
        grub_timeout=${grub_timeout%%.*}
        [[ "${grub_timeout}" -gt 6 ]] && grub_timeout=6
    else
        grub_timeout="${default_grub_timeout}"
    fi

    if [[ -f /etc/default/grub ]]; then
        sed -r -i '/^GRUB_TIMEOUT=/s@^(GRUB_TIMEOUT=).*@\1'"${grub_timeout}"'@g' /etc/default/grub

        case "${pack_manager}" in
            apt-get )
                fnBase_CommandExistIfCheck 'update-grub' && update-grub &> /dev/null
                ;;
            zypper|dnf|yum )
                if [[ -f "/boot/efi/EFI/${distro_name}/grub.cfg" ]]; then
                    # UEFI-based machines
                    grub2-mkconfig -o "/boot/efi/EFI/${distro_name}/grub.cfg" &> /dev/null
                else
                    # BIOS-based machines
                    grub2-mkconfig -o /boot/grub2/grub.cfg &> /dev/null
                fi
                ;;
        esac

    elif [[ -f /etc/grub.conf ]]; then
        sed -r -i '/^timeout=/s@^(timeout=).*@\1'"$grub_timeout"'@g' /etc/grub.conf
    fi

    fnBase_OperationProcedureResult "${grub_timeout}s"
}


#########  2-4. Hostname & Timezone Setting  #########
fn_TimezoneDetection(){
    local l_new_timezone=${1:-''}
    local l_timezone=${l_timezone:-''}

    if [[ -n "${l_new_timezone}" ]]; then
        if [[ -f "/usr/share/zoneinfo/${l_new_timezone}" ]]; then
            l_timezone="${l_new_timezone}"
        else
            l_timezone=$(fn_TimezoneDetection)
        fi
    else
        l_timezone="${ip_public_timezone}"
        [[ -z "${l_timezone}" ]] && l_timezone="${default_timezone}"
    fi
    echo "${l_timezone}"
}

fn_HostnameTimezoneSetting(){
    fnBase_OperationPhaseStatement "Hostname & Timezone"

    # - Hostname Setting
    fnBase_OperationProcedureStatement "Hostname configuration"
    local current_existed_hostname=${current_existed_hostname:-}
    current_existed_hostname=$(hostname)

    if [[ -z "${hostname_specify}" ]]; then
        if [[ -n "${codename}" ]]; then
            hostname_specify="${codename,,}"
        elif [[ -n "${distro_name}" ]]; then
            hostname_specify="${distro_name,,}"
        fi

        local l_host_ip
        # prefer internal ip
        if [[ -n "${ip_local}" ]]; then
            l_host_ip="${ip_local}"
        elif [[ -n "${ip_public}" && "${ip_public}" =~ ^([0-9]{1,3}.){3}[0-9]{1,3}$ ]]; then
            l_host_ip="${ip_public}"
        else
            l_host_ip="$RANDOM"
        fi
        l_host_ip="${l_host_ip//./-}"

        local l_isp_name=${l_isp_name:-}
        if [[ -n "${ip_public_as}" ]]; then
            if [[ "${ip_public_as}" =~ Amazon.com ]]; then
                l_isp_name='AWS'
            elif [[ "${ip_public_as}" =~ 'Google LLC' ]]; then
                l_isp_name='GCP'   # Google LLC
            elif [[ "${ip_public_as}" =~ 'Microsoft Corporation' ]]; then
                # Microsoft Azure Datacenter IP Ranges
                # https://www.microsoft.com/en-us/download/details.aspx?id=41653
                l_isp_name='Azure'    # Microsoft Corporation
            elif [[ "${ip_public_as}" =~ DigitalOcean ]]; then
                l_isp_name='DO'   # DigitalOcean
            elif [[ "${ip_public_as}" =~ Choopa ]]; then
                l_isp_name='Vultr' # Vultr  AS20473 Choopa, LLC
            elif [[ "${ip_public_as}" =~ Alibaba ]]; then
                l_isp_name='Aliyun'   # Alibaba
            fi
        fi

        if [[ "${codename}" == 'wheezy' ]]; then
            hostname_specify="${hostname_specify}${l_host_ip}"
        else
            if [[ -n "${l_isp_name}" ]]; then
                hostname_specify="${l_isp_name}-${hostname_specify}-${l_host_ip}"
            else
                hostname_specify="${hostname_specify^}-${l_host_ip}"
            fi
        fi    # end if codename

    fi    # end if hostname_specify

    # nmcli general hostname
    # nmcli general hostname "${hostname_specify}"
    if fnBase_CommandExistIfCheck 'hostnamectl'; then
        # The static hostname is stored in /etc/hostname, see hostname(5) for more information. The pretty hostname, chassis type, and icon name are stored in /etc/machine-info, see machine-info(5). -- man hostnamectl
        # --static, --transient, --pretty
        # hostnamectl set-hostname "${hostname_specify}" 2> /dev/null
        hostnamectl --static set-hostname "${hostname_specify}" 2> /dev/null
        hostnamectl --pretty set-hostname "${hostname_specify}" 2> /dev/null
        [[ -f '/etc/hostname' ]] && echo "${hostname_specify}" > /etc/hostname
        # development, integration, staging, production
        # Debian Jessie has no set-deployment
        hostnamectl set-deployment development &> /dev/null
        [[ -n "${ip_public_locate}" ]] && hostnamectl set-location "${ip_public_locate}.${ip_public_country_code}" 2> /dev/null
    else
        hostname "${hostname_specify}" &> /dev/null   # temporarily change, when reboot, it will recover
        if [[ -f '/etc/sysconfig/network' ]]; then
            sed -r -i '/^HOSTNAME=/s@^(HOSTNAME=).*@\1'"${hostname_specify}"'@g' /etc/sysconfig/network #RHEL
        elif [[ -f '/etc/hostname' ]]; then
            echo "${hostname_specify}" > /etc/hostname  #Debian/OpenSUSE
        fi
    fi

    # sudo: unable to resolve host (USERNAME)
    local hosts_path=${hosts_path:-'/etc/hosts'}
    if [[ -f "${hosts_path}" ]]; then
        sed -r -i '/^(127.0.0.1|::1)/s@ '"${current_existed_hostname}"'@ '"${hostname_specify}"'@g' "${hosts_path}"

        if [[ -z $(sed -r -n '/^127.0.0.1/{/'"${hostname_specify}"'/p}' "${hosts_path}") ]]; then
            if [[ -z $(sed -r -n '/^127.0.0.1/p' "${hosts_path}") ]]; then
                sed -i '$a 127.0.0.1 '"${hostname_specify}"'' "${hosts_path}"
            else
                sed -i '/^127.0.0.1/a 127.0.0.1 '"${hostname_specify}"'' "${hosts_path}"
            fi
        fi
    fi
    fnBase_OperationProcedureResult "${hostname_specify}"

    # - Timezone Setting
    fnBase_OperationProcedureStatement "Timezone configuration"

    new_timezone=$(fn_TimezoneDetection "${timezone_specify}")

    if fnBase_CommandExistIfCheck 'timedatectl'; then
        timedatectl set-timezone "${new_timezone}" 2> /dev/null
        timedatectl set-local-rtc false
        timedatectl set-ntp true
    else
        if [[ "${pack_manager}" == 'apt-get' ]]; then
            echo "${new_timezone}" > /etc/timezone
            fnBase_CommandExistIfCheck 'dpkg-reconfigure' && dpkg-reconfigure -f noninteractive tzdata &> /dev/null
        else
            # RHEL/OpenSUSE
            local localtime_path='/etc/localtime'
            local new_timezone_path="/usr/share/zoneinfo/${new_timezone}"
            [[ -f "${localtime_path}" ]] && rm -f "${localtime_path}"
            ln -fs "${new_timezone_path}" "${localtime_path}"

            # For CentOS6 or older
            local l_sysconfig_clock='/etc/sysconfig/clock'
            if [[ -s "${l_sysconfig_clock}" ]]; then
                sed -r -i '/^ZONE=/{s@^([^=]+=).*@\1"'"${new_timezone}"'"@g;}' "${l_sysconfig_clock}" 2> /dev/null
            fi
        fi
    fi
    fnBase_OperationProcedureResult "${new_timezone}"
}


#########  2-5. Login User Configuration  #########
fn_LoginDirectivesConfiguration(){
    local l_item="${1:-}"
    local l_val="${2:-}"
    local l_path="${3:-}"
    local l_type=${4:-'login'}

    # - login: /etc/login.defs
    # - passwd: /etc/security/pwquality.conf

    if [[ -n "${l_item}" && -n "${l_val}" && -s "${l_path}" ]]; then
        case "${l_type,,}" in
            l|login ) sed -r -i '/^'"${l_item}"'[[:space:]]+/{s@^([^[:space:]]+[[:space:]]*).*$@\1'"${l_val}"'@g;}' "${l_path}" 2> /dev/null ;;
            p|passwd|password ) sed -r -i '/^#?[[:space:]]*'"${l_item}"'[[:space:]]=/{s@^#?[[:space:]]*([^[:space:]]+[[:space:]]*=[[:space:]]*).*@\1'"${l_val}"'@g;}' "${l_path}" 2> /dev/null ;;
        esac
    fi
}

fn_SystemUserConfiguration(){
    fnBase_OperationPhaseStatement "System User Management"

    # /var/cache/cracklib/cracklib_dict.pwd: No such file or directory
    # https://askubuntu.com/questions/557771/file-cracklib-dict-pwd-not-found-when-creating-a-new-user-or-changing-his-passwo#557772
    local l_cracklib_name=${l_cracklib_name:-'cracklib-runtime'}
    [[ "${pack_manager}" != 'apt-get' ]] && l_cracklib_name='cracklib-dicts'
    fnBase_PackageManagerOperation 'install' "${l_cracklib_name}"

    [[ "${pack_manager}" == 'apt-get' && ! -f '/lib/x86_64-linux-gnu/security/pam_pwquality.so' ]] && fnBase_PackageManagerOperation 'install' 'libpam-pwquality'

    local l_pwquaility_path=${l_pwquaility_path:-'/etc/security/pwquality.conf'}
    if [[ -f "${l_pwquaility_path}" ]]; then
        fnBase_OperationProcedureStatement "Password quality configuration"
        fn_LoginDirectivesConfiguration 'difok' '5' "${l_pwquaility_path}" 'passwd'
        fn_LoginDirectivesConfiguration 'minlen' '10' "${l_pwquaility_path}" 'passwd'
        fn_LoginDirectivesConfiguration 'minclass' '4' "${l_pwquaility_path}" 'passwd'
        fn_LoginDirectivesConfiguration 'maxrepeat' '0' "${l_pwquaility_path}" 'passwd'
        # The check is disabled if the value is 0.
        fn_LoginDirectivesConfiguration 'maxclassrepeat' '0' "${l_pwquaility_path}" 'passwd'
        fn_LoginDirectivesConfiguration 'lcredit' '0' "${l_pwquaility_path}" 'passwd'
        fn_LoginDirectivesConfiguration 'ucredit' '0' "${l_pwquaility_path}" 'passwd'
        fn_LoginDirectivesConfiguration 'dcredit' '0' "${l_pwquaility_path}" 'passwd'
        fn_LoginDirectivesConfiguration 'ocredit' '0' "${l_pwquaility_path}" 'passwd'
        fn_LoginDirectivesConfiguration 'gecoscheck' '0' "${l_pwquaility_path}" 'passwd'
        fnBase_OperationProcedureResult "${l_pwquaility_path}"
    fi

    # - Shadow password configuration
    local l_login_defs=${l_login_defs:-'/etc/login.defs'}
    if [[ -f "${l_login_defs}" ]]; then
        fnBase_OperationProcedureStatement "Shadow password configuration"
        # https://www.poftut.com/linux-etc-login-defs-configuration-examples/
        fn_LoginDirectivesConfiguration 'PASS_MAX_DAYS' "${pass_change_maxday}" "${l_login_defs}"
        fn_LoginDirectivesConfiguration 'PASS_MIN_DAYS' "${pass_change_minday}" "${l_login_defs}"
        fn_LoginDirectivesConfiguration 'PASS_WARN_AGE' "${pass_change_warnningday}" "${l_login_defs}"
        # fn_LoginDirectivesConfiguration 'UMASK' "${umask_default}" "${l_login_defs}"
        fn_LoginDirectivesConfiguration 'ENCRYPT_METHOD' 'SHA512' "${l_login_defs}"
        fn_LoginDirectivesConfiguration 'LOG_UNKFAIL_ENAB' 'no' "${l_login_defs}"
        fnBase_OperationProcedureResult "${l_login_defs}"
    fi

    # - sudoers file configuration
    fnBase_CommandExistIfCheck 'sudo' || fnBase_PackageManagerOperation 'install' "sudo"
    local user_if_existed=${user_if_existed:-0}
    local sudo_config_path=${sudo_config_path:-'/etc/sudoers'}

    # add normal user into group sudo/wheel without prompt password
    if [[ -n "${username_specify}" && -f "${sudo_config_path}" ]]; then
        fnBase_OperationProcedureStatement "sudoers config file"
        [[ -f "${sudo_config_path}${bak_suffix}" ]] || cp -fp "${sudo_config_path}" "${sudo_config_path}${bak_suffix}"

        # disable sudo su - / sudo su root
        if [[ "${pack_manager}" == 'apt-get' ]]; then
            sed -r -i 's@#*[[:space:]]*(%sudo[[:space:]]+ALL=\(ALL:ALL\)[[:space:]]+ALL)@# \1@;/%sudo ALL=NOPASSWD:ALL/d;/group sudo/a %sudo ALL=NOPASSWD:ALL,!/bin/su' "${sudo_config_path}"
        else
            sed -r -i 's@#*[[:space:]]*(%wheel[[:space:]]+ALL=\(ALL\)[[:space:]]+ALL)@# \1@;s@#*[[:space:]]*(%wheel[[:space:]]+ALL=\(ALL\)[[:space:]]+NOPASSWD: ALL).*@\1,!/bin/su@' "${sudo_config_path}"
        fi
        fnBase_OperationProcedureResult "${sudo_config_path}"
    fi

    # - Normal user add/configuration
    # pwck - verify integrity of password files
    # grpck - verify integrity of group files
    if [[ -n "${username_specify}" ]]; then
        fnBase_OperationProcedureStatement "Normal user"

        # Debian/Ubuntu: sudo      RHEL/OpenSUSE: wheel
        local sudo_group_name=${sudo_group_name:-'wheel'}
        [[ "${pack_manager}" == 'apt-get' ]] && sudo_group_name='sudo'

        if [[ -z $(sed -r -n '/^'"${username_specify}"':/{s@^([^:]+):.*@\1@g;p}' /etc/passwd) ]]; then
            # type 1 - create new user and add it into group wheel/sudo
            local l_login_shell=${l_login_shell:-'/bin/bash'}
            if [[ "${sudo_mode_enable}" -eq 1 ]]; then
                useradd -mN -G "${sudo_group_name}" "${username_specify}" &> /dev/null
            else
                useradd -mN "${username_specify}" &> /dev/null
            fi
            # change login shell
            usermod -s "${l_login_shell}" "${username_specify}" &> /dev/null

            # user: 'root', passwd: 'Root@2018'
            local new_password
            new_password="${username_specify^}@$(date +'%Y')"

            # Debian/SUSE not support --stdin
            case "${pack_manager}" in
                # https://debian-administration.org/article/668/Changing_a_users_password_inside_a_script
                apt-get ) echo "${username_specify}:${new_password}" | chpasswd &> /dev/null ;;
                dnf|yum ) echo "${new_password}" | passwd --stdin "${username_specify}" &> /dev/null ;;
                # https://stackoverflow.com/questions/27837674/changing-a-linux-password-via-script#answer-27837785
                zypper ) echo -e "${new_password}\n${new_password}" | passwd "${username_specify}" &> /dev/null ;;
            esac

            # setting user password expired date
            passwd -n "${pass_change_minday}" -x "${pass_change_maxday}" -w "${pass_change_warnningday}" "${username_specify}"  &> /dev/null
            chage -d0 "${username_specify}" &> /dev/null  # new created user have to change passwd when first login
        else
            # type 2 - user has been existed
            # gpasswd -a "${username_specify}"  "${sudo_group_name}" 1> /dev/null
            [[ "${sudo_mode_enable}" -eq 1 ]] && usermod -a -G "${sudo_group_name}" "${username_specify}" 2> /dev/null
            local user_if_existed=1
        fi

        if [[ "${sudo_mode_enable}" -eq 1 ]]; then
            if [[ "${user_if_existed}" -eq 1 ]]; then
                fnBase_OperationProcedureResult "${username_specify} (existed) ∈ ${sudo_group_name}"
            else
                fnBase_OperationProcedureResult "${username_specify} ∈ ${sudo_group_name}, initial password ${new_password}"
            fi
        else
            if [[ "${user_if_existed}" -eq 1 ]]; then
                fnBase_OperationProcedureResult "${username_specify} (existed)"
            else
                fnBase_OperationProcedureResult "${username_specify}, initial password ${new_password}"
            fi
        fi

    fi

    # - Erase last logined in user info
    # last, lastb - show a listing of last logged in users
    # # for last
    # local l_log_wtmp=${l_log_wtmp:-'/var/log/wtmp'}
    # if [[ -s "${l_log_wtmp}" ]]; then
    #     echo '' > "${l_log_wtmp}"
    # else
    #     touch "${l_log_wtmp}"
    # fi
    # # fot lasdb
    # local l_log_btmp=${l_log_btmp:-'/var/log/btmp'}
    # if [[ -s "${l_log_btmp}" ]]; then
    #     echo '' > "${l_log_btmp}"
    # else
    #     touch "${l_log_btmp}"
    # fi

}


#########  2-6. Essential/Administration Packages Installation  #########
fn_PackageOperationProcedureStatement(){
    local l_action="${1:-'install'}"
    local l_item="${2:-}"    # package name
    local l_command="${3:-}"    # command name
    local l_comment="${4:-}"    # comment info
    if [[ -n "${l_action}" && -n "${l_item}" ]]; then
        local l_punctuation='+'
        [[ "${l_action}" == 'install' ]] || l_punctuation='-'
        fnBase_OperationProcedureStatement "package ${l_punctuation} ${c_bold}${c_yellow}${l_item}${c_normal}"
        fnBase_PackageManagerOperation "${l_action}" "${l_item}"
        # 0 is ok, 1 is fail
        local l_status=${l_status:-0}
        if [[ -n "${l_command}" ]]; then
            fnBase_CommandExistIfCheck "${l_command}" || l_status=1
        fi
        fnBase_OperationProcedureResult "${l_comment}" "${l_status}"
    fi
}

fn_VimTextEditorOperation(){
    fnBase_OperationProcedureStatement 'VIM text editor'
    if ! fnBase_CommandExistIfCheck 'vim'; then
        local vim_pack_name=${vim_pack_name:-'vim'}
        case "${pack_manager}" in
            dnf|yum ) vim_pack_name='vim-enhanced' ;;
        esac
        fnBase_PackageManagerOperation 'install' "${vim_pack_name}"
    fi

    local l_vim_config=${l_vim_config:-'/etc/vimrc'}
    [[ -f '/etc/vim/vimrc' ]] && l_vim_config='/etc/vim/vimrc'

    if [[ -f "${l_vim_config}" ]]; then
        [[ -f "${l_vim_config}${bak_suffix}" ]] || cp -fp "${l_vim_config}" "${l_vim_config}${bak_suffix}"
        sed -i -r '/custom configuration start/,/custom configuration end/d' "${l_vim_config}"
        $download_method "${vim_url}" >> "${l_vim_config}"
    fi
    # https://www.cyberciti.biz/faq/vim-vi-text-editor-save-file-without-root-permission/
    # :w !sudo tee %
    # command W :execute ':silent w !sudo tee % > /dev/null' | :edit!

    # vim cut&paste not working in Stretch / Debian 9
    # https://unix.stackexchange.com/questions/318824/vim-cutpaste-not-working-in-stretch-debian-9
    # set mouse-=a
    if [[ "${distro_name}" == 'debian' && "${codename}" == 'stretch' ]]; then
        local vim_defaults=${vim_defaults:-'/usr/share/vim/vim80/defaults.vim'}
        [[ -s "${vim_defaults}" ]] && sed -i -r "/^if has\('mouse'\)/,+2{s@^@\"@g}" "${vim_defaults}"
    fi
    fnBase_OperationProcedureResult
}

fn_EssentialPackInstallation(){
    fnBase_OperationPhaseStatement "Essential Packages Installation"

    if [[ "${pack_manager}" == 'apt-get' ]]; then
        fnBase_CommandExistIfCheck 'sysv-rc-conf' || fn_PackageOperationProcedureStatement 'install' "sysv-rc-conf" # same to chkconfig, Debian Jessie need to install

        fnBase_CommandExistIfCheck 'apt-show-versions' || fn_PackageOperationProcedureStatement 'install' 'apt-show-versions debsums'

        # https://github.com/koalaman/shellcheck/wiki/SC2143
        if [[ "${distro_name}" != 'ubuntu' ]]; then
            # For Debian
            if dpkg --list 2> /dev/null | grep -q 'firmware-linux-nonfree'; then
                fn_PackageOperationProcedureStatement 'install' 'firmware-linux-nonfree'
            fi
        fi

        # resolvconf - name server information handler
        if ! fnBase_CommandExistIfCheck 'resolvconf'; then
            fnBase_PackageManagerOperation 'install' 'resolvconf'

            # Ubuntu Xenial 16.04: if enable SELinux (mode 0 or 1), installed NetworkManager, file '/run/resolvconf/resolv.conf' (see from system log /var/log/syslog) will be eraed after system reboot which makes internet connection fail. If restart service 'resolvconf.service', it will be fixed until next system reboot.
            # https://askubuntu.com/questions/812714/run-resolvconf-resolv-conf-gets-erased-each-time-i-restart-and-i-cant-use-int
            # https://unix.stackexchange.com/questions/174349/what-overwrites-etc-resolv-conf-on-every-boot
            # https://willhaley.com/blog/resolvconf-dns-issue-after-ubuntu-xenial-upgrade/

            # https://duckduckgo.com/?q=public+dns&ia=answer&iax=answer
            # Google DNS: 8.8.8.8 8.8.4.4
            # CloudFlare DNS: 1.1.1.1 1.0.0.1
            # Dyn DNS: 216.146.35.35 216.146.36.36
            # Comodo Secure DNS: 8.26.56.26 8.20.247.20
            # Yandex.DNS: 77.88.8.88 77.88.8.2
            # OpenDNS: 208.67.222.222 208.67.220.220
            # Free DNS: 37.235.1.174 37.235.1.177
            [[ -s /etc/resolvconf/resolv.conf.d/base ]] && echo -e "# Google DNS\nnameserver 8.8.8.8\n# Dyn DNS\nnameserver 216.146.35.35\n# CloudFlare DNS\nnameserver 1.1.1.1" >  /etc/resolvconf/resolv.conf.d/base

            # sudo dpkg-reconfigure resolvconf
            if fnBase_CommandExistIfCheck 'resolvconf'; then
                resolvconf --enable-updates &> /dev/null
                resolvconf -u &> /dev/null
            fi
        fi
    fi

    [[ -s '/usr/share/bash-completion/bash_completion' || -s '/etc/profile.d/bash_completion.sh' ]] || fn_PackageOperationProcedureStatement 'install' "bash-completion"

    # Gnome
    if fnBase_CommandExistIfCheck 'gnome-shell'; then
        fnBase_CommandExistIfCheck 'gnome-tweaks' || fn_PackageOperationProcedureStatement 'install' 'gnome-tweak-tool'

        # https://extensions.gnome.org/extension/6/applications-menu/
        # fnBase_CommandExistIfCheck 'gnome-shell-extension-tool'
        fn_PackageOperationProcedureStatement 'install' 'gnome-shell-extensions'

        # fnBase_CommandExistIfCheck 'dconf-editor' || fn_PackageOperationProcedureStatement 'install' 'dconf-editor'
    fi

    # man - an interface to the on-line reference manuals
    fnBase_CommandExistIfCheck 'man' || fn_PackageOperationProcedureStatement 'install' "man"

    # curl - command line tool for transferring data with URL syntax
    fnBase_CommandExistIfCheck 'curl' || fn_PackageOperationProcedureStatement 'install' "curl"

    # gawk - pattern scanning and processing language
    fnBase_CommandExistIfCheck 'gawk' || fn_PackageOperationProcedureStatement 'install' "gawk"

    # https://en.wikipedia.org/wiki/Util-linux
    # util-linux is a standard package distributed by the Linux Kernel Organization for use as part of the Linux operating system.
    # lscpu, lsblk, ...
    local l_util_linux=${l_util_linux:-'util-linux'}
    case "${pack_manager}" in
        yum|dnf ) l_util_linux='util-linux-ng' ;;
    esac
    fnBase_CommandExistIfCheck 'lsblk' || fn_PackageOperationProcedureStatement 'install' "${l_util_linux}" 'lsblk'

    # gnupg2 - GNU privacy guard - a free PGP replacement (dummy transitional package)
    local l_gnupg2_name=${l_gnupg2_name:-'gnupg2'}
    fnBase_CommandExistIfCheck 'gpg2' || fn_PackageOperationProcedureStatement 'install' "${l_gnupg2_name}" 'gpg2' 'gpg2'
    # dirmngr is used for network access by gpg, gpgsm, and dirmngr-client, among other tools.
    # rhel 7 / amzn2 has no dirmngr
    # https://pkgs.org/download/dirmngr
    fnBase_CommandExistIfCheck 'dirmngr' || fn_PackageOperationProcedureStatement 'install' 'dirmngr' 'dirmngr' 'dirmngr'

    # Vim Text Editor
    fn_VimTextEditorOperation

    # - Haveged Installation for random num generation
    local rng_config_path=${rng_config_path:-'/etc/default/rng-tools'}
    if [[ ! -f "${rng_config_path}" ]]; then
        fn_PackageOperationProcedureStatement 'install' "rng-tools haveged"
        if [[ -f "${rng_config_path}" ]]; then
            [[ -f "${rng_config_path}${bak_suffix}" ]] || cp -fp "${rng_config_path}" "${rng_config_path}${bak_suffix}"
            sed -i -r '/^HRNGDEVICE/d;/#HRNGDEVICE=\/dev\/null/a HRNGDEVICE=/dev/urandom' "${rng_config_path}"
        fi
    fi

    # - Chrony
    case "${pack_manager}" in
        apt-get )
            # https://github.com/koalaman/shellcheck/wiki/SC2143
            if dpkg --list | grep -q 'ntp'; then
                fn_PackageOperationProcedureStatement 'remove' "ntp"
            fi

            if dpkg --list 2> /dev/null | grep -q 'chrony'; then
                fn_PackageOperationProcedureStatement 'install' "chrony"
                fnBase_SystemServiceManagement 'chrony' 'enable'
            fi
            ;;
        dnf|yum )
            # $(rpm -qa | awk -F- 'match($1,/^ntp$/){print $1}')
            [[ -n $(rpm -qa 2> /dev/null | sed -r -n '/^ntp-/{s@^([^-]+).*@\1@g;p}') ]] && fn_PackageOperationProcedureStatement "remove" "ntp"

            if [[ -z $(rpm -qa 2> /dev/null | sed -r -n '/^chrony-/{s@^([^-]+).*@\1@g;p}') ]]; then
                fn_PackageOperationProcedureStatement 'install' "chrony"
                fnBase_SystemServiceManagement 'chronyd' 'enable'
            fi
            ;;
        zypper )
            # 'zypper packages -i' consumes more time then 'rpm -qa'
            # $(zypper packages -i | awk -F\| 'match($3,/^[[:space:]]*ntp[[:space:]]*$/){print}')
            [[ -n $(rpm -qa 2> /dev/null | sed -r -n '/^ntp-/{s@^([^-]+).*@\1@g;p}') ]] && fn_PackageOperationProcedureStatement "remove" "ntp"

            if [[ -z $(rpm -qa 2> /dev/null | sed -r -n '/^chrony-/{s@^([^-]+).*@\1@g;p}') ]]; then
                fn_PackageOperationProcedureStatement 'install' "chrony"
                [[ -f '/etc/ntp.conf.rpmsave' ]] && rm -f '/etc/ntp.conf.rpmsave'
                fnBase_SystemServiceManagement 'chronyd' 'enable'
            fi
            ;;
    esac

    # - Filesystem
    # mlocate: locate   /etc/cron.daily/mlocate
    fnBase_CommandExistIfCheck 'locate' || fn_PackageOperationProcedureStatement 'install' 'mlocate'
    fnBase_CommandExistIfCheck 'tree' || fn_PackageOperationProcedureStatement 'install' 'tree'

    #  - Compress & Decompress
    # https://www.2daygeek.com/zip-unzip-command-usage-in-linux-unix/
    fnBase_CommandExistIfCheck 'zip' || fn_PackageOperationProcedureStatement 'install' 'zip unzip'
    fnBase_CommandExistIfCheck 'tar' || fn_PackageOperationProcedureStatement 'install' 'tar'
    # .tar.gz
    fnBase_CommandExistIfCheck 'gzip' || fn_PackageOperationProcedureStatement 'install' "gzip"
    # .tar.bz2
    fnBase_CommandExistIfCheck 'bzip2' || fn_PackageOperationProcedureStatement 'install' "bzip2"
    # .tar.xz
    local l_xz_name=${l_xz_name:-'xz'}
    [[ "${pack_manager}" == 'apt-get' ]] && l_xz_name='xz-utils'
    fnBase_CommandExistIfCheck 'xz' || fn_PackageOperationProcedureStatement 'install' "${l_xz_name}" 'xz'

    # - Network
    # iproute: ip, ss, bridge, rtacct, rtmon, tc, ctstat, lnstat, nstat, routef, routel, rtstat, tipc, arpd and devlink
    if ! fnBase_CommandExistIfCheck 'ss'; then
        local l_iproute_name=${l_iproute_name:-'iproute'}
        case "${pack_manager}" in
            apt-get|zypper ) l_iproute_name='iproute2' ;;
            dnf|yum ) l_iproute_name='iproute' ;;
        esac
        fn_PackageOperationProcedureStatement 'install' "${l_iproute_name}" 'ss'
    fi

    # lsof - list open files
    fnBase_CommandExistIfCheck 'lsof' || fn_PackageOperationProcedureStatement 'install' 'lsof' 'lsof'

    # dnsutils/bind-utils: dig nslookup nsupdate
    if ! fnBase_CommandExistIfCheck 'dig'; then
        local dns_utils=${dns_utils:-'dnsutils'}
        [[ "${pack_manager}" != 'apt-get' ]] && dns_utils='bind-utils'
        fn_PackageOperationProcedureStatement 'install' "${dns_utils}" 'dig' 'dig'
    fi

    # ☆ nc - TCP/IP swiss army knife
    # netcat - TCP/IP swiss army knife -- transitional package
    # netcat-traditional - TCP/IP swiss army knife
    # netcat-openbsd - TCP/IP swiss army knife
    # In Debian, netcat-openbsd need to be installed manually
    local l_netcat_packs=${l_netcat_packs:-}
    case "${pack_manager}" in
        apt-get ) l_netcat_packs='netcat netcat-openbsd' ;;
        yum|dnf )
            # CentOS 6 - nc
            if fnBase_CommandExistIfCheck 'systemctl'; then
                l_netcat_packs='nmap-ncat'
            else
                l_netcat_packs='nc'
            fi
            ;;
        zypper ) l_netcat_packs='netcat-openbsd' ;;
    esac
    fnBase_CommandExistIfCheck 'nc' || fn_PackageOperationProcedureStatement 'install' "${l_netcat_packs}" 'nc'

    # - parallel - build and execute command lines from standard input in parallel
    if ! fnBase_CommandExistIfCheck 'parallel'; then
        fnBase_OperationProcedureStatement 'Parallel computing'
        local parallel_name=${parallel_name:-'parallel'}
        # SLES repo in AWK has no parallel utility
        [[ "${pack_manager}" == 'zypper' ]] && parallel_name='gnu_parallel'
        fnBase_PackageManagerOperation 'install' "${parallel_name}"
        # sudo parallel --bibtex ==> Type: 'will cite' and press enter.
        if fnBase_CommandExistIfCheck 'parallel'; then
            # echo 'will cite' > parallel --bibtex &> /dev/null
            echo 'will cite' | parallel --bibtex &> /dev/null
            fnBase_OperationProcedureResult "${parallel_name}"
        else
            fnBase_OperationProcedureResult "${parallel_name}" 1
        fi
    fi

    # - Team collaboration
    # tmate - terminal multiplexer with instant terminal sharing
    # https://www.2daygeek.com/tmate-instantly-share-your-terminal-session-to-anyone-in-seconds/
    # tmate show-messages
    # fnBase_CommandExistIfCheck 'tmate' || fnBase_PackageManagerOperation 'install' 'tmate' 'tmate'

    # screen - screen manager with VT100/ANSI terminal emulation
    # Ctrl+A  +  d       detach the session
    # screen -r          Reattach to a detached screen process
    # screen -ls         just list possible matches
    fnBase_CommandExistIfCheck 'screen' || fnBase_PackageManagerOperation 'install' 'screen' 'screen'
}

fn_MegaCliInstallation(){
    # Install MegaCli For Symbios Logic MegaRAID Controller

    local l_pci_raid_info=${l_pci_raid_info:-}
    l_pci_raid_info=$(lspci | sed -r -n '/RAID/p')
    # Dell R720 | 03:00.0 RAID bus controller: LSI Logic / Symbios Logic MegaRAID SAS 2008 [Falcon] (rev 03)
    # Dell R730 | 03:00.0 RAID bus controller: LSI Logic / Symbios Logic MegaRAID SAS-3 3108 [Invader] (rev 02)

    # local l_product_manufacturer=${l_product_manufacturer:-}    # Dell Inc.
    # [[ -s '/sys/devices/virtual/dmi/id/sys_vendor' ]] && l_product_manufacturer=$(cat /sys/devices/virtual/dmi/id/sys_vendor)
    # "${l_product_manufacturer}" =~ Dell
    if [[ "${l_pci_raid_info}" =~ MegaRAID ]]; then
        local l_megacli_dir=${l_megacli_dir:-'/opt/MegaRAID/MegaCli'}
        local l_megacli_path=${l_megacli_path:-"${l_megacli_dir}/MegaCli64"}
        if [[ ! (-d "${l_megacli_dir}" && -x "${l_megacli_path}") ]]; then
            fnBase_OperationProcedureStatement 'MegaCLI utility'
            [[ -d "${l_megacli_dir}" ]] && rm -rf "${l_megacli_dir}"
            [[ -d "${l_megacli_dir}" ]] || mkdir -p "${l_megacli_dir}"
            local l_megacli_link='https://gitlab.com/MaxdSre/axd-ShellScript/raw/master/utilities/MegaCLI/MegaCli-8.07.14-1.noarch.tar.xz'
            megacli_save_path=${megacli_save_path:-"/tmp/${l_megacli_link##*/}"}
            $download_method "${l_megacli_link}" > "${megacli_save_path}"
            tar xf "${megacli_save_path}" -C "${l_megacli_dir}" --strip-components=1 2> /dev/null
            [[ -s "${l_megacli_dir}"/libstorelibir-2.so.14.07-0 ]] && ln -fs "${l_megacli_dir}"/libstorelibir-2.so.14.07-0 "${l_megacli_dir}"/libstorelibir-2.so 2> /dev/null
            [[ -f "${megacli_save_path}" ]] && rm -f "${megacli_save_path}"

            # MegaCli not providing all the information we need like mapping to linux devices and raid level (readable), so we are going to use some extra tools.
            fnBase_PackageManagerOperation 'install' 'sg3-utils'
            fnBase_OperationProcedureResult "${l_megacli_dir}"
        fi
    fi
}

fn_AdministrationPackInstallation(){
    fnBase_OperationPhaseStatement "Administration Packages Installation"
    # https://www.cyberciti.biz/tips/top-linux-monitoring-tools.html

    # - Temperature
    # fancontrol - utility to control the fan speed
    # lm-sensors - utilities to read temperature/voltage/fan sensors
    # psensor - display graphs for monitoring hardware temperature
    if ! fnBase_CommandExistIfCheck 'sensors'; then
        fnBase_OperationProcedureStatement 'Print sensors information'

        local l_sensor_name=${l_sensor_name:-'lm_sensors'}
        case "${pack_manager}" in
            apt-get ) l_sensor_name='lm-sensors' ;;
            zypper ) l_sensor_name='sensors' ;;
        esac

        fnBase_PackageManagerOperation 'install' "${l_sensor_name}"
        fnBase_CommandExistIfCheck 'sensors-detect' && sensors-detect --auto &> /dev/null
        fnBase_SystemServiceManagement 'lm-sensors' 'disable'
        if fnBase_CommandExistIfCheck 'sensors'; then
            fnBase_OperationProcedureResult 'sensors'
        else
            fnBase_OperationProcedureResult 'sensors' '1'
        fi
    fi

    # - PCI devices detection
    # lspci - list all PCI devices    /usr/share/misc/pci.ids
    fnBase_OperationProcedureStatement "list all PCI devices"
    local l_lspci_name=${l_lspci_name:-'pciutils'}
    fnBase_CommandExistIfCheck 'lspci' || fnBase_PackageManagerOperation 'install' "${l_lspci_name}" 'lspci'
    # update-pciids - download new version of the PCI ID list
    # This utility requires curl, wget or lynx to be installed. If gzip or bzip2 are available, it automatically downloads the compressed version of the list.
    # fnBase_CommandExistIfCheck 'update-pciids' && update-pciids -q 2> /dev/null
    fnBase_OperationProcedureResult "${l_lspci_name}"

    # LSI's MegaCLI Utility For MegaRAID Controllers
    fn_MegaCliInstallation

    # - parallel computing
    # pssh - Parallel versions of SSH-based tools
    # https://www.virtualconfusion.net/articles/use-pssh-to-manage-several-servers
    # local l_pssh_name='pssh'
    # [[ "${pack_manager}" == 'apt-get' ]] && l_pssh_name='parallel-ssh'
    # if ! fnBase_CommandExistIfCheck "${l_pssh_name}"; then
    #     fnBase_OperationProcedureStatement "parallel SSH tools"
    #     fnBase_PackageManagerOperation 'install' 'pssh'
    #     if fnBase_CommandExistIfCheck "${l_pssh_name}"; then
    #         fnBase_OperationProcedureResult "${l_pssh_name}"
    #     else
    #         fnBase_OperationProcedureResult "${l_pssh_name}" 1
    #     fi
    # fi

    # pdsh - issue commands to groups of hosts in parallel
    fnBase_CommandExistIfCheck 'pdsh' || fn_PackageOperationProcedureStatement 'install' 'pdsh' 'pdsh'

    # ☆ sysstat - Collection of performance monitoring tools for Linux
    # The sysstat package contains the following system performance tools:
    # - sar: collects and reports system activity information;
    # - iostat: reports CPU utilization and disk I/O statistics;
    # - tapestat: reports statistics for tapes connected to the system;
    # - mpstat: reports global and per-processor statistics;
    # - pidstat: reports statistics for Linux tasks (processes);
    # - sadf: displays data collected by sar in various formats;
    # - cifsiostat: reports I/O statistics for CIFS filesystems.
    #
    # The statistics reported by sar deal with I/O transfer rates, paging activity, process-related activities, interrupts, network activity, memory and swap space utilization, CPU utilization, kernel activities and TTY statistics, among others. Both UP and SMP machines are fully supported.

    # - read log in /var/log/sa/
    # sar -f /var/log/sa/sa04
    # - see all statistics
    # sar -A
    # mpstat -P ALL

    if ! fnBase_CommandExistIfCheck 'mpstat'; then
        fnBase_OperationProcedureStatement 'sysstat utility'
        fnBase_PackageManagerOperation 'install' 'sysstat'

        local l_sysstat_path='/etc/sysconfig/sysstat'
        [[ -f "${l_sysstat_path}" ]] || l_sysstat_path='/etc/sysstat/sysstat'
        if [[ -s "${l_sysstat_path}" ]]; then
            sed -r -i '/^#?[[:space:]]*HISTORY=/{s@^#?[[:space:]]*([^=]+=).*$@\128@g}' "${l_sysstat_path}"
            sed -r -i '/^#?[[:space:]]*REPORTS=/{s@^#?[[:space:]]*([^=]+=).*$@\1true@g}' "${l_sysstat_path}"
        fi

        # https://www.crybit.com/sysstat-sar-on-ubuntu-debian/
        # https://www.server-world.info/en/note?os=Ubuntu_16.04&p=sysstat
        if [[ "${pack_manager}" == 'apt-get' ]]; then
            local l_default_sysstat='/etc/default/sysstat'
            [[ -s "${l_default_sysstat}" ]] && sed -r -i '/^ENABLED=/{s@^([^"]+").*(")$@\1true\2@g}' "${l_default_sysstat}"

            local l_cron_sysstat='/etc/cron.d/sysstat'
            # Generate a daily summary of process accounting at 23:53
            # 53 23 * * * root command -v sa2 > /dev/null && sa2 -A
            if [[ -s "${l_cron_sysstat}" ]]; then
                sed -r -i '/Generate a daily summary/,+1{d}' "${l_cron_sysstat}"
                sed -r -i '$a # Generate a daily summary of process accounting at 23:53\n53 23 * * * root command -v sa2 > /dev/null && sa2 -A' "${l_cron_sysstat}"
            fi
        fi

        local l_sysstat_default=${l_sysstat_default:-'/etc/default/sysstat'}
        if [[ -f "${l_sysstat_default}" ]]; then
            sed -r -i '/^ENABLED=/{s@^([^=]+=).*@\1"true"@g;}' "${l_sysstat_default}"
        fi
        fnBase_SystemServiceManagement 'sysstat' 'enable'
        fnBase_SystemServiceManagement 'sysstat' 'restart'
        fnBase_OperationProcedureResult 'sar/iostat/mpstat/pidstat'
    fi

    # ioping - Simple disk I/O latency measuring tool
    fnBase_CommandExistIfCheck 'ioping' || fn_PackageOperationProcedureStatement 'install' 'ioping' 'ioping'

    # ☆ nmon - systems administrator, tuner, benchmark tool.
    fnBase_CommandExistIfCheck 'nmon' || fn_PackageOperationProcedureStatement 'install' 'nmon' 'nmon'

    # ☆ glances - CLI curses based monitoring tool
    fnBase_CommandExistIfCheck 'glances' || fn_PackageOperationProcedureStatement 'install' 'glances'

    # - Hardware Detection
    # lshw - list hardware   not exist in SLES's repository
    # usage:  lshw -class disk / lshw -class disk -class storage / lshw -short -C disk
    fnBase_CommandExistIfCheck 'lshw' || fn_PackageOperationProcedureStatement 'install' 'lshw' 'lshw'

    # lsscsi - list SCSI devices (or hosts) and their attributes
    fnBase_CommandExistIfCheck 'lsscsi' || fn_PackageOperationProcedureStatement 'install' 'lsscsi' 'lsscsi'

    # lsusb - list USB devices
    local l_lsusb_name=${l_lspci_name:='usbutils'}
    fnBase_CommandExistIfCheck 'lsscsi' || fn_PackageOperationProcedureStatement 'install' "${l_lsusb_name}" 'lsscsi'

    # dmidecode - DMI table decoder
    fnBase_CommandExistIfCheck 'dmidecode' || fn_PackageOperationProcedureStatement 'install' 'dmidecode' 'dmidecode'

    # hwinfo - probe for hardware
    # fnBase_CommandExistIfCheck 'hwinfo' || fn_PackageOperationProcedureStatement 'install' 'hwinfo' 'hwinfo'

    # - Disk Check
    # ☆ smartctl - Control and Monitor Utility for SMART Disks
    # smartctl -i /dev/sda / smartctl -a /dev/sda / smartctl -H /dev/sda
    if ! fnBase_CommandExistIfCheck 'smartctl'; then
        local l_smartctl_name=${l_smartctl_name:-'smartmontools'}
        fn_PackageOperationProcedureStatement 'install' "${l_smartctl_name}" 'smartctl' 'smartctl'
        fnBase_SystemServiceManagement 'smartd' 'disable'
    fi

    # - Disk I/O
    # ☆ dstat: a versatile replacement for vmstat, iostat and ifstat.
    # versatile tool for generating system resource statistics
    fnBase_CommandExistIfCheck 'dstat' || fn_PackageOperationProcedureStatement 'install' 'dstat' 'dstat'
    # iotop - simple top-like I/O monitor
    fnBase_CommandExistIfCheck 'iotop' || fn_PackageOperationProcedureStatement 'install' 'iotop' 'iotop'

    # - Network traffic monitor
    # vnstat - a console-based network traffic monitor
    # vnstati - png image output support for vnStat
    # vnstat --create -i enp0s25
    if ! fnBase_CommandExistIfCheck 'vnstat'; then
        # https://www.cyberciti.biz/faq/centos-redhat-fedora-linux-install-vnstat-bandwidth-monitor/
        fnBase_OperationProcedureStatement 'Network traffic monitor'
        local l_vnstat_list='vnstat'
        [[ "${pack_manager}" == 'apt-get' ]] && l_vnstat_list="${l_vnstat_list} vnstati"
        fnBase_PackageManagerOperation 'install' "${l_vnstat_list}"
        fnBase_SystemServiceManagement 'vnstat' 'enable'
        # conf path - /etc/vnstat.conf
        # log dir - /var/lib/vnstat/
        # vnstati -s/-h -i eth0 -o /tmp/network-log.png
        fnBase_OperationProcedureResult 'vnstat'
    fi

    # nmap - Network exploration tool and security / port scanner
    fnBase_CommandExistIfCheck 'nmap' || fn_PackageOperationProcedureStatement 'install' 'nmap' 'nmap'

    # ☆ iptraf-ng - Interactive Colorful IP LAN Monitor
    fnBase_CommandExistIfCheck 'iptraf-ng' || fn_PackageOperationProcedureStatement 'install' 'iptraf-ng' 'iptraf-ng'
    # iftop - displays bandwidth usage information on an network interface
    fnBase_CommandExistIfCheck 'iftop' || fn_PackageOperationProcedureStatement 'install' 'iftop' 'iftop'
    # ☆ tcpdump - dump traffic on a network
    fnBase_CommandExistIfCheck 'tcpdump' || fn_PackageOperationProcedureStatement 'install' 'tcpdump' 'tcpdump'
    # whois - Searches for an object in a RFC 3912 database.
    fnBase_CommandExistIfCheck 'whois' || fn_PackageOperationProcedureStatement 'install' 'whois' 'whois'
    # hping3 - Active Network Smashing Tool
    local hping_name=${hping_name:-'hping3'}
    [[ "${distro_name}" == 'opensuse' ]] && hping_name='hping'
    fnBase_CommandExistIfCheck "${hping_name}" || fn_PackageOperationProcedureStatement 'install' "${hping_name}" "${hping_name}"

    # traceroute - Traces the route taken by packets over an IPv4/IPv6 network
    fnBase_CommandExistIfCheck 'traceroute' || fn_PackageOperationProcedureStatement 'install' 'traceroute' 'traceroute'
    # ☆ mtr - a network diagnostic tool
    fnBase_CommandExistIfCheck 'mtr' || fn_PackageOperationProcedureStatement 'install' 'mtr' 'mtr'

    # nmcli - Network Manager Command Line Interface
    # https://docs.ubuntu.com/core/en/stacks/network/network-manager/docs/
    # NetworkManager stores all network configuration as "connections", which are collections of data (Layer2 details, IP addressing, etc.) that describe how to create or connect to a  network. A connection is "active" when a device uses that connection\'s configuration to create or connect to a network. There may be multiple connections that apply to a device, but only one of them can be active on that device at any given time. The additional connections can be used to allow quick switching between different networks and configurations.

    # nmcli general status
    # nmcli dev status
    # nmcli con/connection show [-a]
    # nmcli con add type ethernet con-name ${NAME_OF_CONNECTION} ifname ${interface-name} ip4 ${IP_ADDRESS} gw4 ${GW_ADDRESS}
    # nmcli con add type ethernet con-name static2 ifname enp0s3 ip4 192.168.1.50/24 gw4 192.168.1.1
    # nmcli con mod static2 ipv4.dns “8.8.8.8 8.8.4.4”
    # nmcli con down static1 ; nmcli con up static2
    # https://www.tecmint.com/configure-network-connections-using-nmcli-tool-in-linux/
    # local nmcli_pack_name='NetworkManager'
    # [[ "${pack_manager}" == 'apt-get' ]] && nmcli_pack_name='network-manager'
    # fnBase_CommandExistIfCheck 'nmcli' || fn_PackageOperationProcedureStatement 'install' "${nmcli_pack_name}" 'nmcli' 'nmcli'

    # - Process /proc
    # ☆ htop - interactive process viewer
    # fnBase_CommandExistIfCheck 'htop' || fn_PackageOperationProcedureStatement 'install' 'htop' 'htop'
    # ☆ atop - Advanced System & Process Monitor
    fnBase_CommandExistIfCheck 'atop' || fn_PackageOperationProcedureStatement 'install' 'atop' 'atop'
    # psmisc: pstree/prtstat/peekfd/killall/fuser
    fnBase_CommandExistIfCheck 'pstree' || fn_PackageOperationProcedureStatement 'install' 'psmisc' 'pstree'

    # procps: free, kill, pkill, pgrep, pmap, ps, pwdx, skill, slabtop, snice, sysctl, tload, top, uptime, vmstat, w, and watch.
    # pmap: reports the memory map of a process or processes
    # pmap -d 6382
    if ! fnBase_CommandExistIfCheck 'pkill'; then
        local procps_name=${procps_name:-'procps'}
        [[ "${pack_manager}" == 'dnf' ]] && procps_name='procps-ng'
        fn_PackageOperationProcedureStatement 'install' "${procps_name}" 'pkill'
    fi

    # - System call
    # strace - trace system calls and signals
    # https://www.tecmint.com/strace-commands-for-troubleshooting-and-debugging-linux/
    fnBase_CommandExistIfCheck 'strace' || fn_PackageOperationProcedureStatement 'install' 'strace' 'strace'

    # - Secure remove
    local l_srm_name=${l_srm_name:-'srm'}
    [[ "${pack_manager}" == 'apt-get' ]] && l_srm_name='secure-delete'
    fnBase_CommandExistIfCheck 'srm' || fn_PackageOperationProcedureStatement 'install' "${l_srm_name}" 'srm' 'srm'
}

fn_ContainerInstallation(){
    if [[ "${container_install}" -eq 1 ]]; then
        case "${distro_name,,}" in
            centos|fedora|debian|ubuntu)
                fnBase_OperationPhaseStatement "Container Installation"
                # https://docs.docker.com/install/#supported-platforms
                fnBase_OperationProcedureStatement 'Docker CE'

                $download_method "${docker_script}" | bash -s -- &> /dev/null

                if fnBase_CommandExistIfCheck 'docker'; then
                    fnBase_OperationProcedureResult "$(docker --version | sed -r -n '/version/{s@.*version[[:space:]]+([[:digit:].]+).*$@\1@g;p}')"
                else
                    fnBase_OperationProcedureResult '' '1'
                fi
            ;;
        esac
    fi
}

#########  2-7. OpenSSH Configuration  #########
fn_OpenSSHPortSpecifiedVerification(){
    if [[ "${ssh_port_specify}" =~ ^[1-9]{1}[0-9]{1,4}$ && "${ssh_port_specify}" -ne "${ssh_port_default}" && "${ssh_port_specify}" -gt 0 && "${ssh_port_specify}" -le 65535 ]]; then
        local l_port_service_info
        l_port_service_info=$(sed -r -n '/^'"${ssh_port_specify}"'[[:space:]]+/{s@^([^[:space:]]+)[[:space:]]*([^\/]+).*@\1|\2@g;p}' /etc/services 2> /dev/null)
        [[ -n "${l_port_service_info}" ]] && ssh_port_specify=''
    fi

    if [[ -z "${ssh_port_specify}" ]]; then
        ssh_port_specify='d'
        install_ssh_server=0
    fi

    # 'd' is default port 22, 'r' is system ports (0,1023], 'n' is user ports [1024,32767], 'e' is regular ephemeral port [32768,49151], 'h' is high ephemeral port [49152,65535].
    case "${ssh_port_specify,,}" in
        d ) ssh_port_specify="${ssh_port_default}" ;;
        r|n|e|h ) ssh_port_specify=$($download_method "${port_generation_script}" | bash -s -- -t "${ssh_port_specify}" -s -j | sed -r -n 's@"@@g;s@.*port_no:([[:digit:]]+).*@\1@g;p') ;;
    esac
    [[ -z "${ssh_port_specify}" ]] && ssh_port_specify="${ssh_port_default}"
}

fn_OpenSSHDirectiveSetting(){
    local l_item="${1:-}"
    local l_val="${2:-}"
    # sshd_config directive lists
    local l_list="${3:-}"
    local l_path=${l_path:-'/etc/ssh/sshd_config'}

    local l_action=${l_action:-0}

    if [[ -n "${l_item}" && -n "${l_val}" && -s "${l_path}" ]]; then
        if [[ -s "${l_list}" ]]; then
            [[ -n $(sed -r -n '/^'"${l_item}"'$/p' "${l_list}" 2> /dev/null) ]] && l_action=1
        else
            l_action=1
        fi
    fi

    if [[ "${l_action}" -eq 1 ]]; then
        local l_record_origin=${l_record_origin:-}
        local l_record_origin_comment=${l_record_origin_comment:-}
        # if result has more then one line, use double quote "" wrap it
        # - whole line start with keyword, format 'PermitEmptyPasswords no'
        l_record_origin=$(sed -r -n '/^'"${l_item}"'[[:space:]]+/{s@[[:space:]]*$@@g;p}' "${l_path}" 2> /dev/null)

        if [[ -z "${l_record_origin}" ]]; then
            # - whole line include keyword start with "#", format '#PermitEmptyPasswords no'
            l_record_origin_comment=$(sed -r -n '/^#[[:space:]]*'"${l_item}"'[[:space:]]+/{s@[[:space:]]*$@@g;p}' "${l_path}" 2> /dev/null)

            if [[ -z "${l_record_origin_comment}" ]]; then
                # append at the end of file
                sed -i -r '$a '"${l_item} ${l_val}"'' "${l_path}" 2> /dev/null
            else
                # append at the end of the directive which is commented by #
                sed -i -r '/^#[[:space:]]*'"${l_item}"'[[:space:]]+/a '"${l_item} ${l_val}"'' "${l_path}" 2> /dev/null
                # remove commented line
                sed -i -r '/^#[[:space:]]*'"${l_item}"'[[:space:]]+/d' "${l_path}" 2> /dev/null
            fi
        else
            if [[ "${l_record_origin##* }" != "${l_val}" ]]; then
                if [[ "${l_val}" =~ @ ]]; then
                    sed -i -r '/^'"${l_item}"'[[:space:]]+/{s/.*/'"${l_item} ${l_val}"'/;}' "${l_path}" 2> /dev/null
                else
                    sed -i -r '/^'"${l_item}"'[[:space:]]+/{s@.*@'"${l_item} ${l_val}"'@;}' "${l_path}" 2> /dev/null
                fi
            fi
        fi
    fi
}

fn_OpenSSHInstallation(){
    fnBase_OperationProcedureStatement "Server & client utility"
    # - client side
    if ! fnBase_CommandExistIfCheck 'ssh'; then
        local ssh_client_pname=${ssh_client_pname:-}
        case "${pack_manager}" in
            apt-get ) ssh_client_pname='openssh-client' ;;
            dnf|yum ) ssh_client_pname='openssh-clients' ;;
            zypper ) ssh_client_pname='openssh' ;;
        esac
        fnBase_PackageManagerOperation 'install' "${ssh_client_pname}"
    fi    # ssh

    # - server side
    if [[ "${install_ssh_server}" -eq 1 ]]; then
        if ! fnBase_CommandExistIfCheck 'sshd'; then
            local ssh_server_pname=${ssh_server_pname:-'openssh-server'}
            [[ "${pack_manager}" == 'zypper' ]] && ssh_server_pname='openssh'
            fnBase_PackageManagerOperation 'install' "${ssh_server_pname}"

            local sshd_service_name=${sshd_service_name:-'sshd'}
            [[ "${pack_manager}" == 'apt-get' ]] && sshd_service_name='ssh'
            fnBase_CommandExistIfCheck 'sshd' && fnBase_SystemServiceManagement "${sshd_service_name}" 'enable'
        fi
    fi

    fnBase_OperationProcedureResult 'OpenSSH'
}

fn_OpenSSHConfiguration(){
    # https://linux-audit.com/audit-and-harden-your-ssh-configuration/
    # SSH configuration scanners https://linuxsecurity.expert/security-tools/ssh-configuratio

    # OpenSUSE merge client & service side into one package 'openssh'
    local l_ssh_config=${l_ssh_config:-'/etc/ssh/ssh_config'}
    local l_sshd_config=${l_sshd_config:-'/etc/ssh/sshd_config'}

    fnBase_OperationProcedureStatement "Directives configuration"
    [[ ! -f "${l_ssh_config}${bak_suffix}" && -f "${l_ssh_config}" ]] && cp -pf "${l_ssh_config}" "${l_ssh_config}${bak_suffix}"
    [[ ! -f "${l_sshd_config}${bak_suffix}" && -f "${l_sshd_config}" ]] && cp -pf "${l_sshd_config}" "${l_sshd_config}${bak_suffix}"

    # 7.2, 6.7, 5.3
    local ssh_version=${ssh_version:-0}
    ssh_version=$(ssh -V 2>&1 | sed -r -n 's@.*_([[:digit:].]{3}).*@\1@p')

    # sshd port detection & SELinux configuration
    if [[ -s "${l_sshd_config}" ]]; then
        ssh_port=$(sed -r -n '/^Port/s@^Port[[:space:]]*(.*)@\1@p' "${l_sshd_config}" 2> /dev/null)
        # selinux
        [[ -n "${ssh_port_specify}" && $(expr "${ssh_port_specify}" \= "${ssh_port}") -eq 0 ]] && fnBase_SELinuxSemanageOperation 'ssh_port_t' "${ssh_port_specify}" 'port' 'add' 'tcp'
        # https://blog.tinned-software.net/ssh-key-authentication-is-not-working-selinux/
        # semanage fcontext -a -t ssh_home_t "${login_user_home}/.ssh/"
        # restorecon -v "${login_user_home}/.ssh/"
        [[ "${login_user}" != 'root' && -d "${login_user_home}/.ssh/" ]] && fnBase_SELinuxSemanageOperation 'ssh_home_t' "${login_user_home}/.ssh(/.*)?" 'fcontext' 'add'
    fi

    # sshd_config configuration
    if [[ -f "${l_sshd_config}" ]]; then
        # sshd_config directive lists
        local l_sshd_directive_list
        l_sshd_directive_list=$(mktemp -t "${mktemp_format}")
        # Attention: On CentOS 6.x, export `man sshd_config` has special character, must remove ^H first
        # ctrl + V + H --> ^H
        # /etc/manpath.config
        # Error info: No manual entry for sshd_config
        # AWS EC2 remove .gz files stores under dir /usr/share/man/man{1,3,5,7,8} which make `man sshd_config` not works
        man sshd_config 2> /dev/null | sed -r -n 's@^H\w@@g;p' | sed -r -n '/^[[:space:]]{1,8}[[:upper:]]{1}[[:alpha:]]+[[:space:]]*/{s@^[[:space:]]*([[:alpha:]]+).*@\1@g;p}' > "${l_sshd_directive_list}"
        [[ -z "${l_sshd_directive_list}" ]] && : > "${l_sshd_directive_list}"

        # SSH configuration test
        # -T      Extended test mode.  Check the validity of the configuration file, output the effective configuration to stdout and then exit.  Optionally, Match rules may be applied by specifying the connection parameters using one or more -C options.
        # sshd -T

        # - Port
        [[ -n "${ssh_port_specify}" ]] && fn_OpenSSHDirectiveSetting 'Port' "${ssh_port_specify}" "${l_sshd_directive_list}"
        # fail2ban configuration  if ssh port is not default port 22, change 'port    = ssh' to 'port    = $port_specify' in file '/etc/fail2ban/jail.local'

        # - Banner
        # before login /etc/issue, Setting 'Banner' in /etc/ssh/sshd_config
        # after login /etc/motd
        fn_OpenSSHDirectiveSetting 'PrintMotd' 'yes' "${l_sshd_directive_list}"
        # Last login: Fri Jan  5 16:19:01 2018 from 13.125.75.217
        fn_OpenSSHDirectiveSetting 'PrintLastLog' 'no' "${l_sshd_directive_list}"
        local l_ssh_banner='/etc/ssh/sshd_banner'
        [[ -s "${l_ssh_banner}" ]] || echo -e "#################################################################\n##                 Welcome to GNU/Linux World                  ##\n##         All connections are monitored and recorded          ##\n##   Unauthorized user is prohibited, Disconnect immediately   ##\n#################################################################\n" > "${l_ssh_banner}"
        chmod 644 "${l_ssh_banner}"
        fn_OpenSSHDirectiveSetting 'Banner' "${l_ssh_banner}" "${l_sshd_directive_list}"

        # Only Use SSH Protocol 2
        # sed -i -r 's@^#?(Protocol 2)@\1@' "${l_sshd_config}"
        fn_OpenSSHDirectiveSetting 'Protocol' '2' "${l_sshd_directive_list}"

        # Just allow ipv4
        # Specifies which address family should be used by sshd(8).  Valid arguments are 'any', 'inet' (use IPv4 only), or 'inet6' (use IPv6 only).  The default is 'any'.
        fn_OpenSSHDirectiveSetting 'AddressFamily' 'inet' "${l_sshd_directive_list}"

        # Specifies whether compression is enabled after the user has authenticated successfully. [yes|delayed|no]
        fn_OpenSSHDirectiveSetting 'Compression' 'delayed' "${l_sshd_directive_list}"

        # AllowGroups : This keyword can be followed by a list of group name patterns, separated by spaces.
        local group_allow_name
        group_allow_name=${group_allow_name:-'ssh_group_allow'}
        local group_path='/etc/gshadow'
        [[ "${pack_manager}" == 'zypper' ]] && group_path='/etc/group'
        [[ -z $(sed -n '/^'"${group_allow_name}"':/{s@^([^:]+).*$@\1@g;p}' "${group_path}" 2> /dev/null) ]] && groupadd "${group_allow_name}" &>/dev/null
        fn_OpenSSHDirectiveSetting 'AllowGroups' "${group_allow_name}" "${l_sshd_directive_list}"
        gpasswd -a "${login_user}" "${group_allow_name}" &>/dev/null

        [[ -n "${username_specify}" ]] && gpasswd -a "${username_specify}" "${group_allow_name}" &>/dev/null

        # Disable root Login via SSH PermitRootLogin {yes,without-password,forced-commands-only,no}
        if [[ "${disable_ssh_root}" -eq 1 ]]; then
            if [[ "${login_user}" != 'root' || ("${login_user}" == 'root' && -n "${username_specify}") ]]; then
                gpasswd -d root "${group_allow_name}" &>/dev/null
                fn_OpenSSHDirectiveSetting 'PermitRootLogin' 'no' "${l_sshd_directive_list}"
            fi
        fi

        # Disallow forward
        local l_tcp_forward=${l_tcp_forward:='yes'}
        [[ "${forward_disable}" -eq 1 ]] && l_tcp_forward='no'
        fn_OpenSSHDirectiveSetting 'AllowTcpForwarding' "${l_tcp_forward}" "${l_sshd_directive_list}"

        # Specifies whether X11 forwarding is permitted.   A system administrator may have a stance in which they want to protect clients that may expose themselves to attack by unwittingly requesting X11 forwarding, which can warrant a “no” setting.
        fn_OpenSSHDirectiveSetting 'X11Forwarding' 'no' "${l_sshd_directive_list}"
        fn_OpenSSHDirectiveSetting 'AllowAgentForwarding' 'no' "${l_sshd_directive_list}"

        # Disabling sshd DNS Checks
        fn_OpenSSHDirectiveSetting 'UseDNS' 'no' "${l_sshd_directive_list}"
        # Log Out Timeout Interval, just work for Protocol 2
        fn_OpenSSHDirectiveSetting 'ClientAliveCountMax' '3' "${l_sshd_directive_list}"
        fn_OpenSSHDirectiveSetting 'ClientAliveInterval' '90' "${l_sshd_directive_list}"
        # MaxAuthTries
        fn_OpenSSHDirectiveSetting 'MaxAuthTries' '3' "${l_sshd_directive_list}"
        # MaxSessions
        fn_OpenSSHDirectiveSetting 'MaxSessions' '2' "${l_sshd_directive_list}"
        # Disallow the system send TCP keepalive messages to the other side
        fn_OpenSSHDirectiveSetting 'TCPKeepAlive' 'no' "${l_sshd_directive_list}"
        # Don't read the user's ~/.rhosts and ~/.shosts files
        fn_OpenSSHDirectiveSetting 'IgnoreRhosts' 'yes' "${l_sshd_directive_list}"
        # Disable Host-Based Authentication
        fn_OpenSSHDirectiveSetting 'HostbasedAuthentication' 'no' "${l_sshd_directive_list}"
        # Disallow Empty Password Login
        fn_OpenSSHDirectiveSetting 'PermitEmptyPasswords' 'no' "${l_sshd_directive_list}"
        # https://unix.stackexchange.com/questions/115839/change-sshd-logging-file-location-on-centos
        fn_OpenSSHDirectiveSetting 'SyslogFacility' 'AUTHPRIV' "${l_sshd_directive_list}"
        # Enable Logging Message {QUIET, FATAL, ERROR, INFO, VERBOSE, DEBUG, DEBUG1, DEBUG2, DEBUG3}
        fn_OpenSSHDirectiveSetting 'LogLevel' 'VERBOSE' "${l_sshd_directive_list}"
        # Check file modes and ownership of the user's files and home directory before accepting login
        fn_OpenSSHDirectiveSetting 'StrictModes' 'yes' "${l_sshd_directive_list}"

        # Log sftp level file access (read/write/etc.) that would not be easily logged otherwise.
        # https://unix.stackexchange.com/questions/61580/sftp-gives-an-error-received-message-too-long-and-what-is-the-reason#answer-327284
        # https://serverfault.com/questions/660160/openssh-difference-between-internal-sftp-and-sftp-server
        # Subsystem sftp /usr/lib/openssh/sftp-server
        if [[ -n $(sed -n -r '/^#?Subsystem[[:space:]]+sftp[[:space:]]+/p' "${l_sshd_config}") ]]; then
            sed -r -i '/^#?Subsystem[[:space:]]+sftp[[:space:]]+/{s@^#?(Subsystem[[:space:]]+sftp)[[:space:]]+.*$@\1 internal-sftp -l INFO@g;}' "${l_sshd_config}"
        else
            sed -i -r '$a Subsystem sftp internal-sftp -l INFO' "${l_sshd_config}"
        fi

        # Checks whether the account has been locked with passwd -l
        # AWS EC2
        local l_usepamchecklocks=${l_usepamchecklocks:-'yes'}
        [[ "${distro_name}" == 'sles' ]] && l_usepamchecklocks='no'
        [[ -s "${l_sshd_directive_list}" ]] && fn_OpenSSHDirectiveSetting 'UsePAMCheckLocks' "${l_usepamchecklocks}" "${l_sshd_directive_list}"

        # Supported HostKey algorithms by order of preference
        sed -i -r 's@^#?(HostKey /etc/ssh/ssh_host_rsa_key)$@\1@' "${l_sshd_config}"
        sed -i -r 's@^#?(HostKey /etc/ssh/ssh_host_ecdsa_key)$@\1@' "${l_sshd_config}"

        # https://wiki.mozilla.org/Security/Guidelines/OpenSSH
        local l_sshd_keyalgorithms=${l_sshd_keyalgorithms:-'diffie-hellman-group-exchange-sha256'}
        local l_sshd_ciphers=${l_sshd_ciphers:-'aes256-ctr,aes192-ctr,aes128-ctr'}
        local l_sshd_macs=${l_sshd_macs:-'hmac-sha2-512,hmac-sha2-256'}

        if [[ $(expr "${ssh_version}" \>= 6.7) -eq 1 ]]; then
            sed -i -r 's@^#?(HostKey /etc/ssh/ssh_host_ec25519_key)$@\1@' "${l_sshd_config}"
            # Turn on privilege separation  yes/sandbox
            fn_OpenSSHDirectiveSetting 'UsePrivilegeSeparation' 'sandbox' "${l_sshd_directive_list}"

            l_sshd_keyalgorithms='curve25519-sha256@libssh.org,ecdh-sha2-nistp521,ecdh-sha2-nistp384,ecdh-sha2-nistp256,diffie-hellman-group-exchange-sha256'
            l_sshd_ciphers='chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr'
            l_sshd_macs='hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-512,hmac-sha2-256,umac-128@openssh.com'
        else
            fn_OpenSSHDirectiveSetting 'UsePrivilegeSeparation' 'yes' "${l_sshd_directive_list}"
        fi

        # Specifies the available KEX (Key Exchange) algorithms
        fn_OpenSSHDirectiveSetting 'KexAlgorithms' "${l_sshd_keyalgorithms}" "${l_sshd_directive_list}"
        # Ciphers Setting
        fn_OpenSSHDirectiveSetting 'Ciphers' "${l_sshd_ciphers}" "${l_sshd_directive_list}"
        # Message authentication codes (MACs) Setting
        fn_OpenSSHDirectiveSetting 'MACs' "${l_sshd_macs}" "${l_sshd_directive_list}"

        # Using PAM, the follow `ChallengeResponseAuthentication` and `PasswordAuthentication` used by PAM authentication
        fn_OpenSSHDirectiveSetting 'UsePAM' 'yes' "${l_sshd_directive_list}"
        # Challenge-response Authentication support 'keyboard-interactive' authentication (verification code)
        fn_OpenSSHDirectiveSetting 'ChallengeResponseAuthentication' 'no' "${l_sshd_directive_list}"

        if [[ "${ssh_keygen_only}" -eq 1 && -s "${login_user_home}/.ssh/authorized_keys" ]]; then
            # Disable Password Authentication
            fn_OpenSSHDirectiveSetting 'PasswordAuthentication' 'no' "${l_sshd_directive_list}"
            # Use Public Key Based Authentication
            fn_OpenSSHDirectiveSetting 'PubkeyAuthentication' 'yes' "${l_sshd_directive_list}"

            # Specify File Containing Public Key Allowed Authentication Login
            # AuthorizedKeysFile  .ssh/authorized_keys
            fn_OpenSSHDirectiveSetting 'AuthorizedKeysFile' '%h/.ssh/authorized_keys' "${l_sshd_directive_list}"

            # Just Allow Public Key Authentication Login
            if [[ $(expr "${ssh_version}" \>= 6.7) -eq 1 ]]; then
                fn_OpenSSHDirectiveSetting 'AuthenticationMethods' 'publickey' "${l_sshd_directive_list}"
            fi

            # publickey (SSH key), password publickey (password), keyboard-interactive (verification code)
            # fn_OpenSSHDirectiveSetting 'AuthenticationMethods' 'publickey,password publickey,keyboard-interactive' "${l_sshd_directive_list}"
        else
            fn_OpenSSHDirectiveSetting 'PasswordAuthentication' 'yes'
        fi

    fi

    if [[ $(expr "${ssh_version}" \= 0) -eq 1 ]]; then
        fnBase_OperationProcedureResult
    else
        if [[ -n "${ssh_port_specify}" && $(expr "${ssh_port_specify}" \= "${ssh_port_default}") -eq 0 ]]; then
            fnBase_OperationProcedureResult "v${ssh_version} port ${ssh_port_specify}"
        else
            fnBase_OperationProcedureResult "v${ssh_version}"
        fi
    fi

    if [[ "${login_user}" == 'root' && -f "${login_user_home}/.ssh/authorized_keys" && -n "${username_specify}" && "${ssh_keygen_only}" -eq 1 ]]; then
        fnBase_OperationProcedureStatement "authorized_keys"
        # add authorized_keys to new created user
        local newuser_home=${newuser_home:-}
        # newuser_home=$(awk -F: 'match($0,/^'"${username_specify}"'/){print $6}' /etc/passwd)
        newuser_home=$(sed -r -n '/^'"${username_specify}"':/{p}' /etc/passwd | cut -d: -f6)

        if [[ -n "${newuser_home}" ]]; then
            (umask 077; [[ -d "${newuser_home}/.ssh" ]] || mkdir -p "${newuser_home}/.ssh"; cat "${login_user_home}/.ssh/authorized_keys" >> "${newuser_home}/.ssh/authorized_keys"; chown -R "${username_specify}" "${newuser_home}/.ssh")
            [[ -d "${newuser_home}/.ssh/" ]] && fnBase_SELinuxSemanageOperation 'ssh_home_t' "${newuser_home}/.ssh/" 'fcontext' 'add'
        fi
        fnBase_OperationProcedureResult "${username_specify} <= ${login_user}"
    fi
}

fn_OpenBSDSecureShellOperation(){
    fnBase_OperationPhaseStatement 'OpenBSD Secure Shell'
    fn_OpenSSHInstallation
    fn_OpenSSHConfiguration
}


#########  2-8. Firewall Setting - firewalld/iptables/ufw/SuSEfirewall2  #########
# Block Top 10 Known-bad IPs
# $download_method https://isc.sans.edu/top10.html | sed -r -n '/ipdetails.html/{s@.*?ip=([^"]+)".*@\1@g;s@^0+@@g;s@\.0+@.@g;p}'
fn_FirewallConfiguration(){
    fnBase_OperationPhaseStatement "Firewall Application"
    fnBase_OperationProcedureStatement "Firewall for ${distro_name}"

    l_ssh_connection_ip=${l_ssh_connection_ip:-}
    # l_ssh_connecting_port=${l_ssh_connecting_port:-}

    # SSH_CLIENT, SSH_CONNECTION will be failed to read if run this script with sudo privilege
    if [[ -n "${SSH_CLIENT:-}" ]]; then
        l_ssh_connection_ip="${SSH_CLIENT%% *}"
        # l_ssh_connecting_port="${SSH_CLIENT##* }"
    elif [[ -n "${SSH_CONNECTION:-}" ]]; then
        l_ssh_connection_ip="${SSH_CONNECTION%% *}"
        # l_ssh_connecting_port="${SSH_CONNECTION##* }"
    else
        l_ssh_connection_ip=$(who | sed -r -n '$s@.*\(([^\)]+)\).*@\1@gp')
        if [[ ! "${l_ssh_connection_ip}" =~ ^([0-9]{1,3}.){3}[0-9]{1,3}$ ]]; then
            if fnBase_CommandExistIfCheck 'ping'; then
                l_ssh_connection_ip=$(ping -c1 -n "${l_ssh_connection_ip}" 2> /dev/null | sed -r -n '1{s@^[^\(]*\(([^\)]*).*@\1@gp}')
            elif fnBase_CommandExistIfCheck 'host'; then
                l_ssh_connection_ip=$(host "${l_ssh_connection_ip}" 2> /dev/null | sed -r -n 's@.*address[[:space:]]*(.*)$@\1@g;p')
            elif fnBase_CommandExistIfCheck 'dig'; then
                l_ssh_connection_ip=$(dig +short "${l_ssh_connection_ip}" 2> /dev/null)
            else
                l_ssh_connection_ip=''
            fi
        fi
        [[ "${l_ssh_connection_ip}" == ":0" ]] && l_ssh_connection_ip='127.0.0.1'
    fi

    [[ -n "${ssh_port_specify}" ]] && ssh_port="${ssh_port_specify}"
    if [[ "${restrict_remote_login}" -eq 1 && -n "${l_ssh_connection_ip}" ]]; then
        $download_method "${firewall_configuration_script}" | bash -s -- -p "${ssh_port}" -H "${l_ssh_connection_ip}" -s
    else
        $download_method "${firewall_configuration_script}" | bash -s -- -p "${ssh_port}" -s
    fi

    local l_firewall_type=${l_firewall_type:-}
    case "${pack_manager}" in
        apt-get ) l_firewall_type='ufw' ;;
        zypper ) l_firewall_type='SuSEfirewall2' ;;
        dnf|yum ) [[ $("${pack_manager}" info firewalld 2>&1 | sed -r -n '/^Name[[:space:]]+:/{s@^[^:]+:[[:space:]]*(.*)$@\1@g;p;q}') == 'firewalld' ]] && l_firewall_type='firewalld' || l_firewall_type='iptables' ;;
    esac
    fnBase_OperationProcedureResult "${l_firewall_type}"
}


#########  2-9. AxdSop Tool Installation  #########
fn_AxdSopInstallation(){
    # Just for personal preference via '-b'
    local l_target_user_home=${l_target_user_home:-"${login_user_home}"}
    local l_target_user=${l_target_user:-"${login_user}"}
    if [[ -n "${username_specify}" && "${username_specify}" != 'root' ]]; then
        l_target_user="${username_specify}"
        l_target_user_home="/home/${username_specify}"
    fi

    # ~/.bashrc
    local l_login_bashrc_path=${l_login_bashrc_path:-"${l_target_user_home}/.bashrc"}
    local l_boost_bash=${l_boost_bash:-'.axdsop/bashrc'}
    local l_boost_bash_path=${l_boost_bash_path:-"${l_target_user_home}/${l_boost_bash}"}

    if [[ -d "${l_target_user_home}" ]]; then
        # https://www.gnu.org/software/gawk/manual/html_node/Splitting-By-Content.html
        # https://stackoverflow.com/questions/20835437/how-to-preserve-the-original-whitespace-between-fields-in-awk
        local l_custom_flag=${l_custom_flag:-'AxdSop Setting'}
        local l_custom_flag_start=${l_custom_flag_start:-"${l_custom_flag} Start"}
        local l_custom_flag_end=${l_custom_flag_start:-"${l_custom_flag} End"}

        # - ~/.bashrc append
        if [[ -f "${l_login_bashrc_path}" ]]; then
            sed -r -i '/'"${l_custom_flag_start}"'/,/'"${l_custom_flag_end}"'/d' "${l_login_bashrc_path}" &> /dev/null
            echo -e "# ${l_custom_flag_start}\n[ -f ~/${l_boost_bash} ] && . ~/${l_boost_bash}\n# ${l_custom_flag_end}" >> "${l_login_bashrc_path}"
        fi

        if [[ ! -d "${l_boost_bash_path%/*}" ]]; then
            mkdir -p "${l_boost_bash_path%/*}"
            chown -R "${l_target_user}" "${l_boost_bash_path%/*}" 2> /dev/null
        fi

        # - ~/.axdsop/bashrc rewrite
        $download_method "${ssh_custom_config}" > "${l_boost_bash_path}"
        chmod 640 "${l_boost_bash_path}" 2> /dev/null
        chown "${l_target_user}" "${l_boost_bash_path}" 2> /dev/null

        # download tool variable 'download_method'
        local downlaod_command_name='download_method'
        if fnBase_CommandExistIfCheck 'curl'; then
            sed -r -i '/^#?[[:space:]]*'"${downlaod_command_name}"'/{/wget/d;s@^#*[[:space:]]*@@g;}' "${l_boost_bash_path}" 2> /dev/null
        else
            sed -r -i '/^#?[[:space:]]*'"${downlaod_command_name}"'/{/wget/!d;s@^#*[[:space:]]*@@g;}' "${l_boost_bash_path}" 2> /dev/null
        fi

        # system update for specific distro
        sed -r -i '/'"${l_custom_flag_start}"'/,/'"${l_custom_flag_end}"'/{/_system_update=/{/'"${pack_manager}"'/!d;s@^#?[[:space:]]*@@g;}}' "${l_boost_bash_path}" 2> /dev/null

        # remove older version kernel
        sed -r -i '/'"${l_custom_flag_start}"'/,/'"${l_custom_flag_end}"'/{/_kernel_(old|remove)=/{/'"${pack_manager}"'/!d;s@^#?[[:space:]]*@@g;}}' "${l_boost_bash_path}" 2> /dev/null

        # mesg: ttyname failed: Inappropriate ioctl for device
        # https://www.gnupg.org/documentation/manuals/gnupg/Invoking-GPG_002dAGENT.html#Invoking-GPG_002dAGENT
        fnBase_CommandExistIfCheck 'gpg2' && sed -r -i '/GPG_TTY=/{s@^#?[[:space:]]*@@g}' "${l_boost_bash_path}" 2> /dev/null

        # docker
        fnBase_CommandExistIfCheck 'docker' && sed -r -i '/docker usage start/,/docker usage end/{/alias/{s@^#?[[:space:]]*@@g;}}' "${l_boost_bash_path}" 2> /dev/null

        # remove sudo in command
        [[ "${l_target_user_home}" =~ ^/home/ ]] || sed -r -i 's@[[:space:]]*sudo[[:space:]]+@ @g;' "${l_boost_bash_path}" 2> /dev/null

        # disable Bash shell commands history permanently
        # https://linuxconfig.org/how-to-disable-bash-shell-commands-history-on-linux
        # set -o | grep history
        # set +o history
        # unset HISTFILE

        # https://www.cyberciti.biz/faq/clear-the-shell-history-in-ubuntu-linux/
        # https://www.tecmint.com/empty-delete-file-content-linux/
        local l_bash_logout_path=${l_bash_logout_path:-"${login_user_home}/.bash_logout"}
        if [[ -s "${l_bash_logout_path}" ]]; then
            sed -r -i '/'"${l_custom_flag_start}"'/,/'"${l_custom_flag_end}"'/d' "${l_bash_logout_path}" &> /dev/null
            echo -e "# ${l_custom_flag_start}\nhistory -c\n[ -s ~/.bash_history ] && : > ~/.bash_history\n# ${l_custom_flag_end}" >> "${l_bash_logout_path}"
        fi

    fi

    # ~/.ssh
    local l_ssh_dir=${l_ssh_dir:-"${l_target_user_home}/.ssh"}
    local l_ssh_config_path=${l_ssh_config_path:-"${l_ssh_dir}/config"}
    [[ -d "${l_ssh_dir}" ]] || mkdir -p "${l_ssh_dir}"
    local l_ssh_sockets_dir=${l_ssh_sockets_dir:-"${l_ssh_dir}/sockets"}
    if [[ ! -d "${l_ssh_sockets_dir}" ]]; then
        # mkdir -m=700 -p "${l_ssh_sockets_dir}"
        mkdir -p "${l_ssh_sockets_dir}"
        chmod 700 "${l_ssh_sockets_dir}"
    fi
    [[ -s "${l_ssh_config_path}" ]] || $download_method "${ssh_custom_config}" > "${l_ssh_config_path}"
    [[ -s "${l_ssh_config_path}" ]] && chmod 600 "${l_ssh_config_path}"
    if [[ -d "${l_ssh_dir}" ]]; then
        chmod 700 "${l_ssh_dir}"
        chown -R "${l_target_user}" "${l_ssh_dir}"
    fi

    # https://access.redhat.com/articles/2123781
    # https://www.openssh.com/security.html
    # OpenSSH clients between versions 5.4 and 7.1 are vulnerable to information disclosure that may allow a malicious server to retrieve information including under some circumstances, user's private keys. This may be mitigated by adding the undocumented config option UseRoaming no to ssh_config.
    # For more information see CVE-2016-0777 and CVE-2016-0778.
    # This bug is corrected in OpenSSH 7.1p2 and in OpenBSD's stable branch.
    local l_ssh_version=${l_ssh_version:-0}
    l_ssh_version=$(ssh -V 2>&1 | sed -r -n 's@.*_([[:digit:].]{3}).*@\1@p')
    if [[ $(expr "${l_ssh_version}" \<= 5.4) -eq 1 || $(expr "${l_ssh_version}" \>= 7.1) -eq 1 ]]; then
        sed -r -i '/UseRoaming/d' "${l_ssh_config_path}"
    fi

    # install shellcheck https://github.com/koalaman/shellcheck
    fnBase_CommandExistIfCheck 'shellcheck' || fnBase_PackageManagerOperation 'install' 'shellcheck'

    # tor|torsocks
    # tor - anonymizing overlay network for TCP
    # torsocks - use SOCKS-friendly applications with Tor
    # config dir /etc/tor/
    # https://linuxconfig.org/install-tor-on-ubuntu-18-04-bionic-beaver-linux
    # Set shell to use torsocks as default for any command.
    # echo ". torsocks on" >> ~/.bashrc
    # source torsocks on
    # Tor mode activated. Every command will be torified for this shell.
    # source torsocks off
    # Tor mode deactivated. Command will NOT go through Tor anymore.
    fnBase_CommandExistIfCheck 'tor' || fnBase_PackageManagerOperation 'install' 'tor'
    fnBase_CommandExistIfCheck 'torsocks' || fnBase_PackageManagerOperation 'install' 'torsocks'
    fnBase_CommandExistIfCheck 'torsocks' && fnBase_SystemServiceManagement 'tor' 'disable'
}

#########  2-10. tmpfs filesystem for /tmp provided by systemd  #########
# use command df to list partition filesystem info
fn_TmpfsSystemSetting(){
    if [[ "${tmpfs_enable}" -eq 1 ]]; then
        # just for systemd
        if fnBase_CommandExistIfCheck 'systemctl'; then
            # 7GB == 7 * 1024 * 1024 == 7340032 KB
            if [[ "${mem_totoal_size}" -ge 7340032 ]]; then
                fnBase_OperationPhaseStatement "TMPFS Filesystem"
                fnBase_OperationProcedureStatement "tmpfs filesystem for /tmp"
                # for debian / suse
                local tmp_mount_source_path=${tmp_mount_source_path:-'/usr/share/systemd/tmp.mount'}
                # for rhel / arch linux
                [[ -s '/lib/systemd/system/tmp.mount' ]] && tmp_mount_source_path='/lib/systemd/system/tmp.mount'
                local tmp_mount_target_path=${tmp_mount_target_path:-'/etc/systemd/system/tmp.mount'}

                if [[ -s "${tmp_mount_source_path}" && ! -s "${tmp_mount_target_path}" ]]; then
                    cp -f "${tmp_mount_source_path}" "${tmp_mount_target_path}"
                    local tmpfs_mem_size=${tmpfs_mem_size:-1}
                    # 16GB == 16777216 KB, 32GB == 33554432 KB, 64GB == 67108864 KB, 128GB == 134217728 KB, 256GB == 268435456 KB, 512GB == 536870912 KB
                    if [[ "${mem_totoal_size}" -ge 134217728 ]]; then
                        tmpfs_mem_size=8
                    elif [[ "${mem_totoal_size}" -ge 67108864 ]]; then
                        tmpfs_mem_size=4
                    elif [[ "${mem_totoal_size}" -ge 33554432 ]]; then
                        tmpfs_mem_size=3
                    elif [[ "${mem_totoal_size}" -ge 16777216 ]]; then
                        tmpfs_mem_size=2
                    fi
                    # Options=size=1g,mode=1777,strictatime,nosuid,nodev
                    sed -r -i '/^Options=/{s@(Options=).*@\1size='"${tmpfs_mem_size}"'g,mode=1777,strictatime,nosuid,nodev@g;}' "${tmp_mount_target_path}"
                    fnBase_SystemServiceManagement 'tmp.mount' 'enable'

                    fnBase_OperationProcedureResult "${tmpfs_mem_size}g"
                fi    # end if configure tmp.mount

            fi    # end if mem_totoal_size
        fi    # end if check systemctl exists or not

    fi   # end if tmpfs_enable
}


#########  2-11. Kernel Optimization  #########
fn_SysctlDirectiveSetting(){
    local l_item=${1:-}
    local l_val=${2:-}
    local l_action=${3:-}
    local l_path=${l_path:-'/etc/sysctl.conf'}

    if [[ -n "${l_item}" && -f "/proc/sys/${l_item//./\/}" && -n "${l_val}" && -f "${l_path}" ]]; then
        if [[ -n $(sed -r -n '/^#?[[:space:]]*'"${l_item}"'[[:space:]]*=/{p}' "${l_path}") ]]; then
            case "${l_action,,}" in
                d|delete )
                    sed -r -i '/^#?[[:space:]]*'"${l_item}"'[[:space:]]*=/d' "${l_path}" 2> /dev/null
                    ;;
                c|comment )
                    sed -r -i '/^#?[[:space:]]*'"${l_item}"'[[:space:]]*=/{s@^#?[[:space:]]*(.*)$@# \1@g}' "${l_path}" 2> /dev/null
                    ;;
                * )
                    sed -r -i '/^#?[[:space:]]*'"${l_item}"'[[:space:]]*=/{s@^#?[[:space:]]*([^=]+=[[:space:]]*).*@\1'"${l_val}"'@g}' "${l_path}" 2> /dev/null
                    ;;
            esac
        else
            [[ -z "${l_action}" ]] && sed -r -i '$ '"${l_item}"' = '"${l_val}"'' "${l_path}" 2> /dev/null
        fi
    fi
}

fn_KernelOptimization(){
    fnBase_OperationPhaseStatement 'Kernel Optimization'

    # The Linux sysctl command configures kernel parameters at runtime. The parameters available are those listed under /proc/sys/.

    local sysctl_config=${sysctl_config:-'/etc/sysctl.conf'}

    # - Kernel parameters    /etc/sysctl.conf
    # https://www.frozentux.net/ipsysctl-tutorial/
    # https://www.kernel.org/doc/html/latest/admin-guide/sysrq.html
    fnBase_OperationProcedureStatement 'Kernel parameters'
    # https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/security_guide/sec-securing_network_access
    if [[ -f "${sysctl_config}" ]]; then
        [[ -f "${sysctl_config}${bak_suffix}" ]] || cp -fp "${sysctl_config}" "${sysctl_config}${bak_suffix}"

        local l_sysctl_pattern=${l_sysctl_pattern:-'Kernel Parameters Setting'}
        if [[ -z $(sed -r -n '/'"${l_sysctl_pattern}"'/p' "${sysctl_config}" 2> /dev/null) ]]; then
            # sed -i '/'"${l_sysctl_pattern}"' Start/,/'"${l_sysctl_pattern}"' End/d' "${sysctl_config}"
            $download_method "${sysctl_url}" > "${sysctl_config}"
            # remove comment begin with single #
            sed -r -i '/'"${l_sysctl_pattern}"' Start/,/'"${l_sysctl_pattern}"' End/{/^#[[:space:]]+/{/('"${l_sysctl_pattern}"'|=)/!d}}' "${sysctl_config}" 2> /dev/null
            # merge mulitple empty lines to one line
            # https://unix.stackexchange.com/questions/18181/why-cant-i-sed-two-or-more-empty-lines-to-one-empty-line#18184
            sed -r -i '$!N;/^\n$/!P;D' "${sysctl_config}" 2> /dev/null
        fi

        # swappiness
        # 16GB == 16 * 1024 * 1024 == 16777216 KB
        [[ "${mem_totoal_size}" -le 16777216 ]] && fn_SysctlDirectiveSetting 'vm.swappiness' '10'

        #  ip forward
        local l_ip_forward=${l_ip_forward:-1}
        [[ "${forward_disable}" -eq 1 ]] && l_ip_forward=0
        fn_SysctlDirectiveSetting 'net.ipv4.ip_forward' "${l_ip_forward}"
        fn_SysctlDirectiveSetting 'net.ipv4.conf.all.forwarding' "${l_ip_forward}"
        fn_SysctlDirectiveSetting 'net.ipv6.conf.all.forwarding' "${l_ip_forward}"
    fi
    fnBase_OperationProcedureResult '/etc/sysctl.conf'

    # - enable TCP Bottleneck Bandwidth and RTT (BBR)  congestion control  >= 4.9
    # https://cloudplatform.googleblog.com/2017/07/TCP-BBR-congestion-control-comes-to-GCP-your-Internet-just-got-faster.html
    # https://www.cyberciti.biz/cloud-computing/increase-your-linux-server-internet-speed-with-tcp-bbr-congestion-control/
    # https://aws.amazon.com/amazon-linux-ami/2017.09-release-notes/

    # egrep 'CONFIG_TCP_CONG_BBR|CONFIG_NET_SCH_FQ' /boot/config-$(uname -r)
    # /etc/sysctl.conf
    # net.core.default_qdisc=fq
    # net.ipv4.tcp_congestion_control=bbr
}

#########  2-12. Security & Audit  #########
fn_SystemHardeningOperation(){
    fnBase_OperationPhaseStatement 'System Hardening'
    # https://highon.coffee/blog/security-harden-centos-7/
    # https://www.safecomputing.umich.edu/protect-the-u/protect-your-unit/hardening/secure-linux-unix-server
    # https://www.slideshare.net/brendangregg/how-netflix-tunes-ec2-instances-for-performance
    # https://www.cyberciti.biz/tips/linux-security.html
    # https://www.thefanclub.co.za/how-to/how-secure-ubuntu-1604-lts-server-part-1-basics

    # /etc/fstab
    # mount -o defults,noatime,discard,nobarrier

    # - Protect su by limiting access only to sudo group
    # https://unix.stackexchange.com/questions/289743/how-to-harden-su-with-dpkg-statoverride#answer-289744
    # dpkg-statoverride - override ownership and mode of files
    # --add user group mode path    If --update is specified and path exists, it is immediately set to the new owner and mode.
    # --update  Immediately try to change the path to the new owner and mode if it exists.
    if fnBase_CommandExistIfCheck 'dpkg-statoverride'; then
        fnBase_OperationProcedureStatement 'Protect su by limiting access'
        # change /bin/su permission from '4755/-rwsr-xr-x' to '4750/-rwsr-x---', from 'root:root' to 'root:sudo'
        dpkg-statoverride --update --add root sudo 4750 /bin/su 2> /dev/null
        fnBase_OperationProcedureResult '4750/-rwsr-x---'
    fi

    # Disable sudo hint in Ubuntu
    if [[ "${distro_name}" == 'ubuntu' ]]; then
        # https://askubuntu.com/questions/22607/remove-note-about-sudo-that-appears-when-opening-the-terminal
        fnBase_OperationProcedureStatement 'Disable sudo hint'
        # $HOME/.sudo_as_admin_successful
        # To run a command as administrator (user "root"), use "sudo <command>".
        # See "man sudo_root" for details.
        local l_sudo_hint_path=${l_sudo_hint_path:-'/etc/bash.bashrc'}
        [[ -f "${l_sudo_hint_path}" && -n $(sed -n '/sudo hint/{p}' "${l_sudo_hint_path}" 2> /dev/null) ]] && sed -r -i '/sudo hint/,/^$/{/^[^#]/{s@^@#@g;}}' "${l_sudo_hint_path}" 2> /dev/null
        fnBase_OperationProcedureResult "${l_sudo_hint_path}"
    fi

    # - Determining default umask
    fnBase_OperationProcedureStatement 'Determining default umask'
    # /etc/login.defs
    [[ -s '/etc/login.defs' ]] && sed -r -i '/^UMASK[[:space:]]+/{s@^([^[:space:]]+[[:space:]]+).*$@\1'${umask_default}'@g;}' /etc/login.defs &> /dev/null
    # /etc/init.d/functions
    [[ -f '/etc/init.d/functions' ]] && sed -r -i '/^#?[[:space:]]*umask[[:space:]]+/{s@^#?[[:space:]]*(umask[[:space:]]+).*$@\1'"${umask_default}"'@g;}' /etc/init.d/functions &> /dev/null
    # /etc/profile
    [[ -f '/etc/profile' ]] && sed -r -i '/umask[[:space:]]*022/{s@^([[:space:]]+umask[[:space:]]+).*$@\1'"${umask_default}"'@g;}' /etc/profile &> /dev/null
    # /etc/bashrc
    [[ -f '/etc/bashrc' ]] && sed -r -i '/umask[[:space:]]*022/{s@^([[:space:]]+umask[[:space:]]+).*$@\1'"${umask_default}"'@g;}' /etc/bashrc &> /dev/null
    # /etc/init.d/rc
    [[ -f '/etc/init.d/rc' ]] && sed -r -i '/umask[[:space:]]*022/{s@^([[:space:]]*umask[[:space:]]+).*$@\1'"${umask_default}"'@g;}' /etc/init.d/rc &> /dev/null
    fnBase_OperationProcedureResult "${umask_default}"

    # - File descriptor    /etc/security/limits.conf
    fnBase_OperationProcedureStatement 'File descriptor'
    local l_nofile_num=${l_nofile_num:-655360}
    local l_proc_num=${l_proc_num:-64000}
    local security_limit_config=${security_limit_config:-'/etc/security/limits.conf'}

    if [[ -f "${security_limit_config}" ]]; then
        [[ -f "${security_limit_config}${bak_suffix}" ]] || cp -fp "${security_limit_config}" "${security_limit_config}${bak_suffix}"
        sed -i -r '/^\* (soft|hard) nofile /d;/End of file/d' "${security_limit_config}"

        echo -e "* soft nofile ${l_nofile_num}\n* hard nofile ${l_nofile_num}\n* soft nproc ${l_proc_num}\n* hard nproc ${l_proc_num}" >> "${security_limit_config}"
        echo 'End of file' >> "${security_limit_config}"
    fi

    # If use Gnome desktop, systemd not read /etc/security/limits.conf
    # https://stackoverflow.com/questions/46441602/how-to-change-open-files-ulimit-n-permanently-for-normal-user-in-debian-stret/47404791#47404791
    # https://bugzilla.redhat.com/show_bug.cgi?id=1364332
    if fnBase_CommandExistIfCheck 'gnome-shell'; then
        if [[ "${pack_manager}" == 'apt-get' ]]; then
            if fnBase_CommandExistIfCheck 'systemctl'; then
                local limit_service_dir=${limit_service_dir:-'/etc/systemd/system/user@.service.d'}
                [[ -d "${limit_service_dir}" ]] || mkdir -p "${limit_service_dir}"
                echo -e "[Service]\nLimitNOFILE=${l_nofile_num}\nLimitNPROC=${l_proc_num}" > "${limit_service_dir}"/limit.conf
                systemctl daemon-reload 1> /dev/null
            fi
        fi
    fi
    fnBase_OperationProcedureResult "${security_limit_config}"

    # - Disable firewire-core
    # fnBase_OperationProcedureStatement 'Disable firewire-core'
    [[ -z $(sed -r -n '/blacklist firewire-core/{p}' /etc/modprobe.d/blacklist.conf 2> /dev/null)  ]] && echo -e '\n# Disable firewire-core\n#blacklist firewire-core' >> /etc/modprobe.d/blacklist.conf
    # fnBase_OperationProcedureResult '/etc/modprobe.d/'

    # - Disable thunderbolt
    # fnBase_OperationProcedureStatement 'Disable Intel thunderbolt interface'
    [[ -z $(sed -r -n '/blacklist thunderbolt/{p}' /etc/modprobe.d/blacklist.conf 2> /dev/null)  ]] && echo -e '\n# Disable thunderbolt\n#blacklist thunderbolt' >> /etc/modprobe.d/blacklist.conf
    # fnBase_OperationProcedureResult '/etc/modprobe.d/'

    # - Disable webcam
    # lsmod | grep uvcvideo
    # fnBase_OperationProcedureStatement 'Disable webcam driver'
    [[ -z $(sed -r -n '/blacklist uvcvideo/{p}' /etc/modprobe.d/blacklist.conf 2> /dev/null)  ]] && echo -e '\n# Disable webcam\n# blacklist uvcvideo' >> /etc/modprobe.d/blacklist.conf
    # fnBase_OperationProcedureResult '/etc/modprobe.d/'

    # - Disable microphone dirver
    # cat /proc/asound/modules
    local l_mircophone_name=${l_mircophone_name:-}
    [[ -s /proc/asound/modules ]] && l_mircophone_name=$(sed -r -n 's@^[[:space:]]*[[:digit:]]+[[:space:]]*(.*)$@\1@g;p' /proc/asound/modules | sort | uniq | sed ':a;N;$!ba;s@\n@ @g')
    if [[ -n "${l_mircophone_name}" ]]; then
        # fnBase_OperationProcedureStatement 'Disable microphone driver'
        [[ -z $(sed -r -n '/blacklist '"${l_mircophone_name}"'/{p}' /etc/modprobe.d/blacklist.conf 2> /dev/null)  ]] && echo -e "\n# Disable microphone\n# blacklist ${l_mircophone_name}" >> /etc/modprobe.d/blacklist.conf
        # fnBase_OperationProcedureResult '/etc/modprobe.d/'
    fi

    # - Disable firewire ohci driver
    fnBase_OperationProcedureStatement 'Disable firewire ohci driver'
    [[ -z $(sed -r -n '/blacklist firewire_ohci/{p}' /etc/modprobe.d/blacklist.conf 2> /dev/null)  ]] && echo -e '\n# Disable firewire ohci\n# blacklist firewire_ohci' >> /etc/modprobe.d/blacklist.conf
    fnBase_OperationProcedureResult '/etc/modprobe.d/'

    # - Disable USB storage devices
    # fnBase_OperationProcedureStatement 'Disable USB storage devices'
    # https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/security_guide/sec-using-usbguard
    # http://linuxtechlab.com/disable-usb-storage-linux/
    # https://www.cyberciti.biz/faq/linux-disable-modprobe-loading-of-usb-storage-driver/

    # Method 1 – Fake install
    # rhel/centos 5.x or older  /etc/modprobe.conf
    # [[ -z $(sed -r -n '/install usb-storage/{p}' /etc/modprobe.conf 2> /dev/null) ]] &&  echo 'install usb-storage : ' >> /etc/modprobe.conf
    # rhel/centos 6.x, 7.x
    # echo 'install usb-storage /bin/true' > /etc/modprobe.d/disable-usb-storage.conf
    # echo 'install usb-storage /bin/true' > /etc/modprobe.d/disable-usb-storage.conf"${bak_suffix}"

    # Method 2 – Removing the USB driver
    # find /lib/modules/$(uname -r) -name *usb-storage*

    # Method 3- Blacklisting USB-storage   not work on centos7
    # load/unload usb-storage module    modprobe [-r] usb-storage
    [[ -z $(sed -r -n '/blacklist usb-storage/{p}' /etc/modprobe.d/blacklist.conf 2> /dev/null)  ]] && echo -e '\n# Disable USB driver\n# blacklist usb_storage\n# blacklist usb-storage' >> /etc/modprobe.d/blacklist.conf
    # fnBase_OperationProcedureResult '/etc/modprobe.d/'


    # - Transparent Huge Pages (THP)
    # THP is a Linux memory management system that reduces the overhead of Translation Lookaside Buffer (TLB) lookups on machines with large amounts of memory by using larger memory pages. You should disable THP on Linux machines to ensure best performance. However it will turn on on system restart. In order to disable them on system startup, you need to add Unit file with script that will disable THP.
    # https://docs.mongodb.com/manual/tutorial/transparent-huge-pages/
    # https://unix.stackexchange.com/questions/99154/disable-transparent-hugepages
    # https://access.redhat.com/solutions/46111
    # https://www.thegeekdiary.com/centos-rhel-7-how-to-disable-transparent-huge-pages-thp/
    fnBase_OperationProcedureStatement 'Transparent Huge Pages (THP)'

    # - check system-wide THP usage
    # grep AnonHugePages /proc/meminfo
    # grep -i HugePages_Total /proc/meminfo
    # egrep 'trans|thp' /proc/vmstat
    # cat /sys/kernel/mm/transparent_hugepage/defrag
    # cat /sys/kernel/mm/transparent_hugepage/enabled

    local transparent_hugepage_dir=${transparent_hugepage_dir:-'/sys/kernel/mm/transparent_hugepage'}
    [[ -d '/sys/kernel/mm/redhat_transparent_hugepage' ]] && transparent_hugepage_dir='/sys/kernel/mm/redhat_transparent_hugepage'
    local khugepaged_defrag_path=${khugepaged_defrag_path:-"${transparent_hugepage_dir}/khugepaged/defrag"}
    local khugepaged_defrag_val=${khugepaged_defrag_val:-0}
    if [[ -s "${khugepaged_defrag_path}" ]]; then
        # rhel 6 -- no
        [[ $(cat "${khugepaged_defrag_path}") =~ ^[0-1]+$ ]] || khugepaged_defrag_val='no'
    fi

    local l_disable_thp_service_path=${l_disable_thp_service_path:-'/etc/systemd/system/disable-thp.service'}

    if fnBase_CommandExistIfCheck 'systemctl'; then
        [[ -s "${l_disable_thp_service_path}" ]] || echo "[Unit]|Description=Disable Transparent Huge Pages (THP)||[Service]|Type=simple|ExecStart=/bin/bash -c \"echo 'never' > transparent_hugepage_dir/enabled 2> /dev/null; echo 'never' > transparent_hugepage_dir/defrag 2> /dev/null; echo 'khugepaged_defrag_val' > khugepaged_defrag_path 2> /dev/null\"||[Install]|WantedBy=multi-user.target" > "${l_disable_thp_service_path}"
    else
        l_disable_thp_service_path='/etc/init.d/disable-thp'
        [[ -s "${l_disable_thp_service_path}" ]] || echo "#!/usr/bin/env bash|# Provides:          disable-transparent-hugepages|# Required-Start:    \$local_fs|# Required-Stop:|# Default-Start:     2 3 4 5|# Default-Stop:      0 1 6|# Short-Description: Disable Linux transparent huge pages|# Description:       Disable Linux transparent huge pages, to improve system performance.||case \"\$1\" in|    start )|        echo 'never' > transparent_hugepage_dir/enabled 2> /dev/null|        echo 'never' > transparent_hugepage_dir/defrag 2> /dev/null|        echo 'khugepaged_defrag_val' > khugepaged_defrag_path 2> /dev/null|        ;;|esac" > "${l_disable_thp_service_path}"
    fi

    sed -i 's@|@\n@g' "${l_disable_thp_service_path}"
    sed -r -i 's@transparent_hugepage_dir@'"${transparent_hugepage_dir}"'@g;' "${l_disable_thp_service_path}"
    sed -r -i 's@khugepaged_defrag_val@'"${khugepaged_defrag_val}"'@g;' "${l_disable_thp_service_path}"
    sed -r -i 's@khugepaged_defrag_path@'"${khugepaged_defrag_path}"'@g;' "${l_disable_thp_service_path}"
    fnBase_SystemServiceManagement "${l_disable_thp_service_path##*/}" 'enable'
    fnBase_OperationProcedureResult "${l_disable_thp_service_path}"


    # - Storage I/O
    fnBase_OperationProcedureStatement 'Storage I/O'
    find /sys/block/* -print | while IFS="" read -r line; do
        if [[ -d "${line}" ]]; then
            local block_queue_dir="${line}"
            [[ -s "${block_queue_dir}/queue/rq_affinity" ]] && echo 2 > "${block_queue_dir}"/queue/rq_affinity 2> /dev/null
            [[ -s "${block_queue_dir}/queue/scheduler" ]] && echo 'noop' > "${block_queue_dir}"/queue/scheduler 2> /dev/null
            [[ -s "${block_queue_dir}/queue/nr_requests" ]] && echo 256 > "${block_queue_dir}"/queue/nr_requests 2> /dev/null
            [[ -s "${block_queue_dir}/queue/read_ahead_kb" ]] && echo 256 > "${block_queue_dir}"/queue/read_ahead_kb 2> /dev/null
        fi
    done
    fnBase_OperationProcedureResult '/sys/block/'

    #  - ip addr resolver
    # http://www.tldp.org/LDP/solrhe/Securing-Optimizing-Linux-RH-Edition-v1.3/chap5sec39.html
    #  Linux uses a resolver library to obtain the IP address corresponding to a host name. The /etc/host.conf file specifies how names are resolved.
    local l_hosts_conf='/etc/host.conf'
    if [[ -s "${l_hosts_conf}" ]]; then
        fnBase_OperationProcedureStatement 'Ip addr resolver'
        sed -r -i '/^#?[[:space:]]*multi[[:space:]]+/{s@^.*$@multi on@g}' "${l_hosts_conf}"
        sed -r -i '/^#?[[:space:]]*order[[:space:]]+/{s@^.*$@order bind,hosts@g}' "${l_hosts_conf}"
        sed -r -i '/^#?[[:space:]]*nospoof[[:space:]]+/{s@^.*$@nospoof on@g}' "${l_hosts_conf}"
        sed -r -i '/^#?[[:space:]]*spoofalert[[:space:]]+/{s@^.*$@spoofalert on@g}' "${l_hosts_conf}"
        sed -r -i '/^#?[[:space:]]*spoof[[:space:]]+/{s@^.*$@spoof warn@g}' "${l_hosts_conf}"
        fnBase_OperationProcedureResult "${l_hosts_conf}"
    fi


    # - enforcing read-only mounting of removable media
    # https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/security_guide/chap-hardening_your_system_with_tools_and_services#sect-Security_Guide-Workstation_Security-Enforcing_Read-Only_Mounting_of_Removable_Media
    if [[ -d '/etc/udev/rules.d' ]]; then
        local l_80_rule=${l_80_rule:-'/etc/udev/rules.d/80-readonly-removables.rules'}
        if [[ ! -s "${l_80_rule}" ]]; then
            fnBase_OperationProcedureStatement "Enforcing read-only mounting of removable media"
            echo "SUBSYSTEM==\"block\",ATTRS{removable}==\"1\",RUN{program}=\"/sbin/blockdev --setro %N\"" > "${l_80_rule}"

            if fnBase_CommandExistIfCheck 'udevadm'; then
                udevadm trigger &> /dev/null
                udevadm control --reload &> /dev/null
            fi
            fnBase_OperationProcedureResult
        fi    # end if l_80_rule
    fi


    # - bash TMOUT / HISTTIMEFORMAT
    local bash_configuration_profile=${bash_configuration_profile:-'/etc/bashrc'}
    [[ -s '/etc/bash.bashrc' ]] && bash_configuration_profile='/etc/bash.bashrc'
    if [[ -s "${bash_configuration_profile}" ]]; then
        fnBase_OperationProcedureStatement 'Bash shell optimization'
        sed -r -i '/Bash custom setting start/,/Bash custom setting end/d' "${bash_configuration_profile}"
        # append
        local l_timeout=${l_timeout:-600}
        echo -e "# Bash custom setting start\n# automatic logout timeout (seconds)\nTMOUT=${l_timeout}\nHISTTIMEFORMAT=\"%F %T %a %z \"\n# Bash custom setting end" >> "${bash_configuration_profile}"
        fnBase_OperationProcedureResult "${bash_configuration_profile}"
    fi

    # - Disable Ctrl+Alt+Del
    # https://www.linuxtechi.com/disable-reboot-using-ctrl-alt-del-keys/
    fnBase_OperationProcedureStatement 'Disable Ctrl+Alt+Del'
    if fnBase_CommandExistIfCheck 'systemctl'; then
        [[ $(systemctl is-enabled ctrl-alt-del.target) != 'masked' ]] && systemctl mask ctrl-alt-del.target &> /dev/null
        # systemctl daemon-reload
    elif [[ -s '/etc/init/control-alt-delete.conf' ]]; then
        # rhel/centos 6
        if [[ ! -s '/etc/init/control-alt-delete.override' ]]; then
            cp -f /etc/init/control-alt-delete.conf /etc/init/control-alt-delete.override
            echo -e "exec /usr/bin/logger -p authpriv.notice -t init \"Ctrl-Alt-Del has been disabled\"" > /etc/init/control-alt-delete.override
        fi
    elif [[ -s '/etc/inittab' ]]; then
        # rhel/centos 5
        sed -r -i '/^ca::ctrlaltdel:\/sbin\/shutdown/{s@^@#@g;}' /etc/inittab &> /dev/null
        sed -r -i '/ca::ctrlaltdel:\/sbin\/shutdown/a ca::ctrlaltdel:/bin/logger -p authpriv.warning -t init "Console-invoked Ctrl-Alt-Del has been disabled"' /etc/inittab &> /dev/null
    fi
    fnBase_OperationProcedureResult


    # - Disable Zeroconf Networking   169.254.0.0/16
    # http://blog.omotech.com/?p=1005
    fnBase_OperationProcedureStatement 'Disable zeroconf networking'
    local l_sysconfig_network='/etc/sysconfig/network'
    [[ "${pack_manager}" == 'zypper' ]] && l_sysconfig_network='/etc/sysconfig/network/config'
    if [[ -s "${l_sysconfig_network}" ]]; then
        sed -r -i '/NOZEROCONF=/d' "${l_sysconfig_network}" &> /dev/null
        # append
        echo "NOZEROCONF=yes" >> "${l_sysconfig_network}"
    fi
    fnBase_OperationProcedureResult "${l_sysconfig_network}"

    # - Partition mount options
    local l_fstab_path=${l_fstab_path:-'/etc/fstab'}
    if [[ -s "${l_fstab_path}" ]]; then
        fnBase_OperationProcedureStatement 'Partition mount options'
        # /boot, /tmp, /var/tmp, /var/log    defaults,nosuid,noexec,nodev
        sed -r -i '/[[:space:]]+\/(boot|tmp|var\/tmp|var\/log)[[:space:]]+.*defaults/{s@(defaults)([[:space:]]+)@\1,nosuid,noexec,nodev\2@g;}' "${l_fstab_path}" &> /dev/null
        # /var   defaults,nosuid
        sed -r -i '/[[:space:]]+\/(var)[[:space:]]+.*defaults/{s@(defaults)([[:space:]]+)@\1,nosuid\2@g;}' "${l_fstab_path}" &> /dev/null
        fnBase_OperationProcedureResult "${l_fstab_path}"
    fi


    #  - Just allow root login via local terminal
    # if [[ -s '/etc/securetty' && ! -s "/etc/securetty${bak_suffix}" ]]; then
    #     fnBase_OperationProcedureStatement 'Just allow root login via local terminal'
    #     cp /etc/securetty "/etc/securetty${bak_suffix}"
    #     echo "tty1" > /etc/securetty
    #     chmod 700 /root &> /dev/null
    #     fnBase_OperationProcedureResult '/etc/securetty'
    # fi

    #  - TCP Wrappers
    # block all but SSH
    # echo "ALL:ALL" >> /etc/hosts.deny
    # echo "sshd:ALL" >> /etc/hosts.allow
}

fn_RecordUserLoginSessionInfo(){
    # http://www.2daygeek.com/automatically-record-all-users-terminal-sessions-activity-linux-script-command
    # https://unix.stackexchange.com/questions/25639/how-to-automatically-record-all-your-terminal-sessions-with-script-utility

    # **Attention** This setting may results in utility "rsync" not work, prompt the following error:
    # protocol version mismatch -- is your shell clean?
    # (see the rsync man page for an explanation)
    # rsync error: protocol incompatibility (code 2) at compat.c(178) [sender=3.1.2]

    fnBase_OperationProcedureStatement 'Record user login session'
    local session_record_dir=${session_record_dir:-'/var/log/session'}
    if [[ ! -d "${session_record_dir}" ]]; then
        mkdir -p "${session_record_dir}"
        chmod 1777 "${session_record_dir}"
        chattr +a "${session_record_dir}"
    fi

    local session_record_profile=${session_record_profile:-'/etc/bashrc'}
    [[ -s '/etc/bash.bashrc' ]] && session_record_profile='/etc/bash.bashrc'
    sed -r -i '/Record terminal sessions start/,/Record terminal sessions end/d' "${session_record_profile}"

    # append
    # tee -a "${session_record_profile}" 1>/dev/null <<EOF
    # cat >> "${session_record_profile}" <<EOF
    echo -e "# Record terminal sessions start\nlogin_ip=\${login_ip:-}\nif [[ -n \"\${SSH_CLIENT:-}\" ]]; then\n    login_ip=\"\${SSH_CLIENT%% *}\"\nelif [[ -n \"\${SSH_CONNECTION:-}\" ]]; then\n    login_ip=\"\${SSH_CONNECTION%% *}\"\nelse\n    login_ip=\$(who | sed -r -n '\$s@.*\(([^\)]+)\).*@\1@gp')\n    [[ \"\${login_ip}\" == \":0\" ]] && login_ip='127.0.0.1'\nfi\n\nif [[ \"X\${SESSION_RECORD:-}\" == 'X' ]]; then\n    login_timestamp=\$(date +\"%Y%m%d-%a-%H%M%S\")\n    # \$\$ current bash process ID (PID)\n    if [[ -z \"\${login_ip}\" ]]; then\n        record_output_path=\"/var/log/session/\${login_timestamp}_\${USER}_r\${RANDOM}.log\"\n    else\n        record_output_path=\"/var/log/session/\${login_timestamp}_\${USER}_\${login_ip}_r\${RANDOM}.log\"\n    fi\n\n    SESSION_RECORD='start'\n    export SESSION_RECORD\n    # /usr/bin/script blongs to package util-linux or util-linux-ng\n    script -t -f -q 2>\"\${record_output_path}.timing\" \"\${record_output_path}\"\n    exit\nfi\n\n# ps -ocommand= -p \$PPID\n# Record terminal sessions end" >> "${session_record_profile}"

    fnBase_OperationProcedureResult "${session_record_dir}"
}

fn_AuditdDirectiveSetting(){
    local l_item="${1:-}"
    local l_val="${2:-}"
    local l_config="${3:-'/etc/audit/auditd.conf'}"
    if [[ -n "${l_item}" && -n "${l_val}" && -s "${l_config}" ]]; then
        sed -r -i '/^[[:space:]]*#?[[:space:]]*'"${l_item}"'[[:space:]]*=/{s@^[[:space:]]*#?[[:space:]]*([^[:space:]]+[[:space:]]*=).*@\1 '"${l_val}"'@g;}' "${l_config}"
    fi
}

fn_AuditOperation(){
    fnBase_OperationPhaseStatement 'System Audit'

    # - Record All User Terminal Sessions
    [[ "${log_user_session}" -eq 1 ]] && fn_RecordUserLoginSessionInfo

    #  - Log
    # rsyslog - reliable system and kernel logging daemon
    # /etc/rsyslog.conf
    # change SyslogFacility val from INFO --> AUTHPRIV in /etc/ssh/sshd_config
    # authpriv.* /var/log/secure   in /etc/rsyslog.conf
    if ! fnBase_CommandExistIfCheck 'rsyslogd'; then
        fn_PackageOperationProcedureStatement 'install' 'rsyslog' 'rsyslogd'
        # https://askubuntu.com/questions/254855/how-do-i-redirect-google-chrome-logs-out-of-my-kern-log
        if [[ -d '/etc/rsyslog.d' ]]; then
            [[ -f '/etc/rsyslog.d/30-seccomp.conf' ]] || echo -e "if \$msg contains 'comm=\"chrome\"' then stop\nif \$msg contains 'comm=\"Chrome_IOThread\"' then stop\nif \$msg contains 'proctitle' then stop" > /etc/rsyslog.d/30-seccomp.conf
        fi
    fi

    # ARP monitoring - arpon,arpwatch;   arpwatch: arpwatch / arpsnmp
    local l_arp_name=${l_arp_name:-'arpwatch'}
    [[ "${pack_manager}" == 'apt-get' && "${distro_name}" == 'ubuntu' ]] || l_arp_name='arpon'

    if ! fnBase_CommandExistIfCheck "${l_arp_name}"; then
        fnBase_OperationProcedureStatement "ARP monitoring"
        fnBase_PackageManagerOperation 'install' "${l_arp_name}"

        local l_arpon_config
        case "${l_arp_name,,}" in
            arpon )
                l_arpon_config='/etc/default/arpon'
                if [[ -f "${l_arpon_config}" ]]; then
                    # Modify to RUN="yes" when you are ready
                    sed -r -i '/^RUN=/{s@^([^=]+=).*@\1"yes"@g;}' "${l_arpon_config}"
                fi
                ;;
            arpwatch )
                l_arpon_config='/etc/default/arpwatch'
                [[ -f '/etc/sysconfig/arpwatch' ]] && l_arpon_config='/etc/sysconfig/arpwatch'
                ;;
        esac

        fnBase_SystemServiceManagement "${l_arp_name}" 'enable'
        fnBase_SystemServiceManagement "${l_arp_name}" 'start'
        fnBase_OperationProcedureResult "${l_arp_name}"
    fi

    # - User-specific process accounting
    # http://mewbies.com/how_to_use_acct_process_system_accouting_tutorial.htm
    # https://www.tecmint.com/how-to-monitor-user-activity-with-psacct-or-acct-tools/
    # ac, lastcomm, accton and sa
    if ! fnBase_CommandExistIfCheck 'ac'; then
        fnBase_OperationProcedureStatement 'User-specific process accounting'
        local acct_name
        local acct_record_file
        case "${pack_manager}" in
            dnf|yum )
                acct_name='psacct'
                acct_record_file='/var/account/pacct'
            ;;
            * )
                acct_name='acct'
                acct_record_file='/var/log/account/pacct'
            ;;
        esac
        # read record file via command dump-acct, e.g. dump-acct /var/log/account/pacct
        fnBase_PackageManagerOperation 'install' "${acct_name}"
        # ac / lastcomm / accton / sa / last / lastb
        # /etc/default/acct    /etc/cron.daily/acct
        [[ -s '/usr/bin/ac' ]] && chmod 750 '/usr/bin/ac'
        fnBase_SystemServiceManagement "${acct_name}" 'enable'
        # empty file content
        [[ -f "${acct_record_file}" ]] && > "${acct_record_file}"
        fnBase_SystemServiceManagement "${acct_name}" 'restart'
        # Activate process accounting and use default file
        fnBase_CommandExistIfCheck 'accton' && accton on &> /dev/null
        fnBase_OperationProcedureResult "${acct_name} ${acct_record_file}"
    fi

    # - User space tool for kernel auditing
    # audit/auditd: audispd/auditctl/auditd/aulast/aulastlog/aureport/ausearch/ausyscall/autrace/auvirt
    local l_auditd_install=${l_auditd_install:-1}
    if [[ "${l_auditd_install}" -eq 1 ]]; then
        fnBase_OperationProcedureStatement 'User space tools for kernel auditing'
        # https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/security_guide/chap-system_auditing
        # https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/security_guide/sec-understanding_audit_log_files
        # https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/security_guide/app-audit_reference#sec-Audit_Events_Fields

        # http://man7.org/linux/man-pages/man2/seccomp.2.html
        # https://en.wikipedia.org/wiki/Seccomp
        # ausearch -m seccomp --raw | aureport --event --summary -i
        # strace -S calls -c -p

        local l_audit_name=${l_audit_name:-'audit'}
        [[ "${pack_manager}" == 'apt-get' ]] && l_audit_name='auditd'
        fnBase_CommandExistIfCheck 'auditd' || fnBase_PackageManagerOperation 'install' "${l_audit_name}"
        # auditctl -v 2>&1 | sed -r -n 's@^[^[:digit:]]+([[:digit:].]+)@\1@g;p'

        # - auditd.conf configuration
        local l_audit_conf=${l_audit_conf:-'/etc/audit/auditd.conf'}
        if [[ -s "${l_audit_conf}" ]]; then
            # https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/security_guide/sec-configuring_the_audit_service
            # log_file = /var/log/audit/audit.log
            fn_AuditdDirectiveSetting 'num_logs' '16' "${l_audit_conf}"
            # log_format raw/nolog
            fn_AuditdDirectiveSetting 'log_format' 'raw' "${l_audit_conf}"
            # the maximum size of a single Audit log file
            fn_AuditdDirectiveSetting 'max_log_file' '128' "${l_audit_conf}"
            fn_AuditdDirectiveSetting 'max_log_file_action' 'keep_logs' "${l_audit_conf}"
            # space_left(xxx) must be larger than admin_space_left(xxx)
            fn_AuditdDirectiveSetting 'space_left' '512' "${l_audit_conf}"
            # default is 'SYSLOG'. It is recommended to set the space_left_action parameter to 'email' or 'exec' with an appropriate notification method.
            fn_AuditdDirectiveSetting 'space_left_action' 'email' "${l_audit_conf}"
            fn_AuditdDirectiveSetting 'action_mail_acct' 'root' "${l_audit_conf}"
            fn_AuditdDirectiveSetting 'admin_space_left' '256' "${l_audit_conf}"
            # Should be set to 'single' to put the system into single-user mode and allow the administrator to free up some disk space.
            fn_AuditdDirectiveSetting 'admin_space_left_action' 'single' "${l_audit_conf}"
            # Specifies an action that is triggered when no free space is available on the partition that holds the Audit log files, must be set to 'halt' or 'single'.
            fn_AuditdDirectiveSetting 'disk_full_action' 'single' "${l_audit_conf}"
            # Specifies an action that is triggered in case an error is detected on the partition that holds the Audit log files, must be set to 'syslog', 'single', or 'halt'.
            fn_AuditdDirectiveSetting 'disk_error_action' 'single' "${l_audit_conf}"
            local l_audit_flush=${l_audit_flush:-'incremental'}
            [[ "${pack_manager}" != 'apt-get' ]] && l_audit_flush='incremental_async'
            fn_AuditdDirectiveSetting 'flush' "${l_audit_flush}" "${l_audit_conf}"
        fi

        # - audit rules
        # https://bugzilla.redhat.com/show_bug.cgi?id=654883
        # auditctl: The audit system is in immutable mode, no rule changes allowed

        # Control rules: Allow the Audit system\'s behavior and some of its configuration to be modified.
        # File system rules: Also known as file watches, allow the auditing of access to a particular file or a directory.
        # System call rules: Allow logging of system calls that any specified program makes.

        # file system rule:   auditctl -w path_to_file -p permissions -k key_name
        # systemctl call rule:   auditctl -a action,filter -S system_call -F field=value -k key_name
        # system call rule:  auditctl -a action,filter -S system_call -F field=value -k key_name

        # nispom.rules -- Information System Security chapter of the National Industrial Security Program Operating Manual.
        # stig.rules -- Security Technical Implementation Guides (STIG).
        # pci-dss-v31.rules -- Payment Card Industry Data Security Standard (PCI DSS) v3.1.

        # stig / nispom
        # local l_rule_specify=${l_rule_specify:-'stig'}

        local l_rule_dir=${l_rule_dir:-'/etc/audit/rules.d'}
        local l_rule_path=${l_rule_path:-'/etc/audit/audit.rules'}
        local l_custome_rule_path

        # suse: /usr/share/doc/packages/audit
            # capp.rules  lspp.rules	nispom.rules  stig.rules
        # rhel/centos: /usr/share/doc/audit-2.7.6/rules
            # 30-nispom.rules  30-pci-dss-v31.rules  30-stig.rules
        # debian: /usr/share/doc/auditd/examples/rules
            # 30-nispom.rules.gz  30-pci-dss-v31.rules.gz  30-stig.rules.gz
        # ubuntu: /usr/share/doc/auditd/examples
            # capp.rules.gz lspp.rules.gz nispom.rules.gz stig.rules.gz

        case "${distro_name}" in
            centos|rhel|fedora|amzn )
                # put specified rule under /etc/audit/rules.d/
                l_custome_rule_path="${l_rule_dir}/${auditd_custom_rule##*/}"
                ;;
            debian )
                # put specified rule under /etc/audit/rules.d/
                l_custome_rule_path="${l_rule_dir}/${auditd_custom_rule##*/}"
                ;;
            ubuntu )
                # overwite /etc/audit/audit.rules
                l_custome_rule_path="${l_rule_path}"
                ;;
            opensuse|sles )
                # overwite /etc/audit/audit.rules
                l_custome_rule_path="${l_rule_path}"
                ;;
        esac

        [[ -n "${l_custome_rule_path}" ]] && $download_method "${auditd_custom_rule}" > "${l_custome_rule_path}"
        [[ -d '/etc/sysconfig/network-scripts' ]] && sed -r -i '/\/etc\/sysconfig\/network-scripts/{s@^#@@g;}' "${l_custome_rule_path}"

        fnBase_SystemServiceManagement 'auditd' 'restart'

        # augenrules --load
        # ausearch -m LOGIN --start today -i
        # ausearch -a 27020
        # ausearch -f /etc/passwd [-i]
        # aureport -x --summary
        # aureport --failed
        # autrace /usr/bin/sudo
        # ausearch -i -p 28278
        # ausearch –start recent -p 21023 –raw | aureport –file –summary
        fnBase_OperationProcedureResult "${l_audit_name}"
    fi
}


fn_Fail2banOperation(){
    # https://www.linode.com/docs/security/using-fail2ban-for-security/
    # https://www.howtoforge.com/tutorial/how-to-install-fail2ban-on-centos/
    # https://unix.stackexchange.com/questions/171567/installing-fail2ban-on-centos-7

    # journalctl -lfu fail2ban
    # Checking the banned IPs by Fail2Ban
    # iptables -L -n
    # Check the Fal2Ban Status
    # fail2ban-client status
    # Unbanning an IP address
    # fail2ban-client set 'sshd' unbanip 'IPADDRESS'

    if ! fnBase_CommandExistIfCheck 'fail2ban-client'; then
        fnBase_OperationProcedureStatement 'Intrusion Prevention'

        local l_fail2ban_packs=${l_fail2ban_packs:-'fail2ban'}
        case "${pack_manager}" in
            yum )
                fnBase_CommandExistIfCheck 'systemctl' && l_fail2ban_packs="${l_fail2ban_packs} fail2ban-systemd"
                ;;
        esac
        fnBase_PackageManagerOperation 'install' "${l_fail2ban_packs}"

        if fnBase_CommandExistIfCheck 'fail2ban-client'; then
            fnBase_SystemServiceManagement 'fail2ban' 'enable'
        fi

        # configuation file
        local l_jail_origin_path=${l_jail_origin_path:-'/etc/fail2ban/jail.conf'}

        if [[ -f "${l_jail_origin_path}" ]]; then
            local l_jail_path=${l_jail_path:-'/etc/fail2ban/jail.local'}
            [[ -s "${l_jail_origin_path}" && ! -s "${l_jail_path}" ]] && cp -pf "${l_jail_origin_path}" "${l_jail_path}" 2> /dev/null

            funcFail2banDirectiveSetting(){
                local l_item="${1:-}"
                local l_val="${2:-}"
                local l_path="${3:-'/etc/fail2ban/jail.local'}"

                if [[ -n "${l_item}" && -n "${l_val}" && -s "${l_path}" ]]; then
                    sed -r -i '/^'"${l_item}"'[[:space:]]*=/{s@^([^=]+=[[:space:]]*).*@\1'"${l_val}"'@g;}' "${l_path}"
                fi
            }

            funcFail2banDirectiveSetting 'bantime' '7200'
            funcFail2banDirectiveSetting 'maxretry' '3'
            funcFail2banDirectiveSetting 'findtime' '1200'
            funcFail2banDirectiveSetting 'bantime' '7200'
            funcFail2banDirectiveSetting 'bantime' '7200'
            [[ -n "${email_address}" ]] && funcFail2banDirectiveSetting 'destemail' "${email_address}"

            case "${pack_manager}" in
                yum|dnf )
                    # default is backend = auto
                    fnBase_CommandExistIfCheck 'systemctl' && funcFail2banDirectiveSetting 'backend' 'systemd'
                    ;;
            esac

            # ssh enable
            if [[ -n $(sed -r -n '/^\[sshd\]/,/^\[/{/^enabled[[:space:]]*=/{p}}' "${l_jail_path}" 2> /dev/null) ]]; then
                sed -r -i '/^\[sshd\]/,/^\[/{/^enabled[[:space:]]*=/{s@^([^=]+=[[:space:]]*).*@\1yes@g}}' "${l_jail_path}" 2> /dev/null
            else
                sed -r -i '/^\[sshd\]/aenabled = true' "${l_jail_path}"
            fi

            fnBase_CommandExistIfCheck 'fail2ban-client' && fnBase_SystemServiceManagement 'fail2ban' 'restart'
        fi

        if fnBase_CommandExistIfCheck 'fail2ban-client'; then
            fnBase_OperationProcedureResult 'Fail2Ban'
        else
            fnBase_OperationProcedureResult 'Fail2Ban' 1
        fi

    fi
}

fn_RkhunterDirectiveSetting(){
    local l_config="${1:-}"
    local l_item="${2:-}"
    local l_val="${3:-}"

    if [[ -s "${l_config}" && -n "${l_item}" ]]; then
        if [[ -n "${l_val}" ]]; then
            sed -r -i '/^#?[[:space:]]*'"${l_item}"'=/{s@^#?[[:space:]]*([^=]*=).*$@\1'"${l_val}"'@g;}' "${l_config}" &> /dev/null
        else
            sed -r -i '/^#?[[:space:]]*'"${l_item}"'=/{s@^#?[[:space:]]*(.*)$@\1@g;}' "${l_config}" &> /dev/null
        fi
    fi
}

fn_AideDirectiveSetting(){
    local l_config="${1:-}"
    local l_item="${2:-}"
    local l_val="${3:-}"

    if [[ -s "${l_config}" && -n "${l_item}" ]]; then
        if [[ -n "${l_val}" ]]; then
            sed -r -i '/^#?[[:space:]]*'"${l_item}"'[[:space:]]*=/{s@^#?[[:space:]]*([^=]*=[[:space:]]*).*$@\1'"${l_val}"'@g;}' "${l_config}" &> /dev/null
        else
            sed -r -i '/^#?[[:space:]]*'"${l_item}"'[[:space:]]*=/{s@^#?[[:space:]]*(.*)$@\1@g;}' "${l_config}" &> /dev/null
        fi
    fi
}

fn_SecurityOperation(){
    fnBase_OperationPhaseStatement 'System Security'

    # - Intrusion Prevention
    fn_Fail2banOperation

    # - Web server scanner
    if ! fnBase_CommandExistIfCheck 'nikto'; then
        fnBase_OperationProcedureStatement 'Web server scanner'
        fnBase_PackageManagerOperation 'install' 'nikto'
        nikto -update &> /dev/null
        fnBase_OperationProcedureResult 'nikto'

        # nikto -C all -h https://www.google.com -p 443
    fi

    # Rootkit Detection    /etc/cron.*/rkhunter
    # chkrootkit - rootkit detector
    # rkhunter - rootkit, backdoor, sniffer and exploit scanner
    if ! fnBase_CommandExistIfCheck 'rkhunter'; then
        # https://www.digitalocean.com/community/tutorials/how-to-use-rkhunter-to-guard-against-rootkits-on-an-ubuntu-vps
        # /etc/rkhunter.conf    /etc/default/rkhunter
        # /var/log/rkhunter/rkhunter.log      /var/log/rkhunter.log
        fnBase_OperationProcedureStatement "Rootkit detection"
        local rootkit_name=${rootkit_name:-'rkhunter'}
        fnBase_PackageManagerOperation 'install' "${rootkit_name}"
        rkhunter --update -q &> /dev/null

        local l_rkhunter_conf=${l_rkhunter_conf:-'/etc/rkhunter.conf'}
        if [[ -s "${l_rkhunter_conf}" ]]; then
            # mail to
            if [[ -n "${email_address}" ]]; then
                fn_RkhunterDirectiveSetting "${l_rkhunter_conf}" 'MAIL-ON-WARNING' "${email_address}"
            else
                fn_RkhunterDirectiveSetting "${l_rkhunter_conf}" 'MAIL-ON-WARNING' 'root'
            fi
            fn_RkhunterDirectiveSetting "${l_rkhunter_conf}" 'MAIL_CMD'
            fn_RkhunterDirectiveSetting "${l_rkhunter_conf}" 'ALLOW_SSH_PROT_V1' '0'
            fn_RkhunterDirectiveSetting "${l_rkhunter_conf}" 'SSH_CONFIG_DIR'

            local l_ssh_permit_root_login=${l_ssh_permit_root_login:-}
            [[ -s '/etc/ssh/sshd_config' ]] && l_ssh_permit_root_login=$(sed -r -n '/^[[:space:]]*PermitRootLogin/{s@^[[:space:]]*[^[:space:]]*[[:space:]]*(.*)$@\1@g;p}' /etc/ssh/sshd_config 2> /dev/null)
            [[ -n "${l_ssh_permit_root_login}" ]] && fn_RkhunterDirectiveSetting "${l_rkhunter_conf}" 'ALLOW_SSH_ROOT_USER' "${l_ssh_permit_root_login}"
        fi

        # /etc/cron.*/rkhunter and /etc/apt/apt.conf.d/90rkhunter
        local l_rkhunter_cron_conf=${l_rkhunter_cron_conf:-'/etc/default/rkhunter'}
        if [[ "${cron_task}" -eq 1 && -s "${l_rkhunter_cron_conf}" ]]; then
            fn_RkhunterDirectiveSetting "${l_rkhunter_conf}" 'CRON_DAILY_RUN' "\"yes\""
            fn_RkhunterDirectiveSetting "${l_rkhunter_conf}" 'CRON_DB_UPDATE' "\"yes\""
            fn_RkhunterDirectiveSetting "${l_rkhunter_conf}" 'DB_UPDATE_EMAIL' "\"yes\""
            # email to
            if [[ -n "${email_address}" ]]; then
                fn_RkhunterDirectiveSetting "${l_rkhunter_conf}" 'REPORT_EMAIL' "\"${email_address}\""
            else
                fn_RkhunterDirectiveSetting "${l_rkhunter_conf}" 'REPORT_EMAIL' "\"root\""
            fi
            fn_RkhunterDirectiveSetting "${l_rkhunter_conf}" 'APT_AUTOGEN' "\"yes\""
            fn_RkhunterDirectiveSetting "${l_rkhunter_conf}" 'NICE' "\"-5\""
            fn_RkhunterDirectiveSetting "${l_rkhunter_conf}" 'RUN_CHECK_ON_BATTERY' "\"false\""
        fi

        # For rhel/centos
        # /etc/sysconfig/rkhunter

        # rkhunter --update
        # rkhunter -C
        # rkhunter --propupd
        # rkhunter -c --sk [--rwo]
        # rkhunter -c --enable all --disable none [--rwo]

        # --sk means skip to push Enter key
        # --rwo means display only warnings

        fnBase_OperationProcedureResult "${rootkit_name}"
    fi

    # clamav: Clam AntiVirus is an anti-virus toolkit for UNIX. The main purpose of this software is the integration with mail servers (attachment scanning).
    if fnBase_CommandExistIfCheck 'systemctl'; then
        if ! fnBase_CommandExistIfCheck 'freshclam'; then
            fnBase_OperationProcedureStatement "Anti-virus toolkit"
            # https://guylabs.ch/2013/09/18/install-clamav-antivirus-in-ubuntu-server-and-client/
            # https://hostpresto.com/community/tutorials/how-to-install-clamav-on-centos-7/
            # https://linux-audit.com/install-clamav-on-centos-7-using-freshclam/
            local clam_name='clamav'
            local clam_service_name=''
            case "${pack_manager}" in
                apt-get )
                    # https://askubuntu.com/questions/589318/freshclam-error-clamd-conf-file-not-found/632911
                    clam_service_name='clamav-freshclam'
                    # clamav-daemon is used to create file /etc/clamav/clamd.conf
                    # /etc/clamav/freshclam.conf -- 'NotifyClamd /etc/clamav/clamd.conf'
                    fnBase_PackageManagerOperation 'install' 'clamav clamav-daemon clamav-freshclam'
                    if [[ -s '/etc/clamav/freshclam.conf' ]]; then
                        # Check for new database 6 times a day
                        sed -r -i '/^#?[[:space:]]*Checks/{s@^#?[[:space:]]*([^[:space:]]*[[:space:]]).*$@\16@g;p}' /etc/clamav/freshclam.conf
                    fi
                    ;;
                dnf|yum )
                    clam_service_name='clamd@scan'
                    fnBase_PackageManagerOperation 'install' 'clamav clamav-scanner clamav-server clamav-devel clamav-update clamav-scanner-systemd clamav-scanner-systemd clamav-scanner-sysvinit clamav-server-sysvinit'

                    if [[ -s '/etc/clamd.d/scan.conf' ]]; then
                        sed -r -i '/^Example$/{s@^#?@#@g;}' /etc/clamd.d/scan.conf
                        sed -r -i '/LocalSocket[[:space:]]+/{s@^#?@@g;}' /etc/clamd.d/scan.conf
                    fi

                    if [[ -s '/etc/freshclam.conf' ]]; then
                        sed -r -i '/^Example$/{s@^#?@#@g;}' /etc/freshclam.conf
                    fi
                    ;;
                zypper )
                    clam_service_name='freshclam'
                    fnBase_PackageManagerOperation 'install' "${clam_name}"
                    [[ -s '/etc/clamd.conf' ]] && sed -r -i '/LocalSocket[[:space:]]+/{s@^#?@@g;}' /etc/clamd.conf
                    ;;
            esac

            if fnBase_CommandExistIfCheck 'freshclam'; then
                # SELinux configuation
                fnBase_SELinuxSemanageOperation 'antivirus_can_scan_system' 'on' 'boolean'
                fnBase_SELinuxSemanageOperation 'clamd_use_jit' 'on' 'boolean'

                fnBase_SystemServiceManagement "${clam_service_name}" 'stop'
                freshclam &> /dev/null
                fnBase_SystemServiceManagement "${clam_service_name}" 'enable'
            fi
            # usage: clamscan -r --bell -i /etc
            fnBase_OperationProcedureResult "${clam_name}"
        fi
    fi

    # aide - Advanced Intrusion Detection Environment    /etc/cron.daily/aide
    if ! fnBase_CommandExistIfCheck 'aide'; then
        # https://www.tecmint.com/check-integrity-of-file-and-directory-using-aide-in-linux/
        fnBase_OperationProcedureStatement "Intrusion detection environment"

        local aide_name='aide'
        local aide_dbname_init
        local aide_dbname_new

        # /etc/default/aide    /etc/aide/aide.conf
        if [[ "${pack_manager}" == 'apt-get' ]]; then
            # auto install email client postfix/exim4

            # https://major.io/2015/10/14/what-i-learned-while-securing-ubuntu/
            # http://sysblog.sund.org/using-aide-on-ubuntu/
            # https://hungred.com/how-to/install-aide-intrusion-detection-system-ubuntu/
            aide_dbname_init='/var/lib/aide/aide.db.new'
            aide_dbname_new='/var/lib/aide/aide.db'
            # package: aide - static binary / aide-common - Common files / aide-dynamic - dynamic binary
            fnBase_PackageManagerOperation 'install' "${aide_name}"
            fnBase_PackageManagerOperation 'install' 'aide-common aide-dynamic'
            # command aideinit is in package aide-common
            aideinit -f &> /dev/null
            [[ -s "${aide_dbname_new}" ]] && rm -f "${aide_dbname_new}"
            [[ -s "${aide_dbname_init}" ]] && mv "${aide_dbname_init}" "${aide_dbname_new}"
            # usage: aide.wrapper -C / --check
        else
            aide_dbname_init='/var/lib/aide/aide.db.new.gz'
            aide_dbname_new='/var/lib/aide/aide.db.gz'
            fnBase_PackageManagerOperation 'install' "${aide_name}"
            aide -i &> /dev/null
            [[ -s "${aide_dbname_new}" ]] && rm -f "${aide_dbname_new}"
            [[ -s "${aide_dbname_init}" ]] && mv "${aide_dbname_init}" "${aide_dbname_new}"
            # usage: aide --check / -C
        fi

        # /etc/aide.conf
        local l_aide_conf=${l_aide_conf:-'/etc/aide.conf'}
        if [[ -f "${l_aide_conf}" ]]; then
            sed -r -i '/^NORMAL[[:space:]]*=/d' "${l_aide_conf}"
            fn_AideDirectiveSetting "${l_aide_conf}" 'NORMAL' 'sha512'

            # Content + file type.
            # CONTENT = sha256+ftype

            # Extended content + file type + access.
            # CONTENT_EX = sha256+ftype+p+u+g+n+acl+selinux+xattrs
        fi

        # /etc/cron.daily/aide for APT
        local l_aide_cron_conf=${l_aide_cron_conf:-'/etc/default/aide'}
        if [[ "${cron_task}" -eq 1 && -s "${l_aide_cron_conf}" ]]; then
            fn_AideDirectiveSetting "${l_aide_cron_conf}" 'CRON_DAILY_RUN' 'yes'
            # email to
            if [[ -n "${email_address}" ]]; then
                fn_AideDirectiveSetting "${l_aide_cron_conf}" 'MAILTO' "${email_address}"
            else
                fn_AideDirectiveSetting "${l_aide_cron_conf}" 'MAILTO' 'root'
            fi
            fn_AideDirectiveSetting "${l_aide_cron_conf}" 'QUIETREPORTS' 'yes'
            fn_AideDirectiveSetting "${l_aide_cron_conf}" 'COPYNEWDB' 'no'
            fn_AideDirectiveSetting "${l_aide_cron_conf}" 'TRUNCATEDETAILS' 'no'
            fn_AideDirectiveSetting "${l_aide_cron_conf}" 'FILTERUPDATES' 'yes'
            fn_AideDirectiveSetting "${l_aide_cron_conf}" 'FILTERINSTALLATIONS' 'yes'
            fn_AideDirectiveSetting "${l_aide_cron_conf}" 'LINES' '2000'
            fn_AideDirectiveSetting "${l_aide_cron_conf}" 'UPAC_CONFDIR'
            fn_AideDirectiveSetting "${l_aide_cron_conf}" 'UPAC_CONFD'
            fn_AideDirectiveSetting "${l_aide_cron_conf}" 'UPAC_SETTINGSD'
            fn_AideDirectiveSetting "${l_aide_cron_conf}" 'CRONEXITHOOK' "\"fatal\""

            fnBase_CommandExistIfCheck 'update-aide.conf' && update-aide.conf &> /dev/null
        fi

        fnBase_OperationProcedureResult "${aide_name}"
    fi

    # AppArmor status
    if fnBase_CommandExistIfCheck 'aa-status'; then
        # /sys/kernel/security/apparmor/profiles
        aa-status &> /dev/null
    fi

    # tiger - Report system security vulnerabilities
    # tripwire - a file integrity checker for UNIX systems
    # As tripwire need to install postfix and config site_key & local_key passphrase, it will prompt interactive window which will disrupt script execution process, so if you wanna install tripwire, please manually execute the following commands
    if fnBase_CommandExistIfCheck 'tripwireNO'; then
        # Tripwire is a tool that aids system administrators and users in monitoring a designated set of files for any changes.
        # https://www.server-world.info/en/note?os=Ubuntu_16.04&p=tripwire
        # https://www.howtoforge.com/tutorial/how-to-monitor-and-detect-modified-files-using-tripwire-on-ubuntu-1604/

        # yum
        yum install -y -q epel-release
        yum install -y -q tripwire
        tripwire-setup-keyfiles
        # apt
        apt-get -yq install tripwire

        # - configuation file
        # /etc/tripwire/twcfg.txt
        # /etc/tripwire/twpol.txt

        # - check directory not exist, run 'tripwire --init' will prompt error
        # tripwire --check 2>&1 | grep 'Filename' > /tmp/no-directory.txt

        # - config file setting
        local l_twpol
        l_twpol='/etc/tripwire/twpol.txt'
        if [[ -s "${l_twpol}" ]]; then
            # Boot Scripts
            sed -r -i '/rc.boot[[:space:]]+/{s@^([[:space:]]+)#*[[:space:]]*@\1#@g;}' "${l_twpol}"
            # System boot changes
            sed -r -i '/\/var\/lock[[:space:]]+/{s@^([[:space:]]+)#*[[:space:]]*@\1#@g;}' "${l_twpol}"
            sed -r -i '/\/var\/run[[:space:]]+/{s@^([[:space:]]+)#*[[:space:]]*@\1#@g;}' "${l_twpol}"
            # Root config files
            sed -r -i '/\/root[[:space:]]+/,/}/{s@^([[:space:]]+)#*[[:space:]]*@\1#@g;}' "${l_twpol}"
            sed -r -i '/\/root[[:space:]]+/{s@^([[:space:]]+)#*[[:space:]]*@\1@g;}' "${l_twpol}"
            sed -r -i '/\/root\/.bashrc[[:space:]]+/{s@^([[:space:]]+)#*[[:space:]]*@\1@g;}' "${l_twpol}"
            sed -r -i '/\/root\/.bash_history[[:space:]]+/{s@^([[:space:]]+)#*[[:space:]]*@\1@g;}' "${l_twpol}"
            # Devices & Kernel information
            sed -r -i '/\/dev\/[^[:space:]]+[[:space:]]+/d' "${l_twpol}"
            sed -r -i '/\/dev[[:space:]]+/a /dev/pts        -> $(Device);\n/dev/shm        -> $(Device);\n/dev/hugepages  -> $(Device);\n/dev/mqueue     -> $(Device);' "${l_twpol}"
            sed -r -i '/\/proc[[:space:]]+/{s@^([[:space:]]+)#*[[:space:]]*@\1#@g;}' "${l_twpol}"
            sed -r -i '/\/proc\/[^[:space:]]+[[:space:]]+/d' "${l_twpol}"
            sed -r -i '/\/proc[[:space:]]+/a /proc/devices           -> $(Device) ;\n/proc/net               -> $(Device) ;\n/proc/tty               -> $(Device) ;\n/proc/cpuinfo           -> $(Device) ;\n/proc/modules           -> $(Device) ;\n/proc/mounts            -> $(Device) ;\n/proc/dma               -> $(Device) ;\n/proc/filesystems       -> $(Device) ;\n/proc/interrupts        -> $(Device) ;\n/proc/ioports           -> $(Device) ;\n/proc/kcore             -> $(Device) ;\n/proc/self              -> $(Device) ;\n/proc/kmsg              -> $(Device) ;\n/proc/stat              -> $(Device) ;\n/proc/loadavg           -> $(Device) ;\n/proc/uptime            -> $(Device) ;\n/proc/locks             -> $(Device) ;\n/proc/meminfo           -> $(Device) ;\n/proc/misc              -> $(Device) ;' "${l_twpol}"
            [[ -d /proc/scsi ]] && sed -r -i '/\/proc[[:space:]]+/a /proc/scsi              -> $(Device) ;' "${l_twpol}"
            sed -r -i '/\/dev[[:space:]]+/,/}/{/[^}]/{s@^[[:space:]]*@    @g;}}' "${l_twpol}"
            # Critical system boot files
            sed -r -i '/\/boot\/[^[:space:]]+[[:space:]]+/d' "${l_twpol}"
            [[ -d /boot/efi ]] && sed -r -i '/\/boot[[:space:]]+/a /boot/efi       -> $(SEC_CRIT) ;' "${l_twpol}"
            sed -r -i '/\/boot[[:space:]]+/,/}/{/[^}]/{s@^[[:space:]]*@    @g;}}' "${l_twpol}"
        fi

        # - recreating the encrypted policy file: /etc/tripwire/tw.pol
        twadmin -m P "${l_twpol}"
        # - initialize database
        tripwire --init

        # - check command
        # sudo tripwire --check
        # sudo tripwire --check --interactive

        # - print report
        # twprint --print-report --twrfile /var/lib/tripwire/report/centos7-20180116-151815.twr > /tmp/report.txt

        # - cron task
        # 30 3 * * * /usr/sbin/tripwire --check | mail -s "Tripwire report for `uname -n`" your_email@domain.com
    fi
}


fn_UnwantedServiceSetting(){
    local l_service_name="${1:-}"
    local l_description="${2:-}"
    local l_result="${3:-}"

    local l_continue=${l_continue:-0}
    if [[ -n "${l_service_name}" ]]; then
        if [[ -s "/lib/systemd/system/${l_service_name}.service" ]]; then
            [[ $(systemctl is-enabled "${l_service_name}" 2>&1) == 'enabled' ]] && l_continue=1
        elif [[ -s "/etc/init.d/${l_service_name}" ]]; then
            local l_sysv_command
            if fnBase_CommandExistIfCheck 'chkconfig'; then
                l_sysv_command='chkconfig'  # for RedHat/OpenSUSE
            elif fnBase_CommandExistIfCheck 'sysv-rc-conf'; then
                l_sysv_command='sysv-rc-conf'   # for Debian
            fi

            [[ -n "${l_sysv_command}" && -n $(${l_sysv_command} --list "${l_service_name}" | sed -r -n '/3:on/{p}') ]] && l_continue=1

        elif fnBase_CommandExistIfCheck "${l_service_name}"; then
            l_continue=1
        fi
    fi

    if [[ "${l_continue}" -eq 1 ]]; then
        fnBase_OperationProcedureStatement "Disable ${l_description} service"
        fnBase_SystemServiceManagement "${l_service_name}" 'disable'
        fnBase_OperationProcedureResult "${l_result}"
    fi
}

fn_UnwantedUtilityServiceOperation(){
    fnBase_OperationPhaseStatement 'Unwanted Utilities & Services'

    # - Remove unwanted packages
    fnBase_OperationProcedureStatement "Remove unwanted utilities"
    # dpkg -l 2>&1 | awk 'match($1,/^rc$/){print $2}' | xargs dpkg --purge 2> /dev/null

    fnBase_CommandExistIfCheck 'gnome-shell' && fnBase_PackageManagerOperation 'remove' 'evolution totem rhythmbox empathy brasero bijiben gnome-maps gnome-music gnome-clocks gnome-contacts gnome-weather'

    if [[ $(find /usr/share/applications -type f -name '*.desktop' -print 2>/dev/null | wc -l) -gt 0 ]]; then
        # - Game
        local game_pack_list=${game_pack_list:-}
        game_pack_list=$(grep Game /usr/share/applications/*.desktop | awk -F: '{!a[$1]++}END{for(i in a) print i}' | while read -r line; do sed -r -n '/Exec/{s@.*=([^[:space:]]+).*@\1@g;p}' "${line}" | awk '{!a[$0]++}END{print}'; done | sed ':a;N;s@\n@ @g;t a;')
        [[ -z "${game_pack_list}" ]] || fnBase_PackageManagerOperation 'remove' "${game_pack_list}"

        # grep Game /usr/share/applications/*.desktop | awk -F: '{!a[$1]++}END{for(i in a) print i}' | while read -r line;do sed -r -n '/Exec/{s@.*=([^[:space:]]+).*@\1@g;p}' "${line}" | awk '{if($0=="sol"){$0="aisleriot"};!a[$0]++}END{print}'; done | xargs -- sudo zypper rm -yu
    fi
    fnBase_OperationProcedureResult


    # - Unwanted service

    # chkconfig --list | grep '3:on'
    # systemctl list-unit-files --state=enabled
    # systemctl list-units --type service
    # systemd-cgtop
    # systemctl list-dependencies graphical.target

    fn_UnwantedServiceSetting 'bluetooth' 'Bluetooth'

    # minissdpd - daemon keeping track of UPnP devices up
    # minissdpd listen for SSDP traffic and keeps track of what are the UPnP devices  up on the network. The list of the UPnP devices is accessed by programs looking for devices, skipping the UPnP discovery process.
    fn_UnwantedServiceSetting 'minissdpd' 'Universal Plug and Play (UPnP)'

    # avahi-daemon - The Avahi mDNS/DNS-SD daemon
    # The Avahi daemon implements the DNS Service Discovery and Multicast DNS protocols, which provide service and host discovery on a network. It allows a system to automatically identify resources on the network, such as printers or web servers. If you removed Avahi daemon and your network connections crashed and you need to manually configure Network Interface Card again.
    # fnBase_PackageManagerOperation 'remove' 'avahi'
    fn_UnwantedServiceSetting 'avahi-daemon' 'Avahi daemon'

    # anacron - runs commands periodically
    # Anacron can be used to execute commands periodically, with a frequency specified in days. Unlike cron(8), it does not assume that the machine is running continuously. Hence, it can be used on machines that aren't running 24 hours a day, to control daily, weekly, and monthly jobs that are usually controlled by cron.
    # /etc/anacrontab
    fn_UnwantedServiceSetting 'anacron' 'Anacron'

    # Service control for the automounter
    # autofs controls the operation of the automount(8) daemon(s) running on the Linux system. Usually autofs is invoked at system boot time with the start parameter and at shutdown time with the  stop parameter. Service control actions can also be manually invoked by the system administrator to shut down, restart, reload or obtain service status.
    fn_UnwantedServiceSetting 'autofs' 'autofs'

    # pcscd - PC/SC Smart Card Daemon
    # pcscd is the daemon program for pcsc-lite. It is a resource manager that coordinates communications with smart card readers and smart cards and cryptographic tokens that are connected to the system.
    fn_UnwantedServiceSetting 'pcscd' 'PC/SC Smart Card Daemon'

    # if [[ "${distro_name}" == 'ubuntu' ]]; then
    #     # https://askubuntu.com/questions/907246/how-to-disable-systemd-resolved-in-ubuntu#907249
    #     local l_network_manager_conf=${l_network_manager_conf:-'/etc/NetworkManager/NetworkManager.conf'}
    #     if [[ -f "${l_network_manager_conf}" ]]; then
    #         sed -r -i '/\[main\]/,/\[/{/dns=/d}; /\[main\]/a dns=default' "${l_network_manager_conf}"
    #         # /etc/resolv.conf -> ../run/resolvconf/resolv.conf
    #         [[ -f /etc/resolv.conf ]] && rm -f /etc/resolv.conf
    #         # systemd-resolved
    #         fn_UnwantedServiceSetting 'systemd-resolved' 'systemd-resolved' 'port 53'
    #         # systemctl restart NetworkManager
    #     fi
    # fi

    # CUPS printing system
    # fnBase_PackageManagerOperation 'remove' 'cupsd'
    fn_UnwantedServiceSetting 'cups' 'CUPS' 'port 631'

    # Universal Addresses to RPC Program Number Mapper
    # Securing rpcbind only affects NFSv2 and NFSv3 implementations, since NFSv4 no longer requires it. If you plan to implement an NFSv2 or NFSv3 server, then rpcbind is required.
    # fnBase_PackageManagerOperation 'remove' 'rpcbind'
    fn_UnwantedServiceSetting 'rpcbind' 'RPC' 'port 111'

    # Postfix is a Mail Transport Agent (MTA), supporting LDAP, SMTP AUTH (SASL), TLS
    # fnBase_PackageManagerOperation 'remove' 'postfix'
    # fn_UnwantedServiceSetting 'postfix' 'Postfix' 'port 25'

    # exim4 - a Mail Transfer Agent
    # fnBase_PackageManagerOperation 'remove' 'exim4'
    # fn_UnwantedServiceSetting 'exim4' 'Exim (v4)' 'port 25'
}


fn_SecuritySummaryOperation(){
    [[ "${unwanted_utility_service_keep}" -ne 1 ]] && fn_UnwantedUtilityServiceOperation
    fn_SystemHardeningOperation
    # https://www.safecomputing.umich.edu/protect-the-u/protect-your-unit/hardening/secure-linux-unix-server
    if [[ "${security_enhance}" -eq 1 ]]; then
        fn_AuditOperation
        fn_SecurityOperation
    fi
}


#########  2-13. Cron Task  #########
fn_CronTaskConfiguration(){
    fnBase_OperationPhaseStatement "Cron Scheduled Task"

    # set cron task at hour 4 in the morning
    local l_crontab=${l_crontab:-'/etc/crontab'}
    if [[ -s "${l_crontab}" ]]; then
        local l_hour_choose=${l_hour_choose:-4}
        sed -r -i '/bin\/anacron/{s@^([[:digit:]]{1,2}[[:space:]]*)[[:digit:]]{1,2}(.*)$@\1'"${l_hour_choose}"' \2@g;}' "${l_crontab}"
    fi

    # - system update weekly
    # local system_update_path="/etc/cron.weekly/${pack_manager%%-*}_update"
    # if [[ ! -s "${system_update_path}" ]]; then
    #     fnBase_OperationProcedureStatement 'system update'
    #     echo -e "#!/usr/bin/env bash\n#system update weekly\n" > "${system_update_path}"
    #     case "${pack_manager}" in
    #         apt-get ) echo -e "/usr/bin/apt-get -yq clean all &> /dev/null\n/usr/bin/apt-get -yq update &> /dev/null\n/usr/bin/apt-get -yq upgrade &> /dev/null\n/usr/bin/apt-get -yq dist-upgrade &> /dev/null\n/usr/bin/apt-get -yq autoremove &> /dev/null\n" >> "${system_update_path}" ;;
    #         dnf ) echo -e "/usr/bin/dnf -yq clean all &> /dev/null\n/usr/bin/dnf -yq makecache &> /dev/null\n/usr/bin/dnf -yq upgrade &> /dev/null\n/usr/bin/dnf -yq autoremove &> /dev/null\n" >> "${system_update_path}" ;;
    #         yum ) echo -e "/usr/bin/yum -y -q clean all &> /dev/null\n/usr/bin/yum -y -q makecache fast &> /dev/null\n/usr/bin/yum -y -q update &> /dev/null\n/usr/bin/yum -y -q upgrade &> /dev/null\n/usr/bin/yum -y -q autoremove &> /dev/null\n"  >> "${system_update_path}" ;;
    #         zypper ) echo -e "/usr/bin/zypper clean -a &> /dev/null\n/usr/bin/zypper ref -f &> /dev/null\n/usr/bin/zypper up -yl &> /dev/null\n/usr/bin/zypper dup -yl &> /dev/null\n/usr/bin/zypper patch -yl &> /dev/null\n/usr/bin/zypper packages --unneeded | awk -F\| 'match(\$1,/^i/){print \$3}' | /usr/bin/xargs /usr/bin/zypper rm -yu &> /dev/null\n" >> "${system_update_path}" ;;
    #     esac
    #     echo -e "#Script end" >> "${system_update_path}"
    #     chmod 750 "${system_update_path}"
    #     fnBase_OperationProcedureResult "${system_update_path}"
    # fi

    # - clamav daily
    if fnBase_CommandExistIfCheck 'clamscan'; then
        local clamav_scan_path=${clamav_scan_path:-'/etc/cron.daily/clamav_scan'}
        if [[ ! -s "${clamav_scan_path}" ]]; then
            fnBase_OperationProcedureStatement 'clam antiVirus scan'
            echo -e "#!/usr/bin/env bash\n#clamav scan daily\n" > "${clamav_scan_path}"
            echo -e "name='clamscan'\nlog_path=\"/var/log/clamav/dailyscan-\$(date +'%F').log\"\nexecuting_path=\$(which \$name 2> /dev/null || command -v \$name 2> /dev/null)\n[[ -z \"\${executing_path}\" ]] && executing_path='/usr/bin/clamscan'\nfreshclam &> /dev/null\n\${executing_path} -r --bell -i --quiet --max-filesize=1024M --enable-stats --exclude-dir=/sys/* / > \"\${log_path}\"\nmail -s \"Daily Clam AntiVirus report for \$(hostname -f)\" root < \"\${log_path}\"\n" >> "${clamav_scan_path}"
            echo -e "#Script end" >> "${clamav_scan_path}"
            chmod 750 "${clamav_scan_path}"
            fnBase_OperationProcedureResult "${clamav_scan_path}"
        fi
    fi

    # - aide daily
    if fnBase_CommandExistIfCheck 'aide'; then
        # for rhel/centos
        local aide_scan_path=${aide_scan_path:-'/etc/cron.daily/aide'}
        if [[ ! -s "${aide_scan_path}" ]]; then
            fnBase_OperationProcedureStatement 'aide check'
            echo -e "#!/usr/bin/env bash\n#Aide check daily\n" > "${aide_scan_path}"
            echo -e "name='aide'\nlog_path=\"/tmp/aide-check-\$(date +'%F').log\"\nexecuting_path=\$(which \$name 2> /dev/null || command -v \$name 2> /dev/null)\n[[ -z \"\${executing_path}\" ]] && executing_path='/sbin/aide'\n# aide --update &> /dev/null\n\${executing_path} --check &> \"\${log_path}\"\nmail -s \"Daily AIDE report for \$(hostname -f)\" root < \"\${log_path}\"\n\n[[ -f \"\${log_path}\" ]] && rm -f \"\${log_path}\"\n" >> "${aide_scan_path}"
            echo -e "#Script end" >> "${aide_scan_path}"
            chmod 750 "${aide_scan_path}"
            fnBase_OperationProcedureResult "${aide_scan_path}"
        fi
    fi
}


#########  3. Operation Time Cost  #########
fn_OperationTimeCost(){
    finish_time=$(date +'%s')        # processing end time
    total_time_cost=$((finish_time-start_time))   # time costing

    printf "\nTotal time cost is ${c_red}%s${c_normal} seconds!\n" "${total_time_cost}"
    printf "\nTo make configuration effect, please ${c_red}%s${c_normal} your system!\n" "reboot"

    [[ -s "${login_user_home}/.bash_history" ]] && echo '' > "${login_user_home}/.bash_history"

    # remove_old_kernel=${remove_old_kernel:-}
    #
    # case "${pack_manager}" in
    #     yum )
    #         if fnBase_CommandExistIfCheck 'package-cleanup'; then
    #             # keep an older kernel
    #             remove_old_kernel='package-cleanup --oldkernels --count=1'
    #         else
    #             remove_old_kernel="${pack_manager} remove \$(rpm -qa | awk -v verinfo=\$(uname -r) 'BEGIN{gsub(\".?el[0-9].*$\",\"\",verinfo)}match(\$0,/^kernel/){if(\$0!~verinfo) print \$0}' | sed '1d')"
    #         fi
    #         ;;
    #     dnf )
    #         remove_old_kernel="${pack_manager} remove \$(rpm -qa | awk -v verinfo=\$(uname -r) 'BEGIN{gsub(\".?el[0-9].*$\",\"\",verinfo)}match(\$0,/^kernel/){if(\$0!~verinfo) print \$0}' | sed '1d')"
    #         ;;
    #     apt-get )
    #         remove_old_kernel="${pack_manager} purge \$(dpkg -l | awk -v verinfo=\$(uname -r) 'match(\$0,/linux-image-/){if(\$0!~/-hwe/&&\$2!~verinfo) print \$2}' | sed '1d')"
    #         ;;
    #     zypper )
    #         [[ $(rpm -qa | grep -c ^kernel-default) -gt 1 ]] && remove_old_kernel="${pack_manager} remove \$(zypper packages --installed-only | awk -F\| -v verinfo=\$(uname -r) 'BEGIN{OFS=\"-\"}match(\$1,/^i/)&&match(\$0,/kernel-default/){gsub(\"-default\",\"\",verinfo);gsub(\" \",\"\",\$0);if(\$4!~verinfo){print\$3,\$4}}')"
    #         ;;
    # esac
    #
    # [[ -z "${remove_old_kernel}" ]] || printf "\nTo remove old version kernel, executing the following commands: \n\n${c_yellow}%s${c_normal}\n\n" "sudo ${remove_old_kernel}"

    if fnBase_CommandExistIfCheck 'restorecon'; then
        if [[ "${enforcing_selinux}" -eq 1 ]]; then
            case "${pack_manager}" in
                apt-get )
                    echo -e "\nAfter reboot, executing the following commands to configure SELinux:\n\n${c_yellow}sudo selinux-config-enforcing enforcing${c_normal}\n\n"
                    ;;
                zypper )
                    echo -e "\nAfter reboot, executing the following commands to configure SELinux:\n\n${c_yellow}sudo restorecon -Rp /\n\nsudo sed -r -i '/^SELINUX=/{s@^[[:space:]]*([^=]+=).*@\1enforcing@g}' /etc/selinux/config\n\nsudo yast2 bootloader${c_normal}\n\n"
                    ;;
            esac
        fi
    fi
}


#########  4. Executing Process  #########
fn_ExecutingProcess(){
    fn_InitializationCheck
    fn_OSInfoDetection

    fnBase_CentralOutputTitle 'Operation Processing, Just Be Patient'
    fn_VariablesPreprocessing
    fn_RepositoryAndPackagesOperation
    fn_SELinuxConfiguration
    fn_GRUBConfiguring
    fn_HostnameTimezoneSetting
    fn_SystemUserConfiguration
    fn_EssentialPackInstallation
    [[ "${administrator_utility}" -eq 1 ]] && fn_AdministrationPackInstallation
    fn_ContainerInstallation
    fn_OpenSSHPortSpecifiedVerification
    fn_OpenBSDSecureShellOperation
    fn_FirewallConfiguration
    [[ "${boost_enable}" -eq 1 ]] && fn_AxdSopInstallation
    fn_TmpfsSystemSetting
    fn_KernelOptimization
    fn_SecuritySummaryOperation
    [[ "${cron_task}" -eq 1 ]] && fn_CronTaskConfiguration

    fn_OperationTimeCost
}

fn_ExecutingProcess


#########  5. EXIT Singal Processing  #########
# trap "commands" EXIT # execute command when exit from shell
fn_TrapEXIT(){
    fnBase_UnsetVars
    unset pass_change_minday
    unset pass_change_maxday
    unset pass_change_warnningday
    unset enforcing_selinux
    unset install_ssh_server
    unset disable_ssh_root
    unset ssh_port
    unset repository_change
    unset ssh_keygen_only
    unset restrict_remote_login
    unset grub_timeout
    unset hostname_specify
    unset username_specify
    unset timezone_specify
    unset tmpfs_enable
    unset sudo_mode_enable
    unset log_user_session
    unset administrator_utility
    unset security_enhance
    unset unwanted_utility_service_keep
    unset cron_task
    unset kernel_upgrade
    unset proxy_server_specify

    unset distro_fullname
    unset distro_name
    unset codename
    unset version_id
    unset ip_local
    unset ip_public
    unset login_user_home
    unset mem_totoal_size
    unset tmpfs_enable
    unset remove_old_kernel
}

trap fn_TrapEXIT EXIT


# # sudo selinux-ready
# Start checking your system if it is selinux-ready or not:
# 	check_dir: OK. /selinux exists.
# 	check_dir: OK. /sys/fs/selinux exists.
# 	check_filesystem: OK. Filesystem 'securityfs' exists.
# 	check_filesystem: OK. Filesystem 'selinuxfs' exists.
# 	check_boot: Assuming GRUB2 as bootloader.
# 	check_boot: OK. Current kernel 'vmlinuz-4.4.103-6.38-default' has boot-parameters 'security=selinux selinux=1'
# 	check_boot: OK. Other kernels with correct parameters:
# 	check_mkinitrd: OK. Your initrd seems to be correct.
# 	check_packages: OK. All essential packages are installed
# 	check_config: OK. Config file seems to be there.
# 	check_config: OK. SELINUX is set to 'permissive'.
# 	check_pam: OK. Your PAM configuration seems to be correct.
# 	check_runlevel: OK. restorecond is enabled on your system


# https://www.2daygeek.com/how-to-count-the-number-of-files-and-folders-directories-in-linux/
# find / -type d -exec echo dirs \; -o -type l -exec echo symlinks \; -o -type f -links +1 -exec echo hardlinks \; -o -type f -exec echo files \; 2> /dev/null | sort | uniq -c

# Check available update & security update
# yum --debuglevel 2 --security check-update 2> /dev/null
#   15 package(s) needed for security, out of 29 available
#   No packages needed for security; 25 packages available
# apt-get -s upgrade
# apt-get -u upgrade --assume-no
# apt-get -V -u upgrade --assume-no
#   48 upgraded, 0 newly installed, 0 to remove and 3 not upgraded.
#   0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.
# zypper list-updates -a


# swap
# http://www.tldp.org/LDP/Linux-Filesystem-Hierarchy/html/proc.html
# /proc/swaps
# strings --bytes=6 $(awk 'match($1,/^\//){print $1;exit}' /proc/swaps)


# Script End
