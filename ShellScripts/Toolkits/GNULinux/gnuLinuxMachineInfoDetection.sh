#!/usr/bin/env bash
# shellcheck disable=SC1090,SC2012,SC2230,SC2154,SC2155
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails

# Target: Detecting GNU/Linux Distribution Hardware & System Info
# Note: Support for RHEL, Debian, SLES, Arch Linux and variants Distribution
# Similar project: https://github.com/dylanaraps/neofetch
# Developer: MaxdSre

# Change Log:
# - Oct 08, 2020 09:11 Thu ET - initialization check method update
# - Sep 19, 2020 09:12 Sat ET - add Pop!_OS support (family name Debian)
# - May 22, 2020 10:10 Fri ET - fix battery info detection if command dmidecode not exist
# - Nov 05, 2019 08:42 Tue ET - comment ipdata.co which needs API key, fix ip-api.com output
# - Oct 30, 2019 21:05 Wed ET - enhance desktop environment detection, change Raspberry Pi detect method, add KDE Plasma support
# - Oct 29, 2019 23:14 Tue ET - add Manjaro support (family name Arch Linux)
# - Oct 16, 2019 09:10 Wed ET - add gawk command exist check (optional)
# - Oct 15, 2019 11:59 Tue ET - change custom script url
# - Oct 14, 2019 16:51 Mon ET - add package manager info for Arch Linux (pacman)
# - Oct 02, 2019 16:32 Wed ET - IP info json format extraction fix
# - Sep 10, 2019 11:35 Tue ET - add Raspberry Pi type detection for variable distro_raspberrypi_type
# - Sep 10, 2019 10:53 Tue ET - add arm architecture detection for variable distro_os_arch
# - Aug 14, 2019 13:46 Wed ET - change to new base functions, testing approved
# - Jun 27, 2019 21:15 Thu ET - screen resolution detection optimization, add desktop environment detection
# - Jun 21, 2019 14:25 Fri ET - add Arch Linux support
# - Apr 18, 2019 11:19 Thu ET - Ubuntu minor release for 'version_id' optimization
# - Jan 01, 2018 21:06 Tue ET - code reconfiguration, include base funcions from other file
# - Aut 07, 2018 03:37 Tue +0800 - add types for os arch
# - Jul 15, 2018 10:46 Sun ET - add system install date detection
# - May 30, 2018 11:46 Wed ET - add package manager, firewall type, python type detection
# - May 10, 2018 17:29 Thu ET - add TCP socket info detection from file `/proc/net/tcp`, same to `ss -tn4 state all`
# - May 03, 2018 14:57 Thu ET - add package management relevant info detection
# - Apr 26, 2018 11:56 Thu ET - add installed packages count, default shell detection
# - Apr 02, 2018 11:47 Mon ET - optimizing ip info query speed, add new ip info api ipdata.co, ip-api.com
# - Feb 27, 2018 17:28 Tue ET - Optimization, add biso, baseboard, onboard_device, chassis, memory info detection
# - Feb 26, 2018 20:19 Mon ET - Refactoring, add product info detection, optimize ip addr detection method
# - Feb 06, 2018 09:49 Tue ET - Add 'timezoneapi.io' to detect timezone info (more quickly)
# - Jan 23, 2018 15:58 Tue +0800 - Add timezone detection
# - Dec 30, 2017 19:41 Sat +0800 - Change output style
# - Aug 30, 2017 11:58 Wed +0800
# - Aug 17, 2017 14:37 Thu +0800
# - June 6, 2017 21:02 Tue +0800
# - May 5, 2017 20:08 Fri ET
# - Mar 11, 2017 10:48~12.27 +0800
# - Feb 23, 2017 14:50~17:01 +0800
# - Oct 19, 2016 10:45 Wed +0800


# Docker Script https://get.docker.com/
# Gitlab Script https://packages.gitlab.com/gitlab/gitlab-ce/install

# lsb_release -a
# lsb-release/redhat-lsb


#########  0-1. Singal Setting  #########
trap fnBase_TrapCleanUp INT QUIT


#########  0-2. Variables Setting  #########
# umask 022    # temporarily change umask value to 022
readonly custom_shellscript_url='https://gitlab.com/axdsop/nixnotes/raw/master/Script/Toolkits'
readonly distro_release_date_list="${custom_shellscript_url}/_Configs/Source/gnulinuxDistroReleaseDateList.txt"
readonly available_type_list='system|bios|baseboard|chassis|processor|memory|disk|battery|socket'

list_all_type=${list_all_type:-0}
type_specify=${type_specify:-}
ip_detection_disable=${ip_detection_disable:-0}
pack_manager_disable=${pack_manager_disable:-1}
json_output=${json_output:-0}
encryption_password=${encryption_password:-}
proxy_server_specify=${proxy_server_specify:-''}
output_json=${output_json:-}



#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | [sudo] bash -s -- [options] ...

Detecting GNU/Linux Hardware & System Info!

Please specify sudo, or it will be fail to execute utility dmidecode.

[available option]
    -h    --help, show help info
    -a    --list all available type detected info
    -t type    --specify type (currenyly available: ${available_type_list}), default is 'system'
    -i    --disable ip info detection, default is enabled
    -m    --enable package management info detection, default is disable
    -j    --json, output result via json format, default is plain text
    -E passwd    --specify password for encryption via openssl, compression via gip, encoded with base64, along with '-j'
    -p [protocol:]ip:port    --proxy host (http|https|socks4|socks5), default protocol is http
\e[0m"
}

# http://wiki.bash-hackers.org/howto/getopts_tutorial
# https://www.mkssoftware.com/docs/man1/getopts.1.asp

while getopts "hat:imjE:p:" option "$@"; do
    case "$option" in
        a ) list_all_type=1 ;;
        t ) type_specify="$OPTARG" ;;
        i ) ip_detection_disable=1 ;;
        m ) pack_manager_disable=0 ;;
        j ) json_output=1 ;;
        E ) encryption_password="$OPTARG" ;;
        p ) proxy_server_specify="$OPTARG" ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done



#########  1-2 Preparation Operation  #########
fn_InitializationCheck(){
    local l_user_codebase_dir="$HOME/.config/axdsop"
    local l_base_function_temp_save_path=''
    local l_need_remove=0
    local l_base_function_url='https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/base_functions.sh'
    l_base_function_temp_save_path=$(find "${l_user_codebase_dir}" -type f -name "${l_base_function_url##*/}" -print 2> /dev/null)
    
    if [[ ! -f "${l_base_function_temp_save_path}" ]]; then
        local l_download_tool=${l_download_tool:-'wget -qO-'}
        [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_tool='curl -fsL'
        l_need_remove=1
        l_base_function_temp_save_path=$(mktemp -t XXXXXXX)
        ${l_download_tool} "${l_base_function_url}" > "${l_base_function_temp_save_path}"
    fi

    if [[ -f "${l_base_function_temp_save_path}" && -s "${l_base_function_temp_save_path}" ]]; then
        . "${l_base_function_temp_save_path}"
        [[ "${l_need_remove}" -eq 1 ]] && rm "${l_base_function_temp_save_path}"
    else
        echo "Sorry, fail to get base script path."
        exit
    fi

    # tput clear  # Echo the clear-screen sequence for the current terminal.
    # doesn't need to check internet connection status forcelly
    fnBase_RunningEnvironmentCheck '0' '0'
    fnBase_InternetConnectionCheck '0'

    # - specified for RHEL/Debian/SLES/Amazon Linux/Arch Linux
    if [[ ! (-s '/etc/os-release' || -s '/etc/redhat-release' || -s '/etc/debian_version' || -s '/etc/SuSE-release' || -s '/etc/arch-release') ]]; then
        if [[ "${json_output}" -eq 1 ]]; then
            output_json='{"status":{"flag":"fail","message":"fail to recognize your system"}}'
            echo "${output_json}"
            exit
        else
            fnBase_ExitStatement "${c_red}Sorry${c_normal}: this script fail to recognize your system!"
        fi
    fi

    # fnBase_CommandExistCheckPhase 'gawk'
    fnBase_CommandExistCheckPhase 'sed'
}

fn_DmidecodeValExtraction(){
    local l_type_name=${1:-}  # dmidecode -t system
    local l_type_item=${2:-}  # Product Name
    local l_dmi_id_keyword=${3:-}    # /sys/devices/virtual/dmi/id/product_name
    local l_str_keyword=${4:-}  # dmidecode -s system-product-name
    local l_return_val=${l_return_val:-}

    local l_dmidecode_exist=${l_dmidecode_exist:-0}
    fnBase_CommandExistIfCheck 'dmidecode' && l_dmidecode_exist=1

    if [[ -n "${l_type_name}" && -n "${l_type_item}" ]]; then
        if [[ -n "${l_dmi_id_keyword}" ]]; then
            local l_dmi_id_dir=${l_dmi_id_dir:-'/sys/devices/virtual/dmi/id'}
            [[ -s "${l_dmi_id_dir}/${l_dmi_id_keyword}" ]] && l_return_val=$(cat "${l_dmi_id_dir}/${l_dmi_id_keyword}" 2> /dev/null)
        elif [[ -n "${l_str_keyword}" ]]; then
            [[ "${l_dmidecode_exist}" -eq 1 ]] && l_return_val=$(dmidecode -s "${l_str_keyword}" 2> /dev/null)
        fi

        if [[ -z "${l_return_val}" ]]; then
            [[ "${l_dmidecode_exist}" -eq 1 ]] && l_return_val=$(dmidecode -q -t "${l_type_name}" 2> /dev/null | sed -r -n '/^[[:space:]]*'"${l_type_item}"'[[:space:]]*:/{s@^[^:]*:[[:space:]]+(.*)$@\1@g;s@^[^=]+=([^[:punct:]]+).*@\1@g;p}')
        fi

    fi

    # trim leading and trailing whitespaces
    # echo $(echo "${l_return_val}")
    l_return_val=$(echo "${l_return_val}" | xargs 2> /dev/null)
    [[ "${l_return_val}" == 'Not Specified' ]] && l_return_val=''
    echo "${l_return_val}"
}

fn_MachineInfoValExtraction(){
    local l_item_hostnamectl=${1:-}
    local l_item_machineinfo=${2:-}
    local l_item_machine_path=${3:-'/etc/machine-info'}
    local l_return_val=${l_return_val:-}

    # hostnamectl directive
    fnBase_CommandExistIfCheck 'hostnamectl' && l_return_val=$(hostnamectl 2> /dev/null | sed -r -n '/'"${l_item_hostnamectl}"':/{s@^[^:]*:[[:space:]]*(.*)$@\1@g;p}' 2> /dev/null)
    # /etc/machine-info directive
    [[ -z "${l_return_val}" && -n "${l_item_hostnamectl}" && -n "${l_item_machineinfo}" && -s "${l_item_machine_path}" ]] && l_return_val=$(sed -r -n '/'"${l_item_machineinfo}"'[[:space:]]*=/{s@"@@g;s@^[^=]*=[[:space:]]*(.*)$@\1@g;p}' "${l_item_machine_path}" 2> /dev/null)

    # trim leading and trailing whitespaces
    # echo $(echo "${l_return_val}")
    l_return_val=$(echo "${l_return_val}" | xargs 2> /dev/null)
    echo "${l_return_val}"
}

fn_ProcessorInfoValExtraction(){
    # 1 /proc/cpuinfo, 2 lscpu, 3 dmidecode
    local l_cpuinfo_name="${1:-}"
    local l_lscpu_name="${2:-}"
    local l_dmi_name="${3:-}"
    local l_prior_num=${4:-1}
    local l_output=${l_output:-}

    case "${l_prior_num}" in
        1 )
            local l_cpuinfo_path=${l_cpuinfo_path:-'/proc/cpuinfo'}
            if [[ -f "${l_cpuinfo_path}" && -n "${l_cpuinfo_name}" ]]; then
                l_output=$(sed -r -n '/^[[:space:]]*'"${l_cpuinfo_name}"'[[:space:]]*:/{s@^[^:]*:[[:space:]]*(.*)$@\1@g;p}' "${l_cpuinfo_path}" 2> /dev/null | uniq)
            fi
            [[ -z "${l_output}" ]] && fn_ProcessorInfoValExtraction "${l_cpuinfo_name}" "${l_lscpu_name}" "${l_dmi_name}" '2'
            ;;
        2 )
            if [[ -n "${l_lscpu_name}" ]]; then
                l_output=$(lscpu 2> /dev/null | sed -r -n 's@\(s\)@@g;/^[[:space:]]*'"${l_lscpu_name}"'[[:space:]]*:/{s@^[^:]*:[[:space:]]*(.*)$@\1@g;p}')
            fi
            [[ -z "${l_output}" ]] && fn_ProcessorInfoValExtraction "${l_cpuinfo_name}" "${l_lscpu_name}" "${l_dmi_name}" '3'
            ;;
        3 )
            if [[ -n "${l_dmi_name}" ]]; then
                fnBase_CommandExistIfCheck 'dmidecode' && l_output=$(dmidecode -q -t processor 2> /dev/null | sed -r -n '/^[[:space:]]*'"${l_dmi_name}"'[[:space:]]*:/{s@^[^:]*:[[:space:]]+(.*)$@\1@g;s@^[^=]+=([^[:punct:]]+).*@\1@g;p}' | uniq)
            fi
            ;;
    esac

    echo "${l_output}"
}

fn_SmartctlDirectiveExtraction(){
    local l_raw_data="${1:-}"
    local l_item="${2:-}"
    local l_output=${l_output:-}
    if [[ -n "${l_raw_data}" && -n "${l_item}" ]]; then
      l_output=$(echo "${l_raw_data}" | sed -r -n '/^'"${l_item}"':/{s@^[^:]+:[[:space:]]*(.*)$@\1@g;p}')
    fi
    echo "${l_output}"
}

fn_OutputJsonConcat(){
    local l_item=${1:-}
    local l_val="${2:-}"
    local l_var=${3:-'output_json'}
    local l_is_sub=${4:-0}
    local l_output=${l_output:-}
    if [[ "${l_item}" == '_s' ]]; then
        l_output="{"
    elif [[ "${l_item}" == '_e' ]]; then
        l_output="${!l_var}"
        l_output=${l_output%,*}
        if [[ "${l_output}" =~ }$ ]]; then
            l_output=${l_output}","
        else
            l_output=${l_output}"},"
        fi

    else
        if [[ -n "${l_item}" && -n "${l_val}" ]]; then
            if [[ "${l_is_sub}" -eq 0 ]]; then
                # escape character double quotation marks `"`
                l_val=$(echo "${l_val}" | sed -r 's@"@\\"@g' 2> /dev/null)
                l_output="${!l_var}\"${l_item}\":\"${l_val}\","
            else
                l_output="${!l_var}\"${l_item}\":[${l_val}],"
            fi

        else
            l_output="${!l_var}"
        fi
    fi
    echo "${l_output}"
}


#########  2 -  System Info (Manufacturer & OS)  #########

#########  2.1 - Product Info (Manufacturer)  #########
fn_ProductInfo(){
    # dmidecode -t system / dmidecode -t 1
    local product_manufacturer=${product_manufacturer:-}    # Manufacturer
    local product_family=${product_family:-}     # Family
    local product_name=${product_name:-}    # Product Name
    local product_version=${product_version:-}  # Version
    local product_serial_num=${product_serial_num:-}    # Serial Number
    local product_uuid=${product_uuid:-}    # UUID
    local product_wakeup_type=${product_wakeup_type:-}    # Wake-up Type
    local product_sku_num=${product_sku_num:-}    # SKU Number
    local product_resolution=${product_resolution:-}  # Screen Resolution

    # 1 -- System Information
    product_manufacturer=$(fn_DmidecodeValExtraction '1' 'Manufacturer' 'sys_vendor' 'system-manufacturer')
    product_family=$(fn_DmidecodeValExtraction '1' 'Family' 'product_family')
    product_name=$(fn_DmidecodeValExtraction '1' 'Product Name' 'product_name' 'system-product-name')
    product_version=$(fn_DmidecodeValExtraction '1' 'Version' 'product_version' 'system-version')
    # [[ "${product_version}" == 'Not Specified' ]] && product_version=''
    product_serial_num=$(fn_DmidecodeValExtraction '1' 'Serial Number' 'product_serial' 'system-serial-number')
    product_uuid=$(fn_DmidecodeValExtraction '1' 'UUID' 'product_uuid' 'system-uuid')
    product_wakeup_type=$(fn_DmidecodeValExtraction '1' 'Wake-up Type')
    product_sku_num=$(fn_DmidecodeValExtraction '1' 'SKU Number')

    # Screen resolution
    fnBase_CommandExistIfCheck 'xdpyinfo' && product_resolution=$(xdpyinfo 2> /dev/null | awk 'match($0,/dimensions:/){print $2; exit}')
    # xdpyinfo | sed -r -n '/^[[:space:]]*dimensions:/{s@^[^:]+:[[:space:]]*([^[:space:]]+).*@\1@g;p}'
    if [[ -z "${product_resolution}" ]]; then
        fnBase_CommandExistIfCheck 'xrandr' && product_resolution=$(xrandr 2> /dev/null | awk 'match($0,/*/){print $1}' 2> /dev/null)
    fi

    if [[ "${json_output}" -ne 1 ]]; then
        fnBase_CentralOutputTitle 'Product Information'
        fnBase_OutputControl 'Manufacturer' "${product_manufacturer}"
        fnBase_OutputControl 'Family' "${product_family}"
        fnBase_OutputControl 'Product Name' "${product_name}"
        fnBase_OutputControl 'Version' "${product_version}"
        fnBase_OutputControl 'Serial Number' "${product_serial_num}"
        fnBase_OutputControl 'UUID' "${product_uuid}"
        fnBase_OutputControl 'Wake-up Type' "${product_wakeup_type}"
        fnBase_OutputControl 'SKU Number' "${product_sku_num}"
        fnBase_OutputControl 'Resolution' "${product_resolution}"

    else
        output_json=$(fn_OutputJsonConcat 'product_manufacturer' "${product_manufacturer}")
        output_json=$(fn_OutputJsonConcat 'product_family' "${product_family}")
        output_json=$(fn_OutputJsonConcat 'product_name' "${product_name}")
        output_json=$(fn_OutputJsonConcat 'product_version' "${product_version}")
        output_json=$(fn_OutputJsonConcat 'product_serial_num' "${product_serial_num}")
        output_json=$(fn_OutputJsonConcat 'product_uuid' "${product_uuid}")
        output_json=$(fn_OutputJsonConcat 'product_wakeup_type' "${product_wakeup_type}")
        output_json=$(fn_OutputJsonConcat 'product_sku_num' "${product_sku_num}")
        output_json=$(fn_OutputJsonConcat 'product_resolution' "${product_resolution}")
    fi
}

#########  2.2 - Distro OS Info  #########
fn_DistroOSInfo(){
    # https://gist.github.com/natefoo/814c5bf936922dad97ff
    # lsb_release -a  / /etc/lsb-release

    local distro_release_file=${distro_release_file:-}
    local distro_fullname=${distro_fullname:-}
    local distro_official_site=${distro_official_site:-}
    distro_name=${distro_name:-}
    distro_version_id=${distro_version_id:-}
    distro_codename=${distro_codename:-}
    distro_family_own=${distro_family_own:-}

    # CentOS 5, CentOS 6, Debian 6 has no file /etc/os-release
    if [[ -s '/etc/os-release' ]]; then
        distro_release_file='/etc/os-release'
        local l_distro_release_info
        l_distro_release_info=$(sed -r -n 's@=@|@g;s@"@@g;/^$/d;p' "${distro_release_file}")

        #distro name，eg: centos/rhel/fedora,debian/ubuntu,opensuse/sles, arch
        distro_name=$(echo "${l_distro_release_info}" | sed -r -n '/^ID\|/{s@^.*?\|(.*)$@\L\1@g;p}')

        #version id, eg: 7/8, 16.04/16.10, 13.2/42.2
        distro_version_id=$(echo "${l_distro_release_info}" | sed -r -n '/^VERSION_ID\|/{s@^.*?\|(.*)$@\L\1@g;p}')
        distro_version_id_details=''

        case "${distro_name,,}" in
            arch )
                # file /etc/arch-release empty
                # version id is rolling
                distro_version_id=$(echo "${l_distro_release_info}" | sed -r -n '/^BUILD_ID\|/{s@^.*?\|(.*)$@\L\1@g;p}')
                ;;
            debian ) [[ -f /etc/debian_version && -s /etc/debian_version ]] && distro_version_id=$(cat /etc/debian_version) ;;
            ubuntu )
                distro_version_id_details=$(echo "${l_distro_release_info}" | sed -r -n '/^VERSION\|/{s@^.*?\|([[:digit:].]+).*$@\L\1@g;p}')
                [[ -n "${distro_version_id_details}" ]] || distro_version_id_details=$(echo "${l_distro_release_info}" | sed -r -n '/^PRETTY_NAME\|/{s@^.*?\|[^[:digit:]]+([[:digit:].]+).*$@\L\1@g;p}')
                ;;
            centos )
                if [[ "${distro_version_id:0:1}" -ge 6 && -s /etc/redhat-release ]]; then
                    # 7.5.1804    7.1804
                    distro_version_id=$(sed -r -n 's@^[^[:digit:]]*([^[:space:]]*).*$@\1@g;p' /etc/redhat-release)
                fi
                ;;
            alpine ) [[ -f /etc/alpine-release && -s /etc/alpine-release ]] && distro_version_id=$(cat /etc/alpine-release) ;;
        esac

        #distro full pretty name, for CentOS ,file redhat-release is more detailed
        if [[ -s '/etc/redhat-release' ]]; then
            distro_fullname=$(cat /etc/redhat-release)
        else
            distro_fullname=$(echo "${l_distro_release_info}" | sed -r -n '/^PRETTY_NAME\|/{s@^.*?\|(.*)$@\1@g;p}')

            case "${distro_name,,}" in
                debian )
                    if fnBase_CommandExistIfCheck 'lsb_release'; then
                        distro_fullname=$(lsb_release -d 2> /dev/null | sed -r -n 's@^[^:]+:[[:space:]]*@@g;p')
                    else
                        distro_fullname=$(echo "${distro_fullname}" | sed -r -n 's@[[:space:]]+[[:digit:].]+[[:space:]]+@ '"${distro_version_id}"' @g;p')
                    fi
                    ;;
            esac
        fi

        # Fedora, Debian，SUSE has no parameter ID_LIKE, only has ID
        # CentOS7 'rhel fedora'
        # Amazon Linux ID_LIKE v1 'rhel fedora'; v2 'centos rhel fedora'
        # Arch Linu only has ID, has no ID_LIKE
        # Pop!_OS 'ubuntu debian'
        distro_family_own=$(echo "${l_distro_release_info}" | sed -r -n '/^ID_LIKE\|/{s@^.*?\|(.*)$@\L\1@g;p}')
        [[ "$distro_family_own" == '' ]] && distro_family_own="${distro_name}"

        if [[ "${distro_name}" == 'amzn' || "${distro_family_own,,}" =~ (centos|fedora|rhel) ]]; then
            # amzn <==> The Amazon Linux
            distro_family_own='rhel'
        elif [[ "${distro_name}" == 'alpine' ]]; then
            distro_family_own='alpine'
        elif [[ "${distro_name}" == 'arch' || "${distro_name}" =~ ^manjaro ]]; then
            distro_family_own='arch'
        elif [[ "${distro_family_own,,}" =~ ^(debian|ubuntu) ]]; then
            distro_family_own='debian'
        fi

        # GNU/Linux distribution official site
        distro_official_site=$(echo "${l_distro_release_info}" | sed -r -n '/^HOME_URL\|/{s@^.*?\|(.*)$@\L\1@g;p}')

        case "${distro_name,,}" in
            debian|ubuntu ) distro_codename=$(echo "${l_distro_release_info}" | sed -r -n '/^VERSION\|/{s@^.*?\|[^\(]+\(([^[:space:]\)]+).*$@\L\1@g;p}') ;;
            opensuse ) distro_codename=$(sed -r -n '/^CODENAME[[:space:]]*=/{s@^[^=]+=[[:space:]]*(.*)$@\L\1@g;p}' /etc/SuSE-release) ;;
            * ) distro_codename='' ;;
        esac
    elif [[ -s '/etc/redhat-release' ]]; then
        distro_release_file='/etc/redhat-release' # for CentOS 5, CentOS 6
        distro_family_own='rhel'   # family is rhel (RedHat)
        distro_fullname=$(cat "${distro_release_file}")

        # distro_name=$(rpm -q --qf "%{name}" -f "${distro_release_file}") # centos-release , fedora-release
        # distro_name=${distro_name%%-*}    # centos, fedora
        distro_name="${distro_fullname%% *}"
        distro_name="${distro_name,,}"

        distro_version_id=$(sed -r -n 's@^[^[:digit:]]*([^[:space:]]*).*$@\1@g;p' "${distro_release_file}")
    elif [[ -s '/etc/debian_version' && -s '/etc/issue.net' ]]; then
        distro_release_file='/etc/issue.net'   #Debian GNU/Linux 6.0
        distro_name=$(sed -r -n 's@([^[:space:]]*).*@\L\1@p' "${distro_release_file}")
        distro_version_id=$(sed -r -n 's@[^[:digit:]]*([[:digit:]]{1}).*@\1@p' "${distro_release_file}") #6
        distro_fullname=$(cat "${distro_release_file}")
        distro_family_own='debian'   # family is debian (Debian)

        # 6.0|Squeeze|2011-02-06|2016-02-29
        # 5.0|Lenny|2009-02-14|2012-02-06
        # 4.0|Etch|2007-04-08|2010-02-15
        # 3.1|Sarge|2005-06-06|2008-03-31
        # 3.0|Woody|2002-07-19|2006-06-30
        # 2.2|Potato|2000-08-15|2003-06-30
        # 2.1|Slink|1999-03-09|2000-10-30
        # 2.0|Hamm|1998-07-24|
        # 1.3|Bo|1997-07-02|
        # 1.2|Rex|1996-12-12|
        # 1.1|Buzz|1996-06-17|
        case "${distro_version_id:0:1}" in
            6 ) distro_codename='squeeze' ;;
            5 ) distro_codename='lenny' ;;
            4 ) distro_codename='etch' ;;
            * ) distro_codename='' ;;
        esac
    fi

    # - Convert distro family name
    case "${distro_family_own,,}" in
        debian ) distro_family_own='Debian' ;;
        suse|sles ) distro_family_own='SUSE' ;;
        rhel ) distro_family_own='RedHat' ;;
        alpine ) distro_family_own='Alpine Linux' ;;
        arch ) distro_family_own='Arch Linux' ;;
        * ) distro_family_own='Unknown' ;;
    esac

    # - Retrieve release date & eol date
    distro_release_date=${distro_release_date:-}
    distro_eol_date=${distro_eol_date:-}
    distro_is_eol=${distro_is_eol:-}
    distro_eol_timestamp=${distro_eol_timestamp:-}

    if [[ "${internet_connect_status}" -eq 1 ]]; then
        case "${distro_name}" in
            rhel|centos|debian|ubuntu )
                local l_distro_release_date_info
                local l_distro_version_id
                if [[ "${distro_name}" == 'centos' && "${distro_version_id}" =~ ^[0-9]+.[0-9]+.[0-9]+$ ]]; then
                    # 7.5.1804 --> 7.1804
                    # sed -r -n 's@^([[:digit:]]+.)[[:digit:]]+.([[:digit:]]+$)@\1\2@g;p'
                    l_distro_version_id=$(echo "${distro_version_id}" | cut -d. -f1,3)
                elif [[ "${distro_name}" == 'ubuntu' && -n "${distro_version_id_details}" ]]; then
                    l_distro_version_id="${distro_version_id_details}"
                else
                    l_distro_version_id="${distro_version_id}"
                fi

                l_distro_release_date_info=$(${download_method} "${distro_release_date_list}" 2> /dev/null | sed -r -n '/^'"${distro_name}"'\|'"${l_distro_version_id}"'\|/{p}')

                # distro name|version|distro_codename|release date|eol date|is eol
                if [[ -n "${l_distro_release_date_info}" ]]; then
                    distro_release_date=$(echo "${l_distro_release_date_info}" | cut -d\| -f4)
                    distro_eol_date=$(echo "${l_distro_release_date_info}" | cut -d\| -f5)
                    distro_eol_timestamp=$(echo "${l_distro_release_date_info}" | cut -d\| -f6)
                    if [[ -n "${distro_eol_timestamp}" ]]; then
                        if [[ $(date +'%s') -ge "${distro_eol_timestamp}" ]]; then
                            distro_is_eol=1
                        else
                            distro_is_eol=0
                        fi
                    else
                        distro_eol_timestamp=''
                        distro_is_eol=$(echo "${l_distro_release_date_info}" | cut -d\| -f7)
                    fi
                fi
                ;;
        esac
    fi

    if [[ "${json_output}" -ne 1 ]]; then
        fnBase_CentralOutputTitle 'GNU/Linux Distro OS Information'
        fnBase_OutputControl 'Pretty Name' "${distro_fullname}"
        fnBase_OutputControl 'Distro Name' "${distro_name}"
        fnBase_OutputControl 'Code Name' "${distro_codename}"
        fnBase_OutputControl 'Version ID' "${distro_version_id}"
        fnBase_OutputControl 'Family Name' "${distro_family_own}"
        fnBase_OutputControl 'Official Site' "${distro_official_site}"
        fnBase_OutputControl 'Release Date' "${distro_release_date}"
        fnBase_OutputControl 'End Of Life' "${distro_eol_date}"
    else
        output_json=$(fn_OutputJsonConcat 'distro_fullname' "${distro_fullname}")
        output_json=$(fn_OutputJsonConcat 'distro_name' "${distro_name}")
        output_json=$(fn_OutputJsonConcat 'distro_codename' "${distro_codename}")
        output_json=$(fn_OutputJsonConcat 'distro_version_id' "${distro_version_id}")
        output_json=$(fn_OutputJsonConcat 'distro_family_own' "${distro_family_own}")
        output_json=$(fn_OutputJsonConcat 'distro_official_site' "${distro_official_site}")
        output_json=$(fn_OutputJsonConcat 'distro_release_date' "${distro_release_date}")
        output_json=$(fn_OutputJsonConcat 'distro_eol_date' "${distro_eol_date}")
        output_json=$(fn_OutputJsonConcat 'distro_eol_timestamp' "${distro_eol_timestamp}")
        output_json=$(fn_OutputJsonConcat 'distro_is_eol' "${distro_is_eol}")
    fi
}

#########  2.3 - Distro Machine Info  #########
fn_DistroMachineInfo(){
    # - Machine Info
    local distro_os_arch=${distro_os_arch:-}
    local distro_raspberrypi_type=${distro_raspberrypi_type:-}
    local distro_kernel_version=${distro_kernel_version:-}
    local distro_machine_id=${distro_machine_id:-}
    local distro_boot_id=${distro_boot_id:-}
    local distro_virtualization=${distro_virtualization:-}
    local distro_static_hostname=${distro_static_hostname:-}
    local distro_pretty_hostname=${distro_pretty_hostname:-}
    local distro_icon_name=${distro_icon_name:-}
    local distro_chassis=${distro_chassis:-}
    local distro_deployment_type=${distro_deployment_type:-}
    local distro_location=${distro_location:-}
    local distro_tomezone=${distro_tomezone:-}
    local distro_install_time=${distro_install_time:-}
    local distro_boot_time=${distro_boot_time:-}
    local distro_uptime=${distro_uptime:-}
    local distro_bash_type=${distro_bash_type:-}
    distro_pack_manager=${distro_pack_manager:-}
    local distro_desktop_environment=${distro_desktop_environment:-}
    local distro_firewall_type=${distro_firewall_type:-}
    local distro_python_type=${distro_python_type:-}

    local distro_machine_hardware_name=${distro_machine_hardware_name:-}

    if fnBase_CommandExistIfCheck 'uname'; then
        local distro_machine_hardware_name=$(uname -m)

        case "${distro_machine_hardware_name}" in
            x86_64|amd64|i?86_64 ) distro_os_arch='amd64' ;;
            aarch64|arm64 ) distro_os_arch='arm64' ;;
            i?86|x86 ) distro_os_arch='386' ;;
            s390x ) distro_os_arch='s390x' ;;
            arm* )
                # For ARM architecture
                # armhf - armv7h, armv7l
                # armel - armv6h
                # arm64 - aarch64

                # For raspberrypi 'dpkg --print-architecture' ==> armhf
                local l_dpkg_arch=''
                l_dpkg_arch=$(dpkg --print-architecture 2> /dev/null)

                if [[ -n "${l_dpkg_arch}" ]]; then
                    distro_os_arch="${l_dpkg_arch}"
                else
                    distro_os_arch='arm'
                fi
                ;;
            * ) distro_os_arch="${distro_machine_hardware_name}" ;;
        esac

        distro_kernel_version=$(uname -s -r 2> /dev/null)
    fi

    [[ -z "${distro_kernel_version}" ]] && distro_kernel_version=$(fn_MachineInfoValExtraction 'Kernel')

    # Raspberry Pi 4 Model B Rev 1.1
    local l_raspberrypi_check=''
    # https://stackoverflow.com/questions/46163678/get-rid-of-warning-command-substitution-ignored-null-byte-in-input
    [[ -f /sys/firmware/devicetree/base/model ]] && l_raspberrypi_check=$(tr -d '\0' < /sys/firmware/devicetree/base/model 2> /dev/null | sed -r -n '/Raspberry Pi/{p}')
    
    if [[ -n "${l_raspberrypi_check}" ]]; then
        # Raspberry Pi Type from script raspi-config  https://www.raspberrypi.org/documentation/configuration/raspi-config.md
        local l_raspberrypi_type_name=''

        if [[ -n $(sed -r -n '/^Revision\s*:\s*[ 123][0-9a-fA-F][0-9a-fA-F][0-9a-fA-F]0[9cC][0-9a-fA-F]$/p' /proc/cpuinfo 2> /dev/null) ]]; then
            l_raspberrypi_type_name='Zero'
        elif [[ -n $(sed -r -n '/^Revision\s*:\s*[ 123][0-9a-fA-F][0-9a-fA-F][0-9a-fA-F]11[0-9a-fA-F]$/p' /proc/cpuinfo 2> /dev/null) ]]; then
            l_raspberrypi_type_name='4'
        elif [[ -n $(sed -r -n '/^Revision\s*:\s*[ 123][0-9a-fA-F][0-9a-fA-F][0-9a-fA-F]04[0-9a-fA-F]$/p' /proc/cpuinfo 2> /dev/null) ]]; then
            l_raspberrypi_type_name='2'
        # elif [[ -n $(sed -r -n '/^Revision\s*:\s*[ 123][0-9a-fA-F][0-9a-fA-F][0-9a-fA-F]11[0-9a-fA-F]$/p' /proc/cpuinfo 2> /dev/null) ]]; then
        #     l_raspberrypi_type_name='1'
        elif [[ -n $(sed -r -n '/^Revision\s*:\s*[ 123][0-9a-fA-F][0-9a-fA-F][0-9a-fA-F]0[0-36][0-9a-fA-F]$/p' /proc/cpuinfo 2> /dev/null) ]]; then
            l_raspberrypi_type_name='1'
        fi

        [[ -n "${l_raspberrypi_type_name}" ]] && distro_raspberrypi_type="Raspberry Pi ${l_raspberrypi_type_name}"
    fi

    if [[ -s '/etc/machine-id' ]]; then
        distro_machine_id=$(cat /etc/machine-id)
    elif [[ -s '/var/lib/dbus/machine-id' ]]; then
        distro_machine_id=$(cat /var/lib/dbus/machine-id)
    fi
    [[ -z ${distro_machine_id} ]] && distro_machine_id=$(fn_MachineInfoValExtraction 'Machine ID')
    # https://unix.stackexchange.com/questions/144812/generate-consistent-machine-unique-id#144892
    [[ -d /proc/sys/kernel/random ]] && distro_boot_id=$(sed -r 's@[[:punct:]]*@@g' /proc/sys/kernel/random/boot_id 2> /dev/null)
    [[ -z "${distro_boot_id}" ]] && distro_boot_id=$(fn_MachineInfoValExtraction 'Boot ID')

    # Xen / KVM / OpenVZ
    distro_virtualization=$(fn_MachineInfoValExtraction 'Virtualization')

    if [[ -f '/etc/sysconfig/network' ]]; then
        distro_static_hostname=$(sed -r -n '/HOSTNAME=/{s@^[^=]+=(.*)$@\1@g;p}' /etc/sysconfig/network)    #RHEL/CentOS
    elif [[ -f '/etc/hostname' ]]; then
        distro_static_hostname=$(cat /etc/hostname)    #Debian/OpenSUSE
    fi
    [[ -z "${distro_static_hostname}" ]] && distro_static_hostname=$(fn_MachineInfoValExtraction 'Static hostname')

    distro_pretty_hostname=$(fn_MachineInfoValExtraction 'Pretty hostname' 'PRETTY_HOSTNAME')
    # https://standards.freedesktop.org/icon-naming-spec/icon-naming-spec-latest.html
    distro_icon_name=$(fn_MachineInfoValExtraction 'Icon name' 'ICON_NAME')
    # Chassis    desktop, laptop, convertible, server, tablet, handset, watch, embedded，vm, container
    distro_chassis=$(fn_MachineInfoValExtraction 'Chassis' 'CHASSIS')
    # Deployment    development, integration, staging, production
    distro_deployment_type=$(fn_MachineInfoValExtraction 'Deployment' 'DEPLOYMENT')
    # Location    LOCATION should be a human-friendly, free-form string describing the physical location of the system, if it is known and applicable. This may be as generic as "Berlin, Germany" or as specific as "Left Rack, 2nd Shelf".
    distro_location=$(fn_MachineInfoValExtraction 'Location' 'LOCATION')
    # Timezone
    fnBase_CommandExistIfCheck 'timedatectl' && distro_tomezone=$(timedatectl 2> /dev/null | sed -r -n '/Time zone:/{s@^[^:]+:[[:space:]]*(.*)@\1@g;p}')
    # for ubuntu/debian
    [[ -z "${distro_tomezone}" && -s /etc/timezone ]] && distro_tomezone=$(cat /etc/timezone)
    # for rhel/sles
    [[ -z "${distro_tomezone}" && -s /etc/sysconfig/clock ]] && distro_tomezone=$(sed -r -n '/^ZONE=/{s@"@@g;s@^[^=]+=(.*)$@\1@g;p}' /etc/sysconfig/clock)
    [[ -z "${distro_tomezone}" && -s /etc/localtime && $(readlink -f /etc/localtime) =~ /usr/share/zoneinfo/ ]] && distro_tomezone=$(readlink -f /etc/localtime | sed -r -n 's@^/usr/share/zoneinfo/(.*)$@\1@g;p')

    # Install time
    # https://unix.stackexchange.com/questions/9971/how-do-i-find-how-long-ago-a-linux-system-was-installed
    # RHEL
    # rpm -qi [basesystem|setup]
    # Install Date: Fri 01 Jun 2018 05:06:56 PM PDT      Build Host: c5b2.bsys.dev.centos.org
    fnBase_CommandExistIfCheck 'rpm' && distro_install_time=$(rpm -qi basesystem 2> /dev/null | sed -r -n '/^Install Date[[:space:]]*:/{s@[[:space:]]{2,}.*$@@g;s@^[^:]+:[[:space:]]*(.*)$@\1@g;p}')
    [[ -z "${distro_install_time}" && -d /etc/ ]] && distro_install_time=$(ls -lact --full-time /etc/ | awk 'END {print $6,$7,$8}')
    # Ubuntu
    [[ -z "${distro_install_time}" && -d /var/log/installer ]] && distro_install_time=$(ls -ltd --full-time /var/log/installer | awk 'END {print $6,$7,$8}')
    distro_install_time=$(echo "${distro_install_time}" | date +'%F %T %z %Z' -f - 2> /dev/null)

    # Boot time
    # https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/deployment_guide/s2-proc-uptime
    [[ -f /proc/uptime ]] && distro_boot_time=$(date +"%F %T" --date="- $(sed -r -n 's@^([^[:space:]]+).*@\1@gp' /proc/uptime) second" 2> /dev/null)
    if [[ -z "${distro_boot_time}" ]]; then
        if fnBase_CommandExistIfCheck 'uptime'; then
            distro_boot_time=$(uptime -s 2> /dev/null)
        else
            # who -b  -- not list second
            distro_boot_time=$(last reboot -F | sed -r -n '/^reboot/{1{s@.*([[:upper:]]+.*)(-|still).*@\1@g;p}}')
        fi
    fi
    distro_boot_time=$(echo "${distro_boot_time}" | date +'%F %T %z %Z' -f - 2> /dev/null)

    # Uptime
    if [[ -n $(uptime -p 2> /dev/null) ]]; then
        distro_uptime=$(uptime -p)
    else
        distro_uptime=$(uptime | sed -r -n 's@.*(up.*)[[:digit:]]+[[:space:]]*user.*@\1@g;s@[[:punct:]]*[[:space:]]*$@@g;s@[[:space:]]+@ @g;s@[[:space:]]+([[:digit:]]+):([[:digit:]]+)@ \1hours, \2minutes@g;p')
    fi
    distro_uptime=$(echo "${distro_uptime}" | sed -r -n 's@years@y@g;s@months@m@g;s@days@d@g;s@hours@h@g;s@minutes@Min@g;p')

    # Bash Type
    # /etc/shells
    if [[ "${SHELL:-}" != '' ]]; then
        distro_bash_type="${SHELL##*/}"
    fi
    # bash version check  ${BASH_VERSINFO[@]} ${BASH_VERSION}
    case "${distro_bash_type}" in
        bash ) distro_bash_type="bash $(bash --version | sed -r -n '1s@[^[:digit:]]*([[:digit:].]*).*@\1@p')";;
    esac
    # cat /proc/$$/cmdline
    # cat /proc/$PPID/cmdline

    # Package Manager (Debian/SUSE/RedHat)
    # OpenSUSE has utility apt-get, aptitude.
    case "${distro_family_own,,}" in
        arch* ) distro_pack_manager='pacman' ;;
        alpine* ) distro_pack_manager='apk' ;;
        debian ) distro_pack_manager='apt-get' ;;
        suse ) distro_pack_manager='zypper' ;;
        redhat )
            if fnBase_CommandExistIfCheck 'dnf'; then
                distro_pack_manager='dnf'
            else
                distro_pack_manager='yum'
            fi
            ;;
    esac

    # Desktop environment
    # ls /usr/share/xsessions/*.desktop 2> /dev/null | wc -l
    if [[ -d /usr/share/xsessions/ && $(find /usr/share/xsessions/ -type f -name '*.desktop' -print 2> /dev/null | wc -l) -gt 0 ]]; then
        # DESKTOP_SESSION=i3 / XDG_SESSION_DESKTOP=i3 / XDG_CURRENT_DESKTOP=i3 / GDMSESSION=i3
        distro_desktop_environment=$(env 2> /dev/null | sed -r -n '/XDG_CURRENT_DESKTOP/{s@[^=]+=(.*)$@\1@g;p}')
        [[ -z "${distro_desktop_environment}" ]] && distro_desktop_environment=$(env 2> /dev/null | sed -r -n '/XDG_SESSION_DESKTOP/{s@[^=]+=(.*)$@\1@g;p}')
        [[ -z "${distro_desktop_environment}" ]] && distro_desktop_environment=$(env 2> /dev/null | sed -r -n '/DESKTOP_SESSION/{s@[^=]+=(.*)$@\1@g;p}')

        if [[ -z "${distro_desktop_environment}" ]]; then
            # distro_desktop_environment=$(ps -ef 2> /dev/null | sed -r -n 's@_@-@g;/bin\/(gnome|budgie|kde|plasma|mate|cinnamon|lxde|xfce|jwm)[^-]*-session$/{s@.*?bin\/([^-]+)-session.*@\1@g;p}')
            distro_desktop_environment=$(ps -ef 2> /dev/null | sed -r -n 's@_@-@g;/(bin\/)?(gnome|budgie|kde|plasma|mate|cinnamon|lxde|xfce|jwm)[^-]*-session$/{/--.*?session/d;s@.*?bin\/([^-]+)-session.*@\1@g;s@.*?[[:space:]]+([^-]+)-session.*@\1@g;p}')

            case "${distro_desktop_environment}" in
                xfce*|budgie*|cinnamon* ) distro_desktop_environment="${distro_desktop_environment^}" ;;
                gnome*|mate*|kde* ) distro_desktop_environment="${distro_desktop_environment^^}" ;;
                plasma* ) distro_desktop_environment='KDE Plasma' ;;
            esac
        fi
    fi

    if [[ -z "${distro_desktop_environment}" ]]; then
        # -m Display information about the window manager and the environment.
        fnBase_CommandExistIfCheck 'wmctrl' && distro_desktop_environment=$(sudo -u "${login_user}" wmctrl -m 2> /dev/null | sed -r -n '/^Name/{s@^[^:]+:[[:space:]]*(.*)$@\1@g;s@[[:space:]]*shell@@Ig;p}')
    fi

    # Firewall Type
    # ufw/SuSEfirewall2/firewalld/iptables
    # Arch Linux choose ufw
    if [[ "${json_output}" -eq 1 ]]; then
        case "${distro_pack_manager}" in
            apt-get|pacman ) distro_firewall_type='ufw' ;;
            zypper )
                if [[ "${distro_name,,}" == 'opensuse' && "${distro_version_id}" =~ ^15 ]]; then
                    distro_firewall_type='firewalld'
                else
                    distro_firewall_type='SuSEfirewall2'
                fi
                ;;
            dnf|yum )
                if [[ $("${distro_pack_manager}" info firewalld 2>&1 | sed -r -n '/^Name[[:space:]]+:/{s@^[^:]+:[[:space:]]*(.*)$@\1@g;p;q}') == 'firewalld' ]]; then
                    distro_firewall_type='firewalld'
                else
                    distro_firewall_type='iptables'
                fi
                ;;
        esac
    fi

    # Python Type
    if fnBase_CommandExistIfCheck 'python3'; then
        distro_python_type='python3'
    elif fnBase_CommandExistIfCheck 'python'; then
        distro_python_type='python'
    fi

    if [[ "${json_output}" -ne 1 ]]; then
        fnBase_CentralOutputTitle 'GNU/Linux Distro Machine Information'
        fnBase_OutputControl 'Static hostname' "${distro_static_hostname}"
        fnBase_OutputControl 'Pretty hostname' "${distro_pretty_hostname}"
        [[ -n "${distro_raspberrypi_type}" ]] && fnBase_OutputControl 'Raspberry Pi Type' "${distro_raspberrypi_type}"
        fnBase_OutputControl 'OS Architecture' "${distro_machine_hardware_name} (${distro_os_arch})"
        fnBase_OutputControl 'Kernel Version' "${distro_kernel_version}"
        fnBase_OutputControl 'Machine ID' "${distro_machine_id}"
        fnBase_OutputControl 'Virtualization' "${distro_virtualization}"
        fnBase_OutputControl 'Chassis' "${distro_chassis}"
        fnBase_OutputControl 'Icon name' "${distro_icon_name}"
        fnBase_OutputControl 'Deployment Environment' "${distro_deployment_type}"
        fnBase_OutputControl 'Location' "${distro_location}"
        fnBase_OutputControl 'Time zone' "${distro_tomezone}"
        fnBase_OutputControl 'Default Shell' "${distro_bash_type}"
        fnBase_OutputControl 'Package Manager' "${distro_pack_manager}"
        fnBase_OutputControl 'Desktop Environment' "${distro_desktop_environment}"
        # [[ "${json_output}" -eq 1 ]] && fnBase_OutputControl 'Firewall Type' "${distro_firewall_type}"
        [[ -n "${distro_install_time}" ]] && fnBase_OutputControl 'Install/Build time' "${distro_install_time} (approx)"
        fnBase_OutputControl 'Boot ID' "${distro_boot_id}"
        fnBase_OutputControl 'Boot time' "${distro_boot_time}"
        fnBase_OutputControl 'Running time' "${distro_uptime}"
    else
        output_json=$(fn_OutputJsonConcat 'distro_static_hostname' "${distro_static_hostname}")
        output_json=$(fn_OutputJsonConcat 'distro_pretty_hostname' "${distro_pretty_hostname}")
        [[ -n "${distro_raspberrypi_type}" ]] && output_json=$(fn_OutputJsonConcat 'distro_raspberrypi_type' "${distro_raspberrypi_type}")
        output_json=$(fn_OutputJsonConcat 'distro_machine_hardware_name' "${distro_machine_hardware_name}")
        output_json=$(fn_OutputJsonConcat 'distro_os_arch' "${distro_os_arch}")
        output_json=$(fn_OutputJsonConcat 'distro_kernel_version' "${distro_kernel_version}")
        output_json=$(fn_OutputJsonConcat 'distro_machine_id' "${distro_machine_id}")
        output_json=$(fn_OutputJsonConcat 'distro_virtualization' "${distro_virtualization}")
        output_json=$(fn_OutputJsonConcat 'distro_chassis' "${distro_chassis}")
        output_json=$(fn_OutputJsonConcat 'distro_icon_name' "${distro_icon_name}")
        output_json=$(fn_OutputJsonConcat 'distro_deployment_type' "${distro_deployment_type}")
        output_json=$(fn_OutputJsonConcat 'distro_location' "${distro_location}")
        output_json=$(fn_OutputJsonConcat 'distro_tomezone' "${distro_tomezone}")
        output_json=$(fn_OutputJsonConcat 'distro_pack_manager' "${distro_pack_manager}")
        output_json=$(fn_OutputJsonConcat 'distro_desktop_environment' "${distro_desktop_environment}")
        output_json=$(fn_OutputJsonConcat 'distro_firewall_type' "${distro_firewall_type}")
        output_json=$(fn_OutputJsonConcat 'distro_python_type' "${distro_python_type}")
        output_json=$(fn_OutputJsonConcat 'distro_install_time' "${distro_install_time}")
        output_json=$(fn_OutputJsonConcat 'distro_boot_id' "${distro_boot_id}")
        output_json=$(fn_OutputJsonConcat 'distro_boot_time' "${distro_boot_time}")
        # output_json=$(fn_OutputJsonConcat 'distro_uptime' "${distro_uptime}")
    fi
}

#########  2.4 - Package Manager Info  #########
fn_PackageManagerInfo(){
    # - Package Info
    # local distro_pack_manager=${distro_pack_manager:-}
    local distro_pack_count=${distro_pack_count:-}
    local distro_pack_patch=${distro_pack_patch:-}
    local distro_pack_update=${distro_pack_update:-}
    local distro_pack_dist_upgrade=${distro_pack_dist_upgrade:-}

    case "${distro_pack_manager}" in
        pacman )
            distro_pack_count=$(pacman -Qq 2> /dev/null | wc -l)

            # https://bbs.archlinux.org/viewtopic.php?id=8181
            distro_pack_update=$(sudo pacman -Syup --noprogressbar 2>&1 | sed -r -n '/tar\./{p}' | wc -l)
            ;;
        apt-get )
            # /usr/lib/update-notifier/apt-check
            # apt-get update &> /dev/null    # update cache

            # apt list --installed 2> /dev/null | sed -r -n '/installed/{p}' | wc -l
            # dpkg -l 2> /dev/null | sed -r -n '/^i+[[:space:]]*/{p}' | wc -l
            distro_pack_count=$(dpkg --get-selections 2> /dev/null | wc -l)

            # apt list --upgradable
            distro_pack_update=$(apt-get upgrade --dry-run 2> /dev/null | sed -r -n '/^[[:digit:]]+.*?(upgraded|new|remove|upgraded)/{s@[[:space:]]*(,|and)[[:space:]]*@\n@g;p}' | sed -r -n '/[[:space:]]+not[[:space:]]+upgraded/{s@^([[:digit:]]+).*@not upgrade \1@g;p}; /[[:space:]]+upgraded/{s@^([[:digit:]]+).*@upgrade \1@g;p}; /[[:space:]]+new/{s@^([[:digit:]]+).*@new \1@g;p}; /[[:space:]]+remove/{s@^([[:digit:]]+).*@remove \1@g;p};' | sed ':a;N;$!ba;s@\n@, @g' | sed -r -n 's@,?[^[:digit:]]+0@@g;p')
            local l_security_count_update=${l_security_count_update:-0}
            l_security_count_update=$(apt-get upgrade --dry-run | sed -r -n '/^Inst[[:space:]]+.*?security/{p}' | wc -l)
            [[ "${l_security_count_update}" -ne 0 ]] && distro_pack_update="${distro_pack_update} (security ${l_security_count_update})"

            distro_pack_dist_upgrade=$(apt-get dist-upgrade --dry-run 2> /dev/null | sed -r -n '/^[[:digit:]]+.*?(upgraded|new|remove|upgraded)/{s@[[:space:]]*(,|and)[[:space:]]*@\n@g;p}' | sed -r -n '/[[:space:]]+not[[:space:]]+upgraded/{s@^([[:digit:]]+).*@not upgrade \1@g;p}; /[[:space:]]+upgraded/{s@^([[:digit:]]+).*@upgrade \1@g;p}; /[[:space:]]+new/{s@^([[:digit:]]+).*@new \1@g;p}; /[[:space:]]+remove/{s@^([[:digit:]]+).*@remove \1@g;p};' | sed ':a;N;$!ba;s@\n@, @g' | sed -r -n 's@,?[^[:digit:]]+0@@g;p')
            local l_security_count_dist_upgrade=${l_security_count_dist_upgrade:-0}
            l_security_count_dist_upgrade=$(apt-get dist-upgrade --dry-run | sed -r -n '/^Inst[[:space:]]+.*?security/{p}' | wc -l)
            [[ "${l_security_count_dist_upgrade}" -ne 0 ]] && distro_pack_dist_upgrade="${distro_pack_dist_upgrade} (security ${l_security_count_dist_upgrade})"
            ;;
        yum )
            distro_pack_count=$(rpm -qa  2> /dev/null | wc -l)
            # distro_pack_count=$(yum list installed 2> /dev/null | sed -r -n '/installed/{p}' | wc -l)

            # yum list updates
            distro_pack_update=$(yum check-update | sed -r -n '/updates$/{p}' | wc -l)
            if [[ "${distro_pack_update}" -ne 0 ]]; then
                local l_security_count_update=${l_security_count_update:-0}
                l_security_count_update=$(yum --security check-update 2>&1 | sed -r -n '/security.*?package/{s@^([^[:space:]]+).*@\1@g;s@No@0@g;p}')
                [[ "${l_security_count_update}" -ne 0 ]] && distro_pack_update="${distro_pack_update} (security ${l_security_count_update})"
            else
                distro_pack_update=''
            fi
            ;;
        dnf )
            # dnf list [--all]    #  installed as well as available
            # list --available
            distro_pack_count=$(rpm -qa  2> /dev/null | wc -l)
            # distro_pack_count=$(dnf list --installed 2> /dev/null| wc -l)

            distro_pack_update=$(dnf check-update 2> /dev/null | sed -r -n '/[[:space:]]+update/{p}' | wc -l)
            local l_security_count_update=${l_security_count_update:-0}
            l_security_count_update=$(dnf check-update --security 2> /dev/null | sed -r -n '/[[:space:]]+update/{p}' | wc -l)
            [[ "${l_security_count_update}" -ne 0 ]] && distro_pack_update="${distro_pack_update} (security ${l_security_count_update})"

            # dnf distro-sync [<package-spec>...]
            # As necessary upgrades, downgrades or keeps selected installed packages to match the latest version available from any enabled repository. If no package is given, all installed packages are considered.
            ;;
        zypper )
            # In the Status column the search command distinguishes between user installed packages (i+) and automatically installed packages (i).
            # distro_pack_count=$(rpm -qa  2> /dev/null | wc -l)
            distro_pack_count=$(zypper search -i -t package 2> /dev/null | sed -r -n '/^i\+?[[:space:]]+\|?/{p}' | wc -l)

            # List all applicable patches.
            # list-patches (lp) [options]
            distro_pack_patch=$(zypper lp 2> /dev/null | sed -r -n '/^Found[[:space:]]+.*:$/,${/^$/d;/^Found[[:space:]]+/{s@^Found[[:space:]]+([[:digit:]]+).*patches:$@total \1@g};/optional/{s@^([[:digit:]]+).*optional.*@optional \1@g;};/patches[[:space:]]+needed/{s@^([[:digit:]]+)[^(]+\(([[:digit:]]+)[[:space:]]+security.*$@needed \1 (security \2)@g;};p}' | sed ':a;N;$!ba;s@\n@, @g')
            # Check for patches. Displays a count of applicable patches and how many of them have the security category.
            # patch-check (pchk)
            # distro_pack_patch=$(zypper pchk 2> /dev/null | sed -r -n '/^Found[[:space:]]+.*:$/{/^Found[[:space:]]+/{s@^Found[[:space:]]+([[:digit:]]+).*patches:$@total \1@g};p};/^(security|optional)[[:space:]]+\|/{s@^([^[:space:]]+).*?\|[[:space:]]+([[:digit:]]+).*$@\1 \2@g;p}' | sed ':a;N;$!ba;s@\n@, @g')

            # List available updates.
            # list-updates (lu) [options]
            # zypper lu | grep -c '^v'
            # Update installed packages with newer versions, where possible.
            # update (up) [options] [packagename]...
            distro_pack_update=$(zypper up -D -y 2> /dev/null | sed -r -n '/^[[:digit:]]+[[:space:]]+packages/{s@^([[:digit:]]+)[^,]+,[[:space:]]+([[:digit:]]+).*$@upgrade \1, new \2@g;p}')

            # Perform a distribution upgrade.
            # dist-upgrade (dup) [options]
            distro_pack_dist_upgrade=$(zypper dup -D -y 2> /dev/null | sed -r -n '/^[[:digit:]]+.*?(upgrade|downgrade|new|remove)/{s@[[:space:]]*,[[:space:]]*@\n@g;s@[[:punct:]]*$@@g;p}' | sed -r -n '/[[:space:]]+upgrade/{s@^([[:digit:]]+).*@upgrade \1@g;p}; /[[:space:]]+downgrade/{s@^([[:digit:]]+).*@downgrade \1@g;p}; /[[:space:]]+new/{s@^([[:digit:]]+).*@new \1@g;p}; /[[:space:]]+remove/{s@^([[:digit:]]+).*@remove \1@g;p}' | sed ':a;N;$!ba;s@\n@, @g')
            ;;
    esac

    if [[ "${json_output}" -ne 1  ]]; then
        fnBase_CentralOutputTitle 'GNU/Linux Distro Package Information'
        #statements
        fnBase_OutputControl 'Package Manager' "${distro_pack_manager}"
        fnBase_OutputControl 'Installed Packages' "${distro_pack_count}"
        fnBase_OutputControl 'Available Patch' "${distro_pack_patch}"
        fnBase_OutputControl 'Available Update' "${distro_pack_update}"
        fnBase_OutputControl 'Available Dist-upgrade' "${distro_pack_dist_upgrade}"
    else
        # output_json=$(fn_OutputJsonConcat 'distro_pack_manager' "${distro_pack_manager}")
        output_json=$(fn_OutputJsonConcat 'distro_pack_count' "${distro_pack_count}")
        output_json=$(fn_OutputJsonConcat 'distro_pack_patch' "${distro_pack_patch}")
        output_json=$(fn_OutputJsonConcat 'distro_pack_update' "${distro_pack_update}")
        output_json=$(fn_OutputJsonConcat 'distro_pack_dist_upgrade' "${distro_pack_dist_upgrade}")
    fi
}


#########  2.5 - IP Address Detection  #########
fn_IPInfoExtraction(){
    local l_ip=${1:-}
    local l_ip_raw_data=${l_ip_raw_data:-}
    local l_ip_info=${l_ip_info:-}

    if [[ "${l_ip}" =~ ^([0-9]{1,3}.){3}[0-9]{1,3}$ ]]; then
        l_ip="/${l_ip}"
    else
        l_ip=''
    fi

    # - https://ipdata.co/ (check if is tor, has no isp info, merge asn, org info as AS info)  
    # 1500 requests per day per ip, need API Key.
    # curl -fsL https://api.ipdata.co
    # curl -fsL https://api.ipdata.co/172.217.24.142?api-key=test
    # if [[ -z "${l_ip_info}" ]]; then
    #     l_ip_raw_data=$(${download_method} https://api.ipdata.co"${l_ip}"?api-key=test)
    #     if [[ -n "${l_ip_raw_data}" ]]; then
    #         # ip|city|region|country|timezone|asn|isp
    #         # 172.217.24.142|Mountain View|California|US|America/Los_Angeles|AS15169 Google LLC|
    #         l_ip_info=$(echo "${l_ip_raw_data}" | sed -r -n '/"(ip|city|region|country_code|asn|organisation)"/p;/time_zone/,/\}/{/name/{s@name@timezone@g;p}};' | sed -r -n '/asn.*\{/d;s@^[^:]+:[[:space:]]*[[:punct:]]*([^"]+).*$@\1@g;s@[[:punct:]]*$@@g;p' | sed ':a;N;$!ba;s@\n@|@g' | awk -F\| '{printf("%s|%s|%s|%s|%s|%s %s|\n",$1,$2,$3,$4,$7,$5,$6)}')
    #     fi
    # fi

    # - ip-api.com (sometimes may be fail to connect in some regions)
    # 150 requests per minute per ip
    # curl -fsL http://ip-api.com/json
    # curl -fsL http://ip-api.com/json/172.217.24.142
    if [[ -z "${l_ip_info}" ]]; then
        l_ip_raw_data=$(${download_method} http://ip-api.com/json"${l_ip}")
        if [[ -n "${l_ip_raw_data}" && $(echo "${l_ip_raw_data}" | sed -r -n 's@.*"status":"([^"]+)".*$@\1@g;p') == 'success' ]]; then
            # query|city|regionName|countryCode|timezone|as|isp
            # 172.217.24.142|Mountain View|California|US|America/Los_Angeles|AS15169 Google LLC|Google
            l_ip_info=$(echo "${l_ip_raw_data}" | sed -r 's@","@"\n"@g;s@[{}]@\n@g;s@"?,"+@\n"@g;' | sort | sed -r -n '/"(as|isp|city|regionName|countryCode|query|timezone)"/{s@^[^:]+:"([^"]*)"$@\1@g;s@\\/@/@g;p}' | sed ':a;N;$!ba;s@\n@|@g;' | awk -F\| 'BEGIN{OFS="|"}{print $5,$2,$6,$3,$7,$1,$4}')
        fi
    fi

    # - https://ipapi.co/ (has no isp info, merge asn, org info as AS info  )
    # 1000 requests per day per ip
    # curl -fsL https://ipapi.co/json/
    # curl -fsL https://ipapi.co/172.217.24.142/json/
    if [[ -z "${l_ip_info}" ]]; then
        l_ip_raw_data=$(${download_method} https://ipapi.co"${l_ip}"/json/)
        if [[ -n "${l_ip_raw_data}" ]]; then
            # ip|city|region|country|timezone|asn|isp
            # 172.217.24.142|Mountain View|California|US|America/Los_Angeles|AS15169 Google LLC|
            l_ip_info=$(echo "${l_ip_raw_data}" | sed -r -n 's@","@"\n"@g;s@[{}]@\n@g;s@"?,"+@\n"@g;p' | sed -r -n '/^$/d; /"(ip|city|region|country|timezone|asn|org)"/{s@^[^:]+:[[:space:]]*[[:punct:]]*([^"]+).*$@\1@g;s@[[:punct:]]*$@@g;p}' | sed ':a;N;$!ba;s@\n@|@g' | awk -F\| '{printf("%s|%s|%s|%s|%s|%s %s|\n",$1,$2,$3,$4,$5,$6,$7)}')
        fi
    fi

    # - timezoneapi.io (has no as,isp info)
    # As of October 4th, 2018 TimezoneAPI will no longer support free plans. https://timezoneapi.io/announcement-oct-14-2018
    # 1,000 requests per day per ip, exceed return 429 HTTP state code
    # curl -fsL https://timezoneapi.io/api/ip
    # curl -fsL https://timezoneapi.io/api/ip/?ip=172.217.24.142
    # if [[ -z "${l_ip_info}" ]]; then
    #     l_ip_raw_data=""
    #     local l_timezoneapi_ip=${l_timezoneapi_ip:-}
    #     [[ -n "${l_ip}" ]] && l_timezoneapi_ip="/?ip=${l_ip///}"
    #     l_ip_raw_data=$(${download_method} https://timezoneapi.io/api/ip"${l_timezoneapi_ip}")
    #     if [[ -n "${l_ip_raw_data}" && $(echo "${l_ip_raw_data}" | sed -r -n 's@.*"code":"([^"]+)".*@\1@g;p') == '200'  ]]; then
    #         # ip|city|regionName|countryCode|timezone|
    #         # 172.217.24.142|Mountain View|California|US|America/Los_Angeles
    #         l_ip_info=$(echo "${l_ip_raw_data}" | sed -r -n 's@.*"data"(.*)"datetime".*@\1@g;s@","@"\n"@g;s@[{}]@\n@g;s@"?,"+@\n@g;p' | sed -r -n '/"(ip|city|state|country_code|id)"/{s@^[^:]+:"([^"]+)"$@\1@g;s@\\/@/@g;p}' | sed '$d' | sed ':a;N;$!ba;s@\n@|@g;')
    #     fi
    # fi

    # - ipinfo.io (has no timezone info)
    # 1,000 requests per day per ip, exceed return 429 HTTP state code
    # curl -fsL https://ipinfo.io/json
    # curl -fsL https://ipinfo.io/172.217.24.142/json
    if [[ -z "${l_ip_info}" ]]; then
        l_ip_raw_data=""
        l_ip_raw_data=$(${download_method} https://ipinfo.io"${l_ip}"/json)
        if [[ -n "${l_ip_raw_data}" ]]; then
            # ip|city|regionName|countryCode|timezone|as
            # 172.217.24.142|Mountain View|California|US||AS15169 Google LLC
            l_ip_info=$(echo "${l_ip_raw_data}" | sed -r -n 's@","@"\n"@g;s@[{}]@\n@g;s@"?,"+@\n@g;p' | sed -r -n '/^$/d;/"(ip|city|region|country|org)"/{s@^[^:]+:[[:space:]]*[[:punct:]]*([^"]+).*$@\1@g;s@[[:punct:]]*$@@g;p}' | sed ':a;N;$!ba;s@\n@|@g' | awk -F\| '{printf("%s|%s|%s|%s||%s\n",$1,$2,$3,$4,$5)}')
        fi
    else
        local l_ip_as=${l_ip_as:-}
        l_ip_as=$(echo "${l_ip_info}" | cut -d\| -f6)

        if [[ -z "${l_ip_as}" ]]; then
            l_ip_as=$(${download_method} "https://ipinfo.io${l_ip}/org" 2> /dev/null)
            [[ -n "${l_ip_as}" ]] && l_ip_info=$(echo "${l_ip_info}" | awk -F\| -v as="${l_ip_as}" 'BEGIN{OFS="|"}{$6=as;print}')
        fi
    fi

    # - ipsidekick (has timezone info, has no city, state, as info)
    # 1,440 requests per day per ip, exceed return 429 HTTP state code
    # curl -fsL https://ipsidekick.com/json
    # curl -fsL https://ipsidekick.com/172.217.24.142/json
    if [[ -z "${l_ip_info}" ]]; then
        l_ip_raw_data=""
        l_ip_raw_data=$(${download_method} https://ipsidekick.com/"${l_ip}"/json)
        if [[ -n "${l_ip_raw_data}" && ! "${l_ip_raw_data}" =~ error ]]; then
            # ip|city|regionName|countryCode|timezone
            # 172.217.24.142|||US|America/Los_Angeles
            l_ip_info=$(echo "${l_ip_raw_data}" | sed 's@},@\n@g;' | sed -r -n '/"country"/{s@.*code[[:punct:]]*([^[:punct:]]+).*@\1@g;p}; /"timeZone"/{s@.*name[[:punct:]]*([^"]+).*@\1@g;p}; /"ip"/{s@.*:[[:punct:]]*([^"]+).*@\1@g;p}' | sed ':a;N;$!ba;s@\n@|@g' | awk -F\| '{printf("%s|||%s|%s\n",$3,$1,$2)}')
        fi
    fi

    # - ip hostname
    # host $IP
    # nslookup $IP
    # 142.24.217.172.in-addr.arpa	 nrt20s01-in-f14.1e100.net.
    # 142.24.217.172.in-addr.arpa	 syd09s06-in-f14.1e100.net.
    # 142.24.217.172.in-addr.arpa  nrt20s01-in-f142.1e100.net.

    echo "${l_ip_info}"
    # ip|city|regionName|countryCode|timezone|as|isp
    # 172.217.24.142|Mountain View|California|US|America/Los_Angeles|AS15169 Google LLC|Google
    # AS number: Autonomous System (AS) number, separated by space
}

fn_IPInfoDetection(){
    # https://stackoverflow.com/questions/14594151/methods-to-detect-public-ip-address-in-bash
    # https://www.gnu.org/software/bash/manual/html_node/Redirections.html
    # if [[ -d "/dev" ]]; then
    #     # https://major.io/icanhazip-com-faq/
    #     # exec 6<> /dev/tcp/icanhazip.com/80
    #     # echo -e 'GET / HTTP/1.0\r\nHost: icanhazip.com\r\n\r' >&6
    #     # while read i; do [[ -n "$i" ]] && ip_public="$i" ; done <&6
    #     # exec 6>&-
    #
    #     exec 6<> /dev/tcp/ipinfo.io/80
    #     echo -e 'GET / HTTP/1.0\r\nHost: ipinfo.io\r\n\r' >&6
    #     # echo -e 'GET /ip HTTP/1.0\r\nHost: ipinfo.io\r\n\r' >&6
    #     # echo -e 'GET /country HTTP/1.0\r\nHost: ipinfo.io\r\n\r' >&6
    #
    #     # ip_public=$(cat 0<&6 | sed -r -n '/^\{/,/^\}/{/\"ip\"/{s@[[:space:],]*@@g;s@[^:]*:"([^"]*)"@\1@g;p}}')
    #     detect_ip_info=$(cat 0<&6 | sed -r -n '/^\{/,/^\}/p')
    #     exec 6>&-
    # fi    # end if /dev

    # internal ip
    ip_local=${ip_local:-'127.0.0.1'}
    # hostname -I
    fnBase_CommandExistIfCheck 'ip' && ip_local=$(ip route get 1 | sed -r -n '1{s@.*src[[:space:]]*([^[:space:]]+).*$@\1@g;p}')


    # - Detect Real External IP
    local dig_ip=${dig_ip:-}
    local dig_ip_info=${dig_ip_info:-}
    # - dig ip     get real public ip via dig
    if fnBase_CommandExistIfCheck 'dig'; then
        dig_ip=$(dig +short myip.opendns.com @resolver1.opendns.com 2> /dev/null)
    fi

    # - detected ip
    local detect_ip_info=${detect_ip_info:-}
    local detect_ip=${detect_ip:-}
    local detect_ip_city=${detect_ip_city:-}
    local detect_ip_region=${detect_ip_region:-}
    local detect_ip_country_code=${detect_ip_country_code:-}
    local detect_ip_timezone=${detect_ip_timezone:-}
    local detect_ip_as=${detect_ip_as:-}
    local detect_ip_isp=${detect_ip_isp:-}

    local ip_proxy=${ip_proxy:-}
    local ip_proxy_country_code=${ip_proxy_country_code:-}
    local ip_proxy_locate=${ip_proxy_locate:-}
    local ip_proxy_timezone=${ip_proxy_timezone:-}
    local ip_proxy_as=${ip_proxy_as:-}
    local ip_proxy_isp=${ip_proxy_isp:-}

    local ip_public=${ip_public:-}
    local ip_public_country_code=${ip_public_country_code:-}
    local ip_public_locate=${ip_public_locate:-}
    local ip_public_timezone=${ip_public_timezone:-}
    local ip_public_as=${ip_public_as:-}
    local ip_public_isp=${ip_public_isp:-}


    local detect_ip_info
    detect_ip_info=$(fn_IPInfoExtraction)
    if [[ -n "${detect_ip_info}" ]]; then
        detect_ip=$(echo "${detect_ip_info}" | cut -d\| -f1)
        if [[ -n "${detect_ip}" ]]; then
            detect_ip_city=$(echo "${detect_ip_info}" | cut -d\| -f2)
            detect_ip_region=$(echo "${detect_ip_info}" | cut -d\| -f3)
            detect_ip_country_code=$(echo "${detect_ip_info}" | cut -d\| -f4)
            detect_ip_timezone=$(echo "${detect_ip_info}" | cut -d\| -f5)
            detect_ip_as=$(echo "${detect_ip_info}" | awk -F\| '{if($6~/Inc./){$6=gensub(/^(.*Inc.).*/,"\\1","g",$6)};print $6}' 2> /dev/null)
            detect_ip_isp=$(echo "${detect_ip_info}" | awk -F\| '{print $7}')
        fi
    fi    # end if detect_ip_info

    if [[ -n "${dig_ip}" && -n "${detect_ip}" &&  "${dig_ip}" != "${detect_ip}" ]]; then
        local dig_ip_info
        dig_ip_info=$(fn_IPInfoExtraction "${dig_ip}")
        # ip|city|regionName|countryCode|timezone|as|isp
        ip_public="${dig_ip}"
        if [[ -n "${dig_ip_info}" ]]; then
            ip_public_country_code=$(echo "${dig_ip_info}" | cut -d\| -f4)
            ip_public_locate="$(echo "${dig_ip_info}" | cut -d\| -f3).$(echo "${dig_ip_info}" | cut -d\| -f2)"
            ip_public_timezone="$(echo "${dig_ip_info}" | cut -d\| -f5)"
            ip_public_as="$(echo "${dig_ip_info}" | awk -F\| '{if($6~/Inc./){$6=gensub(/^(.*Inc.).*/,"\\1","g",$6)};print $6}' 2> /dev/null)"
            ip_public_isp="$(echo "${dig_ip_info}" | awk -F\| '{print $7}')"
        fi

        ip_proxy="${detect_ip}"
        ip_proxy_country_code="${detect_ip_country_code}"
        ip_proxy_locate="${detect_ip_region}.${detect_ip_city}"
        ip_proxy_timezone="${detect_ip_timezone}"
        ip_proxy_as="${detect_ip_as}"
        ip_proxy_isp="${detect_ip_isp}"

    else
        ip_public="${detect_ip}"
        ip_public_country_code="${detect_ip_country_code}"
        ip_public_locate="${detect_ip_region}.${detect_ip_city}"
        ip_public_timezone="${detect_ip_timezone}"
        ip_public_as="${detect_ip_as}"
        ip_public_isp="${detect_ip_isp}"
    fi

    if [[ "${json_output}" -ne 1 ]]; then
        fnBase_CentralOutputTitle 'IP Address Information'
        [[ -n "${ip_local}" && "${ip_local}" != "${ip_public}" ]] && fnBase_OutputControl 'Local IP Addr' "${ip_local}"
        if [[ -n "${ip_public}" ]]; then
            fnBase_OutputControl 'Public IP Addr' "${ip_public} (${ip_public_country_code}.${ip_public_locate})"
            fnBase_OutputControl 'Public IP AS Info' "${ip_public_as}"
            fnBase_OutputControl 'Public IP ISP Info' "${ip_public_isp}"
        fi

        if [[ -n "${ip_proxy}" ]]; then
            fnBase_OutputControl 'Proxy IP Addr' "${ip_proxy} (${ip_proxy_country_code}.${ip_proxy_locate})"
            fnBase_OutputControl 'Proxy IP AS Info' "${ip_proxy_as}"
            fnBase_OutputControl 'Proxy IP ISP Info' "${ip_proxy_isp}"
        fi
    else
        [[ -n "${ip_local}" && "${ip_local}" != "${ip_public}" ]] && output_json=$(fn_OutputJsonConcat 'ip_local' "${ip_local}")
        output_json=$(fn_OutputJsonConcat 'ip_public' "${ip_public}")
        output_json=$(fn_OutputJsonConcat 'ip_public_locate' "${ip_public_locate}")
        output_json=$(fn_OutputJsonConcat 'ip_public_country_code' "${ip_public_country_code}")
        output_json=$(fn_OutputJsonConcat 'ip_public_timezone' "${ip_public_timezone}")
        output_json=$(fn_OutputJsonConcat 'ip_public_as' "${ip_public_as}")
        output_json=$(fn_OutputJsonConcat 'ip_public_isp' "${ip_public_isp}")

        output_json=$(fn_OutputJsonConcat 'ip_proxy' "${ip_proxy}")
        output_json=$(fn_OutputJsonConcat 'ip_proxy_country_code' "${ip_proxy_country_code}")
        output_json=$(fn_OutputJsonConcat 'ip_proxy_locate' "${ip_proxy_locate}")
        output_json=$(fn_OutputJsonConcat 'ip_proxy_timezone' "${ip_proxy_timezone}")
        output_json=$(fn_OutputJsonConcat 'ip_proxy_as' "${ip_proxy_as}")
        output_json=$(fn_OutputJsonConcat 'ip_proxy_isp' "${ip_proxy_isp}")
    fi
}


#########  2.* - System Info Collection  #########
fn_SYSTEMInfo(){
    # system      1, 12, 15, 23, 32
    [[ "${json_output}" -eq 1 ]] && output_json="${output_json}\"system\":{"
    fn_ProductInfo
    fn_DistroOSInfo
    fn_DistroMachineInfo
    [[ "${pack_manager_disable}" -ne 1 ]] && fn_PackageManagerInfo
    [[ "${ip_detection_disable}" -ne 1 && "${internet_connect_status}" -eq 1 ]] && fn_IPInfoDetection
    [[ "${json_output}" -eq 1 ]] && output_json=$(fn_OutputJsonConcat '_e')
}


#########  3.1 - BIOS Info  #########
fn_BIOSInfo(){
    # bios        0, 13
    # biosdecode / dmidecode -t bios / dmidecode -t 0,13
    local bios_vender=${bios_vender:-}    # Vendor
    local bios_version=${bios_version:-}  # Version
    local bios_release_date=${bios_release_date:-}    # Release Date
    local bios_address=${bios_address:-}  # Address
    local bios_runtime_size=${bios_runtime_size:-}    # Runtime Size
    local bios_rom_size=${bios_rom_size:-}    # ROM Size
    local bios_characteristics=${bios_characteristics:-}  # Characteristics
    local bios_revision=${bios_revision:-}    # BIOS Revision
    local bios_language=${bios_language:-}    # BIOS Language
    local bios_language_format=${bios_language_format:-}  # Language Description Format
    local bios_language_installed=${bios_language_installed:-}    # Currently Installed Language

    # 0 --BIOS Information
    bios_vender=$(fn_DmidecodeValExtraction 'bios' 'Vendor' 'bios_vendor' 'bios-vendor')
    bios_version=$(fn_DmidecodeValExtraction 'bios' 'Version' 'bios_version' 'bios-version')
    bios_release_date=$(fn_DmidecodeValExtraction 'bios' 'Release Date' 'bios_date' 'bios-release-date' | date +'%F' -f - 2> /dev/null)
    bios_address=$(fn_DmidecodeValExtraction 'bios' 'Address')
    bios_runtime_size=$(fn_DmidecodeValExtraction 'bios' 'Runtime Size')
    bios_rom_size=$(fn_DmidecodeValExtraction 'bios' 'ROM Size')
    bios_characteristics=$(dmidecode -t bios 2> /dev/null | sed -r -n '/^[[:space:]]*Characteristics:/,/^[[:space:]]*[^:]*:/{/:/d;s@^[[:space:]]*(.*?)[[:space:]]*(is|are).*@\1@g;s@[[:space:]]*$@@g;p}' | sed ':a;N;$!ba;s@\n@, @g')
    bios_revision=$(fn_DmidecodeValExtraction 'bios' 'BIOS Revision')
    # 13 -- BIOS Language Information
    bios_language_format=$(fn_DmidecodeValExtraction 'bios' 'Language Description Format')
    bios_language_installed=$(fn_DmidecodeValExtraction 'bios' 'Currently Installed Language')

    # output
    if [[ "${json_output}" -ne 1 ]]; then
        fnBase_CentralOutputTitle 'BIOS Information'
        fnBase_OutputControl 'Vendor' "${bios_vender}"
        fnBase_OutputControl 'Version' "${bios_version}"
        fnBase_OutputControl 'Release Date' "${bios_release_date}"
        fnBase_OutputControl 'Address' "${bios_address}"
        fnBase_OutputControl 'Runtime Size' "${bios_runtime_size}"
        fnBase_OutputControl 'ROM Size' "${bios_rom_size}"
        fnBase_OutputControl 'BIOS Revision' "${bios_revision}"
        fnBase_OutputControl 'BIOS Language' "${bios_language}"
        fnBase_OutputControl 'Language Description Format' "${bios_language_format}"
        fnBase_OutputControl 'Currently Installed Language' "${bios_language_installed}"
        fnBase_OutputControl 'Characteristics' "${bios_characteristics}"
    else
        output_json="${output_json}\"bios\":{"
        output_json=$(fn_OutputJsonConcat 'bios_vender' "${bios_vender}")
        output_json=$(fn_OutputJsonConcat 'bios_version' "${bios_version}")
        output_json=$(fn_OutputJsonConcat 'bios_release_date' "${bios_release_date}")
        output_json=$(fn_OutputJsonConcat 'bios_address' "${bios_address}")
        output_json=$(fn_OutputJsonConcat 'bios_runtime_size' "${bios_runtime_size}")
        output_json=$(fn_OutputJsonConcat 'bios_revision' "${bios_revision}")
        output_json=$(fn_OutputJsonConcat 'bios_language' "${bios_language}")
        output_json=$(fn_OutputJsonConcat 'bios_language_format' "${bios_language_format}")
        output_json=$(fn_OutputJsonConcat 'bios_language_installed' "${bios_language_installed}")
        output_json=$(fn_OutputJsonConcat 'bios_characteristics' "${bios_characteristics}")
        output_json=$(fn_OutputJsonConcat '_e')
    fi
}


#########  3.2 - Baseboard Info  #########
fn_BASEBOARDInfo(){
    # baseboard   2, 10, 41
    # dmidecode -t baseboard / dmidecode -t 2
    local baseboard_manufacturer=${baseboard_manufacturer:-}    # Manufacturer
    local baseboard_product_name=${baseboard_product_name:-}    # Product Name
    local baseboard_version=${baseboard_version:-}   # Version
    local baseboard_serial_num=${baseboard_serial_num:-}    # Serial Number
    local baseboard_asset_tag=${baseboard_asset_tag:-}  # Asset Tag
    local baseboard_feateures=${baseboard_feateures:-}  # Features
    local baseboard_location_in_chassis=${baseboard_location_in_chassis:-}  # Location In Chassis
    local baseboard_chassis_handle=${baseboard_chassis_handle:-}    # Chassis Handle
    local baseboard_type=${baseboard_type:-}    # Type
    local baseboard_contained_object_handles=${baseboard_contained_object_handles:-}    # Contained Object Handles

    baseboard_manufacturer=$(fn_DmidecodeValExtraction '2' 'Manufacturer' 'board_vendor' 'baseboard-manufacturer')
    baseboard_product_name=$(fn_DmidecodeValExtraction '2' 'Product Name' 'board_name' 'baseboard-product-name')
    baseboard_version=$(fn_DmidecodeValExtraction '2' 'Version' 'board_version' 'baseboard-version')
    baseboard_serial_num=$(fn_DmidecodeValExtraction '2' 'Serial Number' 'board_serial' 'baseboard-serial-number')
    baseboard_asset_tag=$(fn_DmidecodeValExtraction '2' 'Asset Tag' 'board_asset_tag' 'baseboard-asset-tag')
    baseboard_feateures=$(dmidecode -t baseboard 2> /dev/null | sed -r -n '/^[[:space:]]*Features:/,/^[[:space:]]*[^:]*:/{/:/d;s@^[[:space:]]*(.*?)[[:space:]]*(is|are)[[:space:]]*a?[[:space:]]*(.*)@\3@g;s@[[:space:]]*$@@g;p}' | sed ':a;N;$!ba;s@\n@, @g')
    baseboard_location_in_chassis=$(fn_DmidecodeValExtraction '2' 'Location In Chassis')
    baseboard_chassis_handle=$(fn_DmidecodeValExtraction '2' 'Chassis Handle')
    baseboard_type=$(fn_DmidecodeValExtraction '2' 'Type')
    baseboard_contained_object_handles=$(fn_DmidecodeValExtraction '2' 'Contained Object Handles')

    # output
    if [[ "${json_output}" -ne 1 ]]; then
        fnBase_CentralOutputTitle 'Baseboard Information'
        fnBase_OutputControl 'Manufacturer' "${baseboard_manufacturer}"
        fnBase_OutputControl 'Product Name' "${baseboard_product_name}"
        fnBase_OutputControl 'Version' "${baseboard_version}"
        fnBase_OutputControl 'Serial Number' "${baseboard_serial_num}"
        fnBase_OutputControl 'Asset Tag' "${baseboard_asset_tag}"
        fnBase_OutputControl 'Features' "${baseboard_feateures}"
        fnBase_OutputControl 'Location In Chassis' "${baseboard_location_in_chassis}"
        fnBase_OutputControl 'Chassis Handle' "${baseboard_chassis_handle}"
        fnBase_OutputControl 'Type' "${baseboard_type}"
        fnBase_OutputControl 'Contained Object Handles' "${baseboard_contained_object_handles}"
    else
        output_json="${output_json}\"baseboard\":{"
        output_json=$(fn_OutputJsonConcat 'baseboard_manufacturer' "${baseboard_manufacturer}")
        output_json=$(fn_OutputJsonConcat 'baseboard_product_name' "${baseboard_product_name}")
        output_json=$(fn_OutputJsonConcat 'baseboard_version' "${baseboard_version}")
        output_json=$(fn_OutputJsonConcat 'baseboard_serial_num' "${baseboard_serial_num}")
        output_json=$(fn_OutputJsonConcat 'baseboard_asset_tag' "${baseboard_asset_tag}")
        output_json=$(fn_OutputJsonConcat 'baseboard_feateures' "${baseboard_feateures}")
        output_json=$(fn_OutputJsonConcat 'baseboard_location_in_chassis' "${baseboard_location_in_chassis}")
        output_json=$(fn_OutputJsonConcat 'baseboard_chassis_handle' "${baseboard_chassis_handle}")
        output_json=$(fn_OutputJsonConcat 'baseboard_type' "${baseboard_type}")
        output_json=$(fn_OutputJsonConcat 'baseboard_contained_object_handles' "${baseboard_contained_object_handles}")

        # 10 - On Board Device Information
        local onboard_devices_info
        onboard_devices_info=$(dmidecode -t 10 2> /dev/null| sed -r -n '/DMI type/,/^$/{/(DMI type|Device Information)/d;s@^$@---@g;s@^[[:space:]]*@@g;s@^[^:]+:[[:space:]]*(.*)$@\1@g;s@[[:space:]]*$@@g;p}' | sed -r ':a;N;$!ba;s@\n@|@g;s@[|][-]+[|]?@\n@g;' | sed '/^$/d' | awk -F\| '{printf("{\"obd_type\":\"%s\",\"obd_status\":\"%s\",\"obd_desc\":\"%s\"},",$1,$2,$3)}' | sed -r 's@,$@@g;')
        # subjson wrap by []
        [[ -n "${onboard_devices_info}" ]] && output_json=$(fn_OutputJsonConcat 'onboard_device_list' "${onboard_devices_info}" "" '1')

        # 41 - Onboard Devices Extended Information


        # 8 - Port Connector Information
        # dmidecode -t connector / dmidecode -t 8
        # Port Type|External Reference Designator|External Connector Type|Internal Reference Designator|Internal Connector Type
        local port_connector_info=${port_connector_info:-}
        port_connector_info=$(dmidecode -t 8 2> /dev/null | sed -r -n '/DMI type/,/^$/{/(DMI type|Port Connector Information)/d;s@^$@---@g;s@^[[:space:]]*@@g;s@^[^:]+:[[:space:]]*(.*)$@\1@g;s@[[:space:]]*$@@g;p}' | sed -r ':a;N;$!ba;s@\n@|@g;s@[|][-]+[|]?@\n@g;' | sed '/^$/d' | awk -F\| '{printf("{\"port_type\":\"%s\",\"external_reference_designator\":\"%s\",\"external_connector_type\":\"%s\",\"internal_reference_designator\":\"%s\",\"internal_connector_type\":\"%s\"},",$5,$3,$4,$1,$2)}' | sed -r 's@,$@@g;'
        )
        # subjson wrap by []
        [[ -n "${onboard_devices_info}" ]] && output_json=$(fn_OutputJsonConcat 'port_connector_list' "${port_connector_info}" "" '1')

        # 9 - System Slot Information
        # dmidecode -t slot / dmidecode -t 9
        # Designation|Current Usage|Type|Length|ID|Bus Address
        local system_slot_info=${system_slot_info:-}
        system_slot_info=$(dmidecode -t 9 2> /dev/null | sed -r -n '/DMI type/,/^$/{/(DMI type| Information)/d;s@^$@---@g;/^[[:space:]]+[^:]+$/d;/Characteristics:/d;s@^[[:space:]]*@@g;s@^[^:]+:[[:space:]]*(.*)$@\1@g;s@[[:space:]]*$@@g;p}' | sed -r ':a;N;$!ba;s@\n@|@g;s@[|][-]+[|]?@\n@g;' | sed '/^$/d' | awk -F\| '{printf("{\"slot_designation\":\"%s\",\"slot_current_usage\":\"%s\",\"slog_type\":\"%s\",\"slot_type\":\"%s\",\"slot_length\":\"%s\",\"slot_bus_address\":\"%s\"},",$1,$2,$3,$4,$5,$6)}' | sed -r 's@,$@@g;'
        )
        # subjson wrap by []
        [[ -n "${onboard_devices_info}" ]] && output_json=$(fn_OutputJsonConcat 'system_slot_list' "${system_slot_info}" "" '1')

        output_json=$(fn_OutputJsonConcat '_e')
    fi
}


#########  3.3 - Chassis Info  #########
fn_CHASSISInfo(){
    # chassis     3
    # dmidecode -t chassis / dmidecode -t 3
    local chassis_manufacturer=${chassis_manufacturer:-}    # Manufacturer
    local chassis_type=${chassis_type:-}    # Type
    local chassis_version=${chassis_version:-}  # Version
    local chassis_serial_num=${chassis_serial_num:-}    # Serial Number
    local chassis_asset_tag=${chassis_asset_tag:-}  # Asset Tag
    local chassis_lock=${chassis_lock:-}    # Lock
    local chassis_bootup_state=${chassis_bootup_state:-}    # Boot-up State
    local chassis_power_supply_state=${chassis_power_supply_state:-}    # Power Supply State
    local chassis_thermal_state=${chassis_thermal_state:-}  # Thermal State
    local chassis_oem_information=${chassis_oem_information:-}  # OEM Information
    local chassis_height=${chassis_height:-}    # Height
    local chassis_num_of_power_cords=${chassis_num_of_power_cords:-}    # Number Of Power Cords
    local chassis_contianed_elements=${chassis_contianed_elements:-}    # Contained Elements
    local chassis_sku_num=${chassis_sku_num:-}  # SKU Number

    chassis_manufacturer=$(fn_DmidecodeValExtraction 'chassis' 'Manufacturer' 'chassis_vendor' 'chassis-manufacturer')
    # chassis_type=$(fn_DmidecodeValExtraction 'chassis' 'Type' 'chassis_type' 'chassis-type')
    chassis_type=$(fn_DmidecodeValExtraction 'chassis' 'Type')
    chassis_version=$(fn_DmidecodeValExtraction 'chassis' 'Version' 'chassis_version' 'chassis-version')
    chassis_serial_num=$(fn_DmidecodeValExtraction 'chassis' 'Serial Number' 'chassis_serial' 'chassis-serial-number')
    chassis_asset_tag=$(fn_DmidecodeValExtraction 'chassis' 'Asset Tag' 'chassis_asset_tag' 'chassis-asset-tag')
    chassis_lock=$(fn_DmidecodeValExtraction 'chassis' 'Lock')
    chassis_bootup_state=$(fn_DmidecodeValExtraction 'chassis' 'Boot-up State')
    chassis_power_supply_state=$(fn_DmidecodeValExtraction 'chassis' 'Power Supply State')
    chassis_thermal_state=$(fn_DmidecodeValExtraction 'chassis' 'Thermal State')
    chassis_oem_information=$(fn_DmidecodeValExtraction 'chassis' 'OEM Information')
    chassis_height=$(fn_DmidecodeValExtraction 'chassis' 'Height')
    chassis_num_of_power_cords=$(fn_DmidecodeValExtraction 'chassis' 'Number Of Power Cords')
    chassis_contianed_elements=$(fn_DmidecodeValExtraction 'chassis' 'Contained Elements')
    chassis_sku_num=$(fn_DmidecodeValExtraction 'chassis' 'SKU Number')

    # output
    if [[ "${json_output}" -ne 1 ]]; then
        fnBase_CentralOutputTitle 'Chassis Information'
        fnBase_OutputControl 'Manufacturer' "${chassis_manufacturer}"
        fnBase_OutputControl 'Type' "${chassis_type}"
        fnBase_OutputControl 'Version' "${chassis_version}"
        fnBase_OutputControl 'Serial Number' "${chassis_serial_num}"
        fnBase_OutputControl 'Asset Tag' "${chassis_asset_tag}"
        fnBase_OutputControl 'Lock' "${chassis_lock}"
        fnBase_OutputControl 'Boot-up State' "${chassis_bootup_state}"
        fnBase_OutputControl 'Power Supply State' "${chassis_power_supply_state}"
        fnBase_OutputControl 'Thermal State' "${chassis_thermal_state}"
        fnBase_OutputControl 'OEM Information' "${chassis_oem_information}"
        fnBase_OutputControl 'Height' "${chassis_height}"
        fnBase_OutputControl 'Number Of Power Cords' "${chassis_num_of_power_cords}"
        fnBase_OutputControl 'Contained Elements' "${chassis_contianed_elements}"
        fnBase_OutputControl 'SKU Number' "${chassis_sku_num}"
    else
        output_json="${output_json}\"chassis\":{"
        output_json=$(fn_OutputJsonConcat 'chassis_manufacturer' "${chassis_manufacturer}")
        output_json=$(fn_OutputJsonConcat 'chassis_type' "${chassis_type}")
        output_json=$(fn_OutputJsonConcat 'chassis_version' "${chassis_version}")
        output_json=$(fn_OutputJsonConcat 'chassis_serial_num' "${chassis_serial_num}")
        output_json=$(fn_OutputJsonConcat 'chassis_asset_tag' "${chassis_asset_tag}")
        output_json=$(fn_OutputJsonConcat 'chassis_lock' "${chassis_lock}")
        output_json=$(fn_OutputJsonConcat 'chassis_bootup_state' "${chassis_bootup_state}")
        output_json=$(fn_OutputJsonConcat 'chassis_power_supply_state' "${chassis_power_supply_state}")
        output_json=$(fn_OutputJsonConcat 'chassis_thermal_state' "${chassis_thermal_state}")
        output_json=$(fn_OutputJsonConcat 'chassis_oem_information' "${chassis_oem_information}")
        output_json=$(fn_OutputJsonConcat 'chassis_height' "${chassis_height}")
        output_json=$(fn_OutputJsonConcat 'chassis_num_of_power_cords' "${chassis_num_of_power_cords}")
        output_json=$(fn_OutputJsonConcat 'chassis_contianed_elements' "${chassis_contianed_elements}")
        output_json=$(fn_OutputJsonConcat 'chassis_sku_num' "${chassis_sku_num}")
        output_json=$(fn_OutputJsonConcat '_e')
    fi
}


#########  3.4 - Processor Info  #########
fn_PROCESSORInfo(){
    # processor   4
    # cache       7
    # dmidecode -t processor / dmidecode -t 4
    # /sys/devices/system/cpu
    local processor_manufacturer=${processor_manufacturer:-}    # Manufacturer
    local processor_family=${processor_family:-}    # Family
    local processor_version=${processor_version:-}    # Version
    local processors_serial_num=${processors_serial_num:-}    # Serial Number
    local processor_status=${processor_status:-}    # Status
    local processor_arch=${processor_arch:-}    # Architecture
    local processor_byte_order=${processor_byte_order:-}    # Byte Order
    local processos_core_num=${processos_core_num:-}    # Core Count
    local processos_core_enabled_num=${processos_core_enabled_num:-}    # Core Enabled
    local processor_thread_num=${processor_thread_num:-}    # Thread Count
    local processor_thread_per_core=${processor_thread_per_core:-}    # Thread(s) per core
    local processor_frequency=${processor_frequency:-}    # Current Speed
    local processor_frequency_max=${processor_frequency_max:-}    # CPU max MHz
    local processor_frequency_min=${processor_frequency_min:-}    # CPU min MHz
    local processor_bogomips=${processor_bogomips:-}    # BogoMIPS
    local processor_cache_l1d=${processor_cache_l1d:-}    # L1d cache
    local processor_cache_l1i=${processor_cache_l1i:-}    # L1i cache
    local processor_cache_l2=${processor_cache_l2:-}    # L2 cache
    local processor_cache_l3=${processor_cache_l3:-}    # L3 cache
    local processor_virtualization=${processor_virtualization:-}    # Virtualization
    local processor_virtualization_vendor=${processor_virtualization_vendor:-}    # Hypervisor vendor
    local processor_virtualization_type=${processor_virtualization_type:-}    # Virtualization type
    local processor_clflush_size=${processor_clflush_size:-}    # clflush size
    local processor_cache_alignment=${processor_cache_alignment:-}    # Cache Alignment
    local processor_address_size=${processor_address_size:-}    # Address size
    local processor_asset_tag=${processor_asset_tag:-}    # Asset Tag
    local processor_part_num=${processor_part_num:-}    # Part Number
    local processor_flags=${processor_flags:-}    # Flags
    local processor_bugs=${processor_bugs:-}    # Bugs

    processor_manufacturer=$(fn_ProcessorInfoValExtraction 'vendor_id' 'Vendor ID' 'Manufacturer')
    processor_family=$(fn_ProcessorInfoValExtraction 'cpu family' 'CPU family' 'Family' '3')
    processor_version=$(fn_ProcessorInfoValExtraction 'model name' 'Model name' 'Version')
    processors_serial_num=$(fn_ProcessorInfoValExtraction '' '' 'Serial Number' '3')
    processor_status=$(fn_ProcessorInfoValExtraction '' '' 'Status' '3')
    processor_arch=$(fn_ProcessorInfoValExtraction '' 'Architecture' '' '2')
    processor_byte_order=$(fn_ProcessorInfoValExtraction '' 'Byte Order' '' '2')
    processos_core_num=$(fn_ProcessorInfoValExtraction '' 'CPU' 'Core Count' '2')
    processos_core_enabled_num=$(fn_ProcessorInfoValExtraction '' '' 'Core Enabled' '3')
    processor_thread_num=$(fn_ProcessorInfoValExtraction '' '' 'Thread Count' '3')
    processor_thread_per_core=$(fn_ProcessorInfoValExtraction '' 'Thread per core' '' '2')
    processor_frequency=$(fn_ProcessorInfoValExtraction '' '' 'Current Speed' '3')
    processor_frequency_max=$(fn_ProcessorInfoValExtraction '' 'CPU max MHz' '' '2')
    processor_frequency_min=$(fn_ProcessorInfoValExtraction '' 'CPU min MHz' '' '2')
    processor_bogomips=$(fn_ProcessorInfoValExtraction '' 'BogoMIPS' '' '2')
    processor_cache_l1d=$(fn_ProcessorInfoValExtraction '' 'L1d cache' '' '2')
    processor_cache_l1i=$(fn_ProcessorInfoValExtraction '' 'L1i cache' '' '2')
    processor_cache_l2=$(fn_ProcessorInfoValExtraction '' 'L2 cache' '' '2')
    processor_cache_l3=$(fn_ProcessorInfoValExtraction 'cache size' 'L3 cache' '')
    processor_virtualization=$(fn_ProcessorInfoValExtraction '' 'Virtualization' '' '2')
    processor_virtualization_vendor=$(fn_ProcessorInfoValExtraction '' 'Hypervisor vendor' '' '2')
    processor_virtualization_type=$(fn_ProcessorInfoValExtraction '' 'Virtualization type' '' '2')
    processor_clflush_size=$(fn_ProcessorInfoValExtraction 'clflush size' '' '')
    processor_cache_alignment=$(fn_ProcessorInfoValExtraction 'cache_alignment' '' '')
    processor_address_size=$(fn_ProcessorInfoValExtraction 'address sizes' '' '')
    processor_asset_tag=$(fn_ProcessorInfoValExtraction '' '' 'Asset Tag' '3')
    processor_part_num=$(fn_ProcessorInfoValExtraction '' '' 'Part Number' '3')
    processor_flags=$(fn_ProcessorInfoValExtraction 'flags' 'Flags' '' '1')
    processor_bugs=$(fn_ProcessorInfoValExtraction 'bugs' '' '')

    # output
    if [[ "${json_output}" -ne 1 ]]; then
        fnBase_CentralOutputTitle 'CPU Processor Information'
        fnBase_OutputControl 'Manufacturer' "${processor_manufacturer}"
        fnBase_OutputControl 'Family' "${processor_family}"
        fnBase_OutputControl 'Version' "${processor_version}"
        fnBase_OutputControl 'Serial Number' "${processors_serial_num}"
        fnBase_OutputControl 'Status' "${processor_status}"
        fnBase_OutputControl 'Architecture' "${processor_arch}"
        fnBase_OutputControl 'Byte Order' "${processor_byte_order}"
        fnBase_OutputControl 'Core Count' "${processos_core_num}"
        [[ "${processos_core_num}" -ne "${processos_core_enabled_num}" ]] && fnBase_OutputControl 'Core Enabled' "${processos_core_enabled_num}"
        fnBase_OutputControl 'Thread Count' "${processor_thread_num}"
        fnBase_OutputControl 'Thread(s) per core' "${processor_thread_per_core}"
        fnBase_OutputControl 'Current Speed' "${processor_frequency}"
        # fnBase_OutputControl 'CPU max MHz' "${processor_frequency_max}"
        # fnBase_OutputControl 'CPU min MHz' "${processor_frequency_min}"
        fnBase_OutputControl 'BogoMIPS' "${processor_bogomips}"
        fnBase_OutputControl 'L1d cache' "${processor_cache_l1d}"
        fnBase_OutputControl 'L1i cache' "${processor_cache_l1i}"
        fnBase_OutputControl 'L2 cache' "${processor_cache_l2}"
        fnBase_OutputControl 'L3 cache' "${processor_cache_l3}"
        fnBase_OutputControl 'Virtualization' "${processor_virtualization}"
        fnBase_OutputControl 'Hypervisor vendor' "${processor_virtualization_vendor}"
        fnBase_OutputControl 'Virtualization type' "${processor_virtualization_type}"
        fnBase_OutputControl 'clflush size' "${processor_clflush_size}"
        fnBase_OutputControl 'Cache Alignment' "${processor_cache_alignment}"
        fnBase_OutputControl 'Address size' "${processor_address_size}"
        fnBase_OutputControl 'Asset Tag' "${processor_asset_tag}"
        fnBase_OutputControl 'Part Number' "${processor_part_num}"
        fnBase_OutputControl 'Bugs' "${processor_bugs}"
        fnBase_OutputControl 'Flags' "${processor_flags}"
    else
        output_json="${output_json}\"processor\":{"
        output_json=$(fn_OutputJsonConcat 'processor_manufacturer' "${processor_manufacturer}")
        output_json=$(fn_OutputJsonConcat 'processor_family' "${processor_family}")
        output_json=$(fn_OutputJsonConcat 'processor_version' "${processor_version}")
        output_json=$(fn_OutputJsonConcat 'processors_serial_num' "${processors_serial_num}")
        output_json=$(fn_OutputJsonConcat 'processor_status' "${processor_status}")
        output_json=$(fn_OutputJsonConcat 'processor_arch' "${processor_arch}")
        output_json=$(fn_OutputJsonConcat 'processor_byte_order' "${processor_byte_order}")
        output_json=$(fn_OutputJsonConcat 'processos_core_num' "${processos_core_num}")
        output_json=$(fn_OutputJsonConcat 'processos_core_enabled_num' "${processos_core_enabled_num}")
        output_json=$(fn_OutputJsonConcat 'processor_thread_num' "${processor_thread_num}")
        output_json=$(fn_OutputJsonConcat 'processor_thread_per_core' "${processor_thread_per_core}")
        output_json=$(fn_OutputJsonConcat 'processor_frequency' "${processor_frequency}")
        output_json=$(fn_OutputJsonConcat 'processor_frequency_max' "${processor_frequency_max}")
        output_json=$(fn_OutputJsonConcat 'processor_frequency_min' "${processor_frequency_min}")
        output_json=$(fn_OutputJsonConcat 'processor_bogomips' "${processor_bogomips}")
        output_json=$(fn_OutputJsonConcat 'processor_cache_l1d' "${processor_cache_l1d}")
        output_json=$(fn_OutputJsonConcat 'processor_cache_l1i' "${processor_cache_l1i}")
        output_json=$(fn_OutputJsonConcat 'processor_cache_l2' "${processor_cache_l2}")
        output_json=$(fn_OutputJsonConcat 'processor_cache_l3' "${processor_cache_l3}")
        output_json=$(fn_OutputJsonConcat 'processor_virtualization' "${processor_virtualization}")
        output_json=$(fn_OutputJsonConcat 'processor_virtualization_vendor' "${processor_virtualization_vendor}")
        output_json=$(fn_OutputJsonConcat 'processor_virtualization_type' "${processor_virtualization_type}")
        output_json=$(fn_OutputJsonConcat 'processor_clflush_size' "${processor_clflush_size}")
        output_json=$(fn_OutputJsonConcat 'processor_cache_alignment' "${processor_cache_alignment}")
        output_json=$(fn_OutputJsonConcat 'processor_address_size' "${processor_address_size}")
        output_json=$(fn_OutputJsonConcat 'processor_asset_tag' "${processor_asset_tag}")
        output_json=$(fn_OutputJsonConcat 'processor_part_num' "${processor_part_num}")
        output_json=$(fn_OutputJsonConcat 'processor_bugs' "${processor_bugs}")
        output_json=$(fn_OutputJsonConcat 'processor_flags' "${processor_flags}")

        local processor_cache_info
        # Socket Designation|Location|Installed Size|Maximum Size|Speed|Operational Mode|System Type|Associativity|Error Correction Type|Configuration
        processor_cache_info=$(dmidecode -t 7 2> /dev/null | sed -r -n '/DMI type/,/^$/{/(DMI type|Cache Information)/d;/Supported SRAM/,/Installed SRAM/{d};s@^$@---@g;s@^[[:space:]]*@@g;s@^[^:]+:[[:space:]]*(.*)$@\1@g;s@[[:space:]]*$@@g;p}' | sed -r ':a;N;$!ba;s@\n@|@g;s@[|][-]+[|]?@\n@g;' | sed '/^$/d' | awk -F\| 'BEGIN{OFS="|"}{print $1,$4,$5,$6,$7,$3,$9,$10,$8,$2}' | sort -t"|" -k 1,1rn | awk -F\| '{printf("{\"socket_designation\":\"%s\",\"location\":\"%s\",\"installed_size\":\"%s\",\"maximum_size\":\"%s\",\"speed\":\"%s\",\"operational_mode\":\"%s\",\"system_type\":\"%s\",\"associativity\":\"%s\",\"error_correction_type\":\"%s\",\"configuration\":\"%s\"},",$1,$2,$3,$4,$5,$6,$7,$8,$9,$10)}' | sed -r 's@,$@@g;'
        )
        # subjson wrap by []
        [[ -n "${processor_cache_info}" ]] && output_json=$(fn_OutputJsonConcat 'processor_cache_info' "${processor_cache_info}" "" '1')
        output_json=$(fn_OutputJsonConcat '_e')
    fi
}


#########  3.5 - Graphic Info  #########
fn_GRAPHICInfo(){
    sudo lspci -vnn 2> /dev/null | sed -r -n  '/(VGA|3D|2D)/,/^$/{p}'
}

#########  3.5 - Memory Info  #########
fn_MEMORYInfo(){
    # memory      5, 6, 16, 17
    # 16 Physical Memory Array
    # 17 Memory Device

    local memory_location=${memory_location:-}  # Location
    local memory_use=${memory_use:-}    # Use
    local memory_maximum_capacity=${memory_maximum_capacity:-}  # Maximum Capacity
    local memory_devices_num=${memory_devices_num:-}    # Number Of Devices
    local memory_error_correction_type=${memory_error_correction_type:-}    # Error Correction Type
    local memory_error_info_handle=${memory_error_info_handle:-}    # Error Information Handle

    memory_location=$(fn_DmidecodeValExtraction '16' 'Location')
    memory_use=$(fn_DmidecodeValExtraction '16' 'Use')
    memory_maximum_capacity=$(fn_DmidecodeValExtraction '16' 'Maximum Capacity')
    memory_devices_num=$(fn_DmidecodeValExtraction '16' 'Number Of Devices')
    memory_error_correction_type=$(fn_DmidecodeValExtraction '16' 'Error Correction Type')
    memory_error_info_handle=$(fn_DmidecodeValExtraction '16' 'Error Information Handle')

    if [[ "${json_output}" -ne 1 ]]; then
        fnBase_CentralOutputTitle 'Physical Memory Information'
        fnBase_OutputControl 'Location' "${memory_location}"
        fnBase_OutputControl 'Use' "${memory_use}"
        fnBase_OutputControl 'Maximum Capacity' "${memory_maximum_capacity}"
        fnBase_OutputControl 'Number Of Devices' "${memory_devices_num}"
        fnBase_OutputControl 'Error Correction Type' "${memory_error_correction_type}"
        fnBase_OutputControl 'Error Information Handle' "${memory_error_info_handle}"
    else
        output_json="${output_json}\"memory\":{"
        output_json=$(fn_OutputJsonConcat 'memory_location' "${memory_location}")
        output_json=$(fn_OutputJsonConcat 'memory_use' "${memory_use}")
        output_json=$(fn_OutputJsonConcat 'memory_maximum_capacity' "${memory_maximum_capacity}")
        output_json=$(fn_OutputJsonConcat 'memory_devices_num' "${memory_devices_num}")
        output_json=$(fn_OutputJsonConcat 'memory_error_correction_type' "${memory_error_correction_type}")
        output_json=$(fn_OutputJsonConcat 'memory_error_info_handle' "${memory_error_info_handle}")

        # 17 - Memory Device
        # Locator|Set|FormFactor|Type|Size|Speed|TypeDetail|Manufacturer|SerialNumber|Rank|ErrorInformationHandle
        local l_memory_devices_info
        l_memory_devices_info=$(dmidecode -t 17 2> /dev/null | sed -r -n '/DMI type 17/,/^$/{/^[[:space:]]*(Locator|Set|Form Factor|Type|Size|Speed|Type Detail|Manufacturer|Serial Number|Rank|Error Information Handle|^$):?/!d;s@^[[:space:]]*@@g;s@^$@---@g;s@^[^:]+:[[:space:]]*(.*)$@\1@g;s@[[:space:]]*$@@g;p}' | sed -r ':a;N;$!ba;s@\n@|@g;s@[|][-]+[|]?@\n@g;' | sed '/^$/d' | awk -F\| 'BEGIN{OFS="|"}{
            gsub(/^[^_]+_/,"",$5);
            if(match($2,/^[[:digit:]]+/) == 0){$2=""};
            if(match($8,/^[[:digit:]]+/) == 0){$8=""};
            # Synchronous Registered (Buffered)
            if(match($7,/^Synchronous Registered/) == 1){$7="Sync"}else{$7=""};
            # Not Specified
            if(match($9,/^Not Specified/) == 1){$9=""}
            # Not Specified
            if(match($10,/^Not Specified/) == 1){$10=""}
            if(match($11,/^[[:digit:]]+/) == 0){$11=""};
            # Not Provided
            if(match($1,/^Not Provided/) == 1){$1=""};
            print $5,$4,$3,$6,$2,$8,$7,$9,$10,$11,$1
        }' | awk -F\| '{printf("{\"m_locator\":\"%s\",\"m_set\":\"%s\",\"m_form_factor\":\"%s\",\"m_type\":\"%s\",\"m_size\":\"%s\",\"m_speed\":\"%s\",\"m_type_detail\":\"%s\",\"m_manufacturer\":\"%s\",\"m_serial_num\":\"%s\",\"m_rank\":\"%s\",\"m_error_info_handle\":\"%s\"},",$1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11)}' | sed -r 's@,$@@g;'
        )
        # subjson wrap by []
        [[ -n "${l_memory_devices_info}" ]] && output_json=$(fn_OutputJsonConcat 'memory_device_list' "${l_memory_devices_info}" "" '1')

        output_json=$(fn_OutputJsonConcat '_e')
    fi
}


########  3.6 - Disk Info  #########
fn_MegaRAIDInfoDetection(){
    local l_item="${1:-}"
    local l_raw_data="${2:-}"

    local l_val=${l_val:-}
    if [[ -n "${l_item}" && -n "${l_raw_data}" ]]; then
        l_val=$(echo "${l_raw_data}" | sed -r -n '/^'"${l_item}"'[[:space:]]*:/{s@^[^:]+:[[:space:]]*(.*)$@\1@g;p}')
    fi
    echo "${l_val}"
}

fn_DiskRAIDInfo(){
    local l_pci_raid_info
    l_pci_raid_info=$(lspci 2> /dev/null | sed -r -n '/RAID/p')

    local l_megacli_dir=${l_megacli_dir:-'/tmp/MegaRAID/MegaCli'}
    local l_megacli_path=${l_megacli_path:-"${l_megacli_dir}/MegaCli64"}
    local l_megacli_link=${l_megacli_link:-'https://gitlab.com/MaxdSre/axd-ShellScript/raw/master/utilities/MegaCLI/MegaCli-8.07.14-1.noarch.tar.xz'}

    if [[ -n "${l_pci_raid_info}" ]]; then
        # dmesg | grep -i raid
        local l_product_manufacturer
        [[ -s '/sys/devices/virtual/dmi/id/sys_vendor' ]] && l_product_manufacturer=$(cat /sys/devices/virtual/dmi/id/sys_vendor)
        # Dell Inc.
        if [[ "${l_product_manufacturer}" =~ Dell && "${l_pci_raid_info}" =~ MegaRAID ]]; then
            # Detecting Dell service RAID info via utility MegaCli
            # https://docs.broadcom.com/docs-and-downloads/raid-controllers/raid-controllers-common-files/8-07-14_MegaCLI.zip

            # Dell R720 | 03:00.0 RAID bus controller: LSI Logic / Symbios Logic MegaRAID SAS 2008 [Falcon] (rev 03)
            # Dell R730 | 03:00.0 RAID bus controller: LSI Logic / Symbios Logic MegaRAID SAS-3 3108 [Invader] (rev 02)

            if [[ ! -d "${l_megacli_dir}" || ! -x "${l_megacli_path}" ]]; then
                [[ -d "${l_megacli_dir}" ]] && rm -rf "${l_megacli_dir}"
                [[ -d "${l_megacli_dir}" ]] || mkdir -p "${l_megacli_dir}"

                megacli_save_path=${megacli_save_path:-"/tmp/${l_megacli_link##*/}"}
                ${download_method} "${l_megacli_link}" > "${megacli_save_path}"
                tar xf "${megacli_save_path}" -C "${l_megacli_dir}" --strip-components=1
                [[ -s "${l_megacli_dir}"/libstorelibir-2.so.14.07-0 ]] && ln -fs "${l_megacli_dir}"/libstorelibir-2.so.14.07-0 "${l_megacli_dir}"/libstorelibir-2.so 2> /dev/null
                [[ -f "${megacli_save_path}" ]] && rm -f "${megacli_save_path}"

                # mkdir -p /opt/MegaRAID/MegaCli
                # tar xf /tmp/MegaCli-8.07.14-1.noarch.tar.xz -C /opt/MegaRAID/MegaCli --strip-components=1
                # ln -fs /opt/MegaRAID/MegaCli/libstorelibir-2.so.14.07-0 /opt/MegaRAID/MegaCli/libstorelibir-2.so

                # MegaCli not providing all the information we need like mapping to linux devices and raid level (readable), so we are going to use some extra tools.
                # yum install sg3_utils
                # apt-get install sg3-utils
            fi
        fi
    fi

    if [[ -s "${l_megacli_path}" ]]; then
        fnBase_CentralOutputTitle 'RAID Disk Info'

        # ./MegaCli64 -NoLog -V      # MegaCLI SAS RAID Management Tool  Ver 8.07.14 Dec 16, 2013
        # ./MegaCli64 -NoLog -FwTermLog -Dsply -aAll  # Firmware Term Log Information
        # ./MegaCli64 -NoLog -ShowSummary -aAll  # System & Hardware (Controller/BBU/Enclosure/PD) & Storage symmary
        # ./MegaCli64 -NoLog -cfgdsply -aAll    # Raid info, setting,disk info


        # - Adapter information
        # ./MegaCli64 -NoLog -AdpAllInfo -aAll  # Adapter info summary
        # ./MegaCli64 -NoLog -AdpGetTime -aAll  # Adapter dant & time
        # ./MegaCli64 -NoLog -adpCount  # Adapter count

        # - PCI information for Controller
        # ./MegaCli64 -NoLog -AdpGetPciInfo -aAll
        fnBase_CentralOutputTitle 'PCI information for Controller' '1'
        local l_pci_controller
        l_pci_controller=$("${l_megacli_path}" -AdpGetPciInfo -aAll 2> /dev/null)

        fnBase_CentralOutput 'Bus Number ' "$(fn_MegaRAIDInfoDetection 'Bus Number ' "${l_pci_controller}")"
        fnBase_CentralOutput 'Device Number' "$(fn_MegaRAIDInfoDetection 'Device Number' "${l_pci_controller}")"
        fnBase_CentralOutput 'Function Number' "$(fn_MegaRAIDInfoDetection 'Function Number' "${l_pci_controller}")"


        # - BBU(battery backup unit) Information
        # ./MegaCli64 -NoLog -AdpBbuCmd -GetBbuStatus -aAll  # BBU status for Adapter
        # ./MegaCli64 -NoLog -AdpBbuCmd -GetBbuCapacityInfo -aAll  # BBU Capacity Info for Adapter
        # ./MegaCli64 -NoLog -AdpBbuCmd -GetBbuDesignInfo -aAll  # BBU Design Info for Adapter
        # ./MegaCli64 -NoLog -AdpBbuCmd -GetBbuProperties -aAll  # BBU Properties for Adapter
        fnBase_CentralOutputTitle 'BBU(battery backup unit) Information' '1'
        local l_bbu_status_info
        l_bbu_status_info=$("${l_megacli_path}" -AdpBbuCmd -GetBbuStatus -aAll)
        fnBase_CentralOutput 'BatteryType' "$(fn_MegaRAIDInfoDetection 'BatteryType' "${l_bbu_status_info}")"
        fnBase_CentralOutput 'Voltage' "$(fn_MegaRAIDInfoDetection 'Voltage' "${l_bbu_status_info}")"
        fnBase_CentralOutput 'Current' "$(fn_MegaRAIDInfoDetection 'Current' "${l_bbu_status_info}")"
        fnBase_CentralOutput 'Temperature' "$(fn_MegaRAIDInfoDetection 'Temperature' "${l_bbu_status_info}")"
        fnBase_CentralOutput 'Battery State' "$(fn_MegaRAIDInfoDetection 'Battery State' "${l_bbu_status_info}")"
        fnBase_CentralOutput 'Charging Status' "$(fn_MegaRAIDInfoDetection 'Charging Status' "${l_bbu_status_info}")"
        fnBase_CentralOutput 'Charger Status' "$(fn_MegaRAIDInfoDetection 'Charger Status' "${l_bbu_status_info}")"
        fnBase_CentralOutput 'Relative State of Charge' "$(fn_MegaRAIDInfoDetection 'Relative State of Charge' "${l_bbu_status_info}")"
        fnBase_CentralOutput 'Remaining Capacity' "$(fn_MegaRAIDInfoDetection 'Remaining Capacity' "${l_bbu_status_info}")"
        fnBase_CentralOutput 'Full Charge Capacity' "$(fn_MegaRAIDInfoDetection 'Full Charge Capacity' "${l_bbu_status_info}")"
        fnBase_CentralOutput 'isSOHGood' "$(fn_MegaRAIDInfoDetection 'isSOHGood' "${l_bbu_status_info}")"


        # - Virtual Drive Information
        # ./MegaCli64 -NoLog -LDInfo -Lall -a0  # Gather info about Virtual drives
        # ./MegaCli64 -NoLog -LDInfo -Lall -aAll  # Virtual Drive Information
        fnBase_CentralOutputTitle 'Virtual Drives Information' '1'
        local l_virtual_device_info
        l_virtual_device_info=$("${l_megacli_path}" -LDInfo -Lall -a0 2> /dev/null | sed -r -n '/Target Id/,/^$/{/^Default[[:space:]]+/d;p}')

        fnBase_CentralOutput 'Name' "$(fn_MegaRAIDInfoDetection 'Name' "${l_virtual_device_info}")"

        # RAID Level
        # RAID 1 -- Primary-1, Secondary-0, RAID Level Qualifier-0
        # RAID 0 -- Primary-0, Secondary-0, RAID Level Qualifier-0
        # RAID 5 -- Primary-5, Secondary-0, RAID Level Qualifier-3
        # RAID 10 -- Primary-1, Secondary-3, RAID Level Qualifier-0
        fnBase_CentralOutput 'RAID Level' "$(fn_MegaRAIDInfoDetection 'RAID Level' "${l_virtual_device_info}")"
        fnBase_CentralOutput 'Size' "$(fn_MegaRAIDInfoDetection 'Size' "${l_virtual_device_info}")"
        fnBase_CentralOutput 'Sector Size' "$(fn_MegaRAIDInfoDetection 'Sector Size' "${l_virtual_device_info}")"
        fnBase_CentralOutput 'Parity Size' "$(fn_MegaRAIDInfoDetection 'Parity Size' "${l_virtual_device_info}")"
        fnBase_CentralOutput 'State' "$(fn_MegaRAIDInfoDetection 'State' "${l_virtual_device_info}")"
        fnBase_CentralOutput 'Strip Size' "$(fn_MegaRAIDInfoDetection 'Strip Size' "${l_virtual_device_info}")"
        fnBase_CentralOutput 'Number Of Drives' "$(fn_MegaRAIDInfoDetection 'Number Of Drives' "${l_virtual_device_info}")"
        fnBase_CentralOutput 'Span Depth' "$(fn_MegaRAIDInfoDetection 'Span Depth' "${l_virtual_device_info}")"
        fnBase_CentralOutput 'Current Access Policy' "$(fn_MegaRAIDInfoDetection 'Current Access Policy' "${l_virtual_device_info}")"
        fnBase_CentralOutput 'Disk Cache Policy' "$(fn_MegaRAIDInfoDetection 'Disk Cache Policy' "${l_virtual_device_info}")"
        fnBase_CentralOutput 'Bad Blocks Exist' "$(fn_MegaRAIDInfoDetection 'Bad Blocks Exist' "${l_virtual_device_info}")"


        # - Disk Info
        # ./MegaCli64 -NoLog -PDList -aAll       # disk info
        # ./MegaCli64 -NoLog -cfgdsply -aAll    # Raid info, raid setting, disk info
        fnBase_CentralOutputTitle 'Disk Information' '1'

        "${l_megacli_path}" -PDList -aAll | sed -r -n '/Enclosure Device ID/,/S.M.A.R.T alert/{/^$/d;/S.M.A.R.T alert/{s@.*@&\n---@g};s@[[:space:]]*$@@g;/^(Enclosure Device ID)/d;p}' | sed -r ':a;N;$!ba;s@\n@|@g;s@[|][-]+[|]?@\n@g;' | sed '/^$/d' | while read -r line; do
            raw_info=$(echo "${line}" | sed -r -n 's@\|@\n@g;s@'\''s@@g;p')
            fnBase_CentralOutput 'Slot Number' "$(fn_MegaRAIDInfoDetection 'Slot Number' "${raw_info}")"
            fnBase_CentralOutput "Drive's position" "$(fn_MegaRAIDInfoDetection "Drive position" "${raw_info}")"    # Drive's position
            fnBase_CentralOutput 'Enclosure position' "$(fn_MegaRAIDInfoDetection 'Enclosure position' "${raw_info}")"
            fnBase_CentralOutput 'Device Id' "$(fn_MegaRAIDInfoDetection 'Device Id' "${raw_info}")"
            # WWN(World Wide Name)
            fnBase_CentralOutput 'WWN' "$(fn_MegaRAIDInfoDetection 'WWN' "${raw_info}")"
            fnBase_CentralOutput 'Sequence Number' "$(fn_MegaRAIDInfoDetection 'Sequence Number' "${raw_info}")"
            fnBase_CentralOutput 'PD Type' "$(fn_MegaRAIDInfoDetection 'PD Type' "${raw_info}")"
            fnBase_CentralOutput 'Raw Size' "$(fn_MegaRAIDInfoDetection 'Raw Size' "${raw_info}")"
            fnBase_CentralOutput 'Inquiry Data' "$(fn_MegaRAIDInfoDetection 'Inquiry Data' "${raw_info}")"
            fnBase_CentralOutput 'Device Speed' "$(fn_MegaRAIDInfoDetection 'Device Speed' "${raw_info}")"
            fnBase_CentralOutput 'Media Type' "$(fn_MegaRAIDInfoDetection 'Media Type' "${raw_info}")"
            fnBase_CentralOutput 'Drive Temperature' "$(fn_MegaRAIDInfoDetection 'Drive Temperature' "${raw_info}")"
            fnBase_CentralOutput 'S.M.A.R.T alert' "$(fn_MegaRAIDInfoDetection 'Drive has flagged a S.M.A.R.T alert' "${raw_info}")"
            fnBase_CentralOutputTitle
        done
    fi

    [[ -d "${l_megacli_dir}" ]] && rm -rf "${l_megacli_dir}"
}

fn_DiskNonRAIDInfo(){
    # https://techglimpse.com/disk-ssd-hdd-linux-commands/
    local l_disk_details
    l_disk_details=$(mktemp -t "${mktemp_format}")

    # disk|size|disklabel type|disk identifier
    fdisk -l 2>&1 | sed -r -n '/^Disk[[:space:]]*\//,/^$/{s@^Disk[[:space:]]*(\/[^:]+):[[:space:]]*([^,]*).*@\1|\2@g;/^(Units|Sector size|I\/O size)/d;s@^[^:]*:[[:space:]]*(.*)@\1@g;s@^$@---@g;p}' | sed ':a;N;$!ba;s@\n@|@g;s@|---|*@\n@g;' | sed -r -n '/^$/d;/(mapper|ram|loop)/d;p' | while IFS="|" read -r disk_name disk_realsize disk_labeltype disk_identifier; do
        disk_name="${disk_name%/}"
        # Hard Disk Drive (HDD) | Solid State Drive (SSD)
        # cat /sys/block/sda/queue/rotational
        # If the above output is 0 (zero), then the disk is SSD (because SSD does not rotate). You should see output '1' on machines that has HDD disk.
        local l_disk_type=${l_disk_type:-}
        l_disk_type=$(cat /sys/block/"${disk_name##*/}"/queue/rotational 2> /dev/null)
        case "${l_disk_type}" in
            0 ) l_disk_type='SSD' ;;
            1|* ) l_disk_type='HDD' ;;
        esac

        local l_smartctl_info=${l_smartctl_info:-}
        l_smartctl_info=$(smartctl -H -d auto -i "${disk_name}" 2> /dev/null | sed -r -n 's@[[:space:]]is:@:@g;p')
        if [[ -n "${l_smartctl_info}" && -n $(echo "${l_smartctl_info}" | sed -r -n '/START OF INFORMATION SECTION/p') ]]; then
            local l_disk_model_family=${l_disk_model_family:-}
            local l_disk_device_model=${l_disk_device_model:-}
            local l_disk_serial_number=${l_disk_serial_number:-}
            local l_disk_firmware_ver=${l_disk_firmware_ver:-}
            local l_disk_user_capacity=${l_disk_user_capacity:-}
            local l_disk_sector_size=${l_disk_sector_size:-}
            local l_disk_rotation_rate=${l_disk_rotation_rate:-}
            local l_disk_form_factor=${l_disk_form_factor:-}
            local l_disk_ata_ver=${l_disk_ata_ver:-}
            local l_disk_sata_ver=${l_disk_sata_ver:-}
            local l_disk_smart_support=${l_disk_smart_support:-}
            local l_disk_smart_health=${l_disk_smart_health:-}

            l_disk_model_family=$(funcSmartctlDirectiveExtraction "${l_smartctl_info}" 'Model Family')
            l_disk_device_model=$(funcSmartctlDirectiveExtraction "${l_smartctl_info}" 'Device Model')
            l_disk_serial_number=$(funcSmartctlDirectiveExtraction "${l_smartctl_info}" 'Serial Number')
            l_disk_firmware_ver=$(funcSmartctlDirectiveExtraction "${l_smartctl_info}" 'Firmware Version')
            l_disk_user_capacity=$(funcSmartctlDirectiveExtraction "${l_smartctl_info}" 'User Capacity' | sed -r -n 's@^[^\[]*\[([^]]*).*@\1@g;p')
            l_disk_sector_size=$(funcSmartctlDirectiveExtraction "${l_smartctl_info}" 'Sector Sizes')
            l_disk_rotation_rate=$(funcSmartctlDirectiveExtraction "${l_smartctl_info}" 'Rotation Rate')
            l_disk_form_factor=$(funcSmartctlDirectiveExtraction "${l_smartctl_info}" 'Form Factor')
            l_disk_ata_ver=$(funcSmartctlDirectiveExtraction "${l_smartctl_info}" 'ATA Version')
            l_disk_sata_ver=$(funcSmartctlDirectiveExtraction "${l_smartctl_info}" 'SATA Version')
            l_disk_smart_support=$(funcSmartctlDirectiveExtraction "${l_smartctl_info}" 'SMART support' | sed -r -n 's@^([^-]*).*@\1@g;p' | sed ':a;N;$!ba;s@\n@@g')
            l_disk_smart_health=$(echo "${l_smartctl_info}" | sed -r -n '/SMART[[:space:]]*overall-health/{s@^[^:]*:[[:space:]]*(.*)@\L\1@g;p}')

            # Device Name|Disk Type|Disk RealSize|Disk LabelType|DIsk Identifier|Model Family|Device Model|Serial Number|Firmware Version|User Capacity|Sector Sizes|Rotation Rate|Form Factor|ATA Version|SATA Version|SMART support|SMART Health
            echo "${disk_name}|${l_disk_type}|${disk_realsize}|${disk_labeltype}|${disk_identifier}|${l_disk_model_family}|${l_disk_device_model}|${l_disk_serial_number}|${l_disk_firmware_ver}|${l_disk_user_capacity}|${l_disk_sector_size}|${l_disk_rotation_rate}|${l_disk_form_factor}|${l_disk_ata_ver}|${l_disk_sata_ver}|${l_disk_smart_support}|${l_disk_smart_health}" >> "${l_disk_details}"
        else
            # Device Name|Disk Type|Disk RealSize|Disk LabelType|DIsk Identifier
            echo "${disk_name}|${l_disk_type}|${disk_realsize}|${disk_labeltype}|${disk_identifier}" >> "${l_disk_details}"
        fi    # end if l_smartctl_info

    done

    if [[ -s "${l_disk_details}" ]]; then
        if [[ "${json_output}" -ne 1 ]]; then
            while IFS="|" read -r disk_name disk_type disk_realsize disk_labeltype disk_identifier disk_model_family disk_device_model disk_serial_number disk_firmware_ver disk_user_capacity disk_sector_size disk_rotation_rate disk_form_factor disk_ata_ver disk_sata_ver disk_smart_support disk_smart_health; do
                fnBase_CentralOutputTitle "Non-RAID Disk - ${disk_name}" '1'
                fnBase_OutputControl 'Disk Type' "${disk_type}"
                fnBase_OutputControl 'Disk Real Size' "${disk_realsize}"
                fnBase_OutputControl 'Disklabel Type' "${disk_labeltype}"
                fnBase_OutputControl 'Disk Identifier' "${disk_identifier}"
                fnBase_OutputControl 'Model Family' "${disk_model_family}"
                fnBase_OutputControl 'Device Model' "${disk_device_model}"
                fnBase_OutputControl 'Serial Number' "${disk_serial_number}"
                fnBase_OutputControl 'Firmware Version' "${disk_firmware_ver}"
                fnBase_OutputControl 'User Capacity' "${disk_user_capacity}"
                fnBase_OutputControl 'Sector Sizes' "${disk_sector_size}"
                fnBase_OutputControl 'Rotation Rate' "${disk_rotation_rate}"
                fnBase_OutputControl 'Form Factor' "${disk_form_factor}"
                fnBase_OutputControl 'ATA Version' "${disk_ata_ver}"
                fnBase_OutputControl 'SATA Version' "${disk_sata_ver}"
                fnBase_OutputControl 'SMART support' "${disk_smart_support}"
                fnBase_OutputControl 'SMART Health' "${disk_smart_health}"
            done < "${l_disk_details}"
        else
            local l_noraid_disk_info
            l_noraid_disk_info=$(awk -F\| '{printf("{\"d_device_name\":\"%s\",\"d_type\":\"%s\",\"d_realsize\":\"%s\",\"d_labeltype\":\"%s\",\"d_identifier\":\"%s\",\"d_model_family\":\"%s\",\"d_device_model\":\"%s\",\"d_serial_num\":\"%s\",\"d_firmware_ver\":\"%s\",\"d_user_capacity\":\"%s\",\"d_sector_size\":\"%s\",\"d_rotation_rate\":\"%s\",\"d_form_factor\":\"%s\",\"d_ata_ver\":\"%s\",\"d_sata_ver\":\"%s\",\"d_smart_support\":\"%s\",\"d_smart_health\":\"%s\"},",$1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17)}' "${l_disk_details}" | sed -r 's@,$@@g;')

            # subjson wrap by []
            [[ -n "${l_noraid_disk_info}" ]] && output_json=$(fn_OutputJsonConcat 'noraid_disk' "${l_noraid_disk_info}" "" '1')

            output_json=$(fn_OutputJsonConcat '_e')
        fi
    fi
}

fn_DISKInfo(){
    if [[ "${json_output}" -ne 1 ]]; then
        fnBase_CentralOutputTitle "Disk Information"
    else
        output_json="${output_json}\"disk\":{"
    fi

    # - For RAID
    fn_DiskRAIDInfo

    # - For Not-RAID
    fn_DiskNonRAIDInfo

    [[ "${json_output}" -eq 1 ]] && output_json=$(fn_OutputJsonConcat '_e')
}


########  3.7 - Portable Battery Info  #########
fn_BATTERYInfo(){
	# /sys/class/power_supply/BAT0

    # 22 Portable Battery
    # dmidecode -t 22
    local battery_location=${battery_location:-}    # Location
    local battery_manufacturer=${battery_manufacturer:-}    # Manufacturer
    local battery_serial_num=${battery_serial_num:-}    # Serial Number
    local battery_name=${battery_name:-}    # Name
    local battery_chemistry=${battery_chemistry:-}    # Chemistry
    local battery_deigin_capacity=${battery_deigin_capacity:-}    # Design Capacity
    local battery_deigin_voltage=${battery_deigin_voltage:-}    # Design Voltage
    local battery_sbds_ver=${battery_sbds_ver:-}    # SBDS Version
    local battery_maxinum_error=${battery_maxinum_error:-}    # Maximum Error
    local battery_sbds_manufacture_date=${battery_sbds_manufacture_date:-}    # SBDS Manufacture Date
    local battery_oem_specific_info=${battery_oem_specific_info:-}    # OEM-specific Information

	if fnBase_CommandExistIfCheck 'dmidecode'; then
		battery_location=$(fn_DmidecodeValExtraction '22' 'Location')
		battery_manufacturer=$(fn_DmidecodeValExtraction '22' 'Manufacturer')
		battery_serial_num=$(fn_DmidecodeValExtraction '22' 'Serial Number')
		battery_name=$(fn_DmidecodeValExtraction '22' 'Name')
		battery_chemistry=$(fn_DmidecodeValExtraction '22' 'Chemistry')
		battery_deigin_capacity=$(fn_DmidecodeValExtraction '22' 'Design Capacity')
		battery_deigin_voltage=$(fn_DmidecodeValExtraction '22' 'Design Voltage')
		battery_sbds_ver=$(fn_DmidecodeValExtraction '22' 'SBDS Version')
		battery_maxinum_error=$(fn_DmidecodeValExtraction '22' 'Maximum Error')
		battery_sbds_manufacture_date=$(fn_DmidecodeValExtraction '22' 'SBDS Manufacture Date')
		battery_oem_specific_info=$(fn_DmidecodeValExtraction '22' 'OEM-specific Information')
	else
		local l_battery_power_supply_info=''
		l_battery_power_supply_info=$(ls /sys/class/power_supply/*/type | xargs -i{} sh -c "echo -n {}' '; cat < {}" | sed -r -n '/battery$/I{s@^([^[:space:]]+)type.*@\1uevent@p;q}' | xargs -i{} sh -c "cat < {}")
		battery_location=''	
		battery_manufacturer=$(sed -r -n '/^POWER_SUPPLY_MANUFACTURER=/{s@^[^=]+=@@g;p}' <<< "${l_battery_power_supply_info}")
		battery_serial_num=$(sed -r -n '/^POWER_SUPPLY_SERIAL_NUMBER=/{s@^[^=]+=@@g;p}' <<< "${l_battery_power_supply_info}")
		battery_name=$(sed -r -n '/^POWER_SUPPLY_MODEL_NAME=/{s@^[^=]+=@@g;p}' <<< "${l_battery_power_supply_info}")
		battery_chemistry=$(sed -r -n '/^POWER_SUPPLY_TECHNOLOGY=/{s@^[^=]+=@@g;p}' <<< "${l_battery_power_supply_info}")
		battery_deigin_capacity=$(sed -r -n '/^POWER_SUPPLY_ENERGY_FULL_DESIGN=/{s@^[^=]+=@@g;p}' <<< "${l_battery_power_supply_info}")
		battery_deigin_voltage=$(sed -r -n '/^POWER_SUPPLY_VOLTAGE_MIN_DESIGN=/{s@^[^=]+=@@g;p}' <<< "${l_battery_power_supply_info}")
		battery_sbds_ver=''
		battery_maxinum_error=''
		battery_sbds_manufacture_date=''
		battery_oem_specific_info=''
	fi

    # output
    if [[ "${json_output}" -ne 1 ]]; then
        [[ -z "${battery_location}" && -z "${battery_manufacturer}" ]] || fnBase_CentralOutputTitle 'Portable Battery Information'
        fnBase_OutputControl 'Location' "${battery_location}"
        fnBase_OutputControl 'Manufacturer' "${battery_manufacturer}"
        fnBase_OutputControl 'Serial Number' "${battery_serial_num}"
        fnBase_OutputControl 'Name' "${battery_name}"
        fnBase_OutputControl 'Chemistry' "${battery_chemistry}"
        fnBase_OutputControl 'Design Capacity' "${battery_deigin_capacity}"
        fnBase_OutputControl 'Design Voltage' "${battery_deigin_voltage}"
        fnBase_OutputControl 'Maximum Error' "${battery_maxinum_error}"
        fnBase_OutputControl 'SBDS Version' "${battery_sbds_ver}"
        fnBase_OutputControl 'SBDS Manufacture Date' "${battery_sbds_manufacture_date}"
        fnBase_OutputControl 'OEM-specific Information' "${battery_oem_specific_info}"
    else
        output_json="${output_json}\"portable_battery\":{"
        output_json=$(fn_OutputJsonConcat 'battery_location' "${battery_location}")
        output_json=$(fn_OutputJsonConcat 'battery_manufacturer' "${battery_manufacturer}")
        output_json=$(fn_OutputJsonConcat 'battery_serial_num' "${battery_serial_num}")
        output_json=$(fn_OutputJsonConcat 'battery_name' "${battery_name}")
        output_json=$(fn_OutputJsonConcat 'battery_chemistry' "${battery_chemistry}")
        output_json=$(fn_OutputJsonConcat 'battery_deigin_capacity' "${battery_deigin_capacity}")
        output_json=$(fn_OutputJsonConcat 'battery_deigin_voltage' "${battery_deigin_voltage}")
        output_json=$(fn_OutputJsonConcat 'battery_maxinum_error' "${battery_maxinum_error}")
        output_json=$(fn_OutputJsonConcat 'battery_sbds_ver' "${battery_sbds_ver}")
        output_json=$(fn_OutputJsonConcat 'battery_sbds_manufacture_date' "${battery_sbds_manufacture_date}")
        output_json=$(fn_OutputJsonConcat 'battery_oem_specific_info' "${battery_oem_specific_info}")
        output_json=$(fn_OutputJsonConcat '_e')
    fi
}


########  4 - Network Statistics  #########

########  4.1 - TCP Socket Status  #########
fn_TCPSocketInfoTranslation(){
    local l_item="${1:-}"
    local l_output=''

    if [[ -n "${l_item}" ]]; then
        if [[ "${l_item}" =~ : ]]; then
            local l_local_port
            local l_local_ip
            l_local_port=$(printf "%d\n" 0x"${l_item##*:}")
            [[ "${l_local_port}" -eq 0 ]] && l_local_port='*'
            l_local_ip=$(echo "${l_item%%:*}" | sed -r 's@.{2}@0x& @g;s@.*@printf "%d\n%d\n%d\n%d" &@e;' | sed '1!G;h;$!d;s@\n@.@g')
            l_output="${l_local_ip}:${l_local_port}"
        else
            l_output=$(printf "%d\n" 0x"${l_item}")
        fi
    fi

    echo "${l_output}"
}

fn_TCPSOCKETInfo(){
    # /proc/net/tcp   /   ss -tn4 state all  / lsof -iTCP/-iUDP -P -n
    # ss - All standard TCP states: established, syn-sent, syn-recv, fin-wait-1, fin-wait-2, time-wait, closed, close-wait, last-ack, listening and closing.
    # http://lkml.iu.edu/hypermail/linux/kernel/0409.1/2166.html
    # https://stackoverflow.com/questions/13280131/hexadecimal-to-decimal-in-shell-script#13280173
    # https://stackoverflow.com/questions/4614775/converting-hex-to-decimal-in-awk-or-sed#32450316


    # https://stackoverflow.com/questions/5992211/list-of-possible-internal-socket-statuses-from-proc#5992274
    # https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/include/net/tcp_states.h?id=HEAD

    # TCP_ESTABLISHED|established|01
    # TCP_SYN_SENT|syn-sent|02
    # TCP_SYN_RECV|syn-recv|03
    # TCP_FIN_WAIT1|fin-wait-1|04
    # TCP_FIN_WAIT2|fin-wait-2|05
    # TCP_TIME_WAIT|time-wait|06
    # TCP_CLOSE|closed|07
    # TCP_CLOSE_WAIT|close-wait|08
    # TCP_LAST_ACK|last-ack|09
    # TCP_LISTEN|listening|0A
    # TCP_CLOSING|closing|0B
    # TCP_NEW_SYN_RECV|new-syn-recv|0C

    # 0100007F --> 01 00 00 7F --> 7F 00 00 01 -(Hex to Decimal)-> 127.0.0.1

    if [[ "${json_output}" -ne 1 ]]; then
        fnBase_CentralOutputTitle 'TCP Socket Info'

        if fnBase_CommandExistIfCheck 'lsof'; then
            local l_process_details
            l_process_details=$(mktemp -t "${mktemp_format}")
            ps -eo pid,ppid,comm,stat,start,%mem,%cpu,cmd --sort=pid > "${l_process_details}"

            local l_socket_info
            l_socket_info=$(lsof -iTCP -P -n | awk 'BEGIN{OFS="|"}NR>1{gsub(/[\(\)]/,"",$NF);if($9~/->/){gsub(/\*/,"->&",$9);gsub(/->/,"|",$9);}else if($9~/*/){gsub(/.*/,"&|*:*",$9)}else{gsub(/.*/,"&|0.0.0.0:*",$9)}; gsub(/^[^:]*:/,"",$9) ; print $9,$NF,$3,$2}' | while IFS="|" read -r line; do
                # Local Port|Remote Addr|State|Command|PID
                # 51128|127.0.0.1:9090|ESTABLISHED|prometheus|22356
                comm_path=$(awk 'BEGIN{OFS="|"}match($1,/^'"${line##*|}"'$/){cmd_path=gensub(/.* [[:digit:]]{1,2}.[[:digit:]]{1,2} (.*)$/,"\\1","g",$0); print $1,$3,$4,$5,cmd_path}' "${l_process_details}" 2> /dev/null | sed -r 's@.*-Dcatalina.home=([^[:space:]]+).*@\1@g;s@.*-Djetty.home=(.*) -Djetty.base.*@\1@;s@.*QuorumPeerMain (.*)/bin.*@\1@g;s@.*-Dproject.dir=(.*)( .*)?@\1@g;s@.*-Des.path.home=([^[:space:]]+).*@\1@g;;s@^([^[:space:]]+).*@\1@g;s@:$@@g;')
                # pid,comm,stat,start,cmd
                # 22356|prometheus|Ssl|07:55:02|/opt/Prometheus/Server/prometheus
                echo "${line%|*}|${comm_path}"
                #local addr|remote addr|socket state|user|start|stat|pid|cmd path
                # 127.0.0.1:51128|127.0.0.1:9090|ESTABLISHED|prometheus|07:55:02|Ssl|22356|/opt/Prometheus/Server/prometheus
            done
            )
            echo "${l_socket_info}" | sort -t"|" -k1,1n | sed '1i Local Port|Remote Addr|Socket State|User|PID|Comm|State|Start Time|Cmd Path'
             # | column -x -t -s\|
            # State defination see:  man ps | grep 'PROCESS STATE CODES'
        else
            # shellcheck disable=SC2080
            declare -A socket_status_array=( ['01']='ESTABLISHED' ['02']='SYN_SENT' ['03']='SYN_RECV' ['04']='FIN_WAIT1' ['05']='FIN_WAIT2' ['06']='TIME_WAIT' ['07']='CLOSE' ['08']='CLOSE_WAIT' ['09']='LAST_ACK' ['0A']='LISTEN' ['0B']='CLOSING' ['0C']='NEW_SYN_RECV' )

            local l_socket_info
            l_socket_info=$(sed -r -n '/^[[:space:]]*[[:digit:]]+:/{p}' /proc/net/tcp | awk 'BEGIN{OFS="|"}{print $2,$3,$4}' | while IFS="|" read -r local_addr remote_addr socket_status; do
                # Local Address:Port | Peer Address:Port | State
                remote_addr_info=$(fn_TCPSocketInfoTranslation "${remote_addr}")
                # $(funcIPInfoExtraction ${remote_addr_info%%:*})

                echo "$(fn_TCPSocketInfoTranslation "${local_addr}")|${remote_addr_info}|${socket_status_array[${socket_status}]}"
            done
            )

            echo "${l_socket_info}" | sort -t"|" -k1,1 | sed '1i Local Address:Port|Peer Address:Port|State' | column -x -t -s\|
        fi
    fi
}

#########  5 - Executing Process  #########
fn_ExecutingProcess(){
    fn_InitializationCheck

    if [[ "${list_all_type}" -eq 1 ]]; then
        # loop all available
        # echo "${available_type_list}" | sed -r -n 's@\|@\n@g;p' | while read -r line; do func"${line^^}"Info; done

        fn_SYSTEMInfo
        fn_BIOSInfo
        fn_BASEBOARDInfo
        fn_CHASSISInfo
        fn_PROCESSORInfo
        # fn_GRAPHICInfo
        fn_MEMORYInfo
        fn_DISKInfo
        fn_BATTERYInfo
        # fn_TCPSOCKETInfo
    else
        case "${type_specify,,}" in
            p|processor ) type_specify='processor' ;;
            m|memory ) type_specify='memory' ;;
            d|disk ) type_specify='disk' ;;
            bios|baseboard|chassis|battery )
                type_specify="${type_specify,,}"
                ;;
            socket) type_specify='TCPSocket' ;;
            system|s|* ) type_specify='system' ;;
        esac
        fn_"${type_specify^^}"Info
    fi


    # if [[ "${json_output}" -eq 1 && -n "${output_json}" ]]; then
    if [[ "${json_output}" -eq 1 ]]; then
        output_json="${output_json}\"status\":{"
        output_json=$(fn_OutputJsonConcat 'flag' 'ok')
        output_json=$(fn_OutputJsonConcat 'message' 'congratulations')
        output_json=$(fn_OutputJsonConcat '_e')
        output_json="{${output_json%,}}"

        # string compression
        if [[ -n "${encryption_password}" ]]; then
            # https://linuxconfig.org/using-openssl-to-encrypt-messages-and-files-on-linux

            # openssl enc -ciphers
            # -base6 same as -a
            # -A  if the -a option is set then base64 process the data on one line.     base64 -w0
            # -e  encrypt the input data: this is the default.
            # -d  decrypt the input data.
            # -salt   use salt (randomly generated or provide with -S option) when encrypting (this is the default).

            # encryption and compression
            echo "${output_json}" | gzip | openssl enc -aes-256-cbc -e -a -A -salt -pass pass:"${encryption_password}"
            # decryption and decompression
            # openssl enc -aes-256-cbc -d -a -A -salt -pass pass:"${encryption_password}" | gzip -d
        else
            echo "${output_json}"
        fi
    fi
}

fn_ExecutingProcess



#########  6 - EXIT Singal Processing  #########
# trap "commands" EXIT # execute command when exit from shell
fn_TrapEXIT(){
    fnBase_UnsetVars
    unset json_output
    unset ip_detection_disable
    unset pack_manager_disable
    unset encryption_password
    unset proxy_server_specify
    unset output_json
}

trap fn_TrapEXIT EXIT

# Script End
