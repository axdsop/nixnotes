# Toolkits - GNU/Linux

## List

* [GNU/Linux Official Documents Downlaod](./gnuLinuxOfficialDocumentationDownload.sh)
* [GNU/Linux Machine Info Detection](./gnuLinuxMachineInfoDetection.sh)
* [GNU/Linux Life Cycle Info](./gnuLinuxLifeCycleInfo.sh)
* [GNU/Linux FIrewall Rule Configuration](./gnuLinuxFirewallRuleConfiguration.sh)
* [GNU/Linux Post-installation Configuration](./gnuLinuxPostInstallationConfiguration.sh)
* [GNU/Linux GNOME Desktop Configuration](./gnuLinuxGnomeDesktopConfiguration.sh)

<!-- End -->
