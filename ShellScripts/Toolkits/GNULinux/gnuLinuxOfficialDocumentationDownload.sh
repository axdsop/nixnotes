#!/usr/bin/env bash
# shellcheck disable=SC1090,SC2230,SC2030,SC2031,SC2034,SC2154,SC2181
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails

# Official Doc Site
# - https://access.redhat.com/documentation/en-us/
# - https://www.suse.com/documentation/
# - https://doc.opensuse.org
# - https://docs.aws.amazon.com

# Target: Download RedHat/SUSE/OpenSUSE/AWS Official Product Documentations On GNU/Linux
# Developer: MaxdSre

# Change Log:
# - Jan 16, 2023 13:29 Thu ET - fix Red Hat doc opertion (temporary method)
# - Jan 25, 2023 12:30 Wed ET - fix openSUSE, AWS doc operation
# - Oct 29, 2020 15:25 Thu ET - SUSE Documentation page html code update, it uses json file
# - Oct 08, 2020 09:11 Thu ET - initialization check method update
# - Aug 13, 2019 13:32 ~ 15:38 Tue ET - change to new base functions, testing approved
# - Apr 25, 2019 10:19 Thu ET - aws changed xml structure from '<section>' to '<section id="...">'
# - Jan 01, 2019 21:26 Tue ET - code reconfiguration, include base funcions from other file
# - Dec 26, 2018 14:27 Wed ET - optimize AWS file download
# - Dec 17, 2018 21:12 ET - Fix AWS codebuild meta-inf get
# - Nov 20, 2018 13:11 ET - RHEL/AWS Documentation page html code updated
# - Oct 26, 2018 17:20 +0800 - AWS documentation use javascript to generate html code
# - Aug 31, 2018 17:40 +0800 - RHEL Documentation page html code updated
# - Aug 21, 2018 21:20~22:43 +0800 - AWS product_page_url extraction method optimization, add support Chinese version download for AWS
# - Aug 01, 2018 21:29 +0800 - SUSE Documentation page html code
# - Jul 25, 2018 13:38 ET - SUSE Documentation page html code updated
# - Jul 08, 2018 14:01 ET - RHEL Documentation page html code updated, without `allTheData` any more
# - Jun 28, 2018 10:57 ET - AWS Documentation page html code updated
# - May 26, 2018 13:22 Sat ET - code optimization for RHEL 7 without `allTheData`, but sub topic `Linux Containers and Atomic Host` has `allTheData`
# - Apr 30, 2018 09:56 Mon ET - RHEL Documentation page html code updated
# - Apr 24, 2018 13:18 Tue ET - AWS Documentation page html code updated
# - Dec 29, 2017 12:49 Fri +0800 - Change output style
# - Nov 16, 2017 09:22 Thu +0800
# - Oct 17, 2017 16:57 Tue +0800
# - Sep 26, 2017 19:17 Tue +0800
# - Sep 16, 2017 +0800 ~ Sep 20, 2017 16:03 +0800  Refactoring
# - Aug 03, 2017 18:57 Thu +0800


#########  0-1. Singal Setting  #########
trap fnBase_TrapCleanUp INT QUIT

#########  0-2. Variables Setting  #########
# umask 022    # temporarily change umask value to 022
gnulinux_distro_name=${gnulinux_distro_name:-}
language_choose_cn=${language_choose_cn:-0}
category_manual_choose=${category_manual_choose:-0}
file_type=${file_type:-'PDF'}       # PDF, ePub
doc_save_dir=${doc_save_dir:-''}
nautilus_action=${nautilus_action:-1}
proxy_server_specify=${proxy_server_specify:-}
# download fail retry times (count)
max_retry_count=${max_retry_count:-3}


#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | bash -s -- [options] ...

Downloading RedHat/SUSE/OpenSUSE/AWS Official Product Documentations On GNU/Linux.

[available option]
    -h    --help, show help info
    -d distro_name    --specify GNU/Linux distribution name (Red Hat/SUSE/OpenSUSE/AWS)
    -c    --category choose, default download all categories under specific product
    -t file_type    --specify file type (pdf|epub), default is pdf
    -s save_dir    --specify documentation save path (e.g. /tmp), default is ~ or ~/Downloads
    -D    --disable nautilus action (open target directory in new window) if it exist, default is enable
    -p [protocol:]ip:port    --proxy host (http|https|socks4|socks5), default protocol is http
\e[0m"
}

#     -l    --choose Chinese version (just for AWS, default is English)


while getopts "hcd:t:s:Dp:" option "$@"; do
    case "$option" in
        c ) category_manual_choose=1 ;;
        d ) gnulinux_distro_name="$OPTARG" ;;
        # l ) language_choose_cn=1 ;;
        t ) file_type="$OPTARG" ;;
        s ) doc_save_dir="$OPTARG" ;;
        D ) nautilus_action=0 ;;
        p ) proxy_server_specify="$OPTARG" ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done

#########  1-2 Preparation Operation  #########
fn_InitializationCheck(){
    local l_user_codebase_dir="$HOME/.config/axdsop"
    local l_base_function_temp_save_path=''
    local l_need_remove=0
    local l_base_function_url='https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/base_functions.sh'  # custom function perfix fnBase_XXXXXX
    l_base_function_temp_save_path=$(find "${l_user_codebase_dir}" -type f -name "${l_base_function_url##*/}" -print 2> /dev/null)
    
    if [[ ! -f "${l_base_function_temp_save_path}" ]]; then
        local l_download_tool=${l_download_tool:-'wget -qO-'}
        [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_tool='curl -fsL'
        l_need_remove=1
        l_base_function_temp_save_path=$(mktemp -t XXXXXXX)
        ${l_download_tool} "${l_base_function_url}" > "${l_base_function_temp_save_path}"
    fi

    if [[ -f "${l_base_function_temp_save_path}" && -s "${l_base_function_temp_save_path}" ]]; then
        . "${l_base_function_temp_save_path}"
        [[ "${l_need_remove}" -eq 1 ]] && rm "${l_base_function_temp_save_path}"
    else
        echo "Sorry, fail to get base script path."
        exit
    fi

    # tput clear  # Echo the clear-screen sequence for the current terminal.
    # specify 1 means need sudo privilege
    fnBase_RunningEnvironmentCheck '0'

    fnBase_CommandExistCheckPhase 'gawk'
}

fn_GetOptsVariableConfiguration(){
    # - file type
    case "${file_type,,}" in
        pdf|pd|p ) file_type='pdf' ;;
        epub|epu|ep|e ) file_type='epub' ;;
        * ) file_type='pdf' ;;
    esac

    # - documentation save directory
    if [[ -z "${doc_save_dir}" ]]; then
        # default save directory configuration
        if [[ -d "${login_user_home}/Downloads" ]]; then
            doc_save_dir="${login_user_home}/Downloads"
        elif [[ -d "${login_user_home}/Desktop" ]]; then
            doc_save_dir="${login_user_home}/Desktop"
        else
            doc_save_dir="${login_user_home}"
        fi
    else
        # check if begin with / or ~ or not
        if [[ "${doc_save_dir}" =~ ^[^(\/|\~)] ]]; then
            doc_save_dir="${login_user_home}/${doc_save_dir}"
        fi

        # check if create directory successfully or not
        if [[ ! -d "${doc_save_dir}" ]]; then
            mkdir -p "${doc_save_dir}"  2>/dev/null
            chown -R "${login_user}" "${doc_save_dir}"  2>/dev/null
            [[ $? -eq 0 ]] || fnBase_ExitStatement "${c_red}Patient${c_normal}: login user ${c_blue}${login_user}${c_normal} cannot create directory ${c_red}${doc_save_dir}${c_normal} (Permission denied).\nPlease change login user or use utility ${c_red}sudo${c_normal}."
        else
            [[ -w "${doc_save_dir}" ]] || fnBase_ExitStatement "${c_red}Patient${c_normal}: login user ${c_blue}${login_user}${c_normal} has no ${c_red}write${c_normal} permission for ${c_blue}${doc_save_dir}${c_normal}.\nPlease change login user or use utility ${c_red}sudo${c_normal}."
        fi
    fi

    doc_save_dir="${doc_save_dir/%\//}"
}


#########  2-1. GNU/Linux Distribution Choose  #########
fn_LinuxDistributionMenuList(){
    local l_distribution_arr=("Red Hat" "SUSE" "OpenSUSE" "AWS")    # index array

    echo "${c_bold}${c_blue}Available GNU/Linux Distribution List:${c_normal}"
    PS3="${c_yellow}Choose distribution number(e.g. 1, 2,...): ${c_normal}"

    local l_choose_distro_name=${l_choose_distro_name:-}

    select item in "${l_distribution_arr[@]}"; do
        l_choose_distro_name="${item}"
        [[ -n "${l_choose_distro_name}" ]] && break
    done < /dev/tty

    gnulinux_distro_name="${l_choose_distro_name}"
    unset PS3
}

fn_LinuxDistributionSelection(){
    case "${gnulinux_distro_name,,}" in
        'red hat'|redhat|rhel|r* ) gnulinux_distro_name='Red Hat' ;;
        suse|s* ) gnulinux_distro_name='SUSE' ;;
        opensuse|open|o* ) gnulinux_distro_name='OpenSUSE' ;;
        amazon|aws|amzn|a* ) gnulinux_distro_name='AWS' ;;
        * ) fn_LinuxDistributionMenuList ;;
    esac

    printf "\nGNU/Linux distribution you choose is ${c_red}%s${c_normal}.\n\n" "${gnulinux_distro_name}"

    readonly notify_summary="${gnulinux_distro_name} Official Documentation Download"
}


#########  2-2. File Download Procedure  #########
fn_FileDownloadCoreOperation(){
    local l_category_name="${1:-}"
    local l_doc_name="${2:-}"
    local l_doc_url="${3:-}"
    local l_doc_save_name="${4:-}"
    local l_retry_count="${5:-0}"

    [[ -f "${l_doc_save_name}" ]] && rm -f "${l_doc_save_name}"

    if [[ "${download_method}" =~ ^curl ]]; then
        ${download_method} -R "${l_doc_url}" -o "${l_doc_save_name}"
    else
        ${download_method} "${l_doc_url}" > "${l_doc_save_name}"
    fi

    if [[ -s "${l_doc_save_name}" ]]; then
        # shellcheck disable=SC2012
        echo -e "... (${c_yellow}$(ls -hs "${l_doc_save_name}" | cut -d' ' -f 1)${c_normal}) [${c_green}ok${c_normal}]"

    else
        if [[ "${l_retry_count}" -le "${max_retry_count}" ]]; then
            if fnBase_CommandExistIfCheck 'sleep'; then
                sleep 1     # sleep 1 second
            fi

            (( l_retry_count++ )) # let l_retry_count+=1
            # iteration
            fn_FileDownloadCoreOperation "${l_category_name}" "${l_doc_name}" "${l_doc_url}" "${l_doc_save_name}" "${l_retry_count}"
        else
            echo -e "... [${c_red}fail${c_normal}]"

            [[ -f "${l_doc_save_name}" ]] && rm -f "${l_doc_save_name}"
            echo "${l_doc_url}" > "${l_doc_save_name%.*}.txt"
        fi
    fi
}

fn_FileDownloadProcedure(){
    local l_category_name="${1:-}"
    local l_doc_name="${2:-}"
    local l_doc_url="${3:-}"
    local l_doc_save_name="${4:-}"
    local l_retry_count="${5:-0}"

    if [[ -n "${l_category_name}" && -n "${l_doc_name}" && -n "${l_doc_url}" && -n "${l_doc_save_name}" ]]; then
        echo -n -e "${c_blue}${l_category_name}${c_normal} - ${c_yellow}${l_doc_name}${c_normal} ..."

        fn_FileDownloadCoreOperation "${l_category_name}" "${l_doc_name}" "${l_doc_url}" "${l_doc_save_name}"
    fi
}

fn_DocSaveDirectoryStatement(){
    local l_target_dir="${1:-}"
    [[ -z "${l_target_dir}" ]] && l_target_dir="${doc_save_dir}"
    printf "Working directory: ${c_bold}${c_red}%s${c_normal}\n\n" "${l_target_dir}"

    # nautilus - a file manager for GNOME
    # Open File Save Path In Graphical Windows
    if [[ "${nautilus_action}" -eq 1 ]]; then
        fnBase_CommandExistIfCheck 'nautilus' && nautilus "${l_target_dir}" 2> /dev/null &
    fi
}


#########  3-1. Red Hat Distribution Operation  #########
# get specific product documentation page link
# global variables
# - choose_product_topic
# - choose_product
# - choose_version
# - product_content_list

fn_RedHatProductPageLinkExtraction(){
    local l_official_site=${l_official_site:-'https://access.redhat.com'}
    # https://access.redhat.com/documentation/en/ redirect to https://access.redhat.com/products/
    local l_official_doc_site=${l_official_doc_site:-"${l_official_site}/documentation/en-us/"}

    choose_product_topic=${choose_product_topic:-}
    choose_product=${choose_product:-}
    choose_version=${choose_version:-}      # product version may not exists (has no version)
    choose_category=${choose_category:-}    # product category via specify -c

    local l_product_topic_list
    l_product_topic_list=$(mktemp -t "${mktemp_format}")
    #${download_method} "${l_official_doc_site}" | sed -r -n '/class="grid-item"/{s@<div[^>]*>@@g;s@<\/div>@\n\n@g;s@<li>@\n@g;p}' | sed -r -n 's@.*href="([^"]*)">([^<]*)<.*@\2|'"${l_official_site}"'\1@g;s@[[:space:]]*<h4[^>]*>@---\n@;s@<[^>]*>@@g;p' | sed -r -n '1d;/^[[:blank:]]*$/d;s@---@@g;p' > "${l_product_topic_list}"
    # ${download_method} "${l_official_doc_site}" | sed -r -n '/class="grid-item"/,/<\/div>/{/^[[:space:]]*<!--.*?-->$/d;s@<\/div>@\n\n@g;/class="grid-item"|<ul|^[[:space:]]+$/d;s@^[[:space:]]*@@g;/<li>/{s@.*href="([^"]*)">([^<]*)<.*@\2|'"${l_official_site}"'\1@g};/<h4/{s@<[^>]*>@@g};s@<\/ul>@@g;p}' > "${l_product_topic_list}"

    # ☆☆ Note ☆☆: Red Hat use javascript to generate the table dynamicly, I'm still not find a effective way to sovle the extracting issue. (01/26/2023 Thu)
    # https://access.redhat.com/products/
    # manually copy html code into file 'doc_rhel_category.html', range "<div id="#category-accordions" xxxx> ... <\/div>"
    # cat doc_rhel_category.html | sed -r -n 's@^[[:space:]]*@@g;s@[[:space:]]*$@@g;p' | sed ':a;N;$!ba;s@\n@@g' | sed -r -n 's@<\/?pfe-[^>]*>@@g;s@<\/div>@&\n@g;s@<\/h[24][^>]*>@&\n@g;s@<h4>@\n&@g;p' | sed -r -n '/<h2[^>]*>/{s@[[:space:]]*<[^>]+>[[:space:]]*@@g;s@.*@--&@g;p}; /<h4>/,/^$/{/<h4>/{s@[[:space:]]*<[^>]+>[[:space:]]*@@g;s@.*@-&@g;p}; /.*<a[[:space:]]*href=/{/>Discover</Id;s@.*<a[[:space:]]*href="([^"]+)".*$@\1@g;p} }' | sed ':a;N;$!ba;s@\n@|@g' | sed -r -n 's@\|-@\n@g;p' | sed -r -n '/^-+/{s@^-+@@g;s@.*@\n&@g};p' > /tmp/rhel_product_topic_list
    local l_rhel_product_category_list=${l_rhel_product_category_list:-}
    l_rhel_product_category_list='https://gitlab.com/axdsop/nixnotes/raw/master/ShellScripts/Toolkits/_Configs/Source/RHEL_Product_Category_List.txt'
    ${download_method} "${l_rhel_product_category_list}" | sed '/^#/d' > "${l_product_topic_list}"

    # Red Hat Developer
    # Red Hat OpenShift Dev Spaces|https://access.redhat.com/documentation/en-us/red_hat_openshift_dev_spaces|https://access.redhat.com/products/red-hat-openshift-dev-spaces
    #
    # Partner Certification
    # Red Hat Enterprise Linux Hardware Certification|https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux_hardware_certification


    # 1 - Product Topic Lists
    local IFS_BAK=${IFS_BAK:-"$IFS"}  # Backup IFS
    IFS="|" # Setting temporary IFS

    echo "${c_blue}Available Product Topic List:${c_normal}"
    PS3="${c_yellow}Choose product topic number(e.g. 1, 2,...): ${c_normal}"

    select item in $(awk '!match($0,/(\/|^$)/){arr[$0]++}END{PROCINFO["sorted_in"]="@ind_str_asc";for (i in arr) print i}' "${l_product_topic_list}" | sed ':a;N;$!ba;s@\n@|@g'); do
        choose_product_topic="${item}"
        [[ -n "${choose_product_topic}" ]] && break
    done < /dev/tty

    IFS=${IFS_BAK}  # Restore IFS
    unset IFS_BAK
    printf "\nProduct topic you choose is ${c_red}%s${c_normal}.\n\n" "${choose_product_topic}"

    # 2 - Product Lists Under Specific Topic
    IFS_BAK=${IFS_BAK:-"$IFS"}  # Backup IFS
    IFS="|" # Setting temporary IFS

    echo "${c_blue}Available Product List Under ${choose_product_topic}:${c_normal}"
    PS3="${c_yellow}Choose product number(e.g. 1, 2,...): ${c_normal}"

    # sed -r -n '/^'"${choose_product_topic}"'$/,/^$/{/\|/p}' "${l_product_topic_list}" | awk -F\| '{print $1}'
    select item in $(sed -r -n '/^'"${choose_product_topic}"'$/,/^$/{/\|/{s@^([^|]+).*$@\1@g;p}}' "${l_product_topic_list}" | sed ':a;N;$!ba;s@\n@|@g'); do
        choose_product="${item}"
        [[ -n "${choose_product}" ]] && break
    done < /dev/tty

    IFS=${IFS_BAK}  # Restore IFS
    unset IFS_BAK

    printf "\nProduct you choose is ${c_red}%s${c_normal}.\n\n" "${choose_product}"

    # 3 - Product Url
    product_url=${product_url:-}
    product_url=$(sed -r -n '/^'"${choose_product_topic}"'$/,/^'"${choose_product}"'\|/{/^'"${choose_product}"'\|/!d;p}' "${l_product_topic_list}" |  cut -d\| -f2)
    # Red Hat OpenShift Dev Spaces|https://access.redhat.com/documentation/en-us/red_hat_openshift_dev_spaces|https://access.redhat.com/products/red-hat-openshift-dev-spaces

    [[ -f "${l_product_topic_list}" ]] && rm -f "${l_product_topic_list}"

    [[ "${product_url}" =~ ^https?:// ]] || fnBase_ExitStatement "${c_red}Sorry${c_normal}: fail to get url of ${c_red}${choose_product}${c_normal}."

    # 4 - Product Version Lists
    local product_page_html=${product_page_html:-}
    product_page_html=$(mktemp -t "${mktemp_format}")
    product_content_list=$(mktemp -t "${mktemp_format}")

    ${download_method} "${product_url}" > "${product_page_html}"

    # check page html code if contains "allTheData"
    local product_version_list=${product_version_list:-}

    fnSub_ProductContentDetail(){
        local l_input_path="${1:-}"
        local l_output_path="${2:-}"
        local l_group_prefix="${3:-}"

        local title_lists
        # category-list --> list-results-heading
        title_lists=$(sed -r -n '/list-results-heading/{s@><\/label>@&|@g;s@[[:space:]]*<[^>]*>[[:space:]]*@@g;s@\|$@@g;s@\|@\n@g;s@&@\\&@g;p}' "${l_input_path}")

        local product_list_raw_info=${product_list_raw_info:-}
        # product_list_raw_info=$(sed -r -n '/pane-product-doclist/,/pane-search-facet-product-version/{/^$/d; /list-results-group/{s@[[:space:]]*<[^>]*>[[:space:]]*@@g;s@.*@group|&@g;p}; /list-result-excerpt/{s@[[:space:]]*<[^>]*>[[:space:]]*@@g;s@.*@excerpt|&@g;p}; /list-result-title/,/<\/a>/{/<\/a>/{s@[[:space:]]*(.*)@title|\1@g;s@[[:space:]]*<[^>]*>[[:space:]]*@@g;p}}; /role="menu"/,/<\/ul>/{/href=/{s@.*href="([^"]+)".*@https://access.redhat.com\1@g;p}};}' "${l_input_path}")

        # list-results-group     -->  group name
        # list-results-heading   -->  category name
        # list-result-title      -->  title
        # list-result-excerpt    -->  short description

        product_list_raw_info=$(sed -r -n '/list-results-group/,/<\/main>/{/^$/d; /list-results-heading/{s@[[:space:]]*<[^>]*>[[:space:]]*@@g;s@.*@group|&@g;p}; /list-result-excerpt/,/<\/[^>]*>/{s@[[:space:]]*<[^>]*>[[:space:]]*@@g;/^$/d;s@[[:space:]]{2,}@@g;;s@.*@excerpt|&@g;p}; /list-result-title/{/<\/a>/{s@[[:space:]]*(.*)@title|\1@g;s@[[:space:]]*<[^>]*>[[:space:]]*@@g;p}}; /role="menu"/,/<\/ul>/{/href=/{s@.*href="([^"]+)".*@https://access.redhat.com\1@g;p}};}' "${l_input_path}")


        item_list_info=$(
            for (( i = 1; i <= $(echo "${title_lists}" | wc -l); i++ )); do
                category_start=${category_start:-}
                category_start=$(echo "${title_lists}" | sed -n ''"${i}"'p')
                category_end=${category_end:-}
                category_end=$(echo "${title_lists}" | sed -n ''"$((i+1))"'p')
                echo "${product_list_raw_info}" | sed -r -n '/^group\|'"${category_start}"'/,/^group\|'"${category_end}"'/{/^group\|/d;/excerpt/{s@.*@&\ngroup|'"${category_start}"'\n---@g;};p}'
            done
        )

        echo "${item_list_info}" | sed ':a;N;$!ba;s@\n@#@g;s@\#---\#*@\n@g;' | sed '/^$/d' | while read -r line; do
            item_info=$(echo "${line}" | sed -r -n 's@#@\n@g;p')
            group_name=$(echo "${item_info}" | sed -r -n '/^group\|/{s@^.*\|(.*)@\1@g;p}')
            # Linux Containers and Atomic Host
            [[ -n "${l_group_prefix}" ]] && group_name="${l_group_prefix} - ${group_name}"
            title_name=$(echo "${item_info}" | sed -r -n '/^title\|/{s@^.*\|(.*)@\1@g;p}')
            excerpt_name=$(echo "${item_info}" | sed -r -n '/^excerpt\|/{s@^.*\|(.*)@\1@g;p}')

            html_single=$(echo "${item_info}" | sed -r -n '/\/html-single\//{p}')
            html=$(echo "${item_info}" | sed -r -n '/\/html\//{p}')
            pdf=$(echo "${item_info}" | sed -r -n '/\/pdf\//{p}')
            epub=$(echo "${item_info}" | sed -r -n '/\/epub\//{p}')

            [[ -n "${title_name}" && -n "${excerpt_name}" ]] && echo "${choose_version}|${group_name}|${title_name}|${excerpt_name}|${html}|${html_single}|${pdf}|${epub}" >> "${l_output_path}"
        done
    }

    fnSub_AllTheDataExtract(){
        local l_input_path="${1:-}"
        local l_output_path="${2:-}"

        sed -r -n '/allTheData/{s@^[^=]*=[[:space:]]*@@g;s@<[^>]*>@@g;s@},@}\n\n@g;p}' "${l_input_path}" | sed -n '/"category"/!d;s@:null@:"null"@g;s@\\/@/@g;s@[[:space:]]*"[[:space:]]*@"@g;p' | awk -v official_site_url="${l_official_site}" 'BEGIN{OFS="|"}{
            if (match($0,/"category"/)) {
                category=gensub(/.*"category":"([^"]*)".*/,"\\1","g",$0);
            } else {category=""};
            if (match($0,/"link"/)) {
                link=gensub(/.*"link":"([^"]*)".*/,"\\1","g",$0);
                if(link!~/^https:\/\//){link=official_site_url""link}
            } else {link=""};
            if (match($0,/"title"/)) {
                title=gensub(/.*"title":"([^"]*)".*/,"\\1","g",$0);
            } else {title=""};
            if (match($0,/"description"/)) {
                description=gensub(/.*"description":"([^"]*)".*/,"\\1","g",$0);
            } else {description=""};
            if (match($0,/"version"/)) {
                version=gensub(/.*"version":"([^"]*)".*/,"\\1","g",$0);
            } else {version=""};
            if (match($0,/"Single-page"/)) {
                single_page=gensub(/.*"Single-page":"([^"]*)".*/,"\\1","g",$0);
                if(single_page!~/^https:\/\//){single_page=official_site_url""single_page}
            } else {single_page=""};
            if (match($0,/"PDF"/)) {
                pdf=gensub(/.*"PDF":"([^"]*)".*/,"\\1","g",$0); if(pdf!~/^https:\/\//){pdf=official_site_url""pdf}
            } else {pdf=""};
            if (match($0,/"ePub"/)) {
                epub=gensub(/.*"ePub":"([^"]*)".*/,"\\1","g",$0); if(epub!~/^https:\/\//){epub=official_site_url""epub}
            } else {epub=""};
            print version,category,title,description,link,single_page,pdf,epub
        }' > "${l_output_path}"
    }

    if [[ -n $(sed -r -n '/allTheData/p' "${product_page_html}") ]]; then
        fnSub_AllTheDataExtract "${product_page_html}" "${product_content_list}"
    else
        product_version_list=$(sed -r -n '/version-list/,/<\/ul>/{/value=/!d;s@^[^>]*>([^<]+).*value="([^"]+)".*@\1|'"${l_official_site}"'\2@g;p}' "${product_page_html}")
        echo "${product_version_list}" > "${product_content_list}"
        # 9|https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9
        # 8|https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8
        ## 8.0 Beta|https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8-beta
        # 7|https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7
    fi

    local version_list=${version_list:-}
    # awk -F\| '{arr[$1]++}END{PROCINFO["sorted_in"]="@ind_num_desc";for (i in arr) print i}'
    # sed -r -n 's@^([^\|]+).*$@\1@g;p'
    version_list=$(cut -d\| -f1 "${product_content_list}" | sort -rn)

    if [[ -n "${version_list}" ]]; then
        local IFS_BAK=${IFS_BAK:-"$IFS"}  # Backup IFS
        IFS="|" # Setting temporary IFS

        echo "${c_blue}Available Version List Of ${choose_product}:${c_normal}"
        PS3="${c_yellow}Choose product version number(e.g. 1, 2,...): ${c_normal}"

        select item in $(echo "${version_list}" | sort -t. -k 1,1rn -k 2,2rn -k 3,3rn -k 4,4rn -k 5,5rn | sed ':a;N;$!ba;s@\n@|@g'); do
            choose_version="${item}"
            [[ -n "${choose_version}" ]] && break
        done < /dev/tty

        IFS=${IFS_BAK}  # Restore IFS
        unset IFS_BAK

        printf "\nProduct version you choose is ${c_red}%s${c_normal}.\n\n" "${choose_version}"
    fi

    # manually extract product topic list while whitout 'allTheData'
    if [[ -n "${product_version_list}" ]]; then
        local product_version_url=${product_version_url:-}
        product_version_url=$(echo "${product_version_list}" | sed -r -n '/^'"${choose_version}"'\|/{s@^[^\|]*\|(.*)@\1@g;p}')
        # /documentation/en-us/red_hat_enterprise_linux/7
        [[ "${product_version_url}" =~ ^https?:// ]] || product_version_url="${l_official_site}/${product_version_url}"

        local product_version_html=${product_version_html:-}
        product_version_html=$(mktemp -t "${mktemp_format}")
        ${download_method} "${product_version_url}" > "${product_version_html}"

        # empty file contents
        : > "${product_content_list}"

        # Linux Atomic Host, Just for RHEL 7 Only
        if [[ -n $(sed -r -n '/linux_atomic_host/{p}' "${product_version_html}") ]]; then
            local linux_atomic_host_link=${linux_atomic_host_link:-}
            linux_atomic_host_link=$(sed -r -n '/linux_atomic_host/{s@.*href="([^"]+)".*@'"${l_official_site}"'\1@g;p}' "${product_page_html}")

            local linux_atomic_host_html=${linux_atomic_host_html:-}
            linux_atomic_host_html=$(mktemp -t "${mktemp_format}")
            ${download_method} "${linux_atomic_host_link}" > "${linux_atomic_host_html}"

            if [[ -n $(sed -r -n '/allTheData/p' "${linux_atomic_host_html}") ]]; then
                fnSub_AllTheDataExtract "${linux_atomic_host_html}" "${product_content_list}"
                # Linux Containers and Atomic Host
                sed -r -i 's@^([^\|]+\|)(.*)@\1Container - \2@g;' "${product_content_list}"
            else
                fnSub_ProductContentDetail "${linux_atomic_host_html}" "${product_content_list}" 'Container'
            fi
        fi

        fnSub_ProductContentDetail "${product_version_html}" "${product_content_list}"
    fi

    # 5 - Category Lists Under Specific Product
    if [[ "${category_manual_choose}" -eq 1 ]]; then
        local category_list=${category_list:-}
        category_list=$(awk -F\| 'match($1,/^'"${choose_version}"'$/){arr[$2]++}END{PROCINFO["sorted_in"]="@ind_str_asc";for (i in arr) print i}' "${product_content_list}")

        if [[ -n "${category_list}" ]]; then
            local IFS_BAK=${IFS_BAK:-"$IFS"}  # Backup IFS
            IFS="|" # Setting temporary IFS

            echo "${c_blue}Available Category List Of ${choose_product} ${choose_version}:${c_normal}"
            PS3="${c_yellow}Choose product category number(e.g. 1, 2,...): ${c_normal}"

            select item in $(echo "${category_list}" | sed ':a;N;$!ba;s@\n@|@g'); do
                choose_category="${item}"
                [[ -n "${choose_category}" ]] && break
            done < /dev/tty

            IFS=${IFS_BAK}  # Restore IFS
            unset IFS_BAK

            printf "\nProduct category you choose is ${c_red}%s${c_normal}.\n\n" "${choose_category}"
        fi
    fi

    fnBase_NotifySendStatement "${notify_summary}" "${choose_product_topic} | ${choose_product} ${choose_version} ${choose_category}"
}


fn_RedHatOperation(){
    fn_RedHatProductPageLinkExtraction

    # - Download Procedure
    local category_dir=${category_dir:-}
    category_dir="${doc_save_dir}/Red Hat"

    if [[ -z "${choose_version}" ]]; then
        category_dir="${category_dir}/${choose_product_topic}/${choose_product}"
    elif [[ -n $(echo "${choose_version}" | sed -r -n '/^[[:digit:]]+.?[[:digit:]]*$/p') ]]; then
        category_dir="${category_dir}/${choose_product_topic}/${choose_product}/${choose_version}"
    else
        category_dir="${category_dir}/${choose_product_topic}/${choose_product} - ${choose_version}"
    fi

    fn_DocSaveDirectoryStatement "${category_dir}"

    local counter_flag=${counter_flag:-1}
    local category_flag=${category_flag:-}
    local file_url=${file_url:-}

    awk -F\| 'match($1,/^'"${choose_version}"'$/)&&match($2,/'"${choose_category}"'/){arr[$0]=$2}END{PROCINFO["sorted_in"]="@val_str_asc";for (i in arr) print i}' "${product_content_list}" | sort -b -f -t"|" -k 1,1 -k 2,2 -k 3r,3 | while IFS="|" read -r version category title description link single_page pdf_url epub_url; do

        case "${file_type,,}" in
            pdf ) file_url="${pdf_url}" ;;
            epub ) file_url="${epub_url}" ;;
        esac

        if [[ -z "${category}" ]]; then
            sub_category_dir="${category_dir}"
            category='NULL'
        else
            sub_category_dir="${category_dir}/${category}"
        fi    # end if

        [[ -d "${sub_category_dir}" ]] || mkdir -p "${sub_category_dir}"

        if [[ -z "${category_flag}" ]]; then
            category_flag="${sub_category_dir}"
        elif [[ -n "${category_flag}" && "${category_flag}" != "${sub_category_dir}" ]]; then
            counter_flag=1
            category_flag="${sub_category_dir}"
        fi

        if [[ -n "${file_url}" && "${file_url}" =~ ${file_type,,}$ ]]; then
            doc_save_name="${sub_category_dir}/${counter_flag} - ${title//\//&}.${file_url##*.}"
            [[ -f "${doc_save_name}" ]] && rm -f "${doc_save_name}"
            fn_FileDownloadProcedure "${category}" "${title}" "${file_url}" "${doc_save_name}"
            (( counter_flag++ )) # let counter_flag+=1
        fi
    done

    # remove empty dir
    [[ -d "${category_dir}" ]] && find "${category_dir}" -type d -empty -exec ls -d {} \; | while IFS="" read -r line; do [[ -d "${line}" ]]&& rm -rf "${line}"; done

    [[ -f "${product_content_list}" ]] && rm -f "${product_content_list}"
}


#########  3-2. SUSE Distribution Operation  #########
# https://documentation.suse.com
# https://documentation.suse.com/docserv/data/product.json

fn_SUSEConcatenateCompleteURL(){
    local origin_product_url="${1:-}"   # reference url
    local target_url="${2:-}"   # url need to be concatenated

    if [[ "${target_url}" =~ ^https?:// ]]; then
        target_url="${target_url}"
    elif [[ "${target_url}" =~ \.\./ ]]; then
        # https://www.suse.com/documentation/
        target_url="${origin_product_url%/*}/${target_url/#\.\.\//}"
    elif [[ -n "${target_url}" ]]; then
        # https://www.suse.com/documentation/sles-12
        target_url="${origin_product_url}/${target_url}"
    fi

    echo "${target_url}"
}

# global variables
# - choose_product
# - choose_version
# - product_full_name
# - doc_info_list
fn_SUSEProductPageLinkExtraction(){
    local l_official_site=${l_official_site:-'https://www.suse.com'}
    # Product Documentation   https://www.suse.com/documentation/ --> https://documentation.suse.com
    local l_official_doc_site=${l_official_doc_site:-"https://documentation.suse.com"}

    choose_product=${choose_product:-}
    choose_version=${choose_version:-}
    product_full_name=${product_full_name:-}
    choose_category=${choose_category:-}

    local l_official_doc_html=${l_official_doc_html:-}
    l_official_doc_html=$(mktemp -t "${mktemp_format}")

    local l_product_json_url=${l_product_json_url:-'https://documentation.suse.com/docserv/data/product.json'}

    if fnBase_CommandExistIfCheck 'python3'; then
        l_python_name='python3'
    elif fnBase_CommandExistIfCheck 'python'; then
        l_python_name='python'
    fi

    ${download_method} "${l_product_json_url}" | ${l_python_name} -c $'import json,sys\nobj=json.load(sys.stdin)\nproductline_array = obj["productline"]\nproduct_array = obj["product"]\nfor item in productline_array:\n    item_array=product_array[item]\n    for item in item_array:\n        name=item_array[item]["name"]\n        version=item_array[item]["version"]\n        print("{}|{}|https://documentation.suse.com/{}/|https://documentation.suse.com/docserv/data/{}/setdata.json".format(name,version,item,item))\n' 2> /dev/null 1> "${l_official_doc_html}"
    # SUSE Linux Enterprise Server|15 GA|https://documentation.suse.com/sles/15-GA/|https://documentation.suse.com/docserv/data/sles/15-GA/setdata.json

    # 1 - Product Lists
    local IFS_BAK=${IFS_BAK:-"$IFS"}  # Backup IFS
    IFS="|" # Setting temporary IFS

    echo "${c_blue}Available Product List:${c_normal}"
    PS3="${c_yellow}Choose product number(e.g. 1, 2,...): ${c_normal}"

    select item in $(awk -F\| '{gsub(/[[:space:]]+$/,"",$1);gsub(/^[[:space:]]+/,"",$1);arr[$1]++}END{PROCINFO["sorted_in"]="@ind_str_desc";for (i in arr) print i}' "${l_official_doc_html}" | sed ':a;N;$!ba;s@\n@|@g'); do
        choose_product="${item}"
        [[ -n "${choose_product}" ]] && break
    done < /dev/tty
    IFS=${IFS_BAK}  # Restore IFS
    unset IFS_BAK

    printf "\nProduct you choose is ${c_red}%s${c_normal}.\n\n" "${choose_product}"

    # 2 - Version Lists Under Specific Product
    local IFS_BAK=${IFS_BAK:-"$IFS"}  # Backup IFS
    IFS="|" # Setting temporary IFS

    echo "${c_blue}Available Version List:${c_normal}"
    PS3="${c_yellow}Choose version number(e.g. 1, 2,...): ${c_normal}"

    # sort -t. -k 1,1rn -k 2,2rn -k 3,3rn -k 4,4rn -k 5,5rn
    select item in $(awk -F\| 'match($1,/^'"${choose_product}"'$/){print $2}' "${l_official_doc_html}" | sort -t' ' -k 1,1r -k 2,2r -k 3,3r -k 4,4r -k 5,5r | sed ':a;N;$!ba;s@\n@|@g'); do
        choose_version="${item}"
        [[ -n "${choose_version}" ]] && break
    done < /dev/tty

    IFS=${IFS_BAK}  # Restore IFS
    unset IFS_BAK

    printf "\nProduct version you choose is ${c_red}%s${c_normal}.\n\n" "${choose_version}"

    # 3 - Product Url
    # SUSE Linux Enterprise Server|15 GA|https://documentation.suse.com/sles/15-GA/|https://documentation.suse.com/docserv/data/sles/15-GA/setdata.json
    local specific_product_doc_url=${specific_product_doc_url:-}
    specific_product_doc_url=$(awk -F\| 'match($1,/^'"${choose_product}"'$/)&&match($2,/^'"${choose_version}"'$/){print $3}' "${l_official_doc_html}")
    local specific_product_dataset_url=${specific_product_dataset_url:-}
    specific_product_dataset_url=$(awk -F\| 'match($1,/^'"${choose_product}"'$/)&&match($2,/^'"${choose_version}"'$/){print $NF}' "${l_official_doc_html}")

    doc_info_list=${doc_info_list:-}
    doc_info_list=$(mktemp -t "${mktemp_format}")

    if [[ -n "${specific_product_dataset_url}" ]]; then
        # https://documentation.suse.com/docserv/data/sles/15-GA/setdata.json
        ${download_method} "${specific_product_dataset_url}" | ${l_python_name} -c $'import json,sys\nobj=json.load(sys.stdin)\nfor category in obj["category"]:\n    category_title = category["title"][0]["title"]\n    for i in category["document"]:\n        for item in i:\n            if item["lang"] == "en-us":\n                title = item["title"]\n                fotmat = item["format"]\n                html = fotmat["html"] if "html" in fotmat else ""\n                single_html = fotmat["single-html"] if "single-html" in fotmat else ""\n                pdf = fotmat["pdf"] if "pdf" in fotmat else ""\n                epub = fotmat["epub"] if "epub" in fotmat else ""\n                print("{}|{}|{}|{}|{}|{}".format(category_title,title,html,single_html,pdf,epub))\n            else:\n                continue\n' 2> /dev/null 1> "${doc_info_list}"
    fi

    # 4 - Category Lists Under Specific Product
    if [[ "${category_manual_choose}" -eq 1 ]]; then
        local IFS_BAK=${IFS_BAK:-"$IFS"}  # Backup IFS
        IFS="|" # Setting temporary IFS

        echo "${c_blue}Available category List:${c_normal}"
        PS3="${c_yellow}Choose category number(e.g. 1, 2,...): ${c_normal}"

        select item in $(awk -F\| '{arr[$1]++}END{PROCINFO["sorted_in"]="@ind_str_asc";for (i in arr) print i}' "${doc_info_list}" | sed ':a;N;$!ba;s@\n@|@g'); do
            choose_category="${item}"
            [[ -n "${choose_category}" ]] && break
        done < /dev/tty

        IFS=${IFS_BAK}  # Restore IFS
        unset IFS_BAK

        printf "\nProduct category you choose is ${c_red}%s${c_normal}.\n\n" "${choose_category}"
    fi

    fnBase_NotifySendStatement "${notify_summary}" "${choose_product} | ${choose_version} ${choose_category}"

    [[ -f "${l_official_doc_html}" ]] && rm -f "${l_official_doc_html}"
}

fn_SUSEOperation(){
    fn_SUSEProductPageLinkExtraction

    # - Download Procedure
    local category_dir=${category_dir:-}

    if [[ -z "${choose_version}" || "${choose_version}" == 'null' ]]; then
        category_dir="${doc_save_dir}/${choose_product}"
    elif [[ -n "${choose_version}" ]]; then
        category_dir="${doc_save_dir}/${choose_product}/${choose_version}"
    fi

    fn_DocSaveDirectoryStatement "${category_dir}"

    local counter_flag=${counter_flag:-1}
    local category_flag=${category_flag:-}
    local file_url=${file_url:-}

    # sort -b -f -t"|" -k 1,1 -k 2r,2 "${doc_info_list}"
    awk -F\| 'match($1,/'"${choose_category}"'/){print}' "${doc_info_list}" | sort -b -f -t"|" -k 1,1 -k 2r,2 | while IFS="|" read -r category topic html_url single_html_url pdf_url epub_url; do
        # *_url need to add prefix 'https://documentation.suse.com/'

        case "${file_type,,}" in
            pdf ) file_url="${pdf_url}" ;;
            epub ) file_url="${epub_url}" ;;
        esac

        local l_official_doc_site=${l_official_doc_site:-"https://documentation.suse.com"}
        [[ "${file_url}" =~ ^https?:// ]] || file_url="${l_official_doc_site}/${file_url}"

        sub_category_dir="${category_dir}/${category}"

        if [[ ! -d "${sub_category_dir}" ]]; then
            counter_flag=1
            mkdir -p "${sub_category_dir}"
        fi

        if [[ -z "${category_flag}" ]]; then
            category_flag="${sub_category_dir}"
        elif [[ -n "${category_flag}" && "${category_flag}" != "${sub_category_dir}" ]]; then
            counter_flag=1
            category_flag="${sub_category_dir}"
        fi

        if [[ -n "${file_url}" && "${file_url}" =~ ${file_type,,}$ && -d "${sub_category_dir}" ]]; then
            doc_save_name="${sub_category_dir}/${counter_flag} - ${topic//\//&}.${file_url##*.}"
            [[ -f "${doc_save_name}" ]] && rm -f "${doc_save_name}"
            fn_FileDownloadProcedure "${category}" "${topic}" "${file_url}" "${doc_save_name}"
            (( counter_flag++ )) # let counter_flag+=1
        fi
    done

    # remove empty dir
    [[ -d "${category_dir}" ]] && find "${category_dir}" -type d -empty -exec ls -d {} \; | while IFS="" read -r line; do [[ -d "${line}" ]]&& rm -rf "${line}"; done

    [[ -f "${doc_info_list}" ]] && rm -f "${doc_info_list}"
}


#########  3-3. OpenSUSE Distribution Operation  #########
# global variables
# - choose_version
# - doc_info_list
fn_OpenSUSEProductPageLinkExtraction(){
    local l_official_site=${l_official_site:-'https://www.opensuse.org/'}
    local l_official_doc_site=${l_official_doc_site:-'https://doc.opensuse.org'}

    local official_doc_html=${official_doc_html:-}
    official_doc_html=$(mktemp -t "${mktemp_format}")

    # save documentation info for all available release version
    doc_info_list=$(mktemp -t "${mktemp_format}")
    # release version
    choose_version=${choose_version:-}

    # 1 - Current Available Releases
    ${download_method} "${l_official_doc_site}" | sed -r 's@^[[:space:]]+@@g; /^$/d' | sed ':a;N;$!ba;s@\n@@g' | sed -r -n 's@<\/[^>]+>@&\n@g;p' > "${official_doc_html}"

    sed -r -n '/<section[^>]+>/,/<\/section>/{/<h2>/{s@[[:space:]]*<[^>]*>[[:space:]]*@@g;s@.*@---&@g;p}; /<h5[^>]*>/,/<\/div>/{/<h5[^>]*>/{s@[[:space:]]*<[^>]*>[[:space:]]*@@g;s@.*@--&@g;p}; />?(HTML|single HTML|PDF|EPUB)<?/I{s@.*href="([^"]*)".*@\1@g;p}} }' "${official_doc_html}" | sed ':a;N;$!ba;s@\n@|@g' | sed -r -n 's@^-+@-@g;s@\|--@\n@g;p' | awk 'BEGIN{OFS="|"}{if($0~/^-+/){a=gensub(/^-*/,"","g",$0)}else{print a,$0}}' > "${doc_info_list}"

    # release version | title | page | single page | pdf | epub
    # openSUSE Leap 15.4|Startup Guide|https://doc.opensuse.org/documentation/leap/startup/html/book-startup/index.html|https://doc.opensuse.org/documentation/leap/startup/single-html/book-startup/index.html|https://doc.opensuse.org/documentation/leap/startup/book-startup_color_en.pdf|https://doc.opensuse.org/documentation/leap/startup/book-startup_en.epub
    # openSUSE Leap 42.3|Startup Guide|https://doc.opensuse.org/documentation/leap/startup/html/book.opensuse.startup/index.html|https://doc.opensuse.org/documentation/leap/startup/single-html/book.opensuse.startup/index.html|https://doc.opensuse.org/documentation/leap/startup/book.opensuse.startup_color_en.pdf|https://doc.opensuse.org/documentation/leap/startup/book.opensuse.startup_en.epub

    # 2 - Previous openSUSE Documentation Archives
    # https://doc.opensuse.org/opensuse.html --> https://doc.opensuse.org/archive/
    local previous_release_url=${previous_release_url:-}
    previous_release_url=$(sed -r -n '/Archive versions/{s@.*href="([^"]*)".*@'"${l_official_doc_site}"'\1@g;p}' "${official_doc_html}")

    local previous_official_doc_html=${previous_official_doc_html:-}
    previous_official_doc_html=$(mktemp -t "${mktemp_format}")
    ${download_method} "${previous_release_url}" > "${previous_official_doc_html}"

    sed -r 's@^[[:space:]]+@@g; /^$/d' "${previous_official_doc_html}" | sed ':a;N;$!ba;s@\n@@g' | sed -r -n 's@<\/[^>]+>@&\n@g;p' | sed -r -n '/<section[^>]+>/,/<\/section>/{/<h2>/{s@[[:space:]]*<[^>]*>[[:space:]]*@@g;s@.*@---&@g;p}; /<h5[^>]*>/,/<\/div>/{/<h5[^>]*>/{s@[[:space:]]*<[^>]*>[[:space:]]*@@g;s@.*@--&@g;p}; />?(HTML|single HTML|PDF|EPUB)<?/I{s@.*href="([^"]*)".*@\1@g;p}} }' | sed ':a;N;$!ba;s@\n@|@g' | sed -r -n 's@^-+@-@g;s@\|--@\n@g;p' | awk 'BEGIN{OFS="|"}{if($0~/^-+/){a=gensub(/^-*/,"","g",$0)}else{print a,$0}}' >> "${doc_info_list}"     # append

    [[ -f "${official_doc_html}" ]] && rm -f "${official_doc_html}"
    [[ -f "${previous_official_doc_html}" ]] && rm -f "${previous_official_doc_html}"

    IFS_BAK=${IFS_BAK:-"$IFS"}  # Backup IFS
    IFS="|" # Setting temporary IFS

    echo "${c_blue}Available Release Version List:${c_normal}"
    PS3="${c_yellow}Choose version number(e.g. 1, 2,...): ${c_normal}"

    # awk -F\| '{arr[$1]++}END{PROCINFO["sorted_in"]="@ind_str_desc";for (i in arr) print i}' "${doc_info_list}"

    select item in $(awk -F\| '{print $1}' "${doc_info_list}" | awk '!a[$0]++' | sed ':a;N;$!ba;s@\n@|@g'); do
        choose_version="${item}"
        [[ -n "${choose_version}" ]] && break
    done < /dev/tty

    IFS=${IFS_BAK}  # Restore IFS
    unset IFS_BAK

    printf "\nRelease version you choose is ${c_red}%s${c_normal}.\n\n" "${choose_version}"

    fnBase_NotifySendStatement "${notify_summary}" "${choose_version}"
}

fn_OpenSUSEOperation(){
    fn_OpenSUSEProductPageLinkExtraction

    local category_dir="${doc_save_dir}/${choose_version}"
    [[ -d "${category_dir}" ]] || mkdir -p "${category_dir}"

    fn_DocSaveDirectoryStatement "${category_dir}"

    local counter_flag=${counter_flag:-1}
    local category_flag=${category_flag:-}
    local file_url=${file_url:-}

    # sort -b -f -t"|" -k 1,1 -k 2,2
    awk -F\| 'BEGIN{OFS="|"}match($1,/^'"${choose_version}"'$/){print}' "${doc_info_list}" | while IFS="|" read -r release_version title page_url single_page_url pdf_url epub_url; do
        case "${file_type,,}" in
            pdf ) file_url="${pdf_url}" ;;
            epub ) file_url="${epub_url}" ;;
        esac

        if [[ -z "${category_flag}" ]]; then
            category_flag="${category_dir}"
        elif [[ -n "${category_flag}" && "${category_flag}" != "${category_dir}" ]]; then
            counter_flag=1
            category_flag="${category_dir}"
        fi    # end if

        if [[ -n "${file_url}" && "${file_url}" =~ ${file_type,,}$ ]]; then
            doc_save_name="${category_dir}/${counter_flag}-${title//\//&}.${file_url##*.}"
            [[ -f "${doc_save_name}" ]] && rm -f "${doc_save_name}"
            fn_FileDownloadProcedure "${choose_version}" "${title}" "${file_url}" "${doc_save_name}"
            (( counter_flag++ )) # let counter_flag+=1
        fi
    done

    # remove empty dir
    [[ -d "${category_dir}" ]] && find "${category_dir}" -type d -empty -exec ls -d {} \; | while IFS="" read -r line; do [[ -d "${line}" ]]&& rm -rf "${line}"; done

    [[ -f "${doc_info_list}" ]] && rm -f "${doc_info_list}"
}



#########  3-4. Amazon Web Services (AWS)  #########
# get specific product documentation page link
# global variables
# - choose_product_topic
# - choose_product
# - product_page_url
fn_AWSProductPageLinkExtraction_Old(){
    local l_official_site=${l_official_site:-'https://aws.amazon.com'}
    # https://www.amazonaws.cn/documentation/
    # local l_official_site=${l_official_site:-'https://amazonaws-china.com/cn'}

    [[ "${language_choose_cn}" -eq 1 ]] && l_official_site='https://www.amazonaws.cn'

    local l_official_doc_site=${l_official_doc_site:-"${l_official_site}/documentation/"}
    choose_product_topic=${choose_product_topic:-}
    choose_product=${choose_product:-}
    product_page_url=${product_page_url:-}

    local product_topic_list
    product_topic_list=$(mktemp -t "${mktemp_format}")

    if [[ "${language_choose_cn}" -eq 1 ]]; then
        ${download_method} "${l_official_doc_site}" | sed -r -n '/class="columnbuilder[^"]*">/,/id="aws-page-footer"/{/<\/(p|a)>/!d;s@^[[:blank:]]*@@g;s@.*href="([^"]+)"[^>]*>[[:space:]]*([^<]+).*$@\2|\1@g;s@<h3[^>]*>@---&@g;s@[[:space:]]*<[^>]*>[[:space:]]*@@g;s@&nbsp;@ @g;s@&amp;@\&@g;p}' | sed -r -n 's@---@\n@g;/^[[:space:]]*\|/d;/^[[:space:]]+$/d;p' > "${product_topic_list}"
    else
		# - old version
        #${download_method} "${l_official_doc_site}" | sed -r -n '/id="Guides_and_API_References"/,/id="Tutorials_and_Projects"/{/<\/(p|a)>/!d;s@^[[:blank:]]*@@g;s@.*href="([^"]+)"[[:space:]]*>[[:space:]]*([^<]+).*$@\2|\1@g;s@<p>@---&@g;s@[[:space:]]*<[^>]*>[[:space:]]*@@g;s@&nbsp;@ @g;s@&amp;@\&@g;p}' | sed 's@---@\n@g' > "${product_topic_list}"

        # - 2018.10.26 start
        # html content is generated by javascript file，date source comes from file 'main-landing-page.xml'
        # https://aws.amazon.com/documentation/ redirect to https://docs.aws.amazon.com/index.html#lang/en_us

        l_official_doc_site='https://docs.aws.amazon.com/en_us/main-landing-page.xml'
        ${download_method} "${l_official_doc_site}" | sed -r -n '/<service-categories/,/<\/service-categories/{/<tiles>/,/<\/tiles>/{/<tile[^>]*>/,/<\/tile>/{p}}}'| sed -r -n '/<tile[^>]*>/,/<\/tile>/{/<title>/{s@<title>@@g;p};/services/,/<\/services>/{/<service href=/,/<\/service>/{/<service href=/{s@.*href="([^"]+)">@\1@g};/<(prefix|name)>/{s@[[:space:]]*<[^>]+>[[:space:]]*@@g};p}}}' | sed ':a;N;$!ba;s@\n@|@g;s@[[:space:]]*<\/service>@\n@g' | sed -r -n 's@^\|?[[:space:]]*(.*)<\/title>(.*)$@\n\1\n\2@g;s@[[:space:]]*<prefix\/>[[:space:]]*@@g;s@^[[:space:]]*\|@@g;s@&nbsp;@ @g;s@&amp;@\&@g;p' | sed -r -n 's@^\|@@g;p' | awk -F\| '{if ($0~/\|/) {if($2!=""){printf("%s %s|%s\n",$2,$3,$1)}else{printf("%s|%s\n",$3,$1)}} else {print}}' > "${product_topic_list}"

        # Compute
        # Amazon EC2|/ec2/?id=docs_gateway
        # ...
        # Storage
        # Amazon S3|/s3/?id=docs_gateway
        # ...

        # After chooseing specific product (e.g: EC2), the html page content is also generated by javascript file, list data source comes from https://docs.aws.amazon.com/ec2/en_us/landing-page.xml, but per download link need to by download additionaly, for 'User Guide for Linux Instances', json link is https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/meta-inf/guide-info.json
        # ~~As every doc need to download it own json file, the HTTP requests are too frequent in a short time, so I decide not to update this scipt this time.~~  make it work at Nov 20, 2018 Tue 16:12
        # - 2018.10.26 end
    fi

    # ---Database
    # Amazon DynamoDB|/documentation/dynamodb/?id=docs_gateway
    # Amazon ElastiCache|/documentation/elasticache/?id=docs_gateway
    # Amazon Neptune|/documentation/neptune/?id=docs_gateway
    # Amazon RDS|/documentation/rds/?id=docs_gateway
    # Amazon Redshift|/documentation/redshift/?id=docs_gateway

    # 1 - Product Topic Lists
    local IFS_BAK=${IFS_BAK:-"$IFS"}  # Backup IFS
    IFS="|" # Setting temporary IFS

    echo "${c_blue}Available Product Topic List:${c_normal}"
    PS3="${c_yellow}Choose product topic number(e.g. 1, 2,...): ${c_normal}"

    select item in $(awk '!match($0,/(\/|^$)/){arr[$0]++}END{PROCINFO["sorted_in"]="@ind_str_asc";for (i in arr) print i}' "${product_topic_list}" | sed ':a;N;$!ba;s@\n@|@g'); do
        choose_product_topic="${item}"
        [[ -n "${choose_product_topic}" ]] && break
    done < /dev/tty

    IFS=${IFS_BAK}  # Restore IFS
    unset IFS_BAK
    printf "\nProduct topic you choose is ${c_red}%s${c_normal}.\n\n" "${choose_product_topic}"

    # 2 - Product Lists Under Specific Topic
    IFS_BAK=${IFS_BAK:-"$IFS"}  # Backup IFS
    IFS="|" # Setting temporary IFS

    echo "${c_blue}Available Product List Under ${choose_product_topic}:${c_normal}"
    PS3="${c_yellow}Choose product number(e.g. 1, 2,...): ${c_normal}"

    select item in $(sed -r -n '/^'"${choose_product_topic}"'$/,/^$/{/\|/{s@^[[:space:]]*([^\|]+)\|.*$@\1@g;p}}' "${product_topic_list}" | sed ':a;N;$!ba;s@\n@|@g'); do
        choose_product="${item}"
        [[ -n "${choose_product}" ]] && break
    done < /dev/tty

    IFS=${IFS_BAK}  # Restore IFS
    unset IFS_BAK

    printf "\nProduct you choose is ${c_red}%s${c_normal}.\n\n" "${choose_product}"

    fnBase_NotifySendStatement "${notify_summary}" "${choose_product_topic} | ${choose_product}"

    product_page_url=$(sed -r -n '/^'"${choose_product_topic}"'$/,/^$/{/^'"${choose_product}"'\|/{s@^[^\|]+\|(.*)$@\1@g;p}}' "${product_topic_list}")
    if [[ -n "${product_page_url}" && "${product_page_url}" =~ ^/ ]]; then
        if [[ "${product_page_url}" =~ ^/cn/ ]]; then
            product_page_url="${l_official_site%\/cn*}${product_page_url}"
        else
            product_page_url="${l_official_site}${product_page_url}"
        fi
    fi
    # [[ -n "${product_page_url}" && "${product_page_url}" =~ ^/ ]] && product_page_url="${l_official_site}${product_page_url}"

    product_short_name=${product_short_name:-}
    product_short_name=$(sed -r -n '/^'"${choose_product}"'\|/{s@[^\/]+\/([^\/]+).*$@\1@g;p}' "${product_topic_list}")

    [[ -f "${product_topic_list}" ]] && rm -f "${product_topic_list}"
}

fn_AWSProductPageLinkExtraction(){
    local l_official_site=${l_official_site:-'https://aws.amazon.com'}
    # https://www.amazonaws.cn/documentation/
    # local l_official_site=${l_official_site:-'https://amazonaws-china.com/cn'}

    [[ "${language_choose_cn}" -eq 1 ]] && l_official_site='https://www.amazonaws.cn'

    local l_official_doc_site=${l_official_doc_site:-"${l_official_site}/documentation/"}
    choose_product_topic=${choose_product_topic:-}
    choose_product=${choose_product:-}
    product_page_url=${product_page_url:-}

    local product_topic_list
    product_topic_list=$(mktemp -t "${mktemp_format}")

    if [[ "${language_choose_cn}" -eq 1 ]]; then
        ${download_method} "${l_official_doc_site}" | sed -r -n '/class="columnbuilder[^"]*">/,/id="aws-page-footer"/{/<\/(p|a)>/!d;s@^[[:blank:]]*@@g;s@.*href="([^"]+)"[^>]*>[[:space:]]*([^<]+).*$@\2|\1@g;s@<h3[^>]*>@---&@g;s@[[:space:]]*<[^>]*>[[:space:]]*@@g;s@&nbsp;@ @g;s@&amp;@\&@g;p}' | sed -r -n 's@---@\n@g;/^[[:space:]]*\|/d;/^[[:space:]]+$/d;p' > "${product_topic_list}"
    else
        # https://docs.aws.amazon.com/index.html
        # html content is generated by javascript file，date source comes from file 'main-landing-page.xml'

        # - 2018.10.26
        # https://aws.amazon.com/documentation/ redirect to https://docs.aws.amazon.com/index.html#lang/en_us
        # - 2023.01.25
        # https://aws.amazon.com/documentation/ redirect to https://aws.amazon.com/documentation-overview/

        l_official_doc_site='https://docs.aws.amazon.com'

        l_official_doc_data='https://docs.aws.amazon.com/en_us/main-landing-page.xml'
        # ${download_method} "${l_official_doc_data}" | sed -r -n '/<service-categories/,/<\/service-categories/{/<tiles>/,/<\/tiles>/{/<tile[^>]*>/,/<\/tile>/{p}}}'| sed -r -n '/<tile[^>]*>/,/<\/tile>/{/<title>/{s@<title>@@g;p};/services/,/<\/services>/{/<service href=/,/<\/service>/{/<service href=/{s@.*href="([^"]+)">@\1@g};/<(prefix|name)>/{s@[[:space:]]*<[^>]+>[[:space:]]*@@g};p}}}' | sed ':a;N;$!ba;s@\n@|@g;s@[[:space:]]*<\/service>@\n@g' | sed -r -n 's@^\|?[[:space:]]*(.*)<\/title>(.*)$@\n\1\n\2@g;s@[[:space:]]*<prefix\/>[[:space:]]*@@g;s@^[[:space:]]*\|@@g;s@&nbsp;@ @g;s@&amp;@\&@g;p' | sed -r -n 's@^\|@@g;p' | awk -F\| '{if ($0~/\|/) {if($2!=""){printf("%s %s|%s\n",$2,$3,$1)}else{printf("%s|%s\n",$3,$1)}} else {print}}' > "${product_topic_list}"

        ${download_method} "${l_official_doc_data}" 2> /dev/null | sed -r -n '/id="user_guides"/,/<\/section>/{/<list-card>/,/<\/list-card>/{p}}' | sed -r -n '/<title[^>]*>/{s@[[:space:]]*<[^>]*>[[:space:]]*@@g;s@.*@---&@g;p}; /<service[^>]*>/,/<\/service>/{/<service.*?href=/{s@.*href="([^"]+)"[^>]*>.*@\1@g;s@.*@--&@g;p}; /<(prefix|name)>/{s@[[:space:]]*<[^>]*>[[:space:]]*@@g;p}; }' | sed ':a;N;$!ba;s@\n@|@g' | sed -r -n 's@\|--@\n@g;s@&nbsp;@ @g;s@&amp;@\&@g;p' | sed -r -n 's@^-+@\n@g;p' | awk -F\| '{if ($0~/\|/) {printf("%s %s|%s\n",$2,$3,$1)}else {print}}' > "${product_topic_list}"


        # Compute
        # Amazon EC2|/ec2/?id=docs_gateway
        # ...
        # Storage
        # Amazon S3|/s3/?id=docs_gateway
        # ...

        # After chooseing specific product (e.g: EC2), the html page content is also generated by javascript file, list data source comes from https://docs.aws.amazon.com/ec2/en_us/landing-page.xml, but per download link need to by download additionaly, for 'User Guide for Linux Instances', json link is https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/meta-inf/guide-info.json
        # ~~As every doc need to download it own json file, the HTTP requests are too frequent in a short time, so I decide not to update this scipt this time.~~  make it work at Nov 20, 2018 Tue 16:12
        # - 2018.10.26 end
    fi

    # ---Database
    # Amazon DynamoDB|/documentation/dynamodb/?id=docs_gateway
    # Amazon ElastiCache|/documentation/elasticache/?id=docs_gateway
    # Amazon Neptune|/documentation/neptune/?id=docs_gateway
    # Amazon RDS|/documentation/rds/?id=docs_gateway
    # Amazon Redshift|/documentation/redshift/?id=docs_gateway

    # 1 - Product Topic Lists
    local IFS_BAK=${IFS_BAK:-"$IFS"}  # Backup IFS
    IFS="|" # Setting temporary IFS

    echo "${c_blue}Available Product Topic List:${c_normal}"
    PS3="${c_yellow}Choose product topic number(e.g. 1, 2,...): ${c_normal}"

    select item in $(awk '!match($0,/(\/|^$)/){arr[$0]++}END{PROCINFO["sorted_in"]="@ind_str_asc";for (i in arr) print i}' "${product_topic_list}" | sed ':a;N;$!ba;s@\n@|@g'); do
        choose_product_topic="${item}"
        [[ -n "${choose_product_topic}" ]] && break
    done < /dev/tty

    IFS=${IFS_BAK}  # Restore IFS
    unset IFS_BAK
    printf "\nProduct topic you choose is ${c_red}%s${c_normal}.\n\n" "${choose_product_topic}"

    # 2 - Product Lists Under Specific Topic
    IFS_BAK=${IFS_BAK:-"$IFS"}  # Backup IFS
    IFS="|" # Setting temporary IFS

    echo "${c_blue}Available Product List Under ${choose_product_topic}:${c_normal}"
    PS3="${c_yellow}Choose product number(e.g. 1, 2,...): ${c_normal}"

    select item in $(sed -r -n '/^'"${choose_product_topic}"'$/,/^$/{/\|/{s@^[[:space:]]*([^\|]+)\|.*$@\1@g;p}}' "${product_topic_list}" | sed ':a;N;$!ba;s@\n@|@g'); do
        choose_product="${item}"
        [[ -n "${choose_product}" ]] && break
    done < /dev/tty

    IFS=${IFS_BAK}  # Restore IFS
    unset IFS_BAK

    printf "\nProduct you choose is ${c_red}%s${c_normal}.\n\n" "${choose_product}"

    fnBase_NotifySendStatement "${notify_summary}" "${choose_product_topic} | ${choose_product}"

    product_page_url=$(sed -r -n '/^'"${choose_product_topic}"'$/,/^$/{/^'"${choose_product}"'\|/{s@^[^\|]+\|(.*)$@\1@g;p}}' "${product_topic_list}")
    if [[ -n "${product_page_url}" && "${product_page_url}" =~ ^/ ]]; then
        if [[ "${product_page_url}" =~ ^/cn/ ]]; then
            product_page_url="${l_official_site%\/cn*}${product_page_url}"
        else
            product_page_url="${l_official_doc_site}${product_page_url}"
        fi
    fi

    # [[ -n "${product_page_url}" && "${product_page_url}" =~ ^/ ]] && product_page_url="${l_official_site}${product_page_url}"

    product_short_name=${product_short_name:-}
    # product_short_name=$(sed -r -n '/^'"${choose_product}"'\|/{s@[^\/]+\/([^\/]+).*$@\1@g;p}' "${product_topic_list}")
    product_short_name=$(sed -r -n '/^'"${choose_product_topic}"'$/,/^$/{/^'"${choose_product}"'\|/{s@[^\/]+\/([^\/]+).*$@\1@g;p}}' "${product_topic_list}")

    [[ -f "${product_topic_list}" ]] && rm -f "${product_topic_list}"
}

fn_AWSOperation(){
    fn_AWSProductPageLinkExtraction
    # - Download Procedure
    local target_save_dir="${doc_save_dir}/AWS Documentation"
    local category_dir="${target_save_dir}/${choose_product_topic}/${choose_product}"
    [[ -d "${category_dir}" ]] || mkdir -p "${category_dir}"
    fn_DocSaveDirectoryStatement "${category_dir}"

    local counter_flag=${counter_flag:-1}
    local category_flag=${category_flag:-}
    local file_url=${file_url:-}

    local l_title_details_list=${l_title_details_list:-}
    if [[ "${language_choose_cn}" -eq 1 ]]; then
        l_title_details_list=$(${download_method} "${product_page_url}" | sed -r -n '/class="columnbuilder[^"]*">/,/id="aws-page-footer"/{s@<br \/>@@g;s@<\/a>@&\n@g;/(<h3|href=)/!d;p}' | sed -r -n '/<h3[^>]*>/{s@[[:space:]]*<[^>]*>[[:space:]]*@@g;};s@.*href="([^"]+)"[^>]*>([^<]+)<.*$@\2|\1@g;s@^[[:blank:]]*@@g;/<[^>]*>/d;/^(PDF|HTML)/!{s@.*@---\n&@g};p' | sed -r -n ':a;N;$!ba;s@\n@|@g;s@\|?---\|?@\n@g;p' | sed -r '/HTML/!d;s@\|HTML@|&@g;')
        # china version not extract field 'url' defined in the following while loop, so just has 5 field

        echo "${l_title_details_list}" | while IFS="|" read -r title url html html_url pdf pdf_url; do
            # User Guide for Linux Instances|http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/|HTML|http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/|PDF|http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-ug.pdf

            case "${file_type,,}" in
                pdf ) file_url="${pdf_url}" ;;
            esac

            if [[ -z "${category_flag}" ]]; then
                category_flag="${category_dir}"
            elif [[ -n "${category_flag}" && "${category_flag}" != "${category_dir}" ]]; then
                counter_flag=1
                category_flag="${category_dir}"
            fi    # end if

            if [[ -n "${file_url}" && "${file_url}" =~ ${file_type,,}$ ]]; then
                doc_save_name="${category_dir}/${counter_flag}-${title//\//&}.${file_url##*.}"
                [[ -f "${doc_save_name}" ]] && rm -f "${doc_save_name}"
                fn_FileDownloadProcedure "${category_dir##*Amazon Web Services Documentation/}" "${title}" "${file_url}" "${doc_save_name}"
                (( counter_flag++ )) # let counter_flag+=1
            fi

        done

    else
        # old version
        # l_title_details_list=$(${download_method} "${product_page_url}" | sed -r -n '/<tbody>/,/<\/tbody>/{s@<br \/>@@g;s@<\/a>@&\n@g;/href=/!d;p}' | sed -r -n 's@.*href="([^"]+)"[^>]*>([^<]+)<.*$@\2|\1@g;s@^[[:blank:]]*@@g;/\|/!d;/^\|/d;/Kindle/d;/^(PDF|HTML)/!{s@.*@---\n&@g};p' | sed -r -n ':a;N;$!ba;s@\n@|@g;s@\|?---\|?@\n@g;p' | sed -r '/HTML/!d')

        # specific landing-page
        local l_landing_page=${l_landing_page:-"https://docs.aws.amazon.com/${product_short_name}/en_us/landing-page.xml"}
        # https://docs.aws.amazon.com/ec2/en_us/landing-page.xml

        # Amazon EC2|User Guide for Linux Instances|/AWSEC2/latest/UserGuide/
        # Amazon EC2|User Guide for Windows Instances|/AWSEC2/latest/WindowsGuide/
        # ...

        l_title_details_list=$(${download_method} "${l_landing_page}" 2> /dev/null | sed -r -n '/<sections[^>]*>/,/<\/sections>/{/<section[[:space:]]+id=/,/<\/section>/{p}}' | sed -r -n '/<simple-card/,/<\/simple-card>/{/embeddable-link/d;/.*?href=/{s@.*href="([^"]+)".*@\1@g;s@.*@-&@g;p};/<(title)>/{s@-*[[:space:]]*<[^>]*>[[:space:]]*-*@@g;p}}; /<title[^>]*>/{s@[[:space:]]*<[^>]*>[[:space:]]*@@g;s@.*@--&@g;p}' | sed ':a;N;$!ba;s@\n@|@g' | sed -r -n 's@\|-@\n@g;s@&nbsp;@ @g;s@&amp;@\&@g;p' | sed -r -n 's@^-+@@g;p' | awk -F\| '{if ($0~/\|/) {printf("%s|%s|%s\n",a,$2,$1)}else {a=$0}}' | while read -r line;do
            # /AWSEC2/latest/UserGuide/
            partial_url="${line##*|}"
            # /codebuild/latest/userguide/welcome.html
            [[ "${partial_url}" =~ .html$ ]] && partial_url="${partial_url%/*}/"
            # https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/meta-inf/guide-info.json
            json_info=$(${download_method} "https://docs.aws.amazon.com${partial_url}meta-inf/guide-info.json" 2> /dev/null)
            # {
            #     "locale" : "en_us",
            #     "title" : "Amazon Elastic Compute Cloud",
            #     "subtitle" : "User Guide for Linux Instances",
            #     "abstract" : "Use Amazon EC2 to configure, launch, and manage virtual servers in the AWS cloud.",
            #     "pdf" : "ec2-ug.pdf",
            #     "kindle" : "https://www.amazon.com/dp/B076452RSZ",
            #     "github" : "https://github.com/awsdocs/amazon-ec2-user-guide/tree/master/doc_source"
            # }

            pdf_name=$(echo "${json_info}" | sed -r -n '/"pdf"/{s@^[^:]+:[^"]+"([^"]+).*$@\1@g;p}')

            if [[ -n "${pdf_name}" ]]; then
                # https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-ug.pdf
                pdf_url="https://docs.aws.amazon.com${partial_url}${pdf_name}"

                doc_category="${line%%|*}"
                [[ -n "${doc_category}" ]] || doc_category="${choose_product}"
                subtitle_name=$(echo "${json_info}" | sed -r -n '/"subtitle"/{s@^[^:]+:[^"]+"([^"]+).*$@\1@g;p}')

                echo "${doc_category}|${subtitle_name}|${partial_url}|${pdf_url}"
                # Amazon EC2|User Guide for Linux Instances|/AWSEC2/latest/UserGuide/|https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-ug.pdf
            fi
        done
        )

        new_category_dir="${category_dir}"

        echo "${l_title_details_list}" | while IFS="|" read -r doc_category doc_title doc_shorturl doc_pdf_url; do
            file_url="${doc_pdf_url}"

            if [[ "${new_category_dir##*\/}" != "${doc_category}" ]]; then
                new_category_dir="${category_dir}/${doc_category}"
                counter_flag=1
            fi

            [[ -d "${new_category_dir}" ]] || mkdir -p "${new_category_dir}"

            doc_save_name="${new_category_dir}/${counter_flag}-${doc_title//\//&}.${file_url##*.}"

            [[ -f "${doc_save_name}" ]] && rm -f "${doc_save_name}"
            fn_FileDownloadProcedure "${new_category_dir##*/}" "${doc_title}" "${file_url}" "${doc_save_name}"
            (( counter_flag++ )) # let counter_flag+=1
        done

    fi

    # remove empty dir
    [[ -d "${target_save_dir}" ]] && find "${target_save_dir}" -type d -empty -exec ls -d {} \; | while IFS="" read -r line; do [[ -d "${line}" ]]&& rm -rf "${line}"; done
}



#########  4. Operation Time Cost  #########
fn_TotalTimeCosting(){
    finish_time=$(date +'%s')        # End Time Of Operation
    total_time_cost=$((finish_time-start_time))   # Total Time Of Operation
    printf "\nTotal time cost is ${c_red}%s${c_normal} seconds!\n" "${total_time_cost}"
}


#########  5. Executing Process  #########
fn_InitializationCheck
fn_GetOptsVariableConfiguration

fn_LinuxDistributionSelection
fn_"${gnulinux_distro_name// /}"Operation
fn_TotalTimeCosting



#########  4. EXIT Singal Processing  #########
# trap "commands" EXIT # execute command when exit from shell
fn_TrapEXIT(){
    fnBase_UnsetVars
    unset gnulinux_distro_name
    unset file_type
    unset category_manual_choose
    unset language_choose_cn
    unset doc_save_dir
    unset nautilus_action
    unset proxy_server_specify
    unset max_retry_count

    unset choose_product_topic
    unset choose_product
    unset choose_version
    unset choose_category
    unset product_content_list
    unset choose_product
    unset product_full_name
    unset doc_info_list
}

trap fn_TrapEXIT EXIT

# Script End
