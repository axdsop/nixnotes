#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails

# Official Site: https://www.gnome.org/
# Documentation: https://www.gnome.org/technologies/

# Target: Configuration GNOME 3 Desktop Enviroment In GNU/Linux
# Developer: MaxdSre

# Change Log:
# - Oct 08, 2020 09:11 Thu ET - initialization check method update
# - Jul 02, 2019 12:54 Tue ET - add gsettings for evince, gedit
# - Jan 01, 2019 20:52 Tue ET - code reconfiguration, include base funcions from other file
# - Dec 05, 2017 11:03 +0800
# - Sep 25, 2017 14:03 +0800


# GLib-GIO-Message: Using the 'memory' GSettings backend.  Your settings will not be saved or shared with other applications.
# https://askubuntu.com/questions/558446/my-dconf-gsettings-installation-is-broken-how-can-i-fix-it-without-ubuntu-reins
# set $PATH incorrectly



#########  0-1. Singal Setting  #########
trap fnBase_TrapCleanUp INT QUIT

#########  0-2. Variables Setting  #########
# umask 022    # temporarily change umask value to 022
proxy_server_specify=${proxy_server_specify:-}


#########  1-1 getopts Operation  #########
funcHelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | bash -s -- [options] ...

GNOME 3 Desktop Enviroment Configuration In GNU/Linux (RHEL/SUSE/Debian)

Running as normal user.

[available option]
    -h    --help, show help info
    -p [protocol:]ip:port    --proxy host (http|https|socks4|socks5), default protocol is http
\e[0m"
}

while getopts "hp:" option "$@"; do
    case "$option" in
        p ) proxy_server_specify="$OPTARG" ;;
        h|\? ) funcHelpInfo && exit ;;
    esac
done



#########  1-2 Preparation Operation  #########
fn_InitializationCheck(){
    local l_user_codebase_dir="$HOME/.config/axdsop"
    local l_base_function_temp_save_path=''
    local l_need_remove=0
    local l_base_function_url='https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/base_functions.sh'
    l_base_function_temp_save_path=$(find "${l_user_codebase_dir}" -type f -name "${l_base_function_url##*/}" -print 2> /dev/null)
    
    if [[ ! -f "${l_base_function_temp_save_path}" ]]; then
        local l_download_tool=${l_download_tool:-'wget -qO-'}
        [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_tool='curl -fsL'
        l_need_remove=1
        l_base_function_temp_save_path=$(mktemp -t XXXXXXX)
        ${l_download_tool} "${l_base_function_url}" > "${l_base_function_temp_save_path}"
    fi

    if [[ -f "${l_base_function_temp_save_path}" && -s "${l_base_function_temp_save_path}" ]]; then
        . "${l_base_function_temp_save_path}"
        [[ "${l_need_remove}" -eq 1 ]] && rm "${l_base_function_temp_save_path}"
    else
        echo "Sorry, fail to get base script path."
        exit
    fi

    # specify 1 means need sudo privilege
    fnBase_RunningEnvironmentCheck '0'

    fnBase_CommandExistCheckPhase 'gzip' # CentOS/Debian/OpenSUSE: gzip
    fnBase_CommandExistCheckPhase 'tar' # .tar.gz
    fnBase_CommandExistCheckPhase 'unzip'
}


#########  2-1. GNOME Extension Installation  #########
# - CustomCorner
fn_CustomCornerInstallation(){
    local download_link=${download_link:-'https://gitlab.com/eccheng/customcorner/repository/master/archive.tar.gz'}
    local pack_save_path=${pack_save_path:="${temp_save_dir}/${download_link##*/}"}
    $download_method "${download_link}" > "${pack_save_path}"

    local gnome_extension_dir=${gnome_extension_dir:-"${login_user_home}/.local/share/gnome-shell/extensions/customcorner@eccheng.gitlab.com"}
    [[ -d "${gnome_extension_dir}" ]] && rm -rf "${gnome_extension_dir}"
    mkdir -p "${gnome_extension_dir}"
    tar xf "${pack_save_path}" -C "${gnome_extension_dir}" --strip-components=1

    local gnome_shell_version=${gnome_shell_version:-}
    gnome_shell_version=$(gnome-shell --version 2>/dev/null | sed -r -n '/GNOME Shell/{s@[^[:digit:]]*(.*)$@\1@g;p}')
    [[ -f "${gnome_extension_dir}/metadata.json" ]] && sed -i -r '/shell-version/s@("shell-version": \[\").*(\"\],)@\1'"${gnome_shell_version}"'\2@' "${gnome_extension_dir}/metadata.json"

    chown -R "${login_user}" "${gnome_extension_dir}"
    chgrp -hR "${login_user}" "${gnome_extension_dir}" &> /dev/null
    [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
}

# - Proxy Switcher
fn_ProxySwitcherInstattation(){
    local download_link=${download_link:-'https://github.com/tomflannaghan/proxy-switcher/archive/master.zip'}
    local pack_save_path=${pack_save_path:="${temp_save_dir}/${download_link##*/}"}
    $download_method "${download_link}" > "${pack_save_path}"

    local gnome_extension_dir=${gnome_extension_dir:-"${login_user_home}/.local/share/gnome-shell/extensions/ProxySwitcher@flannaghan.com"}
    [[ -d "${gnome_extension_dir}" ]] && rm -rf "${gnome_extension_dir}"
    mkdir -p "${gnome_extension_dir}"

    local temp_decompress_dir=${temp_decompress_dir:-"${temp_save_dir}/proxy-switcher"}
    [[ -d "${temp_decompress_dir}" ]] && rm -rf "${temp_decompress_dir}"
    mkdir -p "${temp_decompress_dir}"

    unzip -q -d "${temp_decompress_dir}" "${pack_save_path}"
    local extract_dir_src=${extract_dir_src:-}
    extract_dir_src=$(find "${temp_decompress_dir}" -type d -name 'src' -print)
    [[ -d "${extract_dir_src}" ]] && cp -R "${extract_dir_src}"/* "${gnome_extension_dir}"
    # extension.js, messages.pot, metadata.json

    chown -R "${login_user}" "${gnome_extension_dir}"
    chgrp -hR "${login_user}" "${gnome_extension_dir}" &> /dev/null

    [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
    [[ -d "${temp_decompress_dir}" ]] && rm -rf "${temp_decompress_dir}"
}


#########  2-2. GNOME Tweak Tool Configuration  #########
# /usr/share/glib-2.0/schemas/*.xml   # https://askubuntu.com/questions/84631/how-do-i-switch-off-continuous-mode-permanently-in-evince#1013853
# gsettings list-schemas
# gsettings list-keys
# gsettings list-schemas | sort | while IFS="" read -r schema; do gsettings list-keys "${schema}" | sort | while IFS="" read -r key; do echo "${schema}|${key}|$(gsettings get ${schema} ${key} 2>/dev/null)"; done; done

fn_GsettingsConfiguration(){
    # - Desktop
    # /usr/share/glib-2.0/schemas/org.gnome.desktop.background.gschema.xml
    gsettings set org.gnome.desktop.background show-desktop-icons false

    # centered|none|scaled|spanned|stretched|wallpater|zoom
    # gsettings set org.gnome.desktop.background picture-options 'zoom'
    # gsettings set org.gnome.desktop.screensaver picture-options|'zoom'

    # - Extensions
    # Applications menu|apps-menu@gnome-shell-extensions.gcampax.github.com
    # Place status indicator|places-menu@gnome-shell-extensions.gcampax.github.com
    # Window list|window-list@gnome-shell-extensions.gcampax.github.com
    # Removeable drive menu|drive-menu@gnome-shell-extensions.gcampax.github.com
    # Windownavigator|windowsNavigator@gnome-shell-extensions.gcampax.github.com
    # ProxySwitcher|ProxySwitcher@flannaghan.com
    # Customcorner|customcorner@eccheng.gitlab.com

    # gsettings get org.gnome.shell enabled-extensions

    # Calendar & Date & Time
    gsettings set org.gnome.desktop.calendar show-weekdate true
    gsettings set org.gnome.desktop.datetime automatic-timezone false
    gsettings set org.gnome.desktop.interface clock-format '24h'
    gsettings set org.gnome.desktop.interface clock-show-weekday true
    gsettings set org.gnome.desktop.interface clock-show-date true
    gsettings set org.gnome.desktop.interface clock-show-seconds false

    # /usr/share/glib-2.0/schemas/org.gnome.desktop.lockdown.gschema.xml
    # gsettings set org.gnome.desktop.lockdown disable-application-handlers false
    # gsettings set org.gnome.desktop.lockdown disable-command-line false
    # gsettings set org.gnome.desktop.lockdown disable-lock-screen false
    # gsettings set org.gnome.desktop.lockdown disable-log-out false
    # gsettings set org.gnome.desktop.lockdown disable-printing false
    # gsettings set org.gnome.desktop.lockdown disable-print-setup false
    # gsettings set org.gnome.desktop.lockdown disable-save-to-disk false
    # gsettings set org.gnome.desktop.lockdown disable-user-switching false
    # gsettings set org.gnome.desktop.lockdown user-administration-disabled false

    # /usr/share/glib-2.0/schemas/org.gnome.desktop.media-handling.gschema.xml
    gsettings set org.gnome.desktop.media-handling autorun-never false
    gsettings set org.gnome.desktop.media-handling automount true
    gsettings set org.gnome.desktop.media-handling automount-open true

    # /usr/share/glib-2.0/schemas/org.gnome.desktop.peripherals.gschema.xml
    # Touchpad
    gsettings set org.gnome.desktop.peripherals.touchpad click-method 'fingers' # none, areas, fingers, default
    gsettings set org.gnome.desktop.peripherals.touchpad disable-while-typing true
    gsettings set org.gnome.desktop.peripherals.touchpad edge-scrolling-enabled true
    gsettings set org.gnome.desktop.peripherals.touchpad left-handed 'mouse'
    gsettings set org.gnome.desktop.peripherals.touchpad natural-scroll true
    gsettings set org.gnome.desktop.peripherals.touchpad send-events 'enabled'
    # gsettings set org.gnome.desktop.peripherals.touchpad speed 0.0
    gsettings set org.gnome.desktop.peripherals.touchpad tap-and-drag true
    gsettings set org.gnome.desktop.peripherals.touchpad tap-to-click true
    gsettings set org.gnome.desktop.peripherals.touchpad two-finger-scrolling-enabled true

    # Desktop Privacy
    # /usr/share/glib-2.0/schemas/org.gnome.desktop.privacy.gschema.xml
    [[ -f "${login_user_home}/.local/share/recently-used.xbel" ]] && rm -f "${login_user_home}/.local/share/recently-used.xbel"
    gsettings set org.gnome.desktop.privacy disable-camera false
    gsettings set org.gnome.desktop.privacy disable-microphone false
    gsettings set org.gnome.desktop.privacy disable-sound-output false
    gsettings set org.gnome.desktop.privacy hide-identity false
    gsettings set org.gnome.desktop.privacy old-files-age 7
    gsettings set org.gnome.desktop.privacy recent-files-max-age 0
    gsettings set org.gnome.desktop.privacy remember-app-usage false
    gsettings set org.gnome.desktop.privacy remember-recent-files false
    gsettings set org.gnome.desktop.privacy remove-old-temp-files true
    gsettings set org.gnome.desktop.privacy remove-old-trash-files true
    gsettings set org.gnome.desktop.privacy report-technical-problems false
    gsettings set org.gnome.desktop.privacy send-software-usage-stats false
    gsettings set org.gnome.desktop.privacy show-full-name-in-top-bar true


    # - Ubuntu dock panel
    gsettings set org.gnome.shell.extensions.dash-to-dock dock-position BOTTOM
    gsettings set org.gnome.shell.extensions.dash-to-dock dash-max-icon-size 32
    # gsettings set org.gnome.shell.extensions.dash-to-dock transparency-mode FIXED
    gsettings set org.gnome.shell.extensions.dash-to-dock extend-height true
    gsettings set org.gnome.shell.extensions.dash-to-dock unity-backlit-items false

    # - Login Screen
    gsettings set org.gnome.login-screen allowed-failures 3
    # gsettings set org.gnome.login-screen banner-message-enable false
    # gsettings set org.gnome.login-screen banner-message-text ''
    gsettings set org.gnome.login-screen disable-restart-buttons true
    gsettings set org.gnome.login-screen disable-user-list false
    gsettings set org.gnome.login-screen enable-fingerprint-authentication true
    gsettings set org.gnome.login-screen enable-password-authentication true
    gsettings set org.gnome.login-screen enable-smartcard-authentication true
    # gsettings set org.gnome.login-screen fallback-logo ''
    # gsettings set org.gnome.login-screen logo ''

    # screen lock time
    # https://askubuntu.com/questions/1042641/how-to-set-custom-lock-screen-time-in-ubuntu-18-04
    gsettings set org.gnome.desktop.session idle-delay 300

    gsettings set org.gnome.desktop.screensaver idle-activation-enabled true
    gsettings set org.gnome.desktop.screensaver lock-delay 60
    gsettings set org.gnome.desktop.screensaver lock-enabled true
    gsettings set org.gnome.desktop.screensaver logout-enabled false
    gsettings set org.gnome.desktop.screensaver show-full-name-in-top-bar true
    gsettings set org.gnome.desktop.screensaver status-message-enabled true
    gsettings set org.gnome.desktop.screensaver user-switch-enabled true

    # /usr/share/glib-2.0/schemas/org.gnome.desktop.sound.gschema.xml
    gsettings set org.gnome.desktop.sound allow-volume-above-100-percent false
    gsettings set org.gnome.desktop.sound event-sounds true
    gsettings set org.gnome.desktop.sound input-feedback-sounds false


    # - Windows
    # Titlebar Buttons (default close)
    gsettings set org.gnome.desktop.wm.preferences button-layout 'appmenu:minimize,maximize,close'

    # - Workspaces
    # gsettings set org.gnome.desktop.wm.preferences num-workspaces 4
    gsettings set org.gnome.shell.overrides dynamic-workspaces true
    gsettings set org.gnome.shell.overrides workspaces-only-on-primary true

    # evince - pdf reader
    # /usr/share/glib-2.0/schemas/org.gnome.Evince.gschema.xml
    # https://gitlab.gnome.org/GNOME/evince/blob/master/data/org.gnome.Evince.gschema.xml
    gsettings set org.gnome.Evince allow-links-change-zoom true
    gsettings set org.gnome.Evince auto-reload true
    gsettings set org.gnome.Evince override-restrictions true
    gsettings set org.gnome.Evince page-cache-size 50
    gsettings set org.gnome.Evince show-caret-navigation-message true
    gsettings set org.gnome.Evince.Default continuous true
    gsettings set org.gnome.Evince.Default dual-page false
    gsettings set org.gnome.Evince.Default dual-page-odd-left false
    gsettings set org.gnome.Evince.Default fullscreen false
    gsettings set org.gnome.Evince.Default show-sidebar false
    gsettings set org.gnome.Evince.Default show-toolbar false
    gsettings set org.gnome.Evince.Default sizing-mode 'fit-width' # fit-page, fit-width, free, automatic

    # gedit
    # /usr/share/glib-2.0/schemas/org.gnome.gedit.gschema.xml
    gsettings set org.gnome.gedit.preferences.editor auto-indent false
    gsettings set org.gnome.gedit.preferences.editor auto-save true
    gsettings set org.gnome.gedit.preferences.editor auto-save-interval 5
    # gsettings set org.gnome.gedit.preferences.editor background-pattern 'none'
    # gsettings set org.gnome.gedit.preferences.editor bracket-matching false
    # gsettings set org.gnome.gedit.preferences.editor create-backup-copy false
    gsettings set org.gnome.gedit.preferences.editor display-line-numbers true
    gsettings set org.gnome.gedit.preferences.editor display-overview-map false
    gsettings set org.gnome.gedit.preferences.editor display-right-margin false
    gsettings set org.gnome.gedit.preferences.editor editor-font 'Monospace 14'
    gsettings set org.gnome.gedit.preferences.editor ensure-trailing-newline true
    gsettings set org.gnome.gedit.preferences.editor highlight-current-line true
    gsettings set org.gnome.gedit.preferences.editor insert-spaces true
    gsettings set org.gnome.gedit.preferences.editor max-undo-actions 2000
    gsettings set org.gnome.gedit.preferences.editor restore-cursor-position true
    gsettings set org.gnome.gedit.preferences.editor right-margin-position 80
    gsettings set org.gnome.gedit.preferences.editor scheme 'solarized-light'
    gsettings set org.gnome.gedit.preferences.editor search-highlighting true
    gsettings set org.gnome.gedit.preferences.editor smart-home-end 'after'
    gsettings set org.gnome.gedit.preferences.editor syntax-highlighting true
    gsettings set org.gnome.gedit.preferences.editor tabs-size 4
    gsettings set org.gnome.gedit.preferences.editor use-default-font false
    # gsettings set org.gnome.gedit.preferences.editor wrap-last-split-mode 'word'
    # gsettings set org.gnome.gedit.preferences.editor wrap-mode 'word'
    # gsettings set org.gnome.gedit.preferences.encodings candidate-encodings ['']
    gsettings set org.gnome.gedit.preferences.ui bottom-panel-visible false
    gsettings set org.gnome.gedit.preferences.ui max-recents 0
    gsettings set org.gnome.gedit.preferences.ui show-tabs-mode 'auto' # never, always, auto
    gsettings set org.gnome.gedit.preferences.ui side-panel-visible false
    gsettings set org.gnome.gedit.preferences.ui statusbar-visible true
    gsettings set org.gnome.gedit.preferences.ui toolbar-visible true

    # gnome-terminal
    # /usr/share/glib-2.0/schemas/org.gnome.Terminal.gschema.xml
    gsettings set org.gnome.Terminal.Legacy.Settings confirm-close true
    gsettings set org.gnome.Terminal.Legacy.Settings default-show-menubar false
    # gsettings set org.gnome.Terminal.Legacy.Settings headerbar @mb nothing
    gsettings set org.gnome.Terminal.Legacy.Settings menu-accelerator-enabled true
    gsettings set org.gnome.Terminal.Legacy.Settings mnemonics-enabled false
    gsettings set org.gnome.Terminal.Legacy.Settings new-terminal-mode 'tab' # tab, window
    gsettings set org.gnome.Terminal.Legacy.Settings schema-version 3
    gsettings set org.gnome.Terminal.Legacy.Settings shell-integration-enabled true
    gsettings set org.gnome.Terminal.Legacy.Settings shortcuts-enabled true
    gsettings set org.gnome.Terminal.Legacy.Settings tab-policy 'automatic'
    gsettings set org.gnome.Terminal.Legacy.Settings tab-position 'top'
    gsettings set org.gnome.Terminal.Legacy.Settings theme-variant 'system'
    gsettings set org.gnome.Terminal.Legacy.Settings unified-menu true


    # - Network Proxy
    # 'none'|'manual'|'auto'
    # org.gnome.system.proxy|mode|'none'
    # automatic
    # org.gnome.system.proxy|autoconfig-url|''
    # Manually
    # org.gnome.system.proxy|ignore-hosts|['localhost', '127.0.0.0/8', '::1']
    # org.gnome.system.proxy|use-same-proxy|true
    # org.gnome.system.proxy.ftp|host|''
    # org.gnome.system.proxy.ftp|port|0
    # org.gnome.system.proxy.http|authentication-password|''
    # org.gnome.system.proxy.http|authentication-user|''
    # org.gnome.system.proxy.http|enabled|false
    # org.gnome.system.proxy.http|host|''
    # org.gnome.system.proxy.http|port|0
    # org.gnome.system.proxy.http|use-authentication|false
    # org.gnome.system.proxy.https|host|''
    # org.gnome.system.proxy.https|port|0
    # org.gnome.system.proxy.socks|host|''
    # org.gnome.system.proxy.socks|port|0
}


#########  3. Executing Process  #########
fn_InitializationCheck

if fnBase_CommandExistIfCheck 'gnome-shell'; then
    # GNOME Extension
    fn_CustomCornerInstallation
    # fn_ProxySwitcherInstattation

    fnBase_CommandExistIfCheck 'gsettings' && fn_GsettingsConfiguration
fi


#########  4. EXIT Singal Processing  #########
# trap "commands" EXIT # execute command when exit from shell
fn_TrapEXIT(){
    fnBase_UnsetVars
    unset proxy_server_specify
    unset pack_manager
}

trap fn_TrapEXIT EXIT


# Script End
