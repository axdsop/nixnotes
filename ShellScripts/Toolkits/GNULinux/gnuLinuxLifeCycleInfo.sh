#!/usr/bin/env bash
# shellcheck disable=SC2030,SC2031
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails

# Target: Extract GNU/Linux Release Lifecycle Info (RHEL/CentOS/Debian/Ubuntu)
# Developer: MaxdSre

# Change Log:
# - Oct 08, 2020 09:11 Thu ET - initialization check method update
# - Oct 02, 2019 09:02 Wed ET - change to new base functions
# - Aug 23, 2019 17:48 Fri ET - change centos parallel operation to optional
# - Aug 06, 2019 19:58 Tue ET - fix rhel,centos eol date, base on previous version
# - Jun 07, 2019 16:25 Sun ET - Debian release info extraction optimization
# - Apr 19, 2019 10:43 Thu ET - Ubuntu current add column 'End of Standard Support' which was `End of Life` before.
# - Jan 01, 2018 20:58 Tue ET - code reconfiguration, include base funcions from other file
# - Dec 04, 2018 10:12 Tue ET - remove rhel7Arr associate array https://lists.centos.org/pipermail/centos-announce/2018-May/022829.html
# - May 10, 2018 06:53 Thu ET - optimize extraction for CentOS 7.5.1084 release https://lists.centos.org/pipermail/centos-announce/2018-May/022829.html
# - May 01, 2018 16:21 ET - optimize codename extractiong for "Ubuntu 18.04 LTS BionicBeaver" list in https://wiki.ubuntu.com/Releases
# - Apr 17, 2018 14:46 ET - output format optimization
# - Oct 30, 2017 19:32 Sat +0800 - add all distros release info print function
# - Oct 12, 2017 11:58 Thu +0800
# - Sep 26, 2017 09:29 Tue +0800
# - Sep 12, 2017 15:49 Tue +0800
# - Aug 28, 2017 09:18 Mon +0800
# - Aug 03, 2017 14:38 Thu +0800
# - June 25, 2017 19:18 Sun +0800


#########  0-1. Singal Setting  #########
trap fnBase_TrapCleanUp INT QUIT

#########  0-2. Variables Setting  #########
# umask 022    # temporarily change umask value to 022
all_list=${all_list:-0}
distribution_choose=${distribution_choose-:''}
show_details=${show_details:-0}
markdown_format=${markdown_format:-0}
proxy_server_specify=${proxy_server_specify:-''}
current_timestamp=$(date +'%s')


#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | bash -s -- [options] ...

Listing GNU/Linux Release Lifecycle Info (RHEL/CentOS/Debian/Ubuntu)!

[available option]
    -h    --help, show help info
    -a    --list all distros(RHEL/CentOS/Debian/Ubuntu) release info
    -c distribution    --specify distribution (RHEL/CentOS/Debian/Ubuntu)
    -d    --show details, list all info
    -m    --output markdown format
    -p [protocol:]ip:port    --proxy host (http|https|socks4|socks5), default protocol is http
\e[0m"
}

while getopts "hac:dmp:" option "$@"; do
    case "$option" in
        a ) all_list=1 ;;
        c ) distribution_choose="$OPTARG" ;;
        d ) show_details=1 ;;
        m ) markdown_format=1 ;;
        p ) proxy_server_specify="$OPTARG" ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done

[[ "${all_list}" -eq 1 ]] && show_details=0


#########  1-2 Preparation Operation  #########
fn_InitializationCheck(){
    local l_user_codebase_dir="$HOME/.config/axdsop"
    local l_base_function_temp_save_path=''
    local l_need_remove=0
    local l_base_function_url='https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/base_functions.sh'
    l_base_function_temp_save_path=$(find "${l_user_codebase_dir}" -type f -name "${l_base_function_url##*/}" -print 2> /dev/null)
    
    if [[ ! -f "${l_base_function_temp_save_path}" ]]; then
        local l_download_tool=${l_download_tool:-'wget -qO-'}
        [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_tool='curl -fsL'
        l_need_remove=1
        l_base_function_temp_save_path=$(mktemp -t XXXXXXX)
        ${l_download_tool} "${l_base_function_url}" > "${l_base_function_temp_save_path}"
    fi

    if [[ -f "${l_base_function_temp_save_path}" && -s "${l_base_function_temp_save_path}" ]]; then
        . "${l_base_function_temp_save_path}"
        [[ "${l_need_remove}" -eq 1 ]] && rm "${l_base_function_temp_save_path}"
    else
        echo "Sorry, fail to get base script path."
        exit
    fi

    # tput clear  # Echo the clear-screen sequence for the current terminal.
    fnBase_RunningEnvironmentCheck '0'

    fnBase_CommandExistCheckPhase 'gawk'
    fnBase_CommandExistCheckPhase 'sed'
    # fnBase_CommandExistCheckPhase 'curl'

    is_use_parallel=${is_use_parallel:-0}
    # [[ "${all_list}" -eq 1 ]] && is_use_parallel=1

    case "${distribution_choose,,}" in
        c|centos )
            # is_use_parallel=1
            fnBase_CommandExistIfCheck 'parallel' && is_use_parallel=1
        ;;
    esac
}


#########  2. GNU/Linux Distributions Selection #########
fn_LinuxDistributionMenuList(){
    local l_distribution_arr=("RHEL" "CentOS" "Debian" "Ubuntu")    # index array
    echo "${c_blue}Available GNU/Linux Distribution List:${c_normal}"
    PS3="${c_yellow}Choose distribution number(e.g. 1, 2,...): ${c_normal}"

    local l_choose_name=''

    select item in "${l_distribution_arr[@]}"; do
        l_choose_name="${item}"
        [[ -n "${l_choose_name}" ]] && break
    done < /dev/tty

    distribution_choose="${l_choose_name}"
    echo -e '\n'
    unset PS3
}

fn_LinuxDistributionSelection(){
    case "${distribution_choose,,}" in
        r|redhat|rhel ) distribution_choose='RHEL' ;;
        c|centos ) distribution_choose='CentOS' ;;
        d|debian ) distribution_choose='Debian' ;;
        u|ubuntu ) distribution_choose='Ubuntu' ;;
        * ) fn_LinuxDistributionMenuList ;;
    esac
}


#########  3. GNU/Linux Distributions  #########

#########  3-1. RHEL  #########
# Red Hat Enterprise Linux Life Cycle
# https://access.redhat.com/support/policy/updates/errata/
# Red Hat Enterprise Linux Release Dates
# https://access.redhat.com/articles/3078
fn_LifeCycleForRHEL(){
    local l_rhel_release_info_page='https://access.redhat.com/articles/3078'
    local l_rhel_life_cycle_page='https://access.redhat.com/support/policy/updates/errata'
    rhel_release_date=$(mktemp -t "${mktemp_format}")
    rhel_eus_date=$(mktemp -t "${mktemp_format}")

    # extract Extended Update Support (EUS) date
    ${download_method} "${l_rhel_life_cycle_page}" | sed -r -n '/id="Long_Support"/,/<\/section>/{/\(end(s|ed)/{s@^[[:space:]]*@@g;s@(<[^>]*>|\(|\))@@g;s@;.*$@@g;s@ end(s|ed) @|@g;s@([[:digit:]]+)st@\1@g;p}}' | awk -F\| 'BEGIN{OFS="|"}{if(arr[$1]==""){"date --date=\""$2"\" +\"%F\"" | getline a;arr[$1]=a}}END{PROCINFO["sorted_in"]="@ind_str_asc";for (i in arr) print i,arr[i]}' > "${rhel_eus_date}"
    # 7.7 (ends August 30, 2021; Final RHEL 7 EUS Release)
    # 7.6|2020-10-31

    # extract per specific version and relevant info
    ${download_method} "${l_rhel_release_info_page}" | sed -r -n '/id=\"RHEL/,/\/table/{/tbody/,/\/tbody/{s@<\/?(tbody|a)[[:space:]]*[^>]*>@@g;s@<(tr|td)>@@;s@<\/td>@|@g;s@(\.)[[:space:]]*@\1@g;s@RHEL ([[:digit:].]+)[[:space:]]*.*@\1|@g;p}}' | awk '{if($0!~/<\/tr>/){ORS="";print $0}else{printf "\n"}}' | awk -F\| 'BEGIN{OFS="|"}{if(a[$1]==""){a[$1]=$0}}END{PROCINFO["sorted_in"]="@ind_num_desc";for(i in a) print a[i]}' > "${rhel_release_date}"
    # Release|General Availability Date|redhat-release Errata Date*|Kernel Version
    # 7.6|2018-10-30|2018-10-30 RHBA-2018:3014|3.10.0-957|

    # output header setting
    local field_seperator=${field_seperator:-}

    if [[ "${all_list}" -ne 1 ]]; then
        if [[ "${show_details}" -eq 1 ]]; then
            [[ "${markdown_format}" -eq 1 ]] && field_seperator='---|---|---|---\n'
            printf "%s|%s|%s|%s\n${field_seperator}" "Version" "Release Date" "EUS Date" "Kernel Version"
        else
            [[ "${markdown_format}" -eq 1 ]] && field_seperator='---|---|---\n'
            printf "%s|%s|%s\n${field_seperator}" "Version" "Release Date" "EUS Date"
        fi
    fi

    awk -F\| '{if(a[$1]==""){a[$1]=$1}}END{PROCINFO["sorted_in"]="@ind_num_desc";for (i in a) print i}' "${rhel_eus_date}" "${rhel_release_date}" | sort -t\. -k 1,1rn -k 2,2rn -k 3,3rn | while read -r version_specific; do
        # 8, 7.7, 7.6 ...
        local specific_version_info=''
        local specific_eus_date=''

        # 7.6|2018-10-30|2018-10-30 RHBA-2018:3014|3.10.0-957|
        specific_version_info=$(awk -F\| 'match($1,/^'"${version_specific}"'$/)' "${rhel_release_date}")

        specific_eus_date=$(awk -F\| 'match($1,/^'"${version_specific}"'$/){print $NF}' "${rhel_eus_date}")

        if [[ -n "${specific_version_info}" ]]; then
            # shellcheck disable=SC2034
            echo "${specific_version_info}" | while IFS="|" read -r version release errate kernel;do
                if [[ "${all_list}" -ne 1 ]]; then
                    if [[ "${show_details}" -eq 1 ]]; then
                        echo "${version_specific}|${release}|${specific_eus_date}|${kernel}"
                    else
                        echo "${version_specific}|${release}|${specific_eus_date}"
                    fi

                else
                    # distro name|version|codename|release date|eol date|is eol
                    local is_eol=0
                    local eol_timestamp=''
                    [[ "${version_specific}" == '7.0' ]] && is_eol=1
                    [[ "${version_specific%%.*}" -lt 6 ]] && is_eol=1

                    # rhel|7.7|||2021-08-30|1
                    # rhel|7.6||2018-10-30|2020-10-31|1604116800|0
                    if [[ -n "${specific_eus_date}" ]]; then
                        is_eol=1
                        eol_timestamp=$(date --date="${specific_eus_date}" +'%s')
                        [[ "${eol_timestamp}" -gt ${current_timestamp} ]] && is_eol=0
                    else
                        if [[ "${version_specific}" =~ ^6 ]]; then
                            is_eol=1
                            local l_version_major=''
                            local l_version_minor=''
                            l_version_major="${version_specific%%.*}"
                            l_version_minor="${version_specific##*.}"
                            ((l_version_minor++))
                            specific_eus_date=$(awk -F\| 'match($1,/^'"${l_version_major}.${l_version_minor}"'$/){print $2}' "${rhel_release_date}")

                            # https://access.redhat.com/support/policy/updates/errata
                            # For 6.x End of Maintenance Support or Maintenance Support 2 (Product retirement)  November 30, 2020
                            [[ -n "${specific_eus_date}" ]] || specific_eus_date='2020-11-30'
                            eol_timestamp=$(date --date="${specific_eus_date}" +'%s')
                            [[ "${eol_timestamp}" -gt ${current_timestamp} ]] && is_eol=0
                        fi
                    fi

                    echo "rhel|${version_specific}||${release}|${specific_eus_date}|${eol_timestamp}|${is_eol}"
                fi

            done

        else
            if [[ "${all_list}" -ne 1 ]]; then
                echo "${version_specific}||${specific_eus_date}"
            else
                echo "rhel|${version_specific}||||1"
            fi
        fi
    done

    [[ -f "${rhel_eus_date}" ]] && rm -f "${rhel_eus_date}"
    unset rhel_eus_date
    [[ -f "${rhel_release_date}" ]] && rm -f "${rhel_release_date}"
    unset rhel_release_date

    fnBase_NotifySendStatement 'Life Cycle Info' 'RHEL'
}

#########  3-2. CentOS  #########
# Release Notes for supported CentOS distributions
# https://wiki.centos.org/Manuals/ReleaseNotes
fn_LifeCycleForCentOS(){
    centos_release_note=$(mktemp -t "${mktemp_format}")
    centos_announce_archive=$(mktemp -t "${mktemp_format}")
    centos_release_date=$(mktemp -t "${mktemp_format}")
    rhel_eus_date=$(mktemp -t "${mktemp_format}")
    rhel_life_cycle='https://access.redhat.com/support/policy/updates/errata'  #Red Hat Enterprise Linux Life Cycle
    release_note_site='https://wiki.centos.org/Manuals/ReleaseNotes'    # Release Notes for supported CentOS distributions
    announce_archive_site='https://lists.centos.org/pipermail/centos-announce/'     # The CentOS-announce Archives
    wiki_site='https://wiki.centos.org'

    # Step 1.1 通過ReleaseNotes頁面提取各Release版本的Release note
    fnSub_CentOSReleaseNote(){
        ${download_method} "${release_note_site}" | sed -r -n '/Release Notes for CentOS [[:digit:]]/{s@<\/a>@\n@g;p}' | sed -r -n '/ReleaseNotes\/CentOS/{s@.*\"(.*)\".*@\1@g;p}' | while read -r line; do
            release_note_page="${wiki_site}${line}"
            # release_note_page_update_time=$(${download_method} "${release_note_page}" | sed -r -n '/Last updated/{s@.*<\/strong> ([^>]*) <span class.*@\1@g;p}')  # page last update time
            release_version=${line##*'CentOS'}
            if [[ "${release_version}" == '7' ]]; then
                # Step 1.2 通過各Release版本的Release note提取準確的release版本號
                release_version=$(${download_method} "${release_note_page}" | sed -r -n '/^<h1/{s@<[^>]*>@@g;s@ \(@.@g;s@\)@@g;s@-@ @g;s@([[:alpha:]]|[[:space:]])@@g;p}')
            fi
            # 7.1611|https://wiki.centos.org/Manuals/ReleaseNotes/CentOS7
            # 7.1406|https://wiki.centos.org/Manuals/ReleaseNotes/CentOS7.1406
            echo "$release_version|$release_note_page" >> "${centos_release_note}"
            # echo "$release_version|$release_note_page"
        done
    }

    # Step 1.2 通過RHEL的Life Cycle頁面提取各RHEL發行版的EUS日期
    fnSub_RedHatExtendedUpdateSupportDate(){
        ${download_method} "${rhel_life_cycle}" | sed -r -n '/id="Long_Support"/,/<\/section>/{/\(end(s|ed)/{s@^[[:space:]]*@@g;s@(<[^>]*>|\(|\))@@g;s@;.*$@@g;s@ end(s|ed) @|@g;s@([[:digit:]]+)st@\1@g;p}}' | awk -F\| 'BEGIN{OFS="|"}{if(arr[$1]==""){"date --date=\""$2"\" +\"%F\"" | getline a;arr[$1]=a}}END{PROCINFO["sorted_in"]="@ind_str_asc";for (i in arr) print i,arr[i]}' > "${rhel_eus_date}"
    }

    [[ "${show_details}" -eq 1 ]] && fnSub_CentOSReleaseNote
    fnSub_RedHatExtendedUpdateSupportDate

    # Step 2.1 通過CentOS-announce Archives頁面，遍歷各個月份的Archives頁面
    # 通過各月份的archive頁，提取符合條件的Release信息，關鍵詞 Release for CentOS
    if [[ "${is_use_parallel}" -eq 1 ]]; then
        fnSub_CentOSSpecificReleaseInfo(){
            archive_url="${1}"
            # https://lists.centos.org/pipermail/centos-announce/2018-December/
            $download_method "${archive_url}" | sed -r -n '/Release for CentOS( Linux |-)[[:digit:]].*x86_64.*$/{s@ \(@.@g;s@\)@@g;s@-@ @g;s@@@g;p}' | sed -r -n '/(Live|Minimal)/d;s@.*=\"([^"]*)\".*Release for .* ([[:digit:].]+) .*@\2|'"${archive_url}"'\1@g;p'
        }

        export download_method="${download_method}"
        export -f fnSub_CentOSSpecificReleaseInfo   # used for command parallel
        # June 2017|2017-June/date.html
        ${download_method} "${announce_archive_site}" | sed -r -n '/Downloadable/,/\/table/{/(Thread|Subject|Author|Gzip|table|<tr>)/d;s@<\/?td>@@g;s@^[[:space:]]*@@g;/^$/d;p}' | sed -r ':a;N;$!ba;s@\n@ @g;s@<\/tr>@\n@g;' | sed -r -n '/href/{s@^[[:space:]]*@@g;s@([^:]*):.*\"(.*)date.html\".*@'"${announce_archive_site}"'\2@g;p}' | parallel -k -j 20 fnSub_CentOSSpecificReleaseInfo 2> /dev/null >> "${centos_announce_archive}"
    else
        ${download_method} "${announce_archive_site}" | sed -r -n '/Downloadable/,/\/table/{/(Thread|Subject|Author|Gzip|table|<tr>)/d;s@<\/?td>@@g;s@^[[:space:]]*@@g;/^$/d;p}' | sed -r ':a;N;$!ba;s@\n@ @g;s@<\/tr>@\n@g;' | sed -r -n '/href/{s@^[[:space:]]*@@g;s@([^:]*):.*\"(.*)date.html\".*@'"${announce_archive_site}"'\2@g;p}' | while read -r archive_url; do
            # https://lists.centos.org/pipermail/centos-announce/2018-December/
            ${download_method} "${archive_url}" | sed -r -n '/Release for CentOS( Linux |-)[[:digit:]].*x86_64.*$/{s@ \(@.@g;s@\)@@g;s@-@ @g;s@@@g;p}' | sed -r -n '/(Live|Minimal)/d;s@.*=\"([^"]*)\".*Release for .* ([[:digit:].]+) .*@\2|'"${archive_url}"'\1@g;p' >> "${centos_announce_archive}"
        done

    fi

    # Step 2.2 通過各Release版本頁面提取release時間
    if [[ "${is_use_parallel}" -eq 1 ]]; then
        fnSub_CentOSReleaseDate(){
            line="$1"
            release_no="${line%%|*}"
            release_archive_page="${line#*|}"
            release_date=$($download_method "${release_archive_page}" | awk '$0~/<I>/{a=gensub(/.*>(.*)<.*/,"\\1","g",$0);"TZ=\"UTC\" date --date=\""a"\" +\"%F %Z\"" | getline b;print b}')    # %F %T %Z
            # 6.9|2017-04-05 UTC|https://lists.centos.org/pipermail/centos-announce/2017-April/022351.html

            # https://lists.centos.org/pipermail/centos-announce/2014-July/020393.html
            [[ "${release_no}" == "7" ]] && release_no='7.1406'
            # https://lists.centos.org/pipermail/centos-announce/2007-April/013660.html
            [[ "${release_no}" == "5" ]] && release_no='5.0'
            echo "${release_no}|${release_date}|${release_archive_page}"
        }

        export download_method="${download_method}"
        export -f fnSub_CentOSReleaseDate   # used for command parallel
        sed '' "${centos_announce_archive}" | parallel -k -j 0 fnSub_CentOSReleaseDate >> "${centos_release_date}"
    else
        sed '' "${centos_announce_archive}" | while read -r line; do
            release_no="${line%%|*}"
            release_archive_page="${line#*|}"
            release_date=$(${download_method} "${release_archive_page}" | awk '$0~/<I>/{a=gensub(/.*>(.*)<.*/,"\\1","g",$0);"TZ=\"UTC\" date --date=\""a"\" +\"%F %Z\"" | getline b;print b}')    # %F %T %Z
            # 6.9|2017-04-05 UTC|https://lists.centos.org/pipermail/centos-announce/2017-April/022351.html

            # https://lists.centos.org/pipermail/centos-announce/2014-July/020393.html
            [[ "${release_no}" == "7" ]] && release_no='7.1406'
            # https://lists.centos.org/pipermail/centos-announce/2007-April/013660.html
            [[ "${release_no}" == "5" ]] && release_no='5.0'
            echo "${release_no}|${release_date}|${release_archive_page}" >> "${centos_release_date}"
        done

    fi
    # Step 3 對提取到的數據進行去重，並按Release版本逆序排序，將Step1中獲取的release note地址合併到輸入結果中
    # shellcheck disable=SC2079
    # declare -A rhel7Arr=( ["7.1406"]='7' ["7.1503"]='7.1' ["7.1511"]='7.2' ["7.1611"]='7.3' ["7.1708"]='7.4' ["7.1804"]='7.5' ["7.1810"]='7.6')   # CentOS與RHEL的版本對應關係

    # 7.1406 7
    # 7.1503 7.1
    # 7.1511 7.2
    # 7.1611 7.3
    # 7.1708 7.4
    # 7.1804 7.5
    # 7.1810 7.6
    local l_rhel7_centos7_relation_list=''
    l_rhel7_centos7_relation_list=$(awk -F\| '{!a[$1]++;arr[$1]=$1}END{PROCINFO["sorted_in"]="@ind_num_asc";for (i in arr) {if (i~/^7/) print arr[i]}}' "${centos_release_date}" | awk '{if($1=='7.1406'){print $1,7}else{print $0,7"."NR-1}}')

    # output header setting
    local field_seperator=${field_seperator:-}
    if [[ "${all_list}" -ne 1 ]]; then
        if [[ "${show_details}" -eq 1 ]]; then
            [[ "${markdown_format}" -eq 1 ]] && field_seperator='---|---|---|---\n'
            printf "%s|%s|%s|%s\n${field_seperator}" "Version" "Release Date" "EUS Date" "Release Note"
        else
            [[ "${markdown_format}" -eq 1 ]] && field_seperator='---|---|---\n'
            printf "%s|%s|%s\n${field_seperator}" "Version" "Release Date" "EUS Date"
        fi
    fi

    # Step 3.1 release版本去重、排序
    awk -F\| '{!a[$1]++;arr[$1]=$0}END{PROCINFO["sorted_in"]="@ind_str_desc";for (i in arr) print arr[i]}' "${centos_release_date}" | sort -t\. -k 1,1rn -k 2,2rn -k 3,3rn | while IFS="|" read -r release_no release_date release_archive_page; do
        # Step 3.2 提取對應release版本的release note
        release_note_page=$(awk -F\| '$1=="'"${release_no}"'"{print $2}' "${centos_release_note}")
        if [[ "${release_no}" =~ ^7 ]]; then
            # pattern_str=${rhel7Arr[${release_no}]}
            pattern_str=$(echo "${l_rhel7_centos7_relation_list}" | awk '$1=='"${release_no}"'{print $2}')
        else
            pattern_str="${release_no}"
        fi
        eus_date=$(awk -F\| '$1=="'"${pattern_str}"'"{print $2}' "${rhel_eus_date}")

        # printf "%s|%s|%s\n" "${release_no}" "${release_date}" "${eus_date}"

        if [[ "${show_details}" -eq 1 ]]; then
            if [[ "${markdown_format}" -eq 1 ]]; then
                if [[ -n "${release_note_page}" ]]; then
                    printf "[%s](%s)|%s|%s|[%s](%s)\n" "${release_no}" "${release_archive_page}" "${release_date}" "${eus_date}" "${release_note_page##*/}" "${release_note_page}"
                else
                    printf "[%s](%s)|%s|%s|%s\n" "${release_no}" "${release_archive_page}" "${release_date}" "${eus_date}"
                fi
            else
                if [[ -n "${release_note_page}" ]]; then
                    printf "%s|%s|%s|%s\n" "${release_no}" "${release_date}" "${eus_date}" "${release_note_page}"
                else
                    printf "%s|%s|%s|%s\n" "${release_no}" "${release_date}" "${eus_date}"
                fi
            fi

        else
            if [[ "${all_list}" -ne 1 ]]; then
                if [[ "${markdown_format}" -eq 1 ]]; then
                    printf "[%s](%s)|%s|%s\n" "${release_no}" "${release_archive_page}" "${release_date}" "${eus_date}"
                else
                    printf "%s|%s|%s\n" "${release_no}" "${release_date}" "${eus_date}"
                fi
            else
                # distro name|version|codename|release date|eol date|is eol
                local is_eol=0
                local eol_timestamp=${eol_timestamp:-}
                [[ "${release_no%%.*}" -lt 6 ]] && is_eol=1
                if [[ -n "${eus_date}" ]]; then
                    is_eol=1
                    eol_timestamp=$(date --date="${eus_date}" +'%s')
                    [[ "${eol_timestamp}" -gt ${current_timestamp} ]] && is_eol=0
                else
                    is_eol=1
                    eol_timestamp=''
                    # [[ "${release_no%%.*}" -eq 6 ]] && is_eol=0

                    if [[ "${release_no}" =~ ^6 ]]; then
                        # is_eol=1
                        local l_version_major=''
                        local l_version_minor=''
                        l_version_major="${release_no%%.*}"
                        l_version_minor="${release_no##*.}"
                        ((l_version_minor++))
                        # 6.10|2018-07-03 UTC|https://lists.centos.org/pipermail/centos-announce/2018-July/022925.html
                        eus_date=$(awk -F\| 'match($1,/^'"${l_version_major}.${l_version_minor}"'$/){print $2}' "${centos_release_date}")
                        eus_date="${eus_date%% *}"

                        # https://access.redhat.com/support/policy/updates/errata
                        # For 6.x End of Maintenance Support or Maintenance Support 2 (Product retirement)  November 30, 2020
                        [[ -n "${eus_date}" ]] || eus_date='2020-11-30'
                        eol_timestamp=$(date --date="${eus_date}" +'%s')
                        [[ "${eol_timestamp}" -gt ${current_timestamp} ]] && is_eol=0
                    fi


                fi

                echo "centos|${release_no}||${release_date%% *}|${eus_date}|${eol_timestamp}|${is_eol}"
            fi
        fi
    done

    [[ -f "${centos_release_note}" ]] && rm -f "${centos_release_note}"
    unset centos_release_note
    [[ -f "${centos_announce_archive}" ]] && rm -f "${centos_announce_archive}"
    unset centos_announce_archive
    [[ -f "${centos_release_date}" ]] && rm -f "${centos_release_date}"
    unset centos_release_date
    [[ -f "${rhel_eus_date}" ]] && rm -f "${rhel_eus_date}"
    unset rhel_eus_date

    fnBase_NotifySendStatement 'Life Cycle Info' 'CentOS'
}

#########  3-3. Debian  #########
# Debian Long Term Support
# https://wiki.debian.org/LTS
# DebianReleases
# https://wiki.debian.org/DebianReleases
fn_DebianDateFormat(){
    local item="${1:-}"

    if [[ -n "${item}" ]]; then
        local item_format=${item_format:-}
        if [[ "${#item}" -gt 4 ]]; then
            item_format=$(date --date="${item}" +"%F" 2>/dev/null)
        fi
        [[ -n "${item_format}" ]] && item="${item_format}"
        echo "${item}"
    fi
}

fn_LifeCycleForDebian(){
    # local official_site='https://www.debian.org'
    local l_debian_release_note='https://wiki.debian.org/DebianReleases'
    local l_debian_release_page='https://www.debian.org/releases/'
    local l_debian_wiki_site='https://wiki.debian.org'
    # https://wiki.debian.org/LTS

    # local debian_lts_note="${l_debian_wiki_site}/LTS"
    # debian_lts_date=$(mktemp -t "${mktemp_format}")
    # ${download_method} "${debian_lts_note}" | sed -r -n '/schedule/,/Legend/{/amd64/d;/background-color/!d;s@<tr>@@g;s@[[:space:]]*<[^>]+>[[:space:]]*@@g;s@^.*(until|to)[[:space:]]*@@g;s@.*“([^”]+)”.*@\1@g;p}' | sed -r -n 'N;s@\n@|@gp' > "${debian_lts_date}"

    debian_release_info_list=$(mktemp -t "${mktemp_format}")
    # shellcheck disable=SC2016
    ${download_method} "${l_debian_release_note}" | sed -r -n '/End of life date/,/point releases/{/<strong>/d;/td/!d;s@ / <a@|<a@g;s@ / @||@g;s@href="([^"]+)">([^<]+)<@>\2|\1<@g;s@[[:space:]]*<tr>[[:space:]]*@---@g;s@[[:space:]]*(<[^>]+>)[[:space:]]*@\1@g;s@<p[^>]+>@@g;/line-56/,${s@<td><\/td>@||@g};s@<td>-<\/td>@||@g;/anchor/{s@^(---).*>([^<]*)<\/td>$@\1\n\2|@g};s@<\/td>@|@g;s@[[:space:]]*(<[^>]+>)[[:space:]]*@@g;s@~@@g;s@[[:space:]]*\([^\)]*\)[[:space:]]*@@g;s@([[:digit:]]{,2})(st|nd|th)( [[:digit:]]{4})@\1\3@g;p}' | sed -r '1d;:a;N;$!ba;s@\n@@g;s@---@\n@g;' > "${debian_release_info_list}"

    # curl -fsL https://wiki.debian.org/DebianStretch | sed -r -n '/Release and updates<\/h3>/{N;p}' | sed -r -n 's@<li>@&\n@g;p' | sed -r -n '/href=/!d;/(Initial release|Updated)/!d;s@<p[^>]*>@@g;s@<span.*$@@g;s@\(?<a.*href="([^"]+)".*$@|\1@g;s@^([^:]+):[^[:digit:]]+([[:digit:].]+)[^\|]+\|(.*)$@\2|\1|\3@g;p' | sort -t\. -k 1,1n -k 2,2rn
    # 9.9|2019-04-27|https://www.debian.org/News/2019/20190427
    # 9.8|2019-02-16|https://www.debian.org/News/2019/20190216
    # ...
    # 9.1|2017-07-22|https://www.debian.org/News/2017/20170722
    # 9.0|2017-06-17|https://lists.debian.org/debian-announce/2017/msg00003.html

    # ${download_method} "${l_debian_release_page}" | sed -r -n '/Index of releases/,/<\/ul>/{p}' | awk '$0~/stable release/{print gensub(/.*href="([^"]+)">[^[:digit:]]+([[:digit:].]+) .*$/,"'"${l_debian_release_page}"'\\1 \\2","g",a)};{a=$0}' | awk '$2>6' | while IFS=" " read -r s_link s_version; do
    #     # https://www.debian.org/releases/buster/ 10
    #     # https://www.debian.org/releases/stretch/ 9
    #
    #     local extract_release_info=''
    #     extract_release_info=$(${download_method} "${s_link}" | sed -r -n '/Release Information<\/h1>/,/included many major/{/h1/d;/include/d;s@<[^>]+>@@g;s@(initially|on) @@g;p}' | sed -r -n ':a;N;$!ba;s@\n@ @g;s@[[:space:]]*was released[[:space:]]*@|@g;s@\. Debian[[:space:]]*@|@g;s@Debian[[:space:]]*@@g;s@([[:digit:]])[[:alpha:]]+,@\1,@g;s@\.$@@g;p')
    #     # 9.3|December 9, 2017|9.0|June 17, 2017
    #     # 8.10|December 9, 2017|8.0|April 26, 2015
    #
    #     if [[ -z "${extract_release_info}" ]]; then
    #         continue
    #     else
    #         local new_release_version
    #         new_release_version=$(echo "${extract_release_info}" | cut -d\| -f1)
    #         local new_release_date
    #         new_release_date=$(echo "${extract_release_info}" | cut -d\| -f2)
    #         local major_release_version
    #         major_release_version=$(echo "${extract_release_info}" | cut -d\| -f3)
    #
    #         local major_version_release_info
    #         major_version_release_info=$(sed -r -n '/^'"${s_version}"'\|/{p}' "${debian_release_info_list}")
    #         local minor_version_release_info
    #         minor_version_release_info=$(echo "${major_version_release_info}" | awk -F\| 'BEGIN{OFS="|"}{$1="'"${new_release_version}"'";$4="'"${new_release_date}"'";print}')
    #         sed -r -i '/^'"${s_version}"'\|/i '"${minor_version_release_info}"'' "${debian_release_info_list}"
    #         sed -r -i '/^'"${s_version}"'\|/{s@^'"${s_version}"'\|@'"${major_release_version}"'\|@g;}' "${debian_release_info_list}"
    #     fi
    # done

    debian_release_info_list_new=$(mktemp -t "${mktemp_format}")

    while IFS="|" read -r release_version codename codename_url releases_date release_url eol_date eol_url lts_date lts_url; do
        # omit empth release_version
        [[ -z "${release_version}" || -z "${releases_date}" ]] && continue
        [[ -n "${codename_url}" && "${codename_url}" =~ ^/ ]] && codename_url="${l_debian_wiki_site}${codename_url}"
        version_major_no=$(echo "${release_version}" | cut -d\. -f1)

        if [[ "${version_major_no}" -ge 6 ]]; then
            # from latest to 5.0 Lenny, older use 'Debian/Etch Life cycle'
            [[ -z "${eol_date}" ]] && eol_date="${release_url}"
            $download_method "${codename_url}" | sed -r -n '/Release and updates<\/h3>/{N;p}' | sed -r -n 's@<li>@&\n@g;p' | sed -r -n '/href=/!d;/(Initial release|Updated)/!d;s@<p[^>]*>@@g;s@<span.*$@@g;s@\(?<a.*href="([^"]+)".*$@|\1@g;s@^([^:]+):[^[:digit:]]+([[:digit:].]+)[^\|]+\|(.*)$@\2|\1|\3@g;p' | sort -t\. -k 1,1n -k 2,2rn -k 3,3rn | awk -F\| -v eol="${eol_date}" '{if(NR==1){k=eol};printf("%s|'${codename}'|'${codename_url}'|%s|%s|%s\n",$1,$2,$3,k);k=$2}' >> "${debian_release_info_list_new}"
            # release_version codename codename_url releases_date release_url eol_date
            # awk -F\| -v eol="${eol_date}" '{if(NR==1){k=eol};print $1,$2,$3,k;k=$2}'
            # 10.0 2019-07-06 https://lists.debian.org/debian-announce/2019/msg00003.html 2022
        else
            echo "${release_version}|${codename}|${codename_url}|${releases_date}|${release_url}|${eol_date}|${eol_url}|${lts_date}|${lts_url}"  >> "${debian_release_info_list_new}"
        fi

    done < "${debian_release_info_list}"

    if [[ "${all_list}" -ne 1 ]]; then
        if [[ "${markdown_format}" -eq 1 ]]; then
            if [[ "${show_details}" -eq 1 ]]; then
                printf "%s|%s|%s|%s|%s\n---|---|---|---|---\n" "Version" "CodeName" "Release Date" "EOL Date" "LTS Date"
            else
                printf "%s|%s|%s|%s\n---|---|---|---\n" "Version" "CodeName" "Release Date" "EOL Date"
            fi
        else
            if [[ "${show_details}" -eq 1 ]]; then
                printf "%s|%s|%s|%s|%s\n" "Version" "CodeName" "Release Date" "EOL Date" "LTS Date"
            else
                printf "%s|%s|%s|%s\n" "Version" "CodeName" "Release Date" "EOL Date"
            fi
        fi
    fi

    while IFS="|" read -r release_version codename codename_url releases_date release_url eol_date eol_url lts_date lts_url; do
        # 8|Jessie|/DebianJessie|April 25 2015|https://www.debian.org/News/2015/20150426|June 6 2018|https://www.debian.org/security/faq#lifespan|June 6 2020|/LTS|

        # omit empth release_version
        [[ -z "${release_version}" ]] && continue

        [[ -n "${codename_url}" && "${codename_url}" =~ ^/ ]] && codename_url="${l_debian_wiki_site}${codename_url}"

        [[ -n "${releases_date}" ]] &&  releases_date=$(fn_DebianDateFormat "${releases_date}")
        [[ -n "${eol_date}" ]] &&  eol_date=$(fn_DebianDateFormat "${eol_date}")
        [[ -n "${lts_date}" ]] &&  lts_date=$(fn_DebianDateFormat "${lts_date}")

        [[ -n "${lts_url}" && "${lts_url}" =~ ^/ ]] && lts_url="${l_debian_wiki_site}${lts_url}"

        if [[ "${show_details}" -eq 1 ]]; then
            if [[ "${markdown_format}" -eq 1 ]]; then
                printf "%s|[%s](%s)|[%s](%s)|[%s](%s)|[%s](%s)\n" "${release_version}" "${codename}" "${codename_url}" "${releases_date}" "${release_url}" "${eol_date}" "${eol_url}" "${lts_date}" "${lts_url}" | sed -r 's@\[\]\(\)@@g;s@\[([^]]+)\]\(\)@\1@g;/^[[:digit:]]/!d'
            else
                printf "%s|%s|%s|%s|%s\n" "${release_version}" "${codename}" "${releases_date}" "${eol_date}" "${lts_date}"
            fi    # end if markdown_format

        else
            local l_eol_date="${eol_date}"
            [[ -n "${lts_date}" ]] && l_eol_date="${lts_date}"

            if [[ "${all_list}" -ne 1 ]]; then
                if [[ "${markdown_format}" -eq 1 ]]; then
                    printf "%s|[%s](%s)|[%s](%s)|[%s](%s)\n" "${release_version}" "${codename}" "${codename_url}" "${releases_date}" "${release_url}" "${l_eol_date}" "${eol_url}"
                else
                    printf "%s|%s|%s|%s\n" "${release_version}" "${codename}" "${releases_date}" "${l_eol_date}"
                fi
            else
                # distro name|version|codename|release date|eol date|is eol
                local is_eol=1
                local eol_timestamp=${eol_timestamp:-}
                [[ "${release_version%%.*}" -lt 8 ]] && is_eol=1

                if [[ -n "${l_eol_date}" ]]; then
                    if [[ "${l_eol_date}" =~ ^approx ]]; then
                        eol_timestamp=$(date --date="01 Jan ${l_eol_date##* }" +'%s')
                        [[ "${eol_timestamp}" -gt ${current_timestamp} ]] && is_eol=0
                    elif [[ "${l_eol_date}" =~ ^[a-zA-Z] ]]; then
                        eol_timestamp=$(date --date="01 ${l_eol_date} next month last day" +'%s')
                        [[ "${eol_timestamp}" -gt ${current_timestamp} ]] && is_eol=0
                    else
                        eol_timestamp=$(date --date="${l_eol_date}" +'%s')
                        [[ "${eol_timestamp}" -gt ${current_timestamp} ]] && is_eol=0
                    fi
                else
                    eol_timestamp=''
                fi

                [[ -n "${releases_date}" ]] && echo "debian|${release_version}|${codename,,}|${releases_date}|${l_eol_date}|${eol_timestamp}|${is_eol}"
            fi

        fi    # end if show_details

    # done < "${debian_release_info_list}"
    done < "${debian_release_info_list_new}"
    fnBase_NotifySendStatement 'Life Cycle Info' 'Debian'
}


#########  3-4. Ubuntu  #########
# List of releases
# https://wiki.ubuntu.com/Releases
# https://lists.ubuntu.com/archives/ubuntu-announce/
# https://ubuntu.com/about/release-cycle

fn_LifeCycleForUbuntu(){
    local l_ubuntu_release_note='https://wiki.ubuntu.com/Releases'
    ubuntu_wiki_site='https://wiki.ubuntu.com'

    ubuntu_release_info=$(mktemp -t "${mktemp_format}")
    $download_method "${l_ubuntu_release_note}" > "${ubuntu_release_info}"

    if [[ "${all_list}" -ne 1 ]]; then
        if [[ "${markdown_format}" -eq 1 ]]; then
            if [[ "${show_details}" -eq 1 ]]; then
                printf "%s|%s|%s|%s|%s\n---|---|---|---|---\n" "Version" "CodeName" "Release Date" "EOL Date" "Doc"
            else
                printf "%s|%s|%s|%s\n---|---|---|---\n" "Version" "CodeName" "Release Date" "EOL Date"
            fi
        else
            if [[ "${show_details}" -eq 1 ]]; then
                printf "%s|%s|%s|%s|%s\n" "Version" "CodeName" "Release Date" "EOL Date" "Doc"
            else
                printf "%s|%s|%s|%s\n" "Version" "CodeName" "Release Date" "EOL Date"
            fi
        fi
    fi

    # Step 1. Current
    # Version|Code name|Docs|Release|End of Standard Support|End of Life
    sed -r -n '/Current/,/Future/{/(table|h3|h2)/d;s@<(td|p|span|strong)[[:space:]]*[^>]*>@@g;s@<\/(span|a|strong)>@@g;s@class="[^"]*" @@;s@<a href="([^"]*)">@\1~@g;s@<tr>[[:space:]]*@@g;s@[[:space:]]*<\/td>@|@g;s@^[[:space:]]*@@g;p}' "${ubuntu_release_info}" | awk '{if($0!~/<\/tr>/){ORS="";print $0}else{printf "\n"}}' | sed -r '/^Ubuntu/!d' | while IFS="|" read -r release_version codename_info doc_info release_date_info eol_info eol_info_extra;do
        # Note: eol_info == End of Standard Support  /  eol_info_extra == End of Life
        release_version=${release_version//Ubuntu /}
        codename=${codename_info##*~}
        codename=$(echo "${codename}" | sed -r -n 's@^([[:upper:]]*[[:lower:]]*).*@\1@g;p')
        codename_url=${ubuntu_wiki_site}${codename_info%%~*}
        doc_type=${doc_info##*~}
        doc_url=${ubuntu_wiki_site}${doc_info%%~*}
        release_date=$(date --date="${release_date_info##*~}" +"%F")
        release_date_url=${release_date_info%%~*}

        local eol_date=${eol_date:-}
        local eol_url=${eol_url:-}

        # remove ','   'January, 2020' == > 'January 2020'
        [[ -n "${eol_info}" ]] && eol_info="${eol_info//,/}"

        if [[ -z "${eol_info}" ]]; then
            eol_date=''
            eol_url=''
        elif [[ -n "${eol_info}" && "${eol_info}" =~ \~ ]]; then
            eol_date=${eol_info##*~}
            eol_url=${eol_info%%~*}
        else
            if [[ "${eol_info}" =~ [0-9]{4} ]]; then
                eol_date=${eol_info}
            fi
        fi

        local eol_date_temp=${eol_date_temp:-}
        if [[ -n "${eol_date}" ]]; then
            eol_date=$(echo "${eol_date}" | sed -r 's@[[:alpha:]]*,@,@g')
            eol_date_temp=$(date --date="${eol_date//HWE /}" +"%F" 2> /dev/null)
            [[ -n "${eol_date_temp}" ]] && eol_date="${eol_date_temp}"
        fi

        if [[ "${show_details}" -eq 1 ]]; then
            if [[ "${markdown_format}" -eq 1 ]]; then
                printf "%s|[%s](%s)|[%s](%s)|[%s](%s)|[%s](%s)\n" "${release_version}" "${codename}" "${codename_url}" "${release_date}" "${release_date_url}" "${eol_date}" "${eol_url}" "${doc_type}" "${doc_url}" | sed -r 's@\[\]\(\)@@g;s@\[([^]]+)\]\(\)@\1@g;'
            else
                printf "%s|%s|%s|%s|%s\n" "${release_version}" "${codename}" "${release_date}" "${eol_date}" | sed -r 's@\[\]\(\)@@g;'
            fi
        else

            if [[ "${all_list}" -ne 1 ]]; then
                if [[ "${markdown_format}" -eq 1 ]]; then
                    printf "%s|[%s](%s)|[%s](%s)|[%s](%s)\n" "${release_version}" "${codename}" "${codename_url}" "${release_date}" "${release_date_url}" "${eol_date}" "${eol_url}" | sed -r 's@\[\]\(\)@@g;s@\[([^]]+)\]\(\)@\1@g;'
                else
                    printf "%s|%s|%s|%s\n" "${release_version}" "${codename}" "${release_date}" "${eol_date}" | sed -r 's@\[\]\(\)@@g;s@\[([^]]+)\]\(\)@\1@g;s@<br>$@@g'
                fi

            else
                # distro name|version|codename|release date|eol date|is eol
                codename="${codename%% *}"
                eol_date="${eol_date//HWE /}"
                local is_eol=0
                local eol_timestamp=${eol_timestamp:-}

                [[ "${release_version%%.*}" -lt 13 ]] && is_eol=1

                if [[ -n "${eol_date}" ]]; then
                    is_eol=1
                    if [[ "${eol_date}" =~ ^[a-zA-Z] ]]; then
                        eol_timestamp=$(date --date="01 ${eol_date} next month last day" +'%s')
                        [[ "${eol_timestamp}" -gt ${current_timestamp} ]] && is_eol=0
                    else
                        eol_timestamp=$(date --date="${eol_date}" +'%s')
                        [[ "${eol_timestamp}" -gt ${current_timestamp} ]] && is_eol=0
                    fi
                else
                    eol_timestamp=''
                fi

                # ---------------------
                # if [[ "${release_version%%.*}" -lt 13 ]]; then
                #     is_eol=1
                # else
                #     if [[ -n "${eol_date}" ]]; then
                #         is_eol=1
                #         if [[ "${eol_date}" =~ ^[a-zA-Z] ]]; then
                #             eol_timestamp=$(date --date="01 ${eol_date} next month last day" +'%s')
                #             [[ "${eol_timestamp}" -gt ${current_timestamp} ]] && is_eol=0
                #         else
                #             eol_timestamp=$(date --date="${eol_date}" +'%s')
                #             [[ "${eol_timestamp}" -gt ${current_timestamp} ]] && is_eol=0
                #         fi
                #     fi
                # fi

                echo "ubuntu|${release_version%% *}|${codename,,}|${release_date}|${eol_date}|${eol_timestamp}|${is_eol}"
            fi

        fi

    done

    # Step 2. End of Life
    # Version|Code name|Docs|Release|End of Life
    sed -r -n '/End of Life<\/h3>/,/Management of releases/{/(table|h3|h2)/d;s@<(td|p|span|strong)[[:space:]]*[^>]*>@@g;s@<\/(span|a|strong)>@@g;s@class="[^"]*" @@;s@<a href="([^"]*)">@\1~@g;s@(^<tr>[[:space:]]*)([^<]*).*@\2<\/td>@g;s@[[:space:]]*<\/td>@|@g;s@^[[:space:]]*@@g;s@<\/strong>@@g;s@ / @=@g;p}' "${ubuntu_release_info}" | awk '{if($0!~/<\/tr>/){ORS="";print $0}else{printf "\n"}}' | sed -r '/^Ubuntu/!d' | while IFS="|" read -r release_version codename_info doc_info release_date_info eol_info;do
        release_version=${release_version//Ubuntu /}
        codename=${codename_info##*~}
        codename=$(echo "${codename}" | sed -r -n 's@^([[:upper:]]*[[:lower:]]*).*@\1@g;p')
        codename_url=${ubuntu_wiki_site}${codename_info%%~*}
        if [[ -z "${doc_info}" ]]; then
            doc_left_type=''
            doc_left_url=''
            doc_right_type=''
            doc_right_url=''
        elif [[ -n "${doc_info}" && "${doc_info}" =~ = ]]; then
            doc_left_info=${doc_info%%=*}
            doc_right_info=${doc_info##*=}

            if [[ -n "${doc_left_info}" && "${doc_left_info}" =~ \~ ]]; then
                doc_left_type=${doc_left_info##*~}
                doc_left_url=${ubuntu_wiki_site}${doc_left_info%%~*}
            fi

            if [[ -n "${doc_right_info}" && "${doc_right_info}" =~ \~ ]]; then
                doc_right_type=${doc_right_info##*~}
                doc_right_url=${ubuntu_wiki_site}${doc_right_info%%~*}
            fi
        elif [[ -n "${doc_info}" && "${doc_info}" =~ \~ ]]; then
            doc_left_type=${doc_info##*~}
            doc_left_url=${ubuntu_wiki_site}${doc_info%%~*}
            doc_right_type=''
            doc_right_url=''
        fi

        release_date=$(date --date="${release_date_info##*~}" +"%F")
        release_date_url=${release_date_info%%~*}

        if [[ -z "${eol_info}" ]]; then
            eol_left_date=''
            eol_left_url=''
            eol_right_date=''
            eol_right_url=''
        elif [[ -n "${eol_info}" && "${eol_info}" =~ \<br\> ]]; then
            eol_left_info=${eol_info%%'<br>'*}
            eol_right_info=${eol_info##*'<br>'}

            if [[ -n "${eol_left_info}" && "${eol_left_info}" =~ \~ ]]; then
                eol_left_date=${eol_left_info##*~}
                eol_left_url=${eol_left_info%%~*}
            fi

            if [[ -n "${eol_right_info}" && "${eol_right_info}" =~ \~ ]]; then
                eol_right_date=${eol_right_info##*~}
                eol_right_url=${eol_right_info%%~*}
            fi
        elif [[ -n "${eol_info}" && "${eol_info}" =~ \~ ]]; then
            eol_left_date=${eol_info##*~}
            eol_left_url=${eol_info%%~*}
            eol_right_date=''
            eol_right_url=''
        fi

        local eol_left_date_temp=${eol_left_date_temp:-}
        if [[ -n "${eol_left_date}" ]]; then
            eol_left_date=$(echo "${eol_left_date}" | sed -r 's@[[:alpha:]]*,@,@g')
            eol_left_date_temp=$(date --date="${eol_left_date//HWE /}" +"%F" 2> /dev/null)
            if [[ -n "${eol_left_date_temp}" ]]; then
                if [[ "${eol_left_date}" =~ HWE ]]; then
                    eol_left_date="HWE ${eol_left_date_temp}"
                else
                    eol_left_date="${eol_left_date_temp}"
                fi
            fi
        fi

        local eol_right_date_temp=${eol_right_date_temp:-}
        if [[ -n "${eol_right_date}" ]]; then
            eol_right_date=$(echo "${eol_right_date}" | sed -r 's@[[:alpha:]]*,@,@g')
            eol_right_date_temp=$(date --date="${eol_right_date//HWE /}" +"%F" 2> /dev/null)
            if [[ -n "${eol_right_date_temp}" ]]; then
                if [[ "${eol_right_date}" =~ HWE ]]; then
                    eol_right_date="HWE ${eol_right_date_temp}"
                else
                    eol_right_date="${eol_right_date_temp}"
                fi
            fi
        fi

        # Version|Code name|Release date|End of Life date|Docs
        if [[ "${show_details}" -eq 1 ]]; then
            if [[ "${markdown_format}" -eq 1 ]]; then
                printf "%s|[%s](%s)|[%s](%s)|[%s](%s)<br>[%s](%s)|[%s](%s) / [%s](%s)\n" "${release_version}" "${codename}" "${codename_url}" "${release_date}" "${release_date_url}" "${eol_left_date}" "${eol_left_url}" "${eol_right_date}" "${eol_right_url}" "${doc_left_type}" "${doc_left_url}" "${doc_right_type}" "${doc_right_url}" | sed -r 's@\[\]\(\)@@g;s@\[([^]]+)\]\(\)@\1@g;s@[[:space:]]*\/[[:space:]]*\|@\|@g;s@[[:space:]]*\/[[:space:]]*$@@g'
            else
                printf "%s|%s|%s|%s<br>%s\n" "${release_version}" "${codename}" "${release_date}" "${eol_left_date}" "${eol_right_date}" | sed -r 's@\[\]\(\)@@g;s@[[:space:]]*\/[[:space:]]*\|@\|@g;s@<br>$@@g'
            fi
        else
            if [[ "${all_list}" -ne 1 ]]; then
                if [[ "${markdown_format}" -eq 1 ]]; then
                    printf "%s|[%s](%s)|[%s](%s)|[%s](%s)<br>[%s](%s)\n" "${release_version}" "${codename}" "${codename_url}" "${release_date}" "${release_date_url}" "${eol_left_date}" "${eol_left_url}" "${eol_right_date}" "${eol_right_url}" | sed -r 's@\[\]\(\)@@g;s@\[([^]]+)\]\(\)@\1@g;s@[[:space:]]*\/[[:space:]]*\|@\|@g;s@[[:space:]]*\/[[:space:]]*$@@g'
                else
                    printf "%s|%s|%s|%s<br>%s\n" "${release_version}" "${codename}" "${release_date}" "${eol_left_date}" "${eol_right_date}" | sed -r 's@\[\]\(\)@@g;s@[[:space:]]*\/[[:space:]]*\|@\|@g;s@<br>$@@g'
                fi
            else
                # distro name|version|codename|release date|eol date|is eol
                codename="${codename%% *}"
                local l_eol_left_date="${eol_left_date//HWE /}"
                local is_eol=0
                local eol_timestamp=${eol_timestamp:-}

                [[ "${release_version%%.*}" -lt 13 ]] && is_eol=1

                if [[ -n "${l_eol_left_date}" ]]; then
                    is_eol=1
                    if [[ "${l_eol_left_date}" =~ ^[a-zA-Z] ]]; then
                        eol_timestamp=$(date --date="01 ${l_eol_left_date} next month last day" +'%s')
                        [[ "${eol_timestamp}" -gt ${current_timestamp} ]] && is_eol=0
                    else
                        eol_timestamp=$(date --date="${l_eol_left_date}" +'%s')
                        [[ "${eol_timestamp}" -gt ${current_timestamp} ]] && is_eol=0
                    fi
                else
                    eol_timestamp=''
                fi

                # ------------------------
                # if [[ "${release_version%%.*}" -lt 13 ]]; then
                #     is_eol=1
                # else
                #     if [[ -n "${l_eol_left_date}" ]]; then
                #         is_eol=1
                #         if [[ "${l_eol_left_date}" =~ ^[a-zA-Z] ]]; then
                #             eol_timestamp=$(date --date="01 ${l_eol_left_date} next month last day" +'%s')
                #             [[ "${eol_timestamp}" -gt ${current_timestamp} ]] && is_eol=0
                #         else
                #             eol_timestamp=$(date --date="${l_eol_left_date}" +'%s')
                #             [[ "${eol_timestamp}" -gt ${current_timestamp} ]] && is_eol=0
                #         fi
                #     fi
                # fi

                echo "ubuntu|${release_version%% *}|${codename,,}|${release_date}|${eol_left_date}|${eol_timestamp}|${is_eol}"

            fi

        fi

    done

    [[ -f "${ubuntu_release_info}" ]] && rm -f "${ubuntu_release_info}"
    unset ubuntu_release_info

    fnBase_NotifySendStatement 'Life Cycle Info' 'Ubuntu'
}


#########  4. Executing Process  #########
fn_InitializationCheck

if [[ "${all_list}" -ne 1 ]]; then
    fn_LinuxDistributionSelection
    fn_LifeCycleFor"${distribution_choose}"
else
    fn_LifeCycleForRHEL
    fn_LifeCycleForCentOS
    fn_LifeCycleForDebian
    fn_LifeCycleForUbuntu
fi


#########  5. EXIT Singal Processing  #########
# trap "commands" EXIT # execute command when exit from shell
fn_TrapEXIT(){
    fnBase_UnsetVars
    unset distribution_choose
    unset show_details
    unset markdown_format
    unset proxy_server_specify
}

trap fn_TrapEXIT EXIT

# Script End
