#!/usr/bin/env bash
# shellcheck disable=SC2034
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails
# IFS=$'\n\t' #IFS  Internal Field Separator

# Target: Detecting Hardware & System Info From Host Running GNU/Linux (RHEL/CentOS/Fedora/Debian/Ubuntu/OpenSUSE and variants)
# Developer: MaxdSre

# Change Log:
# - Oct 02, 2019 08:57 Wed ET - change function name format, move to archive directory Common/Archive
# - Feb 13, 2018 09:46 Feb ET ~ Feb 22, 2018 17:24 ET


#########  0-1. Singal Setting  #########
script_self_name=$(basename "$(readlink -nf "$0")")
mktemp_format="Temp-${script_self_name%%.*}_XXXXXXXXX"
# trap '' HUP	#overlook SIGHUP when internet interrupted or terminal shell closed
# trap '' INT   #overlook SIGINT when enter Ctrl+C, QUIT is triggered by Ctrl+\
trap fn_TrapCleanUp INT QUIT

fn_TrapCleanUp(){
    rm -rf /tmp/"${mktemp_format%%_*}"* 2>/dev/null
    printf "Detect $(tput setaf 1)%s$(tput sgr0) or $(tput setaf 1)%s$(tput sgr0), begin to exit shell\n" "CTRL+C" "CTRL+\\"
    exit
}


#########  0-2. Variables Setting  #########
umask 022    # temporarily change umask value to 022
readonly term_cols=$(tput cols)
# term_lines=$(tput lines)
readonly c_bold="$(tput bold)"
readonly c_normal="$(tput sgr0)"     # c_normal='\e[0m'
# black 0, red 1, green 2, yellow 3, blue 4, magenta 5, cyan 6, gray 7
readonly c_red="$(tput setaf 1)"     # c_red='\e[31;1m'
readonly c_green="$(tput setaf 2)"    # c_blue='\e[32m'
readonly c_yellow="$(tput setaf 3)"    # c_blue='\e[33m'
readonly c_blue="$(tput setaf 4)"    # c_blue='\e[34m'

list_all_type=${list_all_type:-0}
type_specify=${type_specify:-'product'}


#########  1-1 getopts Operation  #########
fn_HelpInfo(){
cat <<EOF
${c_blue}Usage:
    script [options] ...
    script | sudo bash -s -- [options] ...

Post Installation Configuring RHEL/CentOS/Fedora/Amazon Linux/Debian/Ubuntu/SLES/OpenSUSE!
This script requires superuser privileges (eg. root, su).

[available option]
    -h    --help, show help info
    -a    --list all system info detected
    -t typename    --specify type (product|bios|baseboard|chassis|memory|cache|port|slot|point|battery|fan|temperature|disk), default is 'product'
${c_normal}
EOF
exit
}

while getopts "at:h" option "$@"; do
    case "$option" in
        a ) list_all_type=1 ;;
        t ) type_specify="$OPTARG" ;;
        h|\? ) fn_HelpInfo ;;
    esac
done

#########  1-2 Initialization Prepatation  #########
fn_CentralOutput(){
    local item="${1:-}"
    local val="${2:-}"
    if [[ -n "${item}" ]]; then
        local item_width
        item_width="${#item}"
        local l_term_cols=${l_term_cols:-}
        [[ -z "${l_term_cols}" ]] && l_term_cols=$(tput cols)

        local left_space
        left_space=$(( (l_term_cols - item_width) / 2 ))
        local printf_val
        printf_val=$((left_space + item_width))

        # '#' stantd for title line
        if [[ "${val}" == '#' ]]; then
            printf "%${printf_val}s\n" "${item}"
        else
            printf "%$((printf_val - item_width / 2))s ${c_bold}${c_red}%s${c_normal}\n" "${item}:" "${val}"
        fi    # end if val
    fi
}

fn_CentralOutputTitle(){
    local l_item="${1:-}"
    local l_level="${2:-0}"

    echo ''
    [[ "${l_level}" -eq 0 ]] && fn_CentralOutput '==========================================' '#'
    if [[ -n "${l_item}" ]]; then
        fn_CentralOutput "${l_item}" '#'
        fn_CentralOutput '==========================================' '#'
    fi
    echo ''
}

fn_ExitStatement(){
    local str="$*"
    [[ -n "$str" ]] && echo -e "${str}\n"
    rm -rf /tmp/"${mktemp_format%%_*}"* 2>/dev/null
    exit
}

fn_CommandExistCheck(){
    # $? -- 0 is find, 1 is not find
    local name="$1"
    if [[ -n "$name" ]]; then
        local executing_path=${executing_path:-}
        executing_path=$(which "$name" 2> /dev/null || command -v "$name" 2> /dev/null)
        [[ -n "${executing_path}" ]] && return 0 || return 1
    else
        return 1
    fi
}

fn_InitializationCheck(){
    # 1 - Check root or sudo privilege
    [[ "$UID" -ne 0 ]] && fn_ExitStatement "${c_red}Sorry${c_normal}: this script requires superuser privileges (eg. root, su)."

    # 2 - specified for RHEL/Debian/SLES/Amazon Linux
    [[ -s '/etc/os-release' || -s '/etc/redhat-release' || -s '/etc/debian_version' || -s '/etc/SuSE-release' ]] || fn_ExitStatement "${c_red}Sorry${c_normal}: this script just support RHEL/CentOS/Debian/Ubuntu/OpenSUSE derivates!"

    # 3 - bash version check  ${BASH_VERSINFO[@]} ${BASH_VERSION}
    # bash --version | sed -r -n '1s@[^[:digit:]]*([[:digit:].]*).*@\1@p'
    [[ "${BASH_VERSINFO[0]}" -lt 4 ]] && fn_ExitStatement "${c_red}Sorry${c_normal}: this script need BASH version 4+, your current version is ${c_blue}${BASH_VERSION%%-*}${c_normal}."

    fn_CommandExistCheck 'curl' || fn_ExitStatement "${c_red}Error${c_normal}, No ${c_blue}curl${c_normal} command found!"

    fn_CommandExistCheck 'gawk' || fn_ExitStatement "${c_red}Error${c_normal}, No ${c_blue}gawk${c_normal} command found!"

    fn_CommandExistCheck 'lspci' || fn_ExitStatement "${c_red}Error${c_normal}, No ${c_blue}lspci${c_normal} command found!"

    # dmidecode - DMI table decoder
    # fn_CommandExistCheck 'dmidecode' || fn_ExitStatement "${c_red}dmidecode${c_normal}, No ${c_blue}dmidecode${c_normal} command found!"
}

fn_DmidecodeValExtraction(){
    local l_type_name=${1:-}  # dmidecode -t system
    local l_type_item=${2:-}  # Product Name
    local l_dmi_id_keyword=${3:-}    # /sys/devices/virtual/dmi/id/product_name
    local l_str_keyword=${4:-}  # dmidecode -s system-product-name
    local l_return_val=${l_return_val:-}

    local l_dmidecode_exist=${l_dmidecode_exist:-0}
    fn_CommandExistCheck 'dmidecode' && l_dmidecode_exist=1

    if [[ -n "${l_type_name}" && -n "${l_type_item}" ]]; then
        if [[ -n "${l_dmi_id_keyword}" ]]; then
            local l_dmi_id_dir=${l_dmi_id_dir:-'/sys/devices/virtual/dmi/id'}
            [[ -s "${l_dmi_id_dir}/${l_dmi_id_keyword}" ]] && l_return_val=$(cat "${l_dmi_id_dir}/${l_dmi_id_keyword}")
        elif [[ -n "${l_str_keyword}" ]]; then
            [[ "${l_dmidecode_exist}" -eq 1 ]] && l_return_val=$(dmidecode -s "${l_str_keyword}" 2> /dev/null)
        fi

        if [[ -z "${l_return_val}" ]]; then
            [[ "${l_dmidecode_exist}" -eq 1 ]] && l_return_val=$(dmidecode -q -t "${l_type_name}" 2> /dev/null | sed -r -n '/^[[:space:]]*'"${l_type_item}"':/{s@^[^:]*:[[:space:]]+(.*)$@\1@g;s@^[^=]+=([^[:punct:]]+).*@\1@g;p}')
        fi

    fi

    # trim leading and trailing whitespaces
    # echo $(echo "${l_return_val}")
    l_return_val=$(echo "${l_return_val}" | xargs 2> /dev/null)
    [[ "${l_return_val}" == 'Not Specified' ]] && l_return_val=''
    echo "${l_return_val}"
}

fn_MachineInfoValExtraction(){
    local l_item_hostnamectl=${1:-}     # hostnamectl
    local l_item_machineinfo=${2:-}
    local l_item_machine_path=${3:-'/etc/machine-info'}    # /etc/machine-info, /etc/os-release
    local l_return_val=${l_return_val:-}

    if [[ -n "${l_item_machineinfo}" ]]; then
        case "${l_item_machine_path}" in
            /etc/machine-info|/etc/os-release )
                [[ -s "${l_item_machine_path}" ]] && l_return_val=$(sed -r -n '/'"${l_item_machineinfo}"'[[:space:]]*=/{s@"@@g;s@^[^=]*=[[:space:]]*(.*)$@\1@g;p}' "${l_item_machine_path}" 2> /dev/null)
                ;;
        esac
    fi

    if [[ -z "${l_return_val}" && -n "${l_item_hostnamectl}" ]]; then
        fn_CommandExistCheck 'hostnamectl' && l_return_val=$(hostnamectl | sed -r -n '/'"${l_item_hostnamectl}"':/{s@^[^:]*:[[:space:]]*(.*)$@\1@g;p}' 2> /dev/null)
    fi

    # trip leading and trailing whitespaces
    echo "$(echo "${l_return_val}")"
}


# Product / Hardware / System / Software

#########  2-0. Operating System Info Detection  #########
# Operating System Info Detection
fn_OSInfo(){
    local l_operating_system=${l_operating_system:-}
    local l_os_architecture=${l_os_architecture:-}
    local l_kernel=${l_kernel:-}
    local l_machine_id=${l_machine_id:-}
    local l_boot_id=${l_boot_id:-}
    local l_virtualization=${l_virtualization:-}
    local l_static_hostname=${l_static_hostname:-}
    local l_pretty_hostname=${l_pretty_hostname:-}
    local l_icon_name=${l_icon_name:-}
    local l_chassis=${l_chassis:-}
    local l_deployment=${l_deployment:-}
    local l_timezone=${l_timezone:-}
    local l_boot_time=${l_boot_time:-}
    local l_uptime=${l_uptime:-}

    # Operating System
    # lsb_release -d
    l_operating_system=$(fn_MachineInfoValExtraction 'Operating System' 'PRETTY_NAME' '/etc/os-release')
    # OS Architecture
    fn_CommandExistCheck 'uname' && l_os_architecture=$(uname -m)
    # Kernel
    fn_CommandExistCheck 'uname' && l_kernel=$(uname -s -r)
    [[ -z "${l_kernel}" ]] && l_kernel=$(fn_MachineInfoValExtraction 'Kernel')
    # Machine ID
    if [[ -s '/etc/machine-id' ]]; then
        l_machine_id=$(cat /etc/machine-id)
    elif [[ -s '/var/lib/dbus/machine-id' ]]; then
        l_machine_id=$(cat /var/lib/dbus/machine-id)
    fi
    [[ -z ${l_machine_id} ]] && l_machine_id=$(fn_MachineInfoValExtraction 'Machine ID')
    # Boot ID
    # https://unix.stackexchange.com/questions/144812/generate-consistent-machine-unique-id#144892
    [[ -d /proc/sys/kernel/random ]] && l_boot_id=$(sed -r 's@[[:punct:]]*@@g' /proc/sys/kernel/random/boot_id)
    [[ -z "${l_boot_id}" ]] && l_boot_id=$(fn_MachineInfoValExtraction 'Boot ID')
    # Virtualization
    l_virtualization=$(fn_MachineInfoValExtraction 'Virtualization')
    # Static hostname
    if [[ -f '/etc/sysconfig/network' ]]; then
        l_static_hostname=$(sed -r -n '/HOSTNAME=/{s@^[^=]+=(.*)$@\1@g;p}' /etc/sysconfig/network)    #RHEL/CentOS
    elif [[ -f '/etc/hostname' ]]; then
        l_static_hostname=$(cat /etc/hostname)    #Debian/OpenSUSE
    fi
    [[ -z "${l_static_hostname}" ]] && l_static_hostname=$(fn_MachineInfoValExtraction 'Static hostname')
    # Pretty hostname
    l_pretty_hostname=$(fn_MachineInfoValExtraction 'Pretty hostname' 'PRETTY_HOSTNAME')
    # Icon name
    # https://standards.freedesktop.org/icon-naming-spec/icon-naming-spec-latest.html
    l_icon_name=$(fn_MachineInfoValExtraction 'Icon name' 'ICON_NAME')
    # Chassis    desktop, laptop, convertible, server, tablet, handset, watch, embedded，vm, container
    l_chassis=$(fn_MachineInfoValExtraction 'Chassis' 'CHASSIS')
    # Deployment    development, integration, staging, production
    l_deployment=$(fn_MachineInfoValExtraction 'Deployment' 'DEPLOYMENT')
    # Location    LOCATION should be a human-friendly, free-form string describing the physical location of the system, if it is known and applicable. This may be as generic as "Berlin, Germany" or as specific as "Left Rack, 2nd Shelf".
    l_location=$(fn_MachineInfoValExtraction 'Location' 'LOCATION')

    # Timezone
    fn_CommandExistCheck 'timedatectl' && l_timezone=$(timedatectl | sed -r -n '/Time zone:/{s@^[^:]+:[[:space:]]*(.*)@\1@g;p}')
    # for ubuntu/debian
    [[ -z "${l_timezone}" && -s /etc/timezone ]] && l_timezone=$(cat /etc/timezone)
    # for rhel/sles
    [[ -z "${l_timezone}" && -s /etc/sysconfig/clock ]] && l_timezone=$(sed -r -n '/^ZONE=/{s@"@@g;s@^[^=]+=(.*)$@\1@g;p}' /etc/sysconfig/clock)
    [[ -z "${l_timezone}" && -s /etc/localtime && $(readlink -f /etc/localtime) =~ /usr/share/zoneinfo/ ]] && l_timezone=$(readlink -f /etc/localtime | sed -r -n 's@^/usr/share/zoneinfo/(.*)$@\1@g;p')

    # Boot time
    if [[ -n $(uptime -s 2> /dev/null) ]]; then
        l_boot_time=$(uptime -s)
    else
        # who -b  -- not list second
        l_boot_time=$(last reboot -F | sed -r -n '/^reboot/{1{s@.*([[:upper:]]+.*)(-|still).*@\1@g;p}}' | date -f - +'%F %T')
    fi

    # Uptime
    if [[ -n $(uptime -p 2> /dev/null) ]]; then
        l_uptime=$(uptime -p)
    else
        l_uptime=$(uptime | sed -r -n 's@.*(up.*)[[:digit:]]+[[:space:]]*user.*@\1@g;s@[[:punct:]]*[[:space:]]*$@@g;s@[[:space:]]+@ @g;s@[[:space:]]+([[:digit:]]+):([[:digit:]]+)@ \1hours, \2minutes@g;p')
    fi
    l_uptime=$(echo "${l_uptime}" | sed -r -n 's@years@y@g;s@months@m@g;s@days@d@g;s@hours@h@g;s@minutes@Min@g;p')


    fn_CentralOutput 'Operating System' "${l_operating_system}"
    fn_CentralOutput 'OS Architecture' "${l_os_architecture}"
    fn_CentralOutput 'Kernel' "${l_kernel}"
    fn_CentralOutput 'Machine ID' "${l_machine_id}"
    fn_CentralOutput 'Virtualization' "${l_virtualization}"
    fn_CentralOutput 'Static hostname' "${l_static_hostname}"
    fn_CentralOutput 'Pretty hostname' "${l_pretty_hostname}"
    fn_CentralOutput 'Chassis' "${l_chassis}"
    fn_CentralOutput 'Icon name' "${l_icon_name}"
    fn_CentralOutput 'Deployment' "${l_deployment}"
    fn_CentralOutput 'Location' "${l_location}"
    fn_CentralOutput 'Time zone' "${l_timezone}"
    fn_CentralOutput 'Boot ID' "${l_boot_id}"
    fn_CentralOutput 'Boot time' "${l_boot_time}"
    fn_CentralOutput 'Uptime' "${l_uptime}"
}


#########  2-1. Product Info Detection  #########
# Product Relevant Info, e.g. manufacturer, product name, serial num ...

# Keyword     Types
# ──────────────────────────────
# system      1, 12, 15, 23, 32
#
# Type   Information
# ────────────────────────────────────────────
# 1   System
# 12   System Configuration Options
# 15   System Event Log
# 23   System Reset
# 32   System Boot

# 1 System Information
# 12 System Configuration Options
# 32 System Boot Information

# Handle 0x0C00, DMI type 12, 5 bytes
# Handle 0x2000, DMI type 32, 11 bytes

fn_ProductInfo(){
    # dmidecode -t system / dmidecode -t 1
    local l_product_manufacturer=${l_product_manufacturer:-}    # Manufacturer
    local l_product_family=${l_product_family:-}     # Family
    local l_product_name=${l_product_name:-}    # Product Name
    local l_product_version=${l_product_version:-}  # Version
    local l_product_serial_num=${l_product_serial_num:-}    # Serial Number
    local l_product_uuid=${l_product_uuid:-}    # UUID
    local l_product_wakeup_type=${l_product_wakeup_type:-}    # Wake-up Type
    local l_product_sku_num=${l_product_sku_num:-}    # SKU Number
    local l_product_boot_info_status=${l_product_boot_info_status:-}    # System Boot Information Status

    # 1 -- System Information
    l_product_manufacturer=$(fn_DmidecodeValExtraction '1' 'Manufacturer' 'sys_vendor' 'system-manufacturer')
    l_product_family=$(fn_DmidecodeValExtraction '1' 'Family' 'product_family')
    l_product_name=$(fn_DmidecodeValExtraction '1' 'Product Name' 'product_name' 'system-product-name')
    l_product_version=$(fn_DmidecodeValExtraction '1' 'Version' 'product_version' 'system-version')
    # [[ "${l_product_version}" == 'Not Specified' ]] && l_product_version=''
    l_product_serial_num=$(fn_DmidecodeValExtraction '1' 'Serial Number' 'product_serial' 'system-serial-number')
    l_product_uuid=$(fn_DmidecodeValExtraction '1' 'UUID' 'product_uuid' 'system-uuid')
    l_product_wakeup_type=$(fn_DmidecodeValExtraction '1' 'Wake-up Type')
    l_product_sku_num=$(fn_DmidecodeValExtraction '1' 'SKU Number')
    # 32 -- System Boot Information
    l_product_boot_info_status=$(fn_DmidecodeValExtraction '32' 'Status')

    fn_CentralOutputTitle 'System Information'
    fn_CentralOutput "Manufacturer" "${l_product_manufacturer}"
    fn_CentralOutput "Family" "${l_product_family}"
    fn_CentralOutput "Product Name" "${l_product_name}"
    fn_CentralOutput "Version" "${l_product_version}"
    fn_CentralOutput "Serial Number" "${l_product_serial_num}"
    fn_CentralOutput "UUID" "${l_product_uuid}"
    fn_CentralOutput "Wake-up Type" "${l_product_wakeup_type}"
    fn_CentralOutput "SKU Number" "${l_product_sku_num}"
    fn_CentralOutput "System Boot Information Status" "${l_product_boot_info_status}"

    echo ''
    fn_OSInfo
}


#########  2-2. BIOS Info Detection  #########
# Type   Information
# ────────────────────────────────────────────
#    0   BIOS
#   13   BIOS Language
#
# Keyword     Types
# ──────────────────────────────
# bios        0, 13

# 0 BIOS Information
# 13 BIOS Language Information

fn_BIOSInfo(){
    # biosdecode
    # dmidecode -t bios / dmidecode -t 0,13
    local l_bios_vender=${l_bios_vender:-}    # Vendor
    local l_bios_version=${l_bios_version:-}  # Version
    local l_bios_release_date=${l_bios_release_date:-}    # Release Date
    local l_bios_address=${l_bios_address:-}  # Address
    local l_bios_runtime_size=${l_bios_runtime_size:-}    # Runtime Size
    local l_bios_rom_size=${l_bios_rom_size:-}    # ROM Size
    local l_bios_characteristics=${l_bios_characteristics:-}  # Characteristics
    local l_bios_revision=${l_bios_revision:-}    # BIOS Revision
    local l_bios_language=${l_bios_language:-}    # BIOS Language
    local l_bios_language_format=${l_bios_language_format:-}  # Language Description Format
    local l_bios_language_installed=${l_bios_language_installed:-}    # Currently Installed Language

    # 0 --BIOS Information
    l_bios_vender=$(fn_DmidecodeValExtraction 'bios' 'Vendor' 'bios_vendor' 'bios-vendor')
    l_bios_version=$(fn_DmidecodeValExtraction 'bios' 'Version' 'bios_version' 'bios-version')
    l_bios_release_date=$(fn_DmidecodeValExtraction 'bios' 'Release Date' 'bios_date' 'bios-release-date' | date +'%F' -f - 2> /dev/null)
    l_bios_address=$(fn_DmidecodeValExtraction 'bios' 'Address')
    l_bios_runtime_size=$(fn_DmidecodeValExtraction 'bios' 'Runtime Size')
    l_bios_rom_size=$(fn_DmidecodeValExtraction 'bios' 'ROM Size')
    l_bios_characteristics=$(dmidecode -t bios | sed -r -n '/^[[:space:]]*Characteristics:/,/^[[:space:]]*[^:]*:/{/:/d;s@^[[:space:]]*(.*?)[[:space:]]*(is|are).*@\1@g;s@[[:space:]]*$@@g;p}' | sed ':a;N;$!ba;s@\n@, @g')
    l_bios_revision=$(fn_DmidecodeValExtraction 'bios' 'BIOS Revision')
    # 13 -- BIOS Language Information
    l_bios_language_format=$(fn_DmidecodeValExtraction 'bios' 'Language Description Format')
    l_bios_language_installed=$(fn_DmidecodeValExtraction 'bios' 'Currently Installed Language')


    fn_CentralOutputTitle 'BIOS Information'
    fn_CentralOutput 'Vendor' "${l_bios_vender}"
    fn_CentralOutput 'Version' "${l_bios_version}"
    fn_CentralOutput 'Release Date' "${l_bios_release_date}"
    fn_CentralOutput 'Address' "${l_bios_address}"
    fn_CentralOutput 'Runtime Size' "${l_bios_runtime_size}"
    fn_CentralOutput 'ROM Size' "${l_bios_rom_size}"
    fn_CentralOutput 'BIOS Revision' "${l_bios_revision}"
    fn_CentralOutput 'BIOS Language' "${l_bios_language}"
    fn_CentralOutput 'Language Description Format' "${l_bios_language_format}"
    fn_CentralOutput 'Currently Installed Language' "${l_bios_language_installed}"
    fn_CentralOutput 'Characteristics' "${l_bios_characteristics}"
}


#########  2-3. Baseboard Info Detection  #########
# Type   Information
# ────────────────────────────────────────────
#    2   Baseboard
#   10   On Board Devices
#   41   Onboard Devices Extended Information
#
# Keyword     Types
# ──────────────────────────────
# baseboard   2, 10, 41

# 2 Base Board Information
# 10 On Board Device Information
# 41 Onboard Device

fn_BaseboardInfo(){
    # dmidecode -t baseboard / dmidecode -t 2

    local l_baseboard_manufacturer=${l_baseboard_manufacturer:-}    # Manufacturer
    local l_baseboard_product_name=${l_baseboard_product_name:-}    # Product Name
    local l_baseboard_version=${l_baseboard_version:-}   # Version
    local l_baseboard_serial_num=${l_baseboard_serial_num:-}    # Serial Number
    local l_baseboard_asset_tag=${l_baseboard_asset_tag:-}  # Asset Tag
    local l_baseboard_feateures=${l_baseboard_feateures:-}  # Features
    local l_baseboard_location_in_chassis=${l_baseboard_location_in_chassis:-}  # Location In Chassis
    local l_baseboard_chassis_handle=${l_baseboard_chassis_handle:-}    # Chassis Handle
    local l_baseboard_type=${l_baseboard_type:-}    # Type
    local l_baseboard_contained_object_handles=${l_baseboard_contained_object_handles:-}    # Contained Object Handles

    l_baseboard_manufacturer=$(fn_DmidecodeValExtraction '2' 'Manufacturer' 'board_vendor' 'baseboard-manufacturer')
    l_baseboard_product_name=$(fn_DmidecodeValExtraction '2' 'Product Name' 'board_name' 'baseboard-product-name')
    l_baseboard_version=$(fn_DmidecodeValExtraction '2' 'Version' 'board_version' 'baseboard-version')
    l_baseboard_serial_num=$(fn_DmidecodeValExtraction '2' 'Serial Number' 'board_serial' 'baseboard-serial-number')
    l_baseboard_asset_tag=$(fn_DmidecodeValExtraction '2' 'Asset Tag' 'board_asset_tag' 'baseboard-asset-tag')
    l_baseboard_feateures=$(dmidecode -t baseboard | sed -r -n '/^[[:space:]]*Features:/,/^[[:space:]]*[^:]*:/{/:/d;s@^[[:space:]]*(.*?)[[:space:]]*(is|are)[[:space:]]*a?[[:space:]]*(.*)@\3@g;s@[[:space:]]*$@@g;p}' | sed ':a;N;$!ba;s@\n@, @g')

    l_baseboard_location_in_chassis=$(fn_DmidecodeValExtraction '2' 'Location In Chassis')
    l_baseboard_chassis_handle=$(fn_DmidecodeValExtraction '2' 'Chassis Handle')
    l_baseboard_type=$(fn_DmidecodeValExtraction '2' 'Type')
    l_baseboard_contained_object_handles=$(fn_DmidecodeValExtraction '2' 'Contained Object Handles')

    fn_CentralOutputTitle 'Base Board Information'
    fn_CentralOutput 'Manufacturer' "${l_baseboard_manufacturer}"
    fn_CentralOutput 'Product Name' "${l_baseboard_product_name}"
    fn_CentralOutput 'Version' "${l_baseboard_version}"
    fn_CentralOutput 'Serial Number' "${l_baseboard_serial_num}"
    fn_CentralOutput 'Asset Tag' "${l_baseboard_asset_tag}"
    fn_CentralOutput 'Features' "${l_baseboard_feateures}"
    fn_CentralOutput 'Location In Chassis' "${l_baseboard_location_in_chassis}"
    fn_CentralOutput 'Chassis Handle' "${l_baseboard_chassis_handle}"
    fn_CentralOutput 'Type' "${l_baseboard_type}"
    fn_CentralOutput 'Contained Object Handles' "${l_baseboard_contained_object_handles}"

}

fn_OnBoardDeviceInfo(){
    # dmidecode -t baseboard / dmidecode -t 10,41

    # 10 - On Board Device Information
    # DmidecodeType|Type|Status|Description
    # e.g. On Board Device Information|Video|Enabled|Mobile Intel HD Graphics

    fn_CentralOutputTitle 'On Board Device Information'

    echo "${c_red}Type|Status|Description${c_normal}"

    dmidecode -t 10 | sed -r -n '/DMI type/,/^$/{/(DMI type|Device Information)/d;s@^$@---@g;s@^[[:space:]]*@@g;s@^[^:]+:[[:space:]]*(.*)$@\1@g;s@[[:space:]]*$@@g;p}' | sed -r ':a;N;$!ba;s@\n@|@g;s@[|][-]+[|]?@\n@g;' | sed '/^$/d'

}

fn_OnBoardDeviceExtendedInfo(){
    # 41 - Onboard Devices Extended Information
    # DmidecodeType|Reference Designation|Type|Status|Type Instance|Bus Address
    # e.g. Onboard Device|Integrated NIC 1|Ethernet|Enabled|1|0000:01:00.0

    # - PCI devices list
    # 'lspci -vm' ==> 'Bus Address' ==> '[domain:]bus:device.function'
    # Device|Class|Vendor|Device|Rev|ProgIf
    # e.g. 01:00.0|Ethernet controller|Broadcom Limited|NetXtreme BCM5720 Gigabit Ethernet PCIe
    # lspci -vm | sed -r -n '/(SVendor|SDevice)/d;s@^$@---@g;s@^[^:]+:[[:space:]]*(.*)$@\1@g;p' | sed -r ':a;N;$!ba;s@\n@|@g;s@[|][-]+[|]?@\n@g;' | sed '/^$/d'

    pci_devices_info=$(lspci -vm | sed -r -n '/(SVendor|SDevice)/d;s@^$@---@g;s@^[^:]+:[[:space:]]*(.*)$@\1@g;p' | sed -r ':a;N;$!ba;s@\n@|@g;s@[|][-]+[|]?@\n@g;' | sed '/^$/d')

    fn_CentralOutputTitle 'Onboard Devices Extended Information'

    echo "${c_red}Type|Type Instance|Status|Bus Address|Reference Designation|Vendor|Device|Rev|ProgIf${c_normal}"

    dmidecode -t 41 | sed -r -n '/DMI type 41/,/^$/{/(DMI type)/d;s@^$@---@g;s@^[[:space:]]*@@g;s@[[:space:]]*$@@g;s@^[^:]+:[[:space:]]*(.*)$@\1@g;p}' | sed -r ':a;N;$!ba;s@\n@|@g;s@[|][-]+[|]?@\n@g;' | sed '/^$/d' | while IFS="|" read -r dmidecode_type reference_designation d_type status type_instance bus_addr; do
        pci_info=$(echo "${pci_devices_info}" | sed -r -n '/^'"${bus_addr#*:}"'\|/{p}' 2> /dev/null)

        # Type|Type Instance|Status|Bus Address|Reference Designation|Vendor|Device|Rev|ProgIf
        if [[ -n "${pci_info}" ]]; then
            echo "${pci_info}" | awk -F\| -v d_type="${d_type}" -v type_instance="${type_instance}" -v status="${status}" -v bus_addr="${bus_addr}" -v reference_designation="${reference_designation}" 'BEGIN{OFS="|"}{
                # Type|Type Instance|Status|Bus Address|Class|Reference Designation|Vendor|Device|Rev|ProgIf
                # print d_type,type_instance,status,bus_addr,$2,reference_designation,$3,$4,$5,$6
                # Type|Type Instance|Status|Bus Address|Reference Designation|Vendor|Device|Rev|ProgIf
                print d_type,type_instance,status,bus_addr,reference_designation,$3,$4,$5,$6
            }'
        else
            # echo "${d_type}|${type_instance}|${status}|${bus_addr}||${reference_designation}"
            echo "${d_type}|${type_instance}|${status}|${bus_addr}|${reference_designation}"
        fi
    done
}


#########  2-4. Chassis Info Detection  #########
# Type   Information
# ────────────────────────────────────────────
#    3   Chassis
#
# Keyword     Types
# ──────────────────────────────
# chassis     3

# 3 Chassis Information

# Mother Board
fn_ChassisInfo(){
    # dmidecode -t chassis / dmidecode -t 3
    local l_chassis_manufacturer=${l_chassis_manufacturer:-}    # Manufacturer
    local l_chassis_type=${l_chassis_type:-}    # Type
    local l_chassis_version=${l_chassis_version:-}  # Version
    local l_chassis_serial_num=${l_chassis_serial_num:-}    # Serial Number
    local l_chassis_asset_tag=${l_chassis_asset_tag:-}  # Asset Tag
    local l_chassis_lock=${l_chassis_lock:-}    # Lock
    local l_chassis_bootup_state=${l_chassis_bootup_state:-}    # Boot-up State
    local l_chassis_power_supply_state=${l_chassis_power_supply_state:-}    # Power Supply State
    local l_chassis_thermal_state=${l_chassis_thermal_state:-}  # Thermal State
    local l_chassis_oem_information=${l_chassis_oem_information:-}  # OEM Information
    local l_chassis_height=${l_chassis_height:-}    # Height
    local l_chassis_num_of_power_cords=${l_chassis_num_of_power_cords:-}    # Number Of Power Cords
    local l_chassis_contianed_elements=${l_chassis_contianed_elements:-}    # Contained Elements
    local l_chassis_sku_num=${l_chassis_sku_num:-}  # SKU Number

    l_chassis_manufacturer=$(fn_DmidecodeValExtraction 'chassis' 'Manufacturer' 'chassis_vendor' 'chassis-manufacturer')
    # l_chassis_type=$(fn_DmidecodeValExtraction 'chassis' 'Type' 'chassis_type' 'chassis-type')
    l_chassis_type=$(fn_DmidecodeValExtraction 'chassis' 'Type')
    l_chassis_version=$(fn_DmidecodeValExtraction 'chassis' 'Version' 'chassis_version' 'chassis-version')
    l_chassis_serial_num=$(fn_DmidecodeValExtraction 'chassis' 'Serial Number' 'chassis_serial' 'chassis-serial-number')
    l_chassis_asset_tag=$(fn_DmidecodeValExtraction 'chassis' 'Asset Tag' 'chassis_asset_tag' 'chassis-asset-tag')
    l_chassis_lock=$(fn_DmidecodeValExtraction 'chassis' 'Lock')
    l_chassis_bootup_state=$(fn_DmidecodeValExtraction 'chassis' 'Boot-up State')
    l_chassis_power_supply_state=$(fn_DmidecodeValExtraction 'chassis' 'Power Supply State')
    l_chassis_thermal_state=$(fn_DmidecodeValExtraction 'chassis' 'Thermal State')
    l_chassis_oem_information=$(fn_DmidecodeValExtraction 'chassis' 'OEM Information')
    l_chassis_height=$(fn_DmidecodeValExtraction 'chassis' 'Height')
    l_chassis_num_of_power_cords=$(fn_DmidecodeValExtraction 'chassis' 'Number Of Power Cords')
    l_chassis_contianed_elements=$(fn_DmidecodeValExtraction 'chassis' 'Contained Elements')
    l_chassis_sku_num=$(fn_DmidecodeValExtraction 'chassis' 'SKU Number')

    fn_CentralOutputTitle 'Base Board Information'
    fn_CentralOutput 'Manufacturer' "${l_chassis_manufacturer}"
    fn_CentralOutput 'Type' "${l_chassis_type}"
    fn_CentralOutput 'Version' "${l_chassis_version}"
    fn_CentralOutput 'Serial Number' "${l_chassis_serial_num}"
    fn_CentralOutput 'Asset Tag' "${l_chassis_asset_tag}"
    fn_CentralOutput 'Lock' "${l_chassis_lock}"
    fn_CentralOutput 'Boot-up State' "${l_chassis_bootup_state}"
    fn_CentralOutput 'Power Supply State' "${l_chassis_power_supply_state}"
    fn_CentralOutput 'Thermal State' "${l_chassis_thermal_state}"
    fn_CentralOutput 'OEM Information' "${l_chassis_oem_information}"
    fn_CentralOutput 'Height' "${l_chassis_height}"
    fn_CentralOutput 'Number Of Power Cords' "${l_chassis_num_of_power_cords}"
    fn_CentralOutput 'Contained Elements' "${l_chassis_contianed_elements}"
    fn_CentralOutput 'SKU Number' "${l_chassis_sku_num}"

}


#########  2-5. Memory Info Detection  #########
# Type   Information
# ────────────────────────────────────────────
#    5   Memory Controller
#    6   Memory Module
#   16   Physical Memory Array
#   17   Memory Device
#
# Keyword     Types
# ──────────────────────────────
# memory      5, 6, 16, 17

# 16 Physical Memory Array
# 17 Memory Device

fn_MemoryInfo(){
    # 16 -- Physical Memory Array
    local l_memory_location=${l_memory_location:-}  # Location
    local l_memory_use=${l_memory_use:-}    # Use
    local l_memory_maximum_capacity=${l_memory_maximum_capacity:-}  # Maximum Capacity
    local l_memory_number_of_devices=${l_memory_number_of_devices:-}    # Number Of Devices
    local l_memory_error_correction_type=${l_memory_error_correction_type:-}    # Error Correction Type
    local l_memory_error_info_handle=${l_memory_error_info_handle:-}    # Error Information Handle

    l_memory_location=$(fn_DmidecodeValExtraction '16' 'Location')
    l_memory_use=$(fn_DmidecodeValExtraction '16' 'Use')
    l_memory_maximum_capacity=$(fn_DmidecodeValExtraction '16' 'Maximum Capacity')
    l_memory_number_of_devices=$(fn_DmidecodeValExtraction '16' 'Number Of Devices')
    l_memory_error_correction_type=$(fn_DmidecodeValExtraction '16' 'Error Correction Type')
    l_memory_error_info_handle=$(fn_DmidecodeValExtraction '16' 'Error Information Handle')


    fn_CentralOutputTitle 'Physical Memory Array'
    fn_CentralOutput 'Location' "${l_memory_location}"
    fn_CentralOutput 'Use' "${l_memory_use}"
    fn_CentralOutput 'Maximum Capacity' "${l_memory_maximum_capacity}"
    fn_CentralOutput 'Number Of Devices' "${l_memory_number_of_devices}"
    fn_CentralOutput 'Error Correction Type' "${l_memory_error_correction_type}"
    fn_CentralOutput 'Error Information Handle' "${l_memory_error_info_handle}"


    fn_CentralOutputTitle 'Memory Device List'
    # Locator|Set|Form Factor|Type|Size|Speed|Manufacturer|Serial Number|Type Detail|Rank|Error Information Handle
    dmidecode -t 17 | sed -r -n '/DMI type 17/,/^$/{/^[[:space:]]*(Locator|Set|Form Factor|Type|Size|Speed|Type Detail|Manufacturer|Serial Number|Rank|Error Information Handle|^$):?/!d;s@^[[:space:]]*@@g;s@^$@---@g;s@^[^:]+:[[:space:]]*(.*)$@\1@g;s@[[:space:]]*$@@g;p}' | sed -r ':a;N;$!ba;s@\n@|@g;s@[|][-]+[|]?@\n@g;' | sed '/^$/d' | awk -F\| 'BEGIN{OFS="|";print "'"${c_red}"'Locator|Set|FormFactor|Type|Size|Speed|TypeDetail|Manufacturer|SerialNumber|Rank|ErrorInformationHandle'"${c_normal}"'"}{
        gsub(/^[^_]+_/,"",$5);
        if(match($2,/^[[:digit:]]+/) == 0){$2=""};
        if(match($8,/^[[:digit:]]+/) == 0){$8=""};
        # Synchronous Registered (Buffered)
        if(match($7,/^Synchronous Registered/) == 1){$7="Sync"}else{$7=""};
        # Not Specified
        if(match($9,/^Not Specified/) == 1){$9=""}
        # Not Specified
        if(match($10,/^Not Specified/) == 1){$10=""}
        if(match($11,/^[[:digit:]]+/) == 0){$11=""};
        # Not Provided
        if(match($1,/^Not Provided/) == 1){$1=""};
        print $5,$4,$3,$6,$2,$8,$7,$9,$10,$11,$1
    }'

    # A2|1|DIMM|DDR4|16384 MB|2400 MHz|00AD00B300AD|41EC1436|Synchronous Registered (Buffered)|2|Not Provided
    # DIMM_A2|1|DIMM|DDR3|16384 MB|1333 MHz|00CE00B300CE|85CC30AC|Synchronous Registered (Buffered)|2|Not Provided
}


#########  2-6. Cache Info Detection  #########
# Type   Information
# ────────────────────────────────────────────
#    7   Cache
#
# Keyword     Types
# ──────────────────────────────
# cache       7
#
# 7 Cache Information

fn_CacheInfo(){
    # dmidecode -t cache / dmidecode -t 7

    fn_CentralOutputTitle 'CPU Cache Information'

    # origin: Socket Designation|Configuration|Operational Mode|Location|Installed Size|Maximum Size|Speed|Error Correction Type|System Type|Associativity
    echo "${c_red}Socket Designation|Location|Installed Size|Maximum Size|Speed|Operational Mode|System Type|Associativity|Error Correction Type|Configuration${c_normal}"

    dmidecode -t 7 | sed -r -n '/DMI type/,/^$/{/(DMI type|Cache Information)/d;/Supported SRAM/,/Installed SRAM/{d};s@^$@---@g;s@^[[:space:]]*@@g;s@^[^:]+:[[:space:]]*(.*)$@\1@g;s@[[:space:]]*$@@g;p}' | sed -r ':a;N;$!ba;s@\n@|@g;s@[|][-]+[|]?@\n@g;' | sed '/^$/d' | awk -F\| 'BEGIN{OFS="|"}{print $1,$4,$5,$6,$7,$3,$9,$10,$8,$2}'
}


#########  2-7. Port Connector Info Detection  #########
# Type   Information
# ────────────────────────────────────────────
#    8   Port Connector
#
# Keyword     Types
# ──────────────────────────────
# connector   8
#
# 8 Port Connector Information

fn_PortConnectorInfo(){
    # dmidecode -t connector / dmidecode -t 8

    fn_CentralOutputTitle 'Port Connector Information'

    # origin: Internal Reference Designator|Internal Connector Type|External Reference Designator|External Connector Type|Port Type
    echo "${c_red}Port Type|External Reference Designator|External Connector Type|Internal Reference Designator|Internal Connector Type${c_normal}"

    dmidecode -t 8 | sed -r -n '/DMI type/,/^$/{/(DMI type|Port Connector Information)/d;s@^$@---@g;s@^[[:space:]]*@@g;s@^[^:]+:[[:space:]]*(.*)$@\1@g;s@[[:space:]]*$@@g;p}' | sed -r ':a;N;$!ba;s@\n@|@g;s@[|][-]+[|]?@\n@g;' | sed '/^$/d' | awk -F\| 'BEGIN{OFS="|"}{print $5,$3,$4,$1,$2}'
}


#########  2-8. System Slot Information  #########
# Type   Information
# ────────────────────────────────────────────
#    9   System Slots
#
# Keyword     Types
# ──────────────────────────────
# slot        9
#
# 9 System Slot Information

fn_SystemSlotInfo(){
    # dmidecode -t slot / dmidecode -t 9

    fn_CentralOutputTitle 'System Slot Information'

    # origin: Designation|Type|Current Usage|Length|ID|Bus Address
    echo "${c_red}Designation|Current Usage|Type|Length|ID|Bus Address${c_normal}"

    dmidecode -t slot | sed -r -n '/DMI type/,/^$/{/(DMI type| Information)/d;s@^$@---@g;/^[[:space:]]+[^:]+$/d;/Characteristics:/d;s@^[[:space:]]*@@g;s@^[^:]+:[[:space:]]*(.*)$@\1@g;s@[[:space:]]*$@@g;p}' | sed -r ':a;N;$!ba;s@\n@|@g;s@[|][-]+[|]?@\n@g;' | sed '/^$/d' | awk -F\| 'BEGIN{OFS="|"}{print $1,$3,$2,$4,$5,$6}'
}


#########  2-9. Built-in Pointing Device Information  #########
# Keyword     Types
# ──────────────────────────────
# 21   Built-in Pointing Device
#
# 21 Built-in Pointing Device

fn_BuiltinPointingDeviceInfo(){
    # dmidecode -t 21

    fn_CentralOutputTitle 'Built-in Pointing Device Information'

    # origin: Type|Interface|Buttons
    echo "${c_red}Type|Interface|Buttons${c_normal}"

    dmidecode -t 21 | sed -r -n '/DMI type/,/^$/{/(DMI type|Pointing Device)/d;s@^$@---@g;s@^[[:space:]]*@@g;s@^[^:]+:[[:space:]]*(.*)$@\1@g;s@[[:space:]]*$@@g;p}' | sed -r ':a;N;$!ba;s@\n@|@g;s@[|][-]+[|]?@\n@g;' | sed '/^$/d'

}


#########  2-10. Portable Battery Information  #########
# Keyword     Types
# ──────────────────────────────
# 22   Portable Battery
#
# 22 Portable Battery

fn_PortableBatteryInfo(){
    # dmidecode -t 22

    fn_CentralOutputTitle 'Portable Battery Information'

    fn_CentralOutput 'Location' "$(fn_DmidecodeValExtraction '22' 'Location')"
    fn_CentralOutput 'Manufacturer' "$(fn_DmidecodeValExtraction '22' 'Manufacturer')"
    fn_CentralOutput 'Serial Number' "$(fn_DmidecodeValExtraction '22' 'Serial Number')"
    fn_CentralOutput 'Name' "$(fn_DmidecodeValExtraction '22' 'Name')"
    fn_CentralOutput 'Chemistry' "$(fn_DmidecodeValExtraction '22' 'Chemistry')"
    fn_CentralOutput 'Design Capacity' "$(fn_DmidecodeValExtraction '22' 'Design Capacity')"
    fn_CentralOutput 'Design Voltage' "$(fn_DmidecodeValExtraction '22' 'Design Voltage')"
    fn_CentralOutput 'SBDS Version' "$(fn_DmidecodeValExtraction '22' 'SBDS Version')"
    fn_CentralOutput 'SBDS Manufacture Date' "$(fn_DmidecodeValExtraction '22' 'SBDS Manufacture Date')"
    fn_CentralOutput 'OEM-specific Information' "$(fn_DmidecodeValExtraction '22' 'OEM-specific Information')"

    # origin: Location|Manufacturer|Serial Number|Name|Chemistry|Design Capacity|Design Voltage|SBDS Version|Maximum Error|SBDS Manufacture Date|OEM-specific Information
    # echo "${c_red}Type|Interface|Buttons${c_normal}"
    # dmidecode -t 22 | sed -r -n '/DMI type/,/^$/{/(DMI type|Portable Battery)/d;s@^$@---@g;s@^[[:space:]]*@@g;s@^[^:]+:[[:space:]]*(.*)$@\1@g;s@[[:space:]]*$@@g;p}' | sed -r ':a;N;$!ba;s@\n@|@g;s@[|][-]+[|]?@\n@g;' | sed '/^$/d'
}


#########  2-11. Cooling Device Information  #########
# Keyword     Types
# ──────────────────────────────
# 27   Cooling Device
#
# 27 Cooling Device

fn_CoolingDeviceInfo(){
    # dmidecode -t 27

    fn_CentralOutputTitle 'Cooling Device Information'

    # origin: Type|Status|OEM-specific Information
    echo "${c_red}Type|Status|OEM-specific Information${c_normal}"

    dmidecode -t 27 | sed -r -n '/DMI type/,/^$/{/(DMI type|Cooling Device)/d;s@^$@---@g;s@^[[:space:]]*@@g;s@^[^:]+:[[:space:]]*(.*)$@\1@g;s@[[:space:]]*$@@g;p}' | sed -r ':a;N;$!ba;s@\n@|@g;s@[|][-]+[|]?@\n@g;' | sed '/^$/d'
}


#########  2-12. Temperature Probe Information  #########
# Keyword     Types
# ──────────────────────────────
# 28   Temperature Probe
#
# 28 Temperature Probe

fn_TemperatureProbeInfo(){
    # dmidecode -t 28

    fn_CentralOutputTitle 'Temperature Probe'

    # origin: Description|Location|Status|Maximum Value|Minimum Value|Resolution|Tolerance|Accuracy|OEM-specific Information
    echo "${c_red}Description|Location|Status|Maximum Value|Minimum Value|Resolution|Tolerance|Accuracy|OEM-specific Information${c_normal}"

    dmidecode -t 28 | sed -r -n '/DMI type/,/^$/{/(DMI type|Temperature Probe)/d;s@^$@---@g;s@^[[:space:]]*@@g;s@^[^:]+:[[:space:]]*(.*)$@\1@g;s@[[:space:]]*$@@g;p}' | sed -r ':a;N;$!ba;s@\n@|@g;s@[|][-]+[|]?@\n@g;' | sed '/^$/d'
}


#########  2-13. Disk Information  #########
# 磁盤
# https://www.binarytides.com/linux-command-check-disk-partitions/
# https://unix.stackexchange.com/questions/121757/harddisk-serial-number-from-terminal
# https://www.cyberciti.biz/faq/find-hard-disk-hardware-specs-on-linux/

# http://blog.csdn.net/harbor1981/article/details/42772377
# https://serverfault.com/questions/718654/can-i-detect-hardware-raid-infromation-from-inside-linux/718769#718769


# Solid State Drive (SSD) / Hard Disk Drive (HDD)

fn_MegaRAIDInfoDetection(){
    local l_item="${1:-}"
    local l_raw_data="${2:-}"

    local l_val=${l_val:-}
    if [[ -n "${l_item}" && -n "${l_raw_data}" ]]; then
        l_val=$(echo "${l_raw_data}" | sed -r -n '/^'"${l_item}"'[[:space:]]*:/{s@^[^:]+:[[:space:]]*(.*)$@\1@g;p}')
    fi
    echo "${l_val}"
}

fn_DiskRAIDInfo(){
    local l_pci_raid_info
    l_pci_raid_info=$(lspci | sed -r -n '/RAID/p')

    local l_megacli_dir=${l_megacli_dir:-'/opt/MegaRAID/MegaCli'}
    local l_megacli_path=${l_megacli_path:-"${l_megacli_dir}/MegaCli64"}
    local l_megacli_link=${l_megacli_link:-'https://gitlab.com/MaxdSre/axd-ShellScript/raw/master/utilities/MegaCLI/MegaCli-8.07.14-1.noarch.tar.xz'}

    if [[ -n "${l_pci_raid_info}" ]]; then
        # dmesg | grep -i raid
        local l_product_manufacturer
        [[ -s '/sys/devices/virtual/dmi/id/sys_vendor' ]] && l_product_manufacturer=$(cat /sys/devices/virtual/dmi/id/sys_vendor)
        # Dell Inc.
        if [[ "${l_product_manufacturer}" =~ Dell && "${l_pci_raid_info}" =~ MegaRAID ]]; then
            # Detecting Dell service RAID info via utility MegaCli
            # https://docs.broadcom.com/docs-and-downloads/raid-controllers/raid-controllers-common-files/8-07-14_MegaCLI.zip

            # Dell R720 | 03:00.0 RAID bus controller: LSI Logic / Symbios Logic MegaRAID SAS 2008 [Falcon] (rev 03)
            # Dell R730 | 03:00.0 RAID bus controller: LSI Logic / Symbios Logic MegaRAID SAS-3 3108 [Invader] (rev 02)

            if [[ ! -d "${l_megacli_dir}" || ! -x "${l_megacli_path}" ]]; then
                [[ -d "${l_megacli_dir}" ]] && rm -rf "${l_megacli_dir}"
                [[ -d "${l_megacli_dir}" ]] || mkdir -p "${l_megacli_dir}"

                megacli_save_path=${megacli_save_path:-"/tmp/${l_megacli_link##*/}"}
                curl -fsSL "${l_megacli_link}" > "${megacli_save_path}"
                tar xf "${megacli_save_path}" -C "${l_megacli_dir}" --strip-components=1
                [[ -s "${l_megacli_dir}"/libstorelibir-2.so.14.07-0 ]] && ln -fs "${l_megacli_dir}"/libstorelibir-2.so.14.07-0 "${l_megacli_dir}"/libstorelibir-2.so 2> /dev/null
                [[ -f "${megacli_save_path}" ]] && rm -f "${megacli_save_path}"

                # mkdir -p /opt/MegaRAID/MegaCli
                # tar xf /tmp/MegaCli-8.07.14-1.noarch.tar.xz -C /opt/MegaRAID/MegaCli --strip-components=1
                # ln -fs /opt/MegaRAID/MegaCli/libstorelibir-2.so.14.07-0 /opt/MegaRAID/MegaCli/libstorelibir-2.so

                # MegaCli not providing all the information we need like mapping to linux devices and raid level (readable), so we are going to use some extra tools.
                # yum install sg3_utils
                # apt-get install sg3-utils
            fi
        fi
    fi

    if [[ -s "${l_megacli_path}" ]]; then
        fn_CentralOutputTitle 'RAID Disk Info'

        # ./MegaCli64 -NoLog -V      # MegaCLI SAS RAID Management Tool  Ver 8.07.14 Dec 16, 2013
        # ./MegaCli64 -NoLog -FwTermLog -Dsply -aAll  # Firmware Term Log Information
        # ./MegaCli64 -NoLog -ShowSummary -aAll  # System & Hardware (Controller/BBU/Enclosure/PD) & Storage symmary
        # ./MegaCli64 -NoLog -cfgdsply -aAll    # Raid info, setting,disk info


        # - Adapter information
        # ./MegaCli64 -NoLog -AdpAllInfo -aAll  # Adapter info summary
        # ./MegaCli64 -NoLog -AdpGetTime -aAll  # Adapter dant & time
        # ./MegaCli64 -NoLog -adpCount  # Adapter count

        # - PCI information for Controller
        # ./MegaCli64 -NoLog -AdpGetPciInfo -aAll
        fn_CentralOutputTitle 'PCI information for Controller' '1'
        local l_pci_controller
        l_pci_controller=$("${l_megacli_path}" -AdpGetPciInfo -aAll 2> /dev/null)

        fn_CentralOutput 'Bus Number ' "$(fn_MegaRAIDInfoDetection 'Bus Number ' "${l_pci_controller}")"
        fn_CentralOutput 'Device Number' "$(fn_MegaRAIDInfoDetection 'Device Number' "${l_pci_controller}")"
        fn_CentralOutput 'Function Number' "$(fn_MegaRAIDInfoDetection 'Function Number' "${l_pci_controller}")"


        # - BBU(battery backup unit) Information
        # ./MegaCli64 -NoLog -AdpBbuCmd -GetBbuStatus -aAll  # BBU status for Adapter
        # ./MegaCli64 -NoLog -AdpBbuCmd -GetBbuCapacityInfo -aAll  # BBU Capacity Info for Adapter
        # ./MegaCli64 -NoLog -AdpBbuCmd -GetBbuDesignInfo -aAll  # BBU Design Info for Adapter
        # ./MegaCli64 -NoLog -AdpBbuCmd -GetBbuProperties -aAll  # BBU Properties for Adapter
        fn_CentralOutputTitle 'BBU(battery backup unit) Information' '1'
        local l_bbu_status_info
        l_bbu_status_info=$("${l_megacli_path}" -AdpBbuCmd -GetBbuStatus -aAll)
        fn_CentralOutput 'BatteryType' "$(fn_MegaRAIDInfoDetection 'BatteryType' "${l_bbu_status_info}")"
        fn_CentralOutput 'Voltage' "$(fn_MegaRAIDInfoDetection 'Voltage' "${l_bbu_status_info}")"
        fn_CentralOutput 'Current' "$(fn_MegaRAIDInfoDetection 'Current' "${l_bbu_status_info}")"
        fn_CentralOutput 'Temperature' "$(fn_MegaRAIDInfoDetection 'Temperature' "${l_bbu_status_info}")"
        fn_CentralOutput 'Battery State' "$(fn_MegaRAIDInfoDetection 'Battery State' "${l_bbu_status_info}")"
        fn_CentralOutput 'Charging Status' "$(fn_MegaRAIDInfoDetection 'Charging Status' "${l_bbu_status_info}")"
        fn_CentralOutput 'Charger Status' "$(fn_MegaRAIDInfoDetection 'Charger Status' "${l_bbu_status_info}")"
        fn_CentralOutput 'Relative State of Charge' "$(fn_MegaRAIDInfoDetection 'Relative State of Charge' "${l_bbu_status_info}")"
        fn_CentralOutput 'Remaining Capacity' "$(fn_MegaRAIDInfoDetection 'Remaining Capacity' "${l_bbu_status_info}")"
        fn_CentralOutput 'Full Charge Capacity' "$(fn_MegaRAIDInfoDetection 'Full Charge Capacity' "${l_bbu_status_info}")"
        fn_CentralOutput 'isSOHGood' "$(fn_MegaRAIDInfoDetection 'isSOHGood' "${l_bbu_status_info}")"


        # - Virtual Drive Information
        # ./MegaCli64 -NoLog -LDInfo -Lall -a0  # Gather info about Virtual drives
        # ./MegaCli64 -NoLog -LDInfo -Lall -aAll  # Virtual Drive Information
        fn_CentralOutputTitle 'Virtual Drives Information' '1'
        local l_virtual_device_info
        l_virtual_device_info=$("${l_megacli_path}" -LDInfo -Lall -a0 2> /dev/null | sed -r -n '/Target Id/,/^$/{/^Default[[:space:]]+/d;p}')

        fn_CentralOutput 'Name' "$(fn_MegaRAIDInfoDetection 'Name' "${l_virtual_device_info}")"

        # RAID Level
        # RAID 1 -- Primary-1, Secondary-0, RAID Level Qualifier-0
        # RAID 0 -- Primary-0, Secondary-0, RAID Level Qualifier-0
        # RAID 5 -- Primary-5, Secondary-0, RAID Level Qualifier-3
        # RAID 10 -- Primary-1, Secondary-3, RAID Level Qualifier-0
        fn_CentralOutput 'RAID Level' "$(fn_MegaRAIDInfoDetection 'RAID Level' "${l_virtual_device_info}")"
        fn_CentralOutput 'Size' "$(fn_MegaRAIDInfoDetection 'Size' "${l_virtual_device_info}")"
        fn_CentralOutput 'Sector Size' "$(fn_MegaRAIDInfoDetection 'Sector Size' "${l_virtual_device_info}")"
        fn_CentralOutput 'Parity Size' "$(fn_MegaRAIDInfoDetection 'Parity Size' "${l_virtual_device_info}")"
        fn_CentralOutput 'State' "$(fn_MegaRAIDInfoDetection 'State' "${l_virtual_device_info}")"
        fn_CentralOutput 'Strip Size' "$(fn_MegaRAIDInfoDetection 'Strip Size' "${l_virtual_device_info}")"
        fn_CentralOutput 'Number Of Drives' "$(fn_MegaRAIDInfoDetection 'Number Of Drives' "${l_virtual_device_info}")"
        fn_CentralOutput 'Span Depth' "$(fn_MegaRAIDInfoDetection 'Span Depth' "${l_virtual_device_info}")"
        fn_CentralOutput 'Current Access Policy' "$(fn_MegaRAIDInfoDetection 'Current Access Policy' "${l_virtual_device_info}")"
        fn_CentralOutput 'Disk Cache Policy' "$(fn_MegaRAIDInfoDetection 'Disk Cache Policy' "${l_virtual_device_info}")"
        fn_CentralOutput 'Bad Blocks Exist' "$(fn_MegaRAIDInfoDetection 'Bad Blocks Exist' "${l_virtual_device_info}")"


        # - Disk Info
        # ./MegaCli64 -NoLog -PDList -aAll       # disk info
        # ./MegaCli64 -NoLog -cfgdsply -aAll    # Raid info, raid setting, disk info
        fn_CentralOutputTitle 'Disk Information' '1'

        "${l_megacli_path}" -PDList -aAll | sed -r -n '/Enclosure Device ID/,/S.M.A.R.T alert/{/^$/d;/S.M.A.R.T alert/{s@.*@&\n---@g};s@[[:space:]]*$@@g;/^(Enclosure Device ID)/d;p}' | sed -r ':a;N;$!ba;s@\n@|@g;s@[|][-]+[|]?@\n@g;' | sed '/^$/d' | while read -r line; do
            raw_info=$(echo "${line}" | sed -r -n 's@\|@\n@g;s@'\''s@@g;p')
            fn_CentralOutput 'Slot Number' "$(fn_MegaRAIDInfoDetection 'Slot Number' "${raw_info}")"
            fn_CentralOutput "Drive's position" "$(fn_MegaRAIDInfoDetection "Drive position" "${raw_info}")"    # Drive's position
            fn_CentralOutput 'Enclosure position' "$(fn_MegaRAIDInfoDetection 'Enclosure position' "${raw_info}")"
            fn_CentralOutput 'Device Id' "$(fn_MegaRAIDInfoDetection 'Device Id' "${raw_info}")"
            # WWN(World Wide Name)
            fn_CentralOutput 'WWN' "$(fn_MegaRAIDInfoDetection 'WWN' "${raw_info}")"
            fn_CentralOutput 'Sequence Number' "$(fn_MegaRAIDInfoDetection 'Sequence Number' "${raw_info}")"
            fn_CentralOutput 'PD Type' "$(fn_MegaRAIDInfoDetection 'PD Type' "${raw_info}")"
            fn_CentralOutput 'Raw Size' "$(fn_MegaRAIDInfoDetection 'Raw Size' "${raw_info}")"
            fn_CentralOutput 'Inquiry Data' "$(fn_MegaRAIDInfoDetection 'Inquiry Data' "${raw_info}")"
            fn_CentralOutput 'Device Speed' "$(fn_MegaRAIDInfoDetection 'Device Speed' "${raw_info}")"
            fn_CentralOutput 'Media Type' "$(fn_MegaRAIDInfoDetection 'Media Type' "${raw_info}")"
            fn_CentralOutput 'Drive Temperature' "$(fn_MegaRAIDInfoDetection 'Drive Temperature' "${raw_info}")"
            fn_CentralOutput 'S.M.A.R.T alert' "$(fn_MegaRAIDInfoDetection 'Drive has flagged a S.M.A.R.T alert' "${raw_info}")"
            fn_CentralOutputTitle
        done

    fi
}

fn_DiskInfo(){
    fn_CentralOutputTitle 'Phyical Disk Info'

    # lshw -class disk
    # udevadm info --query=all --name=/dev/sda
    # hdparm -I /dev/hda
    # fdisk -l
    # sfdisk -l -uM
    # lsscsi
    # parted -l
    # lsblk
    # blkid
    # lshw -class disk
    # lshw -class disk -class storage
    # lshw -short -C disk

    # No RAID
    if [[ -z "$(lspci | sed -r -n '/RAID/p')" ]]; then
        fdisk -l 2>&1 | sed -r -n '/^Disk[[:space:]]*\//{/\/(mapper|ram)/d;s@^Disk[[:space:]]*([^:]+).*@\1@g;s@^Disk[[:space:]]*([^:]+).*@\1@g;p}' | while read -r line; do
            smartctl -H -d auto -i "${line}" 2> /dev/null
            # SMART overall-health self-assessment rest result: PASSED
            fn_CentralOutputTitle
        done
    fi
}


#########  3. Executing Process  #########
fn_ExecutingProcess(){
    fn_InitializationCheck

    if [[ "${list_all_type}" -ne 1 ]]; then
        case "${type_specify}" in
            bios ) fn_BIOSInfo ;;
            baseboard )
                fn_BaseboardInfo
                fn_OnBoardDeviceInfo
                fn_OnBoardDeviceExtendedInfo
                ;;
            chassis ) fn_ChassisInfo ;;
            memory ) fn_MemoryInfo ;;
            cache ) fn_CacheInfo ;;
            port ) fn_PortConnectorInfo ;;
            slot ) fn_SystemSlotInfo ;;
            point ) fn_BuiltinPointingDeviceInfo ;;
            battery ) fn_PortableBatteryInfo ;;
            fan ) fn_CoolingDeviceInfo ;;
            temperature ) fn_TemperatureProbeInfo ;;
            disk )
                fn_DiskRAIDInfo
                fn_DiskInfo
            ;;
            product|* ) fn_ProductInfo ;;
        esac
    else
        fn_ProductInfo
        fn_BIOSInfo
        fn_BaseboardInfo
        fn_OnBoardDeviceInfo
        fn_OnBoardDeviceExtendedInfo
        fn_ChassisInfo
        fn_MemoryInfo
        fn_CacheInfo
        fn_PortConnectorInfo
        fn_SystemSlotInfo
        fn_BuiltinPointingDeviceInfo
        fn_PortableBatteryInfo
        fn_CoolingDeviceInfo
        fn_TemperatureProbeInfo
        fn_DiskRAIDInfo
        fn_DiskInfo
    fi
}

fn_ExecutingProcess


#########  4. EXIT Singal Processing  #########
# trap "commands" EXIT # execute command when exit from shell
fn_TrapEXIT(){
    unset list_all_type
    unset type_specify
    rm -rf /tmp/"${mktemp_format%%_*}"* 2>/dev/null
}

trap fn_TrapEXIT EXIT

# Script End
