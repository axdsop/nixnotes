# Toolkits Script

**Toolkits** is personal custom function collections.

**Note**: Part of my shell scripts have been moved to another project **[Unix-like Dotfiles](https://gitlab.com/axdsop/nix-dotfiles/-/tree/master/Scripts)** on Oct 08, 2020.


## TOC

1. [Applicaitons](#applicaitons)  
2. [GNU/Linux Relevant](#gnulinux-relevant)  
3. [Tools](#tools)  
3.1 [Network](#network)  
3.2 [Miscellaneous](#miscellaneous)  


## Applicaitons

Redirect to [README](./Application/).

## GNU/Linux Relevant

* [GNU/Linux Official Documents Downlaod](./GNULinux/gnuLinuxOfficialDocumentationDownload.sh)
* [GNU/Linux Machine Info Detection](./GNULinux/gnuLinuxMachineInfoDetection.sh)
* [GNU/Linux Life Cycle Info](./GNULinux/gnuLinuxLifeCycleInfo.sh)
* [GNU/Linux FIrewall Rule Configuration](./GNULinux/gnuLinuxFirewallRuleConfiguration.sh)
* [GNU/Linux Post-installation Configuration](./GNULinux/gnuLinuxPostInstallationConfiguration.sh)
* [GNU/Linux GNOME Desktop Configuration](./GNULinux/gnuLinuxGnomeDesktopConfiguration.sh)


## Tools


### Miscellaneous

* [Rename image/video via exiftool](./Miscellaneous/rename_image_video_via_exiftool.sh)
* [PostgreSQL Variants Version Relation Table](./Miscellaneous/postgresql_version_relation_table.sh)
* [MySQL Variants Version Relation Table](./Miscellaneous/mysql_variants_version_relation_table.sh)


<!-- End -->
