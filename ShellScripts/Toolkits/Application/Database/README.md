# Toolkits - Application / Database

## List

* [MySQL/MariaDB/Percona](./MySQLVariants.sh)
* [PostgreSQL](./PostgreSQL.sh)
* [Redis](./Redis.sh)
* [MongoDB](./MongoDB.sh)

<!-- End -->
