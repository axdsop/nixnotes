#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails


# Official site: https://www.postgresql.org
# Documentation
# - https://www.postgresql.org/docs/manuals/
# - https://help.ubuntu.com/community/PostgreSQL


# Target: Installing PostgreSQL Via Their Official Repository On GNU/Linux (RHEL/CentOS/Fedora/Debian/Ubuntu/SLES/OpenSUSE)
# Developer: MaxdSre

# Change Log:
# - Oct 08, 2020 08:50 Thu ET - initialization check method update
# - Oct 15, 2019 12:19 Tue ET - change custom script url
# - Oct 14, 2019 12:26 Fri  ET - change to new base functions
# - Dec 31, 2018 21:35 Mon ET - code reconfiguration, include base funcions from other file
# - June 18, 2018 14:22 Thu ET - not complete


# Life time List Extraction
# https://www.postgresql.org/support/versioning/
# curl -fsL https://www.postgresql.org/support/versioning/ | sed -r -n '/<table/,/<\/table>/{s@^[[:blank:]]*@@g;/<\/(td|tr)>/!d;p}' | sed ':a;N;$!ba;s@\n@@g;s@<\/tr>@\n@g;s@<\/td>@|@g;s@<[^>]*>@@g;' | sed '/^$/d;s@|$@@g'

# sudo -u postgres psql


#########  0-1. Singal Setting  #########
trap fnBase_TrapCleanUp INT QUIT

#########  0-2. Variables Setting  #########
# umask 022    # temporarily change umask value to 022
readonly custom_shellscript_url='https://gitlab.com/axdsop/nixnotes/raw/master/ShellScripts/Toolkits'
readonly os_check_script="${custom_shellscript_url}/GNULinux/gnuLinuxMachineInfoDetection.sh"
readonly firewall_configuration_script="${custom_shellscript_url}/GNULinux/gnuLinuxFirewallRuleConfiguration.sh"
readonly distro_relation_list="${custom_shellscript_url}/_Configs/Source/PostgreSQLVersionAndLinuxDistroRelationTable.txt"

readonly psql_port_default='5432'
psql_port=${psql_port:-"${psql_port_default}"}
readonly data_dir_default='/var/lib/postgresql'
data_dir=${data_dir:-"${data_dir_default}"}
service_name=${service_name:-'postgresql'}

os_detect=${os_detect:-0}
enable_firewall=${enable_firewall:-0}
proxy_server_specify=${proxy_server_specify:-''}


#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | sudo bash -s -- [options] ...

InstallingPostgreSQL On GNU/Linux Via Official Repository!
This script requires superuser privileges (eg. root, su).

[available option]
    -h    --help, show help info
    -o    --os info, detect os distribution info
    -v variant_version --set specific version (eg: 10|9.6|9.5)
    -d data_dir    --set data dir, default is /var/lib/mysql
    -s port    --set PostgreSQL port number, default is 3306
    -f    --enable firewall rule (iptable/firewalld/ufw/SuSEfirewall2), default is disable
    -P [protocol:]ip:port    --proxy host (http|https|socks4|socks5), default protocol is http
\e[0m"
}


while getopts "hod:s:v:fP:" option "$@"; do
    case "$option" in
        o ) os_detect=1 ;;
        d ) data_dir="$OPTARG" ;;
        s ) psql_port="$OPTARG" ;;
        # b ) bind_ip="$OPTARG" ;;
        v ) variant_version="$OPTARG" ;;
        f ) enable_firewall=1 ;;
        P ) proxy_server_specify="$OPTARG" ;;
        h|\? ) fn_HelpInfo && exit;;
    esac
done


#########  1-2 Preparation Operation  #########
fn_InitializationCheck(){
    local l_user_codebase_dir="$HOME/.config/axdsop"
    local l_base_function_temp_save_path=''
    local l_need_remove=0
    local l_base_function_url='https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/base_functions.sh'
    l_base_function_temp_save_path=$(find "${l_user_codebase_dir}" -type f -name "${l_base_function_url##*/}" -print 2> /dev/null)
    
    if [[ ! -f "${l_base_function_temp_save_path}" ]]; then
        local l_download_tool=${l_download_tool:-'wget -qO-'}
        [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_tool='curl -fsL'
        l_need_remove=1
        l_base_function_temp_save_path=$(mktemp -t XXXXXXX)
        ${l_download_tool} "${l_base_function_url}" > "${l_base_function_temp_save_path}"
    fi

    if [[ -f "${l_base_function_temp_save_path}" && -s "${l_base_function_temp_save_path}" ]]; then
        . "${l_base_function_temp_save_path}"
        [[ "${l_need_remove}" -eq 1 ]] && rm "${l_base_function_temp_save_path}"
    else
        echo "Sorry, fail to get base script path."
        exit
    fi


    # tput clear  # Echo the clear-screen sequence for the current terminal.
    # specify 1 means need sudo privilege
    fnBase_RunningEnvironmentCheck '1'

    if [[ "${os_detect}" -eq 1 ]]; then
        $download_method "${os_check_script}" | bash -s --
        exit
    fi
}


fn_OSInfoDetection(){
    local l_json_osinfo=${l_json_osinfo:-}
    l_json_osinfo=$(fnBase_OSInfoRetrieve "${os_check_script}")
    # [[ -z "${l_json_osinfo}" ]] && fnBase_ExitStatement "${c_red}Fatal${c_normal}: fail to extract os info!"

    distro_fullname=${distro_fullname:-}
    distro_fullname=$(fnBase_OSInfoDirectiveValExtraction 'distro_fullname' "${l_json_osinfo}")

    pack_manager=${pack_manager:-}
    pack_manager=$(fnBase_OSInfoDirectiveValExtraction 'distro_pack_manager' "${l_json_osinfo}")

    case "${pack_manager}" in
        yum|dnf|apt-get )
            echo ''
            ;;
        * )
            fnBase_ExitStatement "${c_red}Sorry${c_normal}: your ${c_yellow}${distro_fullname}${c_normal} may not be supported by PostgreSQL official repo currently!"
            ;;
    esac

    distro_name=${distro_name:-}
    distro_name=$(fnBase_OSInfoDirectiveValExtraction 'distro_name' "${l_json_osinfo}")
    distro_name="${distro_name%%-*}"    # centos, fedora

    codename=${codename:-}
    codename=$(fnBase_OSInfoDirectiveValExtraction 'distro_codename' "${l_json_osinfo}")

    version_id=${version_id:-}
    version_id=$(fnBase_OSInfoDirectiveValExtraction 'distro_version_id' "${l_json_osinfo}")

    # ip_public=${ip_public:-}
    # ip_public=$(fnBase_OSInfoDirectiveValExtraction 'ip_public' "${l_json_osinfo}")

    firewall_type=${firewall_type:-}
    firewall_type=$(fnBase_OSInfoDirectiveValExtraction 'distro_firewall_type' "${l_json_osinfo}")
}

fn_InstallationProcedureStatement(){
    local l_item="${1:-}"

    if [[ -n "${l_item}" ]]; then
        case "${l_item,,}" in
            repo ) l_item='official repository' ;;
            pack ) l_item='package installation' ;;
            gpg ) l_item='GnuPG key installation'
        esac
        fnBase_OperationProcedureStatement "${l_item}"
        # printf "procedure - ${c_blue}%s${c_normal} is finished;\n" "${l_item}"
    fi
    # fnBase_OperationProcedureResult
}



################ 2-1. Choose Database & Version ################
fn_ExistedDetection(){
    fnBase_CentralOutputTitle 'PostgreSQL'
    fnBase_OperationPhaseStatement 'Check If Existed'

    fnBase_OperationProcedureStatement 'Detection'
    if fnBase_CommandExistIfCheck 'psql'; then
        fnBase_OperationProcedureResult
        fnBase_ExitStatement "\n${c_red}Attention: ${c_normal}find existed PostgreSQL:\n\n${c_yellow}$(psql --version)${c_normal}"
    else
        fnBase_OperationProcedureResult 'not find'
    fi
}

# verify port num or data dir is legal or not
fn_ParaSpecifiedValVerification(){
    fnBase_OperationPhaseStatement 'Parameters Specified Val Verification'

    # 1- verify if the port specified is available
    fnBase_OperationProcedureStatement 'Port No. verification'
    psql_port=$(echo "${psql_port}" | sed -r 's@[[:alpha:]]*@@g;s@[[:punct:]]*@@g;s@[[:blank:]]*@@g')
    if [[ -n "${psql_port}" ]]; then
        if [[ "${psql_port}" -eq "${psql_port_default}" ]]; then
            fnBase_OperationProcedureResult "${psql_port}"
        elif [[ "${psql_port}" =~ ^[1-9]{1}[0-9]{1,4}$ ]]; then
            local sys_port_start=${sys_port_start:-1024}
            local sys_port_end=${sys_port_end:-65535}

            if [[ "${psql_port}" -ge "${sys_port_start}" && "${psql_port}" -le "${sys_port_end}" ]]; then
                local port_service_info
                port_service_info=$(awk 'match($2,/^'"${psql_port}"'\/tcp$/){print $1,$2}' /etc/services 2> /dev/null)
                if [[ -n "${port_service_info}" ]]; then
                    fnBase_OperationProcedureResult "${port_service_info} in /etc/services" 1
                else
                    fnBase_OperationProcedureResult "${psql_port}"
                fi    # end if port_service_info
            else
                fnBase_OperationProcedureResult "${psql_port}⊄(${sys_port_start},${sys_port_end}) out of range" 1
            fi

        else
            fnBase_OperationProcedureResult "${psql_port} illegal" 1
        fi

    fi

    # 2- verify data dir
    fnBase_OperationProcedureStatement 'Data dir verification'
    if [[ -n "${data_dir}" ]]; then
        data_dir="${data_dir%/}"
        if [[ "${data_dir}" == "${data_dir_default}" || "${data_dir}" =~ ^/ ]]; then
            fnBase_OperationProcedureResult "${data_dir}"
        else
            fnBase_OperationProcedureResult "${data_dir}" 1
            fnBase_ExitStatement "${c_red}Attention: ${c_normal}data dir path must be begin with slash ${c_yellow}/${c_normal} or ${c_yellow}~/${c_normal}."
        fi
    fi
}

fn_VersionSelectionMenu(){
    local l_distro_relation_table=${l_distro_relation_table:-}
    l_distro_relation_table=$($download_method "${distro_relation_list}" 2> /dev/null)

    local l_version_list=${l_version_list:-''}
    case "${pack_manager}" in
        yum|dnf )
            l_version_list=$(echo "${l_distro_relation_table}" | sed -r -n '/rpm\|'"${distro_name}${version_id%%.*}"'/{p}')
            ;;
        apt-get )
            l_version_list=$(echo "${l_distro_relation_table}" | sed -r -n '/deb\|'"${codename}"'/{p}')
            ;;
    esac

    l_version_list="${l_version_list##*|}"

    # 1 - specific version choose
    echo -e "\n${c_yellow}Please Select Specific${c_normal} ${c_red}PostgreSQL${c_normal}${c_yellow} Version: ${c_normal}"
    PS3="Choose version number: "

    choose_version=${choose_version:-}
    select item in $(echo "${l_version_list}" | sed 's@ @\n@g' | sort -rn | sed 's@\n@ @g'); do
        choose_version="${item,,}"
        [[ -n "${choose_version}" ]] && break
    done < /dev/tty

    unset PS3
    printf "\nProduct version you choose is ${c_bold}${c_yellow}%s${c_normal}.\n\n" "${choose_version}"
    choose_version_short="${choose_version//.}"
}


################ 3-1. Repository Configuration ################
fn_RepoConfiguration(){
    local l_repo_path

    case "${pack_manager}" in
        dnf|yum )
            # https://apt.postgresql.org/pub/repos/yum/InstallPGDGRepoRPM.sh
            fnBase_OperationProcedureStatement 'repo'
            l_repo_path='/etc/yum.repos.d/pgdg.repo'

            local l_distro_name_long=${l_distro_name_long:-}
            case "${distro_name}" in
                fedora ) l_distro_name_long='fedora' ;;
                rhel|centos ) l_distro_name_long='redhat' ;;
            esac

            local l_yum_repo_link=${l_yum_repo_link:-'https://download.postgresql.org/pub/repos/yum'}
            local l_gpg_key_path=${l_gpg_key_path:-"/etc/pki/rpm-gpg/RPM-GPG-KEY-PGDG-$choose_version_short"}

            [[ -f "${l_gpg_key_path}" ]] || $download_method "${l_yum_repo_link}/RPM-GPG-KEY-PGDG-${choose_version_short}" > "${l_gpg_key_path}"

            echo -e "[pgdg${choose_version_short}]\nname=PostgreSQL ${choose_version} \$releasever - \$basearch\nbaseurl=${l_yum_repo_link}/${choose_version}/${l_distro_name_long}/${distro_name}-\$releasever-\$basearch\nenabled=1\ngpgcheck=1\ngpgkey=file://${l_gpg_key_path}" > "${l_repo_path}"

            echo -e "[pgdg${choose_version_short}-source]\nname=PostgreSQL ${choose_version} \$releasever - \$basearch - Source\nfailovermethod=priority\nbaseurl=${l_yum_repo_link}/srpms/${choose_version}/${l_distro_name_long}/${distro_name}-\$releasever-\$basearch\nenabled=0\ngpgcheck=1\ngpgkey=file://${l_gpg_key_path}" >> "${l_repo_path}"

            echo -e "[pgdg${choose_version_short}-updates-testing]\nname=PostgreSQL ${choose_version} \$releasever - \$basearch - Updates Testing\nbaseurl=${l_yum_repo_link}/testing/${choose_version}/${l_distro_name_long}/${distro_name}-\$releasever-\$basearch\nenabled=0\ngpgcheck=1\ngpgkey=file://${l_gpg_key_path}" >> "${l_repo_path}"

            echo -e "[pgdg${choose_version_short}-source-updates-testing]\nname=PostgreSQL ${choose_version} \$releasever - \$basearch - Source Testing\nfailovermethod=priority\nbaseurl=${l_yum_repo_link}/srpms/testing/${choose_version}/${l_distro_name_long}/${distro_name}-\$releasever-\$basearch\nenabled=0\ngpgcheck=1\ngpgkey=file://${l_gpg_key_path}" >> "${l_repo_path}"
            fnBase_OperationProcedureResult "${l_repo_path}"
            ;;
        apt-get )
            # https://salsa.debian.org/postgresql/postgresql-common/raw/master/pgdg/apt.postgresql.org.sh
            fnBase_OperationProcedureStatement 'repo'
            l_repo_path='/etc/apt/sources.list.d/pgdg.list'
            echo "deb https://apt.postgresql.org/pub/repos/apt/ ${codename}-pgdg main" > "${l_repo_path}"
            fnBase_OperationProcedureResult "${l_repo_path}"

            # - import GnuPG key
            fnBase_OperationProcedureStatement 'GnuPG key importing'
            # https://apt.postgresql.org/pub/repos/apt/ACCC4CF8.asc
            $download_method https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add - &> /dev/null
            fnBase_OperationProcedureResult 'ACCC4CF8'
            ;;
    esac
    fnBase_PackageManagerOperation
}

################ 3-2. Installation ################
fn_DBInstallationOperation(){
    local l_pack_list=${l_pack_list:-}

    case "${pack_manager}" in
        dnf|yum )
            l_pack_list="postgresql${choose_version_short}-server"
            service_name="postgresql-${choose_version}"

            # service postgresql-9.6 initdb
            # chkconfig postgresql-9.6 on
            # service postgresql-9.6 start

            # /usr/pgsql-9.6/bin/postgresql96-setup initdb
            # systemctl enable postgresql-9.6
            # systemctl start postgresql-9.6

            ;;
        apt-get )
            # pgAdmin 4 graphical administration utility
            # wheezy/jessie/trusty: pgadmin3
            local l_pgadmin_name=${l_pgadmin_name:-'pgadmin4'}
            case "${distro_name}" in
                debian )
                    [[ "${version_id%%.*}" -lt 9 ]] && l_pgadmin_name='pgadmin3'
                    ;;
                ubuntu )
                    [[ "${version_id%%.*}" -lt 16 ]] && l_pgadmin_name='pgadmin3'
                    ;;
            esac

            l_pack_list="postgresql-${choose_version} postgresql-contrib ${l_pgadmin_name}"
            service_name='postgresql'
            ;;
    esac

    fn_InstallationProcedureStatement 'pack'
    fnBase_PackageManagerOperation 'install' "${l_pack_list}"
    fnBase_OperationProcedureResult "${l_pack_list}"

    fnBase_SystemServiceManagement "${service_name}" 'enable'
    fnBase_SystemServiceManagement "${service_name}" 'start'
}


################ 3-2. Configuration ################
fn_DirectiveSetting(){
    local l_item="${1:-}"
    local l_val="${2:-}"
    local l_path="${3:-}"
    local l_action=${4:-0}
    if [[ -n "${l_item}" && -n "${l_val}" && -s "${l_path}" ]]; then
        [[ "${l_action}" -eq 0 ]] && fnBase_OperationProcedureStatement "${l_item}"
        if [[ "${l_val}" =~ @ ]]; then
            sed -r -i '/^#?[[:space:]]*'"${l_item}"'[[:space:]]*=/{s/^#?[[:space:]]*([^=]+=[[:space:]]*)[^[:space:]]*(.*)/\1'"${l_val}"'\2/g;}' "${l_path}"
        else
            sed -r -i '/^#?[[:space:]]*'"${l_item}"'[[:space:]]*=/{s@^#?[[:space:]]*([^=]+=[[:space:]]*)[^[:space:]]*(.*)@\1'"${l_val}"'\2@g;}' "${l_path}"
        fi

        [[ "${l_action}" -eq 0 ]] && fnBase_OperationProcedureResult "${l_val}"
    fi
}

fn_ConfigConfiguration(){
    fnBase_OperationPhaseStatement 'Config File Operation'

    # 1 - detect default config file path - conf_path
    fnBase_OperationProcedureStatement 'Config path detection'
    local l_config_path=${l_config_path:-"/etc/postgresql/${choose_version}/main/postgresql.conf"}
    if [[ -f "${l_config_path}" ]]; then
        fnBase_OperationProcedureResult "${l_config_path}"
    else
        fnBase_OperationProcedureResult "${l_config_path}" 1
    fi


    # 2 - config file configuration
    fnBase_OperationProcedureStatement 'Directives configuration'
    # 2.1 - backup origin conf file
    local l_origin_conf_bak_path
    l_origin_conf_bak_path="${l_config_path}${bak_suffix}"
    [[ -f "${l_origin_conf_bak_path}" ]] || cp -fp "${l_config_path}" "${l_origin_conf_bak_path}"

    # - FILE LOCATIONS
    # data_directory = '/var/lib/postgresql/10/main'
    # hba_file = '/etc/postgresql/10/main/pg_hba.conf'
    # ident_file = '/etc/postgresql/10/main/pg_ident.conf'
    # external_pid_file = '/var/run/postgresql/10-main.pid'

    # - CONNECTIONS AND AUTHENTICATION
    # listen_addresses = 'localhost'
    fn_DirectiveSetting 'port' "${psql_port}" "${l_config_path}"
    fn_DirectiveSetting 'max_connections' '512' "${l_config_path}"
    # unix_socket_directories = '/var/run/postgresql'
    # unix_socket_group = ''
    # unix_socket_permissions = 0777

    # - Security and Authentication

    # - RESOURCE USAGE (except WAL)

    # - WRITE AHEAD LOG

    # - REPLICATION

    # - QUERY TUNING

    # - ERROR REPORTING AND LOGGING

    # - RUNTIME STATISTICS

    # - AUTOVACUUM PARAMETERS

    # - CLIENT CONNECTION DEFAULTS

    # - LOCK MANAGEMENT

    # - VERSION/PLATFORM COMPATIBILITY

    # - ERROR HANDLING

    # - CONFIG FILE INCLUDES

    fnBase_SystemServiceManagement "${service_name}" 'restart'
}

################ 3-3. Security ################
fn_SecurityEnhancementConfiguration(){
    fnBase_OperationPhaseStatement 'Security Utilities Configuration'

    # 1 - Firewall
    if [[ "${enable_firewall}" -eq 1 ]]; then
        fnBase_OperationProcedureStatement 'Firewall configuration'
        $download_method "${firewall_configuration_script}" | bash -s -- -p "${psql_port}" -s
        fnBase_OperationProcedureResult "${firewall_type} ${psql_port}"
    fi
}


#########  3. Executing Process  #########
fn_InitializationCheck

fn_OSInfoDetection

fn_ExistedDetection
fn_ParaSpecifiedValVerification
fn_VersionSelectionMenu
fn_RepoConfiguration
fn_DBInstallationOperation
fn_ConfigConfiguration
fn_SecurityEnhancementConfiguration


#########  4. EXIT Singal Processing  #########
# trap "commands" EXIT # execute command when exit from shell
fn_TrapEXIT(){
    fnBase_UnsetVars
    unset psql_port
    unset data_dir
    unset service_name
    unset os_detect
    unset enable_firewall
    unset proxy_server_specify
}

trap fn_TrapEXIT EXIT


# Script End
