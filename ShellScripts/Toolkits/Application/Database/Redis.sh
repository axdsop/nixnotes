#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails


# Official Site: https://redis.io/
# Documentation
# - https://redis.io/documentation
# - https://github.com/antirez/redis-doc
# - https://redis.io/topics/quickstart
# - https://redis.io/topics/admin
# - https://redislabs.com/blog/stunnel-secure-redis-ssl/
# - https://redis.io/topics/cluster-tutorial


# Target: Automatically Install & Update Redis On GNU/Linux
# Developer: MaxdSre

# Change Log:
# - Oct 08, 2020 08:52 Thu ET - initialization check method update
# - Oct 14, 2019 12:08 Fri ET - change to new base functions
# - Jan 01, 2019 10:52 Tue ET - code reconfiguration, include base funcions from other file
# - Aug 04, 2018 08:35 Sat +0800 - not update config files while update
# - May 23, 2018 22:38 Wed ET - code reconfiguration
# - Jan 16, 2018 19:22 Tue +0800

# redis-cli INFO

# Make sure the port Redis uses to listen for connections (by default 6379 and additionally 16379 if you run Redis in cluster mode, plus 26379 for Sentinel) is firewalled, so that it is not possible to contact Redis from the outside world.

# For remote connection
# redis server: change directive `bind` in redis.conf, open port 6379 in firewall
# local:    redis-cli -h $REMOTE_IP -p 6379

# If set password directive `requirepass` in redis.conf
# redis-cli -h $REMOTE_IP -p 6379 -a $PASS  or use `auth $PASS` without -a
# redis daemon stop/restart need specify password too

# > INFO
# NOAUTH Authentication required.
# > keys *
# (error) NOAUTH Authentication required.

# Attention: If you wanna change password or disable passwd, you need to stop redis daemon first

# Usage
# INFO  <-- for monitor  https://github.com/oliver006/redis_exporter
# CONFIG GET loglevel / CONFIG get requirepass / CONFIG GET *



#########  0-1. Singal Setting  #########
trap fnBase_TrapCleanUp INT QUIT

#########  0-2. Variables Setting  #########
# umask 022    # temporarily change umask value to 022
readonly custom_shellscript_url='https://gitlab.com/axdsop/nixnotes/raw/master/ShellScripts/Toolkits'
readonly os_check_script="${custom_shellscript_url}/GNULinux/gnuLinuxMachineInfoDetection.sh"
readonly firewall_configuration_script="${custom_shellscript_url}/GNULinux/gnuLinuxFirewallRuleConfiguration.sh"
readonly online_service_script_sample_dir="${custom_shellscript_url}/_Configs/Application/redis/initScript"

readonly official_site='https://redis.io'
readonly download_page="${official_site}/download"
readonly software_fullname=${software_fullname:-'Redis'}
readonly application_name=${application_name:-'redis'}
daemon_name=${daemon_name:-'redis-server'}
readonly bashrc_string="${software_fullname%% *} configuration"
installation_dir="/opt/${software_fullname}"
readonly redis_port_default='6379'
redis_port=${redis_port:-"${redis_port_default}"}
config_dir=${config_dir:-"/etc/${application_name}"}
log_dir=${log_dir:-"/var/log/${application_name}"}
readonly data_dir_default="/var/lib/${application_name}"
data_dir=${data_dir:-"${data_dir_default}"}
profile_d_path="/etc/profile.d/${application_name}.sh"

user_name=${user_name:-'root'}
group_name=${group_name:-'root'}

is_existed=${is_existed:-1}   # Default value is 1, assumming application existed
version_check=${version_check:-0}
auth_pass_enable=${auth_pass_enable:-0}
auth_pass=${auth_pass:-}
strict_mode_enable=${strict_mode_enable:-0}
enable_firewall=${enable_firewall:-0}
source_pack_path=${source_pack_path:-}
is_uninstall=${is_uninstall:-0}
proxy_server_specify=${proxy_server_specify:-}
update_operation=${update_operation:-0}



#########  1-1 Initialization Prepatation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | sudo bash -s -- [options] ...

Installing / Updating Redis In-memory Database On GNU/Linux!
This script requires superuser privileges (eg. root, su).

[available option]
    -h    --help, show help info
    -c    --check, check current stable release version
    -f pack_path    --specify latest release package save path (e.g. /PATH/*.tar.gz) or dir (default is ~/Downloads/) in system
    -d data_dir    --set data dir, default is /var/lib/redis
    -s port    --set redis server port number, default is 6379
    -a --enable auth password, default is disable
    -r    --use strict mode (create user redis, default use root), or delete user redis, config dir, data dir while uninstall
    -F    --enable firewall rule (iptable/firewalld/ufw/SuSEfirewall2), default is disable
    -p [protocol:]ip:port    --proxy host (http|https|socks4|socks5), default protocol is http
    -u    --uninstall, uninstall software installed
\e[0m"
}

#########  1-2 getopts Operation  #########
while getopts "hcd:s:arFf:up:" option "$@"; do
    case "$option" in
        c ) version_check=1 ;;
        d ) data_dir="$OPTARG" ;;
        s ) redis_port="$OPTARG" ;;
        a ) auth_pass_enable=1 ;;
        r ) strict_mode_enable=1 ;;
        F ) enable_firewall=1 ;;
        f ) source_pack_path="$OPTARG" ;;
        u ) is_uninstall=1 ;;
        p ) proxy_server_specify="$OPTARG" ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done


#########  1-2 Preparation Operation  #########
fn_InitializationCheck(){
    local l_user_codebase_dir="$HOME/.config/axdsop"
    local l_base_function_temp_save_path=''
    local l_need_remove=0
    local l_base_function_url='https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/base_functions.sh'
    l_base_function_temp_save_path=$(find "${l_user_codebase_dir}" -type f -name "${l_base_function_url##*/}" -print 2> /dev/null)
    
    if [[ ! -f "${l_base_function_temp_save_path}" ]]; then
        local l_download_tool=${l_download_tool:-'wget -qO-'}
        [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_tool='curl -fsL'
        l_need_remove=1
        l_base_function_temp_save_path=$(mktemp -t XXXXXXX)
        ${l_download_tool} "${l_base_function_url}" > "${l_base_function_temp_save_path}"
    fi

    if [[ -f "${l_base_function_temp_save_path}" && -s "${l_base_function_temp_save_path}" ]]; then
        . "${l_base_function_temp_save_path}"
        [[ "${l_need_remove}" -eq 1 ]] && rm "${l_base_function_temp_save_path}"
    else
        echo "Sorry, fail to get base script path."
        exit
    fi


    # tput clear  # Echo the clear-screen sequence for the current terminal.
    # specify 1 means need sudo privilege
    fnBase_RunningEnvironmentCheck '1'

    fnBase_CommandExistCheckPhase 'gzip' # CentOS/Debian/OpenSUSE: gzip
    fnBase_CommandExistCheckPhase 'tar' # .tar.gz
}


#########  2-1. Uninstall Operation  #########
fn_UninstallOperation(){
    if [[ "${is_uninstall}" -eq 1 ]]; then

        if [[ -d "${installation_dir}" ]]; then
            local l_service_script_dir='/etc/init.d'
            fnBase_CommandExistIfCheck 'systemctl' && l_service_script_dir='/etc/systemd/system'

            # - stop daemon running
            find "${l_service_script_dir}"/ -type f -name ''"${daemon_name}"'*' -print | while IFS="" read -r line; do
                if [[ -n "${line}" ]]; then
                    service_name="${line##*/}"
                    service_name="${service_name%%.*}"
                    fnBase_SystemServiceManagement "${service_name}" 'stop'
                    # - remove system init script   SysV init / Systemd
                    [[ -f "${line}" ]] && rm -f "${line}"
                    unset service_name
                fi
            done

            [[ -f "${login_user_home}/.rediscli_history" ]] && rm -f "${login_user_home}/.rediscli_history"

            # - remove data dir
            [[ -d "${log_dir}" ]] && rm -rf "${log_dir}"
            [[ -s "${profile_d_path}" ]] && rm -f "${profile_d_path}"
            [[ -d "${installation_dir}" ]] && rm -rf "${installation_dir}"

            if [[ "${strict_mode_enable}" -eq 1 ]]; then
                [[ -d "${config_dir}" ]] && rm -rf "${config_dir}"
                [[ -d "${data_dir}" ]] && rm -rf "${data_dir}"

                # - remove user and group - redis
                if [[ -n $(sed -r -n '/^'"${application_name}"':/{p}' /etc/passwd) ]]; then
                    userdel -fr "${application_name}" 2> /dev/null
                    groupdel -f "${application_name}" 2> /dev/null
                fi
            fi

            [[ -f "${login_user_home}/.bashrc" ]] && sed -r -i '/'"${bashrc_string}"' start/,/'"${bashrc_string}"' end/d' "${login_user_home}/.bashrc"

            fnBase_CommandExistIfCheck 'systemctl' && systemctl daemon-reload 2> /dev/null
            fnBase_ExitStatement "${software_fullname} is successfully removed from your system!"
        else
            fnBase_ExitStatement "${c_blue}Note${c_normal}: no ${software_fullname} is found in your system!"
        fi

    fi
}


#########  2-2. Latest & Local Version Check  #########
fn_VersionComparasion(){
    fnBase_CentralOutputTitle "${software_fullname}"
    fnBase_OperationPhaseStatement 'Version Detection'

    # - Local version
    current_local_version=${current_local_version:-}
    if [[ -s "${installation_dir}/bin/redis-server" ]]; then
        current_local_version=$("${installation_dir}/bin/redis-server" --version 2>&1 | sed -r -n 's@.*v=([^[:space:]]+).*@\1@g;p')
    elif [[ -s "${installation_dir}/bin/redis-cli" ]]; then
        current_local_version=$("${installation_dir}/bin/redis-cli" --version 2>&1 | sed -r -n 's@^[^[:space:]]+[[:space:]]*([^[:space:]]*)@\1@g;p')
    elif fnBase_CommandExistIfCheck 'redis-server'; then
        current_local_version=$(redis-server --version 2>&1 | sed -r -n 's@.*v=([^[:space:]]+).*@\1@g;p')
    elif fnBase_CommandExistIfCheck 'redis-cli'; then
        current_local_version=$(redis-cli --version 2>&1 | sed -r -n 's@^[^[:space:]]+[[:space:]]*([^[:space:]]*)@\1@g;p')
    else
        is_existed=0
    fi

    if [[ "${is_existed}" -eq 1 ]]; then
        fnBase_OperationProcedureStatement 'Local version detecting'
        fnBase_OperationProcedureResult "${current_local_version}"
    fi

    # - Latest online version info
    fnBase_OperationProcedureStatement 'Online version detecting'
    local download_page_info
    download_page_info=$($download_method "${download_page}" | sed -r -n '/Stable/,/Unstable/{/href=/{s@'\''@"@g;s@.*href="([^"]+)".*$@\1@g;p}}' | sed ':a;N;$!ba;s@\n@|@g')
    # https://raw.githubusercontent.com/antirez/redis/4.0/00-RELEASENOTES|http://download.redis.io/releases/redis-4.0.9.tar.gz
    [[ -n "${download_page_info}" ]] || fnBase_ExitStatement "${c_red}Sorry${c_normal}: fail to extract online release version info!"

    # online_release_downloadlink="${download_page_info##*|}"
    local l_online_release_info
    l_online_release_info=$($download_method "${download_page_info%%|*}" | sed -r -n '1,/Released/{/Released/{s@^[^[:digit:]]*([^[:space:]]+).*Released[[:space:]]*(.*)$@\1|\2@g;p}}')
    # 4.0.9|Mon Mar 26 17:52:32 CEST 2018
    online_release_version="${l_online_release_info%%|*}"
    # +'%F %T %Z'
    online_release_date=$(echo "${l_online_release_info##*|}" | date -f - +"%b %d, %Y" -u)

    if [[ -n "${online_release_version}" ]]; then
        fnBase_OperationProcedureResult "${online_release_version} - ${online_release_date}"
    else
        fnBase_OperationProcedureResult '' 1
    fi

    # - Just online version check
    if [[ "${version_check}" -eq 1 ]]; then
        fnBase_ExitStatement ''

        # if [[ "${is_existed}" -eq 1 ]]; then
        #     fnBase_ExitStatement "Local existed version is ${c_red}${current_local_version}${c_normal}, Latest version online is ${c_red}${online_release_version}${c_normal} (${c_blue}${online_release_date}${c_normal})."
        # else
        #     fnBase_ExitStatement "Latest version online (${c_red}${online_release_version}${c_normal}), Release date ($c_red${online_release_date}$c_normal)."
        # fi
    fi

    # - Online & Local Version Comparasion
    if [[ "${is_existed}" -eq 1 ]]; then
        if [[ "${online_release_version}" == "${current_local_version}" ]]; then
            fnBase_ExitStatement "\nLatest version (${c_red}${online_release_version}${c_normal}) has been existed in your system."
        elif [[ $(fnBase_VersionNumberComparasion "${current_local_version}" "${online_release_version}" '<') -eq 1 ]]; then
            printf "\nExisted version local (${c_red}%s${c_normal}) < Latest version online (${c_red}%s${c_normal}).\n" "${current_local_version}" "${online_release_version}"
        fi
    # else
    #     printf "No %s find in your system!\n" "${software_fullname}"
    fi
}


#########  2-3. Prerequisites  #########
fn_ParaSpecifiedValVerification(){
    fnBase_OperationPhaseStatement 'Parameters Specified Val Verification'

    # 1- verify if the port specified is available
    fnBase_OperationProcedureStatement 'Port No. verification'
    redis_port=$(echo "${redis_port}" | sed -r 's@[[:alpha:]]*@@g;s@[[:punct:]]*@@g;s@[[:blank:]]*@@g')
    if [[ -n "${redis_port}" ]]; then
        if [[ "${redis_port}" -eq "${redis_port_default}" ]]; then
            fnBase_OperationProcedureResult "${redis_port}"
        elif [[ "${redis_port}" =~ ^[1-9]{1}[0-9]{1,4}$ ]]; then
            local sys_port_start=${sys_port_start:-1024}
            local sys_port_end=${sys_port_end:-65535}

            if [[ "${redis_port}" -ge "${sys_port_start}" && "${redis_port}" -le "${sys_port_end}" ]]; then
                local port_service_info
                port_service_info=$(awk 'match($2,/^'"${redis_port}"'\/tcp$/){print $1,$2}' /etc/services 2> /dev/null)
                if [[ -n "${port_service_info}" ]]; then
                    fnBase_OperationProcedureResult "${port_service_info} in /etc/services" 1
                else
                    fnBase_OperationProcedureResult "${redis_port}"
                fi
            else
                fnBase_OperationProcedureResult "${redis_port}⊄(${sys_port_start},${sys_port_end}) out of range" 1
            fi

        else
            fnBase_OperationProcedureResult "${redis_port} illegal" 1
        fi
    fi

    # 2- verify data dir
    fnBase_OperationProcedureStatement 'Data dir verification'
    if [[ -n "${data_dir}" ]]; then
        data_dir="${data_dir%/}"
        if [[ "${data_dir}" == "${data_dir_default}" || "${data_dir}" =~ ^/ ]]; then
            fnBase_OperationProcedureResult "${data_dir}"
        else
            fnBase_OperationProcedureResult "${data_dir}" 1
        fi
    fi

    # 3 - auth password
    if [[ "${auth_pass_enable}" -eq 1 ]]; then
        fnBase_OperationProcedureStatement 'Auth password verification'
        auth_pass=$(fnBase_RandomPasswordGeneration)
        fnBase_OperationProcedureResult "${auth_pass}"
    fi
}

fn_NormalUserOperation(){
    if [[ -z $(sed -r -n '/^'"${application_name}"':/{p}' /etc/passwd)  ]]; then
        if [[ "${strict_mode_enable}" -eq 1 ]]; then
            fnBase_OperationProcedureStatement 'Create user & group'
            # create group
            groupadd -r "${application_name}" 2> /dev/null
            # create user without login privilege
            # -M, --no-create-home   do not create the user's home directory
            # -d, --home-dir HOME_DIR    home directory of the new account
            useradd -r -g "${application_name}" -s /bin/false -d "${installation_dir}" -c "${software_fullname}" "${application_name}" 2> /dev/null
            user_name="${application_name}"
            group_name="${application_name}"
            fnBase_OperationProcedureResult "${user_name}:${group_name}"
        fi
    else
        user_name="${application_name}"
        group_name="${application_name}"
    fi
}

fn_Prerequisites(){
    if [[ "${is_existed}" -ne 1 ]]; then
        fn_ParaSpecifiedValVerification
        fn_NormalUserOperation
    fi
}


#########  2-3. Download & Decompress Latest Software  #########
fn_DirPermissionConfiguration(){
    local l_dir="${1:-}"
    local l_mode=${2:-755}
    local l_user=${3:-"${user_name}"}
    local l_group=${4:-"${group_name}"}

    [[ -n "${l_dir}" && ! -d "${l_dir}" ]] && mkdir -p "${l_dir}"
    if [[ -d "${l_dir}" ]]; then
        chmod "${l_mode}" "${l_dir}"
        chown -R "${l_user}":"${l_group}" "${l_dir}"
    fi
}

fn_DownloadAndDecompressOperation(){
    local l_operation_type=${l_operation_type:-'installing'}
    [[ "${is_existed}" -eq 1 ]] && l_operation_type='updating'
    fnBase_OperationPhaseStatement "${l_operation_type^}"

    # How to verify files for integrity
    # https://redis.io/download#how-to-verify-files-for-integrity
    # https://github.com/antirez/redis-hashes/blob/master/README

    local online_release_pack_info=${online_release_pack_info:-}
    online_release_pack_info=$($download_method 'https://raw.githubusercontent.com/antirez/redis-hashes/master/README' | sed -r -n '/-'"${online_release_version}"'.tar.gz/{s@^.*sha256[[:space:]]*@@g;s@[[:space:]]+@|@g;p}')
    # df4f73bc318e2f9ffb2d169a922dec57ec7c73dd07bccf875695dbeecd5ec510|http://download.redis.io/releases/redis-4.0.9.tar.gz

    local online_release_downloadlink="${online_release_pack_info##*|}"
    local online_release_dgst="${online_release_pack_info%%|*}"

    local online_release_pack_name
    online_release_pack_name="${online_release_downloadlink##*/}"

    local l_is_need_download=${l_is_need_download:-1}
    local l_specified_pack_dir=${l_specified_pack_dir:-}

    # specified file path existed
    if [[ "${source_pack_path}" =~ .tar.gz$ && -s "${source_pack_path}" ]]; then
        [[ "${source_pack_path##*/}" == "${online_release_pack_name}" ]] || fnBase_ExitStatement "Package ${c_red}${source_pack_path}${c_normal} is not same to latest release version name!"

    # specified dir or ~/Downloads existed
    elif [[ (-n "${source_pack_path}" && -d "${source_pack_path}" ) || -d "${user_download_dir}" ]]; then
        if [[ -d "${source_pack_path}" ]]; then
            l_specified_pack_dir="${source_pack_path}"
        elif [[ -d "${user_download_dir}" ]]; then
            l_specified_pack_dir="${user_download_dir}"
        fi

        l_specified_pack_dir="${l_specified_pack_dir%/}"

        if [[ -d "${l_specified_pack_dir}" && -f "${l_specified_pack_dir}/${online_release_pack_name}" ]]; then
            source_pack_path="${l_specified_pack_dir}/${online_release_pack_name}"
        fi
    fi

    # 1.1- existed package path in system
    if [[ -n "${source_pack_path}" && -f "${source_pack_path}" ]]; then
        fnBase_OperationProcedureStatement 'Existed pack dgst verifying'

        if [[ -n "${online_release_dgst}" && $(fnBase_HashDigestVerification "${source_pack_path}" '256') == "${online_release_dgst}" ]]; then
            fnBase_OperationProcedureResult "${c_red}${source_pack_path%/*}/${c_normal}"
            l_is_need_download=0
        else
            fnBase_OperationProcedureResult '' 1
        fi
    fi

    # 2 - Download Directly From Official Site
    local pack_save_path=${pack_save_path:-}

    if [[ "${l_is_need_download}" -eq 1 ]]; then
        fnBase_OperationProcedureStatement "Downloading ${c_red}${online_release_pack_name}${c_normal}"
        pack_save_path="${temp_save_dir}/${online_release_pack_name}"
        [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"

        $download_method "${online_release_downloadlink}" > "${pack_save_path}"

        if [[ -s "${pack_save_path}" ]]; then
            fnBase_OperationProcedureResult
        else
            [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
            fnBase_OperationProcedureResult '' 1
        fi

        fnBase_OperationProcedureStatement "SHA256 dgst verification"
        if [[ -n "${online_release_dgst}" && $(fnBase_HashDigestVerification "${pack_save_path}" '256') == "${online_release_dgst}"  ]]; then
            fnBase_OperationProcedureResult
        else
            [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
            fnBase_OperationProcedureResult '' 1
        fi

        # use variable "source_pack_path" to execute decompress operation, variable "pack_save_path" used for delete temporary download package
        source_pack_path="${pack_save_path}"
    fi

    # 3 - Decompress & Compiling
    fnBase_OperationProcedureStatement 'Decompressing'
    local application_backup_path="${installation_dir}${bak_suffix}"
    [[ -d "${application_backup_path}" ]] && rm -rf "${application_backup_path}"

    [[ -d "${installation_dir}" ]] && mv "${installation_dir}" "${application_backup_path}"
    [[ -d "${installation_dir}" ]] || mkdir -p "${installation_dir}"

    local temp_extract_dir="${temp_save_dir}/${application_name}-$RANDOM"
    [[ -d "${temp_extract_dir}" ]] && rm -rf "${temp_extract_dir}"
    [[ -d "${temp_extract_dir}" ]] || mkdir -p "${temp_extract_dir}"

    tar xf "${source_pack_path}" -C "${temp_extract_dir}" --strip-components=1 2> /dev/null

    if [[ -f "${temp_extract_dir}/redis.conf" ]]; then
        fnBase_OperationProcedureResult "${c_red}${temp_extract_dir%/}/${c_normal}"

        # copy newly downloaded pack to ~/Downloads
        local l_user_download_save_path=${l_user_download_save_path:-"${user_download_dir}/${source_pack_path##*/}"}
        if [[ -d "${user_download_dir}" && ! -f "${l_user_download_save_path}" ]]; then
            cp "${source_pack_path}" "${l_user_download_save_path}"
            chown "${login_user}" "${l_user_download_save_path}"

            # removed old version package existed
            if [[ "${is_existed}" -eq 1 ]]; then
                local l_existed_old_pack_path=${l_existed_old_pack_path:-"${user_download_dir}/${online_release_pack_name//${online_release_version}/${current_local_version}}"}
                [[ -f "${l_existed_old_pack_path}" ]] && rm -f "${l_existed_old_pack_path}"
            fi
        fi
    else
        [[ -d "${temp_extract_dir}" ]] && rm -rf "${temp_extract_dir}"
        fnBase_OperationProcedureResult '' 1
    fi

    # 4 - Essential Packages Installation
    fnBase_OperationProcedureStatement 'Packet Manager Detection'
    # local l_json_osinfo=${l_json_osinfo:-}
    # l_json_osinfo=$(fnBase_OSInfoRetrieve "${os_check_script}")
    # pack_manager=${pack_manager:-}
    # pack_manager=$(fnBase_OSInfoDirectiveValExtraction 'distro_pack_manager' "${l_json_osinfo}")

    pack_manager=${pack_manager:-}
    pack_manager=$(fnBase_OSInfoDirectiveValExtraction 'distro_pack_manager' "$(fnBase_OSInfoRetrieve ${os_check_script})")

    # pack_manager=$($download_method "${os_check_script}" | bash -s -- -i -j 2> /dev/null | sed -r -n 's@.*"system":\{([^\{]*)\}.*$@\1@g;s@","@\n@g;s@":"@|@g;s@(^"|"$)@@g;p' | sed -r -n '/^distro_pack_manager\|/{s@^[^\|]+\|(.*)@\1@g;p}')
    if [[ -n "${pack_manager}" ]]; then
        fnBase_OperationProcedureResult "${pack_manager}"
    else
        fnBase_OperationProcedureResult '' 1
    fi

    fnBase_OperationProcedureStatement 'Essential Packs Intalling'
    local l_essential_packs
    l_essential_packs='gcc make'
    [[ "${pack_manager}" == 'apt-get' ]] && l_essential_packs='build-essential tcl'
    fnBase_PackageManagerOperation 'install' "${l_essential_packs}"
    fnBase_OperationProcedureResult "${l_essential_packs}"

    # 5 - Compiling
    fnBase_OperationProcedureStatement 'Compiling'

    cd "${temp_extract_dir}" || exit
    make distclean &> /dev/null
    make &> /dev/null
    # make test &> /dev/null
    make PREFIX="${installation_dir}" install &> /dev/null

    cd "${installation_dir}" || exit

    if [[ -d "${installation_dir}/bin" ]]; then
        local new_installed_version=${new_installed_version:-}
        new_installed_version=$("${installation_dir}/bin/redis-server" --version 2>&1 | sed -r -n 's@.*v=([^[:space:]]+).*@\1@g;p')

        if [[ "${online_release_version}" == "${new_installed_version}" ]]; then
            [[ -d "${application_backup_path}" ]] && rm -rf "${application_backup_path}"
            fnBase_OperationProcedureResult "${c_red}${installation_dir%/}/${c_normal}"
        else
            [[ -d "${temp_extract_dir}" ]] && rm -rf "${temp_extract_dir}"
            [[ -d "${installation_dir}" ]] && rm -rf "${installation_dir}"
            [[ "${is_existed}" -eq 1 ]] && mv "${application_backup_path}" "${installation_dir}"
            fnBase_OperationProcedureResult '' 1
        fi
    else
        [[ -d "${temp_extract_dir}" ]] && rm -rf "${temp_extract_dir}"
        [[ -d "${installation_dir}" ]] && rm -rf "${installation_dir}"
        [[ "${is_existed}" -eq 1 ]] && mv "${application_backup_path}" "${installation_dir}"
        fnBase_OperationProcedureResult '' 1
    fi

    # 6 - configuration file
    fn_DirPermissionConfiguration "${config_dir}"
    # redis.conf, sentinel.conf
    cp -f "${temp_extract_dir}"/*.conf "${config_dir}"/

    [[ -d "${temp_extract_dir}" ]] && rm -rf "${temp_extract_dir}"
    chown -R "${user_name}":"${group_name}" "${installation_dir}"
    [[ -n "${pack_save_path}" && -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"

    pidfile_path=${pidfile_path:-"/var/run/redis-server-${redis_port}.pid"}
}


#########  2-4. Export include/lib/bin Path  #########
fn_RedisDirectiveSetting(){
    local l_item="${1:-}"
    local l_val="${2:-}"
    local l_action="${3:-}"
    local l_path=${l_path:-"${config_dir}/redis.conf"}

    if [[ -n "${l_item}" && -n "${l_val}" && -s "${l_path}" ]]; then
        [[ -n "${l_action}" ]] && fnBase_OperationProcedureStatement "${l_item}"
        if [[ -n $(sed -r -n '/^'"${l_item}"'[[:space:]]+/{p}' "${l_path}") ]]; then
            if [[ "${l_val}" =~ @ ]]; then
                sed -r -i '/^'"${l_item}"'[[:space:]]+/{s/^#?[[:space:]]*([^[:space:]]+).*/\1 '"${l_val}"'/g}' "${l_path}" 2> /dev/null
            else
                sed -r -i '/^'"${l_item}"'[[:space:]]+/{s@^#?[[:space:]]*([^[:space:]]+).*@\1 '"${l_val}"'@g}' "${l_path}" 2> /dev/null
            fi

        elif [[ -n $(sed -r -n '/^#?[[:space:]]*'"${l_item}"'[[:space:]]+/{p}' "${l_path}") ]]; then
            if [[ "${l_val}" =~ @ ]]; then
                sed -r -i '/^#?[[:space:]]*'"${l_item}"'[[:space:]]+/{s/^#?[[:space:]]*([^[:space:]]+).*/\1 '"${l_val}"'/g}' "${l_path}" 2> /dev/null
            else
                sed -r -i '/^#?[[:space:]]*'"${l_item}"'[[:space:]]+/{s@^#?[[:space:]]*([^[:space:]]+).*@\1 '"${l_val}"'@g}' "${l_path}" 2> /dev/null
            fi
        else
            sed -r -i '$a '"${l_item} ${l_val}"'' "${l_path}" 2> /dev/null
        fi
        [[ -n "${l_action}" ]] && fnBase_OperationProcedureResult "${l_val}"
    fi
}

fn_RedisConfConfiguration(){
    fnBase_OperationPhaseStatement 'redis.conf configuration'
    local l_config_path=${l_config_path:-"${config_dir}/redis.conf"}
    if [[ -f "${l_config_path}" ]]; then
        chmod 640 "${l_config_path}"
        chown "${user_name}":"${group_name}" "${l_config_path}"
    fi

    # - configuration configuration
    [[ "${auth_pass_enable}" -eq 1 && -n "${auth_pass}" ]] && fn_RedisDirectiveSetting 'requirepass' "${auth_pass}" '1'
    fn_RedisDirectiveSetting 'bind' '127.0.0.1' '1'
    fn_RedisDirectiveSetting 'port' "${redis_port}" '1'
    fn_RedisDirectiveSetting 'dir' "${data_dir}" '1'
    fn_RedisDirectiveSetting 'pidfile' "${pidfile_path}" '1'
    fn_RedisDirectiveSetting 'protected-mode' 'yes' '1'
    fn_RedisDirectiveSetting 'loglevel' 'warning'
    fn_RedisDirectiveSetting 'logfile' "\"${log_dir}/${application_name}.log\""

    fn_RedisDirectiveSetting 'maxclients' '10000'
    # fn_RedisDirectiveSetting 'maxmemory' '2gb'

    fn_RedisDirectiveSetting 'maxmemory-policy' 'allkeys-lru'
    # maxmemory-policy noeviction
    # volatile-lru -> Evict using approximated LRU among the keys with an expire set.
    # allkeys-lru -> Evict any key using approximated LRU.
    # volatile-lfu -> Evict using approximated LFU among the keys with an expire set.
    # allkeys-lfu -> Evict any key using approximated LFU.
    # volatile-random -> Remove a random key among the ones with an expire set.
    # allkeys-random -> Remove a random key, any key.
    # volatile-ttl -> Remove the key with the nearest expire time (minor TTL)
    # noeviction -> Don't evict anything, just return an error on write operations.

    fn_RedisDirectiveSetting 'tcp-backlog' '512'
    fn_RedisDirectiveSetting 'timeout' '20'
    fn_RedisDirectiveSetting 'tcp-keepalive' '90'
    # By default Redis does not run as a daemon. Use 'yes' if you need it.
    # Note that Redis will write a pid file in /var/run/redis.pid when daemonized.
    fn_RedisDirectiveSetting 'daemonize' 'yes'

    local l_supervised
    l_supervised='systemd'
    fnBase_CommandExistIfCheck 'systemctl' || l_supervised='upstart'
    fn_RedisDirectiveSetting 'supervised' "${l_supervised}" '1'

    # Set the number of databases. The default database is DB 0, you can select a different one on a per-connection basis using SELECT <dbid> where dbid is a number between 0 and 'databases'-1
    fn_RedisDirectiveSetting 'databases' '32'
    fn_RedisDirectiveSetting 'always-show-logo' 'no'
    # if sedup monitoring, you may set it to 'no'
    fn_RedisDirectiveSetting 'stop-writes-on-bgsave-error' 'yes'
    fn_RedisDirectiveSetting 'rdbcompression' 'yes'
    fn_RedisDirectiveSetting 'rdbchecksum' 'yes'
    fn_RedisDirectiveSetting 'dbfilename' 'dump.rdb'
    fn_RedisDirectiveSetting 'slave-serve-stale-data' 'no'
    fn_RedisDirectiveSetting 'slave-read-only' 'yes'
    fn_RedisDirectiveSetting 'repl-diskless-sync' 'no'
    fn_RedisDirectiveSetting 'repl-diskless-sync-delay' '5'
    fn_RedisDirectiveSetting 'repl-disable-tcp-nodelay' 'no'
    fn_RedisDirectiveSetting 'slave-priority' '100'
    fn_RedisDirectiveSetting 'appendonly' 'yes'
    fn_RedisDirectiveSetting 'appendfilename' "\"appendonly.aof\""

    # no: don't fsync, just let the OS flush the data when it wants. Faster.
    # always: fsync after every write to the append only log. Slow, Safest.
    # everysec: fsync only one time every second. Compromise.
    fn_RedisDirectiveSetting 'appendfsync' 'everysec'

    fn_RedisDirectiveSetting 'repl-disable-tcp-nodelay' 'no'

    # - REDIS CLUSTER
    # https://www.digitalocean.com/community/tutorials/how-to-configure-a-redis-cluster-on-ubuntu-14-04

    # - SLOW LOG
}

fn_ServiceScriptConfiguration(){
    fnBase_OperationProcedureStatement 'Service Script'
    # - init / service script
    local l_service_script_dir=''
    local l_service_script_path=''

    if fnBase_CommandExistIfCheck 'systemctl'; then
        l_service_script_dir='/etc/systemd/system'
        l_service_script_path="${l_service_script_dir}/${daemon_name}.service"

        [[ -f "${l_service_script_path}" ]] && fnBase_SystemServiceManagement "${l_service_script_path##*/}" 'stop'

        echo -e "[Unit]\nDescription=Advanced key-value store\nAfter=network.target\n\n[Service]\nType=notify\nExecStart=installation_dir/bin/redis-server config_dir/redis.conf\nExecStop=installation_dir/bin/redis-cli shutdown\nPIDFile=pidfile\nTimeoutStopSec=0\nRestart=always\nUser=user_name\nGroup=group_name\n\n[Install]\nWantedBy=multi-user.target\nAlias=redis.service" > "${l_service_script_path}"

        sed -i -r 's@config_dir@'"${config_dir}"'@g' "${l_service_script_path}"
        sed -i -r 's@installation_dir@'"${installation_dir}"'@g' "${l_service_script_path}"
        sed -i -r 's@pidfile@'"${pidfile_path}"'@g' "${l_service_script_path}"
        sed -r -i '/^\[Service\]/,/^\[/{s@^(User=).*$@\1'"${user_name}"'@g;}' "${l_service_script_path}"
        sed -r -i '/^\[Service\]/,/^\[/{s@^(Group=).*$@\1'"${user_name}"'@g;}' "${l_service_script_path}"

        chmod 644 "${l_service_script_path}"
    else
        l_service_script_dir='/etc/init.d'
        l_service_script_path="${l_service_script_dir}/${daemon_name}"
        local l_service_script_link
        l_service_script_link="${online_service_script_sample_dir}/redis.init"

        [[ -f "${l_service_script_path}" ]] && fnBase_SystemServiceManagement "${l_service_script_path##*/}" 'stop'

        $download_method "${l_service_script_link}" > "${l_service_script_path}"
        sed -r -i '/Configuraton Start/,/Configuraton End/{s@^(USER=).*$@\1'"${user_name}"'@g;s@^(GROUP=).*$@\1'"${group_name}"'@g;s@^(PORT=).*$@\1'"${redis_port}"'@g;}' "${l_service_script_path}"

        chmod 755 "${l_service_script_path}"
    fi

    fnBase_SystemServiceManagement "${l_service_script_path##*/}" 'start'
    fnBase_OperationProcedureResult "${l_service_script_path}"
}

fn_PostInstallationConfiguration(){
    [[ "${is_existed}" -eq 0 ]] && fn_RedisConfConfiguration

    fnBase_OperationPhaseStatement 'Configuration'

    fnBase_OperationProcedureStatement '/etc/sysctl.conf'
    # https://redis.io/topics/admin
    # Make sure to set the Linux kernel overcommit memory setting to 1. Add vm.overcommit_memory = 1 to /etc/sysctl.conf and then reboot or run the command sysctl vm.overcommit_memory=1 for this to take effect immediately.
    local l_sysctl_path=${l_sysctl_path:-'/etc/sysctl.conf'}
    if [[ -f "${l_sysctl_path}" ]]; then
        if [[ -n $(sed -r -n '/^vm.overcommit_memory[[:space:]]*=/{p}' "${l_sysctl_path}") ]]; then
            sed -r -i '/^vm.overcommit_memory[[:space:]]*=/{s@^(vm.overcommit_memory).*@\1 = 1@g}' "${l_sysctl_path}" 2> /dev/null
        else
            sed -r -i '$a vm.overcommit_memory = 1' 2> /dev/null
        fi
    fi
    fnBase_CommandExistIfCheck 'sysctl' && sysctl vm.overcommit_memory=1 &> /dev/null
    fnBase_OperationProcedureResult 'vm.overcommit_memory = 1'

    fnBase_OperationProcedureStatement 'Executing path'
    echo "export PATH=\$PATH:${installation_dir}/bin" > "${profile_d_path}"
    # shellcheck source=/dev/null
    source "${profile_d_path}" 2> /dev/null
    fnBase_OperationProcedureResult "${profile_d_path}"

    if [[ "${is_existed}" -eq 0 ]]; then
        fnBase_OperationProcedureStatement 'Log directory'
        fn_DirPermissionConfiguration "${log_dir}"
        fnBase_OperationProcedureResult "${log_dir}"

        fnBase_OperationProcedureStatement 'Data directory'
        fn_DirPermissionConfiguration "${data_dir}"
        fnBase_OperationProcedureResult "${data_dir}"

        # fnBase_OperationProcedureStatement 'Config directory'
        # fn_DirPermissionConfiguration "${config_dir}"
        # fnBase_OperationProcedureResult "${config_dir}"

        # - command alias
        if [[ -s "${login_user_home}/.bashrc" ]]; then
            fnBase_OperationProcedureStatement 'Command alias'
            local l_command_alias_name
            l_command_alias_name='redis_cli_command'

            sed -r -i '/'"${bashrc_string}"' start/,/'"${bashrc_string}"' end/d' "${login_user_home}/.bashrc"
            if [[ -n "${auth_pass}" ]]; then
                echo -e "#${bashrc_string} start\n#using 'auth ${auth_pass}' without -a\nalias ${l_command_alias_name}=\"${installation_dir}/bin/redis-cli -h 127.0.0.1 -p ${redis_port} -a ${auth_pass}\"\n#${bashrc_string} end" >> "${login_user_home}/.bashrc"
            else
                echo -e "#${bashrc_string} start\nalias ${l_command_alias_name}=\"${installation_dir}/bin/redis-cli -h 127.0.0.1 -p ${redis_port}\"\n#${bashrc_string} end" >> "${login_user_home}/.bashrc"
            fi

            fnBase_OperationProcedureResult "~/.bashrc alias ${l_command_alias_name}"
        fi

        # - firewall
        if [[ "${enable_firewall}" -eq 1 ]]; then
            fnBase_OperationProcedureStatement 'Firewall configuration'
            # $download_method "${firewall_configuration_script}" | bash -s -- -p "${redis_port}" -H "${remote_host_ip}" -s
            $download_method "${firewall_configuration_script}" | bash -s -- -p "${redis_port}" -s
            fnBase_OperationProcedureResult "${redis_port}"
        fi

        # - init / service script
        fn_ServiceScriptConfiguration
    fi
}

#########  2-5. Operation Time Cost  #########
fn_TotalTimeCosting(){
    finish_time=$(date +'%s')        # End Time Of Operation
    total_time_cost=$((finish_time-start_time))   # Total Time Of Operation
    printf "\nTotal time cost is ${c_red}%s${c_normal} seconds!\n" "${total_time_cost}"

    if [[ -n "${auth_pass}" ]]; then
        echo -e "\nFor local connection, using ${c_yellow}redis-cli -h 127.0.0.1 -p ${redis_port} -a ${auth_pass}${c_normal} or using ${c_yellow}auth ${auth_pass}${c_normal} without -a."
    else
        echo -e "\nFor local connection, using ${c_yellow}redis-cli -h 127.0.0.1 -p ${redis_port}${c_normal}."
    fi

    printf "\n${c_bold}${c_blue}%s${c_normal}: You need to relogin to make bash profile effect!\n" 'Notice'
}


#########  3. Executing Process  #########
fn_InitializationCheck

fn_UninstallOperation
fn_VersionComparasion
fn_Prerequisites
fn_DownloadAndDecompressOperation
fn_PostInstallationConfiguration
fn_TotalTimeCosting


#########  4. EXIT Singal Processing  #########
# trap "commands" EXIT # execute command when exit from shell
fn_TrapEXIT(){
    fnBase_UnsetVars
    unset daemon_name
    unset installation_dir
    unset redis_port
    unset config_dir
    unset log_dir
    unset data_dir
    unset profile_d_path
    unset user_name
    unset group_name
    unset is_existed
    unset version_check
    unset auth_pass_enable
    unset auth_pass
    unset strict_mode_enable
    unset enable_firewall
    unset source_pack_path
    unset is_uninstall
    unset proxy_server_specify
    unset update_operation
}

trap fn_TrapEXIT EXIT


# WARNING you have Transparent Huge Pages (THP) support enabled in your kernel. This will create latency and memory usage issues with Redis. To fix this issue run the command 'echo never > /sys/kernel/mm/transparent_hugepage/enabled' as root, and add it to your /etc/rc.local in order to retain the setting after a reboot. Redis must be restarted after THP is disabled.

# (error) NOAUTH Authentication required.

# (error) DENIED Redis is running in protected mode because protected mode is enabled, no bind address was specified, no authentication password is requested to clients. In this mode connections are only accepted from the loopback interface. If you want to connect from external computers to Redis you may adopt one of the following solutions: 1) Just disable protected mode sending the command 'CONFIG SET protected-mode no' from the loopback interface by connecting to Redis from the same host the server is running, however MAKE SURE Redis is not publicly accessible from internet if you do so. Use CONFIG REWRITE to make this change permanent. 2) Alternatively you can just disable the protected mode by editing the Redis configuration file, and setting the protected mode option to 'no', and then restarting the server. 3) If you started the server manually just for testing, restart it with the '--protected-mode no' option. 4) Setup a bind address or an authentication password. NOTE: You only need to do one of the above things in order for the server to start accepting connections from the outside.

# Script End
