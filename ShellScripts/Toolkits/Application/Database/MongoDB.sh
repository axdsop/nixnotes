#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails


# Official site: https://www.mongodb.com
# Documentation
# - https://docs.mongodb.com/manual/
# - https://docs.mongodb.com/manual/tutorial/getting-started/
# - http://docs.mongodb.org/manual/reference/ulimit/#recommended-settings
# - https://severalnines.com/blog/become-mongodb-dba-basics-mongodb-configuration

# Harden Doc
# - https://docs.mongodb.com/manual/administration/security-checklist/
# - https://severalnines.com/blog/optimizing-your-linux-environment-mongodb

# MongoDB DBA
# - https://severalnines.com/blog?series=689
# - https://severalnines.com/blog?categories=309


# Target: Installing & Configuring MongoDB Community Server On GNU/Linux (RHEL/CentOS/Debian/Ubuntu/SLES/Amazon Linux)
# Developer: MaxdSre

# Change Log:
# - Oct 08, 2020 08:49 Thu ET - initialization check method update
# - Oct 16, 2019 10:14 Wed ET - fix variable in service file, remove commitIntervalMs in mongod.conf
# - Oct 15, 2019 12:13 Tue ET - change custom script url
# - Oct 14, 2019 12:28 Fri ET - change to new base functions
# - Dec 31, 2018 16:18 Mon ET - code reconfiguration, include base funcions from other file
# - Aug 04, 2018 08:45 Sat +0800 - not update config files while update
# - Jun 28, 2018 13:00 ET - add shared libary check 'libcurl.so.4'
# - Jun 10, 2018 10:20 Sun ET - add RuntimeDirectory, RuntimeDirectoryMode in systemd service for create runtimedir in /var/run
# - May 31, 2018 15:02 Thu ET - add passphrase for self-sign certificate, set directive --sslPEMKeyPassword
# - May 28, 2018 10:39 Mon ET - code reconfiguration
# - Dec 22, 2017 19:40 Fri +0800
# - Dec 21, 2017 19:40 Thu +0800


#########  0-1. Singal Setting  #########
trap fnBase_TrapCleanUp INT QUIT

#########  0-2. Variables Setting  #########
# umask 022    # temporarily change umask value to 022
readonly official_site='https://www.mongodb.com'
readonly download_page='https://www.mongodb.org/dl/linux/'
readonly custom_shellscript_url='https://gitlab.com/axdsop/nixnotes/raw/master'
readonly os_check_script="${custom_shellscript_url}/ShellScripts/Toolkits/GNULinux/gnuLinuxMachineInfoDetection.sh"
readonly firewall_configuration_script="${custom_shellscript_url}/ShellScripts/Toolkits/GNULinux/gnuLinuxFirewallRuleConfiguration.sh"
readonly online_mongod_cnf_sample_url="${custom_shellscript_url}/ShellScripts/Toolkits/_Configs/Application/mongodb/config/mongod.conf"
readonly online_service_script_sample_dir="${custom_shellscript_url}/ShellScripts/Toolkits/_Configs/Application/mongodb/initScript"

software_fullname=${software_fullname:-'MongoDB community server'}
installation_dir="/opt/${software_fullname%% *}"
readonly application_name='mongodb'
readonly daemon_name='mongod'
readonly bashrc_string="${software_fullname%% *} configuration"
user_name=${user_name:-'root'}
group_name=${group_name:-'root'}

# https://docs.mongodb.com/manual/reference/default-mongodb-port/
readonly mongod_port_default='27017'
mongod_port=${mongod_port:-"${mongod_port_default}"}
readonly data_dir_default="/var/lib/${application_name}"
data_dir=${data_dir:-"${data_dir_default}"}
config_dir=${config_dir:-"/etc/${application_name}"}
config_path="${config_dir}/${daemon_name}.conf"
log_dir=${log_dir:-"/var/log/${application_name}"}
run_dir=${run_dir:-"/var/run/${application_name}"}
profile_d_path="/etc/profile.d/${application_name}.sh"
readonly security_limit_config='/etc/security/limits.conf'

is_existed=${is_existed:-1}   # Default value is 1, assumming application existed

version_check=${version_check:-0}
admin_name=${admin_name:-'admin'}
admin_password_new=${admin_password_new:-''}
source_pack_path=${source_pack_path:-}
os_detect=${os_detect:-0}
is_uninstall=${is_uninstall:-0}
security_enhancement=${security_enhancement:-0}
enable_firewall=${enable_firewall:-0}


#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
script [options] ...
script | sudo bash -s -- [options] ...

Installing / Configuring MongoDB community server On GNU/Linux (RHEL/CentOS/Debian/Ubuntu/SUSE/Amazon Linux)!
This script requires superuser privileges (eg. root, su).

[available option]
    -h    --help, show help info
    -c    --check, check installed or not
    -o    --os info, detect os distribution info
    -a admin_name    --specify administrator user for mongodb, default is 'admin'
    -d data_dir    --set data dir, default is /var/lib/mongodb
    -s port    --set mongod port number, default is 27017
    -f file_path    --manually specify absolute path of mongo package in local system, e.g. /tmp/mongodb-linux-x86_64-ubuntu1804-4.0.5.tgz, default is download directly from official download page
    -E    --security enhancement (create user mongodb; remove datadir while uninstall)
    -F    --enable firewall rule (iptable/firewalld/ufw/SuSEfirewall2), default is disable
    -u    --uninstall, uninstall software installed, default keep data dir unless along with -E
    -p [protocol:]ip:port    --proxy host (http|https|socks4|socks5), default protocol is http
\e[0m"
}

while getopts "hcof:a:d:s:EFup:" option "$@"; do
    case "$option" in
        c ) version_check=1 ;;
        o ) os_detect=1 ;;
        f ) source_pack_path="$OPTARG" ;;
        a ) admin_name="$OPTARG" ;;
        d ) data_dir="$OPTARG" ;;
        s ) mongod_port="$OPTARG" ;;
        E ) security_enhancement=1 ;;
        F ) enable_firewall=1 ;;
        u ) is_uninstall=1 ;;
        p ) proxy_server_specify="$OPTARG" ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done



#########  1-2 Preparation Operation  #########
fn_InitializationCheck(){
    local l_user_codebase_dir="$HOME/.config/axdsop"
    local l_base_function_temp_save_path=''
    local l_need_remove=0
    local l_base_function_url='https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/base_functions.sh'
    l_base_function_temp_save_path=$(find "${l_user_codebase_dir}" -type f -name "${l_base_function_url##*/}" -print 2> /dev/null)
    
    if [[ ! -f "${l_base_function_temp_save_path}" ]]; then
        local l_download_tool=${l_download_tool:-'wget -qO-'}
        [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_tool='curl -fsL'
        l_need_remove=1
        l_base_function_temp_save_path=$(mktemp -t XXXXXXX)
        ${l_download_tool} "${l_base_function_url}" > "${l_base_function_temp_save_path}"
    fi

    if [[ -f "${l_base_function_temp_save_path}" && -s "${l_base_function_temp_save_path}" ]]; then
        . "${l_base_function_temp_save_path}"
        [[ "${l_need_remove}" -eq 1 ]] && rm "${l_base_function_temp_save_path}"
    else
        echo "Sorry, fail to get base script path."
        exit
    fi

    # tput clear  # Echo the clear-screen sequence for the current terminal.
    # specify 1 means need sudo privilege
    fnBase_RunningEnvironmentCheck '1'

    if [[ "${os_detect}" -eq 1 ]]; then
        $download_method "${os_check_script}" | bash -s --
        exit
    fi

    fnBase_CommandExistCheckPhase 'gzip' # CentOS/Debian/OpenSUSE: gzip
    fnBase_CommandExistCheckPhase 'tar' # .tar.gz
    fnBase_CommandExistCheckPhase 'openssl'
}

fn_OSInfoDetection(){
    local l_json_osinfo=${l_json_osinfo:-}
    l_json_osinfo=$(fnBase_OSInfoRetrieve "${os_check_script}")
    # [[ -z "${l_json_osinfo}" ]] && fnBase_ExitStatement "${c_red}Fatal${c_normal}: fail to extract os info!"

    distro_fullname=${distro_fullname:-}
    distro_fullname=$(fnBase_OSInfoDirectiveValExtraction 'distro_fullname' "${l_json_osinfo}")

    distro_name=${distro_name:-}
    distro_name=$(fnBase_OSInfoDirectiveValExtraction 'distro_name' "${l_json_osinfo}")
    distro_name="${distro_name%%-*}"    # centos, fedora

    codename=${codename:-}
    codename=$(fnBase_OSInfoDirectiveValExtraction 'distro_codename' "${l_json_osinfo}")

    version_id=${version_id:-}
    version_id=$(fnBase_OSInfoDirectiveValExtraction 'distro_version_id' "${l_json_osinfo}")

    # ip_public=${ip_public:-}
    # ip_public=$(fnBase_OSInfoDirectiveValExtraction 'ip_public' "${l_json_osinfo}")
    ip_public_locate=${ip_public_locate:-}
    ip_public_locate=$(fnBase_OSInfoDirectiveValExtraction 'ip_public_locate' "${l_json_osinfo}")

    ip_public_country_code=${ip_public_country_code:-}
    ip_public_country_code=$(fnBase_OSInfoDirectiveValExtraction 'ip_public_country_code' "${l_json_osinfo}")
}


#########  2-1. Uninstall Operation  #########
fn_UninstallOperation(){
    if [[ "${is_uninstall}" -eq 1 ]]; then
        # fnBase_CommandExistIfCheck 'mongod';
        if [[ -s "${installation_dir}/bin/mongod" ]]; then
            fnBase_SystemServiceManagement "${daemon_name}" 'stop'

            [[ -d "${installation_dir}" ]] && rm -rf "${installation_dir}"
            [[ -d "${config_dir}" ]] && rm -rf "${config_dir}"
            [[ -d "${log_dir}" ]] && rm -rf "${log_dir}"
            [[ -d "${run_dir}" ]] && rm -rf "${run_dir}"
            [[ -s "${profile_d_path}" ]] && rm -f "${profile_d_path}"

            if [[ "${security_enhancement}" -eq 1 ]]; then
                [[ -d "${data_dir}" ]] && rm -rf "${data_dir}"
                # - remove user and group
                if [[ -n $(sed -r -n '/^'"${application_name}"':/{p}' /etc/passwd) ]]; then
                    userdel -fr "${application_name}" 2> /dev/null
                    groupdel -f "${application_name}" 2> /dev/null
                    sed -r -i '/^'"${application_name}"'[[:space:]]*/d' "${security_limit_config}" 2> /dev/null
                fi
            fi

            [[ -f "${login_user_home}/.bashrc" ]] && sed -r -i '/'"${bashrc_string}"' start/,/'"${bashrc_string}"' end/d' "${login_user_home}/.bashrc"

            local l_service_script_dir='/etc/init.d'
            fnBase_CommandExistIfCheck 'systemctl' && l_service_script_dir='/etc/systemd/system'

            # - stop daemon running
            find "${l_service_script_dir}"/ -type f -name ''${daemon_name}'*' -print | while IFS="" read -r line; do
                if [[ -n "${line}" ]]; then
                    service_name="${line##*/}"
                    service_name="${service_name%%.*}"
                    fnBase_SystemServiceManagement "${service_name}" 'stop'
                    # - remove system init script   SysV init / Systemd
                    [[ -f "${line}" ]] && rm -f "${line}"
                    unset service_name
                fi
            done

            if fnBase_CommandExistIfCheck 'mongod'; then
                fnBase_ExitStatement "${c_red}Sorry${c_normal}, fail to uninstall ${software_fullname}!"
            else
                fnBase_CommandExistIfCheck 'systemctl' && systemctl daemon-reload 2> /dev/null
                fnBase_ExitStatement "${software_fullname} is successfully removed from your system!"
            fi
        else
            fnBase_ExitStatement "${c_blue}Note${c_normal}: no ${software_fullname} is found in your system!"
        fi
    fi
}


#########  2-2. Latest & Local Version Check  #########
fn_LibarylibcurlCheck(){
    # mongod: error while loading shared libraries: libcurl.so.4: cannot open shared object file: No such file or directory
    # apt|libcurl4-openssl-dev|/usr/lib/x86_64-linux-gnu/libcurl.so.4
    # zypper|libcurl4|/usr/lib64/libcurl.so.4
    # yum|libcurl|/usr/lib64/libcurl.so.4

    # updatedb &> /dev/null; locate -c libcurl.so.4
    [[ -z $(find /usr/lib* -name 'libcurl.so.4' -print 2>/dev/null) ]] && fnBase_ExitStatement "${c_red}Fatal Error${c_normal}: MongoDB need shared libraries ${c_blue}libcurl.so.4${c_normal} (apt: libcurl4-openssl-dev, zypper: libcurl4 , yum: libcurl )!"
}

fn_VersionComparasion(){
    fnBase_CentralOutputTitle "${software_fullname}"
    fnBase_OperationPhaseStatement 'Version Detection'

    # - Local version
    current_local_version=${current_local_version:-}

    if [[ -s "${installation_dir}/bin/mongod" ]]; then
        current_local_version=$("${installation_dir}/bin/mongod" --version 2>&1 | sed -r -n '/^db version/{s@^.*v([[:digit:].]+)$@\1@g;p}')
    elif [[ -s "${installation_dir}/bin/mongo" ]]; then
        current_local_version=$("${installation_dir}/bin/mongo" --version 2>&1 | sed -r -n '/shell version/{s@^.*v([[:digit:].]+)$@\1@g;p}')
    elif fnBase_CommandExistIfCheck 'mongod'; then
        current_local_version=$(mongod --version 2>&1 | sed -r -n '/^db version/{s@^.*v([[:digit:].]+)$@\1@g;p}')
    elif fnBase_CommandExistIfCheck 'mongo'; then
        current_local_version=$(mongo --version 2>&1 | sed -r -n '/shell version/{s@^.*v([[:digit:].]+)$@\1@g;p}')
    else
        is_existed=0
        fn_LibarylibcurlCheck
    fi

    if [[ "${is_existed}" -eq 1 ]]; then
        fnBase_OperationProcedureStatement 'Local version detecting'
        fnBase_OperationProcedureResult "${current_local_version}"
    fi

    # - Latest online version info
    fnBase_OperationProcedureStatement 'Online version detecting'

    # Ubuntu 16.04.4 LTS (Xenial Xerus) just has `python3`, has no 'python'
    local l_python_name=${l_python_name:-'python'}

    if fnBase_CommandExistIfCheck 'python3'; then
        l_python_name='python3'
    elif fnBase_CommandExistIfCheck 'python'; then
        l_python_name='python'
    fi

    local download_page_info=${download_page_info:-}
    download_page_info=$($download_method "${official_site}/download-center" | sed -r -n '/server-data/,${/mongo/!d;s@^[[:space:]]*@@g;s@&quot;@"@g;s@<[^>]+>@@g;s@^[^=]*=[[:space:]]*@@g;p}' | ${l_python_name} -c $'import json,sys\nobj=json.load(sys.stdin)\nfor version in obj["community"]["versions"]:\n    if version["current"] and version["production_release"]:\n        print("release|{}|{}".format(version["version"],version["date"]));\n        for pack in version["downloads"]:\n            if "arch" in pack and pack["arch"] == "x86_64" and pack["edition"] == "targeted":\n                print("{}|{}|{}".format(pack["target"],pack["archive"]["sha256"],pack["archive"]["url"]))\n        break' 2> /dev/null)

    [[ -n "${download_page_info}" ]] || fnBase_ExitStatement "${c_red}Sorry${c_normal}: fail to extract online release version info!"

    # release|3.6.5|05/21/2018
    # ubuntu1604|96ae02a9ead67c088018999ca515caed7b8be378625bbf09646617bdc6ba067b|https://fastdl.mongodb.org/linux/mongodb-linux-x86_64-ubuntu1604-3.6.5.tgz

    local l_online_release_info=${l_online_release_info:-}
    l_online_release_info=$(echo "${download_page_info}" | sed -r -n '/^release\|/{s@^([^\|]+)\|@@g;p}')
    online_release_version="${l_online_release_info%%|*}"
    online_release_date=$(echo "${l_online_release_info##*|}" | date -f - +"%b %d, %Y" -u)

    if [[ -n "${online_release_version}" ]]; then
        fnBase_OperationProcedureResult "${online_release_version} - ${online_release_date}"
    else
        fnBase_OperationProcedureResult '' 1
    fi

    # - supported distro system detect
    fnBase_OperationProcedureStatement 'Supported distro detecting'
    local available_distro_list=${available_distro_list:-}
    available_distro_list=$(echo "${download_page_info}" | sed -r -n '/^release\|/d;s@^([^\|]+).*@\1@g;p')

    local l_distro_pattern=${l_distro_pattern:-}
    case "${distro_name}" in
        rhel|centos )
            # rhel70 / rhel62
            l_distro_pattern=$(echo "${available_distro_list}" | sed -r -n '/^rhel'"${version_id%%.*}"'/{p}')
            ;;
        ubuntu )
            # ubuntu1604/ubuntu1404
            # 16.04.4
            local l_version_str
            # l_version_str=$(echo "${version_id}" | cut -d. --output-delimiter='' -f 1,2)
            l_version_str=$(echo "${version_id}" | cut -d. -f 1,2)
            l_version_str="${l_version_str//.}"

            l_distro_pattern=$(echo "${available_distro_list}" | sed -r -n '/^ubuntu'"${l_version_str}"'/{p}')

            # Ubuntu bionic 18.04 use ubuntu xenial 16.04 package
            [[ "${l_version_str}" == '1804' && -z "${l_distro_pattern}" ]] && l_distro_pattern='ubuntu1604'
            ;;
        debian )
            # debian71 / debian81 / debian92
            l_distro_pattern=$(echo "${available_distro_list}" | sed -r -n '/^debian'"${version_id%%.*}"'/{p}')
            ;;
        suse|sles )
            # suse12
            l_distro_pattern=$(echo "${available_distro_list}" | sed -r -n '/^suse'"${version_id%%.*}"'/{p}')
            ;;
        amzn ) l_distro_pattern='amazon' ;;
    esac

    if [[ -n "${l_distro_pattern}" ]]; then
        online_release_pack_info=${online_release_pack_info:-}
        online_release_pack_info=$(echo "${download_page_info}" | sed -r -n '/^'"${l_distro_pattern}"'\|/{s@^[^\|]+\|@@g;p}')
        # 96ae02a9ead67c088018999ca515caed7b8be378625bbf09646617bdc6ba067b|https://fastdl.mongodb.org/linux/mongodb-linux-x86_64-ubuntu1604-3.6.5.tgz

        fnBase_OperationProcedureResult "${distro_fullname}"
    else
        fnBase_OperationProcedureResult "${distro_fullname} not support" '1'
        fnBase_ExitStatement "\nSorry, MongoDB community edition doesn't support your system ${c_yellow}${distro_fullname}${c_normal}."
    fi


    # - Just online version check
    if [[ "${version_check}" -eq 1 ]]; then
        fnBase_ExitStatement ''
    fi

    # - Online & Local Version Comparasion
    if [[ "${is_existed}" -eq 1 ]]; then
        if [[ "${online_release_version}" == "${current_local_version}" ]]; then
            fnBase_ExitStatement "\nLatest version (${c_red}${online_release_version}${c_normal}) has been existed in your system."
        elif [[ $(fnBase_VersionNumberComparasion "${current_local_version}" "${online_release_version}" '<') -eq 1 ]]; then
            printf "\nExisted version local (${c_red}%s${c_normal}) < Latest version online (${c_red}%s${c_normal}).\n" "${current_local_version}" "${online_release_version}"
        fi
    # else
    #     printf "No %s find in your system!\n" "${software_fullname}"
    fi
}


#########  2-3. Prerequisites  #########
fn_ParaSpecifiedValVerification(){
    fnBase_OperationPhaseStatement 'Parameters Specified Val Verification'

    # 1- verify if the port specified is available
    fnBase_OperationProcedureStatement 'Port No. verification'
    mongod_port=$(echo "${mongod_port}" | sed -r 's@[[:alpha:]]*@@g;s@[[:punct:]]*@@g;s@[[:blank:]]*@@g')
    if [[ -n "${mongod_port}" ]]; then
        if [[ "${mongod_port}" -eq "${mongod_port_default}" ]]; then
            fnBase_OperationProcedureResult "${mongod_port}"
        elif [[ "${mongod_port}" =~ ^[1-9]{1}[0-9]{1,4}$ ]]; then
            local sys_port_start=${sys_port_start:-1024}
            local sys_port_end=${sys_port_end:-65535}

            if [[ "${mongod_port}" -ge "${sys_port_start}" && "${mongod_port}" -le "${sys_port_end}" ]]; then
                local port_service_info
                port_service_info=$(awk 'match($2,/^'"${mongod_port}"'\/tcp$/){print $1,$2}' /etc/services 2> /dev/null)
                if [[ -n "${port_service_info}" ]]; then
                    fnBase_OperationProcedureResult "${port_service_info} in /etc/services" 1
                else
                    fnBase_OperationProcedureResult "${mongod_port}"
                fi
            else
                fnBase_OperationProcedureResult "${mongod_port}⊄(${sys_port_start},${sys_port_end}) out of range" 1
            fi
        else
            fnBase_OperationProcedureResult "${mongod_port} illegal" 1
        fi
    fi

    # 2- verify data dir
    fnBase_OperationProcedureStatement 'Data dir verification'
    if [[ -n "${data_dir}" ]]; then
        data_dir="${data_dir%/}"
        if [[ "${data_dir}" == "${data_dir_default}" || "${data_dir}" =~ ^/ ]]; then
            fnBase_OperationProcedureResult "${data_dir}"
        else
            fnBase_OperationProcedureResult "${data_dir}" 1
        fi
    fi
}

fn_NormalUserOperation(){
    if [[ -z $(sed -r -n '/^'"${application_name}"':/{p}' /etc/passwd)  ]]; then
        if [[ "${security_enhancement}" -eq 1 ]]; then
            fnBase_OperationProcedureStatement 'Create user & group'
            # create group
            groupadd -r "${application_name}" 2> /dev/null
            # create user without login privilege
            # -M, --no-create-home   do not create the user's home directory
            # -d, --home-dir HOME_DIR    home directory of the new account
            useradd -r -g "${application_name}" -s /bin/false -d "${installation_dir}" -c "${software_fullname}" "${application_name}" 2> /dev/null
            usermod -a -G "${application_name}" "${login_user}" 2> /dev/null
            user_name="${application_name}"
            group_name="${application_name}"
            fnBase_OperationProcedureResult "${user_name}:${group_name}"

            if [[ -f "${security_limit_config}" ]]; then
                fnBase_OperationProcedureStatement 'User Limit'
                local l_limit_val=${l_limit_val:-'64000'}
                sed -r -i '/^'"${application_name}"'[[:space:]]*/d' "${security_limit_config}"
                sed -r -i '/End of file/d' "${security_limit_config}"
                echo -e "${application_name} soft nofile ${l_limit_val}\n${application_name} hard nofile ${l_limit_val}\n${application_name} soft nproc ${l_limit_val}\n${application_name} hard nproc ${l_limit_val}\nEnd of file" >> "${security_limit_config}"
                fnBase_OperationProcedureResult "${security_limit_config} ${l_limit_val}"
            fi
        fi
    else
        user_name="${application_name}"
        group_name="${application_name}"
    fi
}

fn_Prerequisites(){
    if [[ "${is_existed}" -ne 1 ]]; then
        fn_ParaSpecifiedValVerification
        fn_NormalUserOperation
    fi
}


#########  2-4. Download & Decompress Latest Software  #########
fn_DownloadAndDecompressOperation(){
    local l_operation_type=${l_operation_type:-'installing'}
    [[ "${is_existed}" -eq 1 ]] && l_operation_type='updating'
    fnBase_OperationPhaseStatement "${l_operation_type^}"

    # 96ae02a9ead67c088018999ca515caed7b8be378625bbf09646617bdc6ba067b|https://fastdl.mongodb.org/linux/mongodb-linux-x86_64-ubuntu1604-3.6.5.tgz
    local online_release_downloadlink="${online_release_pack_info##*|}"
    local online_release_dgst="${online_release_pack_info%%|*}"
    local online_release_pack_name
    online_release_pack_name="${online_release_downloadlink##*/}"

    local l_is_need_download=${l_is_need_download:-1}
    local l_specified_pack_dir=${l_specified_pack_dir:-}

    # specified file path existed
    if [[ "${source_pack_path}" =~ .tgz$ && -s "${source_pack_path}" ]]; then
        [[ "${source_pack_path##*/}" == "${online_release_pack_name}" ]] || fnBase_ExitStatement "Package ${c_red}${source_pack_path}${c_normal} is not same to latest release version name!"

    # specified dir or ~/Downloads existed
    elif [[ (-n "${source_pack_path}" && -d "${source_pack_path}" ) || -d "${user_download_dir}" ]]; then
        if [[ -d "${source_pack_path}" ]]; then
            l_specified_pack_dir="${source_pack_path}"
        elif [[ -d "${user_download_dir}" ]]; then
            l_specified_pack_dir="${user_download_dir}"
        fi

        l_specified_pack_dir="${l_specified_pack_dir%/}"

        if [[ -d "${l_specified_pack_dir}" && -f "${l_specified_pack_dir}/${online_release_pack_name}" ]]; then
            source_pack_path="${l_specified_pack_dir}/${online_release_pack_name}"
        fi
    fi

    # 1.1- existed package path in system
    if [[ -n "${source_pack_path}" && -f "${source_pack_path}" ]]; then
        fnBase_OperationProcedureStatement 'Existed pack dgst verifying'

        if [[ -n "${online_release_dgst}" && $(fnBase_HashDigestVerification "${source_pack_path}" '256') == "${online_release_dgst}" ]]; then
            fnBase_OperationProcedureResult "${c_red}${source_pack_path%/*}/${c_normal}"
            l_is_need_download=0
        else
            fnBase_OperationProcedureResult '' 1
        fi
    fi

    # 2 - Download Directly From Official Site
    local pack_save_path=${pack_save_path:-}

    if [[ "${l_is_need_download}" -eq 1 ]]; then
        fnBase_OperationProcedureStatement "Downloading ${c_red}${online_release_pack_name}${c_normal}"
        pack_save_path="${temp_save_dir}/${online_release_pack_name}"
        [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"

        $download_method "${online_release_downloadlink}" > "${pack_save_path}"

        if [[ -s "${pack_save_path}" ]]; then
            fnBase_OperationProcedureResult
        else
            [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
            fnBase_OperationProcedureResult '' 1
        fi

        fnBase_OperationProcedureStatement "SHA256 dgst verification"
        if [[ -n "${online_release_dgst}" && $(fnBase_HashDigestVerification "${pack_save_path}" '256') == "${online_release_dgst}"  ]]; then
            fnBase_OperationProcedureResult
        else
            [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
            fnBase_OperationProcedureResult '' 1
        fi

        # use variable "source_pack_path" to execute decompress operation, variable "pack_save_path" used for delete temporary download package
        source_pack_path="${pack_save_path}"
    fi

    # 3 - Decompress & Compiling
    fnBase_OperationProcedureStatement 'Decompressing'
    local application_backup_path="${installation_dir}${bak_suffix}"
    [[ -d "${application_backup_path}" ]] && rm -rf "${application_backup_path}"

    [[ -d "${installation_dir}" ]] && mv "${installation_dir}" "${application_backup_path}"
    [[ -d "${installation_dir}" ]] || mkdir -p "${installation_dir}"

    tar xf "${source_pack_path}" -C "${installation_dir}" --strip-components=1 2> /dev/null

    find "${installation_dir}"/ -type d -exec chmod 755 {} \;
    find "${installation_dir}"/ -type f -exec chmod 640 {} \;
    find "${installation_dir}"/bin/ -type f -exec chmod 755 {} \;

    local new_installed_version=${new_installed_version:-}
    new_installed_version=$("${installation_dir}/bin/mongod" --version 2>&1 | sed -r -n '/db[[:space:]]*version/{s@^[^[:digit:]]+([[:digit:].]+).*$@\1@g;p}')

    if [[ "${online_release_version}" == "${new_installed_version}" ]]; then
        [[ -d "${application_backup_path}" ]] && rm -rf "${application_backup_path}"
        fnBase_OperationProcedureResult "${c_red}${installation_dir%/}/${c_normal}"

        # copy newly downloaded pack to ~/Downloads
        local l_user_download_save_path=${l_user_download_save_path:-"${user_download_dir}/${source_pack_path##*/}"}
        if [[ -d "${user_download_dir}" && ! -f "${l_user_download_save_path}" ]]; then
            cp "${source_pack_path}" "${l_user_download_save_path}"
            chown "${login_user}" "${l_user_download_save_path}"

            # removed old version package existed
            if [[ "${is_existed}" -eq 1 ]]; then
                local l_existed_old_pack_path=${l_existed_old_pack_path:-"${user_download_dir}/${online_release_pack_name//${online_release_version}/${current_local_version}}"}
                [[ -f "${l_existed_old_pack_path}" ]] && rm -f "${l_existed_old_pack_path}"
            fi
        fi
    else
        [[ -d "${installation_dir}" ]] && rm -rf "${installation_dir}"
        [[ "${is_existed}" -eq 1 ]] && mv "${application_backup_path}" "${installation_dir}"
        fnBase_OperationProcedureResult '' 1
    fi

    chown -R "${user_name}":"${group_name}" "${installation_dir}"
    [[ -n "${pack_save_path}" && -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
}


#########  2-5. Config Directive Configuration  #########
fn_DirPermissionConfiguration(){
    local l_dir="${1:-}"
    local l_mode=${2:-755}
    local l_user=${3:-"${user_name}"}
    local l_group=${4:-"${group_name}"}

    [[ -n "${l_dir}" && ! -d "${l_dir}" ]] && mkdir -p "${l_dir}"
    if [[ -d "${l_dir}" ]]; then
        chmod "${l_mode}" "${l_dir}"
        chown -R "${l_user}":"${l_group}" "${l_dir}"
    fi
}

fn_MongodDirectiveSetting(){
    local l_section="${1:-}"
    local l_item="${2:-}"
    local l_val="${3:-}"
    local l_action="${4:-0}"
    local l_path=${l_path:-"${config_path}"}

    if [[ -n "${l_item}" && -n "${l_val}" && -s "${l_path}" ]]; then
        [[ "${l_action}" -eq 0 ]] && fnBase_OperationProcedureStatement "${l_item}"
        if [[ "${l_val}" =~ @ ]]; then
            sed -r -i '/'"${l_section}"'[[:space:]]*begin/,/'"${l_section}"'[[:space:]]*end/{/^[[:space:]]*#?[[:space:]]*'"${l_item}"'[[:space:]]*:/{s/^([[:space:]]*)#?[[:space:]]*([^:]+:).*/\1\2 '"${l_val}"'/g;}}' "${l_path}"
        else
            sed -r -i '/'"${l_section}"'[[:space:]]*begin/,/'"${l_section}"'[[:space:]]*end/{/^[[:space:]]*#?[[:space:]]*'"${l_item}"'[[:space:]]*:/{s@^([[:space:]]*)#?[[:space:]]*([^:]+:).*@\1\2 '"${l_val}"'@g;}}' "${l_path}"
        fi
        # sed -r -i '/'"${l_section}"'[[:space:]]*begin/,/'"${l_section}"'[[:space:]]*end/{/^[[:space:]]*#?[[:space:]]*'"${l_item}"'[[:space:]]*:/{s@^([[:space:]]*)#?[[:space:]]*([^:]+:).*@\1\2 '"${l_val}"'@g;}}' "${l_path}"
        [[ "${l_action}" -eq 0 ]] && fnBase_OperationProcedureResult "${l_val}"
    fi
}

fn_MongodConfConfiguration(){
    fnBase_OperationPhaseStatement "${daemon_name}.conf configuration"
    # https://docs.mongodb.com/manual/reference/configuration-options/

    fn_DirPermissionConfiguration "${config_dir}"
    [[ -s "${config_path}" ]] || $download_method "${online_mongod_cnf_sample_url}" > "${config_path}"
    chmod 640 "${config_path}"
    chown "${user_name}":"${group_name}" "${config_path}"

    # - configuration configuration
    fn_MongodDirectiveSetting 'network interfaces' 'port' "${mongod_port}"
    fn_MongodDirectiveSetting 'network interfaces' 'pathPrefix' "${run_dir}"

    fn_MongodDirectiveSetting 'process management' 'pidFilePath' "${run_dir}/${daemon_name}.pid"

    # processManagement.fork
    # https://docs.mongodb.com/manual/reference/configuration-options/#processManagement.fork
    # Enable a daemon mode that runs the mongos or mongod process in the background. By default mongos or mongod does not run as a daemon: typically you will run mongos or mongod as a daemon, either by using processManagement.fork or by using a controlling process that handles the daemonization process (e.g. as with upstart and systemd).
    fn_MongodDirectiveSetting 'process management' 'fork' 'false'

    fn_MongodDirectiveSetting 'data storage' 'dbPath' "${data_dir}"
    fn_MongodDirectiveSetting 'log storage' 'path' "${log_dir}/${daemon_name}.log"
}

#########  2-6. Service Script  #########
fn_ServiceScriptConfiguration(){
    fnBase_OperationProcedureStatement 'Service Script'
    # - init / service script
    local l_service_script_link=''
    local l_service_script_dir=''
    local l_service_script_path=''

    if fnBase_CommandExistIfCheck 'systemctl'; then
        l_service_script_dir='/etc/systemd/system'
        l_service_script_path="${l_service_script_dir}/${daemon_name}.service"

        [[ -f "${l_service_script_path}" ]] && fnBase_SystemServiceManagement "${l_service_script_path##*/}" 'stop'

        # https://unix.stackexchange.com/questions/207469/systemd-permission-issue-with-mkdir-execstartpre
        # https://serverfault.com/questions/779634/create-a-directory-under-var-run-at-boot
        echo -e "[Unit]\nDescription=MongoDB Community Server\nAfter=network.target\nDocumentation=https://docs.mongodb.org/manual\n\n[Service]\nUser=mongodb\nGroup=mongodb\nRestart=on-failure\nRuntimeDirectory=${run_dir##*/}\nRuntimeDirectoryMode=750\nExecStart=${installation_dir}/bin/mongod --config ${config_path} --pidfilepath ${run_dir}/${daemon_name}.pid\n# file size\nLimitFSIZE=infinity\n# cpu time\nLimitCPU=infinity\n# virtual memory size\nLimitAS=infinity\n# open files\nLimitNOFILE=65535\n# processes/threads\nLimitNPROC=65535\n# locked memory\nLimitMEMLOCK=infinity\n# total threads (user+kernel)\nTasksMax=infinity\nTasksAccounting=false\n[Install]\nWantedBy=multi-user.target" > "${l_service_script_path}"

        sed -r -i '/^\[Service\]/,/^\[/{s@^(User=).*$@\1'"${user_name}"'@g;}' "${l_service_script_path}"
        sed -r -i '/^\[Service\]/,/^\[/{s@^(Group=).*$@\1'"${user_name}"'@g;}' "${l_service_script_path}"
        local l_service_paras
        l_service_paras="${installation_dir}/bin/${daemon_name} --config ${config_dir}/${daemon_name}.conf --pidfilepath ${run_dir}/${daemon_name}.pid"
        sed -r -i '/^\[Service\]/,/^\[/{s@^(ExecStart=).*$@\1'"${l_service_paras}"'@g;}' "${l_service_script_path}"

        chmod 644 "${l_service_script_path}"
        systemctl daemon-reload 2> /dev/null
    else
        l_service_script_dir='/etc/init.d'
        l_service_script_path="${l_service_script_dir}/${daemon_name}"
        l_service_script_link="${online_service_script_sample_dir}/${daemon_name}.init"

        [[ -f "${l_service_script_path}" ]] && fnBase_SystemServiceManagement "${l_service_script_path##*/}" 'stop'

        $download_method "${l_service_script_link}" > "${l_service_script_path}"
        sed -r -i '/Configuraton Start/,/Configuraton End/{s@^(USER=).*$@\1'"${user_name}"'@g;s@^(GROUP=).*$@\1'"${group_name}"'@g;}' "${l_service_script_path}"
        chmod 755 "${l_service_script_path}"
    fi

    fnBase_OperationProcedureResult "${l_service_script_path}"
}

#########  2-7. Security Enhancement  #########
fn_SysctlDirectiveSetting(){
    local l_item=${1:-}
    local l_val=${2:-}
    local l_action=${3:-}
    local l_path=${l_path:-'/etc/sysctl.conf'}

    if [[ -n "${l_item}" && -f "/proc/sys/${l_item//./\/}" && -n "${l_val}" && -f "${l_path}" ]]; then
        if [[ -n $(sed -r -n '/^#?[[:space:]]*'"${l_item}"'[[:space:]]*=/{p}' "${l_path}") ]]; then
            case "${l_action,,}" in
                d|delete )
                    sed -r -i '/^#?[[:space:]]*'"${l_item}"'[[:space:]]*=/d' "${l_sysctl_path}" 2> /dev/null
                    ;;
                c|comment )
                    sed -r -i '/^#?[[:space:]]*'"${l_item}"'[[:space:]]*=/{s@^#?[[:space:]]*(.*)$@# \1@g}' "${l_sysctl_path}" 2> /dev/null
                    ;;
                * )
                    sed -r -i '/^#?[[:space:]]*'"${l_item}"'[[:space:]]*=/{s@^#?[[:space:]]*([^=]+=[[:space:]]*).*@\1'"${l_val}"'@g}' "${l_sysctl_path}" 2> /dev/null
                    ;;
            esac
        else
            [[ -z "${l_action}" ]] && sed -r -i '$ '"${l_item}"' = '"${l_val}"'' "${l_sysctl_path}" 2> /dev/null
        fi
    fi
}

fn_SecurityEnhancement(){
    # https://docs.mongodb.com/manual/administration/security-checklist/
    # https://docs.mongodb.com/manual/core/security-hardening/
    fnBase_OperationPhaseStatement 'Security Enhancement'

    # - User administrator
    local l_mongo_exec_path=${l_mongo_exec_path:-"${installation_dir}/bin/mongo"}
    local admin_password_new=${admin_password_new:-}
    if [[ -n "${admin_name}" && -s "${l_mongo_exec_path}" ]]; then
        # https://docs.mongodb.com/manual/tutorial/enable-authentication/
        # https://docs.mongodb.com/manual/reference/program/mongo/#mongo-shell-authentication-options
        fnBase_OperationProcedureStatement 'User administrator'

        fnBase_SystemServiceManagement "${daemon_name}" 'start'

        while [[ -z $(ss -tnl | sed -r -n '/:'"${mongod_port}"'/{s@^[^:]+:([^[:space:]]+).*@\1@g;p}') ]]; do
            sleep 1
        done

        admin_password_new=$(fnBase_RandomPasswordGeneration)

        "${l_mongo_exec_path}" admin --eval 'db.createUser({user:"'"${admin_name}"'",pwd:"'"${admin_password_new}"'", roles:[{role:"userAdminAnyDatabase",db:"admin"}]})' 1> /dev/null

        sed -r -i '/security begin/,/security end/{/(security|authorization):/{s@#[[:space:]]*@@g;}}' "${config_path}"
        fn_MongodDirectiveSetting 'security' 'authorization' 'enabled' '1'

        fnBase_OperationProcedureResult "${admin_name}:${admin_password_new}"
    fi

    # - sysctl
    local l_sysctl_path=${l_sysctl_path:-'/etc/sysctl.conf'}
    if [[ -f "${l_sysctl_path}" ]]; then
        fnBase_OperationProcedureStatement 'sysctl optimization'
        # Optimizing Your Linux Environment for MongoDB
        # https://severalnines.com/blog/optimizing-your-linux-environment-mongodb
        fn_SysctlDirectiveSetting 'net.core.somaxconn' '4096'
        fn_SysctlDirectiveSetting 'net.ipv4.tcp_fin_timeout' '30'
        fn_SysctlDirectiveSetting 'net.ipv4.tcp_keepalive_intvl' '30'
        fn_SysctlDirectiveSetting 'net.ipv4.tcp_keepalive_time' '120'
        fn_SysctlDirectiveSetting 'net.ipv4.tcp_max_syn_backlog' '4096'
        fn_SysctlDirectiveSetting 'vm.dirty_background_ratio' '10'
        fn_SysctlDirectiveSetting 'vm.dirty_ratio' '20'
        fn_SysctlDirectiveSetting 'vm.swappiness' '6'
        fnBase_OperationProcedureResult "${l_sysctl_path}"
    fi

    # - firewall
    if [[ "${enable_firewall}" -eq 1 ]]; then
        fnBase_OperationProcedureStatement 'Firewall configuration'
        # $download_method "${firewall_configuration_script}" | bash -s -- -p "${mongod_port}" -H "${remote_host_ip}" -s
        $download_method "${firewall_configuration_script}" | bash -s -- -p "${mongod_port}" -s
        fnBase_OperationProcedureResult "${mongod_port}"
    fi

    # - self-signed ssl certificate
    local l_key_passphrases
    local l_cert_dir=${l_cert_dir:-"${config_dir}/ssl"}
    fn_DirPermissionConfiguration "${l_cert_dir}"
    local l_self_cert_path=${l_self_cert_path:-"${l_cert_dir}/${application_name}.pem"}
    # https://testnb.readthedocs.io/en/stable/examples/Notebook/Configuring%20the%20Notebook%20and%20Server.html
    # openssl req -x509 -nodes -days 365 -newkey rsa:1024 -keyout mycert.pem -out mycert.pem
    if [[ ! -f "${l_self_cert_path}" ]]; then
        fnBase_OperationProcedureStatement 'Self-signed ssl cert'
        local l_cert_C=${l_cert_C:-'CN'}    # Country Name
        local l_cert_ST=${l_cert_ST:-'Shanghai'}    # State or Province Name
        local l_cert_L=${l_cert_L:-'Shanghai'}    # Locality Name
        local l_cert_O=${l_cert_O:-'MaxdSre'}    # Organization Name
        local l_cert_OU=${l_cert_OU:-'MongoDB'}    # Organizational Unit Name
        local l_cert_CN=${l_cert_CN:-'127.0.0.1'}    # Common Name
        # https://www.mongodb.com/community
        local l_cert_email=${l_cert_email:-'community@mongodb.com'}    # Email Address

        l_cert_C="${ip_public_country_code}"
        l_cert_ST="${ip_public_locate%%.*}"
        l_cert_L="${ip_public_locate##*.}"

        # https://docs.mongodb.com/manual/tutorial/configure-ssl/
        # openssl req -newkey rsa:2048 -new -x509 -days 365 -nodes -out mongodb-cert.crt -keyout mongodb-cert.key
        # cat mongodb-cert.key mongodb-cert.crt > mongodb.pem
        # -nodes if this option is specified then if a private key is created it will not be encrypted.

        # expire date 3650 days, crypt type RSA, key length 4096
        # openssl req -x509 -nodes -days 3650 -newkey rsa:4096 -keyout "${l_self_cert_path}" -out "${l_self_cert_path}" -subj "/C=${l_cert_C}/ST=${l_cert_ST}/L=${l_cert_L}/O=${l_cert_O}/OU=${l_cert_OU}/CN=${l_cert_CN}/emailAddress=${l_cert_email}" 2> /dev/null
        # openssl rsa -in "${l_self_cert_path}" -text -noout 2> /dev/null

        l_key_passphrases=$(fnBase_RandomPasswordGeneration)
        openssl req -x509 -days 3650 -newkey rsa:4096 -keyout "${l_self_cert_path}" -out "${l_self_cert_path}" -passout pass:"${l_key_passphrases}" -subj "/C=${l_cert_C}/ST=${l_cert_ST}/L=${l_cert_L}/O=${l_cert_O}/OU=${l_cert_OU}/CN=${l_cert_CN}/emailAddress=${l_cert_email}" 2> /dev/null

        if [[ -f "${config_path}" ]]; then
            # net.ssl.PEMKeyFile   The .pem file that contains both the TLS/SSL certificate and key.
            # net.ssl.PEMKeyPassword  The password to de-crypt the certificate-key file (i.e. PEMKeyFile).
            # net.ssl.CAFile The .pem file that contains the root certificate chain from the Certificate Authority.
            sed -r -i '/network interfaces begin/,/network interfaces end/{/(ssl|mode|PEMKeyFile|PEMKeyPassword|disabledProtocols):/{s@#[[:space:]]*@@g;}}' "${config_path}"
            fn_MongodDirectiveSetting 'network interfaces' 'PEMKeyFile' "${l_self_cert_path}" '1'
            fn_MongodDirectiveSetting 'network interfaces' 'PEMKeyPassword' "${l_key_passphrases}" '1'
            fn_MongodDirectiveSetting 'network interfaces' 'allowConnectionsWithoutCertificates' 'true' '1'
            fn_MongodDirectiveSetting 'network interfaces' 'allowInvalidCertificates' 'true' '1'
            # fn_MongodDirectiveSetting 'network interfaces' 'allowInvalidHostnames' 'false' '1'
        fi

        [[ -f "${l_self_cert_path}" ]] && chmod 644 "${l_self_cert_path}"
        fnBase_OperationProcedureResult "${l_self_cert_path}"
    fi


    if [[ -s "${login_user_home}/.bashrc" ]]; then
        fnBase_OperationProcedureStatement 'Command alias'

        sed -r -i '/'"${bashrc_string}"' start/,/'"${bashrc_string}"' end/d' "${login_user_home}/.bashrc"
        echo -e "#${bashrc_string} start\nalias mongo_command=\"${l_mongo_exec_path} -u '${admin_name}' -p '${admin_password_new}' --ssl --sslAllowInvalidCertificates --sslPEMKeyFile ${l_self_cert_path} --sslPEMKeyPassword ${l_key_passphrases} --authenticationDatabase 'admin'\"\n#${bashrc_string} end" >> "${login_user_home}/.bashrc"
        fnBase_OperationProcedureResult '~/.bashrc alias mongo_command'
    fi

    fnBase_SystemServiceManagement "${daemon_name}" 'restart'
}


#########  2-8. Post-installation Configuration  #########
fn_PostInstallationConfiguration(){
    fnBase_OperationPhaseStatement 'Configuration'

    if [[ "${is_existed}" -eq 0 ]]; then
        fn_DirPermissionConfiguration "${log_dir}"
        fn_DirPermissionConfiguration "${data_dir}"
        fn_DirPermissionConfiguration "${run_dir}"
    fi

    # - bin path
    fnBase_OperationProcedureStatement 'Executing path'
    echo "export PATH=${installation_dir}/bin:\$PATH" > "${profile_d_path}"
    # shellcheck source=/dev/null
    source "${profile_d_path}" 2> /dev/null
    fnBase_OperationProcedureResult "${profile_d_path}"

    if [[ "${is_existed}" -eq 0 ]]; then
        # - init / service script
        fn_ServiceScriptConfiguration

        # - directive configuration
        fn_MongodConfConfiguration

        # - security enhancement
        fn_SecurityEnhancement
    fi
}


#########  2-5. Operation Time Cost  #########
fn_TotalTimeCosting(){
    finish_time=$(date +'%s')        # End Time Of Operation
    total_time_cost=$((finish_time-start_time))   # Total Time Of Operation
    printf "\nTotal time cost is ${c_red}%s${c_normal} seconds!\n" "${total_time_cost}"
    printf "\n${c_bold}${c_blue}%s${c_normal}: You need to relogin to make bash profile effect!\n" 'Notice'
}




#########  3. Executing Process  #########
fn_InitializationCheck

fn_UninstallOperation
fn_OSInfoDetection
fn_VersionComparasion
fn_Prerequisites
fn_DownloadAndDecompressOperation
fn_PostInstallationConfiguration
fn_TotalTimeCosting


#########  4. EXIT Singal Processing  #########
# trap "commands" EXIT # execute command when exit from shell
fn_TrapEXIT(){
    fnBase_UnsetVars
    unset software_fullname
    unset user_name
    unset group_name
    unset mongod_port
    unset data_dir
    unset config_dir
    unset log_dir
    unset run_dir
    unset profile_d_path
    unset installation_dir
    unset is_existed
    unset version_check
    unset admin_name
    unset admin_password_new
    unset source_pack_path
    unset os_detect
    unset is_uninstall
    unset security_enhancement
    unset enable_firewall
}

trap fn_TrapEXIT EXIT


# mongod --sslMode requireSSL --sslDisabledProtocols TLS1_0,TLS1_1 --sslPEMKeyFile /etc/ssl/mongodb.pem --sslCAFile /etc/ssl/ca.pem <additional options>

# db.getRole( "readWrite", { showPrivileges: true } )

# Script End
