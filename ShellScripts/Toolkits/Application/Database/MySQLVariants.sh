#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails

# Target: Installing MySQL/MariaDB/Percona Via Their Official Repository On GNU/Linux (RHEL/CentOS/Fedora/Debian/Ubuntu/SLES/OpenSUSE)
# Developer: MaxdSre

# Change Log:
# - Oct 08, 2020 08:49 Thu ET - initialization check method update
# - Oct 15, 2019 12:18 Tue ET - change custom script url
# - Oct 14, 2019 13:10 Fri  ET - change to new base functions
# - Dec 31, 2018 16:46 Mon ET - code reconfiguration, include base funcions from other file
# - Apr 19, 2018 14:42 Thu ET - remove NO_AUTO_CREATE_USER in sql_mode for MySQL 8
# - Jan 22, 2018 13:03 Mon +0800 - conf optimization
# - Jan 08 ~ 09, 2018 19:38 Mon +0800 - add SELinux support, reconfiguration 2 days
# - Dec 06, 2017 18:24 Wed +0800 - reconfiguration 2 days
# - Nov 03, 2017 16:49 Fri +0800
# - Oct 27, 2017 17:46 Fri +0800
# - Jul 19, 2017 13:18 Wed +0800 ~ Sep 08, 2017 17:58 Fri +0800


# https://www.percona.com/blog/2017/11/02/mysql-vs-mariadb-reality-check/
# https://www.atlantic.net/community/whatis/mysql-vs-mariadb-vs-percona/

# benchmark: sysbench
# monitor tool: innotop, mytop, mysqladmin, prometheus&grafana

# Optimization
# https://minervadb.com/index.php/2018/02/28/mysql-5-7-performance-tuning-immediately-after-installation/


#########  0-1. Singal Setting  #########
trap fnBase_TrapCleanUp INT QUIT

#########  0-2. Variables Setting  #########
# umask 022    # temporarily change umask value to 022
readonly custom_shellscript_url='https://gitlab.com/axdsop/nixnotes/raw/master/ShellScripts/Toolkits'
readonly os_check_script="${custom_shellscript_url}/GNULinux/gnuLinuxMachineInfoDetection.sh"
readonly firewall_configuration_script="${custom_shellscript_url}/GNULinux/gnuLinuxFirewallRuleConfiguration.sh"
readonly mysql_variants_version_list="${custom_shellscript_url}/_Configs/Source/mysqlVariantsVersionAndLinuxDistroRelationTable.txt"
readonly mysqld_cnf_url="${custom_shellscript_url}/_Configs/Application/mysql/mysqld.cnf"

auto_installation=${auto_installation:-0}
root_password_new=${root_password_new:-''}
mysql_variant_type=${mysql_variant_type:-''}
variant_version=${variant_version:-''}
enable_firewall=${enable_firewall:-0}
slave_mode=${slave_mode:-0}
proxy_server_specify=${proxy_server_specify:-''}

readonly data_dir_default='/var/lib/mysql'
readonly mysql_port_default='3306'
readonly conf_path_default='/etc/my.cnf'
conf_path=${conf_path:-"${conf_path_default}"}

data_dir=${data_dir:-"${data_dir_default}"}
mysql_port=${mysql_port:-"${mysql_port_default}"}
bind_ip=${bind_ip:-'127.0.0.1'}
mysql_log_dir=${mysql_log_dir:-}
mysql_run_dir=${mysql_run_dir:-'/var/run/mysqld'}

db_name=${db_name:-}    # Percona-Server, MySQL, MariaDB
db_version=${db_version:-}
db_newly_installed_version=${db_newly_installed_version:-}

service_name=${service_name:-'mysql'}
# is_existed=${is_existed:-0}
# version_check=${version_check:-0}
# is_uninstall=${is_uninstall:-0}
# remove_datadir=${remove_datadir:-0}


#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | sudo bash -s -- [options] ...

Installing MySQL/MariaDB/Percona On GNU/Linux Via Official Repository!
This script requires superuser privileges (eg. root, su).

[available option]
    -h    --help, show help info
    -a    --auto installation, default choose MySQL variants latest version (Percona > MariaDB > MySQL)
    -t variant_type    --set MySQL variant (MySQL|MariaDB|Percona)
    -v variant_version --set MySQL variant version (eg: 5.6|5.7|8.0|10.x), along with -t
    -d data_dir    --set data dir, default is /var/lib/mysql
    -s port    --set MySQL port number, default is 3306
    -b bind_ip    --set 'bind_address', default is local ('127.0.0.1'), can also set all ('0.0.0.0') or custom ip e.g. '192.168.8.8' or '192.168.8.0/24'
    -S    --enable slave mode, default is master mode
    -f    --enable firewall rule (iptable/firewalld/ufw/SuSEfirewall2), default is disable
    -P [protocol:]ip:port    --proxy host (http|https|socks4|socks5), default protocol is http
    -U    --update/install base script content in '~/.axdsop/', default is no action
\e[0m"
# -p root_passwd    --set root password for 'root'@'localhost', default is empty or temporary password in /var/log/mysqld.log or /var/log/mysql/mysqld.log
}

while getopts "had:s:b:t:v:SfP:" option "$@"; do
    case "$option" in
        a ) auto_installation=1;;
        # p ) root_password_new="$OPTARG" ;;
        d ) data_dir="$OPTARG" ;;
        s ) mysql_port="$OPTARG" ;;
        b ) bind_ip="$OPTARG" ;;
        t ) mysql_variant_type="$OPTARG" ;;
        v ) variant_version="$OPTARG" ;;
        S ) slave_mode=1 ;;
        f ) enable_firewall=1 ;;
        P ) proxy_server_specify="$OPTARG" ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done


#########  1-2 Preparation Operation  #########
fn_InitializationCheck(){
    local l_user_codebase_dir="$HOME/.config/axdsop"
    local l_base_function_temp_save_path=''
    local l_need_remove=0
    local l_base_function_url='https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/base_functions.sh'
    l_base_function_temp_save_path=$(find "${l_user_codebase_dir}" -type f -name "${l_base_function_url##*/}" -print 2> /dev/null)
    
    if [[ ! -f "${l_base_function_temp_save_path}" ]]; then
        local l_download_tool=${l_download_tool:-'wget -qO-'}
        [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_tool='curl -fsL'
        l_need_remove=1
        l_base_function_temp_save_path=$(mktemp -t XXXXXXX)
        ${l_download_tool} "${l_base_function_url}" > "${l_base_function_temp_save_path}"
    fi

    if [[ -f "${l_base_function_temp_save_path}" && -s "${l_base_function_temp_save_path}" ]]; then
        . "${l_base_function_temp_save_path}"
        [[ "${l_need_remove}" -eq 1 ]] && rm "${l_base_function_temp_save_path}"
    else
        echo "Sorry, fail to get base script path."
        exit
    fi


    # tput clear  # Echo the clear-screen sequence for the current terminal.
    # specify 1 means need sudo privilege
    fnBase_RunningEnvironmentCheck '1'

    login_user_cnf_path=${login_user_cnf_path:-"${login_user_home}/.my.cnf"}
    mysql_custom_command=${mysql_custom_command:-"mysql --defaults-file=${login_user_cnf_path}"}
}

fn_OSInfoDetection(){
    local l_json_osinfo=${l_json_osinfo:-}
    l_json_osinfo=$(fnBase_OSInfoRetrieve "${os_check_script}")
    # [[ -z "${l_json_osinfo}" ]] && fnBase_ExitStatement "${c_red}Fatal${c_normal}: fail to extract os info!"

    distro_fullname=${distro_fullname:-}
    distro_fullname=$(fnBase_OSInfoDirectiveValExtraction 'distro_fullname' "${l_json_osinfo}")

    distro_name=${distro_name:-}
    distro_name=$(fnBase_OSInfoDirectiveValExtraction 'distro_name' "${l_json_osinfo}")
    distro_name="${distro_name%%-*}"    # rhel/centos/fedora/debian/ubuntu/sles/opensuse

    codename=${codename:-}
    codename=$(fnBase_OSInfoDirectiveValExtraction 'distro_codename' "${l_json_osinfo}")

    version_id=${version_id:-}
    version_id=$(fnBase_OSInfoDirectiveValExtraction 'distro_version_id' "${l_json_osinfo}")

    ip_local=${ip_local:-'127.0.0.1'}
    ip_local=$(fnBase_OSInfoDirectiveValExtraction 'ip_local' "${l_json_osinfo}")

    pack_manager=${pack_manager:-}
    pack_manager=$(fnBase_OSInfoDirectiveValExtraction 'distro_pack_manager' "${l_json_osinfo}")

    fnBase_CentralOutputTitle 'GNU/Linux Distribution Information'

    [[ -z "${distro_name}" ]] || fnBase_CentralOutput 'Distro Name' "${distro_name}"
    [[ -z "${version_id}" ]] || fnBase_CentralOutput 'Version ID' "${version_id}"
    [[ -z "${codename}" ]] || fnBase_CentralOutput "Code Name" "${codename}"
    [[ -z "${distro_fullname}" ]] || fnBase_CentralOutput 'Full Name' "${distro_fullname}"
    fnBase_CentralOutputTitle

    version_id=${version_id%%.*}
}

fn_PackageOperationProcedureStatement(){
    local l_action="${1:-'install'}"
    local l_item="${2:-}"
    local l_name="${3:-}"
    if [[ -n "${l_action}" && -n "${l_item}" ]]; then
        local l_punctuation='+'
        [[ "${l_action}" == 'install' ]] || l_punctuation='remove'
        fnBase_OperationProcedureStatement "${l_punctuation} package ${c_bold}${c_yellow}${l_item}${c_normal}"
        fnBase_PackageManagerOperation "${l_action}" "${l_item}"
        fnBase_OperationProcedureResult "${l_name}"
    fi
}

# - DB Relevant Custom Function
fn_CodenameForDatabase(){
    local databaseType="${1:-}"
    local distroName="${2:-}"
    local versionId="${3:-}"
    [[ -z "${versionId}" ]] || versionId="${versionId%%.*}"

    if [[ -n "${databaseType}" ]]; then
        case "${databaseType,,}" in
            mysql )
                case "${distroName,,}" in
                    rhel|centos ) codename="el${versionId}" ;;
                    fedora ) codename="fc${versionId}" ;;
                    sles ) codename="sles${versionId}" ;;
                esac
                ;;
            mariadb )
                case "${distroName,,}" in
                    rhel|centos|fedora|opensuse ) codename="${distroName,,}${versionId}" ;;
                esac
                ;;
            percona|percona-server )
                case "${distroName,,}" in
                    rhel|centos ) codename="rhel${versionId}" ;;
                esac
                ;;
        esac
    fi

    # MySQL  rhel/centos  --> el7, el6, el5
    # MySQL  fedora       --> fc28, fc27, fc26, fc25
    # MySQL  sles         --> sles12, sles11
    # MariaDB  rhel       --> rhel7, rhel6, rhel5
    # MariaDB  centos     --> centos7,centos6
    # MariaDB  fedora     --> fedora26, fedora25
    # MariaDB  opensuse   --> opensuse42
    # Percona rhel/centos  --> rhel7, rhel6, rhel5
}

fn_DBServiceStatusOperation(){
    # service name: mariadb/mysql/mysqld
    local l_service_name="${1:-}"
    local l_action="${2:-}"

    if [[ -n "${l_service_name}" ]]; then
        case "${l_action}" in
            start|stop|reload|restart|status ) l_action="${l_action}" ;;
            * ) l_action='status' ;;
        esac

        # mariadb 5.5 just use  service mysql {status,start,stop}
        if [[ "${db_name}" == 'MariaDB' && "${db_version%%.*}" -lt 10 ]]; then
            service "${l_service_name}" "${l_action}" &> /dev/null
        else
            fnBase_SystemServiceManagement "${l_service_name}" "${l_action}"
        fi
    fi
}

fn_ConfDirectiveConfiguration(){
    local l_conf_path="${1:-}"
    local l_para="${2:-}"
    local l_para_val="${3:-}"

    if [[ -f "${l_conf_path}" && -n "${l_para}" && -n "${l_para_val}" ]]; then
        case "${l_para_val}" in
            d|delete )
                sed -r -i '/\[mysqld\]/,${/^#*[[:space:]]*'"${l_para}"'[[:space:]]*=/d}' "${l_conf_path}" 2> /dev/null
                ;;
            u|uncomment )
                sed -r -i '/\[mysqld\]/,${/^#*[[:space:]]*'"${l_para}"'[[:space:]]*=/{s@^#?[[:space:]]*@@g;}}' "${l_conf_path}" 2> /dev/null
                ;;
            * )
                sed -r -i '/\[mysqld\]/,${/^#*[[:space:]]*'"${l_para}"'[[:space:]]*=/{s@^#?[[:space:]]*([^=]+=[[:space:]]*)([^[:space:]]+)(.*)$@\1'"${l_para_val}"'\3@g;}}' "${l_conf_path}" 2> /dev/null
                ;;
        esac
    fi
}

fn_InstallationProcedureStatement(){
    local l_item="${1:-}"

    if [[ -n "${l_item}" ]]; then
        case "${l_item,,}" in
            repo ) l_item='official repository' ;;
            pack ) l_item='package installation' ;;
            gpg ) l_item='GnuPG key installation'
        esac
        fnBase_OperationProcedureStatement "${l_item}"
        # printf "procedure - ${c_blue}%s${c_normal} is finished;\n" "${l_item}"
    fi
    # fnBase_OperationProcedureResult
}



################ 2-1. Choose Database & Version ################
fn_ExistedDetection(){
    fnBase_OperationPhaseStatement 'Check If Existed'

    fnBase_OperationProcedureStatement 'Detection'
    if fnBase_CommandExistIfCheck 'mysql'; then
        fnBase_OperationProcedureResult
        fnBase_ExitStatement "\n${c_red}Attention: ${c_normal}find existed MySQL variants:\n\n${c_yellow}$(mysql --version)${c_normal}"
    else
        fnBase_OperationProcedureResult 'not find'
    fi
}

# verify port num or data dir is legal or not
fn_ParaSpecifiedValVerification(){
    fnBase_OperationPhaseStatement 'Parameters Specified Val Verification'

    # 1- verify if the port specified is available
    fnBase_OperationProcedureStatement 'Port No. verification'
    mysql_port=$(echo "${mysql_port}" | sed -r 's@[[:alpha:]]*@@g;s@[[:punct:]]*@@g;s@[[:blank:]]*@@g')
    if [[ -n "${mysql_port}" ]]; then
        if [[ "${mysql_port}" -eq "${mysql_port_default}" ]]; then
            fnBase_OperationProcedureResult "${mysql_port}"
        elif [[ "${mysql_port}" =~ ^[1-9]{1}[0-9]{1,4}$ ]]; then
            local sys_port_start=${sys_port_start:-1024}
            local sys_port_end=${sys_port_end:-65535}

            if [[ "${mysql_port}" -ge "${sys_port_start}" && "${mysql_port}" -le "${sys_port_end}" ]]; then
                local port_service_info
                port_service_info=$(awk 'match($2,/^'"${mysql_port}"'\/tcp$/){print $1,$2}' /etc/services 2> /dev/null)
                if [[ -n "${port_service_info}" ]]; then
                    fnBase_OperationProcedureResult "${port_service_info} in /etc/services" 1
                else
                    fnBase_OperationProcedureResult "${mysql_port}"
                fi    # end if port_service_info
            else
                fnBase_OperationProcedureResult "${mysql_port}⊄(${sys_port_start},${sys_port_end}) out of range" 1
            fi

        else
            fnBase_OperationProcedureResult "${mysql_port} illegal" 1
        fi

    fi

    # 2- verify data dir
    fnBase_OperationProcedureStatement 'Data dir verification'
    if [[ -n "${data_dir}" ]]; then
        data_dir="${data_dir%/}"
        if [[ "${data_dir}" == "${data_dir_default}" || "${data_dir}" =~ ^/ ]]; then
            fnBase_OperationProcedureResult "${data_dir}"
        else
            fnBase_OperationProcedureResult "${data_dir}" 1
            fnBase_ExitStatement "${c_red}Attention: ${c_normal}data dir path must be begin with slash ${c_yellow}/${c_normal} or ${c_yellow}~/${c_normal}."
        fi
    fi
}

# - variant and version (V2) choose has 3 method
# extract mysql variants lists or variant version lists, invoked by funcV2*
fn_V2InfoExtraction(){
    local local_variants_version_list="${1:-}"
    local local_distro_name="${2:-}"
    local local_db_name="${3:-}"
    local local_codename="${4:-}"
    local local_output=${local_output:-}

    if [[ -n "${local_distro_name}" && -n "${local_db_name}" && -n "${local_codename}" ]]; then
        # variant version
        local_output=$(awk -F\| 'match($4,/'"${local_distro_name}"'/)&&match($1,/'"${local_db_name}"'/)&&match($2,/^'"${local_codename}"'$/){print $3}' "${local_variants_version_list}")

    elif [[ -n "${local_distro_name}" ]]; then
        # mysql variants
        local_output=$(awk -F\| 'match($4,/'"${local_distro_name}"'/){a[$1]=$0}END{PROCINFO["sorted_in"]="@ind_str_asc"; for (i in a) print i}' "${local_variants_version_list}" | sed ':a;N;$!ba;s@ @-@g;s@\n@ @g')
    fi

    echo "${local_output}"
}

# - 1. via choose list - default operation
fn_V2SelectionListOperation(){
    # 1 - database choose
    echo "${c_yellow}Available MySQL Variants List:${c_normal}"
    PS3="Choose variant number: "

    # fn_V2InfoExtraction "${v2_info_list}" "${distro_name}"
    select item in $(fn_V2InfoExtraction "${v2_info_list}" "${distro_name}"); do
        db_name="${item}"
        [[ -n "${db_name}" ]] && break
    done < /dev/tty

    if [[ -z "${db_name}" ]]; then
        fnBase_ExitStatement "${c_red}Sorry${c_normal}: no available MySQL variants support your system ${c_yellow}${distro_fullname}${c_normal}."
    fi

    # 2 - specific version choose
    echo -e "\n${c_yellow}Please Select Specific${c_normal} ${c_red}${db_name}${c_normal}${c_yellow} Version: ${c_normal}"
    PS3="Choose version number: "

    # generate specific codename for rhel/centos/fedora/sles/opensuse
    # echo "db name is ${db_name}, distro name is ${distro_name}, version id is ${version_id}"
    fn_CodenameForDatabase "${db_name}" "${distro_name}" "${version_id}"

    select item in $(fn_V2InfoExtraction "${v2_info_list}" "${distro_name}" "${db_name}" "${codename}"); do
        db_version="${item,,}"
        [[ -n "${db_version}" ]] && break
    done < /dev/tty

    unset PS3

    if [[ -z "${db_version}" ]]; then
        fnBase_ExitStatement "${c_red}Sorry${c_normal}: no available version of ${c_yellow}${db_name}${c_normal} support your system ${c_yellow}${distro_fullname}${c_normal}."
    fi
}

# - 2. auto_installation
fn_V2AutomaticSelection(){
    # generate specific codename for rhel/centos/fedora/sles/opensuse
    fn_CodenameForDatabase "${db_name}" "${distro_name}" "${version_id}"

    # sequence: Percona, MariaDB, MySQL
    db_name='Percona'
    local db_version_list=${db_version_list:-}
    db_version_list=$(fn_V2InfoExtraction "${v2_info_list}" "${distro_name}" "${db_name}" "${codename}")
    db_version=$(echo "${db_version_list}" | awk '{print $1}')

    if [[ -z "${db_version}" ]]; then
        db_name='MariaDB'
        db_version_list=$(fn_V2InfoExtraction "${v2_info_list}" "${distro_name}" "${db_name}" "${codename}")
        db_version=$(echo "${db_version_list}" | awk '{print $1}')

        if [[ -z "${db_version}" ]]; then
            db_name='MySQL'
            db_version_list=$(fn_V2InfoExtraction "${v2_info_list}" "${distro_name}" "${db_name}" "${codename}")
            db_version=$(echo "${db_version_list}" | awk '{print $1}')

            if [[ -z "${db_version}" ]]; then
                fnBase_ExitStatement "${c_red}Sorry${c_normal}: no appropriate MySQL variant & version finds!"
            fi    # end MySQL

        fi    # end MariaDB

    fi    # end Percona
}

# - 3. manually setting
fn_V2ManuallySpecify(){
    case "${mysql_variant_type}" in
        MySQL|mysql|my ) db_name='MySQL' ;;
        MariaDB|mariadb|ma ) db_name='MariaDB' ;;
        Percona|percona|pe|p ) db_name='Percona' ;;
        * ) fnBase_ExitStatement "${c_red}Sorry${c_normal}: please specify correct ${c_yellow}MySQL/MariaDB/Percona${c_normal} via ${c_yellow}-t${c_normal}!"
    esac

    # generate specific codename for rhel/centos/fedora/sles/opensuse
    fn_CodenameForDatabase "${db_name}" "${distro_name}" "${version_id}"

    local db_version_list=${db_version_list:-}
    db_version_list=$(fn_V2InfoExtraction "${v2_info_list}" "${distro_name}" "${db_name}" "${codename}")

    db_version="${variant_version}"
    if [[ -z "${db_version_list}" ]]; then
        fnBase_ExitStatement "${c_red}Sorry${c_normal}: no specific ${c_yellow}${db_name}${c_normal} version finds!"
    elif [[ -z $(echo "${db_version_list}" | sed 's@ @\n@g' | sed -n '/^'"${db_version}"'$/p') ]]; then
        [[ -z "${db_version}" ]] && db_version='NULL'
        fnBase_ExitStatement "${c_red}Sorry${c_normal}: version you specified is ${c_yellow}${db_version}${c_normal}, please specify correct version: ${c_yellow}${db_version_list// /\/}${c_normal} via ${c_yellow}-v${c_normal}!"
    fi
}

# all in one (default/auto installation/manually setting)
fn_V2SelectionListGeneration(){
    fnBase_OperationPhaseStatement 'MySQL Variants Selection'

    v2_info_list=$(mktemp -t "${mktemp_format}")
    # $download_method "${mysql_variants_version_list}" > "${v2_info_list}"
    # exclude Cluser,  substitude '-' for ' ' in db_name
    $download_method "${mysql_variants_version_list}" | awk -F\| 'BEGIN{OFS="|"}{gsub(" ","-",$1); if($1!~/Cluster/) {print}}' > "${v2_info_list}"

    [[ -s "${v2_info_list}" ]] || fnBase_ExitStatement "${c_red}Sorry${c_normal}: fail to get MySQL variants version relation table!"

    if [[ "${auto_installation}" -eq 1 ]]; then
        fn_V2AutomaticSelection
    elif [[ -n "${mysql_variant_type}" ]]; then
        fn_V2ManuallySpecify
    else
        fn_V2SelectionListOperation
    fi

    if [[ -n "${db_version}" ]]; then
        fnBase_OperationProcedureStatement 'Database choose'
        fnBase_OperationProcedureResult "${db_name} ${db_version}"
    else
        fnBase_ExitStatement "\n${c_red}Attention${c_normal}: ${c_yellow}${db_name}${c_normal} official repository may be not available for your distro version."
    fi

    # Percona-Server ==> Percona
    db_name="${db_name%%-*}"

    [[ -f "${v2_info_list}" ]] && rm -f "${v2_info_list}"

    # - MySQL/Percona 5.6 need memory space > 512MB
    case "${db_name}" in
        MySQL|Percona )
            # 524288  512 * 1204   KB
            if [[ "${db_version}" == '5.6' && $(awk 'match($1,/^MemTotal/){print $2}' /proc/meminfo) -le 524288 ]]; then
                fnBase_OperationProcedureStatement 'Memory space check'
                fnBase_OperationProcedureResult "< 512M" 1
                fnBase_ExitStatement "${c_red}Attention${c_normal}: ${c_yellow}${db_name} ${db_version}${c_normal} needs more memory space while installing or service starting."
            fi
            ;;
    esac

    if [[ "${pack_manager}" == 'apt-get' ]]; then
        fnBase_CommandExistIfCheck 'systemctl' || fnBase_PackageManagerOperation 'install' "sysv-rc-conf" # same to chkconfig
    fi
}


################ 2-2. MariaDB/MySQL/Percona Installation ################
fn_MariaDBOperation(){
    # https://mariadb.com/kb/en/the-mariadb-library/yum/
    # https://downloads.mariadb.org/mariadb/repositories
    # https://mariadb.com/kb/en/the-mariadb-library/installing-mariadb-deb-files/
    case "${distro_name}" in
        rhel|centos|fedora|opensuse )
            local system_arch=${system_arch:-'amd64'}
            case "$(uname -m)" in
                x86_64 ) system_arch='amd64' ;;
                x86|i386 ) system_arch='x86' ;;
            esac
            ;;
    esac    # end case distro_name

    # remove MairDB 5.5
    # fnBase_PackageManagerOperation 'remove' "mariadb-server mariadb-libs"   #5.5
    fn_PackageOperationProcedureStatement 'remove' 'mariadb-server mariadb-libs'

    case "${distro_name}" in
        rhel|centos|fedora )
            fn_InstallationProcedureStatement 'repo'
            local repo_path=${repo_path:-'/etc/yum.repos.d/MariaDB.repo'}

            echo -e "# ${db_name} ${db_version} ${distro_name} repository list\n# http://downloads.mariadb.org/mariadb/repositories/\n[${db_name,,}]\nname = ${db_name}\nbaseurl = http://yum.mariadb.org/${db_version}/${codename}-${system_arch}\ngpgkey=https://yum.mariadb.org/RPM-GPG-KEY-MariaDB\ngpgcheck=1" > "${repo_path}"

            # Manually Importing the MariaDB Signing Key
            # rpm --import https://yum.mariadb.org/RPM-GPG-KEY-MariaDB
            fnBase_OperationProcedureResult "${repo_path}"

            fn_InstallationProcedureStatement 'pack'
            # dnf install MariaDB-server  /  yum install MariaDB-server MariaDB-client
            local pack_name_list=${pack_name_list:-'MariaDB-server MariaDB-client'}
            [[ "${distro_name}" == 'fedora' && "${pack_manager}" == 'dnf' ]] && pack_name_list='MariaDB-server'

            fnBase_PackageManagerOperation     # just make cache
            fnBase_PackageManagerOperation 'install' "${pack_name_list}"
            fnBase_OperationProcedureResult "${pack_name_list}"

            service_name='mysql'
            # mariadb 5.5 just use   service mysql {status,start,stop}
            # if [[ "${db_version%%.*}" -lt 10 ]]; then
            #     service "${service_name}" start &> /dev/null
            #     service "${service_name}" restart &> /dev/null
            # else
            #     # service name: mariadb/mysql/mysqld
            #     fnBase_SystemServiceManagement "${service_name}" 'start'
            #     fnBase_SystemServiceManagement "${service_name}" 'restart'
            # fi
            ;;
        opensuse )
            fn_InstallationProcedureStatement 'repo'
            local repo_path=${repo_path:-'/etc/zypp/repos.d/mariadb.repo'}

            echo -e "# ${db_name} ${db_version} ${distro_name} repository list\n# http://downloads.mariadb.org/mariadb/repositories/\n[${db_name,,}]\nname = ${db_name}\nbaseurl = http://yum.mariadb.org/${db_version}/${codename}-${system_arch}\ngpgkey=https://yum.mariadb.org/RPM-GPG-KEY-MariaDB\ngpgcheck=1" > "${repo_path}"
            fnBase_OperationProcedureResult "${repo_path}"

            fn_InstallationProcedureStatement 'gpg'
            # Manually Importing the MariaDB Signing Key  CBCB082A1BB943DB
            rpm --import https://yum.mariadb.org/RPM-GPG-KEY-MariaDB &> /dev/null
            fnBase_OperationProcedureResult 'RPM-GPG-KEY-MariaDB'

            fn_InstallationProcedureStatement 'pack'
            # zypper install MariaDB-server MariaDB-client
            # - OpenSUSE Self Repo: mariadb-server, mariadb-client
            # - MariaDB Official Repo: MariaDB-server MariaDB-client
            local pack_name_list=${pack_name_list:-'MariaDB-server MariaDB-client'}
            fnBase_PackageManagerOperation     # just make cache
            fnBase_PackageManagerOperation 'install' "${pack_name_list}"
            fnBase_OperationProcedureResult "${pack_name_list}"

            # service name: mariadb/mysql/mysqld
            service_name='mysql'
            ;;
        debian|ubuntu )
            fn_InstallationProcedureStatement 'repo'
            local repo_path=${repo_path:-'/etc/apt/sources.list.d/mariadb.list'}
            local repo_mirror_url=${repo_mirror_url:-'http://nyc2.mirrors.digitalocean.com'}

            # Repo mirror site url
            local ip_info=${ip_info:-}
            ip_info=$($download_tool_origin ipinfo.io)
            if [[ -n "${ip_info}" ]]; then
                local host_ip=${host_ip:-}
                local host_country=${host_ip:-}
                local host_city=${host_ip:-}
                local host_org=${host_ip:-}
                host_ip=$(echo "$ip_info" | sed -r -n '/\"ip\"/{s@[[:space:],]*@@g;s@[^:]*:"([^"]*)"@\1@g;p}')
                host_country=$(echo "$ip_info" | sed -r -n '/\"country\"/{s@[[:space:],]*@@g;s@[^:]*:"([^"]*)"@\1@g;p}')
                host_city=$(echo "$ip_info" | sed -r -n '/\"city\"/{s@[[:space:],]*@@g;s@[^:]*:"([^"]*)"@\1@g;p}')
                host_org=$(echo "$ip_info" | sed -r -n '/\"org\"/{s@[[:space:],]*@@g;s@[^:]*:"([^"]*)"@\1@g;p}')

                case "${host_country}" in
                    CN )
                        [[ "${host_city}" == 'Beijing' ]] && repo_mirror_url='http://mirrors.tuna.tsinghua.edu.cn' || repo_mirror_url='http://mirrors.neusoft.edu.cn'
                        ;;
                    * )
                        # Just for Digital Ocean VPS
                        if [[ -n "${host_org}" && "${host_org}" =~ DigitalOcean ]]; then
                            local mirror_region=${mirror_region:-'nyc2'}
                            case "${host_city}" in
                                Singapore ) mirror_region='sgp1' ;;
                                Amsterdam ) mirror_region='ams2' ;;
                                'New York'|NewYork ) mirror_region='nyc2' ;;
                                'San Francisco'|SanFrancisco ) mirror_region='sfo1' ;;
                            esac
                            repo_mirror_url="http://${mirror_region}.mirrors.digitalocean.com"
                        fi
                        ;;
                esac
            fi

            # {
            #   "ip": "128.199.72.46",
            #   "city": "Singapore",
            #   "region": "Central Singapore Community Development Council",
            #   "country": "SG",
            #   "loc": "1.2855,103.8565",
            #   "org": "AS14061 DigitalOcean, LLC"
            # }

            # - GnuPG key importing
            local gpg_keyid=${gpg_keyid:-'0xF1656F24C74CD1D8'}
            case "${codename}" in
                precise|trusty|wheezy|jessie ) gpg_keyid='0xcbcb082a1bb943db' ;;
            esac

            # Debian  sid       0xF1656F24C74CD1D8 arch=amd64,i386
            #         stretch   0xF1656F24C74CD1D8 arch=amd64,i386,ppc64el
            #         jessie    0xcbcb082a1bb943db arch=amd64,i386
            #         wheezy    0xcbcb082a1bb943db arch=amd64,i386
            # Ubuntu  zesty     0xF1656F24C74CD1D8 arch=amd64,i386
            #         yakkety   0xF1656F24C74CD1D8 arch=amd64,i386
            #         xenial    0xF1656F24C74CD1D8 arch=amd64,i386
            #         trusty    0xcbcb082a1bb943db arch=amd64,i386,ppc64el
            #         precise   0xcbcb082a1bb943db arch=amd64,i386

            local arch_list=${arch_list:-'amd64,i386'}
            case "${codename,,}" in
                stretch|trusty ) arch_list="${arch_list},ppc64el" ;;
            esac

            echo -e "# ${db_name} ${db_version} repository list\n# http://downloads.mariadb.org/mariadb/repositories/\ndeb [arch=${arch_list}] ${repo_mirror_url}/${db_name,,}/repo/${db_version}/${distro_name} ${codename} main\ndeb-src ${repo_mirror_url}/${db_name,,}/repo/${db_version}/${distro_name} ${codename} main" > "${repo_path}"
            fnBase_OperationProcedureResult "${repo_path}"

            fn_InstallationProcedureStatement 'gpg'
            fnBase_PackageManagerOperation 'install' "software-properties-common"
            # debian >=9 need dirmngr, used for GnuPG
            # [[ "${distro_name}" == 'debian' && "${version_id%%.*}" -ge 9 ]]
            fnBase_CommandExistIfCheck 'dirmngr' || fnBase_PackageManagerOperation 'install' 'dirmngr'

            apt-key adv --recv-keys --keyserver keyserver.ubuntu.com "${gpg_keyid}" &> /dev/null
            # apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xcbcb082a1bb943db/0xF1656F24C74CD1D8
            fnBase_OperationProcedureResult "${gpg_keyid}"

            # https://stackoverflow.com/questions/23358918/preconfigure-an-empty-password-for-mysql-via-debconf-set-selections
            # https://askubuntu.com/questions/79257/how-do-i-install-mysql-without-a-password-prompt
            export DEBIAN_FRONTEND=noninteractive

            # fnBase_CommandExistIfCheck 'debconf-set-selections' || fnBase_PackageManagerOperation 'install' 'debconf-utils'

            # setting root password during installation via command debconf-set-selections
            # debconf-set-selections <<< 'mariadb-server-'"${db_version}"' mysql-server/root_password password '"${mysql_pass}"''
            # debconf-set-selections <<< 'mariadb-server-'"${db_version}"' mysql-server/root_password_again password '"${mysql_pass}"''

            fn_InstallationProcedureStatement 'pack'
            local pack_name_list=${pack_name_list:-'mariadb-server'}
            fnBase_PackageManagerOperation     # just make cache
            fnBase_PackageManagerOperation 'install' "${pack_name_list}"
            fnBase_OperationProcedureResult "${pack_name_list}"

            service_name='mariadb'
            unset DEBIAN_FRONTEND
            ;;
    esac    # end case distro_name
}

fn_MySQLOperation(){
    # https://dev.mysql.com/downloads/repo/yum/
    # https://dev.mysql.com/downloads/repo/suse/
    # https://dev.mysql.com/downloads/repo/apt/

    # https://dev.mysql.com/doc/refman/5.7/en/using-systemd.html
    # /etc/my.cnf or /etc/mysql/my.cnf (RPM platforms)
    # /etc/mysql/mysql.conf.d/mysqld.cnf (Debian platforms)

    case "${distro_name}" in
        rhel|centos|fedora|sles )
            # for centos, error info may be saved in /var/log/messages
            fn_InstallationProcedureStatement 'gpg'
            # - GnuPG importing
            local repo_path=${repo_path:-'/etc/yum.repos.d/mysql-community.repo'}
            # https://dev.mysql.com/doc/refman/5.7/en/checking-gpg-signature.html
            local gpg_path=${gpg_path:-'/etc/pki/rpm-gpg/RPM-GPG-KEY-mysql'}

            if [[ "${distro_name}" == 'sles' ]]; then
                repo_path='/etc/zypp/repos.d/mysql-community.repo'
                gpg_path='/etc/RPM-GPG-KEY-mysql'
            fi

            if [[ ! -f "${gpg_path}" ]]; then
                # - method 1
                $download_method 'https://repo.mysql.com/RPM-GPG-KEY-mysql' > "${gpg_path}"
                # - method 2
                [[ -s "${gpg_path}" ]] || $download_method 'https://dev.mysql.com/doc/refman/5.7/en/checking-gpg-signature.html' | sed -r -n '/BEGIN PGP/,/END PGP/{s@[[:space:]]*<[^>]*>[[:space:]]*@@g;p}' > "${gpg_path}"
                # - method 3
                [[ -s "${gpg_path}" ]] || rpm --import 'http://dev.mysql.com/doc/refman/5.7/en/checking-gpg-signature.html'

                # - method 4
                # gpg --import mysql_pubkey.asc
                #
                # gpg --keyserver pgp.mit.edu --recv-keys 5072E1F5
                # gpg -a --export 5072E1F5 --output mysql_pubkey.asc
                # rpm --import mysql_pubkey.asc       #  import the key into your RPM configuration to validate RPM install packages
            else
                rpm --import "${gpg_path}" &> /dev/null
            fi
            fnBase_OperationProcedureResult "${gpg_path}"

            # - Repo generation
            fn_InstallationProcedureStatement 'repo'
            version_id=${version_id%%.*}
            local releasever_basearch=${releasever_basearch:-}

            case "${distro_name}" in
                rhel|centos ) releasever_basearch="el/${version_id}" ;;
                fedora ) releasever_basearch="fc/\$releasever" ;;
                sles ) releasever_basearch="sles/${version_id}" ;;
            esac

            local extra_paras=${extra_paras:-}
            [[ "${distro_name}" == 'sles' ]] && extra_paras="autorefresh=0\ntype=rpm-md\n"

            echo -e "[mysql-connectors-community]\nname=MySQL Connectors Community\nbaseurl=http://repo.mysql.com/yum/mysql-connectors-community/${releasever_basearch}/\$basearch/\nenabled=1\n${extra_paras}gpgcheck=1\ngpgkey=file://${gpg_path}\n" > "${repo_path}"

            echo -e "[mysql-tools-community]\nname=MySQL Tools Community\nbaseurl=http://repo.mysql.com/yum/mysql-tools-community/${releasever_basearch}/\$basearch/\nenabled=1\n${extra_paras}gpgcheck=1\ngpgkey=file://${gpg_path}\n" >> "${repo_path}"

            # MySQL Community Version
            echo -e "[mysql${db_version//.}-community]\nname=MySQL ${db_version} Community Server\nbaseurl=http://repo.mysql.com/yum/mysql-${db_version}-community/${releasever_basearch}/\$basearch/\nenabled=1\n${extra_paras}gpgcheck=1\ngpgkey=file://${gpg_path}\n"  >> "${repo_path}"
            fnBase_OperationProcedureResult "${repo_path}"

            fn_InstallationProcedureStatement 'pack'
            local pack_name_list=${pack_name_list:-'mysql-community-server'}
            fnBase_PackageManagerOperation     # just make cache
            fnBase_PackageManagerOperation 'install' "${pack_name_list}"
            fnBase_OperationProcedureResult "${pack_name_list}"

            service_name='mysqld'
            [[ "${distro_name}" == 'sles' ]] && service_name='mysql'
            ;;
        debian|ubuntu )
            # Method 1 - install gpg & sources file via official package, appear prompt, not recommend
            # local mysql_official_site=${mysql_official_site:-'https://dev.mysql.com'}
            # local apt_config_url=${apt_config_url:-}
            # apt_config_url=$($download_tool_origin $($download_tool_origin "${mysql_official_site}/downloads/repo/apt/" | sed -r -n '/button03/{s@.*href="([^"]*)".*@'"${mysql_official_site}"'\1@g;p}') | sed -r -n '/No thanks/{s@.*href="(.*)".*@'"${mysql_official_site}"'\1@g;p}')
            #
            # # curl -fsL $(curl -fsL https://dev.mysql.com/downloads/repo/apt/ | sed -r -n '/button03/{s@.*href="([^"]*)".*@https://dev.mysql.com\1@g;p}') | sed -r -n '/No thanks/{s@.*href="(.*)".*@https://dev.mysql.com\1@g;p}'
            # # https://dev.mysql.com/get/mysql-apt-config_0.8.7-1_all.deb
            #
            # if [[ -n "${apt_config_url}" ]]; then
            #     local apt_config_pack_name=${apt_config_pack_name:-}
            #     apt_config_pack_name=${apt_config_url##*/}
            #     local apt_config_pack_save_path=${apt_config_pack_save_path:-"/tmp/${apt_config_pack_name}"}
            #     $download_tool_origin "${apt_config_url}" > "${apt_config_pack_save_path}"
            #
            #     [[ -s "${apt_config_pack_save_path}" ]] && dpkg -i "${apt_config_pack_save_path}"
            # fi

            fn_InstallationProcedureStatement 'repo'
            # Method 1 - Manually operation
            local repo_path=${repo_path:-'/etc/apt/sources.list.d/mysql.list'}

            # deb http://repo.mysql.com/apt/{debian|ubuntu}/ {jessie|wheezy|trusty|utopic|vivid} {mysql-5.6|mysql-5.7|workbench-6.2|utilities-1.4|connector-python-2.0}

            echo -e "deb http://repo.mysql.com/apt/${distro_name}/ ${codename} mysql-tools\ndeb http://repo.mysql.com/apt/${distro_name}/ ${codename} mysql-${db_version}\ndeb-src http://repo.mysql.com/apt/${distro_name}/ ${codename} mysql-${db_version}" > "${repo_path}"
            fnBase_OperationProcedureResult "${repo_path}"

            # Use command 'dpkg-reconfigure mysql-apt-config' as root for modifications.
            # deb http://repo.mysql.com/apt/debian/ stretch mysql-apt-config

            # - GnuPG importing
            fn_InstallationProcedureStatement 'gpg'
            # apt-key adv --keyserver pgp.mit.edu --recv-keys 5072E1F5
            # apt-key adv --recv-keys --keyserver keyserver.ubuntu.com 5072E1F5

            local gpg_path=${gpg_path:-}
            gpg_path=$(mktemp -t Temp_XXXXXX.txt)       # mysql_gpg.asc
            # - method 1
            $download_method 'https://repo.mysql.com/RPM-GPG-KEY-mysql' > "${gpg_path}"
            # - method 2
            [[ -s "${gpg_path}" ]] || $download_method 'https://dev.mysql.com/doc/refman/5.7/en/checking-gpg-signature.html' | sed -r -n '/BEGIN PGP/,/END PGP/{s@[[:space:]]*<[^>]*>[[:space:]]*@@g;p}' > "${gpg_path}"

            # The following signatures were invalid: EXPKEYSIG 8C718D3B5072E1F5 MySQL Release Engineering <mysql-build@oss.oracle.com>

            # /etc/apt/trusted.gpg
            # --------------------
            # pub   dsa1024 2003-02-03 [SCA] [expired: 2019-02-17]
            #       A4A9 4068 76FC BD3C 4567  70C8 8C71 8D3B 5072 E1F5
            # uid           [ expired] MySQL Release Engineering <mysql-build@oss.oracle.com>

            # new
            # pub   dsa1024 2003-02-03 [SCA] [expires: 2022-02-16]
            #       A4A9406876FCBD3C456770C88C718D3B5072E1F5
            # uid           [ unknown] MySQL Release Engineering <mysql-build@oss.oracle.com>

            apt-key add "${gpg_path}" &> /dev/null
            # apt-key list |& grep 'MySQL Release Engineering'
            [[ -f "${gpg_path}" ]] && rm -f "${gpg_path}"
            fnBase_OperationProcedureResult 'RPM-GPG-KEY-mysql'

            export DEBIAN_FRONTEND=noninteractive

            # fnBase_CommandExistIfCheck 'debconf-set-selections' || fnBase_PackageManagerOperation 'install' 'debconf-utils'
            # debconf-set-selections <<< 'mysql-server mysql-server/root_password password '"${mysql_pass}"''
            # debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password '"${mysql_pass}"''

            fn_InstallationProcedureStatement 'pack'
            local pack_name_list=${pack_name_list:-'mysql-server'}
            fnBase_PackageManagerOperation     # just make cache
            fnBase_PackageManagerOperation 'install' "${pack_name_list}"
            # https://dev.mysql.com/doc/mysql-apt-repo-quick-guide/en/#updating-apt-repo-client-lib
            # Special Notes on Upgrading the Shared Client Libraries
            fnBase_PackageManagerOperation 'install' "libmysqlclient20"
            fnBase_OperationProcedureResult "${pack_name_list}"

            service_name='mysql'
            unset DEBIAN_FRONTEND
            ;;
    esac    # end case distro_name
}

fn_PerconaOperation(){
    # https://www.percona.com/doc/percona-server/LATEST/installation/yum_repo.html
    # https://www.percona.com/doc/percona-server/LATEST/installation/apt_repo.html
    # https://www.percona.com/blog/2016/10/13/new-signing-key-for-percona-debian-and-ubuntu-packages/

    case "${distro_name}" in
        rhel|centos )
            fn_InstallationProcedureStatement 'gpg'
            local repo_path=${repo_path:-'/etc/yum.repos.d/percona-release.repo'}
            # https://dev.mysql.com/doc/refman/5.7/en/checking-gpg-signature.html
            local gpg_path=${gpg_path:-'/etc/pki/rpm-gpg/RPM-GPG-KEY-Percona'}

            # - GnuPG importing
            [[ -s "${gpg_path}" ]] || $download_method 'https://www.percona.com/downloads/RPM-GPG-KEY-percona' > "${gpg_path}"
            fnBase_OperationProcedureResult 'RPM-GPG-KEY-Percona'

            # - repo generation
            fn_InstallationProcedureStatement 'repo'
            echo -e "[percona-release-\$basearch]\nname = Percona-Release YUM repository - \$basearch\nbaseurl = http://repo.percona.com/release/\$releasever/RPMS/\$basearch\nenabled = 1\ngpgcheck = 1\ngpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-Percona\n" > "${repo_path}"

            echo -e "[percona-release-noarch]\nname = Percona-Release YUM repository - noarch\nbaseurl = http://repo.percona.com/release/\$releasever/RPMS/noarch\nenabled = 1\ngpgcheck = 1\ngpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-Percona" >> "${repo_path}"

            # [percona-release-$basearch]
            # name = Percona-Release YUM repository - $basearch
            # baseurl = http://repo.percona.com/release/$releasever/RPMS/$basearch
            # enabled = 1
            # gpgcheck = 1
            # gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-Percona
            #
            # [percona-release-noarch]
            # name = Percona-Release YUM repository - noarch
            # baseurl = http://repo.percona.com/release/$releasever/RPMS/noarch
            # enabled = 1
            # gpgcheck = 1
            # gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-Percona
            fnBase_OperationProcedureResult "${repo_path}"

            # yum install Percona-Server-server-{57,56,55}
            fn_InstallationProcedureStatement 'pack'
            local pack_name_list=${pack_name_list:-"Percona-Server-server-${db_version//.}"}
            fnBase_PackageManagerOperation     # just make cache
            fnBase_PackageManagerOperation 'install' "${pack_name_list}"
            fnBase_OperationProcedureResult "${pack_name_list}"

            service_name='mysqld'
            ;;
        debian|ubuntu )
            fn_InstallationProcedureStatement 'repo'
            # Method 1 - Via official .deb package
            # # https://repo.percona.com/apt/percona-release_0.1-4.stretch.deb
            # local apt_repo_url=${apt_repo_url:-'https://repo.percona.com/apt/'}
            # local repo_pack_name=${repo_pack_name:-"percona-release_0.1-4.$(lsb_release -sc)_all.deb"}
            # repo_pack_name=$($download_method "${apt_repo_url}" | awk 'match($0,/'"${codename}"'/){a=gensub(/.*href="([^"]*)".*/,"\\1","g",$0);}END{print a}')
            # local repo_pack_save_path=${repo_pack_save_path:-"/tmp/${repo_pack_name}"}
            #
            # $download_method "${apt_repo_url}${repo_pack_name}" > "${repo_pack_save_path}"
            # dpkg -i "${repo_pack_save_path}"
            # [[ -f "${repo_pack_save_path}" ]] && rm -f "${repo_pack_save_path}"


            # Method 2 - Manually setting
            local repo_path=${repo_path:-'/etc/apt/sources.list.d/percona-release.list'}

            # deb http://repo.percona.com/apt stretch {main,testing,experimental}
            # deb-src http://repo.percona.com/apt stretch {main,testing,experimental}
            echo -e "# Percona releases, stable\ndeb http://repo.percona.com/apt ${codename} main\ndeb-src http://repo.percona.com/apt ${codename} main\n" > "${repo_path}"
            fnBase_OperationProcedureResult "${repo_path}"

            # - GnuPG importing
            fn_InstallationProcedureStatement 'gpg'
            fnBase_CommandExistIfCheck 'dirmngr' || fnBase_PackageManagerOperation 'install' 'dirmngr'
            # https://www.percona.com/blog/2016/10/13/new-signing-key-for-percona-debian-and-ubuntu-packages/
            # old gpg key
            apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8507EFA5 &> /dev/null
            # new gpg key
            apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 9334A25F8507EFA5 &> /dev/null
            fnBase_OperationProcedureResult '8507EFA5 / 9334A25F8507EFA5'

            export DEBIAN_FRONTEND=noninteractive

            fn_InstallationProcedureStatement 'pack'
            local pack_name_list=${pack_name_list:-"percona-server-server-${db_version} percona-server-common-${db_version} libdbd-mysql-perl"}
            fnBase_PackageManagerOperation     # just make cache
            fnBase_PackageManagerOperation 'install' "${pack_name_list}"
            fnBase_OperationProcedureResult "percona-server-server"

            service_name='mysql'
            unset DEBIAN_FRONTEND
            ;;
    esac    # end case distro_name
}

fn_DBInstallationOperation(){
    fnBase_OperationPhaseStatement 'Initialization Installation'
    fn_"${db_name}"Operation
    fn_DBServiceStatusOperation "${service_name}" 'start'
    fn_DBServiceStatusOperation "${service_name}" 'restart'
}

############# 2-3. MySQL Post-installation Configuration #############
fn_RootPasswordConfiguration(){
    local l_retry=${1:-0}
    [[ "${l_retry}" -eq 0 ]] && fnBase_OperationProcedureStatement 'New root password'
    # https://dev.mysql.com/doc/refman/5.7/en/alter-user.html#alter-user-authentication
    # -- MySQL 5.7
    # sudo grep 'temporary password' /var/log/mysqld.log
    #                                /var/log/mysql/mysqld.log
    # mysql -uroot -p
    # ALTER USER 'root'@'localhost' IDENTIFIED BY 'MyNewPass4!';
    #
    # -- MySQL 5.6
    # mysql_secure_installation
    local l_login_user_my_cnf=${l_login_user_my_cnf:-"${login_user_cnf_path}"}
    [[ -f "${l_login_user_my_cnf}" ]] && rm -f "${l_login_user_my_cnf}"

    # generate strong random password for mysql user root@localhost
    root_password_new=$(fnBase_RandomPasswordGeneration)

    case "${db_name}" in
        MariaDB )
            mysql -e "set password for 'root'@'localhost' = PASSWORD('${root_password_new}');" 2> /dev/null
            ;;
        MySQL|Percona )
            case "${db_version}" in
                5.5|5.6 )
                    mysql -e "set password for 'root'@'localhost' = PASSWORD('${root_password_new}');" 2> /dev/null
                    ;;
                * )
                    # 5.7 +
                    case "${distro_name}" in
                        debian|ubuntu )
                            mysql -e "alter user 'root'@'localhost' identified with mysql_native_password by '${root_password_new}';" 2> /dev/null
                            ;;
                        * )
                            # https://dev.mysql.com/doc/mysql-sles-repo-quick-guide/en/
                            local error_log_file=${error_log_file:-'/var/log/mysqld.log'}
                            [[ -s '/var/log/mysql/mysqld.log' ]] && error_log_file='/var/log/mysql/mysqld.log'

                            local tempRootPassword=${tempRootPassword:-}
                            tempRootPassword=$(awk '$0~/temporary password/{a=$NF}END{print a}' "${error_log_file}")
                            # Please use --connect-expired-password option or invoke mysql in interactive mode.
                            mysql -uroot -p"${tempRootPassword}" --connect-expired-password -e "alter user 'root'@'localhost' identified with mysql_native_password by '${root_password_new}';" 2> /dev/null
                            ;;
                    esac    # end case distro_name
                    ;;
            esac    # end case db_version
            ;;
    esac    # end case db_name

    # https://dev.mysql.com/doc/refman/5.7/en/mysql-commands.html

    # ~/.my.cnf
    # prompt=(\\u@\\h) [\\d]>\\_
    # prompt=MariaDB/Percona/MySQL [\\d]>\\_

    db_newly_installed_version=$(mysql -uroot -p"${root_password_new}" -Bse "select version();" 2> /dev/null | sed -r -n 's@^([^-]*).*@\1@g;p')

    if [[ -n "${db_newly_installed_version}" ]]; then
        # https://dev.mysql.com/doc/refman/5.7/en/password-security-user.html
        if [[ -n "${l_login_user_my_cnf}" ]]; then
            # echo  -e "[client]\nuser=root\npassword=${root_password_new}\n\n[mysql]\nprompt=(\\u@\\h) [\\d]>\\_" > "${login_user_home}/.my.cnf"
            echo  -e "[client]\nuser=root\npassword=\"${root_password_new}\"\n\n[mysql]\nprompt=${db_name%%-*} [\\d]>\\_" > "${l_login_user_my_cnf}"
            chown "${login_user}" "${l_login_user_my_cnf}"
            chmod 400 "${l_login_user_my_cnf}"
            fnBase_OperationProcedureResult "${l_login_user_my_cnf}"
        fi
    else
        fn_RootPasswordConfiguration '1'
        # fnBase_OperationProcedureResult "${l_login_user_my_cnf}" 1
    fi
}

# mysql_secure_installation
fn_MySQLSecureInstallationConfiguration(){
    # https://dev.mysql.com/doc/refman/5.7/en/default-privileges.html
    local l_mysql_command="${l_mysql_command:-"${mysql_custom_command}"}"

    # extract from /usr/bin/mysql_secure_installation
    # - set_root_password
    # UPDATE mysql.user SET Password=PASSWORD('$esc_pass') WHERE User='root';

    if [[ -n "${l_mysql_command}" ]]; then
        fnBase_OperationProcedureStatement 'mysql_secure_installation'
        # - remove_anonymous_users
        ${l_mysql_command} -e "DELETE FROM mysql.user WHERE User='';"
        # - remove_remote_root
        ${l_mysql_command} -e "DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');"
        # - remove_test_database
        ${l_mysql_command} -e "DROP DATABASE IF EXISTS test;"
        # - reload_privilege_tables
        ${l_mysql_command} -e "FLUSH PRIVILEGES;"

        fnBase_OperationProcedureResult
    fi
}

# ~/.mysql_history ==> /dev/null
fn_UserHistoryLogPathConfiguration(){
    local l_mysql_command="${l_mysql_command:-"${mysql_custom_command}"}"

    if [[ -n "${l_mysql_command}" ]]; then
        fnBase_OperationProcedureStatement 'User Logging History'
        # https://dev.mysql.com/doc/refman/5.7/en/mysql-logging.html
        local login_user_mysql_history=${login_user_mysql_history:-"${login_user_home}/.mysql_history"}
        [[ -f "${login_user_mysql_history}" ]] && rm -f "${login_user_mysql_history}"
        ln -fs /dev/null "${login_user_mysql_history}"

        fnBase_OperationProcedureResult "${login_user_mysql_history} ==> /dev/null"
    fi
}

# mysql_tzinfo_to_sql
fn_TimeZoneInfoImport(){
    # https://dev.mysql.com/downloads/timezones.html
    # https://mariadb.com/kb/en/library/mysql_tzinfo_to_sql/
    # https://dev.mysql.com/doc/refman/5.7/en/time-zone-support.html
    # https://dev.mysql.com/doc/refman/5.7/en/mysql-tzinfo-to-sql.html
    local l_mysql_command="${l_mysql_command:-"${mysql_custom_command}"}"

    if [[ -n "${l_mysql_command}" && -d '/usr/share/zoneinfo' ]]; then
        if fnBase_CommandExistIfCheck 'mysql_tzinfo_to_sql'; then
            fnBase_OperationProcedureStatement 'Import time zone'
            mysql_tzinfo_to_sql /usr/share/zoneinfo 2> /dev/null | ${l_mysql_command} mysql
            fnBase_OperationProcedureResult 'mysql_tzinfo_to_sql'
        fi
    fi
}

# UDF (User Defined Function), just for Percona
fn_UserDefinedFunctionForPercona(){
    # Percona Server is distributed with several useful UDF (User Defined Function) from Percona Toolkit.
    local l_mysql_command="${l_mysql_command:-"${mysql_custom_command}"}"

    if [[ "${db_name}" == 'Percona' && -n "${l_mysql_command}" ]]; then
        fnBase_OperationProcedureStatement 'User Defined Function'
        $l_mysql_command -e "CREATE FUNCTION fnv1a_64 RETURNS INTEGER SONAME 'libfnv1a_udf.so'"
        $l_mysql_command -e "CREATE FUNCTION fnv_64 RETURNS INTEGER SONAME 'libfnv_udf.so'"
        $l_mysql_command -e "CREATE FUNCTION murmur_hash RETURNS INTEGER SONAME 'libmurmur_udf.so'"
        fnBase_OperationProcedureResult 'For Percona'
    fi
}


########## 2-4. MySQL Config File Configuration ##########
fn_NewDatadirOperation(){
    if [[ -n "${data_dir}" && "${data_dir}" != "${data_dir_default}" ]]; then
        fnBase_OperationPhaseStatement 'Data dir operation'

        if [[ -d "${data_dir}" ]]; then
            fnBase_OperationProcedureStatement 'Check if is checked'
            local data_dir_temp=${data_dir_temp:-"${data_dir}.$(date +'%s').old"}
            mv "${data_dir}" "${data_dir_temp}"
            fnBase_OperationProcedureResult "${data_dir} ==> ${data_dir_temp}"
            # echo -e "${c_red}Attention: ${c_normal}Existed dir ${c_blue}${data_dir}${c_normal} has been rename to ${c_blue}${data_dir_temp}${c_normal}."
        fi

        fnBase_OperationProcedureStatement 'Relocate data dir'
        [[ -d "${data_dir}" ]] && rm -rf "${data_dir}"
        mkdir -p "${data_dir}"
        chmod --reference=${data_dir_default} "${data_dir}"
        cp -R ${data_dir_default}/. "${data_dir}"
        chown -R mysql:mysql "${data_dir}"
        fnBase_OperationProcedureResult "${data_dir_default} ==> ${data_dir}"
        # fn_InstallationProcedureStatement "Relocating from ${data_dir_default} to ${data_dir}"
    fi
}

fn_ConfigConfiguration(){
    fnBase_OperationPhaseStatement 'Config File Operation'

    # 1 - detect default config file path - conf_path
    fnBase_OperationProcedureStatement 'Config path detection'
    case "${db_name,,}" in
        mysql )
            # https://dev.mysql.com/doc/refman/5.7/en/cannot-create.html
            case "${distro_name}" in
                rhel|centos|fedora|sles )
                    conf_path="${conf_path_default}"
                    # Don't change socket path, or it will prompt   ERROR 2002 (HY000): Can't connect to local MySQL server through socket '/var/lib/mysql/mysql.sock' (2)
                    ;;
                debian|ubuntu ) conf_path='/etc/mysql/mysql.conf.d/mysqld.cnf' ;;
            esac
            ;;
        percona )
            case "${distro_name}" in
                rhel|centos )
                    conf_path="${conf_path_default}"
                    # $(expr "${db_version}" \>= 5.7) -eq 1
                    [[ $(fnBase_VersionNumberComparasion "${db_version}" '5.7' '<') -eq 1 ]] || conf_path='/etc/percona-server.conf.d/mysqld.cnf'
                    ;;
                debian|ubuntu )
                    conf_path='/etc/mysql/my.cnf'
                    # $(expr "${db_version}" \>= 5.7) -eq 1
                    [[ $(fnBase_VersionNumberComparasion "${db_version}" '5.7' '<') -eq 1 ]] || conf_path='/etc/mysql/percona-server.conf.d/mysqld.cnf'
                    ;;
            esac
            ;;
        mariadb )
            case "${distro_name}" in
                rhel|centos|fedora|opensuse ) conf_path="${conf_path_default}" ;;
                debian|ubuntu ) conf_path='/etc/mysql/my.cnf' ;;
            esac
            ;;
    esac

    if [[ -f "${conf_path}" ]]; then
        fnBase_OperationProcedureResult "${conf_path}"
    else
        fnBase_OperationProcedureResult "${conf_path}" 1
        # fnBase_ExitStatement "${c_red}Sorry${c_normal}: fail to locate MySQL config file ${c_yellow}${conf_path}${c_normal}!"
    fi

    # 2 - config file configuration
    fnBase_OperationProcedureStatement 'Directives configuration'

    # 2.1 - backup origin conf file
    local l_origin_conf_bak_path
    l_origin_conf_bak_path="${conf_path}${bak_suffix}"
    [[ -f "${l_origin_conf_bak_path}" ]] || cp -fp "${conf_path}" "${l_origin_conf_bak_path}"
    # 2.2 - download my.cnf template
    $download_method "${mysqld_cnf_url}" > "${conf_path}"
    # 2.3 - replace parater mysqlvariant --> ${db_name,,}
    sed -r -i 's@mysqlvariant@'"${db_name,,}"'@g;' "${conf_path}"
    # 2.4 - log dir configuration, name rule setting in my.cnf
    mysql_log_dir="/var/log/${db_name,,}"
    if [[ ! -d "${mysql_log_dir}" ]]; then
        mkdir -p "${mysql_log_dir}"
        chown -R mysql:mysql "${mysql_log_dir}"
    fi

    # 2.5 -  replace original val to new conf
    # 2.5.1 - pid_file
    local pid_file_val_origin
    pid_file_val_origin=$(sed -r -n '/\[mysqld\]/,${/^pid_file[[:space:]]*=/{s@^([^=]+=[[:space:]]*)([^[:space:]]+)(.*)$@\2@g;p}}' "${l_origin_conf_bak_path}")

    if [[ -n "${pid_file_val_origin}" ]]; then
        fn_ConfDirectiveConfiguration "${conf_path}" 'pid_file' "${pid_file_val_origin}"
        [[ "${pid_file_val_origin}" =~ ^/var/run/ ]] && mysql_run_dir=$(dirname "${pid_file_val_origin}")
    fi

    # 2.5.2 - socket
    local socket_val_origin
    socket_val_origin=$(sed -r -n '/\[mysqld\]/,${/^socket[[:space:]]*=/{s@^([^=]+=[[:space:]]*)([^[:space:]]+)(.*)$@\2@g;p}}' "${l_origin_conf_bak_path}")
    [[ -n "${socket_val_origin}" ]] && fn_ConfDirectiveConfiguration "${conf_path}" 'socket' "${socket_val_origin}"

    # 2.5.3 - datadir
    local datadir_val_origin
    datadir_val_origin=$(sed -r -n '/\[mysqld\]/,${/^datadir[[:space:]]*=/{s@^([^=]+=[[:space:]]*)([^[:space:]]+)(.*)$@\2@g;p}}' "${l_origin_conf_bak_path}")
    if [[ -n "${datadir_val_origin}" ]]; then
        fn_ConfDirectiveConfiguration "${conf_path}" 'datadir' "${datadir_val_origin}"
    fi

    # 2.5.4 - bind_address
    case "${bind_ip,,}" in
        l|local|'127.0.0.1' ) bind_ip='127.0.0.1' ;;
        a|all|'0.0.0.0' ) bind_ip='0.0.0.0' ;;
        * ) [[ "${bind_ip}" =~ ^([0-9]{1,3}.){3}[0-9]{1,3}(/[0-9]{1,3}){0,1}$ ]] || bind_ip='127.0.0.1' ;;
    esac

    [[ "${bind_ip}" != '127.0.0.1' ]] && fn_ConfDirectiveConfiguration "${conf_path}" 'bind_address' "${bind_ip}"

    # config file configuration end
    fnBase_OperationProcedureResult

    # 3 - config file optimization
    fnBase_OperationProcedureStatement 'Directives optimization'
    # innodb_log_file_size = 256M
    # Bigger means more write throughput but longer recovery time
    # allow 1~2h worth of writes to be buffered in transaction logs, log sequence number
    # > pager grep seq
    # > show engine innodb status\G sleep(60); show engine innodb status\G
    # > nopager
    # > select (114172321602-114172162446)*60/1024/1024;

    # 3.1 - server_id    not change default val right now
    # fn_ConfDirectiveConfiguration "${conf_path}" 'server_id' 'delete'
    fn_ConfDirectiveConfiguration "${conf_path}" 'server_id' "${ip_local##*.}"

    # 3.2 master/slave mode
    case "${slave_mode}" in
        1 )
            sed -r -i '/Relay Log/,/Relay Log End/{/=/{s@^#?[[:space:]]*@@g;}}' "${conf_path}"
            sed -r -i '/Binary Log/,/Binary Log End/{/=/{s@^#?[[:space:]]*@# @g;}}' "${conf_path}"
            sed -r -i '/replicate-ignore-db/{s@^#?[[:space:]]*@@g;}' "${conf_path}"
            ;;
        0|* )
            sed -r -i '/Binary Log/,/Binary Log End/{/=/{s@^#?[[:space:]]*@@g;}}' "${conf_path}"
            sed -r -i '/Relay Log/,/Relay Log End/{/=/{s@^#?[[:space:]]*@# @g;}}' "${conf_path}"
            sed -r -i '/replicate-ignore-db/{s@^#?[[:space:]]*@# @g;}' "${conf_path}"
            ;;
    esac


    # - ssl https://dev.mysql.com/doc/mysql-secure-deployment-guide/5.7/en/secure-deployment-secure-connections.html
    # https://dev.mysql.com/doc/refman/5.7/en/creating-ssl-files-using-openssl.html
    # ssl_ca=/var/lib/mysql/ca.pem
    # ssl_cert=/var/lib/mysql/client-cert.pem
    # ssl_key=/var/lib/mysql/client-key.pem
    # tls_version=TLSv1.1,TLSv1.2


    # 3.3 - For Specific Variant
    # 3.3.1 - Just for MariaDB
    if [[ "${db_name}" == 'MariaDB' ]]; then
        fn_ConfDirectiveConfiguration "${conf_path}" 'log_timestamps' 'delete'
        fn_ConfDirectiveConfiguration "${conf_path}" 'innodb_page_cleaners' 'delete'
        fn_ConfDirectiveConfiguration "${conf_path}" 'innodb_adaptive_hash_index_parts' 'delete'

        case "${distro_name}" in
            rhel|centos|fedora ) fn_ConfDirectiveConfiguration "${conf_path}" 'pid_file' 'delete' ;;
        esac

        sed -r -i '/Security Enhancement Start/,/Security Enhancement End/d' "${conf_path}" 2> /dev/null
    fi

    # 3.3.2 - Just for Percona
    if [[ "${db_name}" == 'Percona' ]]; then
        sed -r -i '/Percona Server only/{s@^#?[[:space:]]*@@g;}' "${conf_path}"
    else
        sed -r -i '/Percona Server only/d' "${conf_path}"
    fi

    # 3.3.3 - For MySQL/Percona
    case "${db_name}" in
        MySQL|Percona )
            # MySQL >= 8.0.1, not use NO_AUTO_CREATE_USER
            # https://dev.mysql.com/doc/refman/5.7/en/group-by-handling.html
            # sql_mode: ONLY_FULL_GROUP_BY will result in "ERROR 1055 (42000): 'xx.xx.xx' isn't in GROUP BY" while use clause group by
            # sql_mode=ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION
            if [[ $(fnBase_VersionNumberComparasion "${db_newly_installed_version}" '5.7' '<') -eq 1 ]]; then
                sed -r -i '/sql_mode/{s@ONLY_FULL_GROUP_BY,@@g;}' "${conf_path}"
            elif [[ $(fnBase_VersionNumberComparasion "${db_newly_installed_version}" '8.0.1' '<') -eq 0 ]]; then
                sed -r -i '/sql_mode/{s@NO_AUTO_CREATE_USER,@@g;}' "${conf_path}"
            fi

            # log_warnings = 2    # For MySQL 5.7.2 deprecated
            # log_error_verbosity = 3    # For MySQL 5.7.2 or newer
            if [[ $(fnBase_VersionNumberComparasion "${db_newly_installed_version}" '5.7.2' '<') -eq 1 ]]; then
                fn_ConfDirectiveConfiguration "${conf_path}" 'log_error_verbosity' 'delete'
                fn_ConfDirectiveConfiguration "${conf_path}" 'log_warnings' 'uncomment'
            else
                fn_ConfDirectiveConfiguration "${conf_path}" 'log_warnings' 'delete'
                fn_ConfDirectiveConfiguration "${conf_path}" 'log_error_verbosity' 'uncomment'
            fi

            # query_cache_* deprecated as of MySQL 5.7.20 and removed in MySQL 8.0
            if [[ $(fnBase_VersionNumberComparasion "${db_newly_installed_version}" '5.7.20' '<') -eq 0 ]]; then
                sed -r -i '/Query Cache Start/,/Query Cache End/d' "${conf_path}"
            fi

            local l_target_directives=${l_target_directives:-}
            l_target_directives=$(sed -r -n '/^[^=]+=[^#]*#[[:space:]]*For MySQL/{s@^#*[[:space:]]*([^=]+)=([[:space:]]*[^[:space:]]*)[[:space:]]*#[[:space:]]*For MySQL[[:space:]]*([[:digit:].]+).*@\1|\3@g;s@[[:space:]]*(|)[[:space:]]*@\1@g;p}' "${conf_path}")
            if [[ -n "${l_target_directives}" ]]; then
                echo "${l_target_directives}" | while IFS="|" read -r item_name start_ver; do
                    if [[ $(fnBase_VersionNumberComparasion "${db_newly_installed_version}" "${start_ver}" '<') -eq 1 ]]; then
                        fn_ConfDirectiveConfiguration "${conf_path}" "${item_name}" 'delete'
                    else
                        fn_ConfDirectiveConfiguration "${conf_path}" "${item_name}" 'uncomment'
                    fi
                done
            fi

            ;;
    esac

    fnBase_OperationProcedureResult
}

fn_SecurityUtilityRulesConfiguration(){
    fnBase_OperationPhaseStatement 'Security Utilities Configuration'

    # 1 - Firewall
    if [[ "${enable_firewall}" -eq 1 ]]; then
        fnBase_OperationProcedureStatement 'Firewall configuration'
        # $download_method "${firewall_configuration_script}" | bash -s -- -p "${mysql_port}" -H "${remote_host_ip}" -s
        $download_method "${firewall_configuration_script}" | bash -s -- -p "${mysql_port}" -s

        local l_firewall_type=''
        case "${pack_manager}" in
            apt-get ) l_firewall_type='ufw' ;;
            zypper ) l_firewall_type='SuSEfirewall2' ;;
            dnf|yum ) [[ $("${pack_manager}" info firewalld 2>&1 | sed -r -n '/^Name[[:space:]]+:/{s@^[^:]+:[[:space:]]*(.*)$@\1@g;p;q}') == 'firewalld' ]] && l_firewall_type='firewalld' || l_firewall_type='iptables' ;;
        esac

        fnBase_OperationProcedureResult "${l_firewall_type}"
    fi

    # 2 - SElinux
    # https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/selinux_users_and_administrators_guide/chap-managing_confined_services-mariadb

    # Be careful of SELinux
    # [Warning] Can't create test file /data/mysql/centos.lower-test
    # https://phe1129.wordpress.com/2012/04/02/change-mysql-data-folder-on-selinux/
    # https://dba.stackexchange.com/questions/80232/mysql-cant-create-test-file-error-on-centos
    if fnBase_CommandExistIfCheck 'getenforce'; then
        fnBase_OperationProcedureStatement 'SELinux configuration'
        # semanage port -a -t mysqld_port_t -p tcp 3307
        fnBase_SELinuxSemanageOperation 'mysqld_port_t' "${mysql_port_default}" 'port' 'add' 'tcp'
        [[ -n "${mysql_port}" && "${mysql_port}" != "${mysql_port_default}" ]] && fnBase_SELinuxSemanageOperation 'mysqld_port_t' "${mysql_port}" 'port' 'add' 'tcp'

        # semanage fcontext -a -t mysqld_db_t "/var/lib/mysql(/.*)?"
        fnBase_SELinuxSemanageOperation 'mysqld_db_t' "${data_dir}" 'fcontext' 'add'
        [[ -d "${data_dir_default}" ]] && fnBase_SELinuxSemanageOperation 'mysqld_db_t' "${data_dir_default}" 'fcontext' 'add'

        # mysqld_etc_t /etc/my.cnf, /etc/mysql/
        [[ -f "${conf_path_default}" ]] && fnBase_SELinuxSemanageOperation 'mysqld_etc_t' "${conf_path_default}" 'fcontext' 'add'
        if [[ -f "${conf_path}" && "${conf_path}" != "${conf_path_default}" ]]; then
            local l_conf_dir
            l_conf_dir=$(dirname "${conf_path}")
            [[ "${l_conf_dir}" == '/etc' ]] || fnBase_SELinuxSemanageOperation 'mysqld_etc_t' "${l_conf_dir}" 'fcontext' 'add'
        fi

        # mysqld_exec_t /usr/libexec/mysqld, /usr/sbin/mysqld

        # mysqld_unit_file_t /usr/lib/systemd/system/mysqld.service

        # semanage fcontext -a -t mysqld_log_t "/data/log(/.*)?"
        [[ -d "${mysql_log_dir}" ]] && fnBase_SELinuxSemanageOperation 'mysqld_log_t' "${mysql_log_dir}" 'fcontext' 'add'

        # semanage fcontext -a -t mysqld_var_run_t "var/run/mysqld(/.*)?"
        fnBase_SELinuxSemanageOperation 'mysqld_var_run_t' "${mysql_run_dir}" 'fcontext' 'add'
        fnBase_OperationProcedureResult
    fi


    # 3 - Apparmor
    if [[ "${db_name,,}" == 'mysql' ]]; then
        fnBase_OperationProcedureStatement 'Apparmor configuration'

        case "${distro_name}" in
            # rhel|centos|fedora|sles ) ;;
            debian|ubuntu )
                # https://dba.stackexchange.com/questions/106085/cant-create-file-var-lib-mysql-user-lower-test
                local apparmor_mysqld_path=${apparmor_mysqld_path:-'/etc/apparmor.d/usr.sbin.mysqld'}
                [[ -s "${apparmor_mysqld_path}" ]] && sed -r -i '/Allow data dir access/,/^$/{s@'"${data_dir_default}/"'@'"${data_dir}/"'@g}' "${apparmor_mysqld_path}"
                ;;
        esac
        fnBase_OperationProcedureResult
    fi
}

fn_PostInstallationOperation(){
    fnBase_OperationPhaseStatement 'MySQL Secure Configuration'
    fn_RootPasswordConfiguration
    fn_MySQLSecureInstallationConfiguration

    fn_UserHistoryLogPathConfiguration
    fn_TimeZoneInfoImport
    fn_UserDefinedFunctionForPercona

    fn_DBServiceStatusOperation "${service_name}" 'stop'
    fn_NewDatadirOperation
    fn_ConfigConfiguration

    fn_SecurityUtilityRulesConfiguration
    fn_ConfDirectiveConfiguration "${conf_path}" 'datadir' "${data_dir}"
    fn_ConfDirectiveConfiguration "${conf_path}" 'port' "${mysql_port}"
    fn_DBServiceStatusOperation "${service_name}" 'start'

    # empty general log
    local l_general_log=${l_general_log:-"/var/log/${db_name,,}/${db_name,,}_general.log"}
    [[ -s "${l_general_log}" ]] && echo '' > "${l_general_log}"

    echo -e "\nSuccessfully installing ${c_yellow}${db_name} ${db_newly_installed_version}${c_normal}, account info stores in ${c_yellow}~/.my.cnf${c_normal}!\n\nVersion info:\n\n${c_yellow}$(mysql --version)${c_normal}\n"
}

#########  3. Executing Process  #########
fn_InitializationCheck
fn_OSInfoDetection

fn_ExistedDetection
fn_ParaSpecifiedValVerification
fn_V2SelectionListGeneration
fn_DBInstallationOperation
fn_PostInstallationOperation


#########  4. EXIT Singal Processing  #########
# trap "commands" EXIT # execute command when exit from shell
fn_TrapEXIT(){
    fnBase_UnsetVars
    unset auto_installation
    unset root_password_new
    unset mysql_variant_type
    unset variant_version
    unset enable_firewall
    unset slave_mode
    unset proxy_server_specify
    unset conf_path
    unset data_dir
    unset mysql_port
    unset mysql_log_dir
    unset mysql_run_dir
    unset db_name
    unset db_version
    unset db_newly_installed_version
    unset service_name
}

trap fn_TrapEXIT EXIT


# 14.14 InnoDB Startup Options and System Variables
# https://dev.mysql.com/doc/refman/5.7/en/innodb-parameters.html
# 5.1.4 Server Command Options
# https://dev.mysql.com/doc/refman/5.7/en/server-options.html
# 5.1.5 Server System Variables
# https://dev.mysql.com/doc/refman/5.7/en/server-system-variables.html

# APT interrupt install operation will occur the following condition :
# E: Could not get lock /var/lib/dpkg/lock - open (11: Resource temporarily unavailable)
# E: Unable to lock the administration directory (/var/lib/dpkg/), is another process using it?
# solution is :
# rm -rf /var/lib/dpkg/lock /var/cache/apt/archives/lock
# dpkg --configure -a

# ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'mypass';
# ALTER USER 'root'@'localhost' IDENTIFIED BY 'MyNewPass4!';

# Remove from Debian variants
# sudo apt-get purge MariaDB* mariadb* Percona* mysql* -yq
# sudo apt-get autoremove -yq
# sudo rm -rf ~/.my.cnf* ~/.mysql_history /var/log/mysqld.log /var/log/mysql* /var/lib/mysql/ /data/ /etc/mysql* /etc/my.cnf* /etc/apt/sources.list.d/mysql.list

# - Security Relevant
# https://dev.mysql.com/doc/refman/5.7/en/security-plugins.html
# local-infile=0   https://dev.mysql.com/doc/refman/5.7/en/load-data-local.html

# https://dev.mysql.com/doc/refman/5.7/en/information-schema.html
# https://dev.mysql.com/doc/refman/5.7/en/tables-table.html


# -- export database (master --> slave)
# mysqldump --databases database1 database2 database3 ... | gzip > /tmp/mysql_export.sql.gz

# -- import database (slave --> master)
# set autocommit=0;
# set foreign_key_checks=0;
# mysql < /tmp/mysql_export.sql
# set autocommit=1;
# set foreign_key_checks=1;

# -- mm - master  192.168.8.8
# grant replication slave on *.* to 'repl_slave'@'192.168.8.9' identified by 'mysql_slave@2018';
# show master status\G

# -- mm - slave  192.168.8.9
# stop slave;
# reset slave;
# change master to master_host='192.168.8.8',master_user='repl_slave',master_password='mysql_slave@2018',master_log_file='mysql-bin.000005',master_log_pos=860239165;
# start slave;
# show slave status\G

# -- List per storage engine size
# SELECT engine, count(*) as 'table', concat(round(sum(table_rows)/1000,3),' K') as rows , concat(round(sum(data_length)/(1024*1024),3),' MB') as data_size, concat(round(sum(index_length)/(1024*1024),3),' MB') as index_size, concat(round(sum(data_length+index_length)/(1024*1024*1024),3),' GB') as total_size, round(sum(index_length)/sum(data_length),2) idxfrac FROM information_schema.TABLES WHERE table_schema not in ('mysql','performance_schema','information_schema') GROUP BY engine HAVING engine is not NULL ORDER BY table_rows DESC;

# -- List per database size
# SELECT table_schema as 'database', concat(round(sum(table_rows)/1000,3),' K') rows, concat(round(sum(data_length+index_length)/(1024*1024*1024),3),' GB') total_size, round(sum(index_length)/sum(data_length),2) idxfrac FROM information_schema.TABLES WHERE table_schema not in ('mysql', 'performance_schema', 'information_schema') GROUP BY table_schema ORDER BY sum(data_length+index_length) DESC;

# -- List per InnoDB table
# SELECT concat(table_schema,'.',table_name) as 'table', engine, concat(round(table_rows/1000,3),'K') rows, concat(round(data_length/(1024*1024),3),' MB') data_size, concat(round(index_length/(1024*1024),3),' MB') index_size, concat(round((data_length+index_length)/(1024*1024*1024),3),'GB') total_size, round(index_length/data_length,2) idxfrac FROM information_schema.TABLES WHERE engine = 'InnoDB' AND table_schema not in ('mysql', 'performance_schema', 'information_schema') ORDER BY table_rows DESC;


# -- X Plugin  mysqlx-port = 33060
# https://dev.mysql.com/doc/refman/8.0/en/x-plugin-system-variables-options.html
# https://dev.mysql.com/doc/refman/8.0/en/document-store-setting-up.html

# Script End
