#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails

# Official Site: https://www.sublimetext.com
# License Keys: https://appnee.com/sublime-text-3-universal-license-keys-collection-for-win-mac-linux/

# Target: Automatically Install & Update Sublime Text 3 Editor On GNU/Linux
# Developer: MaxdSre

# Change Log:
# - Oct 08, 2020 09:03 Thu ET - initialization check method update
# - Oct 08, 2019 16:45 Tue ET - change to new base functions
# - Jan 01, 2019 11:24 Tue ET - code reconfiguration, include base funcions from other file
# - Sep 19, 2017 15:12 Tue +0800


#########  0-1. Singal Setting  #########
trap fnBase_TrapCleanUp INT QUIT

#########  0-2. Variables Setting  #########
# umask 022    # temporarily change umask value to 022
readonly official_site_url='https://www.sublimetext.com'
readonly software_fullname=${software_fullname:-'Sublime Text'}
readonly application_name=${application_name:-'SublimeText'}
readonly installation_dir="/opt/${application_name}"
readonly pixmaps_png_path="/usr/share/pixmaps/${application_name}.png"
readonly application_desktop_path="/usr/share/applications/${application_name}.desktop"

is_existed=${is_existed:-1}   # Default value is 1， assume system has installed Sublime Text
version_check=${version_check:-0}
source_pack_path=${source_pack_path:-}
is_uninstall=${is_uninstall:-0}
proxy_server_specify=${proxy_server_specify:-}


#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | sudo bash -s -- [options] ...
Installing / Updating Sublime Text 3 Editor On GNU/Linux!
This script requires superuser privileges (eg. root, su).

Support authorized software, please purchase license via \e[0m\e[31mhttps://www.sublimetext.com/buy\e[0m\e[34m.

[available option]
    -h    --help, show help info
    -c    --check, check current stable release version
    -f pack_path    --specify latest release package save path (e.g. /PATH/*.tar.bz2) or dir (default is ~/Downloads/) in system
    -p [protocol:]ip:port    --proxy host (http|https|socks4|socks5), default protocol is http
    -u    --uninstall, uninstall software installed
\e[0m"
}

while getopts "hcup:" option "$@"; do
    case "$option" in
        c ) version_check=1 ;;
        f ) source_pack_path="$OPTARG" ;;
        u ) is_uninstall=1 ;;
        p ) proxy_server_specify="$OPTARG" ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done


#########  1-2 Preparation Operation  #########
fn_InitializationCheck(){
    local l_user_codebase_dir="$HOME/.config/axdsop"
    local l_base_function_temp_save_path=''
    local l_need_remove=0
    local l_base_function_url='https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/base_functions.sh'
    l_base_function_temp_save_path=$(find "${l_user_codebase_dir}" -type f -name "${l_base_function_url##*/}" -print 2> /dev/null)
    
    if [[ ! -f "${l_base_function_temp_save_path}" ]]; then
        local l_download_tool=${l_download_tool:-'wget -qO-'}
        [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_tool='curl -fsL'
        l_need_remove=1
        l_base_function_temp_save_path=$(mktemp -t XXXXXXX)
        ${l_download_tool} "${l_base_function_url}" > "${l_base_function_temp_save_path}"
    fi

    if [[ -f "${l_base_function_temp_save_path}" && -s "${l_base_function_temp_save_path}" ]]; then
        . "${l_base_function_temp_save_path}"
        [[ "${l_need_remove}" -eq 1 ]] && rm "${l_base_function_temp_save_path}"
    else
        echo "Sorry, fail to get base script path."
        exit
    fi

    # tput clear  # Echo the clear-screen sequence for the current terminal.
    # specify 1 means need sudo privilege
    fnBase_RunningEnvironmentCheck '1'

    fnBase_CommandExistCheckPhase 'bzip2' # CentOS/Debian/OpenSUSE: bzip2
    fnBase_CommandExistCheckPhase 'tar' # .tar.bz2
}


#########  2-1. Uninstall  #########
fn_UninstallOperation(){
    if [[ "${is_uninstall}" -eq 1 ]]; then
        [[ "${is_existed}" -eq 1 ]] || fnBase_ExitStatement "${c_blue}Note${c_normal}: no ${software_fullname} is found in your system!"

        [[ -f "${pixmaps_png_path}" ]] && rm -f "${pixmaps_png_path}"
        [[ -f "${application_desktop_path}" ]] && rm -f "${application_desktop_path}"

        [[ -d "${installation_dir}" ]] && rm -rf "${installation_dir}"
        [[ -d "${installation_dir}${bak_suffix}" ]] && rm -rf "${installation_dir}${bak_suffix}"

        local config_path=${config_path:-"${login_user_home}/.config/sublime-text-3"}
        config_path=$(find "${login_user_home}/.config" -type d -name 'sublime-text*' -exec ls -d {} \;)
        [[ -d "${config_path}" ]] && rm -rf "${config_path}"    # ~/.config/sublime-text-3

        [[ -d "${installation_dir}" ]] || fnBase_ExitStatement "${software_fullname}  is successfully removed from your system!"
    fi
}


#########  2-2. Latest & Local Version Check  #########
fn_VersionComparasion(){
    fnBase_CentralOutputTitle "${software_fullname}"
    fnBase_OperationPhaseStatement 'Version Detection'

    # - Local version
    current_local_version=${current_local_version:-}
    # Sublime Text Build 3176
    [[ -f "${installation_dir}/sublime_text" ]] && current_local_version=$("${installation_dir}/sublime_text" -v | sed -r -n 's@[^[:digit:]]+([[:digit:]]*).*@\1@g;p')
    [[ -z "${current_local_version}" ]] && is_existed=0

    if [[ "${is_existed}" -eq 1 ]]; then
        fnBase_OperationProcedureStatement 'Local version detecting'
        fnBase_OperationProcedureResult "${current_local_version}"
    fi

    # - Latest online version info
    fnBase_OperationProcedureStatement 'Online version detecting'
    download_page_html=$(mktemp -t "${mktemp_format}")

    local download_page_url=${download_page_url:-}
    download_page_url=$($download_method "${official_site_url}" | sed -r -n '/Download<\/a>/{s@.*href="([^"]*)".*@'"${official_site_url/%\/}"'\1@g;p}' | head -n 1)

    $download_method "${download_page_url}" > "${download_page_html}"

    online_release_version=${online_release_version:-}
    online_release_version=$(sed -r -n '/Version:/{s@<[^>]*>@@g;s@[^[:digit:]]*([[:digit:]]*).*@\1@g;p}' "${download_page_html}")

    online_release_date=${online_release_date:-}
    online_release_date=$(sed -r -n '/release-date/{s@<[^>]*>@@g;p}' "${download_page_html}" | awk '{"date --date=\""$0"\" +\"%F\"" | getline a;print a;exit}')

    if [[ -n "${online_release_version}" ]]; then
        fnBase_OperationProcedureResult "${online_release_version} - ${online_release_date}"
    else
        fnBase_OperationProcedureResult '' 1
    fi

    # - Just online version check
    if [[ "${version_check}" -eq 1 ]]; then
        fnBase_ExitStatement ''
    fi

    # - Online & Local Version Comparasion
    if [[ "${is_existed}" -eq 1 ]]; then
        if [[ "${online_release_version}" == "${current_local_version}" ]]; then
            fnBase_ExitStatement "\nLatest version (${c_red}${online_release_version}${c_normal}) has been existed in your system."
        elif [[ $(fnBase_VersionNumberComparasion "${current_local_version}" "${online_release_version}" '<') -eq 1 ]]; then
            printf "\nExisted version local (${c_red}%s${c_normal}) < Latest version online (${c_red}%s${c_normal}).\n" "${current_local_version}" "${online_release_version}"
        fi
    # else
    #     printf "No %s find in your system!\n" "${software_fullname}"
    fi
}


#########  2-3. Download & Decompress Latest Software  #########
fn_DownloadAndDecompressOperation(){
    local l_operation_type=${l_operation_type:-'installing'}
    [[ "${is_existed}" -eq 1 ]] && l_operation_type='updating'
    fnBase_OperationPhaseStatement "${l_operation_type^}"

    local online_release_downloadlink=${online_release_downloadlink:-}
    online_release_downloadlink=$(sed -r -n '/Linux repos/{s@.*href="([^"]+)">64 bit.*@\1@g;p}' "${download_page_html}")
    # https://download.sublimetext.com/sublime_text_3_build_3143_x64.tar.bz2
    [[ "${online_release_downloadlink}" =~ ^https?:// ]] || fnBase_ExitStatement "${c_red}Sorry${c_normal}: fail to extract package download url!"

    local online_release_pack_name="${online_release_downloadlink##*/}"

    local l_is_need_download=${l_is_need_download:-1}
    local l_specified_pack_dir=${l_specified_pack_dir:-}

    # specified file path existed
    if [[ "${source_pack_path}" =~ .tar.bz2$ && -s "${source_pack_path}" ]]; then
        [[ "${source_pack_path##*/}" == "${online_release_pack_name}" ]] || fnBase_ExitStatement "Package ${c_red}${source_pack_path}${c_normal} is not same to latest release version name!"

    # specified dir or ~/Downloads existed
    elif [[ (-n "${source_pack_path}" && -d "${source_pack_path}" ) || -d "${user_download_dir}" ]]; then
        if [[ -d "${source_pack_path}" ]]; then
            l_specified_pack_dir="${source_pack_path}"
        elif [[ -d "${user_download_dir}" ]]; then
            l_specified_pack_dir="${user_download_dir}"
        fi

        l_specified_pack_dir="${l_specified_pack_dir%/}"

        if [[ -d "${l_specified_pack_dir}" && -f "${l_specified_pack_dir}/${online_release_pack_name}" ]]; then
            source_pack_path="${l_specified_pack_dir}/${online_release_pack_name}"
        fi
    fi

    # 1.1- existed package path in system
    if [[ -n "${source_pack_path}" && -f "${source_pack_path}" ]]; then
        fnBase_OperationProcedureStatement 'Existed pack find'
        if [[ -s "${source_pack_path}" ]]; then
            fnBase_OperationProcedureResult "${c_red}${source_pack_path%/*}/${c_normal}"
            l_is_need_download=0
        else
            fnBase_OperationProcedureResult '' 1
        fi
    fi

    # 2 - Download Directly From Official Site
    local pack_save_path=${pack_save_path:-}

    if [[ "${l_is_need_download}" -eq 1 ]]; then
        fnBase_OperationProcedureStatement "Downloading ${c_red}${online_release_pack_name}${c_normal}"
        pack_save_path="${temp_save_dir}/${online_release_pack_name}"
        [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"

        $download_method "${online_release_downloadlink}" > "${pack_save_path}"

        if [[ -s "${pack_save_path}" ]]; then
            fnBase_OperationProcedureResult
        else
            [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
            fnBase_OperationProcedureResult '' 1
        fi

        # use variable "source_pack_path" to execute decompress operation, variable "pack_save_path" used for delete temporary download package
        source_pack_path="${pack_save_path}"
    fi

    # 3 - Decompress & Compiling
    fnBase_OperationProcedureStatement 'Decompressing'
    local application_backup_path="${installation_dir}${bak_suffix}"
    [[ -d "${application_backup_path}" ]] && rm -rf "${application_backup_path}"

    [[ -d "${installation_dir}" ]] && mv "${installation_dir}" "${application_backup_path}"
    [[ -d "${installation_dir}" ]] || mkdir -p "${installation_dir}"

    tar xf "${source_pack_path}" -C "${installation_dir}" --strip-components=1 2> /dev/null

    local new_installed_version=${new_installed_version:-}
    new_installed_version=$("${installation_dir}/sublime_text" -v | sed -r -n 's@[^[:digit:]]+([[:digit:]]*).*@\1@g;p')    # Just Installed Version In System

    if [[ "${online_release_version}" == "${new_installed_version}" ]]; then
        [[ -d "${application_backup_path}" ]] && rm -rf "${application_backup_path}"
        fnBase_OperationProcedureResult "${c_red}${installation_dir%/}/${c_normal}"

        # copy newly downloaded pack to ~/Downloads
        local l_user_download_save_path=${l_user_download_save_path:-"${user_download_dir}/${source_pack_path##*/}"}
        if [[ -d "${user_download_dir}" && ! -f "${l_user_download_save_path}" ]]; then
            cp "${source_pack_path}" "${l_user_download_save_path}"
            chown "${login_user}" "${l_user_download_save_path}"

            # removed old version package existed
            if [[ "${is_existed}" -eq 1 ]]; then
                local l_existed_old_pack_path=${l_existed_old_pack_path:-"${user_download_dir}/${online_release_pack_name//${online_release_version}/${current_local_version}}"}
                [[ -f "${l_existed_old_pack_path}" ]] && rm -f "${l_existed_old_pack_path}"
            fi
        fi
    else
        [[ -d "${installation_dir}" ]] && rm -rf "${installation_dir}"
        [[ "${is_existed}" -eq 1 ]] && mv "${application_backup_path}" "${installation_dir}"
        fnBase_OperationProcedureResult '' 1
    fi

    [[ -n "${pack_save_path}" && -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
}


#########  2-4. Plug-in And Preferences Configuration  #########
fn_PluginsAndUserPreferencesConfiguration(){
    fnBase_OperationPhaseStatement 'Configuration'

    fnBase_OperationProcedureStatement 'Plug-in Installationg'
    local default_config_path=${default_config_path:-}
    default_config_path="${login_user_home}/.config/sublime-text-3"

    if [[ ! -d "${default_config_path}" ]]; then
        mkdir -p "${default_config_path}"
        chown -R "${login_user}" "${default_config_path}"
        chgrp -R "${login_user}" "${default_config_path}" &> /dev/null
    fi

    # - Plug-in list
    local plugin_list=${plugin_list:-}
    local plugin_save_path=${plugin_save_path:-"${default_config_path}/Installed Packages"}

    [[ -d "${plugin_save_path}" ]] || mkdir -p "${plugin_save_path}"

plugin_list=$(cat << EOF
Emmet|https://github.com/sergeche/emmet-sublime/archive/master.zip
Better Completion|https://github.com/Pleasurazy/Sublime-Better-Completion/archive/master.zip
Side​Bar​Enhancements|https://github.com/SideBarEnhancements-org/SideBarEnhancements/archive/st3.zip
Convert​To​UTF8|https://github.com/seanliang/ConvertToUTF8/archive/master.zip
Bracket​Highlighter|https://github.com/facelessuser/BracketHighlighter/archive/master.zip
Markdown​Editing|https://github.com/SublimeText-Markdown/MarkdownEditing/archive/master.zip
SublimeCodeIntel|https://github.com/SublimeCodeIntel/SublimeCodeIntel/archive/master.zip
File​Diffs|https://github.com/colinta/SublimeFileDiffs/archive/master.zip
Sublime​Linter|https://github.com/SublimeLinter/SublimeLinter3/archive/master.zip
EOF
)

    echo "${plugin_list}" | while IFS="|" read -r plugin_name download_url; do
        local plugin_save_name=${plugin_save_name:-}
        plugin_save_name="${plugin_save_path}/${plugin_name}"
        $download_method "${download_url}" > "${plugin_save_name}"
        # [[ -s "${plugin_save_name}" ]] && printf "Successfully install plug-in ${c_blue}%s${c_normal}.\n" "${plugin_name}"
        unset plugin_save_name
    done

    fnBase_OperationProcedureResult

    # - Package Control
    fnBase_OperationProcedureStatement 'Package Manager Installation'
    # https://packagecontrol.io/
    local packagecontrol_site=${packagecontrol_site:-'https://packagecontrol.io'}
    local package_control_url=${package_control_url:-}
    package_control_url=$($download_method "${packagecontrol_site}/installation" | sed -r -n '/Control.sublime-package<\/a>/{s@.*href="([^"]*)".*@'"${packagecontrol_site}"'\1@g;p}')

    local package_control_save_path=${package_control_save_path:-"${plugin_save_path}/${package_control_url##*/}"}
    package_control_save_path="${package_control_save_path//%20/ }"
    $download_method "${package_control_url}" > "${package_control_save_path}"

    if [[ -s "${package_control_save_path}" ]]; then
        fnBase_OperationProcedureResult 'Package Control'
    else
        fnBase_OperationProcedureResult '' 1
    fi


    # - Preferences Configuration
    fnBase_OperationProcedureStatement 'Preferences Configuration'
    local user_preference_dir=${user_preference_dir:-"${default_config_path}/Packages/User"}
    [[ -d "${user_preference_dir}" ]] || mkdir -p "${user_preference_dir}"

    # "color_scheme": "Packages/User/SublimeLinter/Monokai (SL).tmTheme",

tee "${user_preference_dir}/Preferences.sublime-settings" 1> /dev/null <<-EOF
{
    "auto_find_in_selection": true,
    "bold_folder_labels": true,
    "font_face": "Ubuntu Mono",
    "font_options": "subpixel_antialias",
    "font_size": 16,
    "highlight_line": true,
    "highlight_modified_tabs": true,
    "ignored_packages":
    [
        "Vintage"
    ],
    "update_check":false,
    "line_numbers": true,
    "line_padding_bottom": 1,
    "line_padding_top": 1,
	"rulers": [],
    "scroll_past_end": true,
    "tab_completion": false,
    "tab_size": 4,
    "theme": "Adaptive.sublime-theme",
    "translate_tabs_to_spaces": true,
    "trim_trailing_white_space_on_save": true,
    "vintage_start_in_command_mode": false,
    "word_wrap": true
}
EOF


    chown -R "${login_user}" "${default_config_path}"
    chgrp -R "${login_user}" "${default_config_path}" &> /dev/null

    fnBase_OperationProcedureResult "${default_config_path}"
}


#########  2-5. Desktop Configuration  #########
fn_DesktopConfiguration(){
    fnBase_OperationPhaseStatement 'Configuration'

    if [[ -d '/usr/share/applications' ]]; then
        fnBase_OperationProcedureStatement 'Desktop Configuration'

        [[ -f "${installation_dir}/Icon/48x48/sublime-text.png" ]] && ln -sf "${installation_dir}/Icon/48x48/sublime-text.png" "${pixmaps_png_path}"

        # /PATH/share/applications/filezilla.desktop
        echo -e "[Desktop Entry]\nVersion=1.0\nType=Application\nName=Sublime Text\nGenericName[en]=Text Editor\nComment=Sophisticated text editor for code, markup and prose\nExec=installation_dir/sublime_text %F\nTerminal=false\nMimeType=text/plain;\nIcon=application_name.png\nCategories=TextEditor;Development;\nStartupNotify=true\nActions=Window;Document;\n\n[Desktop Action Window]\nName=New Window\nExec=installation_dir/sublime_text -n\nOnlyShowIn=Unity;\n\n[Desktop Action Document]\nName=New File\nExec=installation_dir/sublime_text --command new_file\nOnlyShowIn=Unity;" > "${application_desktop_path}"

        sed -i -r 's@application_name@'"${application_name}"'@g' "${application_desktop_path}"
        sed -i -r 's@installation_dir@'"${installation_dir}"'@g' "${application_desktop_path}"
        fnBase_OperationProcedureResult "${application_desktop_path%/*}/"
    fi
}


#########  2-6. Operation Time Cost  #########
fn_TotalTimeCosting(){
    finish_time=$(date +'%s')        # End Time Of Operation
    total_time_cost=$((finish_time-start_time))   # Total Time Of Operation
    printf "\nTotal time cost is ${c_red}%s${c_normal} seconds!\n" "${total_time_cost}"
}


#########  3. Executing Process  #########
fn_InitializationCheck

fn_UninstallOperation
fn_VersionComparasion
fn_DownloadAndDecompressOperation
fn_DesktopConfiguration
fn_TotalTimeCosting


#########  4. EXIT Singal Processing  #########
# trap "commands" EXIT # execute command when exit from shell
fn_TrapEXIT(){
    fnBase_UnsetVars
    unset is_existed
    unset version_check
    unset source_pack_path
    unset is_uninstall
    unset proxy_server_specify
    unset download_method
    unset current_local_version
    unset online_release_version
    unset online_release_date
}

trap fn_TrapEXIT EXIT

# Script End
