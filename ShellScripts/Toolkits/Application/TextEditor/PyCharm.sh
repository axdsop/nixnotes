#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails


# Official Site: https://www.jetbrains.com/pycharm/
# Documentation
# - https://www.jetbrains.com/help/pycharm
# - https://www.jetbrains.com/help/pycharm/install-and-set-up-pycharm.html

# Target: Automatically Install & Update JetBrains PyCharm On GNU/Linux
# Developer: MaxdSre

# Change Log:
# - Jun 12, 2022 11:06 Sun ET - add config and cache dir path to delete
# - Oct 08, 2020 09:02 Thu ET - initialization check method update
# - Oct 08, 2019 19:55 Tue ET - change to new base functions
# - May 22, 2018 14:53~16:58 Mon ET

# Free license server - Just for trial


# https://stackoverflow.com/questions/43444949/why-does-intellij-want-to-accept-incoming-network-connections
# Listen port 6942, 63342
# local server http://127.0.0.1:63342


#########  0-1. Singal Setting  #########
trap fnBase_TrapCleanUp INT QUIT

#########  0-2. Variables Setting  #########
# umask 022    # temporarily change umask value to 022
readonly official_site='https://www.jetbrains.com/pycharm/'
readonly software_fullname='JetBrains PyCharm'
readonly application_name='PyCharm'
installation_dir="/opt/${application_name}"
readonly pixmaps_png_path="/usr/share/pixmaps/${application_name}.png"
readonly application_desktop_path="/usr/share/applications/${application_name}.desktop"
is_existed=${is_existed:-0}      # Default value is 0， assume software is not existed

source_pack_path=${source_pack_path:-}
version_check=${version_check:-0}
type_choose=${type_choose:-0}
is_uninstall=${is_uninstall:-0}
proxy_server_specify=${proxy_server_specify:-''}


#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | sudo bash -s -- [options] ...

Installing / Updating PyCharm On GNU/Linux!
This script requires superuser privileges (eg. root, su).

JetBrains PyCharm Professional is a commercial software, please support genuine software. Pay link \e[0m\e[31mhttps://www.jetbrains.com/store/\e[0m\e[33m.

[available option]
    -h    --help, show help info
    -c    --check, check current stable release version
    -f pack_path    --specify latest release package save path (e.g. /PATH/*.tar.gz) or dir (default is ~/Downloads/) in system
    -t type    --choose version type (0 is Community, 1 is Professional), default is 0
    -p [protocol:]ip:port    --proxy host (http|https|socks4|socks5), default protocol is http
    -u    --uninstall, uninstall software installed
\e[0m"
}

while getopts "hcf:t:up:" option "$@"; do
    case "$option" in
        c ) version_check=1 ;;
        f ) source_pack_path="$OPTARG" ;;
        t ) type_choose="$OPTARG" ;;
        u ) is_uninstall=1 ;;
        p ) proxy_server_specify="$OPTARG" ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done


#########  1-2 Preparation Operation  #########
fn_InitializationCheck(){
    local l_user_codebase_dir="$HOME/.config/axdsop"
    local l_base_function_temp_save_path=''
    local l_need_remove=0
    local l_base_function_url='https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/base_functions.sh'
    l_base_function_temp_save_path=$(find "${l_user_codebase_dir}" -type f -name "${l_base_function_url##*/}" -print 2> /dev/null)
    
    if [[ ! -f "${l_base_function_temp_save_path}" ]]; then
        local l_download_tool=${l_download_tool:-'wget -qO-'}
        [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_tool='curl -fsL'
        l_need_remove=1
        l_base_function_temp_save_path=$(mktemp -t XXXXXXX)
        ${l_download_tool} "${l_base_function_url}" > "${l_base_function_temp_save_path}"
    fi

    if [[ -f "${l_base_function_temp_save_path}" && -s "${l_base_function_temp_save_path}" ]]; then
        . "${l_base_function_temp_save_path}"
        [[ "${l_need_remove}" -eq 1 ]] && rm "${l_base_function_temp_save_path}"
    else
        echo "Sorry, fail to get base script path."
        exit
    fi

    # tput clear  # Echo the clear-screen sequence for the current terminal.
    # specify 1 means need sudo privilege
    fnBase_RunningEnvironmentCheck '1'

    fnBase_CommandExistCheckPhase 'gzip' # CentOS/Debian/OpenSUSE: gzip
    fnBase_CommandExistCheckPhase 'tar' # .tar.gz
}


#########  2-1. Uninstall  #########
fn_UninstallOperation(){
    if [[ "${is_uninstall}" -eq 1 ]]; then
        if [[ -d "${installation_dir}" ]]; then
            [[ -d "${installation_dir}" ]] && rm -rf "${installation_dir}"
            [[ -d "${installation_dir}${bak_suffix}" ]] && rm -rf "${installation_dir}${bak_suffix}"

            [[ -f "${pixmaps_png_path}" ]] && rm -f "${pixmaps_png_path}"
            [[ -f "${application_desktop_path}" ]] && rm -f "${application_desktop_path}"

            # ~/.PyCharm2018.3/
            rm -rf "${login_user_home}/.PyCharm"*
            [[ -d "${login_user_home}/.local/share/JetBrains/" ]] && rm -rf "${login_user_home}/.local/share/JetBrains/"
            # [[ -d "${login_user_home}/PycharmProjects" ]] && rm -rf "${login_user_home}/PycharmProjects"

            # PyCharm2022.1
            rm -rf "${login_user_home}/.config/JetBrains/"
            rm -rf "${login_user_home}/.cache/JetBrains/"

            [[ -d "${installation_dir}" ]] || fnBase_ExitStatement "${software_fullname} is successfully removed from your system!"
        else
            fnBase_ExitStatement "${c_blue}Note${c_normal}: no ${software_fullname} is found in your system!"
        fi
    fi
}

#########  2-1. Latest & Local Version Check  #########
fn_VersionComparasion(){
    fnBase_CentralOutputTitle 'JetBrains PyCharm'
    fnBase_OperationPhaseStatement 'Version Detection'

    # - Local version
    current_local_version=${current_local_version:-}
    local local_release_type=${local_release_type:-}
    if [[ -f "${installation_dir}/VERSION" ]]; then
        is_existed=1
        local_release_type=$(cut -d\| -f 1 "${installation_dir}/VERSION")
        current_local_version=$(cut -d\| -f 2 "${installation_dir}/VERSION")

        fnBase_OperationProcedureStatement 'Local version detecting'
        fnBase_OperationProcedureResult "${local_release_type} ${current_local_version}"
    fi

    # - Latest online version info
    fnBase_OperationProcedureStatement 'Online version detecting'

    # 0 is Community, 1 is Professional
    case "${type_choose,,}" in
        1 ) type_choose='PCP' ;;
        0|* ) type_choose='PCC' ;;
    esac

    [[ "${is_existed}" -eq 1 && "${local_release_type,,}" != "${type_choose,,}" ]] && type_choose='PCP'

    # curl 'https://data.services.jetbrains.com/products/releases?code=PCP%2CPCC&latest=true&type=release&build=&_=1527017746697' -H 'Origin: https://www.jetbrains.com' -H 'Accept-Encoding: gzip, deflate, br' -H 'Accept-Language: en-US,en;q=0.9' -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3590.37 Safari/537.36' -H 'Accept: application/json, text/javascript, */*; q=0.01' -H 'Referer: https://www.jetbrains.com/pycharm/download/' -H 'Connection: keep-alive' -H 'DNT: 1' --compressed


    # Ubuntu 16.04.4 LTS (Xenial Xerus) just has `python3`, has no 'python'
    local l_python_name=${l_python_name:-'python'}

    if fnBase_CommandExistIfCheck 'python3'; then
        l_python_name='python3'
    elif fnBase_CommandExistIfCheck 'python'; then
        l_python_name='python'
    fi

    # https://stackoverflow.com/questions/2043453/executing-python-multi-line-statements-in-the-one-line-command-line#2043499
    online_release_info=${online_release_info:-}
    online_release_info=$($download_method 'https://data.services.jetbrains.com/products/releases?code=PCP%2CPCC&latest=true' | ${l_python_name} -c $'import json,sys\nobj=json.load(sys.stdin)\nfor item in obj.keys(): release_info = obj[item][0]; print("{}|{}|{}|{}|{}|{}|{}|{}".format(item, release_info["date"],release_info["majorVersion"],release_info["version"],release_info["build"],release_info["downloads"]["linux"]["link"],release_info["downloads"]["linux"]["checksumLink"],release_info["downloads"]["linux"]["size"]))' | sed -r -n '/^'"${type_choose}"'/{p}')

    # PCC|2019-09-25|2019.2|2019.2.3|192.6817.19|https://download.jetbrains.com/python/pycharm-community-2019.2.3.tar.gz|https://download.jetbrains.com/python/pycharm-community-2019.2.3.tar.gz.sha256|360509772
    # PCP|2019-09-25|2019.2|2019.2.3|192.6817.19|https://download.jetbrains.com/python/pycharm-professional-2019.2.3.tar.gz|https://download.jetbrains.com/python/pycharm-professional-2019.2.3.tar.gz.sha256|482051405

    online_release_type="${online_release_info%%|*}"
    online_release_date=$(echo "${online_release_info}" | cut -d\| -f 2 | date +'%b %d, %Y' -f - 2> /dev/null)
    online_release_version=$(echo "${online_release_info}" | cut -d\| -f 4)
    online_release_build_info=$(echo "${online_release_info}" | cut -d\| -f 5)

    if [[ -n "${online_release_version}" ]]; then
        fnBase_OperationProcedureResult "${online_release_type} ${online_release_version} - ${online_release_date}"
    else
        fnBase_OperationProcedureResult '' 1
    fi

    # - Just online version check
    if [[ "${version_check}" -eq 1 ]]; then
        fnBase_ExitStatement ''
    fi

    # - Online & Local Version Comparasion
    if [[ "${is_existed}" -eq 1 ]]; then
        if [[ "${online_release_version}" == "${current_local_version}" ]]; then
            fnBase_ExitStatement "\nLatest ${c_yellow}${local_release_type}${c_normal} version (${c_red}${online_release_version}${c_normal}) has been existed in your system."
        elif [[ $(fnBase_VersionNumberComparasion "${current_local_version}" "${online_release_version}" '<') -eq 1 ]]; then
            printf "\nExisted version local (${c_red}%s${c_normal}) < Latest version online (${c_red}%s${c_normal}).\n" "${current_local_version}" "${online_release_version}"
        fi
    # else
    #     printf "No %s find in your system!\n" "${software_fullname}"
    fi
}


#########  2-3. Download & Decompress Latest Software  #########
fn_DownloadAndDecompressOperation(){
    local l_operation_type=${l_operation_type:-'installing'}
    [[ "${is_existed}" -eq 1 ]] && l_operation_type='updating'
    fnBase_OperationPhaseStatement "${l_operation_type^}"

    local online_release_downloadlink
    online_release_downloadlink=$(echo "${online_release_info}" | cut -d\| -f 6)
    local online_release_dgst=${online_release_dgst:-}
    local online_release_pack_name=${online_release_pack_name:-"${online_release_downloadlink##*/}"}

    local l_is_need_download=${l_is_need_download:-1}
    local l_specified_pack_dir=${l_specified_pack_dir:-}

    # specified file path existed
    if [[ "${source_pack_path}" =~ .tar.gz$ && -s "${source_pack_path}" ]]; then
        [[ "${source_pack_path##*/}" == "${online_release_pack_name}" ]] || fnBase_ExitStatement "Package ${c_red}${source_pack_path}${c_normal} is not same to latest ${c_yellow}${online_release_type}${c_normal} version name."

    # specified dir or ~/Downloads existed
    elif [[ (-n "${source_pack_path}" && -d "${source_pack_path}" ) || -d "${user_download_dir}" ]]; then
        if [[ -d "${source_pack_path}" ]]; then
            l_specified_pack_dir="${source_pack_path}"
        elif [[ -d "${user_download_dir}" ]]; then
            l_specified_pack_dir="${user_download_dir}"
        fi

        l_specified_pack_dir="${l_specified_pack_dir%/}"

        if [[ -d "${l_specified_pack_dir}" && -f "${l_specified_pack_dir}/${online_release_pack_name}" ]]; then
            source_pack_path="${l_specified_pack_dir}/${online_release_pack_name}"
        fi
    fi

    # https://download.jetbrains.com/python/pycharm-professional-2018.1.3.tar.gz.sha256
    online_release_dgst=$($download_method "${online_release_downloadlink}.sha256" 2> /dev/null | sed -r -n 's@^[[:space:]]*([^[:space:]]+).*$@\1@g;p')

    # 1.1- existed package path in system
    if [[ -n "${source_pack_path}" && -f "${source_pack_path}" ]]; then
        fnBase_OperationProcedureStatement 'Existed pack dgst verifying'

        if [[ -n "${online_release_dgst}" && $(fnBase_HashDigestVerification "${source_pack_path}" '256') == "${online_release_dgst}" ]]; then
            fnBase_OperationProcedureResult "${c_red}${source_pack_path%/*}/${c_normal}"
            l_is_need_download=0
        else
            fnBase_OperationProcedureResult '' 1
        fi
    fi

    # 2 - Download Directly From Official Site
    local pack_save_path=${pack_save_path:-}
    if [[ "${l_is_need_download}" -eq 1 ]]; then
        fnBase_OperationProcedureStatement "Downloading ${c_red}${online_release_pack_name}${c_normal}"
        pack_save_path="${temp_save_dir}/${online_release_pack_name}"
        [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"

        $download_method "${online_release_downloadlink}" > "${pack_save_path}"

        if [[ -s "${pack_save_path}" ]]; then
            fnBase_OperationProcedureResult
        else
            [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
            fnBase_OperationProcedureResult '' 1
        fi

        fnBase_OperationProcedureStatement "SHA256 dgst verification"
        if [[ -n "${online_release_dgst}" && $(fnBase_HashDigestVerification "${pack_save_path}" '256') == "${online_release_dgst}"  ]]; then
            fnBase_OperationProcedureResult
        else
            [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
            fnBase_OperationProcedureResult '' 1
        fi

        # use variable "source_pack_path" to execute decompress operation, variable "pack_save_path" used for delete temporary download package
        source_pack_path="${pack_save_path}"
    fi

    # 3 - Decompress & Extract
    fnBase_OperationProcedureStatement "Installing"
    local application_backup_path="${installation_dir}${bak_suffix}"
    [[ -d "${application_backup_path}" ]] && rm -rf "${application_backup_path}"

    [[ -d "${installation_dir}" ]] && mv "${installation_dir}" "${application_backup_path}"
    [[ -d "${installation_dir}" ]] || mkdir -p "${installation_dir}"
    tar xf "${source_pack_path}" -C "${installation_dir}" --strip-components=1 2> /dev/null

    local new_installed_version=${new_installed_version:-"${online_release_version}"}

    if [[ "${online_release_version}" == "${new_installed_version}" ]]; then
        # PCP|2018.1.3|181.4892.64
        echo "${online_release_type}|${online_release_version}|${online_release_build_info}" > "${installation_dir}/VERSION"

        [[ -d "${application_backup_path}" ]] && rm -rf "${application_backup_path}"
        fnBase_OperationProcedureResult "${c_red}${installation_dir%/}/${c_normal}"

        # copy newly downloaded pack to ~/Downloads
        local l_user_download_save_path=${l_user_download_save_path:-"${user_download_dir}/${source_pack_path##*/}"}
        if [[ -d "${user_download_dir}" && ! -f "${l_user_download_save_path}" ]]; then
            cp "${source_pack_path}" "${l_user_download_save_path}"
            chown "${login_user}" "${l_user_download_save_path}"
            chgrp -hR "${login_user}" "${l_user_download_save_path}" 2> /dev/null

            # removed old version package existed
            if [[ "${is_existed}" -eq 1 ]]; then
                local l_existed_old_pack_path=${l_existed_old_pack_path:-"${user_download_dir}/${online_release_pack_name//${online_release_version}/${current_local_version}}"}
                [[ -f "${l_existed_old_pack_path}" ]] && rm -f "${l_existed_old_pack_path}"
            fi
        fi
    else
        [[ -d "${installation_dir}" ]] && rm -rf "${installation_dir}"
        [[ "${is_existed}" -eq 1 ]] && mv "${application_backup_path}" "${installation_dir}"
        fnBase_OperationProcedureResult '' 1
    fi

    chown -R root:root "${installation_dir}"
    [[ -n "${pack_save_path}" && -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
}


#########  2-4. Desktop Configuration  #########
fn_DesktopConfiguration(){
    fnBase_OperationPhaseStatement 'Configuration'

    if [[ -d '/usr/share/applications' ]]; then
        fnBase_OperationProcedureStatement 'Desktop Configuration'
        [[ -f "${installation_dir}/bin/pycharm.png" ]] && ln -sf "${installation_dir}/bin/pycharm.png" "${pixmaps_png_path}"

        echo -e "[Desktop Entry]\nVersion=1.0\nType=Application\nGenericName[en]=Text Editor\nName=PyCharm\nComment=Python IDE for Professional Developers by JetBrains.\nIcon=application_name\nExec=\"installation_dir/bin/pycharm.sh\" %f\nComment=The Drive to Develop\nCategories=Development;IDE;\nTerminal=false\nStartupWMClass=jetbrains-pycharm" > "${application_desktop_path}"

        sed -i -r 's@application_name@'"${application_name}"'@g' "${application_desktop_path}"
        sed -i -r 's@installation_dir@'"${installation_dir}"'@g' "${application_desktop_path}"
        fnBase_OperationProcedureResult "${application_desktop_path}"
    fi
}

#########  2-5. Operation Time Cost  #########
fn_TotalTimeCosting(){
    finish_time=$(date +'%s')        # End Time Of Operation
    total_time_cost=$((finish_time-start_time))   # Total Time Of Operation
    printf "\nTotal time cost is ${c_red}%s${c_normal} seconds!\n" "${total_time_cost}"
}


#########  3. Executing Process  #########
fn_InitializationCheck

fn_UninstallOperation
fn_VersionComparasion
fn_DownloadAndDecompressOperation
fn_DesktopConfiguration
fn_TotalTimeCosting


#########  4. EXIT Singal Processing  #########
# trap "commands" EXIT # execute command when exit from shell
fn_TrapEXIT(){
    fnBase_UnsetVars
    unset installation_dir
    unset is_existed
    unset version_check
    unset source_pack_path
    unset is_uninstall
    unset proxy_server_specify
    unset atom_config_path
}

trap fn_TrapEXIT EXIT

# Script End
