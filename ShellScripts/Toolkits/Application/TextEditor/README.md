# Toolkits - Application / Text Editor

## List

* [Atom](./AtomTextEditor.sh)
* [Visual Studio Code](./VisualStudioCode.sh)
* [VSCodium](./VSCodium.sh)
* [Sublime Text 3 Editor](./SublimeText.sh)
* [JetBrains PyCharm](./PyCharm.sh)

<!-- End -->
