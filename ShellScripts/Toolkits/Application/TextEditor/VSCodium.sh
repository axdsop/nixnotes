#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails

# Official Site: https://www.vscodium.com
# Git: https://github.com/VSCodium/vscodium
# Documentation
# - https://github.com/VSCodium/vscodium#download-install
# - https://code.visualstudio.com/docs/getstarted/themes
# Theme Preview
# - https://vscodethemes.com/
# awesome-vscode
# - https://viatsko.github.io/awesome-vscode/

# Extension Gallery Change
# - https://github.com/VSCodium/vscodium/blob/master/DOCS.md#extensions--marketplace


# Target: Automatically Install & Update VSCodium On GNU/Linux
# Developer: MaxdSre

# Change Log:
# - Aug 17, 2022 18:15 Wed ET - version info add release number
# - Aug 10, 2021 10:25 Tue ET - clean cached file in config dir to reduce size
# - Dec 13, 2020 09:03 Sun ET - optimize local version detection method
# - Nov 07, 2020 10:46 Sat ET - fix customize extension dir will be removed while upgrade new version
# - Oct 21, 2020 18:45 Wed ET - add customize extension dir
# - Oct 08, 2020 09:03 Thu ET - initialization check method update
# - Sep 25, 2020 09:32 Fri ET - Change extension gallery in file product.json
# - May 02, 2020 08:50 Sat ET - add software soft link into /usr/bin
# - Apr 18, 2020 15:18 Sat ET - optimize essential shared libaries checking
# - Mar 16, 2020 18:31 Mon ET - fix online release info extraction issue, instead 'sed' of 'cut'
# - Oct 19, 2019 20:15 Sat ET - add customize setting switch via 'customize_setting'
# - Oct 18, 2019 21:35 Fri ET - seperate extension list, settings.json as single file
# - Oct 18, 2019 16:15 Fri ET - add Rust extension install, configuration finle settings.json update
# - Oct 08, 2019 17:10 Tue ET - change to new base functions
# - May 16, 2019 17:04 Thu ET - add sha256 dgst check
# - Apr 07, 2019 22:12 Sun ET - first draft


# You are trying to start vscode as a super user which is not recommended. If you really want to, you must specify an alternate user data directory using the --user-data-dir argument.

# Client id: ~/.config/VSCodium/'exthost Crash Reports'/client_id  # if removed, it will regenerate new one
# Machine id: ~/.config/VSCodium/machineid  # if removed, it will regenerate new one


# Node.js Debug port
# Find listening TCP port:     /opt/VSCodium/vscodium --nolazy --inspect=60658 /opt/VSCodium/resources/app/out/bootstrap-fork --type=extensionHost

# Node.js debugging in VS Code  https://code.visualstudio.com/docs/nodejs/nodejs-debugging

# The port 9229 is the default debug port of the --inspect and --inspect-brk options. To use a different port (e.g. 12345), add it to the options like this: --inspect=12345 and --inspect-brk=12345 and change the port attribute in the launch configuration accordingly.


#########  0-1. Singal Setting  #########
trap fnBase_TrapCleanUp INT QUIT

#########  0-2. Variables Setting  #########
# umask 022    # temporarily change umask value to 022
readonly official_site='https://www.vscodium.com'
readonly software_fullname='VSCodium'
readonly application_name='VSCodium'
installation_dir="/opt/${application_name}"
readonly extension_dir="${installation_dir}/Extensions"
readonly vscodium_bin_path="${installation_dir}/bin/codium"
readonly usr_bin_softlink_path="/usr/bin/codium"
readonly pixmaps_png_path="/usr/share/pixmaps/${application_name}.png"
readonly application_desktop_path="/usr/share/applications/${application_name}.desktop"
is_existed=${is_existed:-1}   # Default value is 1， assume system has installed VSCodium
source_pack_path=${source_pack_path:-}
version_check=${version_check:-0}
customize_setting=${customize_setting:-0}
is_uninstall=${is_uninstall:-0}
proxy_server_specify=${proxy_server_specify:-''}

readonly custom_shellscript_url='https://gitlab.com/axdsop/nixnotes/raw/master/ShellScripts/Toolkits'
readonly custom_settings_json="${custom_shellscript_url}/_Configs/Application/vscode/settings.json"
readonly custom_extension_list="${custom_shellscript_url}/_Configs/Application/vscode/extension_list"


#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | sudo bash -s -- [options] ...

Installing / Updating VSCodium Editor On GNU/Linux!
This script requires superuser privileges (eg. root, su).

[available option]
    -h    --help, show help info
    -c    --check, check current stable release version
    -f pack_path    --specify latest release package save path (e.g. /PATH/*.tar.gz) or dir (default is ~/Downloads/) in system
    -s    --customize setting (extension install, settings.json) enable, default is disable
    -p [protocol:]ip:port    --proxy host (http|https|socks4|socks5), default protocol is http
    -u    --uninstall, uninstall software installed
\e[0m"
}

while getopts "hcf:sup:" option "$@"; do
    case "$option" in
        c ) version_check=1 ;;
        f ) source_pack_path="$OPTARG" ;;
        s ) customize_setting=1 ;;
        u ) is_uninstall=1 ;;
        p ) proxy_server_specify="$OPTARG" ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done


#########  1-2 Preparation Operation  #########
fn_InitializationCheck(){
    local l_user_codebase_dir="$HOME/.config/axdsop"
    local l_base_function_temp_save_path=''
    local l_need_remove=0
    local l_base_function_url='https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/base_functions.sh'
    l_base_function_temp_save_path=$(find "${l_user_codebase_dir}" -type f -name "${l_base_function_url##*/}" -print 2> /dev/null)
    
    if [[ ! -f "${l_base_function_temp_save_path}" ]]; then
        local l_download_tool=${l_download_tool:-'wget -qO-'}
        [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_tool='curl -fsL'
        l_need_remove=1
        l_base_function_temp_save_path=$(mktemp -t XXXXXXX)
        ${l_download_tool} "${l_base_function_url}" > "${l_base_function_temp_save_path}"
    fi

    if [[ -f "${l_base_function_temp_save_path}" && -s "${l_base_function_temp_save_path}" ]]; then
        . "${l_base_function_temp_save_path}"
        [[ "${l_need_remove}" -eq 1 ]] && rm "${l_base_function_temp_save_path}"
    else
        echo "Sorry, fail to get base script path."
        exit
    fi

    # tput clear  # Echo the clear-screen sequence for the current terminal.
    # specify 1 means need sudo privilege
    fnBase_RunningEnvironmentCheck '1'

    fnBase_CommandExistCheckPhase 'gzip' # CentOS/Debian/OpenSUSE: gzip
    fnBase_CommandExistCheckPhase 'tar' # .tar.gz
}


#########  2-1. Uninstall  #########
fn_UninstallOperation(){
    if [[ "${is_uninstall}" -eq 1 ]]; then
        if [[ -d "${installation_dir}" ]]; then
            # remove soft link
            [[ -L "${usr_bin_softlink_path}" && $(readlink -f "${usr_bin_softlink_path}") == "${vscodium_bin_path}" ]] && rm -f "${usr_bin_softlink_path}"

            [[ -d "${installation_dir}" ]] && rm -rf "${installation_dir}"
            [[ -d "${installation_dir}${bak_suffix}" ]] && rm -rf "${installation_dir}${bak_suffix}"

            [[ -f "${pixmaps_png_path}" ]] && rm -f "${pixmaps_png_path}"
            [[ -f "${application_desktop_path}" ]] && rm -f "${application_desktop_path}"

            [[ -d "${login_user_home}/.config/VSCodium/" ]] && rm -rf "${login_user_home}/.config/VSCodium/"

            [[ -d "${installation_dir}" ]] || fnBase_ExitStatement "${software_fullname} is successfully removed from your system!"
        else
            fnBase_ExitStatement "${c_blue}Note${c_normal}: no ${software_fullname} is found in your system!"
        fi
    fi
}


#########  2-1. Latest & Local Version Check  #########
fn_EssentialLibaryCheck(){
    # 1 - error while loading shared libraries: libXss.so.1: cannot open shared object file: No such file or directory
    # apt|libxss1|/usr/lib/x86_64-linux-gnu/libXss.so.1
    # zypper | libXss1 | /usr/lib64/libXss.so.1
    # yum | libXScrnSaver | /usr/lib64/libXss.so.1
    # pacman | libxss | /usr/lib64/libXss.so.1

    [[ -z $(find /usr/lib* -name 'libXss.so.1' -print 2>/dev/null) ]] && fnBase_ExitStatement "${c_red}Fatal Error${c_normal}: ${software_fullname} need shared libraries ${c_blue}libXss.so.1${c_normal} (apt: libxss1, pacman: libxss, zypper: libXss1, yum: libXScrnSaver)!"
}

fn_VSCode_CLI_Operation(){
    local l_path=${1:-"${vscodium_bin_path}"}
    local l_type="${2:-}"
    local l_e_name="${3:-}"
    local l_action="${4:-}"

    if [[ -f "${l_path}" ]]; then
        case "${l_type,,}" in
            version|v )
                local l_version
                # sed -n '1p'
                if [[ "${login_user}" == 'root' ]]; then
                    l_version=$("${l_path}" --version 2> /dev/null | head -n 1)
                else
                    l_version=$(sudo -u "${login_user}" "${l_path}" --version 2> /dev/null | head -n 1)
                fi
                echo "${l_version}"
                ;;
            extension|e )
                # https://code.visualstudio.com/docs/editor/command-line#_working-with-extensions
                local l_extension_dir_specify=${l_extension_dir_specify:-"${extension_dir}/${login_user}"}  # default path ~/.vscode-oss/extensions/
                if [[ ! -d "${l_extension_dir_specify}" ]]; then
                    mkdir -p "${l_extension_dir_specify}"
                    chown -R "${login_user}" "${l_extension_dir_specify}"
                fi

                case "${l_action,,}" in
                    install|i )
                        local l_result=0
                        if [[ "${login_user}" == 'root' ]]; then
                            "${l_path}" --extensions-dir "${l_extension_dir_specify}" --install-extension "${l_e_name}" --force &> /dev/null
                            [[ -n $("${l_path}" --extensions-dir "${l_extension_dir_specify}" --list-extensions | sed -r -n '/'"${l_e_name}"'/{p}') ]] && l_result=1
                        else
                            sudo -u "${login_user}" "${l_path}" --extensions-dir "${l_extension_dir_specify}" --install-extension "${l_e_name}" --force &> /dev/null
                            [[ -n $(sudo -u "${login_user}" "${l_path}" --extensions-dir "${l_extension_dir_specify}" --list-extensions | sed -r -n '/'"${l_e_name}"'/{p}') ]] && l_result=1
                        fi
                        echo "${l_result}"
                        ;;
                    uninstall|u )
                        if [[ "${login_user}" == 'root' ]]; then

                            "${l_path}" --extensions-dir "${l_extension_dir_specify}" --uninstall-extension "${l_e_name}" 1> /dev/null
                        else
                            sudo -u "${login_user}" "${l_path}" --extensions-dir "${l_extension_dir_specify}" --uninstall-extension "${l_e_name}" 1> /dev/null
                        fi
                        ;;
                    disable|d )
                        if [[ "${login_user}" == 'root' ]]; then

                            "${l_path}" --extensions-dir "${l_extension_dir_specify}" --disable-extension "${l_e_name}" 1> /dev/null
                        else
                            sudo -u "${login_user}" "${l_path}" --extensions-dir "${l_extension_dir_specify}" --disable-extension "${l_e_name}" 1> /dev/null
                        fi
                        ;;
                esac
                ;;
        esac
    fi
}

fn_Local_Version_Detection(){
    local l_install_dir=${1:-"${installation_dir}"}
    local l_bin_path=${2:-"${vscodium_bin_path}"}
    local l_output=${l_output:-}

    if [[ -n "${l_install_dir}" && -d "${l_install_dir}" ]]; then
        local l_app_package_json=${l_app_package_json:-"${installation_dir}/resources/app/package.json"}
        # [[ -f "${l_app_package_json}" ]] && l_output=$(sed -r -n '/"?version"?:/{s@[",]@@g;s@[^:]+:[[:space:]]*([[:digit:].]+).*$@\1@g;p;q}' "${l_app_package_json}")
        # https://github.com/VSCodium/vscodium/releases/tag/1.70.1.22229 add release number
        [[ -f "${l_app_package_json}" ]] && l_output=$(sed -r -n '/"?(version|release)"?:/{s@[",]@@g;s@[^:]+:[[:space:]]*([[:digit:].]+).*$@\1@g;p}' "${l_app_package_json}" | sed 'N;s@\n@.@g')
    fi

    if [[ -z "${l_output}" && -f "${l_bin_path}" ]]; then
        l_output=$(fn_VSCode_CLI_Operation "${l_bin_path}" 'v')
    fi

    echo "${l_output}"
}

fn_VersionComparasion(){
    [[ "${version_check}" -eq 0 ]] && fn_EssentialLibaryCheck

    fnBase_CentralOutputTitle "${software_fullname}"
    fnBase_OperationPhaseStatement 'Version Detection'

    # - Local version
    current_local_version=${current_local_version:-}
    current_local_version=$(fn_Local_Version_Detection)
    [[ -z "${current_local_version}" ]] && is_existed=0

    if [[ "${is_existed}" -eq 1 ]]; then
        fnBase_OperationProcedureStatement 'Local version detecting'
        fnBase_OperationProcedureResult "${current_local_version}"
    fi

    # - Latest online version info
    fnBase_OperationProcedureStatement 'Online version detecting'
    local l_online_release_info=${l_online_release_info:-}
    # relese version | release date | .tar.gz package download link | sha256
    # 1.52.0|2020-12-12T00:40:25Z|https://github.com/VSCodium/vscodium/releases/download/1.52.0/VSCodium-linux-x64-1.52.0.tar.gz|https://github.com/VSCodium/vscodium/releases/download/1.52.0/VSCodium-linux-x64-1.52.0.tar.gz.sha256

    l_online_release_info=$($download_method https://api.github.com/repos/VSCodium/vscodium/releases/latest | sed -r -n '/(tag_name|published_at|browser_download_url)/{/browser_download_url/{/.tar.gz/!d;/linux-x64/!d;/-reh-/d};s@[^:]*:[[:space:]]*"([^"]*)".*@\1@g;p}' | sed ':a;N;$!ba;s@\n@|@g;')

    online_release_version=$(echo "${l_online_release_info}" | cut -d\| -f1)
    [[ -z "${online_release_version}" ]] && fnBase_ExitStatement "${c_red}Sorry${c_normal}: fail to get latest online version!"
    online_release_date=$(echo "${l_online_release_info}" | cut -d\| -f2 | date -f - +"%b %d, %Y" 2>/dev/null)
    # online_release_downloadlink=$(echo "${l_online_release_info}" | cut -d\| -f3)
    online_release_downloadlink=$(echo "${l_online_release_info}" | sed -r -n 's@\|@\n@g;p' | sed -r -n '/linux-x64/!d;/\.sha[[:digit:]]+/d;p')
    local l_online_release_dgst_downloadlink
    # l_online_release_dgst_downloadlink=$(echo "${l_online_release_info}" | cut -d\| -f4)
    l_online_release_dgst_downloadlink=$(echo "${l_online_release_info}" | sed -r -n 's@.*\|(.*?sha[[:digit:]]+).*@\1@g;p')
    online_release_dgst=$($download_method "${l_online_release_dgst_downloadlink}" | cut -d' ' -f1)

    # [[ -z "${online_release_downloadlink}" ]] && fnBase_ExitStatement "${c_red}Sorry${c_normal}: fail to get package download link!"

    if [[ -n "${online_release_version}" ]]; then
        fnBase_OperationProcedureResult "${online_release_version} - ${online_release_date}"
    else
        fnBase_OperationProcedureResult '' 1
    fi

    # - Just online version check
    if [[ "${version_check}" -eq 1 ]]; then
        fnBase_ExitStatement ''
    fi

    # - Online & Local Version Comparasion
    if [[ "${is_existed}" -eq 1 ]]; then
        if [[ "${online_release_version}" == "${current_local_version}" ]]; then
            fnBase_ExitStatement "\nLatest version (${c_red}${online_release_version}${c_normal}) has been existed in your system!"
        elif [[ $(fnBase_VersionNumberComparasion "${current_local_version}" "${online_release_version}" '<') -eq 1 ]]; then
            printf "\nExisted version local (${c_red}%s${c_normal}) < Latest version online (${c_red}%s${c_normal}).\n" "${current_local_version}" "${online_release_version}"
        fi
    # else
    #     printf "No %s find in your system!\n" "${software_fullname}"
    fi
}


#########  2-3. Download & Decompress Latest Software  #########
fn_DownloadAndDecompressOperation(){
    # fnBase_CentralOutputTitle 'Operation Processing, Just Be Patient'
    local l_operation_type=${l_operation_type:-'installing'}
    [[ "${is_existed}" -eq 1 ]] && l_operation_type='updating'
    fnBase_OperationPhaseStatement "${l_operation_type^}"

    local online_release_pack_name="VSCodium-linux-x64-${online_release_version}.tar.gz"

    local l_is_need_download=${l_is_need_download:-1}
    local l_specified_pack_dir=${l_specified_pack_dir:-}

    # specified file path existed
    if [[ "${source_pack_path}" =~ .tar.gz$ && -s "${source_pack_path}" ]]; then
        [[ "${source_pack_path##*/}" == "${online_release_pack_name}" ]] || fnBase_ExitStatement "Package ${c_red}${source_pack_path}${c_normal} is not same to latest release version."

    # specified dir or ~/Downloads existed
    elif [[ (-n "${source_pack_path}" && -d "${source_pack_path}" ) || -d "${user_download_dir}" ]]; then
        if [[ -d "${source_pack_path}" ]]; then
            l_specified_pack_dir="${source_pack_path}"
        elif [[ -d "${user_download_dir}" ]]; then
            l_specified_pack_dir="${user_download_dir}"
        fi

        l_specified_pack_dir="${l_specified_pack_dir%/}"

        if [[ -d "${l_specified_pack_dir}" && -f "${l_specified_pack_dir}/${online_release_pack_name}" ]]; then
            source_pack_path="${l_specified_pack_dir}/${online_release_pack_name}"
        fi
    fi

    # 1.1- existed package path in system
    if [[ -n "${source_pack_path}" && -f "${source_pack_path}" ]]; then
        fnBase_OperationProcedureStatement 'Existed pack dgst verifying'

        if [[ -n "${online_release_dgst}" && $(fnBase_HashDigestVerification "${source_pack_path}" '256') == "${online_release_dgst}" ]]; then
            fnBase_OperationProcedureResult "${c_red}${source_pack_path%/*}/${c_normal}"
            l_is_need_download=0
        else
            fnBase_OperationProcedureResult '' 1
        fi
    fi

    # 2 - Download Directly From Official Site
    local pack_save_path=${pack_save_path:-}
    if [[ "${l_is_need_download}" -eq 1 ]]; then
        fnBase_OperationProcedureStatement "Downloading ${c_red}${online_release_pack_name}${c_normal}"
        pack_save_path="${temp_save_dir}/${online_release_pack_name}"
        [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"

        $download_method "${online_release_downloadlink}" > "${pack_save_path}"

        if [[ -s "${pack_save_path}" ]]; then
            fnBase_OperationProcedureResult
        else
            [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
            fnBase_OperationProcedureResult '' 1
            fnBase_ExitStatement ''
        fi

        fnBase_OperationProcedureStatement "SHA256 dgst verification"
        if [[ -n "${online_release_dgst}" && $(fnBase_HashDigestVerification "${pack_save_path}" '256') == "${online_release_dgst}"  ]]; then
            fnBase_OperationProcedureResult
        else
            [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
            fnBase_OperationProcedureResult '' 1
        fi

        # use variable "source_pack_path" to execute decompress operation, variable "pack_save_path" used for delete temporary download package
        source_pack_path="${pack_save_path}"
    fi

    # 3 - Decompress & Extract
    fnBase_OperationProcedureStatement "Installing"
    local application_backup_path="${installation_dir}${bak_suffix}"
    [[ -d "${application_backup_path}" ]] && rm -rf "${application_backup_path}"

    [[ -d "${installation_dir}" ]] && mv "${installation_dir}" "${application_backup_path}"
    [[ -d "${installation_dir}" ]] || mkdir -p "${installation_dir}"

    tar xf "${source_pack_path}" -C "${installation_dir}" --strip-components=1 2> /dev/null

    # move extension dir from old directory to new directory
    local extension_dir_backup=${extension_dir_backup:-"${application_backup_path}/${extension_dir##*/}"} # /PATH/Extensions/
    [[ -d "${extension_dir_backup}" ]] && mv "${extension_dir_backup}" "${installation_dir}"

    local new_installed_version=${new_installed_version:-}
    new_installed_version=$(fn_Local_Version_Detection)

    if [[ "${online_release_version}" == "${new_installed_version}" ]]; then
        [[ -d "${application_backup_path}" ]] && rm -rf "${application_backup_path}"
        fnBase_OperationProcedureResult "${c_red}${installation_dir%/}/${c_normal}"

        # create soft link to directory /usr/bin
        ln -fs "${vscodium_bin_path}" "${usr_bin_softlink_path}"

        # copy newly downloaded pack to ~/Downloads
        local l_user_download_save_path=${l_user_download_save_path:-"${user_download_dir}/${source_pack_path##*/}"}
        if [[ -d "${user_download_dir}" && ! -f "${l_user_download_save_path}" ]]; then
            cp "${source_pack_path}" "${l_user_download_save_path}"
            chown "${login_user}" "${l_user_download_save_path}"
            chgrp -hR "${login_user}" "${l_user_download_save_path}" 2> /dev/null

            # removed old version package existed
            if [[ "${is_existed}" -eq 1 ]]; then
                local l_existed_old_pack_path=${l_existed_old_pack_path:-"${user_download_dir}/${online_release_pack_name//${online_release_version}/${current_local_version}}"}
                [[ -f "${l_existed_old_pack_path}" ]] && rm -f "${l_existed_old_pack_path}"
            fi
        fi
    else
        [[ -d "${installation_dir}" ]] && rm -rf "${installation_dir}"
        [[ "${is_existed}" -eq 1 ]] && mv "${application_backup_path}" "${installation_dir}"
        fnBase_OperationProcedureResult '' 1
        fnBase_ExitStatement ''
    fi

    # 4 - Extension Gallery Change ('extensionsGallery')
    # Solution https://github.com/VSCodium/vscodium/blob/master/DOCS.md#extensions--marketplace
    # VS Code use marketplace.visualstudio.com
    # VSCodium use open-vsx.org  but it'll miss some extensions  https://github.com/VSCodium/vscodium/issues/519
    # Platform|serviceUrl|itemUrl
    # VSCodium|https://open-vsx.org/vscode/gallery|https://open-vsx.org/vscode/item
    # VS Code|https://marketplace.visualstudio.com/_apis/public/gallery|https://marketplace.visualstudio.com/items
   
    local l_product_json_path="${installation_dir}/resources/app/product.json"

    if [[ -f "${l_product_json_path}" ]]; then
        fnBase_OperationProcedureStatement "Extension Gallery Change"

        local l_serviceUrl='https://marketplace.visualstudio.com/_apis/public/gallery'
        local l_itemUrl='https://marketplace.visualstudio.com/items'

        sed -r -i '/extensionsGallery/,/}/{/"serviceUrl"/{s@^(.*?)http[^"]+(.*?)@\1'"${l_serviceUrl}"'\2@g}; /"itemUrl"/{s@^(.*?)http[^"]+(.*?)@\1'"${l_itemUrl}"'\2@g}}' "${l_product_json_path}" 2> /dev/null
        fnBase_OperationProcedureResult 'open-vsx.org -> marketplace.visualstudio.com'
    fi

    chown -R root:root "${installation_dir}"

    find "${extension_dir}" -maxdepth 1 -type d ! -path "${extension_dir}" -print  2> /dev/null | while read -r line; do
        # directory name is same to specific normal user name
        [[ -n "${line}" && -d "${line}" ]] && chown -R "${line##*/}" "${line}" 2> /dev/null
    done

    find "${installation_dir}" -maxdepth 1 -type f -name "*.so" -exec chmod 644 {} \; 2> /dev/null
    find "${installation_dir}" -maxdepth 1 -type f -name "*.so.*" -exec chmod 644 {} \; 2> /dev/null  # libvulkan.so.1  libonnxruntime.so.1.7.0

    #  The SUID sandbox helper binary was found, but is not configured correctly. Rather than run without sandboxing I'm aborting now. You need to make sure that /opt/VSCodium/chrome-sandbox is owned by root and has mode 4755.
    [[ -f "${installation_dir}/chrome-sandbox" ]] && chmod 4755 "${installation_dir}/chrome-sandbox"

    [[ -n "${pack_save_path}" && -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"

    # clean cached files in config dirs to save space
    local l_config_dir="${login_user_home}/.config/VSCodium/"
    if [[ -d "${l_config_dir}/" ]]; then
        rm -rf "${l_config_dir}/"{logs,Cache,CachedExtensionVSIXs,CachedData}/*
        rm -rf "${l_config_dir}/exthost Crash Reports/"
        rm -f "${l_config_dir}/machineid"
    fi
}

#########  2-4. Extension Operation  #########
fn_ExtensionInstallation(){
    local l_bin_path=${1:-"${vscodium_bin_path}"}
    local l_e_name=${2:-}
    local l_e_fullname=${3:-}

    fnBase_OperationProcedureStatement "${l_e_name}"

    if [[ $(fn_VSCode_CLI_Operation "${l_bin_path}" 'e' "${l_e_fullname}" 'install') -eq 1 ]]; then
        fnBase_OperationProcedureResult
    else
        fnBase_OperationProcedureResult '' 1
    fi
}

fn_ExtensionAndEditorSettingOperation(){
    local l_is_install_extension=${l_is_install_extension:-0}
    [[ ("${is_existed}" -ne 1 && "${customize_setting}" -eq 1) || (-d "${extension_dir}" && ! -d "${extension_dir}/${login_user}") ]] && l_is_install_extension=1

    if [[ "${l_is_install_extension}" -eq 1 ]]; then
        fnBase_OperationPhaseStatement 'Extensions Installation'
        # https://code.visualstudio.com/docs/editor/extension-gallery
        # https://code.visualstudio.com/docs/editor/command-line#_working-with-extensions

        # [DEP0005] DeprecationWarning: Buffer() is deprecated due to security and usability issues. Please use the Buffer.alloc(), Buffer.allocUnsafe(), or Buffer.from() methods instead.

        local l_extension_list=''
        l_extension_list=$($download_method "${custom_extension_list}" 2> /dev/null | sed -r -n '/(^#|^$)/d;p')

        local l_theme_name=''
        local l_icon_name=''

        if [[ -n "${l_extension_list}" ]]; then
            echo "${l_extension_list}" | while IFS="|" read -r ext_fullname ext_instll_path setting_name; do
                fn_ExtensionInstallation "${vscodium_bin_path}" "${ext_fullname}" "${ext_instll_path}"
            done

            l_theme_name=$(echo "${l_extension_list}" | sed -r -n '/theme/{p;q}' | cut -d\| -f3)
            l_icon_name=$(echo "${l_extension_list}" | sed -r -n '/icons/{p;q}' | cut -d\| -f3)
        fi
    fi

    fnBase_OperationPhaseStatement 'Configuration'

    if [[ "${is_existed}" -ne 1 && "${customize_setting}" -eq 1 ]]; then
        fnBase_OperationProcedureStatement 'Perference Setting'

        local l_config_dir="${login_user_home}/.config/VSCodium"
        local l_config_user_path="${l_config_dir}/User"
        [[ -d "${l_config_user_path}" ]] || mkdir -p "${l_config_user_path}"

        chown -R "${login_user}" "${l_config_dir}"
        chgrp -hR "${login_user}" "${l_config_dir}" 2> /dev/null

        # sed ':a;N;$!ba;s@\n@\\n@g;s@"@\\&@g' $HOME/.config/VSCodium/User/settings.json

        # https://code.visualstudio.com/docs/python/linting
        # https://code.visualstudio.com/docs/getstarted/tips-and-tricks#_customization
        local l_settings_json_path="${l_config_user_path}/settings.json"
        $download_method "${custom_settings_json}" > "${l_settings_json_path}"
        sed -r -i '/workbench\.colorTheme/{s@^(.*:[[:space:]]*")[^"]+(".*)$@\1'"${l_theme_name}"'\2@g}' "${l_settings_json_path}"
        sed -r -i '/workbench\.iconTheme/{s@^(.*:[[:space:]]*")[^"]+(".*)$@\1'"${l_icon_name}"'\2@g}' "${l_settings_json_path}"

        chown -R "${login_user}" "${l_config_user_path}"
        chgrp -hR "${login_user}" "${l_config_user_path}" &> /dev/null

        fnBase_OperationProcedureResult "${l_config_user_path}/settings.json"
    fi
}


#########  2-5. Desktop Configuration  #########
fn_DesktopConfiguration(){
    if [[ -d '/usr/share/applications' ]]; then
        fnBase_OperationProcedureStatement 'Desktop Configuration'
        [[ -f "${installation_dir}/resources/app/resources/linux/code.png" ]] && ln -sf "${installation_dir}/resources/app/resources/linux/code.png" "${pixmaps_png_path}"

        echo -e "[Desktop Entry]\nVersion=1.0\nType=Application\nGenericName[en]=Text Editor\nName=VSCodium\nComment=VSCodium - The advanced editor\nIcon=${pixmaps_png_path}\nExec=/bin/bash -c \"login_user_extension_dir=${extension_dir}/\$USER; if [[ ! -d \${login_user_extension_dir} ]]; then sudo mkdir -p \${login_user_extension_dir}; sudo chown -R \$USER \${login_user_extension_dir}; fi; ${vscodium_bin_path} --extensions-dir \${login_user_extension_dir}\"\nComment=The Drive to Develop\nCategories=Development;IDE;\nTerminal=false" > "${application_desktop_path}"
        #echo -e "[Desktop Entry]\nVersion=1.0\nType=Application\nName=VSCodium\nComment=VSCodium - The advanced editor\nIcon=${pixmaps_png_path}\nExec=/bin/bash -c \"${vscodium_bin_path} --extensions-dir ${extension_dir}/\$USER\"\nComment=The Drive to Develop\nCategories=Development;IDE;\nTerminal=false" > "${application_desktop_path}"

        fnBase_OperationProcedureResult "${application_desktop_path}"
    fi
}


#########  2-6. Operation Time Cost  #########
fn_TotalTimeCosting(){
    finish_time=$(date +'%s')        # End Time Of Operation
    total_time_cost=$((finish_time-start_time))   # Total Time Of Operation
    printf "\nTotal time cost is ${c_red}%s${c_normal} seconds!\n" "${total_time_cost}"
}


#########  3. Executing Process  #########
fn_InitializationCheck

fn_UninstallOperation
fn_VersionComparasion
fn_DownloadAndDecompressOperation
fn_ExtensionAndEditorSettingOperation
fn_DesktopConfiguration
fn_TotalTimeCosting


#########  4. EXIT Singal Processing  #########
# trap "commands" EXIT # execute command when exit from shell
fn_TrapEXIT(){
    fnBase_UnsetVars
    unset installation_dir
    unset is_existed
    unset version_check
    unset source_pack_path
    unset customize_setting
    unset is_uninstall
    unset proxy_server_specify
}

trap fn_TrapEXIT EXIT


# Script End
