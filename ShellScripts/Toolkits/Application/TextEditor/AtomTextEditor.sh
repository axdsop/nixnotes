#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails


# Official Site:
# - https://atom.io
# - https://github.com/atom/atom/releases
# - https://atom.io/docs
# - https://flight-manual.atom.io/getting-started/sections/installing-atom/

# Keyboard Shortcut overriding (Emmet and Markdown Preview)
# https://discuss.atom.io/t/keyboard-shortcut-overriding-emmet-and-markdown-preview/14113


# Target: Automatically Install & Update Atom Text Editor On GNU/Linux
# Developer: MaxdSre

# Change Log:
# - Oct 08, 2020 09:01 Thu ET - initialization check method update
# - Apr 18, 2020 15:18 Sat ET - optimize essential shared libaries checking
# - Nov 05, 2019 09:53 Tue ET - add package remove under /tmp after copyt to home directory
# - Oct 08, 2019 16:45 Tue ET - change to new base functions
# - Jun 15, 2019 10:30 Sat ET - support Arch Linux, add shared libary check, add local version check function
# - Dec 30, 2018 11:36 Sun ET - code reconfiguration, include base funcions from other file
# - May 21, 2018 13:23 Mon ET - code reconfiguration
# - Nov 28, 2017 17:26 Tue +0800
# - Nov 20, 2017 09:08 Mon +0800
# - Sep 21, 2017 17:45 Thu +0800



#########  0-1. Singal Setting  #########
trap fnBase_TrapCleanUp INT QUIT

#########  0-2. Variables Setting  #########
# umask 022    # temporarily change umask value to 022
readonly official_site='https://atom.io'
readonly software_fullname='Atom Text Editor'
readonly application_name='AtomTextEditor'
installation_dir="/opt/${application_name}"
readonly pixmaps_png_path="/usr/share/pixmaps/${application_name}.png"
readonly application_desktop_path="/usr/share/applications/${application_name}.desktop"
is_existed=${is_existed:-1}   # Default value is 1， assume system has installed Atom Text Editor

version_check=${version_check:-0}
source_pack_path=${source_pack_path:-}
is_uninstall=${is_uninstall:-0}
proxy_server_specify=${proxy_server_specify:-}



#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | sudo bash -s -- [options] ...

Installing / Updating Atom Text Editor On GNU/Linux!
This script requires superuser privileges (eg. root, su).

[available option]
    -h    --help, show help info
    -c    --check, check current stable release version
    -f pack_path    --specify installation package path (e.g. /tmp/atom-amd64)
    -p [protocol:]ip:port    --proxy host (http|https|socks4|socks5), default protocol is http
    -u    --uninstall, uninstall software installed
\e[0m"
}

while getopts "hcf:up:" option "$@"; do
    case "$option" in
        c ) version_check=1 ;;
        f ) source_pack_path="$OPTARG" ;;
        u ) is_uninstall=1 ;;
        p ) proxy_server_specify="$OPTARG" ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done


#########  1-2 Preparation Operation  #########
fn_InitializationCheck(){
    local l_user_codebase_dir="$HOME/.config/axdsop"
    local l_base_function_temp_save_path=''
    local l_need_remove=0
    local l_base_function_url='https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/base_functions.sh'
    l_base_function_temp_save_path=$(find "${l_user_codebase_dir}" -type f -name "${l_base_function_url##*/}" -print 2> /dev/null)
    
    if [[ ! -f "${l_base_function_temp_save_path}" ]]; then
        local l_download_tool=${l_download_tool:-'wget -qO-'}
        [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_tool='curl -fsL'
        l_need_remove=1
        l_base_function_temp_save_path=$(mktemp -t XXXXXXX)
        ${l_download_tool} "${l_base_function_url}" > "${l_base_function_temp_save_path}"
    fi

    if [[ -f "${l_base_function_temp_save_path}" && -s "${l_base_function_temp_save_path}" ]]; then
        . "${l_base_function_temp_save_path}"
        [[ "${l_need_remove}" -eq 1 ]] && rm "${l_base_function_temp_save_path}"
    else
        echo "Sorry, fail to get base script path."
        exit
    fi

    # tput clear  # Echo the clear-screen sequence for the current terminal.
    # specify 1 means need sudo privilege
    fnBase_RunningEnvironmentCheck '1'

    fnBase_CommandExistCheckPhase 'gzip' # CentOS/Debian/OpenSUSE: gzip
    fnBase_CommandExistCheckPhase 'tar' # .tar.gz

    atom_config_path=${atom_config_path:-"${login_user_home}/.atom"}
}


#########  2-1. Uninstall  #########
fn_UninstallOperation(){
    if [[ "${is_uninstall}" -eq 1 ]]; then
        if [[ -d "${installation_dir}" ]]; then
            [[ -d "${installation_dir}" ]] && rm -rf "${installation_dir}"
            [[ -d "${installation_dir}${bak_suffix}" ]] && rm -rf "${installation_dir}${bak_suffix}"

            [[ -f "${pixmaps_png_path}" ]] && rm -f "${pixmaps_png_path}"
            [[ -f "${application_desktop_path}" ]] && rm -f "${application_desktop_path}"

            [[ -d "${atom_config_path}" ]] && rm -rf "${atom_config_path}"    # ~/.atom

            [[ -d "${installation_dir}" ]] || fnBase_ExitStatement "${software_fullname} is successfully removed from your system!"
        else
            fnBase_ExitStatement "${c_blue}Note${c_normal}: no ${software_fullname} is found in your system!"
        fi
    fi
}


#########  2-2. Latest & Local Version Check  #########
fn_EssentialLibaryCheck(){
    # 1 - error while loading shared libraries: libgconf-2.so.4: cannot open shared object file: No such file or directory
    # apt|libgconf2-4|/usr/lib/x86_64-linux-gnu/libgconf-2.so.4
    # zypper|gconf2|/usr/lib64/libgconf-2.so.4
    # yum|GConf2|/usr/lib64/libgconf-2.so.4
    # pacman|gconf|/usr/lib64/libgconf-2.so.4

    # updatedb &> /dev/null; locate -c libgconf-2.so.4
    [[ -z $(find /usr/lib* -name 'libgconf-2.so.4' -print 2>/dev/null) ]] && fnBase_ExitStatement "${c_red}Fatal Error${c_normal}: atom need shared libraries ${c_blue}libgconf-2.so.4${c_normal} (apt: libgconf2-4, pacman: gconf, zypper: gconf2, yum: GConf2)!"

    # 2 - error while loading shared libraries: libXss.so.1: cannot open shared object file: No such file or directory
    # apt|libxss1|/usr/lib/x86_64-linux-gnu/libXss.so.1
    # zypper | libXss1 | /usr/lib64/libXss.so.1
    # yum | libXScrnSaver | /usr/lib64/libXss.so.1
    # pacman | libxss | /usr/lib64/libXss.so.1

    [[ -z $(find /usr/lib* -name 'libXss.so.1' -print 2>/dev/null) ]] && fnBase_ExitStatement "${c_red}Fatal Error${c_normal}: atom need shared libraries ${c_blue}libXss.so.1${c_normal} (apt: libxss1, pacman: libxss, zypper: libXss1, yum: libXScrnSaver)!"

    # 3 - error while loading shared libraries: libasound.so.2: cannot open shared object file: No such file or directory
    # apt|libasound2|/usr/lib/x86_64-linux-gnu/libasound.so.2
    # pacman | alsa-lib | /usr/lib64/libasound.so.2
}

fn_UtilityLocalVersionDetection(){
    local l_path="${1:-}" # atom install directory
    # local l_action=${2:-'--version'}
    local l_output=${l_output:-}

    # https://wiki.archlinux.org/index.php/Running_GUI_applications_as_root
    # No protocol specified
    # (atom:6681): Gtk-WARNING **: 10:46:21.805: cannot open display: :0

    [[ -f "${l_path}/resources/LICENSE.md" ]] && l_output=$(sed -r -n '/^Package:.*atom@/{s@^[^[:digit:]]+@@g;p}' "${l_path}/resources/LICENSE.md")

    if [[ -z "${l_output}" ]]; then
        if [[ -f "${installation_dir}/atom" ]]; then
            l_output=$(sudo -u "${login_user}" "${installation_dir}/atom" --version 2>/dev/null | sed -r -n '/^Atom/{s@^[^:]+:[[:space:]]*([[:digit:].]+).*$@\1@g;p}')
        elif fnBase_CommandExistIfCheck 'atom'; then
            l_output=$(sudo -u "${login_user}" atom --version 2>/dev/null | sed -r -n '/^Atom/{s@^[^:]+:[[:space:]]*([[:digit:].]+).*$@\1@g;p}')
        fi
    fi
    echo "${l_output}"
}

fn_VersionComparasion(){
    [[ "${version_check}" -eq 0 ]] && fn_EssentialLibaryCheck

    fnBase_CentralOutputTitle "${software_fullname}"
    fnBase_OperationPhaseStatement 'Version Detection'

    # - Local version
    current_local_version=${current_local_version:-}

    current_local_version=$(fn_UtilityLocalVersionDetection "${installation_dir}")
    [[ -z "${current_local_version}" ]] && is_existed=0

    if [[ "${is_existed}" -eq 1 ]]; then
        fnBase_OperationProcedureStatement 'Local version detecting'
        fnBase_OperationProcedureResult "${current_local_version}"
    fi

    # - Latest online version info
    fnBase_OperationProcedureStatement 'Online version detecting'
    # https://developer.github.com/v3/repos/releases/#get-the-latest-release
    local l_online_release_info=${l_online_release_info:-}
    # relese version | release date | .tar.gz package download link
    # v1.35.1|2019-03-13T15:36:14Z|https://github.com/atom/atom/releases/download/v1.35.1/atom-amd64.tar.gz

    l_online_release_info=$($download_method https://api.github.com/repos/atom/atom/releases/latest 2> /dev/null | sed -r -n '/(tag_name|published_at|browser_download_url)/{/browser_download_url/{/.tar.gz/!d;/amd64/!d};s@[^:]*:[[:space:]]*"([^"]*)".*@\1@g;p}' | sed ':a;N;$!ba;s@\n@|@g;')

    online_release_version=$(echo "${l_online_release_info}" | sed -r -n 's@^([^|]+)\|.*@\1@g;s@^[^[:digit:]]*@@g;p')
    [[ -z "${online_release_version}" ]] && fnBase_ExitStatement "${c_red}Sorry${c_normal}: fail to get latest online version!"

    online_release_date=$(echo "${l_online_release_info}" | cut -d\| -f2 | date -f - +"%b %d, %Y" 2>/dev/null)
    online_release_downloadlink=$(echo "${l_online_release_info}" | cut -d\| -f3)
    # [[ -z "${online_release_downloadlink}" ]] && fnBase_ExitStatement "${c_red}Sorry${c_normal}: fail to get package download link!"

    if [[ -n "${online_release_version}" ]]; then
        fnBase_OperationProcedureResult "${online_release_version} - ${online_release_date}"
    else
        fnBase_OperationProcedureResult '' 1
    fi

    # - Just online version check
    if [[ "${version_check}" -eq 1 ]]; then
        fnBase_ExitStatement ''

        # if [[ "${is_existed}" -eq 1 ]]; then
        #     fnBase_ExitStatement "Local existed version is ${c_red}${current_local_version}${c_normal}.\nLatest version online is ${c_red}${online_release_version}${c_normal} (${c_yellow}${online_release_date}${c_normal})."
        # else
        #     fnBase_ExitStatement "Latest version online (${c_red}${online_release_version}${c_normal}), Release date ($c_red${online_release_date}$c_normal)."
        # fi
    fi

    # - Online & Local Version Comparasion
    if [[ "${is_existed}" -eq 1 ]]; then
        if [[ "${online_release_version}" == "${current_local_version}" ]]; then
            fnBase_ExitStatement "\nLatest version (${c_red}${online_release_version}${c_normal}) has been existed in your system!"
        elif [[ $(fnBase_VersionNumberComparasion "${current_local_version}" "${online_release_version}" '<') -eq 1 ]]; then
            printf "\nExisted version local (${c_red}%s${c_normal}) < Latest version online (${c_red}%s${c_normal}).\n" "${current_local_version}" "${online_release_version}"
        fi
    else
        # printf "No %s find in your system!\n" "${software_fullname}"

        # Install Essential Packages For Debian/Ubuntu
        if [[ -f /etc/debian_version ]]; then
            if fnBase_CommandExistIfCheck 'apt'; then
                # https://github.com/atom/atom#archive-extraction
                local l_pack_list
                l_pack_list=$($download_method https://github.com/atom/atom | sed -r -n '/Install dependencies/{s@.*install[[:space:]]*([^<]*)<.*@\1@g;p}')
                [[ -z "${l_pack_list}" ]] && l_pack_list='git gconf2 gconf-service libgtk2.0-0 libudev1 libgcrypt20 libnotify4 libxtst6 libnss3 python gvfs-bin xdg-utils libcap2'
                # shellcheck disable=SC2086
                apt-get install ${l_pack_list} -yq &> /dev/null
            fi
        fi

    fi
}


#########  2-3. Download & Decompress Latest Software  #########
fn_DownloadAndDecompressOperation(){
    # fnBase_CentralOutputTitle 'Operation Processing, Just Be Patient'
    local l_operation_type=${l_operation_type:-'installing'}
    [[ "${is_existed}" -eq 1 ]] && l_operation_type='updating'
    fnBase_OperationPhaseStatement "${l_operation_type^}"

    local l_package_path=${l_package_path:-}
    local pack_save_path=${pack_save_path:-}

    # https://github.com/atom/atom/releases/download/v${online_release_version}/atom-amd64.tar.gz
    local l_online_release_pack_name="${online_release_downloadlink##*/}"
    # atom-1.33.1-amd64.tar.gz
    local online_release_pack_name="atom-${online_release_version}-adm64.${l_online_release_pack_name#*.}"

    local l_is_need_download=${l_is_need_download:-1}
    local l_specified_pack_dir=${l_specified_pack_dir:-}

    # specified file path existed
    if [[ "${source_pack_path}" =~ .tar.gz$ && -s "${source_pack_path}" ]]; then
        [[ "${source_pack_path##*/}" == "${online_release_pack_name}" ]] || fnBase_ExitStatement "Package ${c_red}${source_pack_path}${c_normal} is not same to latest release version."

    # specified dir or ~/Downloads existed
    elif [[ (-n "${source_pack_path}" && -d "${source_pack_path}" ) || -d "${user_download_dir}" ]]; then
        if [[ -d "${source_pack_path}" ]]; then
            l_specified_pack_dir="${source_pack_path}"
        elif [[ -d "${user_download_dir}" ]]; then
            l_specified_pack_dir="${user_download_dir}"
        fi

        l_specified_pack_dir="${l_specified_pack_dir%/}"

        if [[ -d "${l_specified_pack_dir}" && -f "${l_specified_pack_dir}/${online_release_pack_name}" ]]; then
            source_pack_path="${l_specified_pack_dir}/${online_release_pack_name}"
        fi
    fi

    # 1.1- existed package path in system
    # Atom doesn't provide package hash digest.
    if [[ -n "${source_pack_path}" && -f "${source_pack_path}" ]]; then
        # fnBase_OperationProcedureStatement 'Existed pack dgst verifying'
        # if [[ -n "${online_release_dgst}" && $(fnBase_HashDigestVerification "${source_pack_path}" '256') == "${online_release_dgst}" ]]; then
        #     fnBase_OperationProcedureResult "${c_red}${source_pack_path%/*}/${c_normal}"
        #     l_is_need_download=0
        # else
        #     fnBase_OperationProcedureResult '' 1
        # fi

        l_is_need_download=0
        fnBase_OperationProcedureStatement 'Existed pack find'
        fnBase_OperationProcedureResult "${source_pack_path%/*}"
    fi
    [[ -f "${source_pack_path}" && -n "${source_pack_path}" ]] && l_is_need_download=0

    # 2 - Download Directly From Official Site
    local pack_save_path=${pack_save_path:-}
    if [[ "${l_is_need_download}" -eq 1 ]]; then
        fnBase_OperationProcedureStatement "Downloading ${c_red}${online_release_pack_name}${c_normal}"
        pack_save_path="${temp_save_dir}/${online_release_pack_name}"
        [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"

        $download_method "${online_release_downloadlink}" > "${pack_save_path}"

        if [[ -s "${pack_save_path}" ]]; then
            fnBase_OperationProcedureResult
        else
            [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
            fnBase_OperationProcedureResult '' 1
        fi

        # fnBase_OperationProcedureStatement "SHA256 dgst verification"
        # if [[ -n "${online_release_dgst}" && $(fnBase_HashDigestVerification "${pack_save_path}" '256') == "${online_release_dgst}"  ]]; then
        #     fnBase_OperationProcedureResult
        # else
        #     [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
        #     fnBase_OperationProcedureResult '' 1
        # fi

        # use variable "source_pack_path" to execute decompress operation, variable "pack_save_path" used for delete temporary download package
        source_pack_path="${pack_save_path}"
    fi

    # 3 - Decompress & Extract
    fnBase_OperationProcedureStatement "Installing"
    local application_backup_path="${installation_dir}${bak_suffix}"
    [[ -d "${application_backup_path}" ]] && rm -rf "${application_backup_path}"

    [[ -d "${installation_dir}" ]] && mv "${installation_dir}" "${application_backup_path}"
    [[ -d "${installation_dir}" ]] || mkdir -p "${installation_dir}"
    tar xf "${source_pack_path}" -C "${installation_dir}" --strip-components=1 2> /dev/null

    local new_installed_version=${new_installed_version:-}
    new_installed_version=$(fn_UtilityLocalVersionDetection "${installation_dir}")

    if [[ "${online_release_version}" == "${new_installed_version}" ]]; then
        # libffmpeg.so  libnode.so
        find "${installation_dir}" -type f -name '*.so' -exec chmod 644 {} \;
        chown -R root:root "${installation_dir}"

        [[ -f "${pixmaps_png_path}" ]] && rm -f "${pixmaps_png_path}"
        [[ -f "${application_desktop_path}" ]] && rm -f "${application_desktop_path}"
        [[ -d "${application_backup_path}" ]] && rm -rf "${application_backup_path}"
        fnBase_OperationProcedureResult "${c_red}${installation_dir%/}/${c_normal}"

        local l_user_download_save_path=${l_user_download_save_path:-"${user_download_dir}/${source_pack_path##*/}"}
        if [[ -d "${user_download_dir}" && ! -f "${l_user_download_save_path}" ]]; then
            cp "${source_pack_path}" "${l_user_download_save_path}"
            chown "${login_user}" "${l_user_download_save_path}"
            chgrp -hR "${login_user}" "${l_user_download_save_path}" 2> /dev/null

            # removed old version package existed
            if [[ "${is_existed}" -eq 1 ]]; then
                local l_existed_old_pack_path=${l_existed_old_pack_path:-"${user_download_dir}/${online_release_pack_name//${online_release_version}/${current_local_version}}"}
                [[ -f "${l_existed_old_pack_path}" ]] && rm -f "${l_existed_old_pack_path}"
            fi
        fi
    else
        [[ -d "${installation_dir}" ]] && rm -rf "${installation_dir}"

        if [[ "${is_existed}" -eq 1 ]]; then
            mv "${application_backup_path}" "${installation_dir}"
            fnBase_OperationProcedureResult 'update' 1
        else
            fnBase_OperationProcedureResult 'install' 1
        fi
    fi

    chown -R root:root "${installation_dir}"
    [[ -n "${pack_save_path}" && -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
}


#########  2-4. Post-installation Configuration  #########
fn_PostInstallationConfiguration(){
    local l_apm_path=${l_apm_path:-"${installation_dir}/resources/app/apm/bin/apm"}

    # 1 - Plugin Installation
    if [[ "${is_existed}" -ne 1 ]]; then
        fnBase_OperationPhaseStatement "Plugin Installation"
        [[ -d "${atom_config_path}" ]] || mkdir -p "${atom_config_path}"
        local l_pack_install_dir=${l_pack_install_dir:-"${atom_config_path}/packages"}
        [[ -d "${l_pack_install_dir}" ]] || mkdir -p "${l_pack_install_dir}"
        chown -R "${login_user}" "${atom_config_path}"
        local l_packs_list_path=${l_packs_list_path:-"${atom_config_path}/package.txt"}
        echo -e "atom-bootstrap4\natom-runner\nautocomplete-python\nemmet\nfile-icons\nhighlight-selected\nminimap\nplatformio-ide-terminal\npython-autopep8\npython-tools" > "${l_packs_list_path}"
		# go-plus
        # ide-rust

        plugin_list_path=$(mktemp -t "${mktemp_format}")
        # chown "${login_user}" "${plugin_list_path}"
        chmod 664 "${plugin_list_path}"

        while read -r plugin_name; do
            fnBase_OperationProcedureStatement "Plugin ${plugin_name}"
            echo "${plugin_name}" > "${plugin_list_path}"
            result_output=${result_output:-}

            if [[ "${login_user}" == 'root' ]]; then
               result_output=$("${l_apm_path}" install --packages-file "${plugin_list_path}" 2> /dev/null)
            else
               result_output=$(sudo -u "${login_user}" "${l_apm_path}" install --packages-file "${plugin_list_path}" 2> /dev/null)
            fi

            if [[ -n "${result_output}" ]]; then
               fnBase_OperationProcedureResult
            else
               fnBase_OperationProcedureResult '' 1
            fi

        done < "${l_packs_list_path}"
    fi

    fnBase_OperationPhaseStatement "Configuration"

    # 2 - User References
    if [[ "${is_existed}" -ne 1 ]]; then
        fnBase_OperationProcedureStatement 'User References'
        # sed -r -n ':a;N;$!ba;s@"@\\"@g;s@\n@\\n@g;p'
        echo -e "\"*\":\n  \"autocomplete-python\":\n    enableTouchBar: true\n    showTooltips: true\n    useSnippets: \"all\"\n  core:\n    autoHideMenuBar: true\n    disabledPackages: [\n      \"github\"\n      \"wrap-guide\"\n      \"atom-bootstrap4\"\n      \"go-plus\"\n      \"emmet\"\n    ]\n    telemetryConsent: \"no\"\n  editor:\n    fontSize: 18\n    showIndentGuide: true\n    softWrap: true\n    tabLength: 4\n  \"exception-reporting\":\n    userId: \"816448ac-4595-40c1-82f8-840b23b640df\"\n  \"go-plus\":\n    config:\n      compileOnSave: false\n    test: {}\n  \"platformio-ide-terminal\":\n    core:\n      mapTerminalsTo: \"File\"\n      shell: \"/bin/bash\"\n      workingDirectory: \"Active File\"\n    style:\n      fontSize: \"18\"\n      theme: \"homebrew\"\n    toggles:\n      autoClose: true\n  \"tree-view\":\n    hideVcsIgnoredFiles: true\n  welcome:\n    showOnStartup: false" > "${atom_config_path}/config.cson"
        chown -R "${login_user}" "${atom_config_path}"
        fnBase_OperationProcedureResult "${atom_config_path}/config.cson"
    fi

    # 3 - Executing PATH Configuration
    fnBase_OperationProcedureStatement 'Executing PATH'
    # create dir 'custom_bin' to save soft link about atom, apm
    local custom_bin_path=${custom_bin_path:-"${installation_dir}/custom_bin"}
    [[ -d "${custom_bin_path}" ]] || mkdir -p "${custom_bin_path}"
    local l_atom_path=${l_atom_path:-"${installation_dir}/atom"}
    [[ -s "${l_atom_path}" ]] && ln -sf "${l_atom_path}" "${custom_bin_path}"

    [[ -s "${l_apm_path}" ]] && ln -sf "${l_apm_path}" "${custom_bin_path}"
    # add to PATH execution path
    local l_execute_path=${l_execute_path:-"/etc/profile.d/${application_name}.sh"}
    echo "export PATH=${custom_bin_path}:\$PATH" > "${l_execute_path}"
    # source "${execute_path}" 1> /dev/null
    fnBase_OperationProcedureResult "${l_execute_path}"
}


#########  2-5. Desktop Configuration  #########
fn_DesktopConfiguration(){
    if [[ -d '/usr/share/applications' ]]; then
        fnBase_OperationProcedureStatement 'Desktop Configuration'
        [[ -f "${installation_dir}/atom.png" ]] && ln -sf "${installation_dir}/atom.png" "${pixmaps_png_path}"

        echo -e "[Desktop Entry]\nName=${software_fullname}\nComment=A hackable text editor for the 21st Century.\nGenericName[en]=Text Editor\nExec=installation_dir/atom %F\nIcon=application_name.png\nType=Application\nStartupNotify=true\nCategories=GNOME;GTK;Utility;TextEditor;Development;\nMimeType=text/plain;" > "${application_desktop_path}"

        sed -i -r 's@application_name@'"${application_name}"'@g' "${application_desktop_path}"
        sed -i -r 's@installation_dir@'"${installation_dir}"'@g' "${application_desktop_path}"
        fnBase_OperationProcedureResult "${application_desktop_path}"
    fi

    # if [[ "$is_existed" -eq 1 ]]; then
    #     printf "\n%s was updated to version ${c_red}%s${c_normal} successfully!\n" "${software_fullname}" "${online_release_version}"
    # else
    #     printf "\nInstalling %s version ${c_red}%s${c_normal} successfully!\n" "${software_fullname}" "${online_release_version}"
    # fi
}


#########  2-6. Operation Time Cost  #########
fn_TotalTimeCosting(){
    finish_time=$(date +'%s')        # End Time Of Operation
    total_time_cost=$((finish_time-start_time))   # Total Time Of Operation
    printf "\nTotal time cost is ${c_red}%s${c_normal} seconds!\n" "${total_time_cost}"
}



#########  3. Executing Process  #########
fn_InitializationCheck

fn_UninstallOperation
fn_VersionComparasion
fn_DownloadAndDecompressOperation
fn_PostInstallationConfiguration
fn_DesktopConfiguration
fn_TotalTimeCosting


#########  4. EXIT Singal Processing  #########
# trap "commands" EXIT # execute command when exit from shell
fn_TrapEXIT(){
    fnBase_UnsetVars
    unset installation_dir
    unset is_existed
    unset version_check
    unset source_pack_path
    unset is_uninstall
    unset proxy_server_specify
    unset update_operation
    unset atom_config_path
}

trap fn_TrapEXIT EXIT


# Script End
