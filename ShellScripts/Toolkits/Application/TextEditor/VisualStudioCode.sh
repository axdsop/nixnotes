#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails


# Official Site: https://code.visualstudio.com
# Git: https://github.com/Microsoft/vscode
# Documentation
# - https://code.visualstudio.com/docs
# - https://code.visualstudio.com/docs/editor/command-line
# - https://code.visualstudio.com/docs/getstarted/tips-and-tricks
# - https://code.visualstudio.com/docs/getstarted/tips-and-tricks#_keyboard-reference-sheets
# - https://code.visualstudio.com/docs/getstarted/themes
# Theme Preview
# - https://vscodethemes.com/
# awesome-vscode
# - https://viatsko.github.io/awesome-vscode/


# Target: Automatically Install & Update Visual Studio Code On GNU/Linux
# Developer: MaxdSre

# Change Log:
# - Oct 22, 2020 12:48 Thu ET - stable version default extension directory change (~/.vscode-oss to ~/.vscode)
# - Oct 08, 2020 09:03 Thu ET - initialization check method update
# - May 02, 2020 08:50 Sat ET - add software soft link into /usr/bin
# - Apr 18, 2020 15:18 Sat ET - optimize essential shared libaries checking
# - Oct 19, 2019 20:12 Sat ET - add customize setting switch via 'customize_setting'
# - Oct 18, 2019 21:29 Fri ET - seperate extension list, settings.json as single file
# - Oct 18, 2019 16:10 Fri ET - add Rust extension install, configuration finle settings.json update
# - Oct 08, 2019 16:52 Tue ET - change to new base functions
# - May 18, 2019 17:08 Sat ET - script optimization for version choose
# - May 16, 2019 17:04 Thu ET - add insider version support (stable/insider)
# - Jan 01, 2019 17:32 Tue ET - code reconfiguration, include base funcions from other file
# - Dec 24, 2018 15:53 Mon ET

# You are trying to start vscode as a super user which is not recommended. If you really want to, you must specify an alternate user data directory using the --user-data-dir argument.

# Node.js Debug port
# Find listening TCP port:     /opt/VisualStudioCode/code --nolazy --inspect=20913 /opt/VisualStudioCode/resources/app/out/bootstrap-fork --type=extensionHost

# Node.js debugging in VS Code  https://code.visualstudio.com/docs/nodejs/nodejs-debugging

# The port 9229 is the default debug port of the --inspect and --inspect-brk options. To use a different port (e.g. 12345), add it to the options like this: --inspect=12345 and --inspect-brk=12345 and change the port attribute in the launch configuration accordingly.


#########  0-1. Singal Setting  #########
trap fnBase_TrapCleanUp INT QUIT

#########  0-2. Variables Setting  #########
# umask 022    # temporarily change umask value to 022
readonly official_site='https://code.visualstudio.com/'
readonly software_fullname='Visual Studio Code'
readonly application_name='VisualStudioCode'
installation_dir="/opt/${application_name}"
readonly vscode_bin_dir="${installation_dir}/bin"
# version_info_path=${version_info_path:-"${installation_dir}/version"}
readonly usr_bin_softlink_path="/usr/bin/vscode"
readonly pixmaps_png_path="/usr/share/pixmaps/${application_name}.png"
readonly application_desktop_path="/usr/share/applications/${application_name}.desktop"
is_existed=${is_existed:-1}   # Default value is 1， assume system has installed VSCode
source_pack_path=${source_pack_path:-}
version_check=${version_check:-0}
type_choose=${type_choose:-0}
customize_setting=${customize_setting:-0}
is_uninstall=${is_uninstall:-0}
proxy_server_specify=${proxy_server_specify:-''}

readonly custom_shellscript_url='https://gitlab.com/axdsop/nixnotes/raw/master/ShellScripts/Toolkits'
readonly custom_settings_json="${custom_shellscript_url}/_Configs/Application/vscode/settings.json"
readonly custom_extension_list="${custom_shellscript_url}/_Configs/Application/vscode/extension_list"


#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | sudo bash -s -- [options] ...

Installing / Updating Visual Studio Code (default stable) On GNU/Linux!
This script requires superuser privileges (eg. root, su).

[available option]
    -h    --help, show help info
    -c    --check, check current stable release version
    -f pack_path    --specify latest release package save path (e.g. /PATH/*.tar.gz) or dir (default is ~/Downloads/) in system
    -t type    --choose version type (0 is stable, 1 is insider version), default is 0
    -s    --customize setting (extension install, settings.json) enable, default is disable
    -p [protocol:]ip:port    --proxy host (http|https|socks4|socks5), default protocol is http
    -u    --uninstall, uninstall software installed (include all stable/insider  version configuration files under home directory '\$HOME/')
\e[0m"
}

while getopts "hcf:t:sup:" option "$@"; do
    case "$option" in
        c ) version_check=1 ;;
        f ) source_pack_path="$OPTARG" ;;
        t ) type_choose="$OPTARG" ;;
        s ) customize_setting=1 ;;
        u ) is_uninstall=1 ;;
        p ) proxy_server_specify="$OPTARG" ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done

#########  1-2 Preparation Operation  #########
fn_InitializationCheck(){
    local l_user_codebase_dir="$HOME/.config/axdsop"
    local l_base_function_temp_save_path=''
    local l_need_remove=0
    local l_base_function_url='https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/base_functions.sh'
    l_base_function_temp_save_path=$(find "${l_user_codebase_dir}" -type f -name "${l_base_function_url##*/}" -print 2> /dev/null)
    
    if [[ ! -f "${l_base_function_temp_save_path}" ]]; then
        local l_download_tool=${l_download_tool:-'wget -qO-'}
        [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_tool='curl -fsL'
        l_need_remove=1
        l_base_function_temp_save_path=$(mktemp -t XXXXXXX)
        ${l_download_tool} "${l_base_function_url}" > "${l_base_function_temp_save_path}"
    fi

    if [[ -f "${l_base_function_temp_save_path}" && -s "${l_base_function_temp_save_path}" ]]; then
        . "${l_base_function_temp_save_path}"
        [[ "${l_need_remove}" -eq 1 ]] && rm "${l_base_function_temp_save_path}"
    else
        echo "Sorry, fail to get base script path."
        exit
    fi

    # tput clear  # Echo the clear-screen sequence for the current terminal.
    # specify 1 means need sudo privilege
    fnBase_RunningEnvironmentCheck '1'

    fnBase_CommandExistCheckPhase 'gzip' # CentOS/Debian/OpenSUSE: gzip
    fnBase_CommandExistCheckPhase 'tar' # .tar.gz
}


#########  2-1. Uninstall  #########
fn_UninstallOperation(){
    if [[ "${is_uninstall}" -eq 1 ]]; then
        if [[ -d "${installation_dir}" ]]; then
            # remove soft link
            [[ -L "${usr_bin_softlink_path}" && $(readlink -f "${usr_bin_softlink_path}") =~ "${vscode_bin_dir}" ]] && rm -f "${usr_bin_softlink_path}"

            [[ -d "${installation_dir}" ]] && rm -rf "${installation_dir}"
            [[ -d "${installation_dir}${bak_suffix}" ]] && rm -rf "${installation_dir}${bak_suffix}"

            [[ -f "${pixmaps_png_path}" ]] && rm -f "${pixmaps_png_path}"
            [[ -f "${application_desktop_path}" ]] && rm -f "${application_desktop_path}"

            # https://code.visualstudio.com/docs/editor/portable#_migrate-to-portable-mode
            # stable ~/.vscode/{extensions}   ~/.config/Code/
            # insider ~/.vscode-insiders/{extensions}   ~/.config/Code - Insiders/
            [[ -d "${login_user_home}/.vscode-oss/" ]] && rm -rf "${login_user_home}/.vscode-oss/"
            [[ -d "${login_user_home}/.vscode/" ]] && rm -rf "${login_user_home}/.vscode/"
            [[ -d "${login_user_home}/.config/Code/" ]] && rm -rf "${login_user_home}/.config/Code/"

            if [[ -d "${login_user_home}/.vscode-insiders/" ]]; then
                [[ -d "${login_user_home}/.vscode-insiders/" ]] && rm -rf "${login_user_home}/.vscode-insiders/"
                [[ -d "${login_user_home}/.config/Code - Insiders/" ]] && rm -rf "${login_user_home}/.config/Code - Insiders/"
            else
                [[ -d "${login_user_home}/.vscode/" ]] && rm -rf "${login_user_home}/.vscode/"
                [[ -d "${login_user_home}/.config/Code/" ]] && rm -rf "${login_user_home}/.config/Code/"
            fi

            [[ -d "${installation_dir}" ]] || fnBase_ExitStatement "${software_fullname} is successfully removed from your system!"
        else
            fnBase_ExitStatement "${c_blue}Note${c_normal}: no ${software_fullname} is found in your system!"
        fi
    fi
}


#########  2-1. Latest & Local Version Check  #########
fn_EssentialLibaryCheck(){
    # 1 - error while loading shared libraries: libXss.so.1: cannot open shared object file: No such file or directory
    # apt|libxss1|/usr/lib/x86_64-linux-gnu/libXss.so.1
    # zypper | libXss1 | /usr/lib64/libXss.so.1
    # yum | libXScrnSaver | /usr/lib64/libXss.so.1
    # pacman | libxss | /usr/lib64/libXss.so.1

    [[ -z $(find /usr/lib* -name 'libXss.so.1' -print 2>/dev/null) ]] && fnBase_ExitStatement "${c_red}Fatal Error${c_normal}: ${software_fullname} need shared libraries ${c_blue}libXss.so.1${c_normal} (apt: libxss1, pacman: libxss, zypper: libXss1, yum: libXScrnSaver)!"
}

fn_Code_CLI_Operation(){
    local l_path=${1:-"${vscode_bin_path}"}
    local l_type="${2:-}"
    local l_e_name="${3:-}"
    local l_action="${4:-}"

    if [[ -f "${l_path}" ]]; then
        case "${l_type,,}" in
            version|v )
                local l_version
                if [[ "${login_user}" == 'root' ]]; then
                    l_version=$("${l_path}" --version | sed -n '1p')
                else
                    l_version=$(sudo -u "${login_user}" "${l_path}" --version 2> /dev/null | sed -n '1p')
                fi
                echo "${l_version}"
                ;;
            extension|e )
                # https://code.visualstudio.com/docs/editor/command-line#_working-with-extensions
                case "${l_action,,}" in
                    install|i )
                        local l_result=0
                        if [[ "${login_user}" == 'root' ]]; then
                            "${l_path}" --install-extension "${l_e_name}" --force &> /dev/null
                            [[ -n $("${l_path}" --list-extensions | sed -r -n '/'"${l_e_name}"'/{p}') ]] && l_result=1
                        else
                            sudo -u "${login_user}" "${l_path}" --install-extension "${l_e_name}" --force &> /dev/null
                            [[ -n $(sudo -u "${login_user}" "${l_path}" --list-extensions | sed -r -n '/'"${l_e_name}"'/{p}') ]] && l_result=1
                        fi
                        echo "${l_result}"
                        ;;
                    uninstall|u )
                        if [[ "${login_user}" == 'root' ]]; then

                            "${l_path}" --uninstall-extension "${l_e_name}" 1> /dev/null
                        else
                            sudo -u "${login_user}" "${l_path}" --uninstall-extension "${l_e_name}" 1> /dev/null
                        fi
                        ;;
                    disable|d )
                        if [[ "${login_user}" == 'root' ]]; then

                            "${l_path}" --disable-extension "${l_e_name}" 1> /dev/null
                        else
                            sudo -u "${login_user}" "${l_path}" --disable-extension "${l_e_name}" 1> /dev/null
                        fi
                        ;;
                esac
                ;;
        esac
    fi
}

fn_VersionComparasion(){
    [[ "${version_check}" -eq 0 ]] && fn_EssentialLibaryCheck

    fnBase_CentralOutputTitle "${software_fullname}"
    fnBase_OperationPhaseStatement 'Version Detection'

    # - Local version
    current_local_version=${current_local_version:-}
    local current_local_type=''
    vscode_bin_name=${vscode_bin_name:-'code-insiders'}
    vscode_bin_path=${vscode_bin_path:-}

    if [[ -d "${installation_dir}" ]]; then
        if [[ -f "${vscode_bin_dir}/${vscode_bin_name}" ]]; then
            # code-insiders
            current_local_type='insider'
        elif [[ -f "${vscode_bin_dir}/${vscode_bin_name%-*}" ]]; then
            vscode_bin_name='code' # code
            current_local_type='stable'
        fi

        vscode_bin_path="${vscode_bin_dir}/${vscode_bin_name}"
    fi

    if [[ -n "${vscode_bin_path}" && -f "${vscode_bin_path}" ]]; then
        # 1.35.0-insider
        current_local_version=$(fn_Code_CLI_Operation "${vscode_bin_path}" 'v')
        current_local_version="${current_local_version%%-*}"
    elif [[ -f "${installation_dir}/resources/app/package.json" ]]; then
        # "version": "1.35.0-insider"
        current_local_version=$(sed -r -n '/version/{s@[",]@@g;s@[^:]+:[[:space:]]*([[:digit:].]+).*$@\1@g;p}' "${installation_dir}/resources/app/package.json")
    else
        is_existed=0
    fi

    if [[ "${is_existed}" -eq 1 ]]; then
        fnBase_OperationProcedureStatement "Local ${c_bold}${c_yellow}${current_local_type}${c_normal} version detecting"
        fnBase_OperationProcedureResult "${current_local_version}"
    fi

    # - Latest online version info
    case "${type_choose,,}" in
        1 ) type_choose='insider' ;;
        0|* ) type_choose='stable' ;;
    esac

    [[ -n "${current_local_type}" && "${current_local_type}" != "${type_choose}" && "${type_choose}" != 'insider' ]] && type_choose="${current_local_type}"

    [[ "${type_choose}" == 'stable' ]] && vscode_bin_name='code'
    vscode_bin_path="${vscode_bin_dir}/${vscode_bin_name}"

    fnBase_OperationProcedureStatement "Online ${c_bold}${c_yellow}${type_choose}${c_normal} version detecting"

    # Ubuntu 16.04.4 LTS (Xenial Xerus) just has `python3`, has no 'python'
    local l_python_name=${l_python_name:-'python'}

    if fnBase_CommandExistIfCheck 'python3'; then
        l_python_name='python3'
    elif fnBase_CommandExistIfCheck 'python'; then
        l_python_name='python'
    fi

    # https://code.visualstudio.com/sha?build=stable
    online_release_info=${online_release_info:-}
    online_release_info=$($download_method "${official_site%/*}/sha" | ${l_python_name} -c $'import json,sys\nobj=json.load(sys.stdin)\nfor item in obj["products"]:\n    if item["platform"]["os"] == "linux-x64":\n        print("{}|{}|{}|{}".format(item["productVersion"],str(item["timestamp"])[:10],item["url"],item["sha256hash"]))' | sed -r -n '/'"${type_choose}"'/{p}')

    # Version|Timestamp|DownloadLink|Sha256Sum
    # 1.34.0|1557957555|https://az764295.vo.msecnd.net/stable/a622c65b2c713c890fcf4fbf07cf34049d5fe758/code-stable-1557957899.tar.gz|6f49a6963021e665c87476dbd418e4a703bf7a2f25abf85426108d71fe7f0657
    # 1.35.0-insider|1558115360|https://az764295.vo.msecnd.net/insider/6ac87465f297a9068bf767f45d6f372fc5697de9/code-insider-1558115526.tar.gz|704caaa71aa1e3f6d163a8e0178a2fc5468b97ce1cd7749a58f5547c9c2d47de

    online_release_version=$(echo "${online_release_info}" | cut -d\| -f 1)
    online_release_version="${online_release_version%%-*}"

    if [[ -n "${online_release_version}" ]]; then
        local online_release_timestamp
        online_release_timestamp=$(echo "${online_release_info}" | cut -d\| -f 2)
        online_release_date=$(date --date "@${online_release_timestamp}" +'%b %d, %Y' 2> /dev/null)

        online_release_downloadlink=$(echo "${online_release_info}" | cut -d\| -f 3)
        online_release_dgst=$(echo "${online_release_info}" | cut -d\| -f 4)

        fnBase_OperationProcedureResult "${online_release_version} - ${online_release_date}"
    else
        fnBase_OperationProcedureResult '' 1
        fnBase_ExitStatement "${c_red}Fatal error${c_normal}, fail to get online ${c_yellow}${type_choose}${c_normal} version!"
    fi

    # - Just online version check
    if [[ "${version_check}" -eq 1 ]]; then
        fnBase_ExitStatement ''
    fi

    # - Online & Local Version Comparasion
    if [[ "${is_existed}" -eq 1 ]]; then
        if [[ "${online_release_version}" == "${current_local_version}" ]]; then
            fnBase_ExitStatement "\nLatest ${c_yellow}${type_choose}${c_normal} version (${c_red}${online_release_version}${c_normal}) has been existed in your system!"
        elif [[ $(fnBase_VersionNumberComparasion "${current_local_version}" "${online_release_version}" '<') -eq 1 ]]; then
            printf "\nExisted ${c_yellow}${current_local_type}${c_normal} version local (${c_red}%s${c_normal}) < Latest ${c_yellow}${type_choose}${c_normal} version online (${c_red}%s${c_normal}).\n" "${current_local_version}" "${online_release_version}"
        fi
    # else
    #     printf "No %s find in your system!\n" "${software_fullname}"
    fi
}


#########  2-3. Download & Decompress Latest Software  #########
fn_DownloadAndDecompressOperation(){
    # fnBase_CentralOutputTitle 'Operation Processing, Just Be Patient'
    local l_operation_type=${l_operation_type:-'installing'}
    [[ "${is_existed}" -eq 1 ]] && l_operation_type='updating'
    fnBase_OperationPhaseStatement "${l_operation_type^}"

    local l_online_release_pack_name="${online_release_downloadlink##*/}"
    local online_release_pack_name="VSCode-${online_release_version}-${type_choose}.${l_online_release_pack_name#*.}"

    local l_is_need_download=${l_is_need_download:-1}
    local l_specified_pack_dir=${l_specified_pack_dir:-}

    # specified file path existed
    if [[ "${source_pack_path}" =~ .tar.gz$ && -s "${source_pack_path}" ]]; then
        [[ "${source_pack_path##*/}" == "${online_release_pack_name}" ]] || fnBase_ExitStatement "Package ${c_red}${source_pack_path}${c_normal} is not same to latest release version."

    # specified dir or ~/Downloads existed
    elif [[ (-n "${source_pack_path}" && -d "${source_pack_path}" ) || -d "${user_download_dir}" ]]; then
        if [[ -d "${source_pack_path}" ]]; then
            l_specified_pack_dir="${source_pack_path}"
        elif [[ -d "${user_download_dir}" ]]; then
            l_specified_pack_dir="${user_download_dir}"
        fi

        l_specified_pack_dir="${l_specified_pack_dir%/}"

        if [[ -d "${l_specified_pack_dir}" && -f "${l_specified_pack_dir}/${online_release_pack_name}" ]]; then
            source_pack_path="${l_specified_pack_dir}/${online_release_pack_name}"
        fi
    fi

    # 1.1- existed package path in system
    if [[ -n "${source_pack_path}" && -f "${source_pack_path}" ]]; then
        fnBase_OperationProcedureStatement 'Existed pack dgst verifying'

        if [[ -n "${online_release_dgst}" && $(fnBase_HashDigestVerification "${source_pack_path}" '256') == "${online_release_dgst}" ]]; then
            fnBase_OperationProcedureResult "${c_red}${source_pack_path%/*}/${c_normal}"
            l_is_need_download=0
        else
            fnBase_OperationProcedureResult '' 1
        fi
    fi

    # 2 - Download Directly From Official Site
    local pack_save_path=${pack_save_path:-}
    if [[ "${l_is_need_download}" -eq 1 ]]; then
        fnBase_OperationProcedureStatement "Downloading ${c_red}${online_release_pack_name}${c_normal}"
        pack_save_path="${temp_save_dir}/${online_release_pack_name}"
        [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"

        $download_method "${online_release_downloadlink}" > "${pack_save_path}"

        if [[ -s "${pack_save_path}" ]]; then
            fnBase_OperationProcedureResult
        else
            [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
            fnBase_OperationProcedureResult '' 1
            fnBase_ExitStatement ''
        fi

        fnBase_OperationProcedureStatement "SHA256 dgst verification"
        if [[ -n "${online_release_dgst}" && $(fnBase_HashDigestVerification "${pack_save_path}" '256') == "${online_release_dgst}"  ]]; then
            fnBase_OperationProcedureResult
        else
            [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
            fnBase_OperationProcedureResult '' 1
        fi

        # use variable "source_pack_path" to execute decompress operation, variable "pack_save_path" used for delete temporary download package
        source_pack_path="${pack_save_path}"
    fi

    # 3 - Decompress & Extract
    fnBase_OperationProcedureStatement "Installing"
    local application_backup_path="${installation_dir}${bak_suffix}"
    [[ -d "${application_backup_path}" ]] && rm -rf "${application_backup_path}"

    [[ -d "${installation_dir}" ]] && mv "${installation_dir}" "${application_backup_path}"
    [[ -d "${installation_dir}" ]] || mkdir -p "${installation_dir}"
    tar xf "${source_pack_path}" -C "${installation_dir}" --strip-components=1 2> /dev/null

    local new_installed_version=${new_installed_version:-}

    if [[ -f "${vscode_bin_path}" ]]; then
        new_installed_version=$(fn_Code_CLI_Operation "${vscode_bin_path}" 'v')
        new_installed_version="${new_installed_version%%-*}"
    elif [[ -f "${installation_dir}/resources/app/package.json" ]]; then
        new_installed_version=$(sed -r -n '/version/{s@[",]@@g;s@[^:]+:[[:space:]]*([[:digit:].]+).*$@\1@g;p}' "${installation_dir}/resources/app/package.json")
    fi

    if [[ "${online_release_version}" == "${new_installed_version}" ]]; then
        # echo "${type_choose}|${online_release_version}|${online_release_date}" > "${version_info_path}"
        [[ -d "${application_backup_path}" ]] && rm -rf "${application_backup_path}"
        fnBase_OperationProcedureResult "${c_red}${installation_dir%/}/${c_normal}"

        # create soft link to directory /usr/bin
        ln -fs "${vscode_bin_path}" "${usr_bin_softlink_path}"

        # copy newly downloaded pack to ~/Downloads
        local l_user_download_save_path=${l_user_download_save_path:-"${user_download_dir}/${source_pack_path##*/}"}
        if [[ -d "${user_download_dir}" && ! -f "${l_user_download_save_path}" ]]; then
            cp "${source_pack_path}" "${l_user_download_save_path}"
            chown "${login_user}" "${l_user_download_save_path}"
            chgrp -hR "${login_user}" "${l_user_download_save_path}" 2> /dev/null

            # removed old version package existed
            if [[ "${is_existed}" -eq 1 ]]; then
                local l_existed_old_pack_path=${l_existed_old_pack_path:-"${user_download_dir}/${online_release_pack_name//${online_release_version}/${current_local_version}}"}
                [[ -f "${l_existed_old_pack_path}" ]] && rm -f "${l_existed_old_pack_path}"
            fi
        fi
    else
        [[ -d "${installation_dir}" ]] && rm -rf "${installation_dir}"
        [[ "${is_existed}" -eq 1 ]] && mv "${application_backup_path}" "${installation_dir}"
        fnBase_OperationProcedureResult '' 1
    fi

    chown -R root:root "${installation_dir}"

    find "${installation_dir}" -maxdepth 1 -type f -name "*.so" -exec chmod 644 {} \; 2> /dev/null
    find "${installation_dir}" -maxdepth 1 -type f -name "*.so.*" -exec chmod 644 {} \; 2> /dev/null  # libvulkan.so.1  libonnxruntime.so.1.7.0

    #  The SUID sandbox helper binary was found, but is not configured correctly. Rather than run without sandboxing I'm aborting now. You need to make sure that /opt/VSCodium/chrome-sandbox is owned by root and has mode 4755.
    [[ -f "${installation_dir}/chrome-sandbox" ]] && chmod 4755 "${installation_dir}/chrome-sandbox"

    [[ -n "${pack_save_path}" && -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
}

#########  2-4. Extension Operation  #########
fn_ExtensionInstallation(){
    local l_bin_path=${1:-"${vscode_bin_path}"}
    local l_e_name=${2:-}
    local l_e_fullname=${3:-}

    fnBase_OperationProcedureStatement "${l_e_name}"

    if [[ $(fn_Code_CLI_Operation "${l_bin_path}" 'e' "${l_e_fullname}" 'install') -eq 1 ]]; then
        fnBase_OperationProcedureResult
    else
        fnBase_OperationProcedureResult '' 1
    fi
}

fn_ExtensionAndEditorSettingOperation(){
    if [[ "${is_existed}" -ne 1 && "${customize_setting}" -eq 1 ]]; then
        fnBase_OperationPhaseStatement 'Extensions Installation'
        # https://code.visualstudio.com/docs/editor/extension-gallery
        # https://code.visualstudio.com/docs/editor/command-line#_working-with-extensions

        # [DEP0005] DeprecationWarning: Buffer() is deprecated due to security and usability issues. Please use the Buffer.alloc(), Buffer.allocUnsafe(), or Buffer.from() methods instead.

        local l_extension_list=''
        l_extension_list=$($download_method "${custom_extension_list}" 2> /dev/null | sed -r -n '/(^#|^$)/d;p')

        local l_theme_name=''
        local l_icon_name=''

        if [[ -n "${l_extension_list}" ]]; then
            echo "${l_extension_list}" | while IFS="|" read -r ext_fullname ext_instll_path setting_name; do
                fn_ExtensionInstallation "${vscode_bin_path}" "${ext_fullname}" "${ext_instll_path}"
            done

            l_theme_name=$(echo "${l_extension_list}" | sed -r -n '/theme/{p;q}' | cut -d\| -f3)
            l_icon_name=$(echo "${l_extension_list}" | sed -r -n '/icons/{p;q}' | cut -d\| -f3)
        fi
    fi

    fnBase_OperationPhaseStatement 'Configuration'

    if [[ "${is_existed}" -ne 1 && "${customize_setting}" -eq 1 ]]; then
        fnBase_OperationProcedureStatement 'Preference Setting'

        local l_config_dir="${login_user_home}/.config/Code/"
        local l_config_user_path="${l_config_dir}/User"

        [[ -d "${l_config_user_path}" ]] || mkdir -p "${l_config_user_path}"

        chown -R "${login_user}" "${l_config_dir}"
        chgrp -hR "${login_user}" "${l_config_dir}" 2> /dev/null

        # sed ':a;N;$!ba;s@\n@\\n@g;s@"@\\&@g' $HOME/.config/Code/User/settings.json

        # https://code.visualstudio.com/docs/python/linting
        # https://code.visualstudio.com/docs/getstarted/tips-and-tricks#_customization
        local l_settings_json_path="${l_config_user_path}/settings.json"
        $download_method "${custom_settings_json}" > "${l_settings_json_path}"
        sed -r -i '/workbench\.colorTheme/{s@^(.*:[[:space:]]*")[^"]+(".*)$@\1'"${l_theme_name}"'\2@g}' "${l_settings_json_path}"
        sed -r -i '/workbench\.iconTheme/{s@^(.*:[[:space:]]*")[^"]+(".*)$@\1'"${l_icon_name}"'\2@g}' "${l_settings_json_path}"

        chown -R "${login_user}" "${l_config_user_path}"
        chgrp -R "${login_user}" "${l_config_user_path}" &> /dev/null

        fnBase_OperationProcedureResult "${l_settings_json_path}"
    fi
}


#########  2-5. Desktop Configuration  #########
fn_DesktopConfiguration(){
    if [[ -d '/usr/share/applications' ]]; then
        fnBase_OperationProcedureStatement 'Desktop Configuration'
        local l_code_icon_path="${installation_dir}/resources/app/resources/linux/code.png"
        [[ -f "${l_code_icon_path}" ]] && ln -sf "${l_code_icon_path}" "${pixmaps_png_path}"

        echo -e "[Desktop Entry]\nVersion=1.0\nType=Application\nGenericName[en]=Text Editor\nName=VSCode ${type_choose^}\nComment=Visual Studio Code - Code Editing. Redefined.\nIcon=${pixmaps_png_path}\nExec=\"${vscode_bin_path}\"\nComment=The Drive to Develop\nCategories=Development;IDE;\nTerminal=false" > "${application_desktop_path}"

        fnBase_OperationProcedureResult "${application_desktop_path}"
    fi
}


#########  2-6. Operation Time Cost  #########
fn_TotalTimeCosting(){
    finish_time=$(date +'%s')        # End Time Of Operation
    total_time_cost=$((finish_time-start_time))   # Total Time Of Operation
    printf "\nTotal time cost is ${c_red}%s${c_normal} seconds!\n" "${total_time_cost}"
}


#########  3. Executing Process  #########
fn_InitializationCheck

fn_UninstallOperation
fn_VersionComparasion
fn_DownloadAndDecompressOperation
fn_ExtensionAndEditorSettingOperation
fn_DesktopConfiguration
fn_TotalTimeCosting


#########  4. EXIT Singal Processing  #########
# trap "commands" EXIT # execute command when exit from shell
fn_TrapEXIT(){
    fnBase_UnsetVars
    unset installation_dir
    unset is_existed
    unset version_check
    unset type_choose
    unset source_pack_path
    unset customize_setting
    unset is_uninstall
    unset proxy_server_specify
    unset update_operation
}

trap fn_TrapEXIT EXIT


# Script End
