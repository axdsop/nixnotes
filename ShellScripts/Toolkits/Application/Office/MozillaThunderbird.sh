#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails


# Official Site:
# - https://www.thunderbird.net (https://www.mozilla.org/en-US/thunderbird/)
# - http://kb.mozillazine.org/Knowledge_Base
# - http://kb.mozillazine.org/Profile_folder_-_Thunderbird

# Target: Automatically Install & Update Mozilla Thunderbird On GNU/Linux
# Developer: MaxdSre

# Change Log:
# - Dec 31, 2020 19:18 Thu ET - fix release version extraction for new desigened html page
# - Oct 08, 2020 08:58 Thu ET - initialization check method update
# - Oct 14, 2019 13:28 Fri ET - change to new base functions
# - Dec 31, 2018 16:40 Mon ET - code reconfiguration, include base funcions from other file
# - Aug 07, 2018 02:30 Tue +0800 - Solve `Running Thunderbird as root in a regular user's session is not supported` occur in version 60.0 while executing `$PATH/firefox --version`
# - Jun 12, 2018 11:57 Tue ET - code reconfiguration
# - Jun 2, 2017 08:43 Fri +0800
# - May 15, 2017 11:03 Mon ET
# - Feb 15, 2017 09:42 Wed +0800


# From version 60.0, it prompt error info:
# Running Thunderbird as root in a regular user's session is not supported.  ($XAUTHORITY is /run/user/1000/gdm/Xauthority which is owned by maxdsre.)



#########  0-1. Singal Setting  #########
trap fnBase_TrapCleanUp INT QUIT

#########  0-2. Variables Setting  #########
# umask 022    # temporarily change umask value to 022
readonly official_site_origin='https://www.thunderbird.net'
readonly official_site="${official_site_origin}/en-US/"
# download page: https://www.thunderbird.net/en-US/thunderbird/all/
readonly software_fullname=${software_fullname:-'Mozilla Thunderbird'}
lang=${lang:-'en-US'}  #en-US, zh-TW
readonly os_type='linux-x86_64'
# download_version="Linux 64-bit"  # linux64
# download_language='English \(US\)' #英文 English (US)， 中文 Chinese (Traditional)，需加反引號轉移
readonly application_name=${application_name:-'MozillaThunderbird'}
installation_dir="/opt/${application_name}"
readonly pixmaps_png_path="/usr/share/pixmaps/${application_name}.png"
readonly application_desktop_path="/usr/share/applications/${application_name}.desktop"
is_existed=${is_existed:-1}   # Default value is 0， check if system has installed Mozilla Thunderbird

version_check=${version_check:-0}
change_language=${change_language:-0}
source_pack_path=${source_pack_path:-}
is_uninstall=${is_uninstall:-0}
proxy_server_specify=${proxy_server_specify:-}


#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | sudo bash -s -- [options] ...

Installing / Updating Mozilla Thunderbird Email Application On GNU/Linux!
This script requires superuser privileges (eg. root, su).

[available option]
    -h    --help, show help info
    -c    --check, check current stable release version
    -f pack_path    --specify latest release package save path (e.g. /PATH/*.tar.bz2) or dir (default is ~/Downloads/) in system
    -l    --change language to 'zh-TW', default is 'en-US'
    -p [protocol:]ip:port    --proxy host (http|https|socks4|socks5), default protocol is http
    -u    --uninstall, uninstall software installed
\e[0m"
}

while getopts "hcf:lup:" option "$@"; do
    case "$option" in
        c ) version_check=1 ;;
        f ) source_pack_path="$OPTARG" ;;
        l ) change_language=1 ;;
        u ) is_uninstall=1 ;;
        p ) proxy_server_specify="$OPTARG" ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done



#########  1-2 Preparation Operation  #########
fn_InitializationCheck(){
    local l_user_codebase_dir="$HOME/.config/axdsop"
    local l_base_function_temp_save_path=''
    local l_need_remove=0
    local l_base_function_url='https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/base_functions.sh'
    l_base_function_temp_save_path=$(find "${l_user_codebase_dir}" -type f -name "${l_base_function_url##*/}" -print 2> /dev/null)
    
    if [[ ! -f "${l_base_function_temp_save_path}" ]]; then
        local l_download_tool=${l_download_tool:-'wget -qO-'}
        [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_tool='curl -fsL'
        l_need_remove=1
        l_base_function_temp_save_path=$(mktemp -t XXXXXXX)
        ${l_download_tool} "${l_base_function_url}" > "${l_base_function_temp_save_path}"
    fi

    if [[ -f "${l_base_function_temp_save_path}" && -s "${l_base_function_temp_save_path}" ]]; then
        . "${l_base_function_temp_save_path}"
        [[ "${l_need_remove}" -eq 1 ]] && rm "${l_base_function_temp_save_path}"
    else
        echo "Sorry, fail to get base script path."
        exit
    fi

    # tput clear  # Echo the clear-screen sequence for the current terminal.
    # specify 1 means need sudo privilege
    fnBase_RunningEnvironmentCheck '1'

    fnBase_CommandExistCheckPhase 'bzip2' # CentOS/Debian/OpenSUSE: bzip2
    fnBase_CommandExistCheckPhase 'tar' # .tar.bz2
}

fn_UtilityLocalVersionDetection(){
    local l_path="${1:-}"
    local l_action=${2:-'--version'}
    local l_output=${l_output:-}

    # From version 60.0, it prompt error info:
    # Running Thunderbird as root in a regular user's session is not supported.  ($XAUTHORITY is /run/user/1000/gdm/Xauthority which is owned by maxdsre.)

    if [[ "${login_user}" == 'root' ]]; then
        l_output=$("${l_path}" "${l_action}" 2> /dev/null)
    else
        l_output=$(sudo -u "${login_user}" "${l_path}" "${l_action}" 2> /dev/null)
    fi
    # sed -r -n '/Thunderbird.*?[[:digit:]]+/{s@^[^[:digit:]]+(.*)$@\1@g;p}'
    l_output="${l_output##* }"
    echo "${l_output}"
}


#########  2-1. Uninstall  #########
fn_UninstallOperation(){
    if [[ "${is_uninstall}" -eq 1 ]]; then
        if [[ -d "${installation_dir}" ]]; then
            [[ -f "${pixmaps_png_path}" ]] && rm -f "${pixmaps_png_path}"
            [[ -f "${application_desktop_path}" ]] && rm -f "${application_desktop_path}"

            [[ -d "${installation_dir}" ]] && rm -rf "${installation_dir}"
            [[ -d "${installation_dir}${bak_suffix}" ]] && rm -rf "${installation_dir}${bak_suffix}"

            local config_path="${login_user_home}/.thunderbird/"
            [[ -d "${config_path}" ]] && rm -rf "${config_path}"    # ~/.thunderbird
            local cache_path="${login_user_home}/.cache/thunderbird/"
            [[ -d "${cache_path}" ]] && rm -rf "${cache_path}"   # ~/.cache/thunderbird

            [[ -d "${installation_dir}" ]] || fnBase_ExitStatement "${software_fullname} is successfully removed from your system!"
        else
            fnBase_ExitStatement "${c_blue}Note${c_normal}: no ${software_fullname} is found in your system!"
        fi
    fi
}


#########  2-2. Latest & Local Version Check  #########
fn_VersionComparasion(){
    fnBase_CentralOutputTitle "${software_fullname}"
    fnBase_OperationPhaseStatement 'Version Detection'

    # - Local version
    current_local_version=${current_local_version:-}
    if [[ -s "${installation_dir}/VERSION" ]]; then
        current_local_version=$(cut -d\| -f1 "${installation_dir}/VERSION")
    fi

    [[ -z "${current_local_version}" && -s "${installation_dir}/thunderbird" ]] && current_local_version=$(fn_UtilityLocalVersionDetection "${installation_dir}/thunderbird")

    [[ -z "${current_local_version}" ]] && is_existed=0
    if [[ "${is_existed}" -eq 1 ]]; then
        fnBase_OperationProcedureStatement 'Local version detecting'
        fnBase_OperationProcedureResult "${current_local_version}"
    fi

    # - Latest online version info
    fnBase_OperationProcedureStatement 'Online version detecting'
    # What’s New Page
    local l_what_new_page=${l_what_new_page:-}
    l_what_new_page=$($download_method "${official_site}" 2> /dev/null | sed -r -n '1,/<\/header>/{/href=.*New/{s@.*href="([^"]+)".*@'"${official_site_origin}"'\1@g;p;q}}')
    # https://www.thunderbird.net/en-US/thunderbird/78.6.0/releasenotes/

    local l_online_release_info=${l_online_release_info:-}
    l_online_release_info=$($download_method "${l_what_new_page}" 2> /dev/null | sed -r -n '/<header[^>]*>/,/<\/header>/{/Release Notes/,/<\/section>/{/Version/{s@.*Version[[:space:]]*([[:digit:].]+).*[[:space:]]+on[[:space:]]+(.*)$@\1|\2@g;p}}}')
    # 78.6.0|December 15, 2020
    online_release_version="${l_online_release_info%%|*}"
    online_release_date=$(date +'%b %d, %Y' --date="${l_online_release_info##*|}")

    if [[ -n "${online_release_version}" ]]; then
        fnBase_OperationProcedureResult "${online_release_version} - ${online_release_date}"
    else
        fnBase_OperationProcedureResult '' 1
    fi

    # - Just online version check
    if [[ "${version_check}" -eq 1 ]]; then
        fnBase_ExitStatement ''

        # if [[ "${is_existed}" -eq 1 ]]; then
        #     fnBase_ExitStatement "Local existed version is ${c_red}${current_local_version}${c_normal}, Latest version online is ${c_red}${online_release_version}${c_normal} (${c_blue}${online_release_date}${c_normal})."
        # else
        #     fnBase_ExitStatement "Latest version online (${c_red}${online_release_version}${c_normal}), Release date ($c_red${online_release_date}$c_normal)."
        # fi
    fi

    # - Online & Local Version Comparasion
    if [[ "${is_existed}" -eq 1 ]]; then
        if [[ "${online_release_version}" == "${current_local_version}" ]]; then
            fnBase_ExitStatement "\nLatest version (${c_red}${online_release_version}${c_normal}) has been existed in your system."
        elif [[ $(fnBase_VersionNumberComparasion "${current_local_version}" "${online_release_version}" '<') -eq 1 ]]; then
            printf "\nExisted version local (${c_red}%s${c_normal}) < Latest version online (${c_red}%s${c_normal}).\n" "${current_local_version}" "${online_release_version}"
        fi
    # else
    #     printf "No %s find in your system!\n" "${software_fullname}"
    fi
}


#########  2-3. Download & Decompress Latest Software  #########
fn_DownloadAndDecompressOperation(){
    local l_operation_type=${l_operation_type:-'installing'}
    [[ "${is_existed}" -eq 1 ]] && l_operation_type='updating'
    fnBase_OperationPhaseStatement "${l_operation_type^}"

    [[ "${change_language}" -eq 1 ]] && lang='zh-TW'

    local l_real_download_link=${l_real_download_link:-'https://download-installer.cdn.mozilla.net/pub/thunderbird/releases/'}

    local online_release_downloadlink
    # https://download-installer.cdn.mozilla.net/pub/thunderbird/releases/78.6.0/linux-x86_64/en-US/thunderbird-78.6.0.tar.bz2
    online_release_downloadlink="${l_real_download_link%/}/${online_release_version}/${os_type}/${lang}/thunderbird-${online_release_version}.tar.bz2"
    local online_release_pack_name="${online_release_downloadlink##*/}"

    local online_release_dgst=${online_release_dgst:-}
    # https://download-installer.cdn.mozilla.net/pub/thunderbird/releases/78.6.0/SHA512SUMS
    online_release_dgst=$($download_method "${l_real_download_link%/}/${online_release_version}/SHA512SUMS" | sed -r -n '/'"${os_type}"'.*?'"${lang}"'.*?'"${online_release_pack_name}"'/{s@^[[:space:]]*([^[:space:]]+).*$@\1@g;p}')
    # a4cb67ff69363e89ffcd9b35a292b20a9351b8a817710dd82b2fdbb8e057ab8d71d6c371d60ea5ea8d42707c3484cc3ff7cbd0e004aefbc178ddf2ae7a0e697e  linux-x86_64/en-US/thunderbird-78.6.0.tar.bz2

    local l_is_need_download=${l_is_need_download:-1}
    local l_specified_pack_dir=${l_specified_pack_dir:-}

    # specified file path existed
    if [[ "${source_pack_path}" =~ .tar.bz2$ && -s "${source_pack_path}" ]]; then
        [[ "${source_pack_path##*/}" == "${online_release_pack_name}" ]] || fnBase_ExitStatement "Package ${c_red}${source_pack_path}${c_normal} is not same to latest release version name!"

    # specified dir or ~/Downloads existed
    elif [[ (-n "${source_pack_path}" && -d "${source_pack_path}" ) || -d "${user_download_dir}" ]]; then
        if [[ -d "${source_pack_path}" ]]; then
            l_specified_pack_dir="${source_pack_path}"
        elif [[ -d "${user_download_dir}" ]]; then
            l_specified_pack_dir="${user_download_dir}"
        fi

        l_specified_pack_dir="${l_specified_pack_dir%/}"

        if [[ -d "${l_specified_pack_dir}" && -f "${l_specified_pack_dir}/${online_release_pack_name}" ]]; then
            source_pack_path="${l_specified_pack_dir}/${online_release_pack_name}"
        fi
    fi

    # 1.1- existed package path in system
    if [[ -n "${source_pack_path}" && -f "${source_pack_path}" ]]; then
        fnBase_OperationProcedureStatement 'Existed pack dgst verifying'

        if [[ -n "${online_release_dgst}" && $(fnBase_HashDigestVerification "${source_pack_path}" '512') == "${online_release_dgst}" ]]; then
            fnBase_OperationProcedureResult "${c_red}${source_pack_path%/*}/${c_normal}"
            l_is_need_download=0
        else
            fnBase_OperationProcedureResult '' 1
        fi
    fi

    # 2 - Download Directly From Official Site
    local pack_save_path=${pack_save_path:-}

    if [[ "${l_is_need_download}" -eq 1 ]]; then
        fnBase_OperationProcedureStatement "Downloading ${c_red}${online_release_pack_name}${c_normal}"
        pack_save_path="${temp_save_dir}/${online_release_pack_name}"
        [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"

        $download_method "${online_release_downloadlink}" > "${pack_save_path}"

        if [[ -s "${pack_save_path}" ]]; then
            fnBase_OperationProcedureResult
        else
            [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
            fnBase_OperationProcedureResult '' 1
        fi

        fnBase_OperationProcedureStatement "SHA512 dgst verification"
        if [[ -n "${online_release_dgst}" && $(fnBase_HashDigestVerification "${pack_save_path}" '512') == "${online_release_dgst}"  ]]; then
            fnBase_OperationProcedureResult
        else
            [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
            fnBase_OperationProcedureResult '' 1
        fi

        # use variable "source_pack_path" to execute decompress operation, variable "pack_save_path" used for delete temporary download package
        source_pack_path="${pack_save_path}"
    fi

    # 3 - Decompress & Extract
    fnBase_OperationProcedureStatement 'Installing'
    local application_backup_path="${installation_dir}${bak_suffix}"
    [[ -d "${application_backup_path}" ]] && rm -rf "${application_backup_path}"

    [[ -d "${installation_dir}" ]] && mv "${installation_dir}" "${application_backup_path}"
    [[ -d "${installation_dir}" ]] || mkdir -p "${installation_dir}"
    tar xf "${source_pack_path}" -C "${installation_dir}" --strip-components=1 2> /dev/null
    chown -R root:root "${installation_dir}"

    local new_installed_version=${new_installed_version:-}
    [[ -f "${installation_dir}/thunderbird" ]] && new_installed_version=$(fn_UtilityLocalVersionDetection "${installation_dir}/thunderbird")

    if [[ "${online_release_version}" == "${new_installed_version}" ]]; then
        [[ -f "${pixmaps_png_path}" ]] && rm -f "${pixmaps_png_path}"
        [[ -f "${application_desktop_path}" ]] && rm -f "${application_desktop_path}"
        [[ -d "${application_backup_path}" ]] && rm -rf "${application_backup_path}"
        fnBase_OperationProcedureResult "${c_red}${installation_dir%/}/${c_normal}"

        # write 60.0|Aug 6, 2018 into $installation_dir/VERSION
        echo "${online_release_version}|${online_release_date}" > "${installation_dir}/VERSION"

        # copy newly downloaded pack to ~/Downloads
        local l_user_download_save_path=${l_user_download_save_path:-"${user_download_dir}/${source_pack_path##*/}"}
        if [[ -d "${user_download_dir}" && ! -f "${l_user_download_save_path}" ]]; then
            cp "${source_pack_path}" "${l_user_download_save_path}"
            chown "${login_user}" "${l_user_download_save_path}"

            # removed old version package existed
            if [[ "${is_existed}" -eq 1 ]]; then
                local l_existed_old_pack_path=${l_existed_old_pack_path:-"${user_download_dir}/${online_release_pack_name//${online_release_version}/${current_local_version}}"}
                [[ -f "${l_existed_old_pack_path}" ]] && rm -f "${l_existed_old_pack_path}"
            fi
        fi

    else
        [[ -d "${installation_dir}" ]] && rm -rf "${installation_dir}"
        [[ "${is_existed}" -eq 1 ]] && mv "${application_backup_path}" "${installation_dir}"
        fnBase_OperationProcedureResult '' 1
    fi

    [[ -n "${pack_save_path}" && -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
}


#########  2-4. Desktop Configuration  #########
fn_DesktopConfiguration(){
    if [[ -d '/usr/share/applications' ]]; then
        fnBase_OperationProcedureStatement 'Desktop Configuration'

        local l_thunder_icon_path=${l_thunder_icon_path:-}
        if [[ -f "${installation_dir}/chrome/icons/default/default256.png" ]]; then
            l_thunder_icon_path="${installation_dir}/chrome/icons/default/default256.png"
        elif [[ -f "${installation_dir}/chrome/icons/default/default128.png" ]]; then
            l_thunder_icon_path="${installation_dir}/chrome/icons/default/default128.png"
        fi

        [[ -n "${l_thunder_icon_path}" ]] && ln -sf "${l_thunder_icon_path}" "${pixmaps_png_path}"

        echo -e "[Desktop Entry]\nEncoding=UTF-8\nName=Thunderbird\nGenericName=Email\nComment=Send and Receive Email\nExec=installation_dir/thunderbird %u\nIcon=${pixmaps_png_path}\nTerminal=false\nType=Application\nMimeType=message/rfc822;x-scheme-handler/mailto;\nStartupNotify=true\nCategories=Network;Email;Application;" > "${application_desktop_path}"

        # sed -i -r 's@application_name@'"$application_name"'@g' "${application_desktop_path}"
        sed -i -r 's@installation_dir@'"$installation_dir"'@g' "${application_desktop_path}"

        fnBase_OperationProcedureResult "${application_desktop_path%/*}"
    fi
}


#########  2-5. Operation Time Cost  #########
fn_TotalTimeCosting(){
    finish_time=$(date +'%s')        # End Time Of Operation
    total_time_cost=$((finish_time-start_time))   # Total Time Of Operation
    printf "\nTotal time cost is ${c_red}%s${c_normal} seconds!\n" "${total_time_cost}"
}


#########  3. Executing Process  #########
fn_InitializationCheck

fn_UninstallOperation
fn_VersionComparasion
fn_DownloadAndDecompressOperation
fn_DesktopConfiguration
fn_TotalTimeCosting


#########  4. EXIT Singal Processing  #########
# trap "commands" EXIT # execute command when exit from shell
fn_TrapEXIT(){
    fnBase_UnsetVars
    unset lang
    unset installation_dir
    unset is_existed
    unset version_check
    unset source_pack_path
    unset change_language
    unset is_uninstall
    unset proxy_server_specify
}

trap fn_TrapEXIT EXIT

# Script End
