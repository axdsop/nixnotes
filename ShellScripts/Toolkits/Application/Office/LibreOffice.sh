#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails


# Official Site: https://www.libreoffice.org
# Target: Automatically Install & Update LibreOffice On GNU/Linux
# Developer: MaxdSre

# Change Log:
# - Oct 08, 2020 08:57 Thu ET - initialization check method update
# - Oct 14, 2019 14:41 Fri  ET - change to new base functions
# - Mar 25, 2018 18:14 ET - LibreOffice stop use https://downloadarchive.documentfoundation.org/libreoffice/old/ as archive page
# - Dec 31, 2018 15:59 Mon ET - code reconfiguration, include base funcions from other file
# - Oct 27, 2017 11:42 Fri +0800
# - Jun 08, 2017 09:24 Thu +0800
# - May 19, 2017 15:12 Fri ET
# - Mar 01, 2017 14:01~20:15 Wed +0800


#########  0-1. Singal Setting  #########
trap fnBase_TrapCleanUp INT QUIT

#########  0-2. Variables Setting  #########
# umask 022    # temporarily change umask value to 022

readonly official_site='https://www.libreoffice.org'
readonly download_page="${official_site}/download/download/"
readonly download_link='http://download.documentfoundation.org'   #真實下載鏈接(proximateVersion)
# readonly old_archive_page='https://downloadarchive.documentfoundation.org/libreoffice/old/' #真實下載地址(preciseVersion)
readonly new_archive_page='https://download.documentfoundation.org/libreoffice/stable/'  # refer to latest mirror site, HTTP header 'location' via 'curl -I'
software_fullname=${software_fullname:-'Libre Office'}
version_type=${version_type:-'still'}   # still, fresh
lang=${lang:-'en-US'}  #英文 en-US, 中文 zh-TW
lang_pack_existed=${lang_pack_existed:-0}   # 語言包是否存在

readonly arch=$(uname -m)    # hardware arch
is_existed=${is_existed:-0}  #判斷LibreOffice是否安裝，默認爲0 未安裝， 1 已安裝有
version_check=${version_check:-0}
fresh_version=${fresh_version:-0}
change_language=${change_language:-0}
is_uninstall=${is_uninstall:-0}
proxy_server_specify=${proxy_server_specify:-}



#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | sudo bash -s -- [options] ...

Installing / Updating Libre Office - Free Office Suite On GNU/Linux!
This script requires superuser privileges (eg. root, su).

[available option]
    -h    --help, show help info
    -c    --check, check current stable('still') release version
    -f    --choose latest 'fresh' version, default is mature 'still' version
    -l    --change language to 'zh-TW', default is 'en-US'
    -p [protocol:]ip:port    --proxy host (http|https|socks4|socks5), default protocol is http
    -u    --uninstall, uninstall software installed
\e[0m"
}

while getopts "hcflup:" option "$@"; do
    case "$option" in
        c ) version_check=1 ;;
        f ) fresh_version=1 ;;
        l ) change_language=1 ;;
        u ) is_uninstall=1 ;;
        p ) proxy_server_specify="$OPTARG" ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done


#########  1-2 Preparation Operation  #########
fn_InitializationCheck(){
    local l_user_codebase_dir="$HOME/.config/axdsop"
    local l_base_function_temp_save_path=''
    local l_need_remove=0
    local l_base_function_url='https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/base_functions.sh'
    l_base_function_temp_save_path=$(find "${l_user_codebase_dir}" -type f -name "${l_base_function_url##*/}" -print 2> /dev/null)
    
    if [[ ! -f "${l_base_function_temp_save_path}" ]]; then
        local l_download_tool=${l_download_tool:-'wget -qO-'}
        [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_tool='curl -fsL'
        l_need_remove=1
        l_base_function_temp_save_path=$(mktemp -t XXXXXXX)
        ${l_download_tool} "${l_base_function_url}" > "${l_base_function_temp_save_path}"
    fi

    if [[ -f "${l_base_function_temp_save_path}" && -s "${l_base_function_temp_save_path}" ]]; then
        . "${l_base_function_temp_save_path}"
        [[ "${l_need_remove}" -eq 1 ]] && rm "${l_base_function_temp_save_path}"
    else
        echo "Sorry, fail to get base script path."
        exit
    fi

    # tput clear  # Echo the clear-screen sequence for the current terminal.
    # specify 1 means need sudo privilege
    fnBase_RunningEnvironmentCheck '1'

    fnBase_CommandExistCheckPhase 'gzip' # CentOS/Debian/OpenSUSE: gzip
    fnBase_CommandExistCheckPhase 'tar' # .tar.gz
}

fn_PackManagerCommandCheck(){
    if fnBase_CommandExistIfCheck 'zypper'; then
        zypper ref -f &> /dev/null
        package_type='rpm'
        install_command='zypper in -ly'
        remove_command='zypper rm -y'
    elif fnBase_CommandExistIfCheck 'yum'; then
        yum makecache fast 1> /dev/null
        package_type='rpm'
        install_command='yum localinstall -y'
        remove_command='yum erase -y'
    elif fnBase_CommandExistIfCheck 'dnf'; then
        package_type='rpm'
        install_command='dnf install -y'
        remove_command='dnf remove -y'
    elif fnBase_CommandExistIfCheck 'rpm'; then
        package_type='rpm'
        remove_command='rpm -e'
    elif fnBase_CommandExistIfCheck 'apt-get'; then
        apt-get -yq update &> /dev/null
        package_type='deb'
        install_command='dpkg -i'
        remove_command='apt-get purge -y'
    elif fnBase_CommandExistIfCheck 'dpkg'; then
        package_type='deb'
        install_command='dpkg -i'
        remove_command='dpkg -r'
    fi
}

#########  2-1. Latest & Local Version Check  #########
fn_VersionLocalCheck(){
    ls /usr/bin/libreoffice* &> /dev/null   # check binary program exists or not
    if [[ $? -eq 0 ]]; then
        is_existed=1
        for i in /usr/bin/libreoffice*;do
            [[ -e "${i}" ]] || break
            current_local_version=$($i --version | awk '{print $2;exit}')
            if [[ -n "${current_local_version}" ]]; then
                break
            fi
        done    # Endo for
    fi  #End if
}

fn_VersionOnlineCheck(){
    [[ "${fresh_version}" -eq 1 ]] && version_type='fresh'

    # - Via Release Note Page, But Not Update In Time
    # https://www.libreoffice.org/download/release-notes

    # - Proximate Latest Version
    proximate_latest_version_list=$($download_method "${download_page}" | sed -r -n '/released/{s@^.*<ul[^>]*>@@g;s@<\/a>@ @g;s@<[^>]*>@@g;s@[[:space:]]*$@@g;p}')
    [[ -z "${proximate_latest_version_list}" ]] && fnBase_ExitStatement "${c_red}Sorry${c_normal}: fail to get latest ${c_blue}${version_type}${c_normal} proximate version on official site!"
    # 5.4.2 5.3.6 / 6.2.2 6.2.1 6.1.5
    fresh_proximate_version=${proximate_latest_version_list%% *}
    still_proximate_version=${proximate_latest_version_list##* }

    proximate_latest_version=${proximate_latest_version:-}
    case "${version_type,,}" in
        fresh ) proximate_latest_version="${fresh_proximate_version}" ;;
        still ) proximate_latest_version="${still_proximate_version}" ;;
    esac

    # Note: LibreOffice doesn't support archive page https://downloadarchive.documentfoundation.org/libreoffice/old/ any more, it may be not has precise version number. (Mar 25, 2019)
    # - Precise Latest Version
    # precise_latest_version_arr=($($download_method "${old_archive_page}" | sed -r -n '/>'"${proximate_latest_version}"'/{s@.*<a href="([^"]*)">([^\/]*)\/<.*@\2'" ${old_archive_page}"'\1@g;p}' | sed -n '$p'))
    # 5.4.2.2 https://downloadarchive.documentfoundation.org/libreoffice/old/5.4.2.2/

    # latest_version_online=${latest_version_online:-}

    # shellcheck disable=SC2116
    # if [[ $(echo ${#precise_latest_version_arr[@]}) -ne 0 ]]; then
    #     latest_version_online=${precise_latest_version_arr[0]}
    if [[ -n "${proximate_latest_version}" ]]; then
        latest_version_online="${proximate_latest_version}"
    else
        fnBase_ExitStatement "${c_red}Sorry${c_normal}: fail to get latest ${c_blue}${version_type}${c_normal} precise version on official site!"
    fi

    if [[ "${version_check}" -eq 1 ]]; then
        if [[ "${is_existed}" -eq 1 ]]; then
            fnBase_ExitStatement "Local existed version is ${c_red}${current_local_version}${c_normal}, Latest ${c_blue}${version_type}${c_normal} version online is ${c_red}${latest_version_online}${c_normal}!"
        else
            fnBase_ExitStatement "Latest ${c_blue}${version_type}${c_normal} version online (${c_red}${latest_version_online}${c_normal})!"
        fi
    fi

    if [[ "${is_existed}" -eq 1 ]]; then
        if [[ "${latest_version_online}" == "${current_local_version}" ]]; then
            fnBase_ExitStatement "Latest ${c_blue}${version_type}${c_normal} version (${c_red}${latest_version_online}${c_normal}) has been existed in your system!"
        elif [[ $(fnBase_VersionNumberComparasion "${current_local_version}" "${latest_version_online}" '<') -eq 1 ]]; then
            printf "Existed version local (${c_red}%s${c_normal}) < Latest ${c_blue}%s${c_normal} online version ${c_red}%s${c_normal}!\n" "$current_local_version" "${version_type}" "$latest_version_online"
        fi
    else
        printf "No %s find in your system!\n" "${software_fullname}"
    fi
}


#########  2-2. Uninstall  #########
fn_UninstallOperation(){
    if [[ "${is_uninstall}" -eq 1 ]]; then
        [[ "${is_existed}" -eq 1 ]] || fnBase_ExitStatement "${c_blue}Note${c_normal}: no ${software_fullname} is found in your system!"

        ${remove_command} libreoffice* &> /dev/null

        local config_path="${login_user_home}/.config/libreoffice"
        [[ -d "${config_path}" ]] && rm -rf "${config_path}"    # ~/.config/libreoffice

        ls /usr/bin/libreoffice* &> /dev/null   # check binary program exists or not
        if [[ $? -gt 0 ]]; then
            fnBase_ExitStatement "${software_fullname} (v ${c_red}${current_local_version}${c_normal}) is successfully removed from your system!"
        fi
    fi
}


#########  2-3. Download & Decompress Latest Software  #########
# Custom Function - Package Download & SHA-256 Verify
fn_PackageDownload(){
    local pack_list="$1"
    local pack_save_name="$2"

    $download_method "${pack_list}" > "${pack_save_name}"

    if [[ -f "${pack_save_name}" ]]; then
        local pack_sha256=${pack_sha256:-}
        pack_sha256=$(fnBase_HashDigestVerification "${pack_save_name}" '256')

        if [[ "${pack_sha256}" == 0 ]]; then
            [[ -f "${pack_save_name}" ]] && rm -f "${pack_save_name}"
            fnBase_ExitStatement "${c_red}Sorry${c_normal}: package ${c_blue}${pack_save_name}${c_normal} not exists!"
        else
            local pack_sha256_origin=${pack_sha256_origin:-}
            pack_sha256_origin=$($download_method "${pack_list}.mirrorlist" | sed -n -r '/SHA-256/s@<[^>]*>@@g;s@.*Hash: (.*)@\1@gp')

            if [[ "${pack_sha256}" == "${pack_sha256_origin}" ]]; then
                printf "Package $c_blue%s${c_normal} approves SHA-256 check!\n" "${pack_save_name##*/}"
            else
                [[ -f "${pack_save_name}" ]] && rm -f "${pack_save_name}"
                fnBase_ExitStatement "${c_red}Error${c_normal}, package ${c_blue}${pack_save_name##*/}${c_normal} SHA-256 check inconsistency! The package may not be integrated!"
            fi
        fi
    fi
}

fn_DecompressTargetFile(){
    local pack_save_name="$1"
    local pack_extrect_path="$2"
    local pack_type="$3"

    if [[ -f "${pack_save_name}" ]]; then
        [[ -d "${pack_extrect_path}" ]] && rm -rf "${pack_extrect_path}"
        mkdir -p "${pack_extrect_path}"
        tar xf "${pack_save_name}" -C "${pack_extrect_path}" --strip-components=1
        local pack_path="${pack_extrect_path}/${pack_type^^}S"
        if [[ -d "${pack_path}" ]]; then
            $install_command "${pack_path}"/*."${pack_type}" 1> /dev/null
        fi
    fi
}

fn_RemoveTemporaryFiles(){
    if [[ -d "${temp_save_dir}" ]]; then
        rm -rf "${temp_save_dir}"/LibreOffice* &> /dev/null
    fi
}

fn_InstallationOperation(){
    printf "Begin to download latest ${c_blue}%s${c_normal} version ${c_red}%s${c_normal}, just be patient!\n" "${version_type}" "${latest_version_online}"

    if [[ "${change_language}" -eq 1 ]]; then
        lang='zh-TW'
        lang_pack_existed=1
    fi

    # - Precise Version Link
    # https://www.libreoffice.org/donate/dl/deb-x86_64/6.2.2/en-US/LibreOffice_6.2.2_Linux_x86-64_deb.tar.gz  -->  https://download.documentfoundation.org/libreoffice/stable/6.2.2/deb/x86_64/LibreOffice_6.2.2_Linux_x86-64_deb.tar.gz
    main_pack_link="${new_archive_page}${latest_version_online}/${package_type}/${arch}/LibreOffice_${latest_version_online}_Linux_${arch//_/-}_${package_type}.tar.gz"

    help_pack_link="${new_archive_page}${latest_version_online}/${package_type}/${arch}/LibreOffice_${latest_version_online}_Linux_${arch//_/-}_${package_type}_helppack_$lang.tar.gz"

    [[ "${lang_pack_existed}" -eq 1 ]] && lang_pack_link="${new_archive_page}${latest_version_online}/${package_type}/${arch}/LibreOffice_${latest_version_online}_Linux_${arch//_/-}_${package_type}_langpack_$lang.tar.gz"


    # main_pack_link="${old_archive_page}${latest_version_online}/${package_type}/${arch}/LibreOffice_${latest_version_online}_Linux_${arch//_/-}_${package_type}.tar.gz"
    #
    # help_pack_link="${old_archive_page}${latest_version_online}/${package_type}/${arch}/LibreOffice_${latest_version_online}_Linux_${arch//_/-}_${package_type}_helppack_$lang.tar.gz"
    #
    # [[ "${lang_pack_existed}" -eq 1 ]] && lang_pack_link="${old_archive_page}${latest_version_online}/${package_type}/${arch}/LibreOffice_${latest_version_online}_Linux_${arch//_/-}_${package_type}_langpack_$lang.tar.gz"

    # - Temp Save Path
    main_pack_save_name="${temp_save_dir}/LibreOffice_${latest_version_online}.${lang}.tar.gz"
    help_pack_save_name="${temp_save_dir}/LibreOffice_helppack_${latest_version_online}.${lang}.tar.gz"
    [[ "${lang_pack_existed}" -eq 1 ]] && lang_pack_save_name="${temp_save_dir}/LibreOffice_langpack_${latest_version_online}.${lang}.tar.gz"

    fn_RemoveTemporaryFiles

    ### - Docnload & Verify Controller (Very Important) - ###
    fn_PackageDownload "${main_pack_link}" "${main_pack_save_name}"
    fn_PackageDownload "${help_pack_link}" "${help_pack_save_name}"
    [[ "${lang_pack_existed}" -eq 1 ]] &&  fn_PackageDownload "${lang_pack_link}" "${lang_pack_save_name}"

    ###  - Decompression & Installation -  ###
    $remove_command libreoffice* &> /dev/null
    main_pack_extract_path="${temp_save_dir}/LibreOffice"
    help_pack_extract_path="${temp_save_dir}/LibreOffice_helppack"
    [[ "$lang_pack_existed" -eq 1 ]] &&  lang_pack_extract_path="${temp_save_dir}/LibreOffice_langpack"
    # 不能放在同一個目錄中安裝，help的pack依賴mian中的pack

    if [[ "${is_existed}" -eq 1 ]]; then
        printf "Begin to ${c_red}%s${c_normal} ${software_fullname}!\n" "update"
    else
        printf "Begin to ${c_red}%s${c_normal} ${software_fullname}!\n" "install"
    fi

    ### - Decompression & Extraction & Installation Controller (Very Important) - ###
    fn_DecompressTargetFile "${main_pack_save_name}" "${main_pack_extract_path}" "${package_type}"
    fn_DecompressTargetFile "${help_pack_save_name}" "${help_pack_extract_path}" "${package_type}"
    [[ "${lang_pack_existed}" -eq 1 ]] && fn_DecompressTargetFile "${lang_pack_save_name}" "${lang_pack_extract_path}" "${package_type}"

    fn_RemoveTemporaryFiles

    ls /usr/bin/libreoffice* &> /dev/null   # check binary program exists or not
    if [[ $? -eq 0 ]]; then
        for i in /usr/bin/libreoffice*;do
            [[ -e "${i}" ]] || break
            local new_installed_version=${new_installed_version:-}
            new_installed_version=$("$i" --version | awk '{print $2;exit}')    # Just Installed Version In System

            if [[ "${latest_version_online}" == "${new_installed_version}" ]]; then
                fn_RemoveTemporaryFiles

                if [[ "$is_existed" -eq 1 ]]; then
                    printf "Successfully update %s to ${c_blue}%s${c_normal} version ${c_red}%s${c_normal}!\n" "${software_fullname}" "${version_type}" "${latest_version_online}"
                else
                    printf "Successfully install %s ${c_blue}%s${c_normal} version ${c_red}%s${c_normal}!\n" "${software_fullname}" "${version_type}" "${latest_version_online}"
                fi
            fi

            break
        done
    else
        fnBase_ExitStatement "${c_red}Sorry${c_normal}: fail to install ${software_fullname} , please try later again!"
    fi
}


#########  2-4. Operation Time Cost  #########
fn_TotalTimeCosting(){
    finish_time=$(date +'%s')        # End Time Of Operation
    total_time_cost=$((finish_time-start_time))   # Total Time Of Operation
    printf "\nTotal time cost is ${c_red}%s${c_normal} seconds!\n" "${total_time_cost}"
}


#########  3. Executing Process  #########
fn_InitializationCheck
fn_PackManagerCommandCheck
fn_VersionLocalCheck

fn_UninstallOperation
fn_VersionOnlineCheck
fn_InstallationOperation
fn_TotalTimeCosting


#########  4. EXIT Singal Processing  #########
# trap "commands" EXIT # execute command when exit from shell
fn_TrapEXIT(){
    fnBase_UnsetVars
    unset software_fullname
    unset version_type
    unset lang
    unset lang_pack_existed
    unset is_existed
    unset version_check
    unset is_uninstall
    unset proxy_server_specify
    unset fresh_version
    unset change_language
}

trap fn_TrapEXIT EXIT

# Script End
