#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails


# Official Site: https://filezilla-project.org
# Installation: https://wiki.filezilla-project.org/Client_Installation

# Target: Automatically Install & Update FileZilla FTP Client On GNU/Linux
# Developer: MaxdSre

# Change Log:
# - Oct 08, 2020 08:56 Thu ET - initialization check method update
# - Oct 14, 2019 13:20 Fri  ET - change to new base functions
# - Dec 30, 2018 21:35 Sun ET - code reconfiguration, include base funcions from other file
# - May 23, 2018 13:38 Wed ET - code reconfiguration
# - Jun 07, 2017 08:52~10:29 Wed +0800
# - May 22, 2017 02:34 Mon +0800


#########  0-1. Singal Setting  #########
trap fnBase_TrapCleanUp INT QUIT

#########  0-2. Variables Setting  #########
# umask 022    # temporarily change umask value to 022
readonly official_site='https://filezilla-project.org'
readonly download_page="${official_site}/download.php?show_all=1"
readonly release_note_page="${official_site}/versions.php"
readonly os_type='x86_64-linux'
readonly software_fullname='FileZilla'
readonly application_name='FileZilla'
installation_dir="/opt/${application_name}"
readonly pixmaps_png_path="/usr/share/pixmaps/${application_name}.png"
readonly application_desktop_path="/usr/share/applications/${application_name}.desktop"
is_existed=${is_existed:-0}      # Default value is 0， check if system has installed FileZilla

version_check=${version_check:-0}
source_pack_path=${source_pack_path:-}
is_uninstall=${is_uninstall:-0}
proxy_server_specify=${proxy_server_specify:-}


#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | sudo bash -s -- [options] ...

Installing / Updating FileZilla FTP Client On GNU/Linux!
This script requires superuser privileges (eg. root, su).

[available option]
    -h    --help, show help info
    -c    --check, check current stable release version
    -f pack_path    --specify latest release package save path (e.g. /PATH/*.tar.bz2) or dir (default is ~/Downloads/) in system
    -p [protocol:]ip:port    --proxy host (http|https|socks4|socks5), default protocol is http
    -u    --uninstall, uninstall software installed
\e[0m"
}

while getopts "hcf:up:" option "$@"; do
    case "$option" in
        c ) version_check=1 ;;
        f ) source_pack_path="$OPTARG" ;;
        u ) is_uninstall=1 ;;
        p ) proxy_server_specify="$OPTARG" ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done


#########  1-2 Preparation Operation  #########
fn_InitializationCheck(){
    local l_user_codebase_dir="$HOME/.config/axdsop"
    local l_base_function_temp_save_path=''
    local l_need_remove=0
    local l_base_function_url='https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/base_functions.sh'
    l_base_function_temp_save_path=$(find "${l_user_codebase_dir}" -type f -name "${l_base_function_url##*/}" -print 2> /dev/null)
    
    if [[ ! -f "${l_base_function_temp_save_path}" ]]; then
        local l_download_tool=${l_download_tool:-'wget -qO-'}
        [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_tool='curl -fsL'
        l_need_remove=1
        l_base_function_temp_save_path=$(mktemp -t XXXXXXX)
        ${l_download_tool} "${l_base_function_url}" > "${l_base_function_temp_save_path}"
    fi

    if [[ -f "${l_base_function_temp_save_path}" && -s "${l_base_function_temp_save_path}" ]]; then
        . "${l_base_function_temp_save_path}"
        [[ "${l_need_remove}" -eq 1 ]] && rm "${l_base_function_temp_save_path}"
    else
        echo "Sorry, fail to get base script path."
        exit
    fi

    # tput clear  # Echo the clear-screen sequence for the current terminal.
    # specify 1 means need sudo privilege
    fnBase_RunningEnvironmentCheck '1'

    fnBase_CommandExistCheckPhase 'bzip2' # CentOS/Debian/OpenSUSE: bzip2
    fnBase_CommandExistCheckPhase 'tar' # .tar.bz2
}


#########  2-1. Uninstall  #########
fn_UninstallOperation(){
    if [[ "${is_uninstall}" -eq 1 ]]; then
        if [[ -d "${installation_dir}" ]]; then
            [[ -d "${installation_dir}" ]] && rm -rf "${installation_dir}"
            [[ -d "${installation_dir}${bak_suffix}" ]] && rm -rf "${installation_dir}${bak_suffix}"

            [[ -f "${pixmaps_png_path}" ]] && rm -f "${pixmaps_png_path}"
            [[ -f "${application_desktop_path}" ]] && rm -f "${application_desktop_path}"

            local config_path="${login_user_home}/.config/filezilla"
            [[ -d "${config_path}" ]] && rm -rf "${config_path}"    # ~/.config/filezilla
            local cache_path="${login_user_home}/.cache/filezilla"
            [[ -d "${cache_path}" ]] && rm -rf "${cache_path}"   # ~/.cache/filezilla

            [[ -d "${installation_dir}" ]] || fnBase_ExitStatement "${software_fullname} is successfully removed from your system!"
        else
            fnBase_ExitStatement "${c_blue}Note${c_normal}: no ${software_fullname} is found in your system!"
        fi
    fi
}


#########  2-2. Latest & Local Version Check  #########
fn_VersionComparasion(){
    fnBase_CentralOutputTitle 'FileZilla®'
    fnBase_OperationPhaseStatement 'Version Detection'

    # - Local version
    current_local_version=${current_local_version:-}
    if [[ -f "${installation_dir}/bin/filezilla" ]]; then
        is_existed=1
        # FileZilla 3.33.0 official build, compiled on 2018-05-07
        current_local_version=$("${installation_dir}/bin/filezilla" --version 2> /dev/null | sed -r -n 's@^[^[:space:]]*[[:space:]]*([[:digit:].]+).*@\1@g;p')

        fnBase_OperationProcedureStatement 'Local version detecting'
        fnBase_OperationProcedureResult "${current_local_version}"
    fi

    # - Latest online version info
    fnBase_OperationProcedureStatement "Online version detecting"

    online_release_version=$($download_method "${download_page}" | sed -r -n '/latest stable version/s@.* ([[:digit:].]*).*@\1@p')
    [[ -z "${online_release_version}" ]] && fnBase_ExitStatement "${c_red}Sorry${c_normal}: fail to get latest online version on official site!"

    online_release_date=$($download_method "${release_note_page}" | sed -r -n '/h3/{/'"${online_release_version}"'"/{s@.*\((.*)\).*@\1@p}}' | date +'%b %d, %Y' -f - 2> /dev/null)
    # 3.33.0 2018-05-07

    if [[ -n "${online_release_version}" ]]; then
        fnBase_OperationProcedureResult "${online_release_version} - ${online_release_date}"
    else
        fnBase_OperationProcedureResult '' 1
    fi

    # - Just online version check
    if [[ "${version_check}" -eq 1 ]]; then
        fnBase_ExitStatement ''

        # if [[ "${is_existed}" -eq 1 ]]; then
        #     fnBase_ExitStatement "Local existed version is ${c_red}${current_local_version}${c_normal}, Latest version online is ${c_red}${online_release_version}${c_normal} (${c_blue}${online_release_date}${c_normal})."
        # else
        #     fnBase_ExitStatement "Latest version online (${c_red}${online_release_version}${c_normal}), Release date ($c_red${online_release_date}$c_normal)."
        # fi
    fi

    # - Online & Local Version Comparasion
    if [[ "${is_existed}" -eq 1 ]]; then
        if [[ "${online_release_version}" == "${current_local_version}" ]]; then
            fnBase_ExitStatement "\nLatest version (${c_red}${online_release_version}${c_normal}) has been existed in your system!"
        elif [[ $(fnBase_VersionNumberComparasion "${current_local_version}" "${online_release_version}" '<') -eq 1 ]]; then
            printf "\nExisted version local (${c_red}%s${c_normal}) < Latest version online (${c_red}%s${c_normal})!\n" "${current_local_version}" "${online_release_version}"
        fi
    # else
    #     printf "No %s find in your system!\n" "${software_fullname}"
    fi
}


#########  2-3. Download & Decompress Latest Software  #########
fn_DownloadAndDecompressOperation(){
    local l_operation_type=${l_operation_type:-'installing'}
    [[ "${is_existed}" -eq 1 ]] && l_operation_type='updating'
    fnBase_OperationPhaseStatement "${l_operation_type^}"

    local download_page_html
    download_page_html=$(mktemp -t "${mktemp_format}")
    $download_method "${download_page}" > "${download_page_html}"

    local online_release_downloadlink
    online_release_downloadlink=$(sed -r -n '/'"${os_type}"'/{/http/!d;s@.*href="([^"]+)".*$@\1@g;p}' "${download_page_html}")
    # https://dl2.cdn.filezilla-project.org/client/FileZilla_3.39.0_x86_64-linux-gnu.tar.bz2?h=CuCTQyzcRZZGivo3NP6otA&x=1546227748
    local online_release_dgst=${online_release_dgst:-}
    online_release_dgst=$(sed -r -n '/'"${os_type}"'/,/SHA-512/{/SHA-512/{s@.*>[[:space:]]*([^<]+).*$@\1@g;p}}' "${download_page_html}")
    local online_release_pack_name=${online_release_pack_name:-"${online_release_downloadlink##*/}"}
    [[ "${online_release_pack_name}" =~ \? ]] && online_release_pack_name="${online_release_pack_name%%\?*}"

    local l_is_need_download=${l_is_need_download:-1}
    local l_specified_pack_dir=${l_specified_pack_dir:-}

    # specified file path existed
    if [[ "${source_pack_path}" =~ .tar.bz2$ && -s "${source_pack_path}" ]]; then
        [[ "${source_pack_path##*/}" == "${online_release_pack_name}" ]] || fnBase_ExitStatement "Package ${c_red}${source_pack_path}${c_normal} is not same to latest release version name!"

    # specified dir or ~/Downloads existed
    elif [[ (-n "${source_pack_path}" && -d "${source_pack_path}" ) || -d "${user_download_dir}" ]]; then
        if [[ -d "${source_pack_path}" ]]; then
            l_specified_pack_dir="${source_pack_path}"
        elif [[ -d "${user_download_dir}" ]]; then
            l_specified_pack_dir="${user_download_dir}"
        fi

        l_specified_pack_dir="${l_specified_pack_dir%/}"

        if [[ -d "${l_specified_pack_dir}" && -f "${l_specified_pack_dir}/${online_release_pack_name}" ]]; then
            source_pack_path="${l_specified_pack_dir}/${online_release_pack_name}"
        fi
    fi

    # 1.1- existed package path in system
    if [[ -n "${source_pack_path}" && -f "${source_pack_path}" ]]; then
        fnBase_OperationProcedureStatement 'Existed pack dgst verifying'
        if [[ -n "${online_release_dgst}" && $(fnBase_HashDigestVerification "${source_pack_path}" '512') == "${online_release_dgst}" ]]; then
            fnBase_OperationProcedureResult "${c_red}${source_pack_path%/*}/${c_normal}"
            l_is_need_download=0
        else
            fnBase_OperationProcedureResult '' 1
        fi
    fi

    # 2 - Download Directly From Official Site
    local pack_save_path=${pack_save_path:-}

    if [[ "${l_is_need_download}" -eq 1 ]]; then
        fnBase_OperationProcedureStatement "Downloading ${c_red}${online_release_pack_name}${c_normal}"
        pack_save_path="${temp_save_dir}/${online_release_pack_name}"
        [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"

        $download_method "${online_release_downloadlink}" > "${pack_save_path}"

        if [[ -s "${pack_save_path}" ]]; then
            fnBase_OperationProcedureResult
        else
            [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
            fnBase_OperationProcedureResult '' 1
        fi

        fnBase_OperationProcedureStatement "SHA512 dgst verification"
        if [[ -n "${online_release_dgst}" && $(fnBase_HashDigestVerification "${pack_save_path}" '512') == "${online_release_dgst}"  ]]; then
            fnBase_OperationProcedureResult
        else
            [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
            fnBase_OperationProcedureResult '' 1
        fi

        # use variable "source_pack_path" to execute decompress operation, variable "pack_save_path" used for delete temporary download package
        source_pack_path="${pack_save_path}"
    fi

    # 3 - Decompress & Extract
    fnBase_OperationProcedureStatement "Installing"
    local application_backup_path="${installation_dir}${bak_suffix}"
    [[ -d "${application_backup_path}" ]] && rm -rf "${application_backup_path}"

    [[ -d "${installation_dir}" ]] && mv "${installation_dir}" "${application_backup_path}"
    [[ -d "${installation_dir}" ]] || mkdir -p "${installation_dir}"
    tar xf "${source_pack_path}" -C "${installation_dir}" --strip-components=1 2> /dev/null

    local new_installed_version=${new_installed_version:-}
    new_installed_version=$("${installation_dir}/bin/filezilla" --version 2> /dev/null | sed -r -n 's@^[^[:space:]]*[[:space:]]*([[:digit:].]+).*@\1@g;p')

    if [[ "${online_release_version}" == "${new_installed_version}" ]]; then
        [[ -d "${application_backup_path}" ]] && rm -rf "${application_backup_path}"
        fnBase_OperationProcedureResult "${c_red}${installation_dir%/}/${c_normal}"

        # copy newly downloaded pack to ~/Downloads
        local l_user_download_save_path=${l_user_download_save_path:-"${user_download_dir}/${source_pack_path##*/}"}
        if [[ -d "${user_download_dir}" && ! -f "${l_user_download_save_path}" ]]; then
            cp "${source_pack_path}" "${l_user_download_save_path}"
            chown "${login_user}" "${l_user_download_save_path}"

            # removed old version package existed
            if [[ "${is_existed}" -eq 1 ]]; then
                local l_existed_old_pack_path=${l_existed_old_pack_path:-"${user_download_dir}/${online_release_pack_name//${online_release_version}/${current_local_version}}"}
                [[ -f "${l_existed_old_pack_path}" ]] && rm -f "${l_existed_old_pack_path}"
            fi
        fi

    else
        [[ -d "${installation_dir}" ]] && rm -rf "${installation_dir}"
        [[ "${is_existed}" -eq 1 ]] && mv "${application_backup_path}" "${installation_dir}"
        fnBase_OperationProcedureResult '' 1
    fi

    chown -R root:root "${installation_dir}"
    [[ -n "${pack_save_path}" && -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
}


#########  2-4. Desktop Configuration  #########
fn_DesktopConfiguration(){
    fnBase_OperationPhaseStatement 'Configuration'

    if [[ -d '/usr/share/applications' ]]; then
        fnBase_OperationProcedureStatement 'Desktop Configuration'

        local l_png_path=${l_png_path:-"${installation_dir}/share/icons/hicolor/480x480/apps/filezilla.png"}
        [[ -f "${l_png_path}" ]] && ln -sf "${l_png_path}" "${pixmaps_png_path}"

        # /PATH/share/applications/filezilla.desktop
        echo -e "[Desktop Entry]\nName=FileZilla\nGenericName=FTP client\nGenericName[da]=FTP-klient\nGenericName[de]=FTP-Client\nGenericName[fr]=Client FTP\nComment=Download and upload files via FTP, FTPS and SFTP\nComment[da]=Download og upload filer via FTP, FTPS og SFTP\nComment[de]=Dateien über FTP, FTPS und SFTP übertragen\nComment[fr]=Transférer des fichiers via FTP, FTPS et SFTP\nExec=installation_dir/bin/filezilla\nTerminal=false\nIcon=application_name.png\nType=Application\nCategories=Network;FileTransfer;\nVersion=1.0" > "${application_desktop_path}"

        sed -i -r 's@application_name@'"${application_name}"'@g' "${application_desktop_path}"
        sed -i -r 's@installation_dir@'"${installation_dir}"'@g' "${application_desktop_path}"
        fnBase_OperationProcedureResult "${application_desktop_path%/*}/"
    fi
}


#########  2-5. Operation Time Cost  #########
fn_TotalTimeCosting(){
    finish_time=$(date +'%s')        # End Time Of Operation
    total_time_cost=$((finish_time-start_time))   # Total Time Of Operation
    printf "\nTotal time cost is ${c_red}%s${c_normal} seconds!\n" "${total_time_cost}"
}


#########  3. Executing Process  #########
fn_InitializationCheck

fn_UninstallOperation
fn_VersionComparasion
fn_DownloadAndDecompressOperation
fn_DesktopConfiguration
fn_TotalTimeCosting


#########  4. EXIT Singal Processing  #########
# trap "commands" EXIT # execute command when exit from shell
fn_TrapEXIT(){
    fnBase_UnsetVars
    unset installation_dir
    unset is_existed
    unset version_check
    unset source_pack_path
    unset is_uninstall
    unset proxy_server_specify
}

trap fn_TrapEXIT EXIT

# Script End
