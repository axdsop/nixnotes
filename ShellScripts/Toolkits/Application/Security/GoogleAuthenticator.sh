#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails

# Target: Google Authenticitor Initialization & Configuration On GNU/Linux (RHEL/CentOS/Fedora/Debian/Ubuntu/OpenSUSE and variants)
# Developer: MaxdSre

# Change Log:
# - Oct 08, 2020 08:59 Thu ET - initialization check method update
# - Oct 15, 2019 12:31 Tue ET - change custom script url
# - Oct 14, 2019 16:16 Fri ET - change to new base functions
# - Dec 30, 2018 22:01 Sun ET - code reconfiguration, include base funcions from other file
# - Jul 11, 2018 17:03 Wed ET - logic optimization
# - Mar 22, 2018 17:39 Thu ET

# https://github.com/google/google-authenticator-libpam


#########  0-1. Singal Setting  #########
trap fnBase_TrapCleanUp INT QUIT

#########  0-2. Variables Setting  #########
# umask 022    # temporarily change umask value to 022
readonly custom_shellscript_url='https://gitlab.com/axdsop/nixnotes/raw/master/ShellScripts/Toolkits'
readonly os_check_script="${custom_shellscript_url}/GNULinux/gnuLinuxMachineInfoDetection.sh"

readonly software_fullname=${software_fullname:-'Google Authenticator'}
application_name=${application_name:-'GoogleAuthenticator'}
installation_dir="/opt/${application_name}"
readonly application_name='google-authenticator'
readonly ga_pam_lib_name='pam_google_authenticator.so'
readonly ssh_config_path='/etc/ssh/sshd_config'
readonly sshd_pam_path='/etc/pam.d/sshd'

# gal - Google Authenticator Libpam
gal_lib_path=${gal_lib_path:-"/etc/ld.so.conf.d/${software_fullname// /}.conf"}
gal_execute_path=${gal_execute_path:-"/etc/profile.d/${software_fullname// /}.sh"}

is_existed=${is_existed:-0}
os_detect=${os_detect:-0}
choose_usage_type=${choose_usage_type:-''}
is_uninstall=${is_uninstall:-0}



#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | sudo bash -s -- [options] ...

Google Authenticitor Initialization & Configuration On GNU/Linux!
This script requires superuser privileges (eg. root, su).

[available option]
    -h    --help, show help info
    -o    --os info, detect os distribution info
    -c pam_usage_type    --type available (s|ssh for sshd, d|dm for desktop display manager), default is empty
    -p [protocol:]ip:port    --proxy host (http|https|socks4|socks5), default protocol is http
    -u    --uninstall, uninstall software installed
\e[0m"
}

while getopts "hoc:up:" option "$@"; do
    case "$option" in
        o ) os_detect=1 ;;
        c ) choose_usage_type="$OPTARG" ;;
        u ) is_uninstall=1 ;;
        p ) proxy_server_specify="$OPTARG" ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done



#########  1-2 Preparation Operation  #########
fn_InitializationCheck(){
    local l_user_codebase_dir="$HOME/.config/axdsop"
    local l_base_function_temp_save_path=''
    local l_need_remove=0
    local l_base_function_url='https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/base_functions.sh'
    l_base_function_temp_save_path=$(find "${l_user_codebase_dir}" -type f -name "${l_base_function_url##*/}" -print 2> /dev/null)
    
    if [[ ! -f "${l_base_function_temp_save_path}" ]]; then
        local l_download_tool=${l_download_tool:-'wget -qO-'}
        [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_tool='curl -fsL'
        l_need_remove=1
        l_base_function_temp_save_path=$(mktemp -t XXXXXXX)
        ${l_download_tool} "${l_base_function_url}" > "${l_base_function_temp_save_path}"
    fi

    if [[ -f "${l_base_function_temp_save_path}" && -s "${l_base_function_temp_save_path}" ]]; then
        . "${l_base_function_temp_save_path}"
        [[ "${l_need_remove}" -eq 1 ]] && rm "${l_base_function_temp_save_path}"
    else
        echo "Sorry, fail to get base script path."
        exit
    fi

    # tput clear  # Echo the clear-screen sequence for the current terminal.
    # specify 1 means need sudo privilege
    fnBase_RunningEnvironmentCheck '1'

    if [[ "${os_detect}" -eq 1 ]]; then
        $download_method "${os_check_script}" | bash -s --
        exit
    fi
}

fn_OSInfoDetection(){
    local l_json_osinfo=${l_json_osinfo:-}
    l_json_osinfo=$(fnBase_OSInfoRetrieve "${os_check_script}")
    # [[ -z "${l_json_osinfo}" ]] && fnBase_ExitStatement "${c_red}Fatal${c_normal}: fail to extract os info!"

    distro_fullname=${distro_fullname:-}
    distro_fullname=$(fnBase_OSInfoDirectiveValExtraction 'distro_fullname' "${l_json_osinfo}")

    local distro_is_eol=${distro_is_eol:-0}
    distro_is_eol=$(fnBase_OSInfoDirectiveValExtraction 'distro_is_eol' "${l_json_osinfo}")
    [[ "${distro_is_eol}" -eq 1 ]] && fnBase_ExitStatement "${c_red}Sorry${c_normal}: your system ${c_blue}${distro_fullname}${c_normal} is obsoleted!"

    distro_name=${distro_name:-}
    distro_name=$(fnBase_OSInfoDirectiveValExtraction 'distro_name' "${l_json_osinfo}")
    distro_name="${distro_name%%-*}"    # rhel/centos/fedora/debian/ubuntu/sles/opensuse

    codename=${codename:-}
    codename=$(fnBase_OSInfoDirectiveValExtraction 'distro_codename' "${l_json_osinfo}")

    version_id=${version_id:-}
    version_id=$(fnBase_OSInfoDirectiveValExtraction 'distro_version_id' "${l_json_osinfo}")
    version_id=${version_id%%.*}

    pack_manager=${pack_manager:-}
    pack_manager=$(fnBase_OSInfoDirectiveValExtraction 'distro_pack_manager' "${l_json_osinfo}")
}

fn_OpenSSHDirectiveSetting(){
    local l_item="${1:-}"
    local l_val="${2:-}"
    local l_path=${l_path:-'/etc/ssh/sshd_config'}

    if [[ -n "${l_item}" && -n "${l_val}" && -s "${l_path}" ]]; then
        local l_record_origin=${l_record_origin:-}
        # if result has more then one line, use double quote "" wrap it
        # - whole line start with keyword, format 'PermitEmptyPasswords no'
        l_record_origin=$(sed -r -n '/^'"${l_item}"'[[:space:]]+/{s@[[:space:]]*$@@g;p}' "${l_path}" 2> /dev/null)
        # - whole line inclue keyword start with "#", format '#PermitEmptyPasswords no'
        record_origin_comment=$(sed -r -n '/^#[[:space:]]*'"${l_item}"'[[:space:]]+/{s@[[:space:]]*$@@g;p}' "${l_path}" 2> /dev/null)

        if [[ -z "${l_record_origin}" ]]; then
            if [[ -z "${record_origin_comment}" ]]; then
                # append at the end of file
                sed -i -r '$a '"${l_item} ${l_val}"'' "${l_path}" 2> /dev/null
            else
                # append at the end of the directive which is commented by #
                sed -i -r '/^#[[:space:]]*'"${l_item}"'[[:space:]]+/a '"${l_item} ${l_val}"'' "${l_path}" 2> /dev/null
            fi
        else
            if [[ "${l_record_origin##* }" != "${l_val}" ]]; then
                if [[ "${l_val}" =~ @ ]]; then
                    sed -i -r '/^'"${l_item}"'[[:space:]]+/{s/.*/'"${l_item} ${l_val}"'/;}' "${l_path}" 2> /dev/null
                else
                    sed -i -r '/^'"${l_item}"'[[:space:]]+/{s@.*@'"${l_item} ${l_val}"'@;}' "${l_path}" 2> /dev/null
                fi
            fi
        fi
    fi
}


#########  2-0. Check Existed Or Not  #########
fn_CheckIfExisted(){
    if [[ "${is_uninstall}" -eq 0 ]]; then
        fnBase_CentralOutputTitle "${software_fullname}"
        fnBase_OperationPhaseStatement 'Check If Existed'
        fnBase_OperationProcedureStatement 'Detection'
    fi

    if [[ -d "${installation_dir}/bin" && -s "${installation_dir}/bin/${application_name}" ]]; then
        is_existed=1
        # [[ "${is_uninstall}" -eq 0 ]] && fnBase_ExitStatement "${software_fullname} existed in ${c_yellow}${installation_dir}${c_normal}!"
        [[ "${is_uninstall}" -eq 0 ]] && fnBase_OperationProcedureResult "existed in ${c_yellow}${installation_dir}${c_normal}"
    else
        [[ "${is_uninstall}" -eq 0 ]] && fnBase_OperationProcedureResult 'not exist'
    fi
}

fn_Preprocessing(){
    # - PAM type choose
    case "${choose_usage_type}" in
        s|ssh ) choose_usage_type='ssh';;
        d|dm ) choose_usage_type='desktop';;
    esac

    # - PAM Lib dir
    pam_lib_dir=${pam_lib_dir:-''}
    if [[ -d '/lib64/security' ]]; then
        # CentOS & RHEL & SLE
        pam_lib_dir='/lib64/security'
    elif [[ -d '/lib/x86_64-linux-gnu/security' ]]; then
        # Debian & Ubuntu
        pam_lib_dir='/lib/x86_64-linux-gnu/security'
    fi

    login_ssh_authorize_path=${login_ssh_authorize_path:-"${login_user_home}/.ssh/authorized_keys"}
    pga_statement=${pga_statement:-"auth required pam_google_authenticator.so"}
    pga_secret_key_name=${pga_secret_key_name:-"${application_name//-/_}"}
    login_gpa_secret_key_path=${login_gpa_secret_key_path:-"${login_user_home}/.${pga_secret_key_name}"}
}


#########  2-1. PAM Configuration  #########
fn_PAMForSSHConfiguration(){
    local l_is_uninstall=${1:-0}

    [[ "${l_is_uninstall}" -eq 0 ]] && fnBase_OperationProcedureStatement 'PAM fo SSH'
    local l_comment_str=${l_comment_str:-''}
    if [[ "${l_is_uninstall}" -eq 0 ]]; then
        if [[ -f "${login_user_home}/.ssh/.${pga_secret_key_name}" ]]; then
            pga_statement="${pga_statement} secret=\${HOME}/.ssh/.${pga_secret_key_name} [authtok_prompt=Verification code: ]"
        elif [[ -f "${login_gpa_secret_key_path}" ]]; then
            pga_statement="${pga_statement} secret=\${HOME}/.${pga_secret_key_name} [authtok_prompt=Verification code: ]"
        fi

        [[ -f "${login_ssh_authorize_path}" ]] && l_comment_str='#'
    else
        l_comment_str=''
    fi

    sed -r -i '/pam_google_authenticator.so/d' "${sshd_pam_path}" 2> /dev/null

    # Digital Ocean - CentOS 6 OpenSSH_5.3p1 not work well
    case "${distro_name}" in
        centos|rhel )
            case "${version_id}" in
                7 )
                    [[ "${l_is_uninstall}" -eq 0 ]] && sed -r -i '/^#*auth[[:space:]]+substack[[:space:]]+password-auth/i '"${pga_statement}"'' "${sshd_pam_path}" 2> /dev/null
                    sed -r -i '/^#*auth[[:space:]]+substack[[:space:]]+password-auth/{s@^#*(.*)$@'"${l_comment_str}"'\1@g;}' "${sshd_pam_path}" 2> /dev/null
                    ;;
                6 )
                    [[ "${l_is_uninstall}" -eq 0 ]] && sed -r -i '/^#*auth[[:space:]]+required[[:space:]]+pam_sepermit.so/i '"${pga_statement}"'' "${sshd_pam_path}" 2> /dev/null
                    # sed -r -i '/^#*auth[[:space:]]+include[[:space:]]+password-auth/{s@^#*(.*)$@'"${l_comment_str}"'\1@g;}' "${sshd_pam_path}" 2> /dev/null
                    ;;
            esac
            ;;
        fedora|amzn )
            # fedora 26, 27 approved
            # Amazon AMI 1, 2 approved
            [[ "${l_is_uninstall}" -eq 0 ]] && sed -r -i '/^#*auth[[:space:]]+substack[[:space:]]+password-auth/i '"${pga_statement}"'' "${sshd_pam_path}" 2> /dev/null
            sed -r -i '/^#*auth[[:space:]]+substack[[:space:]]+password-auth/{s@^#*(.*)$@'"${l_comment_str}"'\1@g;}' "${sshd_pam_path}" 2> /dev/null
            ;;
        debian|ubuntu )
            # [[ "${l_is_uninstall}" -eq 0 ]] && sed -r -i '$a '"${pga_statement}"'' "${sshd_pam_path}" 2> /dev/null
            [[ "${l_is_uninstall}" -eq 0 ]] && sed -r -i '/^#*@include[[:space:]]*common-auth/a '"${pga_statement}"'' "${sshd_pam_path}" 2> /dev/null
            sed -r -i '/^#*@include[[:space:]]*common-auth/{s/^#*(.*)$/'"${l_comment_str}"'\1/g;}' "${sshd_pam_path}" 2> /dev/null

            sed -r -i '/^#*@include[[:space:]]*common-password/{s/^#*(.*)$/\1/g;}' "${sshd_pam_path}" 2> /dev/null
            ;;
        sles|opensuse )
            [[ "${l_is_uninstall}" -eq 0 ]] && sed -r -i '/^#*auth[[:space:]]+include[[:space:]]+common-auth/a '"${pga_statement}"'' "${sshd_pam_path}" 2> /dev/null
            sed -r -i '/^#*auth[[:space:]]+include[[:space:]]+common-auth/{s@^#*(.*)$@'"${l_comment_str}"'\1@g;}' "${sshd_pam_path}" 2> /dev/null
            ;;
        * )
            fnBase_ExitStatement "${c_red}Sorry${c_normal}: this script doesn't support PAM configuration for your system ${c_yellow}${distro_fullname}${c_normal} currently."
            ;;
    esac

    [[ "${l_is_uninstall}" -eq 0 ]] && fnBase_OperationProcedureResult "${sshd_pam_path}"

    # - sshd config
    if [[ -s "${ssh_config_path}" ]]; then
        [[ "${l_is_uninstall}" -eq 0 ]] && fnBase_OperationProcedureStatement 'sshd_config Configuration'
        fn_OpenSSHDirectiveSetting 'UsePAM' 'yes'

        local l_challenge_response_authentication=${l_challenge_response_authentication:-'yes'}
        if [[ "${l_is_uninstall}" -ne 0 ]]; then
            l_challenge_response_authentication='no'
            sed -r -i '/^#*AuthenticationMethods[[:space:]]+/d' "${sshd_pam_path}" 2> /dev/null
        fi

        fn_OpenSSHDirectiveSetting 'ChallengeResponseAuthentication' "${l_challenge_response_authentication}"

        if [[ -f "${login_ssh_authorize_path}" ]]; then
            fn_OpenSSHDirectiveSetting 'PasswordAuthentication' 'no'

            if [[ "${l_is_uninstall}" -eq 0 ]]; then
                local ssh_version=${ssh_version:-0}
                ssh_version=$(ssh -V 2>&1 | sed -r -n 's@.*_([[:digit:].]{3}).*@\1@p')

                # publickey (SSH key), password publickey (password), keyboard-interactive (verification code)
                [[ $(expr "${ssh_version}" \>= 6.7) -eq 1 ]] && fn_OpenSSHDirectiveSetting 'AuthenticationMethods' 'publickey,password publickey,keyboard-interactive'
            else
                fn_OpenSSHDirectiveSetting 'AuthenticationMethods' 'publickey'
            fi
        else
            fn_OpenSSHDirectiveSetting 'PasswordAuthentication' 'yes'

            if [[ "${l_is_uninstall}" -eq 0 ]]; then
                sed -r -i '/^#*AuthenticationMethods[[:space:]]+/d' "${ssh_config_path}" 2> /dev/null
            fi
        fi

        # restart sshd service
        fnBase_SystemServiceManagement 'sshd' 'restart'
        [[ "${l_is_uninstall}" -eq 0 ]] && fnBase_OperationProcedureResult "${ssh_config_path}"
    fi
}

fn_PAMForDESKTOPConfiguration(){
    local l_is_uninstall=${1:-0}

    [[ "${l_is_uninstall}" -eq 0 ]] && fnBase_OperationProcedureStatement 'PAM For display manager'
    # GNOME Desktop: /etc/pam.d/gdm-password
    # Unity Desktop for Ubuntu: /etc/pam.d/lightdm
    local l_dm_pam_path=${l_dm_pam_path:-}

    if [[ -f '/etc/pam.d/lightdm' ]]; then
        l_dm_pam_path='/etc/pam.d/lightdm'
        sed -r -i '/pam_google_authenticator.so/d' "${l_dm_pam_path}" 2> /dev/null

        if [[ "${l_is_uninstall}" -eq 0 ]]; then
            case "${distro_name}" in
                ubuntu )
                    sed -r -i '/^#*auth[[:space:]]+requisite[[:space:]]+pam_nologin.so/i '"${pga_statement}"'' "${l_dm_pam_path}" 2> /dev/null
                    ;;
            esac
        fi
    elif [[ -f '/etc/pam.d/gdm-password' ]]; then
        l_dm_pam_path='/etc/pam.d/gdm-password'
        sed -r -i '/pam_google_authenticator.so/d' "${l_dm_pam_path}" 2> /dev/null

        if [[ "${l_is_uninstall}" -eq 0 ]]; then
            case "${distro_name}" in
                centos ) sed -r -i '/^#*auth[[:space:]]+substack[[:space:]]+password-auth/i '"${pga_statement}"'' "${l_dm_pam_path}" 2> /dev/null ;;
                debian ) sed -r -i '/^#*auth[[:space:]]+requisite[[:space:]]+pam_nologin.so/i '"${pga_statement}"'' "${l_dm_pam_path}" 2> /dev/null ;;
                opensuse ) sed -r -i '/^#*auth[[:space:]]+include[[:space:]]+common-auth/i '"${pga_statement}"'' "${l_dm_pam_path}" 2> /dev/null ;;
            esac
        fi
    fi
    [[ "${l_is_uninstall}" -eq 0 ]] && fnBase_OperationProcedureResult "${l_dm_pam_path}"
}


#########  2-2. Uninstall Operation  #########
fn_UninstallOperation(){
    if [[ "${is_uninstall}" -eq 1 ]]; then
        if [[ -d "${installation_dir}" && -d "${installation_dir}/bin" ]]; then
            [[ -d "${installation_dir}" ]] && rm -rf "${installation_dir}"
            [[ -f "${gal_lib_path}" ]] && rm -f "${gal_lib_path}"
            [[ -f "${gal_execute_path}" ]] && rm -f "${gal_execute_path}"
            [[ -L "${pam_lib_dir}/${ga_pam_lib_name}" ]] && rm -f "${pam_lib_dir}/${ga_pam_lib_name}"

            [[ -f "${login_user_home}/${application_name}" ]] && rm -f "${login_user_home}/${application_name}" 2> /dev/null
            [[ -f "${login_gpa_secret_key_path}" ]] && rm -f "${login_gpa_secret_key_path}" 2> /dev/null
            [[ -f "${login_user_home}/.ssh/.${pga_secret_key_name}" ]] && rm -f "${login_user_home}/.ssh/.${pga_secret_key_name}" 2> /dev/null

            rm -f "${login_user_home}"/.google_authenticator~* 2> /dev/null

            # uninstall
            fn_PAMForSSHConfiguration '1'
            fn_PAMForDESKTOPConfiguration '1'

            [[ -d "${installation_dir}" ]] || fnBase_ExitStatement "${software_fullname} is successfully removed from your system!"
        else
            fnBase_ExitStatement "${c_blue}Note${c_normal}: no ${software_fullname} is found in your system!"
        fi
    fi
}


#########  2-3. Essential Package Installation  #########
fn_EssentialPackInstallation(){
    fnBase_OperationProcedureStatement 'Essential packages installation'

    local l_package_list=${l_package_list:-'gcc make autoconf automake libtool'}
    case "${pack_manager}" in
        apt-get ) l_package_list="${l_package_list} libpam0g-dev libqrencode3" ;;
        zypper ) l_package_list="${l_package_list} pam-devel" ;;
        yum|dnf ) l_package_list="${l_package_list} pam-devel" ;;
    esac
    fnBase_PackageManagerOperation 'install' "${l_package_list}"
    fnBase_OperationProcedureResult 'gcc make autoconf automake ...'
}


#########  2-4. Downloading & Decompression  #########
fn_DownloadAndDecompressOperation(){
    fnBase_OperationProcedureStatement 'Downloading & Decompression'

    # https://github.com/google/google-authenticator-libpam.git
    # https://github.com/google/google-authenticator-libpam/archive/master.zip

    local l_git_link=${l_git_link:-"https://github.com/google/google-authenticator-libpam"}
    decompress_dir=${decompress_dir:-"${login_user_home}/PAM_GA_$(date +'%F')_$RANDOM"}
    [[ -d "${decompress_dir}" ]] && rm -rf "${decompress_dir}"

    if fnBase_CommandExistIfCheck 'git'; then
        git clone -q "${l_git_link}.git" "${decompress_dir}"
    elif fnBase_CommandExistIfCheck 'unzip'; then
        local l_package_path=${l_package_path:-"${temp_save_dir}/${application_name}.zip"}
        $download_method "${l_git_link}/archive/master.zip" > "${l_package_path}"
        unzip -q "${l_package_path}" -d "${login_user_home}" 2> /dev/null
        [[ -f "${l_package_path}" ]] && rm -f "${l_package_path}"

        [[ -d "${login_user_home}/${application_name}-libpam-master" ]] && mv "${login_user_home}/${application_name}-libpam-master" "${decompress_dir}"
    else
        fnBase_ExitStatement "${c_red}Sorry${c_normal}: fail to find command ${c_blue}git${c_normal} or ${c_blue}unzip${c_normal}!"
    fi

    if [[ -f "${decompress_dir}/bootstrap.sh" ]]; then
        fnBase_OperationProcedureResult "${decompress_dir}"
    else
        [[ -d "${decompress_dir}" ]] && rm -rf "${decompress_dir}"
        fnBase_OperationProcedureResult "${decompress_dir}" 1
        # fnBase_ExitStatement "${c_red}Sorry${c_normal}: fail to download & decompress ${c_blue}${application_name}${c_normal}!"
    fi
}

#########  2-5. Compiling & Configuration  #########
fn_CompilingAndConfiguration(){
    # Compiling
    if [[ -d "${decompress_dir}" ]]; then
        fnBase_OperationProcedureStatement 'Compiling & Installation'
        cd "${decompress_dir}" || return
        ./bootstrap.sh &> /dev/null
        ./configure --prefix="${installation_dir}" &> /dev/null
        make -j 4 &> /dev/null
        make install &> /dev/null

        [[ -d "${decompress_dir}" ]] && rm -rf "${decompress_dir}"

        if [[ -d "${installation_dir}/bin" ]]; then
            fnBase_OperationProcedureResult "${installation_dir}"
        else
            [[ -d "${installation_dir}" ]] && rm -rf "${installation_dir}"
            fnBase_OperationProcedureResult "${installation_dir}" 1
        fi
    fi

    # Configuration
    local l_bin_dir=${l_bin_dir:-"${installation_dir}/bin"}

    if [[ -d "${l_bin_dir}" ]]; then
        fnBase_OperationProcedureStatement "\$PATH configuration"
        # 1 - add to $PATH
        echo "export PATH=\$PATH:${l_bin_dir}" > "${gal_execute_path}"
        fnBase_OperationProcedureResult "${gal_execute_path}"

        # 2 - export libary file
        # /PATH/lib/security: Debian/Ubuntu/CentOS/RHEL/SLES
        # /PATH/lib64/security: OpenSUSE
        local l_lib_dir=${l_lib_dir:-"${installation_dir}/lib"}
        [[ -d "${installation_dir}/lib64" ]] && l_lib_dir="${installation_dir}/lib64"

        if [[ -d "${l_lib_dir}" ]]; then
            fnBase_OperationProcedureStatement 'Libary export'
            echo "${l_lib_dir}" > "${gal_lib_path}"
            # regenerate caceh, Debian /sbin/ldconfig
            fnBase_CommandExistIfCheck 'ldconfig' && ldconfig -v &> /dev/null
            fnBase_OperationProcedureResult "${gal_lib_path}"

            # 3 - create soft link to PAM lib dir
            if [[ -s "${l_lib_dir}/security/${ga_pam_lib_name}" ]]; then
                fnBase_OperationProcedureStatement 'Soft link creation'
                ln -fs "${l_lib_dir}/security/${ga_pam_lib_name}" "${pam_lib_dir}" 2> /dev/null
                fnBase_OperationProcedureResult "${pam_lib_dir}"
            fi
        fi

    fi

    # - generate secret key
    local l_authenticator_path=${l_authenticator_path:-"${installation_dir}/bin/${application_name}"}
    if [[ -f "${l_authenticator_path}" ]]; then
        fnBase_OperationProcedureStatement 'Secret key generation'

        # Run the google-authenticator binary to create a new secret key in your home directory. These settings will be stored in ~/.google_authenticator.
        # -e requires an argument in the range 0..10
        # https://github.com/koalaman/shellcheck/wiki/SC2069

        [[ -f "/root/.${pga_secret_key_name}" ]] && rm -f "/root/.${pga_secret_key_name}"

        echo 'y' 2> /dev/null | "${l_authenticator_path}" -t -d -f -Q UTF8 -r 3 -R 60 -W -e 8 &> "${login_user_home}/${application_name}"

        if [[ "${login_user}" != 'root' && ! -f "${login_gpa_secret_key_path}" && -f "/root/.${pga_secret_key_name}" ]]; then
            mv "/root/.${pga_secret_key_name}" "${login_gpa_secret_key_path}"
            chown "${login_user}" "${login_gpa_secret_key_path}"
        fi

        fnBase_OperationProcedureResult "${login_gpa_secret_key_path}"

        if [[ -f "${login_gpa_secret_key_path}" ]]; then
            if [[ -d "${login_user_home}/.ssh" ]]; then
                fnBase_OperationProcedureStatement 'Secret key relocation'
                cp -f "${login_gpa_secret_key_path}" "${login_user_home}/.ssh"
                chown "${login_user}" "${login_user_home}/.ssh/.${pga_secret_key_name}"
                fnBase_OperationProcedureResult "${login_user_home}/.ssh/.${pga_secret_key_name}"
            fi
        else
            echo "Fail to create secret key!"
            # fn_UninstallOperation
        fi
    fi
}


#########  4. Operation Time Cost  #########
fn_TotalTimeCosting(){
    finish_time=$(date +'%s')        # End Time Of Operation
    total_time_cost=$((finish_time-start_time))   # Total Time Of Operation
    printf "\nTotal time cost is ${c_red}%s${c_normal} seconds!\n" "${total_time_cost}"

    if [[ -s "${login_user_home}/${application_name}" && -n $(sed -r -n '/google.com/p' "${login_user_home}/${application_name}" 2> /dev/null) ]]; then
        local l_otp_link=${l_otp_link:-}
        l_otp_link=$(sed -r -n '/google.com/{s@^[[:space:]]*@@g;p}' "${login_user_home}/${application_name}" 2> /dev/null)

        printf "\n${c_bold}${c_red}Warning:${c_normal} pasting the following URL into your browser exposes the OTP secret to Google\n\n${c_red}%s${c_normal}\n\nMore details stores in ${c_yellow}%s${c_normal}.\n\n" "${l_otp_link}" "${login_user_home}/${application_name}"
        # Warning: pasting the following URL into your browser exposes the OTP secret to Google
    fi
}


#########  5. Executing Process  #########
fn_InitializationCheck
fn_OSInfoDetection
fn_Preprocessing
fn_CheckIfExisted

[[ "${is_uninstall}" -eq 1 ]] && fn_UninstallOperation

if [[ "${is_existed}" -ne 1 ]]; then
    fnBase_OperationPhaseStatement 'Installation'
    fn_EssentialPackInstallation
    fn_DownloadAndDecompressOperation
    fn_CompilingAndConfiguration
fi

if [[ -n "${choose_usage_type}" ]]; then
    fnBase_OperationPhaseStatement 'PAM Configuration'
    fn_PAMFor${choose_usage_type^^}Configuration
fi

if [[ "${is_existed}" -ne 1 ]]; then
    fn_TotalTimeCosting
fi


#########  6. EXIT Singal Processing  #########
# trap "commands" EXIT # execute command when exit from shell
fn_TrapEXIT(){
    fnBase_UnsetVars
    unset installation_dir
    unset gal_lib_path
    unset gal_execute_path
    unset is_existed
    unset os_detect
    unset choose_usage_type
    unset is_uninstall
    unset pam_lib_dir
    unset login_ssh_authorize_path
    unset pga_statement
    unset pga_secret_key_name
    unset login_gpa_secret_key_path
}

trap fn_TrapEXIT EXIT


# Libraries have been installed in:
#    /opt/GoogleAuthenticator/lib/security
#
# If you ever happen to want to link against installed libraries
# in a given directory, LIBDIR, you must either use libtool, and
# specify the full pathname of the library, or use the '-LLIBDIR'
# flag during linking and do at least one of the following:
#    - add LIBDIR to the 'LD_LIBRARY_PATH' environment variable
#      during execution
#    - add LIBDIR to the 'LD_RUN_PATH' environment variable
#      during linking
#    - use the '-Wl,-rpath -Wl,LIBDIR' linker flag
#    - have your system administrator add LIBDIR to '/etc/ld.so.conf'
#
# See any operating system documentation about shared libraries for
# more information, such as the ld(1) and ld.so(8) manual pages.
#

# Script End
