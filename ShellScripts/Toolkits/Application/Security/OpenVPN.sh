#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails


# Documentation
# - https://openvpn.net/download-open-vpn/
# - https://openvpn.net/index.php/open-source/documentation/howto.html
# - https://blog.g3rt.nl/openvpn-security-tips.html


# Target: Setting Up OpenVPN On GNU/Linux (Deprecated)
# Developer: MaxdSre

# Change Log:
# - Oct 08, 2020 09:01 Thu ET - initialization check method update
# - Oct 15, 2019 12:33 Tue ET - change custom script url
# - Oct 14, 2019 15:02 Fri ET - change to new base functions
# - Dec 31, 2018 20:44 Mon ET - code reconfiguration, include base funcions from other file
# - Jun 02, 2018 14:27 Sat ET - add CleanBrowsing DNS
# - May 30, 2018 16:17 Wed ET - add dns server choose, currently just support ufw, firewalld, doesn't support SLES family
# - Feb 29, 2018 16:04 Tue ET - first draft


# SLES
# https://www.suse.com/documentation/sles-12/book_security/data/cha_security_vpnserver.html
# https://doc.opensuse.org/documentation/leap/security/single-html/book.security/index.html#cha.security.vpnserver



#########  0-1. Singal Setting  #########
trap fnBase_TrapCleanUp INT QUIT

#########  0-2. Variables Setting  #########
# umask 022    # temporarily change umask value to 022
readonly custom_shellscript_url='https://gitlab.com/axdsop/nixnotes/raw/master/ShellScripts/Toolkits'
readonly os_check_script="${custom_shellscript_url}/GNULinux/gnuLinuxMachineInfoDetection.sh"
readonly port_generation_script="${custom_shellscript_url}/Network/random_available_port_generate.sh"
readonly firewall_configuration_script="${custom_shellscript_url}/GNULinux/gnuLinuxFirewallRuleConfiguration.sh"

readonly software_fullname='OpenVPN'
conf_dir='/etc/openvpn'
server_conf_path="${conf_dir}/server.conf"
client_conf_path="${conf_dir}/client.conf"
easyrsa_dir="${conf_dir}/easy-rsa"
easyrsa_path="${easyrsa_dir}/easyrsa"
pki_dir="${easyrsa_dir}/pki"
tls_auth_key="${conf_dir}/ta.key"
protocol='udp'
readonly openvpn_port_default='1194'
openvpn_port=${openvpn_port:-"${openvpn_port_default}"}
dns_choose=${dns_choose:-}
new_client=${new_client:-}
proxy_server_specify=${proxy_server_specify:-}


#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | sudo bash -s -- [options] ...

Installing / Updating Golang Programming Language On GNU/Linux!
This script requires superuser privileges (eg. root, su).

[available option]
    -h    --help, show help info
    -n new_client    --create new client .ovpn file
    -s port    --specify OpenVPN port, default is 1194
    -d dns    --choose public DNS servers (Google,CloudFlare,Dyn,CleanBrowsing,Yandex,OpenDNS,FreeDNS), default is CloudFlare DNS
    -p [protocol:]ip:port    --proxy host (http|https|socks4|socks5), default protocol is http
    -u    --uninstall, uninstall software installed
\e[0m"
}

while getopts "hn:s:d:up:" option "$@"; do
    case "$option" in
        n ) new_client="$OPTARG" ;;
        s ) openvpn_port="$OPTARG" ;;
        d ) dns_choose="$OPTARG" ;;
        # u ) is_uninstall=1 ;;
        p ) proxy_server_specify="$OPTARG" ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done



#########  1-2 Preparation Operation  #########
fn_InitializationCheck(){
    local l_user_codebase_dir="$HOME/.config/axdsop"
    local l_base_function_temp_save_path=''
    local l_need_remove=0
    local l_base_function_url='https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/base_functions.sh'
    l_base_function_temp_save_path=$(find "${l_user_codebase_dir}" -type f -name "${l_base_function_url##*/}" -print 2> /dev/null)
    
    if [[ ! -f "${l_base_function_temp_save_path}" ]]; then
        local l_download_tool=${l_download_tool:-'wget -qO-'}
        [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_tool='curl -fsL'
        l_need_remove=1
        l_base_function_temp_save_path=$(mktemp -t XXXXXXX)
        ${l_download_tool} "${l_base_function_url}" > "${l_base_function_temp_save_path}"
    fi

    if [[ -f "${l_base_function_temp_save_path}" && -s "${l_base_function_temp_save_path}" ]]; then
        . "${l_base_function_temp_save_path}"
        [[ "${l_need_remove}" -eq 1 ]] && rm "${l_base_function_temp_save_path}"
    else
        echo "Sorry, fail to get base script path."
        exit
    fi

    # tput clear  # Echo the clear-screen sequence for the current terminal.
    # specify 1 means need sudo privilege
    fnBase_RunningEnvironmentCheck '1'
}

fn_OSInfoDetection(){
    local l_json_osinfo=${l_json_osinfo:-}
    l_json_osinfo=$(fnBase_OSInfoRetrieve "${os_check_script}")
    # [[ -z "${l_json_osinfo}" ]] && fnBase_ExitStatement "${c_red}Fatal${c_normal}: fail to extract os info!"

    distro_fullname=${distro_fullname:-}
    distro_fullname=$(fnBase_OSInfoDirectiveValExtraction 'distro_fullname' "${l_json_osinfo}")

    pack_manager=${pack_manager:-}
    pack_manager=$(fnBase_OSInfoDirectiveValExtraction 'distro_pack_manager' "${l_json_osinfo}")

    firewall_type=${firewall_type:-}
    firewall_type=$(fnBase_OSInfoDirectiveValExtraction 'distro_firewall_type' "${l_json_osinfo}")

    [[ "${pack_manager}" == 'zypper' || "${firewall_type}" == 'iptables' ]] && fnBase_ExitStatement "${c_red}Sorry${c_normal}: this script doesn't support your system ${c_yellow}${distro_fullname}${c_normal} currently!"

    distro_name=${distro_name:-}
    distro_name=$(fnBase_OSInfoDirectiveValExtraction 'distro_name' "${l_json_osinfo}")
    distro_name="${distro_name%%-*}"    # rhel/centos/fedora/debian/ubuntu/sles/opensuse

    codename=${codename:-}
    codename=$(fnBase_OSInfoDirectiveValExtraction 'distro_codename' "${l_json_osinfo}")

    version_id=${version_id:-}
    version_id=$(fnBase_OSInfoDirectiveValExtraction 'distro_version_id' "${l_json_osinfo}")

    ip_local=${ip_local:-}
    ip_local=$(fnBase_OSInfoDirectiveValExtraction 'ip_local' "${l_json_osinfo}")

    ip_public=${ip_public:-}
    ip_public=$(fnBase_OSInfoDirectiveValExtraction 'ip_public' "${l_json_osinfo}")

    ip_public_locate=${ip_public_locate:-}
    ip_public_locate=$(fnBase_OSInfoDirectiveValExtraction 'ip_public_locate' "${l_json_osinfo}")

    ip_public_country_code=${ip_public_country_code:-}
    ip_public_country_code=$(fnBase_OSInfoDirectiveValExtraction 'ip_public_country_code' "${l_json_osinfo}")

    version_id=${version_id%%.*}
}

fn_SELinuxSemanageOperation(){
    # selinux type / selinux boolean
    local l_item="${1:-}"
    # port num / on/off
    local l_val="${2:-}"
    # port/boolean/fcontext
    local l_type="${3:-'port'}"
    # add -a/delete -d/modify -m for port
    local l_action="${4:-'add'}"
    # tcp/udp
    local l_protocol="${5:-'tcp'}"

    # semanage fcontext -l | grep ssh_home_t
    # semanage fcontext -a -t ssh_home_t "${login_user_home}/.ssh/"
    # restorecon -v "${login_user_home}/.ssh/"

    # semanage boolean -l | grep ssh
    # getsebool ssh_keysign
    # setsebool ssh_keysign on      #temporarily modify until reboot
    # setsebool -P ssh_keysign on   # persist modify

    # semanage port -l | grep ssh
    # semanage port -a -t ssh_port_t -p tcp 22

    if [[ -n "${l_item}" && -n "${l_val}" ]]; then
        case "${l_type,,}" in
            fcontext|f )
                if fnBase_CommandExistIfCheck 'semanage'; then
                    case "${l_action,,}" in
                        add|a ) l_action='--add' ;;
                        delete|d) l_action='--delete' ;;
                        modify|m) l_action='--modify' ;;
                    esac
                    l_val="${l_val%/}"
                    semanage fcontext ${l_action} -t "${l_item}" "${l_val}(/.*)?" 2> /dev/null
                    fnBase_CommandExistIfCheck 'restorecon' &&  restorecon -F -R "${l_val}" 2> /dev/null
                fi
                ;;
            boolean|b )
                if fnBase_CommandExistIfCheck 'setsebool'; then
                    [[ "${l_val}" != 'on' ]] && l_val='off'
                    setsebool "${l_item}" "${l_val}" 2> /dev/null
                    setsebool -P "${l_item}" "${l_val}" 2> /dev/null
                fi
                ;;
            port|p )
                if fnBase_CommandExistIfCheck 'semanage'; then
                    case "${l_action,,}" in
                        add|a ) l_action='--add' ;;
                        delete|d) l_action='--delete' ;;
                        modify|m) l_action='--modify' ;;
                    esac

                    case "${l_protocol,,}" in
                        tcp ) l_protocol='tcp' ;;
                        udp ) l_protocol='udp' ;;
                    esac
                    semanage port "${l_action}" -t "${l_item}" -p "${l_protocol}" "${l_val}" 2> /dev/null
                fi
                ;;
        esac
    fi
}


#########  2-1. Operation  #########
fn_DirectiveSetting(){
    local l_item=${1:-}
    local l_val=${2:-}
    local l_path=${3:-}
    local l_type=${4:-}

    if [[ -n "${l_item}" && -n "${l_val}" && -f "${l_path}" ]]; then
        if [[ "${l_type,,}" == 'set_var' ]]; then
            sed -r -i '/set_var[[:space:]]+'"${l_item}"'[[:space:]]*/{s@^(.*?'"${l_item}"'[[:space:]]*).*@\1'"${l_val}"'@g;}' "${l_path}" 2> /dev/null
        else
            sed -r -i '/^'"${l_item}"'[[:space:]]*/{s@([^[:space:]]+[[:space:]]*).*@\1'"${l_val}"'@g;}' "${l_path}" 2> /dev/null
        fi
    fi
}

fn_PKIConfiguration(){
    # https://community.openvpn.net/openvpn/wiki/GettingStartedwithOVPN
    # https://github.com/OpenVPN/easy-rsa/blob/master/README.quickstart.md

    fnBase_OperationProcedureStatement 'vars settging'
    fn_DirectiveSetting 'EASYRSA_CRL_DAYS' '3650' "${easyrsa_path}" 'set_var'
    # fn_DirectiveSetting 'EASYRSA_KEY_SIZE' '4096' "${easyrsa_path}" 'set_var'
    fn_DirectiveSetting 'EASYRSA_DIGEST' 'sha512' "${easyrsa_path}" 'set_var'
    fn_DirectiveSetting 'EASYRSA_REQ_COUNTRY' "\"${ip_public_country_code}\"" "${easyrsa_path}" 'set_var'
    fn_DirectiveSetting 'EASYRSA_REQ_PROVINCE'    "\"${ip_public_locate%%.*}\"" "${easyrsa_path}" 'set_var'
    fn_DirectiveSetting 'EASYRSA_REQ_CITY'    "\"${ip_public_locate##*.}\"" "${easyrsa_path}" 'set_var'
    fn_DirectiveSetting 'EASYRSA_REQ_ORG' "\"MaxdSre\"" "${easyrsa_path}" 'set_var'
    # set_var EASYRSA_REQ_EMAIL   me@example.net
    fn_DirectiveSetting 'EASYRSA_REQ_OU' "\"Community\"" "${easyrsa_path}" 'set_var'
    fnBase_OperationProcedureResult "${easyrsa_path}"

    # New PKI and CA
    fnBase_OperationPhaseStatement 'PKI Setting'
    cd "${easyrsa_dir}" || exit

    fnBase_OperationProcedureStatement 'init-pki'
    "${easyrsa_path}" init-pki  &> /dev/null
    if [[ -d "${pki_dir}" ]]; then
        fnBase_OperationProcedureResult "${pki_dir}"
    else
        fnBase_OperationProcedureResult '' 1
    fi

    fnBase_OperationProcedureStatement 'build-ca'
    "${easyrsa_path}" --batch build-ca nopass  &> /dev/null
    fnBase_OperationProcedureResult

    # Generating Diffie-Hellman (DH) params
    # sed -r -i '/dh.pem/{s@dh.pem@dhparam.pem@g;p}' "${easyrsa_path}"
    fnBase_OperationProcedureStatement 'gen-dh'
    "${easyrsa_path}" gen-dh &> /dev/null
    fnBase_OperationProcedureResult

    # Generating a CRL
    fnBase_OperationProcedureStatement 'gen-crl'
    "${easyrsa_path}" gen-crl  &> /dev/null
    # CRL is read with each client connection, when OpenVPN is dropped to nobody
    # rhel: nogroup  / sles, debian: nobody
    nogroup_name=${nogroup_name:-'nogroup'}
    local l_group_path=${l_group_path:-}
    if [[ -f /etc/gshadow ]]; then
        l_group_path='/etc/gshadow'
    elif [[ -f /etc/group ]]; then
        l_group_path='/etc/group'    #SLES
    fi
    [[ -n $(sed -r -n '/^nobody:/{s@^([^:]+).*@\1@g;p}' "${l_group_path}") ]] && nogroup_name='nobody'
    chown nobody:"${nogroup_name}" "${pki_dir}/crl.pem"
    fnBase_OperationProcedureResult

    # Generating Server/Client Side Certificate
    fnBase_OperationProcedureStatement 'Server certificate generating'
    local SERVER='server'
    "${easyrsa_path}" build-server-full ${SERVER} nopass  &> /dev/null
    fnBase_OperationProcedureResult "${SERVER}"

    fnBase_OperationProcedureStatement 'Client certificate generating'
    local CLIENT='client'
    "${easyrsa_path}" build-client-full ${CLIENT} nopass  &> /dev/null
    fnBase_OperationProcedureResult "${CLIENT}"

    cp "${pki_dir}/ca.crt" "${pki_dir}/private/ca.key" "${pki_dir}/private/server.key" "${pki_dir}/dh.pem" "${pki_dir}/issued/server.crt" "${pki_dir}/crl.pem" "${conf_dir}"

    # Generate key for tls-auth
    fnBase_OperationProcedureStatement 'tls-auth'
    # HMAC authentication
    openvpn --genkey --secret "${tls_auth_key}"  &> /dev/null
    fnBase_OperationProcedureResult "${tls_auth_key}"

    # openvpn --show-ciphers
    local cipher_choose=${cipher_choose:-'AES-256-CBC'}

    fnBase_OperationProcedureStatement 'DNS Server'
    # https://cleanbrowsing.org
    # CleanBrowsing for kids  185.228.168.168 185.228.168.169
    # https://cleanbrowsing.org/for-adults
    # CleanBrowsing for adult 185.228.168.10 185.228.168.11

    # https://duckduckgo.com/?q=public+dns&ia=answer&iax=answer
    local l_dns_list=${l_dns_list:-}
    l_dns_list="Google DNS: 8.8.8.8 8.8.4.4\nCloudFlare DNS: 1.1.1.1 1.0.0.1\nDyn DNS: 216.146.35.35 216.146.36.36\nCleanBrowsing: 185.228.168.10 185.228.168.11\nYandex.DNS: 77.88.8.88 77.88.8.2\nOpenDNS: 208.67.222.222 208.67.220.220\nFreeDNS: 37.235.1.174 37.235.1.177"

    case "${dns_choose,,}" in
        g|google ) dns_choose='Google' ;;
        c|clean ) dns_choose='CleanBrowsing' ;;
        d|dyn ) dns_choose='Dyn' ;;
        f|freedns ) dns_choose='FreeDNS' ;;
        o|opendns ) dns_choose='OpenDNS' ;;
        y|yandex ) dns_choose='Yandex' ;;
        * ) dns_choose='CloudFlare' ;;
    esac

    local l_dns_ip_list=${l_dns_ip_list:-}
    l_dns_ip_list=$(echo -e "${l_dns_list}" | sed -r -n '/^'"${dns_choose}"'/{s@^[^:]+:[[:space:]]*@@g;p}')

    dns_ip_1="${l_dns_ip_list%% *}"
    dns_ip_2="${l_dns_ip_list##* }"
    fnBase_OperationProcedureResult "${dns_choose}: ${l_dns_ip_list}"

    # - Server conf
    fnBase_OperationProcedureStatement 'Server conf'
    echo -e "port ${openvpn_port}\nproto ${protocol}\ndev tun\nca ca.crt\ncert server.crt\nkey server.key\ndh dh.pem\nremote-cert-eku \"TLS Web Client Authentication\"\ntopology subnet\nserver 10.8.0.0 255.255.255.0\nifconfig-pool-persist ipp.txt\nkeepalive 10 120\nauth SHA512\ntls-auth ta.key 0\npush \"redirect-gateway def1 bypass-dhcp\"\npush \"dhcp-option DNS ${dns_ip_1}\"\npush \"dhcp-option DNS ${dns_ip_2}\"\nkey-direction 0\ncipher ${cipher_choose}\ncomp-lzo\nmax-clients 100\nuser nobody\ngroup ${nogroup_name}\npersist-key\npersist-tun\nstatus openvpn-status.log\nverb 3" > "${server_conf_path}"
    fnBase_OperationProcedureResult "${server_conf_path}"

    # Client conf
    fnBase_OperationProcedureStatement 'Client conf'
    echo -e "client\ndev tun\nproto ${protocol}\nremote ${ip_public} ${openvpn_port}\nremote-cert-eku \"TLS Web Server Authentication\"\nresolv-retry infinite\nnobind\nuser nobody\ngroup ${nogroup_name}\npersist-key\npersist-tun\nauth SHA512\nremote-cert-tls server\ncipher ${cipher_choose}\nkey-direction 1\ncomp-lzo\nverb 3" > "${client_conf_path}"
    fnBase_OperationProcedureResult "${client_conf_path}"
}

fn_EasyRSAOperation(){
    # EasyRSA
    fnBase_OperationPhaseStatement 'EasyRSA'

    fnBase_OperationProcedureStatement 'Online Release Info'
    online_release_info=${online_release_info:-}
    online_release_info=$($download_method https://api.github.com/repos/OpenVPN/easy-rsa/releases/latest | sed -r -n '/(tag_name|published_at|browser_download_url)/{/browser_download_url/{/.tgz/!d;};s@[^:]*:[[:space:]]*"([^"]*)".*@\1@g;p}' | sed ':a;N;$!ba;s@\n@|@g;')
    # v3.0.4|2018-01-21T15:55:53Z|https://github.com/OpenVPN/easy-rsa/releases/download/v3.0.4/EasyRSA-3.0.4.tgz|https://github.com/OpenVPN/easy-rsa/releases/download/v3.0.4/EasyRSA-3.0.4.tgz.sig
    if [[ -n "${online_release_info}" ]]; then
        fnBase_OperationProcedureResult "${online_release_info%%|*}"
    else
        fnBase_OperationProcedureResult '' 1
        # fnBase_ExitStatement "${c_red}Sorry${c_normal}: fail to get EasyRSA release info."
    fi

    fnBase_OperationProcedureStatement 'Installing'
    online_release_downloadlink=$(echo "${online_release_info}" | cut -d\| -f 3)
    # online_release_pack_name="${online_release_downloadlink##*/}"
    save_path="${temp_save_dir}/${online_release_downloadlink##*/}"
    $download_method "${online_release_downloadlink}" > "${save_path}"
    [[ -d "${easyrsa_dir}" ]] || mkdir "${easyrsa_dir}"
    chown -R root:root "${easyrsa_dir}"
    tar xf "${save_path}" -C "${easyrsa_dir}" --strip-components=1

    if [[ -f "${easyrsa_path}" ]]; then
        fnBase_OperationProcedureResult "${easyrsa_dir}"
        [[ -f "${save_path}" ]] && rm -rf "${save_path}"
        fn_PKIConfiguration
    else
        [[ -d "${easyrsa_dir}" ]] && rm -rf "${easyrsa_dir}"
        fnBase_OperationProcedureResult '' 1
    fi
}

fn_OpvnFileCreate(){
    local l_client_name="${1:-}"
    if [[ -n "${l_client_name}" ]]; then
        fnBase_OperationPhaseStatement '.ovpn'
        fnBase_OperationProcedureStatement "Client ${l_client_name}"
        local ovpn_path
        ovpn_path="${login_user_home}/${l_client_name}.ovpn"

        cd "${easyrsa_dir}" || exit
        "${easyrsa_path}" build-client-full "${l_client_name}" nopass &> /dev/null
        cat "${client_conf_path}" > "${ovpn_path}"
        echo '<ca>' >> "${ovpn_path}"
        cat "${pki_dir}/ca.crt" >> "${ovpn_path}"
        echo -e '</ca>\n<cert>' >> "${ovpn_path}"
        cat "${pki_dir}/issued/${l_client_name}.crt" >> "${ovpn_path}"
        echo -e '</cert>\n<key>' >> "${ovpn_path}"
        cat "${pki_dir}/private/${l_client_name}.key" >> "${ovpn_path}"
        echo -e '</key>\n<tls-auth>' >> "${ovpn_path}"
        cat "${tls_auth_key}" >> "${ovpn_path}"
        echo '</tls-auth>' >> "${ovpn_path}"
        chown "${login_user}" "${ovpn_path}"
        chmod 640 "${ovpn_path}"
        fnBase_OperationProcedureResult "${ovpn_path}"
        echo -e "\nNew client .opvn file path: ${c_yellow}${ovpn_path}${c_normal}.\n"
    fi
}

fn_CoreOperationProcedure(){
    fnBase_CentralOutputTitle "${software_fullname}"

    if ! fnBase_CommandExistIfCheck 'openvpn'; then
        fnBase_OperationPhaseStatement 'Essential Packages'
        fnBase_OperationProcedureStatement 'Packages Installing'
        local l_pack_list
        l_pack_list='openvpn openssl ca-certificates'
        case "${pack_manager}" in
            yum )
                fnBase_PackageManagerOperation 'install' 'epel-release'
                l_pack_list="${l_pack_list} openssl-devel pam pam-devel"
                ;;
        esac
        fnBase_PackageManagerOperation 'install' "${l_pack_list}"

        if fnBase_CommandExistIfCheck 'openvpn'; then
            fnBase_OperationProcedureResult
        else
            fnBase_OperationProcedureResult '' 1
        fi
    fi

    if [[ ! -f "${server_conf_path}" ]]; then
        # - EasyRSA
        fn_EasyRSAOperation

        # - SELinux
        fn_SELinuxSemanageOperation 'openvpn_port_t' "${openvpn_port}" 'port' 'add' "${protocol}"

        # - sysctl
        # net.ipv4.ip_forward = 1
        local l_sysctl_path=${l_sysctl_path:-'/etc/sysctl.conf'}
        if [[ -f "${l_sysctl_path}" ]]; then
            if [[ -n $(sed -r -n '/^net.ipv4.ip_forward/{s@^([^=]+=[[:space:]]*).*@\1@g;p}' "${l_sysctl_path}") ]]; then
                sed -r -i '/^net.ipv4.ip_forward/{s@^([^=]+=[[:space:]]*).*@\11@g;p}' "${l_sysctl_path}" 2> /dev/null
            else
                sed -r -i '$a net.ipv4.ip_forward = 1' "${l_sysctl_path}" 2> /dev/null
            fi
            fnBase_CommandExistIfCheck 'sysctl' && sysctl -p &> /dev/null
        fi

        # - Firewall rule
        $download_method "${firewall_configuration_script}" | bash -s -- -p "${openvpn_port}" -t "${protocol}" -m -s

    fi

    # - Service script
    # fnBase_SystemServiceManagement 'openvpn' 'start'
    local l_service_name=${l_service_name-:'openvpn'}
    fnBase_CommandExistIfCheck 'systemctl' && l_service_name='openvpn@server.service'
    fnBase_SystemServiceManagement "${l_service_name}" 'enable'
    fnBase_SystemServiceManagement "${l_service_name}" 'restart'

    # - Client .opvn file
    [[ -z "${new_client}" ]] && new_client="${login_user}"
    fn_OpvnFileCreate "${new_client}"
}


#########  3. Executing Process  #########
fn_InitializationCheck
fn_OSInfoDetection

fn_CoreOperationProcedure


#########  4. EXIT Singal Processing  #########
# trap "commands" EXIT # execute command when exit from shell
fn_TrapEXIT(){
    fnBase_UnsetVars
    unset conf_dir
    unset server_conf_path
    unset client_conf_path
    unset easyrsa_dir
    unset easyrsa_path
    unset pki_dir
    unset tls_auth_key
    unset protocol
    unset openvpn_port
    unset dns_choose
    unset new_client
    unset proxy_server_specify
}

trap fn_TrapEXIT EXIT

# Script End
