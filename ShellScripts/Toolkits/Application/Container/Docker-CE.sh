#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails


#Official Site: https://www.docker.com
#Installation:
# - https://docs.docker.com/install/
# - https://docs.docker.com/config/daemon/
# - https://docs.docker.com/install/linux/linux-postinstall/
# - https://docs.docker.com/develop/develop-images/dockerfile_best-practices/#create-ephemeral-containers

# Target: Automatically Install & Update Docker Via Package Manager On GNU/Linux
# Developer: MaxdSre

# Chnage Log:
# - Dec 21, 2022 20:15 Wed ET - update operating commands
# - Oct 08, 2020 08:47 Thu ET - initialization check method update
# - Oct 15, 2019 12:11 Thu ET - change custom script url
# - Oct 13, 2019 21:18 ET - change to new base functions
# - Mar 05, 2019 19:51 ET - Update docker-ce distribution support list
# - Dec 30, 2018 17:13 Sun ET - code reconfiguration, include base funcions from other file
# - Jun 07, 2018 16:44 ET - code reconfiguration
# - Feb 23, 2018 11:39 ET - Doc link update, add container monitoring tool cTop
# - Aug 09, 2017 11:58 +0800
# - Jun 05, 2017 08:56 +0800
# - May 19, 2017 09:24 ET



########################################################
# Docker CE: Ubuntu, Debian, CentOS, Fedora            #
# Docker EE: Ubuntu, RHEL, CentOS, Oracle Linux, SLES  #
# Docker CE Stable Timeline: March, June, September    #
########################################################

#######################################################################
# https://docs.docker.com/install/#supported-platforms                #
# Debian: Stretch 9, Buster 10                                        #
# Ubuntu: xenial(16.04), bionic(18.04), cosmic(18.10), disco(19.04)   #
# CentOS: 7                                                           #
# Fedora: 28, 29                                                      #
#######################################################################



#########  0-1. Singal Setting  #########
trap fnBase_TrapCleanUp INT QUIT

#########  0-2. Variables Setting  #########
# umask 022    # temporarily change umask value to 022
readonly official_site='https://www.docker.com'
readonly download_page='https://download.docker.com/linux'
readonly custom_shellscript_url='https://gitlab.com/axdsop/nixnotes/raw/master'
readonly os_check_script="${custom_shellscript_url}/ShellScripts/Toolkits/GNULinux/gnuLinuxMachineInfoDetection.sh"
docker_data_dir=${docker_data_dir:-'/var/lib/docker'}
ctop_save_path=${ctop_save_path:-'/usr/local/sbin/ctop'}

is_existed=${is_existed:-1}      # Default value is 1， assume system has installed Docker CE
version_check=${version_check:-0}
is_edge=${is_edge:-0}
os_detect=${os_detect:-0}
monitor_tool=${monitor_tool:-}
is_uninstall=${is_uninstall:-0}
remove_datadir=${remove_datadir:-0}



#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
script [options] ...
script | sudo bash -s -- [options] ...

Installing / Updating Docker CE(stable) On GNU/Linux (CentOS/Debian/Ubuntu)!
This script requires superuser privileges (eg. root, su).

[available option]
    -h    --help, show help info
    -c    --check, check installed or not
    -e    --edge, choose edge version, default is stable
    -o    --os info, detect os distribution info
    -m monitor_tool    --specify container monitor tool (ctop), current available tool is just 'ctop', you could manually install 'Rancher'
    -u    --uninstall, uninstall software installed
    -r    --remove datadir /var/lib/docker, along with '-u'
    -p [protocol:]ip:port    --proxy host (http|https|socks4|socks5), default protocol is http
\e[0m"
}

while getopts "hceom:urp:" option "$@"; do
    case "$option" in
        c ) version_check=1 ;;
        e ) is_edge=1 ;;
        o ) os_detect=1 ;;
        m ) monitor_tool="$OPTARG" ;;
        u ) is_uninstall=1 ;;
        r ) remove_datadir=1 ;;
        p ) proxy_server_specify="$OPTARG" ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done


#########  1-2 Preparation Operation  #########
fn_InitializationCheck(){
    local l_user_codebase_dir="$HOME/.config/axdsop"
    local l_base_function_temp_save_path=''
    local l_need_remove=0
    local l_base_function_url='https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/base_functions.sh'
    l_base_function_temp_save_path=$(find "${l_user_codebase_dir}" -type f -name "${l_base_function_url##*/}" -print 2> /dev/null)
    
    if [[ ! -f "${l_base_function_temp_save_path}" ]]; then
        local l_download_tool=${l_download_tool:-'wget -qO-'}
        [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_tool='curl -fsL'
        l_need_remove=1
        l_base_function_temp_save_path=$(mktemp -t XXXXXXX)
        ${l_download_tool} "${l_base_function_url}" > "${l_base_function_temp_save_path}"
    fi

    if [[ -f "${l_base_function_temp_save_path}" && -s "${l_base_function_temp_save_path}" ]]; then
        . "${l_base_function_temp_save_path}"
        [[ "${l_need_remove}" -eq 1 ]] && rm "${l_base_function_temp_save_path}"
    else
        echo "Sorry, fail to get base script path."
        exit
    fi

    # tput clear  # Echo the clear-screen sequence for the current terminal.
    # specify 1 means need sudo privilege
    fnBase_RunningEnvironmentCheck '1'

    if [[ "${os_detect}" -eq 1 ]]; then
        $download_method "${os_check_script}" | bash -s --
        exit
    fi

    local kernel_version=${kernel_version:-}
    kernel_version=$(uname -r | sed -r -n 's@([0-9]+.[0-9]+).*@\1@p')
    kernel_version=${kernel_version%%-*}

    [[ $(expr "${kernel_version}" \< 3.10) -eq 1 ]] && fnBase_ExitStatement "${c_red}Sorry${c_normal}: Your Linux kernel version ${c_blue}$(uname -r)${c_normal} is not supported for running docker. Please upgrade your kernel to ${c_blue}3.10.0${c_normal} or newer!"
}

fn_OSInfoDetection(){
    local l_json_osinfo=${l_json_osinfo:-}
    l_json_osinfo=$(fnBase_OSInfoRetrieve "${os_check_script}")
    # [[ -z "${l_json_osinfo}" ]] && fnBase_ExitStatement "${c_red}Fatal${c_normal}: fail to extract os info!"

    distro_fullname=${distro_fullname:-}
    distro_fullname=$(fnBase_OSInfoDirectiveValExtraction 'distro_fullname' "${l_json_osinfo}")

    distro_name=${distro_name:-}
    distro_name=$(fnBase_OSInfoDirectiveValExtraction 'distro_name' "${l_json_osinfo}")
    distro_name="${distro_name%%-*}"    # centos, fedora

    distro_os_arch=${distro_os_arch:-}
    distro_os_arch=$(fnBase_OSInfoDirectiveValExtraction 'distro_os_arch' "${l_json_osinfo}")

    codename=${codename:-}
    codename=$(fnBase_OSInfoDirectiveValExtraction 'distro_codename' "${l_json_osinfo}")

    version_id=${version_id:-}
    version_id=$(fnBase_OSInfoDirectiveValExtraction 'distro_version_id' "${l_json_osinfo}")

    pack_manager=${pack_manager:-}
    pack_manager=$(fnBase_OSInfoDirectiveValExtraction 'distro_pack_manager' "${l_json_osinfo}")
}


#########  2-1. Uninstall  #########
fn_UninstallOperation(){
    if [[ "${is_uninstall}" -eq 1 ]]; then
        if [[ "${is_existed}" -eq 1 ]]; then
            [[ "${login_user}" == 'root' ]] || gpasswd -d "${login_user}" docker &> /dev/null

            if [[ -f "${repo_path}" ]]; then
                fnBase_PackageManagerOperation 'remove' 'docker-ce'
                [[ -f "${repo_path}" ]] && rm -f "${repo_path}"
                if [[ "${remove_datadir}" -eq 1 ]]; then
                    [[ -d "${docker_data_dir}" ]] && rm -rf "${docker_data_dir}" &> /dev/null
                    rm -rf /var/lib/containerd &> /dev/null
                fi
                # [[ "${pack_manager}" == 'apt-get' ]] && apt-key del 0EBFCD88 1> /dev/null
                # Warning: apt-key is deprecated. Manage keyring files in trusted.gpg.d instead (see apt-key(8)).

                if [[ "${pack_manager}" == 'apt-get' ]]; then
                    [[ -f /etc/apt/keyrings/docker.gpg ]] && rm -rf /etc/apt/keyrings/docker.gpg
                fi
            fi

            [[ -f "${ctop_save_path}" ]] && rm -f "${ctop_save_path}"
            fnBase_CommandExistIfCheck 'docker' || fnBase_ExitStatement "Docker is successfully removed from your system!"
        else
            fnBase_ExitStatement "No Docker find in your system."
        fi
    fi
}

#########  2-2. Support Distro Info Detection  #########
fn_SupportDistroDetection(){
    local is_support=${is_support:-1}   # is docker official repo support
    local l_version_id=${l_version_id:-"${version_id%%.*}"}

    # Kernerl version >= 3.10
    local l_kernel_version
    l_kernel_version=$(uname -r)
    l_kernel_version="${l_kernel_version%%-*}"
    [[ $(fnBase_VersionNumberComparasion "${l_kernel_version}" '3.10' '<') -eq 1 ]] && fnBase_ExitStatement "Sorry, Docker needs kernel version >= 3.10."

    # Distro version
    case "${distro_name,,}" in
        # https://docs.docker.com/install/linux/docker-ce/centos/
        centos ) [[ "${l_version_id}" -lt 7 ]] && is_support=0 ;;
        # https://docs.docker.com/install/linux/docker-ce/fedora/
        fedora ) [[ "${l_version_id}" -lt 34 ]] && is_support=0 ;;
        # https://docs.docker.com/install/linux/docker-ce/debian/
        # https://docs.docker.com/install/linux/docker-ce/ubuntu/
        debian|ubuntu )
            case "$distro_name" in
                debian ) [[ "${l_version_id}" -lt 10 ]] && is_support=0 ;;
                ubuntu ) [[ "${l_version_id}" -lt 18 ]] && is_support=0 ;;
            esac
            ;;
        * ) is_support=0 ;;
    esac

    [[ "${is_support}" -eq 1 ]] || fnBase_ExitStatement "Sorry, this script doesn't support your system ${c_blue}${distro_fullname}${c_normal}."

    # repo path
    case "${pack_manager}" in
        yum|dnf ) repo_path='/etc/yum.repos.d/docker-ce.repo' ;;
        apt-get ) repo_path='/etc/apt/sources.list.d/docker.list' ;;
    esac
}

#########  2-3. Latest & Local Version Check  #########
fn_VersionComparasion(){
    fnBase_CentralOutputTitle 'Docker CE'
    fnBase_OperationPhaseStatement 'Version Detection'

    # - Local version
    fnBase_OperationProcedureStatement 'Local version detecting'
    local current_local_version=${current_local_version:-}
    if fnBase_CommandExistIfCheck 'docker'; then
        # Cannot connect to the Docker daemon at unix:///var/run/docker.sock. Is the docker daemon running?
        current_local_version=$(docker version --format '{{.Server.Version}}' 2> /dev/null)
        [[ -z "${current_local_version}" ]] && current_local_version=$(docker --version 2> /dev/null | sed -r -n 's@^[^[:digit:]]+([^(,|[:space:])]+).*$@\1@g;p')

        current_local_version="${current_local_version/.ce/-ce}"
    fi

    [[ -z "${current_local_version}" ]] && is_existed=0
    if [[ "${is_existed}" -eq 1 ]]; then
        fnBase_OperationProcedureResult "${current_local_version}"
    else
        fnBase_OperationProcedureResult 'not exist'
    fi

    # - Just online version check
    if [[ "${version_check}" -eq 1 ]]; then
        fnBase_ExitStatement ''
    fi
}

#########  2.4. Container Monitoring & Management Tool  #########
fn_MonitoringToolInstallation(){
    #  cTop - A Command-Line Tool For Container Monitoring And Management
    #  https://github.com/bcicen/ctop

    #  Rancher - A Complete Container Management Platform For Production Environment
    #  http://rancher.com/rancher/
    # https://www.2daygeek.com/rancher-a-complete-container-management-platform-for-production-environment/

    local l_online_download_link
    l_online_download_link=$($download_method https://api.github.com/repos/bcicen/ctop/releases/latest 2> /dev/null| sed -r -n '/browser_download_url/{/linux-amd64/{s@[^:]*:[[:space:]]*"([^"]*)".*@\1@g;p}}')

    local l_online_release_version
    l_online_release_version=$(echo "${l_online_download_link}" | sed -r -n 's@.*v([[:digit:].]+).*@\1@g;p')

    local l_is_need_download=${l_is_need_download:-0}

    if [[ -f "${ctop_save_path}" ]]; then
        local l_local_version=${l_local_version:-}
        l_local_version=$("${ctop_save_path}" -v 2> /dev/null | sed -r -n 's@^[^[:digit:]]+([[:digit:].]+).*@\1@g;p')
        [[ -n "${l_online_release_version}" && "${l_online_release_version}" != "${l_local_version}" ]] && l_is_need_download=1
    else
        case "${monitor_tool,,}" in
            c|ctop ) l_is_need_download=1 ;;
        esac
    fi

    if [[ "${l_is_need_download}" -eq 1 ]]; then
        fnBase_OperationPhaseStatement 'Container Monitoring'
        fnBase_OperationProcedureStatement 'cTop'
        $download_method "${l_online_download_link}" > "${ctop_save_path}"
        chmod +x "${ctop_save_path}"
        local l_ctop_version
        l_ctop_version=$("${ctop_save_path}" -v 2> /dev/null | sed -r -n 's@[^[:digit:]]*([[:digit:].]+).*@\1@g;p')
        fnBase_OperationProcedureResult "${l_ctop_version} ${ctop_save_path}"
    fi
}

#########  3. Docker Operation Function  #########
fn_CoreOperation(){
    if [[ ! -f "${repo_path}" ]]; then
        fnBase_OperationPhaseStatement 'Operation'
        local l_release_type=${l_release_type:-'stable'}

        case "${distro_name}" in
            centos|fedora )
                # - remove old versions
                fnBase_OperationProcedureStatement 'Remove old packages'
                fnBase_PackageManagerOperation 'remove' 'docker docker-client docker-client-latest docker-common docker-latest docker-latest-logrotate docker-logrotate docker-selinux docker-engine-selinux docker-engine'
                fnBase_OperationProcedureResult

                # - add official repository
                fnBase_OperationProcedureStatement 'Official repository'
                $download_method "${download_page}/${distro_name}/docker-ce.repo" > "${repo_path}"

                sed -r -i '/enabled=/s@enabled=1@enabled=0@g' "${repo_path}"

                if [[ "${is_edge}" -eq 1 ]]; then
                    sed -i '/docker-ce-edge]$/,/enabled/s@enabled=0@enabled=1@' "${repo_path}"
                    l_release_type='edge'
                else
                    sed -i '/docker-ce-stable]$/,/enabled/s@enabled=0@enabled=1@' "${repo_path}"
                fi
                fnBase_OperationProcedureResult "${l_release_type} ${repo_path}"
                ;;
            debian|ubuntu )
                # - remove old versions
                fnBase_OperationProcedureStatement 'Remove old packages'
                fnBase_PackageManagerOperation 'remove' 'docker docker-engine docker.io containerd runc'
                fnBase_OperationProcedureResult

                # - essential packages
                fnBase_OperationProcedureStatement 'Essential packages'
                fnBase_PackageManagerOperation 'install' 'apt-transport-https'
                fnBase_CommandExistIfCheck 'systemctl' || fnBase_PackageManagerOperation 'install' 'sysv-rc-conf'

                # software-properties-common used for add-apt-repository
                # python-software-properties used for add-apt-repository
                local l_essential_pack_list
                l_essential_pack_list='apt-transport-https ca-certificates curl'

                case "${distro_name}" in
                    ubuntu )
                        # software-properties-common
                        if [[ "${version_id}" == '14.04' ]]; then # Trusty 14.04
                            l_essential_pack_list="${l_essential_pack_list} linux-image-extra-$(uname -r) linux-image-extra-virtual"
                        fi
                        ;;
                    debian )
                        if [[ "${version_id%%.*}" -eq 7 ]]; then    # Wheezy 7.x
                            # python-software-properties
                            # https://docs.docker.com/install/linux/docker-ce/debian/#extra-steps-for-wheezy-77
                            # https://backports.debian.org/Instructions/
                            echo "deb http://ftp.debian.org/${distro_name} ${codename}-backports main" > /etc/apt/sources.list.d/backports.list
                            # apt-get -t $codename-backports install "package"
                        else
                            # software-properties-common
                            l_essential_pack_list="${l_essential_pack_list} gnupg lsb-release"
                        fi
                        ;;
                esac

                fnBase_PackageManagerOperation 'install' "${l_essential_pack_list}"
                fnBase_OperationProcedureResult

                # - import GnuPG key
                fnBase_OperationProcedureStatement 'GnuPG key importing'

                # Warning: apt-key is deprecated. Manage keyring files in trusted.gpg.d instead (see apt-key(8)).
                # $download_method "${download_page}/${distro_name}/gpg" | apt-key add - &> /dev/null
                # apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 0EBFCD88

                local l_docker_gpg_keyring
                l_docker_gpg_keyring='/etc/apt/keyrings/docker.gpg'
                if [[ ! -f "${l_docker_gpg_keyring}" ]]; then
                    mkdir -p "${l_docker_gpg_keyring%/*}"
                    $download_method "${download_page}/${distro_name}/gpg" | gpg --dearmor -o "${l_docker_gpg_keyring}" 1> /dev/null
                    chmod a+r "${l_docker_gpg_keyring}"
                fi
                fnBase_OperationProcedureResult

                # - add Docker repository
                fnBase_OperationProcedureStatement 'Official repository'
                [[ -f '/etc/apt/sources.list' ]] && sed -i '/docker.com/d' /etc/apt/sources.list

                # lsb_release -cs == ${codename}
                # Debian: x86_64 (or amd64), armhf, and arm64
                # Ubuntu: x86_64 (or amd64), armhf, arm64, and s390x
                if [[ "$is_edge" -eq 1 ]]; then
                    echo "deb [arch=${distro_os_arch} signed-by=${l_docker_gpg_keyring}] ${download_page}/${distro_name} ${codename} stable edge" > "${repo_path}"
                    l_release_type='edge'
                else
                    echo "deb [arch=${distro_os_arch} signed-by=${l_docker_gpg_keyring}] ${download_page}/${distro_name} ${codename} stable" > "${repo_path}"
                fi
                fnBase_OperationProcedureResult "${l_release_type} ${repo_path}"
                ;;
        esac

        if [[ "${is_existed}" -ne 1 ]]; then
            # - install latest version
            fnBase_OperationProcedureStatement 'Installing'
            fnBase_PackageManagerOperation 'cache'
            local online_release_version=${online_release_version:-}
            case "${distro_name}" in
                centos|fedora )
                    online_release_version=$("${pack_manager}" info available docker-ce 2> /dev/null | sed -r -n '/^Version/s@.*:[[:space:]]*(.*)$@\1@p')
                    ;;
                debian|ubuntu )
                    online_release_version=$(apt-cache madison docker-ce 2> /dev/null | sed -r -n '1{s@[^\|]+\|[[:space:]]*([^~]+).*$@\1@;s@^.*?:@@;p}')
                    ;;
            esac
            local l_pack_name=${l_pack_name:-'docker-ce docker-ce-cli containerd.io docker-compose-plugin'}
            [[ -n "${online_release_version}" ]] || l_pack_name='docker'
            fnBase_PackageManagerOperation 'install' "${l_pack_name}"

            fnBase_SystemServiceManagement 'docker' 'start'
            fnBase_SystemServiceManagement 'docker' 'restart'

            local new_installed_version=${new_installed_version:-}
            new_installed_version=$(docker version --format '{{.Server.Version}}' 2> /dev/null)
            if [[ -n "${new_installed_version}" ]]; then
                fnBase_OperationProcedureResult "${l_pack_name%% *} ${online_release_version}"
            else
                [[ -f "${repo_path}" ]] && rm -f "${repo_path}"
                fnBase_OperationProcedureResult '' 1
            fi

            # - add group docker
            if [[ "${login_user}" != 'root' ]]; then
                local l_group_name=${l_group_name:-'docker'}
                fnBase_OperationProcedureStatement 'User group create'
                groupadd "${l_group_name}" &> /dev/null
                usermod -aG "${l_group_name}" "${login_user}" &> /dev/null
                fnBase_OperationProcedureResult "${login_user} (existed) ∈ ${l_group_name}"
            fi

        fi

    fi

    fn_MonitoringToolInstallation
}


########  4.Operation Time Cost  ########
fn_OperationTimeCost(){
    finish_time=$(date +'%s')        # End Time Of Operation
    total_time_cost=$((finish_time-start_time))   # Total Time Of Operation

    printf "\nTotal time cost is $c_red%s$c_normal seconds!\n" "$total_time_cost"
    [[ "${is_uninstall}" -eq 1 || "${is_existed}" -eq 1 ]] || printf "\nTo make configuration effect, please ${c_red}%s${c_normal} your system!\n" "logout"
}



#########  5. Executing Process  #########
fn_InitializationCheck

fn_OSInfoDetection

fn_SupportDistroDetection
fn_UninstallOperation
fn_VersionComparasion
fn_CoreOperation

fn_OperationTimeCost


#########  5. EXIT Singal Processing  #########
# trap "commands" EXIT # execute command when exit from shell
fn_TrapEXIT(){
    fnBase_UnsetVars
    unset docker_data_dir
    unset ctop_save_path
    unset is_existed
    unset version_check
    unset is_edge
    unset os_detect
    unset is_uninstall
    unset remove_datadir
    unset download_method
    unset proxy_server_specify
}

trap fn_TrapEXIT EXIT


# Error Occuring:
# Error response from daemon: conflict: unable to delete 6d83de432e98 (cannot be forced) - image dependent child images
# Solution
# cd /var/lib/docker/image/overlay2/imagedb/content/sha256 && rm -f 6d83de432e98*


# Script End
