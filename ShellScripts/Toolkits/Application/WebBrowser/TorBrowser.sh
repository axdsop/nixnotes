#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails

# Official Site: https://www.torproject.org
# Mar 27, 2019 Meet The New TorProject.org  https://blog.torproject.org/meet-new-torprojectorg

# Documentation:
# - https://2019.www.torproject.org/docs/documentation.html.en
# - https://2019.www.torproject.org/docs/verifying-signatures.html.en

# Target: Automatically Install & Update Tor Browser On GNU/Linux
# Developer: MaxdSre

# Change Log:
# - Oct 08, 2020 09:05 Thu ET - initialization check method update
# - Oct 23, 2019 09:35 Wed ET - fix online release date extraction return multiple values
# - Oct 08, 2019 11:43 Tue ET - change to new base functions
# - Jun 24, 2019 13:07 Mon ET - fix online release version info extraction
# - May 28, 2019 11:39 Tue ET - official page redesign
# - Dec 30, 2018 16:39 Sun ET - code reconfiguration, include base funcions from other file
# - Jun 12, 2018 10:31 Tue ET - code reconfiguration
# - Dec 13, 2017 10:21 Tue +0800  (official page revision)
# - July 25, 2017 09:42 Tue +0800
# - June 07, 2017 17:23 Wed +0800
# - May 16, 2017 17:20 Tue ET
# - Feb 16, 2017 11:43 +0800


#########  0-1. Singal Setting  #########
trap fnBase_TrapCleanUp INT QUIT

#########  0-2. Variables Setting  #########
# umask 022    # temporarily change umask value to 022
readonly official_site='https://www.torproject.org'    # https://2019.www.torproject.org/
readonly pack_download_page="${official_site}/download/"
# readonly pack_download_page="${official_site}/download/download.html"
readonly download_redirect_page='https://dist.torproject.org'  # actual download link  https://dist.torproject.org/torbrowser/
readonly download_version="linux64"  # linux64
readonly download_language='en-US' #英文 en-US, 中文 zh-TW
readonly software_fullname=${software_fullname:-'Tor Browser'}
readonly application_name=${application_name:-'TorBrowser'}
installation_dir="/opt/${application_name}"      # Decompression & Installation Path Of Package
readonly change_log_path="${installation_dir}/Browser/TorBrowser/Docs/ChangeLog.txt"
readonly pixmaps_png_path="/usr/share/pixmaps/${application_name}.png"
readonly application_desktop_path="/usr/share/applications/${application_name}.desktop"
is_existed=${is_existed:-1}      # Default value is 1， assume system has installed Tor Browser

version_check=${version_check:-0}
source_pack_path=${source_pack_path:-}
is_uninstall=${is_uninstall:-0}
proxy_server_specify=${proxy_server_specify:-}



#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | sudo bash -s -- [options] ...

Installing / Updating Tor Browser On GNU/Linux!
This script requires superuser privileges (eg. root, su).

[available option]
    -h    --help, show help info
    -c    --check, check current stable release version
    -f pack_path    --specify latest release package save path (e.g. /PATH/*.tar.xz) or dir (default is ~/Downloads/) in system
    -p [protocol:]ip:port    --proxy host (http|https|socks4|socks5), default protocol is http
    -u    --uninstall, uninstall software installed
\e[0m"
}

while getopts "hcf:up:" option "$@"; do
    case "$option" in
        c ) version_check=1 ;;
        f ) source_pack_path="$OPTARG" ;;
        u ) is_uninstall=1 ;;
        p ) proxy_server_specify="$OPTARG" ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done


#########  1-2 Preparation Operation  #########
fn_InitializationCheck(){
    local l_user_codebase_dir="$HOME/.config/axdsop"
    local l_base_function_temp_save_path=''
    local l_need_remove=0
    local l_base_function_url='https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/base_functions.sh'
    l_base_function_temp_save_path=$(find "${l_user_codebase_dir}" -type f -name "${l_base_function_url##*/}" -print 2> /dev/null)
    
    if [[ ! -f "${l_base_function_temp_save_path}" ]]; then
        local l_download_tool=${l_download_tool:-'wget -qO-'}
        [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_tool='curl -fsL'
        l_need_remove=1
        l_base_function_temp_save_path=$(mktemp -t XXXXXXX)
        ${l_download_tool} "${l_base_function_url}" > "${l_base_function_temp_save_path}"
    fi

    if [[ -f "${l_base_function_temp_save_path}" && -s "${l_base_function_temp_save_path}" ]]; then
        . "${l_base_function_temp_save_path}"
        [[ "${l_need_remove}" -eq 1 ]] && rm "${l_base_function_temp_save_path}"
    else
        echo "Sorry, fail to get base script path."
        exit
    fi

    # tput clear  # Echo the clear-screen sequence for the current terminal.
    # specify 1 means need sudo privilege
    fnBase_RunningEnvironmentCheck '1'

    fnBase_CommandExistCheckPhase 'xz' # Fedora/OpenSUSE: xz   Debian/Ubuntu: xz-utils
    fnBase_CommandExistCheckPhase 'tar' # .tar.xz
}


#########  2-1. Uninstall  #########
fn_UninstallOperation(){
    if [[ "${is_uninstall}" -eq 1 ]]; then
        if [[ -d "${installation_dir}" ]]; then
            [[ -f "${pixmaps_png_path}" ]] && rm -f "${pixmaps_png_path}"
            [[ -f "${application_desktop_path}" ]] && rm -f "${application_desktop_path}"

            [[ -d "${installation_dir}" ]] && rm -rf "${installation_dir}"
            [[ -d "${installation_dir}${bak_suffix}" ]] && rm -rf "${installation_dir}${bak_suffix}"

            [[ -d "${installation_dir}" ]] || fnBase_ExitStatement "${software_fullname} is successfully removed from your system!"
        else
            fnBase_ExitStatement "${c_blue}Note${c_normal}: no ${software_fullname} is found in your system!"
        fi
    fi
}


#########  2-2. Latest & Local Version Check  #########
fn_VersionComparasion(){
    fnBase_CentralOutputTitle "${software_fullname}"
    fnBase_OperationPhaseStatement 'Version Detection'

    # - Local version
    current_local_version=${current_local_version:-}
    if [[ -f "${installation_dir}/start-tor-browser.desktop" && -f "${change_log_path}" ]]; then
        current_local_version=$(sed -r -n '/^Tor Browser/{s@^[^[:digit:]]+([^[:space:]]+).*$@\1@g;p;q}' "${change_log_path}")
    fi

    [[ -z "${current_local_version}" ]] && is_existed=0
    if [[ "${is_existed}" -eq 1 ]]; then
        fnBase_OperationProcedureStatement 'Local version detecting'
        fnBase_OperationProcedureResult "${current_local_version}"
    fi

    # - Latest online version info
    fnBase_OperationProcedureStatement 'Online version detecting'
    # download_page_html=$(mktemp -t "${mktemp_format}")
    # $download_method "${pack_download_page}" > "${download_page_html}"
    online_release_pack_info=${online_release_pack_info:-}
    online_release_pack_info=$($download_method "${pack_download_page}" | sed -r -n '/linux64-/{s@.*href=".*?/dist/([^"]+)">.*$@'"${download_redirect_page}/"'\1@g;p}' | sed ':a;N;$!ba;s@\n@|@g;' )
    # https://dist.torproject.org/torbrowser/8.5.3/tor-browser-linux64-8.5.3_en-US.tar.xz|https://dist.torproject.org/torbrowser/8.5.3/tor-browser-linux64-8.5.3_en-US.tar.xz.asc
    [[ -z "${online_release_pack_info}" ]] && fnBase_ExitStatement "${c_red}Sorry${c_normal}: fail to get package download link!"

    # https://blog.torproject.org/new-release-tor-browser-85 --> 'Full Changelog' https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt
    # But version 8.5.3 not list in this ChangeLog.txt
    # sed -r -n '/^Tor Browser/{/[[:digit:]]+-?[[:alpha:]]+/d;s@^[^[:digit:]]+@@g;s@[[:space:]]*-+[[:space:]]*@|@g;p;q}'

    online_release_version=$(echo "${online_release_pack_info}" | sed -r -n 's@.*linux64-([^_]+).*$@\1@g;p' )

    online_release_date=$($download_method 'https://dist.torproject.org/torbrowser/' | sed -r -n '/'"${online_release_version}"'\//{s@.*<\/a>[[:space:]]*([[:digit:]-]+)[[:space:]]*.*$@\1@g;s@[[:space:]]*$@@g;p}' | date -f - +"%b %d, %Y" -u)

    if [[ -n "${online_release_version}" ]]; then
        fnBase_OperationProcedureResult "${online_release_version} - ${online_release_date}"
    else
        fnBase_OperationProcedureResult '' 1
    fi

    # - Just online version check
    if [[ "${version_check}" -eq 1 ]]; then
        fnBase_ExitStatement ''

        # if [[ "${is_existed}" -eq 1 ]]; then
        #     fnBase_ExitStatement "Local existed version is ${c_red}${current_local_version}${c_normal}, Latest version online is ${c_red}${online_release_version}${c_normal} (${c_blue}${online_release_date}${c_normal})."
        # else
        #     fnBase_ExitStatement "Latest version online (${c_red}${online_release_version}${c_normal}), Release date ($c_red${online_release_date}$c_normal)."
        # fi
    fi

    # - Online & Local Version Comparasion
    if [[ "${is_existed}" -eq 1 ]]; then
        if [[ "${online_release_version}" == "${current_local_version}" ]]; then
            fnBase_ExitStatement "\nLatest version (${c_red}${online_release_version}${c_normal}) has been existed in your system."
        elif [[ $(fnBase_VersionNumberComparasion "${current_local_version}" "${online_release_version}" '<') -eq 1 ]]; then
            printf "\nExisted version local (${c_red}%s${c_normal}) < Latest version online (${c_red}%s${c_normal}).\n" "${current_local_version}" "${online_release_version}"
        fi
    # else
    #     printf "No %s find in your system!\n" "${software_fullname}"
    fi
}


#########  2-3. Download & Decompress Latest Software  #########
fn_DownloadAndDecompressOperation(){
    local l_operation_type=${l_operation_type:-'installing'}
    [[ "${is_existed}" -eq 1 ]] && l_operation_type='updating'
    fnBase_OperationPhaseStatement "${l_operation_type^}"

    local online_release_downloadlink="${online_release_pack_info%%|*}"
    local online_release_pack_name="${online_release_downloadlink##*/}"
    local online_release_dgst=${online_release_dgst:-}
    # https://www.torproject.org/docs/verifying-signatures.html.en#BuildVerification
    # https://dist.torproject.org/torbrowser/8.5/sha256sums-unsigned-build.txt
    online_release_dgst=$($download_method "${download_redirect_page}/torbrowser/${online_release_version}/sha256sums-unsigned-build.txt" | sed -r -n '/'"${online_release_pack_name}"'/{s@^[[:space:]]*([^[:space:]]+).*@\1@g;p}')

    local l_is_need_download=${l_is_need_download:-1}
    local l_specified_pack_dir=${l_specified_pack_dir:-}

    # specified file path existed
    if [[ "${source_pack_path}" =~ .tar.xz$ && -s "${source_pack_path}" ]]; then
        [[ "${source_pack_path##*/}" == "${online_release_pack_name}" ]] || fnBase_ExitStatement "Package ${c_red}${source_pack_path}${c_normal} is not same to latest release version name!"

    # specified dir or ~/Downloads existed
    elif [[ (-n "${source_pack_path}" && -d "${source_pack_path}" ) || -d "${user_download_dir}" ]]; then
        if [[ -d "${source_pack_path}" ]]; then
            l_specified_pack_dir="${source_pack_path}"
        elif [[ -d "${user_download_dir}" ]]; then
            l_specified_pack_dir="${user_download_dir}"
        fi

        l_specified_pack_dir="${l_specified_pack_dir%/}"

        if [[ -d "${l_specified_pack_dir}" && -f "${l_specified_pack_dir}/${online_release_pack_name}" ]]; then
            source_pack_path="${l_specified_pack_dir}/${online_release_pack_name}"
        fi
    fi

    # 1.1- existed package path in system
    if [[ -n "${source_pack_path}" && -f "${source_pack_path}" ]]; then
        fnBase_OperationProcedureStatement 'Existed pack dgst verifying'

        if [[ -n "${online_release_dgst}" && $(fnBase_HashDigestVerification "${source_pack_path}" '256') == "${online_release_dgst}" ]]; then
            fnBase_OperationProcedureResult "${c_red}${source_pack_path%/*}/${c_normal}"
            l_is_need_download=0
        else
            fnBase_OperationProcedureResult '' 1
        fi
    fi

    # 2 - Download Directly From Official Site
    local pack_save_path=${pack_save_path:-}

    if [[ "${l_is_need_download}" -eq 1 ]]; then
        fnBase_OperationProcedureStatement "Downloading ${c_red}${online_release_pack_name}${c_normal}"
        pack_save_path="${temp_save_dir}/${online_release_pack_name}"
        [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"

        $download_method "${online_release_downloadlink}" > "${pack_save_path}"

        if [[ -s "${pack_save_path}" ]]; then
            fnBase_OperationProcedureResult
        else
            [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
            fnBase_OperationProcedureResult '' 1
        fi

        fnBase_OperationProcedureStatement "SHA256 dgst verification"
        if [[ -n "${online_release_dgst}" && $(fnBase_HashDigestVerification "${pack_save_path}" '256') == "${online_release_dgst}"  ]]; then
            fnBase_OperationProcedureResult
        else
            [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
            fnBase_OperationProcedureResult '' 1
        fi

        # use variable "source_pack_path" to execute decompress operation, variable "pack_save_path" used for delete temporary download package
        source_pack_path="${pack_save_path}"
    fi

    # 3 - Decompress & Extract
    fnBase_OperationProcedureStatement 'Installing'
    local application_backup_path="${installation_dir}${bak_suffix}"
    [[ -d "${application_backup_path}" ]] && rm -rf "${application_backup_path}"

    [[ -d "${installation_dir}" ]] && mv "${installation_dir}" "${application_backup_path}"
    [[ -d "${installation_dir}" ]] || mkdir -p "${installation_dir}"
    tar xf "${source_pack_path}" -C "${installation_dir}" --strip-components=1 2> /dev/null
    chown -R "${login_user}" "${installation_dir}"
    find "${installation_dir}" -type f -name "*.so" -exec chmod 644 {} \; 2> /dev/null

    local new_installed_version=${new_installed_version:-}
    [[ -f "${change_log_path}" ]] && new_installed_version=$(sed -r -n '/^Tor Browser/{s@^[^[:digit:]]+([^[:space:]]+).*$@\1@g;p;q}' "${change_log_path}")

    if [[ "${online_release_version}" == "${new_installed_version}" ]]; then
        [[ -f "${pixmaps_png_path}" ]] && rm -f "${pixmaps_png_path}"
        [[ -f "${application_desktop_path}" ]] && rm -f "${application_desktop_path}"
        [[ -d "${application_backup_path}" ]] && rm -rf "${application_backup_path}"
        fnBase_OperationProcedureResult "${c_red}${installation_dir%/}/${c_normal}"

        # copy newly downloaded pack to ~/Downloads
        local l_user_download_save_path=${l_user_download_save_path:-"${user_download_dir}/${source_pack_path##*/}"}
        if [[ -d "${user_download_dir}" && ! -f "${l_user_download_save_path}" ]]; then
            cp "${source_pack_path}" "${l_user_download_save_path}"
            chown "${login_user}" "${l_user_download_save_path}"
            chgrp -hR "${login_user}" "${l_user_download_save_path}" 2> /dev/null

            # removed old version package existed
            if [[ "${is_existed}" -eq 1 ]]; then
                local l_existed_old_pack_path=${l_existed_old_pack_path:-"${user_download_dir}/${online_release_pack_name//${online_release_version}/${current_local_version}}"}
                [[ -f "${l_existed_old_pack_path}" ]] && rm -f "${l_existed_old_pack_path}"
            fi
        fi

    else
        [[ -d "${installation_dir}" ]] && rm -rf "${installation_dir}"
        [[ "${is_existed}" -eq 1 ]] && mv "${application_backup_path}" "${installation_dir}"
        fnBase_OperationProcedureResult '' 1
    fi

    [[ -n "${pack_save_path}" && -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
}


#########  2-4. Desktop Configuration  #########
fn_DesktopConfiguration(){
    if [[ -d '/usr/share/applications/' ]]; then
        fnBase_OperationProcedureStatement 'Desktop Configuration'
        [[ -f "${installation_dir}/Browser/browser/chrome/icons/default/default48.png" ]] && ln -sf "${installation_dir}/Browser/browser/chrome/icons/default/default48.png" "${pixmaps_png_path}"

        echo -e "[Desktop Entry]\nEncoding=UTF-8\nName=Tor Browser\nGenericName[en]=Web Browser\nComment=Tor Browser is +1 for privacy and -1 for mass surveillance\nType=Application\nCategories=Network;WebBrowser;Security;\nExec=sh -c 'installation_dir/Browser/start-tor-browser --detach' dummy %k\nX-TorBrowser-ExecShell=installation_dir/Browser/start-tor-browser --detach\nIcon=${pixmaps_png_path}\nTerminal=false\nStartupWMClass=Tor Browser\nMimeType=text/html;text/xml;application/xhtml+xml;application/vnd.mozilla.xul+xml;text/mml;" > "${application_desktop_path}"

        # sed -i -r 's@application_name@'"$application_name"'@g' "${application_desktop_path}"
        sed -i -r 's@installation_dir@'"$installation_dir"'@g' "${application_desktop_path}"
        fnBase_OperationProcedureResult "${application_desktop_path}"
    fi
}


#########  2-5. Operation Time Cost  #########
fn_TotalTimeCosting(){
    finish_time=$(date +'%s')        # End Time Of Operation
    total_time_cost=$((finish_time-start_time))   # Total Time Of Operation
    printf "\nTotal time cost is ${c_red}%s${c_normal} seconds!\n" "${total_time_cost}"
}



#########  3. Executing Process  #########
fn_InitializationCheck

fn_UninstallOperation
fn_VersionComparasion
fn_DownloadAndDecompressOperation
fn_DesktopConfiguration
fn_TotalTimeCosting


#########  4. EXIT Singal Processing  #########
# trap "commands" EXIT # execute command when exit from shell
fn_TrapEXIT(){
    fnBase_UnsetVars
    unset installation_dir
    unset is_existed
    unset version_check
    unset source_pack_path
    unset is_uninstall
    unset proxy_server_specify
}

trap fn_TrapEXIT EXIT

# Script End
