#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails

# Official Site: https://www.waterfox.net/

# https://www.waterfox.net/blog/waterfox-2019.10-release-download/
# 1. follow the date versioning system of YY.MM.X
# 2. two versions of Waterfox to choose from (classic/current)

# Attention: Waterfox doesn't provide gpg or SHA-256/512 verification https://github.com/MrAlex94/Waterfox/issues/271

# Target: Automatically Install & Update Waterfox On GNU/Linux
# Developer: MaxdSre

# Change Log:
# - Oct 08, 2020 09:06 Thu ET - initialization check method update
# - Feb 20, 2020 09:03 Thu ET - stop maintaining this script because Waterfox web browser was sold to System1 which is an advertising company https://www.waterfox.net/blog/waterfox-has-joined-system1/  https://www.ghacks.net/2020/02/14/waterfox-web-browser-sold-to-system1/
# - Nov 02, 2019 21:03 Sat ET - support both current and classic version, add custom user profile generation
# - Oct 08, 2019 10:53 Tue ET - change to new base functions
# - Jan 01, 2019 17:39 Tue ET - code reconfiguration, include base funcions from other file
# - Jul 15, 2018 15:20 ~ 16:19 Sun ET - first draft



#########  0-1. Singal Setting  #########
trap fnBase_TrapCleanUp INT QUIT

#########  0-2. Variables Setting  #########
# umask 022    # temporarily change umask value to 022
readonly official_site='https://www.waterfox.net' # https://www.waterfoxproject.org/en-US/waterfox/
# readonly download_page="${official_site}/releases/"  # redirect to https://www.waterfox.net/download/
readonly download_page="${official_site}/download/"  # redirect to https://www.waterfox.net/download/   https://cdn.waterfox.net/releases/linux64/installer/
readonly software_fullname=${software_fullname:-'Waterfox'}
readonly os_type='linux-x86_64'
readonly application_name=${application_name:-'Waterfox'}
installation_dir="/opt/${application_name}"      # Decompression & Installation Path Of Package
readonly version_info_path="${installation_dir}/version"
readonly pixmaps_png_path="/usr/share/pixmaps/${application_name}.png"
readonly application_desktop_path="/usr/share/applications/${application_name}.desktop"
is_existed=${is_existed:-0}    # Default value is 0 assume system has installed Waterfox

version_check=${version_check:-0}
type_choose=${type_choose:-0}
change_language=${change_language:-0}
source_pack_path=${source_pack_path:-}
is_uninstall=${is_uninstall:-0}
proxy_server_specify=${proxy_server_specify:-}


#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | sudo bash -s -- [options] ...

Installing / Updating Waterfox Web Browser On GNU/Linux!
This script requires superuser privileges (eg. root, su).

Attention: This script is not maintaining any more since Feb 20, 2020 Thu because Waterfox web browser was sold to System1 which is an advertising company. (https://www.waterfox.net/blog/waterfox-has-joined-system1/)

[available option]
    -h    --help, show help info
    -c    --check, check current stable release version
    -f pack_path    --specify latest release package save path (e.g. /PATH/*.tar.bz2) or dir (default is ~/Downloads/) in system
    -t type    --choose version type (0 is Current, 1 is Classic version), default is 0
    -p [protocol:]ip:port    --proxy host (http|https|socks4|socks5), default protocol is http
    -u    --uninstall, uninstall software installed
\e[0m"
}

while getopts "hcf:t:up:" option "$@"; do
    case "$option" in
        c ) version_check=1 ;;
        f ) source_pack_path="$OPTARG" ;;
        t ) type_choose="$OPTARG" ;;
        u ) is_uninstall=1 ;;
        p ) proxy_server_specify="$OPTARG" ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done



#########  1-2 Preparation Operation  #########
fn_InitializationCheck(){
    local l_user_codebase_dir="$HOME/.config/axdsop"
    local l_base_function_temp_save_path=''
    local l_need_remove=0
    local l_base_function_url='https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/base_functions.sh'
    l_base_function_temp_save_path=$(find "${l_user_codebase_dir}" -type f -name "${l_base_function_url##*/}" -print 2> /dev/null)
    
    if [[ ! -f "${l_base_function_temp_save_path}" ]]; then
        local l_download_tool=${l_download_tool:-'wget -qO-'}
        [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_tool='curl -fsL'
        l_need_remove=1
        l_base_function_temp_save_path=$(mktemp -t XXXXXXX)
        ${l_download_tool} "${l_base_function_url}" > "${l_base_function_temp_save_path}"
    fi

    if [[ -f "${l_base_function_temp_save_path}" && -s "${l_base_function_temp_save_path}" ]]; then
        . "${l_base_function_temp_save_path}"
        [[ "${l_need_remove}" -eq 1 ]] && rm "${l_base_function_temp_save_path}"
    else
        echo "Sorry, fail to get base script path."
        exit
    fi

    # tput clear  # Echo the clear-screen sequence for the current terminal.
    # specify 1 means need sudo privilege
    fnBase_RunningEnvironmentCheck '1'

    fnBase_CommandExistCheckPhase 'bzip2' # CentOS/Debian/OpenSUSE: bzip2
    fnBase_CommandExistCheckPhase 'tar' # .tar.bz2
}

fn_UtilityLocalVersionDetection(){
    local l_path="${1:-}"
    local l_action=${2:-'--version'}
    local l_output=${l_output:-}

    if [[ "${login_user}" == 'root' ]]; then
        l_output=$("${l_path}" "${l_action}" 2> /dev/null)
    else
        l_output=$(sudo -u "${login_user}" "${l_path}" "${l_action}" 2> /dev/null)
    fi
    # sed -r -n '/Waterfox.*?[[:digit:]]+/{s@^[^[:digit:]]+(.*)$@\1@g;p}'
    # Mozilla Waterfox 56.2.2
    l_output="${l_output##* }"
    echo "${l_output}"
}


#########  2-1. Uninstall  #########
fn_UninstallOperation(){
    if [[ "${is_uninstall}" -eq 1 ]]; then
        if [[ -d "${installation_dir}" ]]; then
            [[ -f "${pixmaps_png_path}" ]] && rm -f "${pixmaps_png_path}"
            [[ -f "${application_desktop_path}" ]] && rm -f "${application_desktop_path}"

            [[ -d "${installation_dir}" ]] && rm -rf "${installation_dir}"
            [[ -d "${installation_dir}${bak_suffix}" ]] && rm -rf "${installation_dir}${bak_suffix}"

            local config_path="${login_user_home}/.waterfox/"
            [[ -d "${config_path}" ]] && rm -rf "${config_path}"    # ~/.waterfox
            local cache_path="${login_user_home}/.cache/waterfox/"
            [[ -d "${cache_path}" ]] && rm -rf "${cache_path}"   # ~/.cache/waterfox

            [[ -d "${installation_dir}" ]] || fnBase_ExitStatement "${software_fullname} is successfully removed from your system!"
        else
            fnBase_ExitStatement "${c_blue}Note${c_normal}: no ${software_fullname} is found in your system!"
        fi
    fi
}


#########  2-2. Latest & Local Version Check  #########
fn_VersionComparasion(){
    fnBase_CentralOutputTitle "${software_fullname}"
    fnBase_OperationPhaseStatement 'Version Detection'

    # - Local version
    current_local_version=${current_local_version:-}
    local current_local_type=${current_local_type:-}

    if [[ -f "${installation_dir}/waterfox" ]]; then
        is_existed=1

        if [[ -f "${version_info_path}" && -n $(sed -r -n '/\|/{p}' "${version_info_path}") ]]; then
            current_local_type=$(cut -d\| -f 1 "${version_info_path}")
            current_local_version=$(cut -d\| -f 2 "${version_info_path}")
        else
            # not same any more, different name format
            current_local_version=$(fn_UtilityLocalVersionDetection "${installation_dir}/waterfox")
        fi

        fnBase_OperationProcedureStatement "Local ${c_bold}${c_yellow}${current_local_type}${c_normal} version detecting"
        fnBase_OperationProcedureResult "${current_local_version}"
    fi
    
    # - Latest online version info
    # fnBase_OperationProcedureStatement 'Online version detecting'

    local l_online_release_info=${l_online_release_info:-}

    case "${type_choose,,}" in
        1 ) type_choose='classic' ;;
        0|* ) type_choose='current' ;;
    esac

    [[ -n "${current_local_type}" && "${current_local_type}" != "${type_choose}" && "${type_choose}" != 'classic' ]] && type_choose="${current_local_type}"

    fnBase_OperationProcedureStatement "Online ${c_bold}${c_yellow}${type_choose}${c_normal} version detecting"

    l_online_release_info=$($download_method "${download_page}" | sed -r -n '{s@<\/[^>]*>@&\n@g;p}' | sed -r -n '/'"${os_type}"'/{s@.*href="([^"]+)".*@\1@g;s@.*waterfox-([^-]+)-([[:digit:].]+)\..*@\1|\2|&@g;/'"${type_choose}"'/!d;p}')
    # type|version|url
    # classic|2019.10|https://storage-waterfox.netdna-ssl.com/releases/linux64/installer/waterfox-classic-2019.10.en-US.linux-x86_64.tar.bz2
    # current|2019.10|https://storage-waterfox.netdna-ssl.com/releases/linux64/installer/waterfox-current-2019.10.en-US.linux-x86_64.tar.bz2

    # classic Mozilla Waterfox 56.3
    # current Mozilla Waterfox 2019.10

    online_release_version=$(echo "${l_online_release_info}" | cut -d\| -f 2)

    if [[ -n "${online_release_version}" ]]; then
        online_release_downloadlink=$(echo "${l_online_release_info}" | cut -d\| -f 3)
    else
        fnBase_OperationProcedureResult '' 1
        fnBase_ExitStatement "${c_red}Fatal error${c_normal}, fail to get online ${c_yellow}${type_choose}${c_normal} version!"
    fi

    online_release_date=${online_release_date:-}
    # https://developer.github.com/v3/repos/releases/#get-the-latest-release
    # relese version | release date
    # 56.2.14|2019-09-03T14:58:40Z
    # 2019.10-classic-1|2019-10-24T21:57:52Z
    online_release_date=$($download_method https://api.github.com/repos/MrAlex94/Waterfox/releases/latest 2> /dev/null | sed -r -n '/(tag_name|published_at)/{s@[^:]*:[[:space:]]*"([^"]*)".*@\1@g;p}' | sed ':a;N;$!ba;s@\n@|@g;' | cut -d\| -f2 | date -f - +"%b %d, %Y" -u) # %b %d, %Y %z %Z

    if [[ -n "${online_release_version}" ]]; then
        fnBase_OperationProcedureResult "${online_release_version} - ${online_release_date}"
    else
        fnBase_OperationProcedureResult '' 1
    fi

    # - Just online version check
    if [[ "${version_check}" -eq 1 ]]; then
        fnBase_ExitStatement ''
    fi

    # - Online & Local Version Comparasion
    if [[ "${is_existed}" -eq 1 ]]; then
        if [[ "${online_release_version}" == "${current_local_version}" ]]; then
            fnBase_ExitStatement "\nLatest ${c_yellow}${type_choose}${c_normal} version (${c_red}${online_release_version}${c_normal}) has been existed in your system."
        elif [[ $(fnBase_VersionNumberComparasion "${current_local_version}" "${online_release_version}" '<') -eq 1 ]]; then
            printf "\nExisted ${c_yellow}%s${c_normal} version local (${c_red}%s${c_normal}) < Latest ${c_yellow}%s${c_normal} version online (${c_red}%s${c_normal}).\n" "${current_local_type}" "${current_local_version}" "${type_choose}" "${online_release_version}"
        fi
    # else
    #     printf "No %s find in your system!\n" "${software_fullname}"
    fi
}


#########  2-3. Download & Decompress Latest Software  #########
fn_DownloadAndDecompressOperation(){
    local l_operation_type=${l_operation_type:-'installing'}
    [[ "${is_existed}" -eq 1 ]] && l_operation_type='updating'
    fnBase_OperationPhaseStatement "${l_operation_type^}"

    # https://storage-waterfox.netdna-ssl.com/releases/linux64/installer/waterfox-56.2.14.en-US.linux-x86_64.tar.bz2
    # https://storage-waterfox.netdna-ssl.com/Waterfox/beta/Linux/waterfox-68.0b1.en-US.linux-x86_64.tar.bz2
    # local online_release_downloadlink
    # online_release_downloadlink=$($download_method "${download_page}" | sed -r -n 's@<\/[^>]*>@&\n@g;p' | sed -r -n '/'"${os_type}"'/{s@.*href="([^"]+)".*$@\1@g;/releases/!d;p}')

    # https://storage-waterfox.netdna-ssl.com/releases/linux64/installer/waterfox-classic-2019.10.en-US.linux-x86_64.tar.bz2
    # https://storage-waterfox.netdna-ssl.com/releases/linux64/installer/waterfox-current-2019.10.en-US.linux-x86_64.tar.bz2

    local online_release_pack_name="${online_release_downloadlink##*/}"
    # https://github.com/MrAlex94/Waterfox/issues/271
    local online_release_dgst=${online_release_dgst:-}

    local l_is_need_download=${l_is_need_download:-1}
    local l_specified_pack_dir=${l_specified_pack_dir:-}

    # specified file path existed
    if [[ "${source_pack_path}" =~ .tar.bz2$ && -s "${source_pack_path}" ]]; then
        [[ "${source_pack_path##*/}" == "${online_release_pack_name}" ]] || fnBase_ExitStatement "Package ${c_red}${source_pack_path}${c_normal} is not same to latest release version name!"

    # specified dir or ~/Downloads existed
    elif [[ (-n "${source_pack_path}" && -d "${source_pack_path}" ) || -d "${user_download_dir}" ]]; then
        if [[ -d "${source_pack_path}" ]]; then
            l_specified_pack_dir="${source_pack_path}"
        elif [[ -d "${user_download_dir}" ]]; then
            l_specified_pack_dir="${user_download_dir}"
        fi

        l_specified_pack_dir="${l_specified_pack_dir%/}"

        if [[ -d "${l_specified_pack_dir}" && -f "${l_specified_pack_dir}/${online_release_pack_name}" ]]; then
            source_pack_path="${l_specified_pack_dir}/${online_release_pack_name}"
        fi
    fi

    if [[ -n "${source_pack_path}" && -f "${source_pack_path}" ]]; then
        l_is_need_download=0
    fi

    # 1.1- existed package path in system
    if [[ -n "${source_pack_path}" && -f "${source_pack_path}" ]]; then
        fnBase_OperationProcedureStatement 'Existed pack'

        fnBase_OperationProcedureResult "${c_red}${source_pack_path%/*}/${c_normal}"
        l_is_need_download=0

        # fnBase_OperationProcedureStatement 'Existed pack dgst verifying'
        # if [[ -n "${online_release_dgst}" && $(fnBase_HashDigestVerification "${source_pack_path}" '512') == "${online_release_dgst}" ]]; then
        #     fnBase_OperationProcedureResult "${c_red}${source_pack_path%/*}/${c_normal}"
        #     l_is_need_download=0
        # else
        #     fnBase_OperationProcedureResult '' 1
        # fi
    fi

    # 2 - Download Directly From Official Site
    local pack_save_path=${pack_save_path:-}

    if [[ "${l_is_need_download}" -eq 1 ]]; then
        fnBase_OperationProcedureStatement "Downloading ${c_red}${online_release_pack_name}${c_normal}"
        pack_save_path="${temp_save_dir}/${online_release_pack_name}"
        [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"

        $download_method "${online_release_downloadlink}" > "${pack_save_path}"

        if [[ -s "${pack_save_path}" ]]; then
            fnBase_OperationProcedureResult
        else
            [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
            fnBase_OperationProcedureResult '' 1
        fi

        # fnBase_OperationProcedureStatement "SHA512 dgst verification"
        # if [[ -n "${online_release_dgst}" && $(fnBase_HashDigestVerification "${pack_save_path}" '512') == "${online_release_dgst}"  ]]; then
        #     fnBase_OperationProcedureResult
        # else
        #     [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
        #     fnBase_OperationProcedureResult '' 1
        # fi

        # use variable "source_pack_path" to execute decompress operation, variable "pack_save_path" used for delete temporary download package
        source_pack_path="${pack_save_path}"
    fi

    # 3 - Decompress & Extract
    fnBase_OperationProcedureStatement 'Installing'
    local application_backup_path="${installation_dir}${bak_suffix}"
    [[ -d "${application_backup_path}" ]] && rm -rf "${application_backup_path}"

    [[ -d "${installation_dir}" ]] && mv "${installation_dir}" "${application_backup_path}"
    [[ -d "${installation_dir}" ]] || mkdir -p "${installation_dir}"
    tar xf "${source_pack_path}" -C "${installation_dir}" --strip-components=1 2> /dev/null
    chown -R root:root "${installation_dir}"
    find "${installation_dir}" -type f -name "*.so" -exec chmod 644 {} \; 2> /dev/null

    local new_installed_version=${new_installed_version:-}

    if [[ "${type_choose}" == 'current' ]]; then
        new_installed_version=$(fn_UtilityLocalVersionDetection "${installation_dir}/waterfox")
    else
        # use different version format
        new_installed_version="${online_release_version}"
    fi
   
    if [[ "${online_release_version}" == "${new_installed_version}" ]]; then
        echo "${type_choose}|${online_release_version}|${online_release_date}" > "${version_info_path}"
        [[ -f "${pixmaps_png_path}" ]] && rm -f "${pixmaps_png_path}"
        [[ -f "${application_desktop_path}" ]] && rm -f "${application_desktop_path}"
        [[ -d "${application_backup_path}" ]] && rm -rf "${application_backup_path}"
        fnBase_OperationProcedureResult "${c_red}${installation_dir%/}/${c_normal}"

        # copy newly downloaded pack to ~/Downloads
        local l_user_download_save_path=${l_user_download_save_path:-"${user_download_dir}/${source_pack_path##*/}"}
        if [[ -d "${user_download_dir}" && ! -f "${l_user_download_save_path}" ]]; then
            cp "${source_pack_path}" "${l_user_download_save_path}"
            chown "${login_user}" "${l_user_download_save_path}"
            chgrp -hR "${login_user}" "${l_user_download_save_path}" 2> /dev/null

            # removed old version package existed
            if [[ "${is_existed}" -eq 1 ]]; then
                local l_existed_old_pack_path=${l_existed_old_pack_path:-"${user_download_dir}/${online_release_pack_name//${online_release_version}/${current_local_version}}"}
                [[ -f "${l_existed_old_pack_path}" ]] && rm -f "${l_existed_old_pack_path}"
            fi
        fi

    else
        [[ -d "${installation_dir}" ]] && rm -rf "${installation_dir}"
        [[ "${is_existed}" -eq 1 ]] && mv "${application_backup_path}" "${installation_dir}"
        fnBase_OperationProcedureResult '' 1
    fi

    [[ -n "${pack_save_path}" && -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
}


#########  2-4. User Profiles Configuration  #########
fn_UserProfileGeneration(){
    if [[ "${type_choose}" == 'current' ]]; then
        local l_firefox_bin_path="${installation_dir}/waterfox"
        local l_config_dir="${login_user_home}/.waterfox/"
        local l_profiles_ini=${l_profiles_ini:-"${l_config_dir}/profiles.ini"}
        local l_profile_name="${login_user}"

        local l_userjs_harden_script='https://gitlab.com/axdsop/nixnotes/raw/master/ShellScripts/Toolkits/Miscellaneous/firefox_user_js_secure_hardening.sh'
        local l_temp_save_path="/tmp/${l_userjs_harden_script##*/}"

        $download_method "${l_userjs_harden_script}" > "${l_temp_save_path}"
        
        if [[ -f "${l_temp_save_path}" ]]; then
            source "${l_temp_save_path}"
            fn_UserProfileConfiguration "${l_firefox_bin_path}" "${l_config_dir}" "${l_config_dir}/profiles.ini" "${login_user}" '1'
            # function fn_UserProfileConfiguration lists in script
        fi
    fi
}

#########  2-5. Desktop Configuration  #########
fn_DesktopConfiguration(){
    if [[ -d '/usr/share/applications/' ]]; then
        fnBase_OperationProcedureStatement 'Desktop Configuration'

        local l_mozicon_path=${l_mozicon_path:-}

        if [[ -f "${installation_dir}/browser/icons/mozicon128.png" ]]; then
            l_mozicon_path="${installation_dir}/browser/icons/mozicon128.png"
        elif [[ -f "${installation_dir}/browser/chrome/icons/default/default256.png" ]]; then
            l_mozicon_path="${installation_dir}/browser/chrome/icons/default/default256.png"
        fi

        [[ -n "${l_mozicon_path}" ]] && ln -sf "${l_mozicon_path}" "${pixmaps_png_path}"

        echo -e "[Desktop Entry]\nEncoding=UTF-8\nName=Waterfox Web Browser\nGenericName[en]=Web Browser\nComment=Waterfox web browser\nExec=installation_dir/waterfox %u\nIcon=application_name.png\nTerminal=false\nType=Application\nStartupWMClass=Firefox-bin\nMimeType=text/html;text/xml;application/xhtml+xml;application/vnd.mozilla.xul+xml;text/mml;application/x-xpinstall;x-scheme-handler/http;x-scheme-handler/https;x-scheme-handler/ftp;\nCategories=Network;WebBrowser;Application;" > "${application_desktop_path}"

        sed -i -r 's@application_name@'"$application_name"'@g' "${application_desktop_path}"
        sed -i -r 's@installation_dir@'"$installation_dir"'@g' "${application_desktop_path}"
        fnBase_OperationProcedureResult "${application_desktop_path}"
    fi
}


#########  2-6. Operation Time Cost  #########
fn_TotalTimeCosting(){
    finish_time=$(date +'%s')        # End Time Of Operation
    total_time_cost=$((finish_time-start_time))   # Total Time Of Operation
    printf "\nTotal time cost is ${c_red}%s${c_normal} seconds!\n" "${total_time_cost}"
}


#########  3. Executing Process  #########
fn_InitializationCheck

fn_UninstallOperation
fn_VersionComparasion
fn_DownloadAndDecompressOperation
fn_UserProfileGeneration
fn_DesktopConfiguration
fn_TotalTimeCosting


#########  4. EXIT Singal Processing  #########
# trap "commands" EXIT # execute command when exit from shell
fn_TrapEXIT(){
    fnBase_UnsetVars
    unset lang
    unset installation_dir
    unset is_existed
    unset version_check
    unset type_choose
    unset source_pack_path
    unset change_language
    unset is_uninstall
    unset proxy_server_specify
}

trap fn_TrapEXIT EXIT

# Script End
