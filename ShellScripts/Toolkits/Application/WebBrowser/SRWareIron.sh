#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails


# Official Site
# - https://www.srware.net/iron/
# - https://www.srware.net/en/software_srware_iron.php
# - https://www.srware.net/en/software_srware_iron_chrome_vs_iron.php
# - https://www.srware.net/forum/viewforum.php?f=18


# Target: Automatically Install & Update SRWare Iron On GNU/Linux
# Developer: MaxdSre

# Change Log:
# - Jun 12, 2022 10:19 Sun ET - fix online version extraction issue
# - May 30, 2021 21:00 Sun ET - SRware change the package decompress method via Makeself
# - Oct 08, 2020 09:05 Thu ET - initialization check method update
# - Jul 22, 2020 05:47 Wed ET - Site SSL certificate expired on July 21, 2020 (issued on 4/23/2018)
# - May 22, 2020 16:40 Fri ET - add software soft link into /usr/bin
# - Oct 08, 2019 15:53 Tue ET - change to new base functions
# - Jan 01, 2019 11:15 Tue ET - code reconfiguration, include base funcions from other file
# - Oct 24, 2018 11:07 Wed +0800 - release date code change
# - Jun 25, 2018 12:27 Mon ET - code reconfiguration
# - Mar 23, 2018 09:43 ET - solve installation failure if download page version is not same to forum page version
# - June 07, 2017 16:39 Wed +0800
# - May 9, 2017 14:49 Tue ET
# - Feb 15, 2017 15:24 Wed +0800


# /opt/iron-linux-64/chrome: error while loading shared libraries: libXss.so.1: cannot open shared object file: No such file or directory
# solution: yum install libXScrnSaver

# - Setting default browser
# gnome-www-browser x-www-browser
# http://linux.palemoon.org/help/installation/
# https://wiki.debian.org/HOWTO/DefaultWebBrowser



#########  0-1. Singal Setting  #########
trap fnBase_TrapCleanUp INT QUIT

#########  0-2. Variables Setting  #########
# umask 022    # temporarily change umask value to 022
readonly official_site='https://www.srware.net'
readonly official_iron_site="${official_site}/en/software_srware_iron.php"   #SRWare Iron Official Site
readonly download_page="${official_site}/en/software_srware_iron_download.php"      # Download Page (Extract version no. under Windows, version no. under Linux is older then Windows)
readonly forum_support_page="${official_site}/forum/viewforum.php?f=18"  #Extract Linux version no. via forum page
readonly software_fullname=${software_fullname:-'SRWare Iron'}
readonly application_name=${application_name:-'SRWareIron'}
installation_dir="/opt/${application_name}"
iron_bin_path="${installation_dir}/chrome"
readonly usr_bin_softlink_path="/usr/bin/chrome"
readonly pixmaps_png_path="/usr/share/pixmaps/${application_name}.png"
readonly application_desktop_path="/usr/share/applications/${application_name}.desktop"
is_existed=${is_existed:-1}      # Default value is 1， assume system has installed SRWare Iron

version_check=${version_check:-0}
source_pack_path=${source_pack_path:-}
is_uninstall=${is_uninstall:-0}
proxy_server_specify=${proxy_server_specify:-}


#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | sudo bash -s -- [options] ...

Installing / Updating SRWare Iron Web Browser On GNU/Linux!
This script requires superuser privileges (eg. root, su).

[available option]
    -h    --help, show help info
    -c    --check, check current stable release version
    -f pack_path    --specify latest release package save path (e.g. /PATH/*.tar.gz) or dir (default is ~/Downloads/) in system
    -p [protocol:]ip:port    --proxy host (http|https|socks4|socks5), default protocol is http
    -u    --uninstall, uninstall software installed
\e[0m"
}

while getopts "hcf:up:" option "$@"; do
    case "$option" in
        c ) version_check=1 ;;
        f ) source_pack_path="$OPTARG" ;;
        u ) is_uninstall=1 ;;
        p ) proxy_server_specify="$OPTARG" ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done


#########  1-2 Preparation Operation  #########
fn_InitializationCheck(){
    local l_user_codebase_dir="$HOME/.config/axdsop"
    local l_base_function_temp_save_path=''
    local l_need_remove=0
    local l_base_function_url='https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/base_functions.sh'
    l_base_function_temp_save_path=$(find "${l_user_codebase_dir}" -type f -name "${l_base_function_url##*/}" -print 2> /dev/null)
    
    if [[ ! -f "${l_base_function_temp_save_path}" ]]; then
        local l_download_tool=${l_download_tool:-'wget -qO-'}
        [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_tool='curl -fsL'
        l_need_remove=1
        l_base_function_temp_save_path=$(mktemp -t XXXXXXX)
        ${l_download_tool} "${l_base_function_url}" > "${l_base_function_temp_save_path}"
    fi

    if [[ -f "${l_base_function_temp_save_path}" && -s "${l_base_function_temp_save_path}" ]]; then
        . "${l_base_function_temp_save_path}"
        [[ "${l_need_remove}" -eq 1 ]] && rm "${l_base_function_temp_save_path}"
    else
        echo "Sorry, fail to get base script path."
        exit
    fi

    # tput clear  # Echo the clear-screen sequence for the current terminal.
    # specify 1 means need sudo privilege
    fnBase_RunningEnvironmentCheck '1'

    fnBase_CommandExistCheckPhase 'gzip' # CentOS/Debian/OpenSUSE: gzip
    fnBase_CommandExistCheckPhase 'tar' # .tar.gz
}


#########  2-1. Uninstall  #########
fn_UninstallOperation(){
    if [[ "${is_uninstall}" -eq 1 ]]; then
        if [[ -d "${installation_dir}" ]]; then
            # remove soft link
            [[ -L "${usr_bin_softlink_path}" && $(readlink -f "${usr_bin_softlink_path}") == "${iron_bin_path}" ]] && rm -f "${usr_bin_softlink_path}"

            [[ -f "${pixmaps_png_path}" ]] && rm -f "${pixmaps_png_path}"
            [[ -f "${application_desktop_path}" ]] && rm -f "${application_desktop_path}"

            [[ -d "${installation_dir}" ]] && rm -rf "${installation_dir}"
            [[ -d "${installation_dir}${bak_suffix}" ]] && rm -rf "${installation_dir}${bak_suffix}"

            local config_path="${login_user_home}/.config/chromium"
            [[ -d "${config_path}" ]] && rm -rf "${config_path}"    # ~/.config/chromium
            local cache_path="${login_user_home}/.cache/chromium"
            [[ -d "${cache_path}" ]] && rm -rf "${cache_path}"   # ~/.cache/chromium

            [[ -d "${installation_dir}" ]] || fnBase_ExitStatement "${software_fullname} is successfully removed from your system!"
        else
            fnBase_ExitStatement "${c_blue}Note${c_normal}: no ${software_fullname} is found in your system!"
        fi
    fi
}


#########  2-2. Latest & Local Version Check  #########
fn_LibarylibgconfCheck(){
    # 1 - - error while loading shared libraries: libXss.so.1: cannot open shared object file: No such file or directory
    # apt|libxss1|/usr/lib/x86_64-linux-gnu/libXss.so.1
    # zypper | libXss1 | /usr/lib64/libXss.so.1
    # yum | libXScrnSaver | /usr/lib64/libXss.so.1
    # pacman | libxss | /usr/lib64/libXss.so.1

    [[ -z $(find /usr/lib* -name 'libXss.so.1' -print 2>/dev/null) ]] && fnBase_ExitStatement "${c_red}Fatal Error${c_normal}: atom need shared libraries ${c_blue}libXss.so.1${c_normal} (apt: libxss1, pacman: libxss, zypper: libXss1, yum: libXScrnSaver)!"
}

fn_VersionComparasion(){
    fnBase_CentralOutputTitle "${software_fullname}"
    fnBase_OperationPhaseStatement 'Version Detection'

    # - Local version
    current_local_version=${current_local_version:-}
    if [[ -f "${iron_bin_path}" ]]; then
        current_local_version=$("${iron_bin_path}" --version | sed -r -n 's@^[^[:digit:]]+([[:digit:].]+).*$@\1@g;p')
    fi

    [[ -z "${current_local_version}" ]] && is_existed=0
    if [[ "${is_existed}" -eq 1 ]]; then
        fnBase_OperationProcedureStatement 'Local version detecting'
        fnBase_OperationProcedureResult "${current_local_version}"
    fi

    # - Latest online version info
    fnBase_OperationProcedureStatement 'Online version detecting'
    online_release_info=$(mktemp -t "${mktemp_format}")
    [[ -f "${online_release_info}" ]] && rm -f "${online_release_info}"
    ${download_method} "${forum_support_page}" > "${online_release_info}"
    online_release_version=$(sed -r -n '/Announcements/,/Topics/{/Stable for Linux/{s@.*Iron-Version:[[:space:]]([^[:space:]]+).*@\1@g;p}}' "${online_release_info}")
    online_release_date=$(sed -r -n '/Announcements/,/Topics/{/Stable for Linux/,/username-coloured/{/username-coloured/{s@<[^>]+>@@g;s@.*&[lr]aquo;[[:space:]]*(.*)$@\1@g;p}}}' "${online_release_info}" | date -f - +"%b %d, %Y" 2> /dev/null)
    #online_release_date=$(sed -r -n '/Announcements/,/Topics/{/Stable for Linux/,/username-coloured/{/username-coloured/{s@.*&raquo;[[:space:]]*(.*)$@\1@g;p}}}' "${online_release_info}" | date -f - +"%F")
    # https://stackoverflow.com/questions/8742476/pipe-string-to-gnu-date-for-conversion-how-to-make-it-read-from-stdin

    if [[ -n "${online_release_version}" ]]; then
        fnBase_OperationProcedureResult "${online_release_version} - ${online_release_date}"
    else
        fnBase_OperationProcedureResult '' 1
        fnBase_ExitStatement ''
    fi

    # - Just online version check
    if [[ "${version_check}" -eq 1 ]]; then
        fnBase_ExitStatement ''
    fi

    # - Online & Local Version Comparasion
    if [[ "${is_existed}" -eq 1 ]]; then
        if [[ "${online_release_version}" == "${current_local_version}" ]]; then
            fnBase_ExitStatement "\nLatest version (${c_red}${online_release_version}${c_normal}) has been existed in your system."
        elif [[ $(fnBase_VersionNumberComparasion "${current_local_version}" "${online_release_version}" '<') -eq 1 ]]; then
            printf "\nExisted version local (${c_red}%s${c_normal}) < Latest version online (${c_red}%s${c_normal}).\n" "${current_local_version}" "${online_release_version}"
        fi
    # else
    #     printf "No %s find in your system!\n" "${software_fullname}"
    fi
}


#########  2-3. Download & Decompress Latest Software  #########
fn_DownloadAndDecompressOperation(){
    local l_operation_type=${l_operation_type:-'installing'}
    [[ "${is_existed}" -eq 1 ]] && l_operation_type='updating'
    fnBase_OperationPhaseStatement "${l_operation_type^}"

    local online_release_downloadlink
    online_release_downloadlink="${official_site}/downloads/iron-linux-64.tar.gz"

    local online_release_pack_name=${online_release_pack_name:-"iron-${online_release_version}.tar.gz"}

    local l_is_need_download=${l_is_need_download:-1}
    local l_specified_pack_dir=${l_specified_pack_dir:-}

    # - specified file path existed
    if [[ "${source_pack_path}" =~ .tar.gz$ && -s "${source_pack_path}" ]]; then
        [[ "${source_pack_path##*/}" == "${online_release_pack_name}" ]] || fnBase_ExitStatement "Package ${c_red}${source_pack_path}${c_normal} is not same to latest release version name!"

    # - specified dir or ~/Downloads existed
    elif [[ (-n "${source_pack_path}" && -d "${source_pack_path}" ) || -d "${user_download_dir}" ]]; then
        if [[ -d "${source_pack_path}" ]]; then
            l_specified_pack_dir="${source_pack_path}"
        elif [[ -d "${user_download_dir}" ]]; then
            l_specified_pack_dir="${user_download_dir}"
        fi

        l_specified_pack_dir="${l_specified_pack_dir%/}"

        if [[ -d "${l_specified_pack_dir}" && -f "${l_specified_pack_dir}/${online_release_pack_name}" ]]; then
            source_pack_path="${l_specified_pack_dir}/${online_release_pack_name}"
        fi
    fi

    # 1.1- existed package path in system
    if [[ -n "${source_pack_path}" && -f "${source_pack_path}" ]]; then
        l_is_need_download=0
    fi

    # 2 - Download Directly From Official Site
    local pack_save_path=${pack_save_path:-}

    if [[ "${l_is_need_download}" -eq 1 ]]; then
        fnBase_OperationProcedureStatement "Downloading ${c_red}${online_release_pack_name}${c_normal}"
        pack_save_path="${temp_save_dir}/${online_release_pack_name}"
        [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"

        ${download_method} "${online_release_downloadlink}" > "${pack_save_path}"

        if [[ -s "${pack_save_path}" ]]; then
            fnBase_OperationProcedureResult
        else
            [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
            fnBase_OperationProcedureResult '' 1
        fi

        # use variable "source_pack_path" to execute decompress operation, variable "pack_save_path" used for delete temporary download package
        source_pack_path="${pack_save_path}"
    fi

    # 3 - Decompress & Extract
    fnBase_OperationProcedureStatement 'Installing'
    local application_backup_path="${installation_dir}${bak_suffix}"
    [[ -d "${application_backup_path}" ]] && rm -rf "${application_backup_path}"

    [[ -d "${installation_dir}" ]] && mv "${installation_dir}" "${application_backup_path}"
    [[ -d "${installation_dir}" ]] || mkdir -p "${installation_dir}"
    tar xf "${source_pack_path}" -C "${installation_dir}" --strip-components=1 2> /dev/null

    if [[ -n $(find "${installation_dir}" -maxdepth 0 -empty 2> /dev/null) ]]; then
        tar xf "${source_pack_path}" -C "${installation_dir}" 2> /dev/null
    fi

    # Identification: Iron Self-Extractor / Target directory: iron-linux-64 / Uncompressed size: 359804 KB / Compression: bzip2
    if [[ -f "${installation_dir}/iron-linux-64.sh" ]]; then
        bash "${installation_dir}/iron-linux-64.sh" --accept --quiet --target "${installation_dir}" 1> /dev/null
        # SRWare Iron is based on the Soucecode of Chromium. It is licensed under the BSD-license. To get the full License, Terms&Conditions, please check https://www.srware.net/license.txt and continue only if you accept.
    fi

    chown root:root "${installation_dir}"
    # chown -R root:root "${installation_dir}"

    local new_installed_version=${new_installed_version:-}
    new_installed_version=$("${iron_bin_path}" --version | sed -r -n 's@^[^[:digit:]]+([[:digit:].]+).*$@\1@g;p')

    local l_download_page_version=${l_download_page_version:-}
    l_download_page_version=$(${download_method} "${download_page}" | sed -r -n '/Version:/{/href=/d;s@.*>([[:digit:].]+)<.*@\1@g;p;q}')

    if [[ "${online_release_version}" == "${new_installed_version}" || "${l_download_page_version}" == "${new_installed_version}" ]]; then
        [[ -f "${pixmaps_png_path}" ]] && rm -f "${pixmaps_png_path}"
        [[ -f "${application_desktop_path}" ]] && rm -f "${application_desktop_path}"
        [[ -d "${application_backup_path}" ]] && rm -rf "${application_backup_path}"
        fnBase_OperationProcedureResult "${c_red}${installation_dir%/}/${c_normal}"

        # create soft link to directory /usr/bin
        ln -fs "${iron_bin_path}" ${usr_bin_softlink_path}

        # copy newly downloaded pack to ~/Downloads
        local l_user_download_save_path=${l_user_download_save_path:-"${user_download_dir}/${source_pack_path##*/}"}
        if [[ -d "${user_download_dir}" && ! -f "${l_user_download_save_path}" ]]; then
            cp "${source_pack_path}" "${l_user_download_save_path}"
            chown "${login_user}" "${l_user_download_save_path}"
            chgrp -hR "${login_user}" "${l_user_download_save_path}" 2> /dev/null

            # removed old version package existed
            if [[ "${is_existed}" -eq 1 ]]; then
                local l_existed_old_pack_path=${l_existed_old_pack_path:-"${user_download_dir}/${online_release_pack_name//${online_release_version}/${current_local_version}}"}
                [[ -f "${l_existed_old_pack_path}" ]] && rm -f "${l_existed_old_pack_path}"
            fi
        fi

    else
        [[ -d "${installation_dir}" ]] && rm -rf "${installation_dir}"
        [[ "${is_existed}" -eq 1 ]] && mv "${application_backup_path}" "${installation_dir}"
        fnBase_OperationProcedureResult '' 1
    fi

    find "${installation_dir}" -type f -name "*.so" -exec chmod 644 {} \; 2> /dev/null

    # - Deprecated Configs
    # some files has execute permission by default
    # find "${installation_dir}" -type d -exec 750 {} \; 2> /dev/null
    # find "${installation_dir}" -type f -exec 640 {} \; 2> /dev/null

    # [[ -f "${installation_dir}/chrome" ]] && chmod 0750 "${installation_dir}/chrome"

    # # find / -perm -u=s -type f 2>/dev/null
    # if [[ -f "${installation_dir}/chrome-sandbox" ]]; then
    #     # FATAL:setuid_sandbox_host.cc(157)] The SUID sandbox helper binary was found, but is not configured correctly. Rather than run without sandboxing I'm aborting now. You need to make sure that /opt/iron-linux-64/chrome-sandbox is owned by root and has mode 4755.
    #     chown root:root "${installation_dir}/chrome-sandbox"
    #     chmod 4755 "${installation_dir}/chrome-sandbox"
    # fi

    # if [[ -f "${installation_dir}/chrome-wrapper" ]]; then
    #     # chmod 4755 "${installation_dir}/chrome-wrapper"
    #     chmod 0750 "${installation_dir}/chrome-wrapper"
    # fi

    [[ -n "${pack_save_path}" && -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
}


#########  2-4. Desktop Configuration  #########
fn_DesktopConfiguration(){
    if [[ -d '/usr/share/applications' ]]; then
        fnBase_OperationProcedureStatement 'Desktop Configuration'

        [[ -f "${installation_dir}/product_logo_48.png" ]] && ln -sf "${installation_dir}/product_logo_48.png" "${pixmaps_png_path}"

        echo -e "[Desktop Entry]\nEncoding=UTF-8\nName=SRWare Iron Browser\nGenericName[en]=Web Browser\nComment=SRWare Iron Browser\nExec=${iron_bin_path} %u\nIcon=${pixmaps_png_path}\nTerminal=false\nType=Application\nMimeType=text/html;text/xml;application/xhtml+xml;application/vnd.mozilla.xul+xml;text/mml;\nCategories=Network;WebBrowser;Application;" > "${application_desktop_path}"

        # sed -i -r 's@application_name@'"$application_name"'@g' "${application_desktop_path}"
        # sed -i -r 's@installation_dir@'"$installation_dir"'@g' "${application_desktop_path}"

        fnBase_OperationProcedureResult "${application_desktop_path}"
    fi
}


#########  2-5. Operation Time Cost  #########
fn_TotalTimeCosting(){
    finish_time=$(date +'%s')        # End Time Of Operation
    total_time_cost=$((finish_time-start_time))   # Total Time Of Operation
    printf "\nTotal time cost is ${c_red}%s${c_normal} seconds!\n" "${total_time_cost}"
}


#########  3. Executing Process  #########
fn_InitializationCheck

fn_UninstallOperation
fn_LibarylibgconfCheck
fn_VersionComparasion
fn_DownloadAndDecompressOperation
fn_DesktopConfiguration
fn_TotalTimeCosting



#########  4. EXIT Singal Processing  #########
# trap "commands" EXIT # execute command when exit from shell
fn_TrapEXIT(){
    fnBase_UnsetVars
    unset installation_dir
    unset is_existed
    unset version_check
    unset source_pack_path
    unset is_uninstall
    unset proxy_server_specify
}

trap fn_TrapEXIT EXIT


# Script End
