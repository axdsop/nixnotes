# Toolkits - Application / Web browser

## List

* [Mozilla Firefox](./MozillaFirefox.sh)
* [Waterfox](./Waterfox.sh)
* [GNUzilla and IceCat](./GNUzillaIceCat.sh)
* [SRWare Iron](./SRWareIron.sh)
* [Tor browser](./TorBrowser.sh)

<!-- End -->
