#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails


# Official Site:
# - https://www.mozilla.org/en-US/firefox/
# - https://blog.mozilla.org/security/category/announcements/
# - http://kb.mozillazine.org/Knowledge_Base
# - http://kb.mozillazine.org/Profile_folder_-_Firefox

# about:config configuration
# - https://support.mozilla.org/en-US/kb/about-config-editor-firefox
# - https://developer.mozilla.org/en-US/docs/Mozilla/Preferences/A_brief_guide_to_Mozilla_preferences
# - https://developer.mozilla.org/en-US/docs/Mozilla/Command_Line_Options
# - http://kb.mozillazine.org/User.js_file

# omni.ja how to change default search engine list?
# resource://search-extensions/  list file in 'chrome/browser/search-extensions/' in file browser/omni.ja
# unpack via 'unzip', pack via 'zip -0DXqr omni.ja *'
# - https://developer.mozilla.org/en-US/docs/Mozilla/About_omni.ja_(formerly_omni.jar)
# - remove chrome/browser/content/{pocket,search-extensions}

# Target: Automatically Install & Update Mozilla Firefox On GNU/Linux
# Developer: MaxdSre

# Change Log:
# - Jul 12, 2021 19:57 Mon ET - add SHA512SUMS{,.asc} GPG check
# - Dec 14, 2020 17:04 Mon ET - remove features .xpi, add omni.ja note
# - Oct 08, 2020 09:04 Thu ET - initialization check method update
# - Jul 30, 2020 09:15 Thu ET - hash digest wrap with string `b' '
# - Jul 28, 2020 08:06 Tue ET - add WebRender enable/disable in custom user profile
# - Jun 18, 2020 16:01 Thu ET - enable MPRIS support
# - May 14, 2020 01:39 Thu ET - add libary dependency check
# - May 02, 2020 08:50 Sat ET - add software soft link into /usr/bin
# - Mar 29, 2020 14:05 Sun ET - user js operation function optimization
# - Nov 02, 2019 21:59 Sat ET - seperate custom user profile generation as a single script file, invoke via command source
# - Oct 08, 2019 11:43 Tue ET - change to new base functions
# - Sep 12, 2019 09:43 Thu ET - configure DNS-over-HTTPS (DoH) in user.js
# - Jun 05, 2019 14:08 Wed ET - add custom user profile generation, add user.js to harden firefox from GitHub
# - Apr 10, 2019 14:26 Wed ET - firefox redesign html code, update release version extraction
# - Dec 31, 2018 16:32 Mon ET - code reconfiguration, include base funcions from other file
# - Jun 12, 2018 11:28 Tue ET - code reconfiguration
# - May 09, 2018 12:19 Wed ET - Solve `Running Firefox as root in a regular user's session is not supported` occur in version 60.0 while executing `$PATH/firefox --version`
# - Jun 07, 2017 14:19 Wed +0800
# - May 15, 2017 09:30 Mon ET
# - Feb 15, 2017 14:38 Wed +0800


# From version 60.0, it prompt error info:
# Running Firefox as root in a regular user's session is not supported.  ($XAUTHORITY is /run/user/1000/gdm/Xauthority which is owned by maxdsre.)

# GPG key update
# https://blog.mozilla.org/security/2021/06/02/updating-gpg-key-for-signing-firefox-releases/
# Mozilla regularly rotates their GPG signing subkey every two years. The new GPG subkey’s fingerprint is 14F2 6682 D091 6CDD 81E3 7B6D 61B7 B526 D98F 0353, and will expire on 2023-05-17.


# Action|Command
# ---|---
# Support|about:support
# User Profiles|about:profiles
# Enabled Plugins|about:plugins
# Build Configuration|about:buildconfig
# Memory Use|about:memory
# Performance|about:performance
# Registered Service Workers|about:serviceworkers



#########  0-1. Singal Setting  #########
trap fnBase_TrapCleanUp INT QUIT

#########  0-2. Variables Setting  #########
# umask 022    # temporarily change umask value to 022
readonly official_site='https://www.mozilla.org/en-US/firefox/'
readonly download_page="${official_site}all/"
readonly release_note_page="${official_site}notes/"
readonly software_fullname=${software_fullname:-'Mozilla Firefox'}
lang=${lang:-'en-US'}  #en-US, zh-TW
readonly os_type='linux-x86_64'
readonly application_name=${application_name:-'MozillaFirefox'}
installation_dir="/opt/${application_name}"
firefox_bin_path="${installation_dir}/firefox"
readonly usr_bin_softlink_path="/usr/bin/firefox"
readonly pixmaps_png_path="/usr/share/pixmaps/${application_name}.png"
readonly application_desktop_path="/usr/share/applications/${application_name}.desktop"
is_existed=${is_existed:-1}      # Default value is 1， assume system has installed Mozilla Firefox

version_check=${version_check:-0}
change_language=${change_language:-0}
source_pack_path=${source_pack_path:-}
is_uninstall=${is_uninstall:-0}
proxy_server_specify=${proxy_server_specify:-}
base_function_path=${base_function_path:-}


#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | sudo bash -s -- [options] ...

Installing / Updating Mozilla Firefox Web Browser On GNU/Linux!
This script requires superuser privileges (eg. root, su).

[available option]
    -h    --help, show help info
    -c    --check, check current stable release version
    -f pack_path    --specify latest release package save path (e.g. /PATH/*.tar.bz2) or dir (default is ~/Downloads/) in system
    -l    --change language to 'zh-TW', default is 'en-US'
    -p [protocol:]ip:port    --proxy host (http|https|socks4|socks5), default protocol is http
    -u    --uninstall, uninstall software installed
\e[0m"
}

while getopts "hcf:lup:" option "$@"; do
    case "$option" in
        c ) version_check=1 ;;
        f ) source_pack_path="$OPTARG" ;;
        l ) change_language=1 ;;
        u ) is_uninstall=1 ;;
        p ) proxy_server_specify="$OPTARG" ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done



#########  1-2 Preparation Operation  #########
fn_InitializationCheck(){
    local l_user_codebase_dir="$HOME/.config/axdsop"
    local l_base_function_temp_save_path=''
    local l_need_remove=0
    local l_base_function_url='https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/base_functions.sh'
    l_base_function_temp_save_path=$(find "${l_user_codebase_dir}" -type f -name "${l_base_function_url##*/}" -print 2> /dev/null)
    
    if [[ ! -f "${l_base_function_temp_save_path}" ]]; then
        local l_download_tool=${l_download_tool:-'wget -qO-'}
        [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_tool='curl -fsL'
        l_need_remove=1
        l_base_function_temp_save_path=$(mktemp -t XXXXXXX)
        ${l_download_tool} "${l_base_function_url}" > "${l_base_function_temp_save_path}"
    fi

    if [[ -f "${l_base_function_temp_save_path}" && -s "${l_base_function_temp_save_path}" ]]; then
        . "${l_base_function_temp_save_path}"
        [[ "${l_need_remove}" -eq 1 ]] && rm "${l_base_function_temp_save_path}"
    else
        echo "Sorry, fail to get base script path."
        exit
    fi

    # tput clear  # Echo the clear-screen sequence for the current terminal.
    # specify 1 means need sudo privilege
    fnBase_RunningEnvironmentCheck '1'

    fnBase_CommandExistCheckPhase 'bzip2' # CentOS/Debian/OpenSUSE: bzip2
    fnBase_CommandExistCheckPhase 'tar' # .tar.bz2
}


#########  2-1. Uninstall  #########
fn_UninstallOperation(){
    if [[ "${is_uninstall}" -eq 1 ]]; then
        if [[ -d "${installation_dir}" ]]; then
            # remove soft link
            [[ -L "${usr_bin_softlink_path}" && $(readlink -f "${usr_bin_softlink_path}") == "${firefox_bin_path}" ]] && rm -f "${usr_bin_softlink_path}"

            [[ -f "${pixmaps_png_path}" ]] && rm -f "${pixmaps_png_path}"
            [[ -f "${application_desktop_path}" ]] && rm -f "${application_desktop_path}"

            [[ -d "${installation_dir}" ]] && rm -rf "${installation_dir}"
            [[ -d "${installation_dir}${bak_suffix}" ]] && rm -rf "${installation_dir}${bak_suffix}"

            local l_mozilla_config_dir="${login_user_home}/.mozilla/"
            local l_mozilla_cache_dir="${login_user_home}/.cache/mozilla/"

            [[ -d "${l_mozilla_config_dir%/}/firefox/" ]] && rm -rf "${l_mozilla_config_dir%/}/firefox/"
            [[ -d "${l_mozilla_cache_dir%/}/firefox/" ]] && rm -rf "${l_mozilla_cache_dir%/}/firefox/"

            # remove empty dir
            [[ -z $(find "${l_mozilla_config_dir}" -maxdepth 1 -not -path ${l_mozilla_config_dir} -print 2> /dev/null) && -d "${l_mozilla_config_dir}" ]] && rm -rf "${l_mozilla_config_dir}"
            [[ -z $(find "${l_mozilla_cache_dir}" -maxdepth 1 -not -path ${l_mozilla_cache_dir} -print 2> /dev/null) && -d "${l_mozilla_cache_dir}" ]] && rm -rf "${l_mozilla_cache_dir}"

            # https://wiki.mozilla.org/CA/AddRootToFirefox
            [[ -d /usr/lib/mozilla ]] && rm -rf /usr/lib/mozilla
            [[ -d /usr/lib64/mozilla ]] && rm -rf /usr/lib64/mozilla

            [[ -d "${installation_dir}" ]] || fnBase_ExitStatement "${software_fullname} is successfully removed from your system!"
        else
            fnBase_ExitStatement "${c_blue}Note${c_normal}: no ${software_fullname} is found in your system!"
        fi
    fi
}


#########  2-2. Latest & Local Version Check  #########
fn_EssentialLibaryCheck(){
    # 1 - libdbus-glib-1.so.2: cannot open shared object file: No such file or directory. Couldn't load XPCOM. XPCOMGlueLoad error for file /opt/MozillaFirefox/libxul.so
    # pacman|dbus-glib|/usr/lib/libdbus-glib-1.so.2

    [[ -z $(find /usr/lib* -name 'libdbus-glib-1.so.2' -print 2>/dev/null) ]] && fnBase_ExitStatement "${c_red}Fatal Error${c_normal}: ${software_fullname} need shared libraries ${c_blue}libdbus-glib-1.so.2${c_normal} (apt: libdbus-glib, pacman: dbus-glib, zypper: dbus-1-glib, yum: dbus-glib)!"
}


fn_UtilityLocalVersionDetection(){
    local l_path="${1:-}"
    local l_action=${2:-'--version'}
    local l_output=${l_output:-}

    # From version 60.0, it prompt error info:
    # Running Firefox as root in a regular user's session is not supported.  ($XAUTHORITY is /run/user/1000/gdm/Xauthority which is owned by maxdsre.)

    if [[ "${login_user}" == 'root' ]]; then
        l_output=$("${l_path}" "${l_action}" 2> /dev/null)
    else
        l_output=$(sudo -u "${login_user}" "${l_path}" "${l_action}" 2> /dev/null)
    fi
    # sed -r -n '/Firefox.*?[[:digit:]]+/{s@^[^[:digit:]]+(.*)$@\1@g;p}'
    l_output="${l_output##* }"
    echo "${l_output}"
}

fn_VersionComparasion(){
    [[ "${version_check}" -eq 0 ]] && fn_EssentialLibaryCheck

    fnBase_CentralOutputTitle "${software_fullname}"
    fnBase_OperationPhaseStatement 'Version Detection'

    # - Local version
    current_local_version=${current_local_version:-}
    if [[ -s "${installation_dir}/VERSION" ]]; then
        current_local_version=$(cut -d\| -f1 "${installation_dir}/VERSION")
    fi
    [[ -z "${current_local_version}" && -s "${firefox_bin_path}" ]] && current_local_version=$(fn_UtilityLocalVersionDetection "${firefox_bin_path}")
    [[ -z "${current_local_version}" ]] && is_existed=0
    if [[ "${is_existed}" -eq 1 ]]; then
        fnBase_OperationProcedureStatement 'Local version detecting'
        fnBase_OperationProcedureResult "${current_local_version}"
    fi

    # - Latest online version info
    fnBase_OperationProcedureStatement 'Online version detecting'
    local l_online_release_info=${l_online_release_info:-}
    # deprecated    # l_online_release_info=$($download_method "${release_note_page}" | sed -r -n '/latest-release/,/<\/div>/{s@[[:space:]]*<[^>]*>[[:space:]]*@@g;/^[[:blank:]]*$/d;/Firefox Release/d;p}' | sed ':a;N;$!ba;s@\n@|@g')
    l_online_release_info=$($download_method "${release_note_page}" | sed -r -n '/c-release-(version|date)/{s@[[:space:]]*<[^>]+>[[:space:]]*@@g;p}' | sed ':a;N;$!ba;s@\n@|@g')
    # 66.0.3|April 10, 2019
    # 60.0.2|June 6, 2018
    online_release_version="${l_online_release_info%%|*}"

    if [[ -n "${online_release_version}" ]]; then
        online_release_date=$(date +'%b %d, %Y' --date="${l_online_release_info##*|}")
        fnBase_OperationProcedureResult "${online_release_version} - ${online_release_date}"
    else
        fnBase_OperationProcedureResult '' 1
        fnBase_ExitStatement ''
    fi

    # - Just online version check
    if [[ "${version_check}" -eq 1 ]]; then
        fnBase_ExitStatement ''

        # if [[ "${is_existed}" -eq 1 ]]; then
        #     fnBase_ExitStatement "Local existed version is ${c_red}${current_local_version}${c_normal}, Latest version online is ${c_red}${online_release_version}${c_normal} (${c_blue}${online_release_date}${c_normal})."
        # else
        #     fnBase_ExitStatement "Latest version online (${c_red}${online_release_version}${c_normal}), Release date ($c_red${online_release_date}$c_normal)."
        # fi
    fi

    # - Online & Local Version Comparasion
    if [[ "${is_existed}" -eq 1 ]]; then
        if [[ "${online_release_version}" == "${current_local_version}" ]]; then
            fnBase_ExitStatement "\nLatest version (${c_red}${online_release_version}${c_normal}) has been existed in your system."
        elif [[ $(fnBase_VersionNumberComparasion "${current_local_version}" "${online_release_version}" '<') -eq 1 ]]; then
            printf "\nExisted version local (${c_red}%s${c_normal}) < Latest version online (${c_red}%s${c_normal}).\n" "${current_local_version}" "${online_release_version}"
        else
            printf "\nExisted version local (${c_red}%s${c_normal}) > Current version online (${c_red}%s${c_normal}).\n" "${current_local_version}" "${online_release_version}"
            fnBase_ExitStatement ''
        fi
    # else
    #     printf "No %s find in your system!\n" "${software_fullname}"
    fi
}


#########  2-3. Download & Decompress Latest Software  #########
fn_DownloadAndDecompressOperation(){
    local l_operation_type=${l_operation_type:-'installing'}
    [[ "${is_existed}" -eq 1 ]] && l_operation_type='updating'
    fnBase_OperationPhaseStatement "${l_operation_type^}"

    [[ "${change_language}" -eq 1 ]] && lang='zh-TW'

    local l_real_download_link=${l_real_download_link:-'https://download-installer.cdn.mozilla.net/pub/firefox/releases/'}

    local online_release_downloadlink
    # https://download-installer.cdn.mozilla.net/pub/firefox/releases/89.0/linux-x86_64/en-US/firefox-89.0.tar.bz2
    online_release_downloadlink="${l_real_download_link%/}/${online_release_version}/${os_type}/${lang}/firefox-${online_release_version}.tar.bz2"
    local online_release_pack_name="${online_release_downloadlink##*/}"

    fnBase_OperationProcedureStatement 'SHA512SUMS GPG Verifying'
    # https://download-installer.cdn.mozilla.net/pub/firefox/releases/89.0/KEY
    local online_gpg_link
    online_gpg_link="${l_real_download_link%/}/${online_release_version}/KEY"

    local gpg_save_dir
    gpg_save_dir=$(mktemp -d -t XXXXXXXX)
    $download_method "${l_real_download_link%/}/${online_release_version}/SHA512SUMS" > "${gpg_save_dir}/SHA512SUMS"
    $download_method "${l_real_download_link%/}/${online_release_version}/SHA512SUMS.asc" > "${gpg_save_dir}/SHA512SUMS.asc"

    if [[ -n $(fnBase_GPGSignatureVerification "${gpg_save_dir}/SHA512SUMS" "${gpg_save_dir}/SHA512SUMS.asc" "${online_gpg_link}" 2>&1 | sed -r -n '/Verifying signature/,${/Good signature/{p}}') ]]; then
        fnBase_OperationProcedureResult
    else
        [[ -d "${gpg_save_dir}" ]] && rm -rf "${gpg_save_dir}"
        fnBase_OperationProcedureResult '' 1
        fnBase_ExitStatement ''
    fi

    local online_release_dgst=${online_release_dgst:-}
    # https://download-installer.cdn.mozilla.net/pub/firefox/releases/89.0/SHA256SUMS{,.asc}
    # https://download-installer.cdn.mozilla.net/pub/firefox/releases/89.0/SHA512SUMS{,.asc}
    online_release_dgst=$(sed -r -n '/'"${os_type}"'.*?'"${lang}"'.*?'"${online_release_pack_name}"'/{s@^[[:space:]]*([^[:space:]]+).*$@\1@g;s@^b'\''@@;s@'\''$@@;p}' "${gpg_save_dir}/SHA512SUMS" 2> /dev/null)
    # c1d02511f202622b7a22f427d47f5ab067a0f579386b2a77923cfb24998d2491d0b87e3c26618d854e2dfdd1cef76fd5817325ea41463f5c1dd8c746525874e7  linux-x86_64/en-US/firefox-89.0.tar.bz2
    [[ -d "${gpg_save_dir}" ]] && rm -rf "${gpg_save_dir}"

    local l_is_need_download=${l_is_need_download:-1}
    local l_specified_pack_dir=${l_specified_pack_dir:-}

    # specified file path existed
    if [[ "${source_pack_path}" =~ .tar.bz2$ && -s "${source_pack_path}" ]]; then
        [[ "${source_pack_path##*/}" == "${online_release_pack_name}" ]] || fnBase_ExitStatement "Package ${c_red}${source_pack_path}${c_normal} is not same to latest release version name!"

    # specified dir or ~/Downloads existed
    elif [[ (-n "${source_pack_path}" && -d "${source_pack_path}" ) || -d "${user_download_dir}" ]]; then
        if [[ -d "${source_pack_path}" ]]; then
            l_specified_pack_dir="${source_pack_path}"
        elif [[ -d "${user_download_dir}" ]]; then
            l_specified_pack_dir="${user_download_dir}"
        fi

        l_specified_pack_dir="${l_specified_pack_dir%/}"

        if [[ -d "${l_specified_pack_dir}" && -f "${l_specified_pack_dir}/${online_release_pack_name}" ]]; then
            source_pack_path="${l_specified_pack_dir}/${online_release_pack_name}"
        fi
    fi

    # 1.1- existed package path in system
    if [[ -n "${source_pack_path}" && -f "${source_pack_path}" ]]; then
        fnBase_OperationProcedureStatement 'Existed pack dgst verifying'

        if [[ -n "${online_release_dgst}" && $(fnBase_HashDigestVerification "${source_pack_path}" '512') == "${online_release_dgst}" ]]; then
            fnBase_OperationProcedureResult "${c_red}${source_pack_path%/*}/${c_normal}"
            l_is_need_download=0
        else
            fnBase_OperationProcedureResult '' 1
            fnBase_ExitStatement ''
        fi
    fi

    # 2 - Download Directly From Official Site
    local pack_save_path=${pack_save_path:-}

    if [[ "${l_is_need_download}" -eq 1 ]]; then
        fnBase_OperationProcedureStatement "Downloading ${c_red}${online_release_pack_name}${c_normal}"
        pack_save_path="${temp_save_dir}/${online_release_pack_name}"
        [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"

        $download_method "${online_release_downloadlink}" > "${pack_save_path}"

        if [[ -s "${pack_save_path}" ]]; then
            fnBase_OperationProcedureResult
        else
            [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
            fnBase_OperationProcedureResult '' 1
        fi

        fnBase_OperationProcedureStatement "SHA512 dgst verification"
        if [[ -n "${online_release_dgst}" && $(fnBase_HashDigestVerification "${pack_save_path}" '512') == "${online_release_dgst}"  ]]; then
            fnBase_OperationProcedureResult
        else
            [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
            fnBase_OperationProcedureResult '' 1
            fnBase_ExitStatement ''
        fi

        # use variable "source_pack_path" to execute decompress operation, variable "pack_save_path" used for delete temporary download package
        source_pack_path="${pack_save_path}"
    fi

    # 3 - Decompress & Extract
    fnBase_OperationProcedureStatement 'Installing'
    local application_backup_path="${installation_dir}${bak_suffix}"
    [[ -d "${application_backup_path}" ]] && rm -rf "${application_backup_path}"

    [[ -d "${installation_dir}" ]] && mv "${installation_dir}" "${application_backup_path}"
    [[ -d "${installation_dir}" ]] || mkdir -p "${installation_dir}"
    tar xf "${source_pack_path}" -C "${installation_dir}" --strip-components=1 2> /dev/null
    chown -R root:root "${installation_dir}"
    find "${installation_dir}" -type f -name "*.so" -exec chmod 644 {} \; 2> /dev/null
    # remove hidden Firefox add-ons and plug-ins  https://support.mozilla.org/en-US/questions/1267206
    # about:support  -->  Firefox Features
    find "${installation_dir}/browser/features" -type f -name '*.xpi' -not -name 'screenshot*' -exec rm -rf {} \; 2> /dev/null

    local new_installed_version=${new_installed_version:-}
    new_installed_version=$(fn_UtilityLocalVersionDetection "${firefox_bin_path}")

    if [[ "${online_release_version}" == "${new_installed_version}" ]]; then
        [[ -f "${pixmaps_png_path}" ]] && rm -f "${pixmaps_png_path}"
        [[ -f "${application_desktop_path}" ]] && rm -f "${application_desktop_path}"
        [[ -d "${application_backup_path}" ]] && rm -rf "${application_backup_path}"
        fnBase_OperationProcedureResult "${c_red}${installation_dir%/}/${c_normal}"

        # write '66.0.3|Apr 10, 2019' into $installation_dir/VERSION
        echo "${online_release_version}|${online_release_date}" > "${installation_dir}/VERSION"

        # create soft link to directory /usr/bin
        ln -fs "${firefox_bin_path}" ${usr_bin_softlink_path}

        # copy newly downloaded pack to ~/Downloads
        local l_user_download_save_path=${l_user_download_save_path:-"${user_download_dir}/${source_pack_path##*/}"}
        if [[ -d "${user_download_dir}" && ! -f "${l_user_download_save_path}" ]]; then
            cp "${source_pack_path}" "${l_user_download_save_path}"
            chown "${login_user}" "${l_user_download_save_path}"
            chgrp -hR "${login_user}" "${l_user_download_save_path}" 2> /dev/null

            # removed old version package existed
            if [[ "${is_existed}" -eq 1 ]]; then
                local l_existed_old_pack_path=${l_existed_old_pack_path:-"${user_download_dir}/${online_release_pack_name//${online_release_version}/${current_local_version}}"}
                [[ -f "${l_existed_old_pack_path}" ]] && rm -f "${l_existed_old_pack_path}"
            fi
        fi

    else
        [[ -d "${installation_dir}" ]] && rm -rf "${installation_dir}"
        [[ "${is_existed}" -eq 1 ]] && mv "${application_backup_path}" "${installation_dir}"
        fnBase_OperationProcedureResult '' 1
    fi

    [[ -n "${pack_save_path}" && -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
}


#########  2-4. User Profiles Configuration  #########
fn_UserJSConfiguration_old(){
    local l_path="${1:-}"
    local l_key="${2:-}"
    local l_val="${3:-}"
    if [[ -f "${l_path}" && -n "${l_key}" ]]; then
        if [[ -n "${l_val}" ]]; then
            # sed -r -i '/^user_pref.*'"${l_key}"'/{s@(.*?",[[:space:]]+)[^\)]+(.*)$@\1'"${l_val}"'\2@g;}' "${l_path}" 2> /dev/null
            sed -r -i '/^[[:space:]]*\/*[[:space:]]*user_pref.*'"${l_key}"'/{s@^[[:space:]]*\/*[[:space:]]*(.*?",[[:space:]]+)[^\)]+(.*)$@\1'"${l_val}"'\2@g;}' "${l_path}" 2> /dev/null
        else
            sed -r -i '/^[[:space:]]*\/*[[:space:]]*user_pref.*'"${l_key}"'/{s@[[:space:]]*\/*[[:space:]]*(.*?",[[:space:]]+)[^\)]+(.*)$@\1""\2@g;}' "${l_path}" 2> /dev/null
        fi
    fi
}

fn_UserProfileConfiguration_old(){
    local l_firefox_bin_path="${firefox_bin_path}"
    local l_config_dir="${login_user_home}/.mozilla/firefox"
    local l_profiles_ini=${l_profiles_ini:-"${l_config_dir}/profiles.ini"}
    local l_profile_name="${login_user}"

    # for freshly firefox
    if [[ ! -f "${l_profiles_ini}" ]]; then
        fnBase_OperationProcedureStatement 'User profile generating'
        # ~/.mozilla/firefox/installs.ini  ~/.mozilla/firefox/profiles.ini
        # - generate custom profile (name same to login user name) create file profiles.ini
        # https://developer.mozilla.org/en-US/docs/Mozilla/Command_Line_Options#-CreateProfile_profile_name
        sudo -u "${login_user}" "${l_firefox_bin_path}" -CreateProfile "${l_profile_name}" 2> /dev/null
        # - find custom profile directory
        local l_user_profile_dir=''
        l_user_profile_dir=$(find "${l_config_dir}"  -type d -name "*${l_profile_name}" -print 2> /dev/null)

        if [[ -d "${l_user_profile_dir}" ]]; then
            # - generate default profile created by web browser   create file installs.ini
            nohup=$(nohup sudo -u "${login_user}" "${l_firefox_bin_path}" --headless >/dev/null 2>&1 &)
            while true; do
                nohup_pid=$(sudo ps -e -o pid,args 2> /dev/null | sed -r -n '/--headless/{/sed/d;s@^[[:space:]]*([[:digit:]]+).*$@\1@g;p;q}')
                if [[ -n "${nohup_pid}" ]]; then
                    sleep 1
                    sudo kill -9 "${nohup_pid}" 2> /dev/null
                else
                    break
                fi
            done
        fi

        # local l_installs_uuid=''
        # local l_installs_ini="${l_user_profile_dir%/*}/installs.ini"
        # [[ -f "${l_installs_ini}" ]] && l_installs_uuid=$(sed -r -n '/^\[/{s@[[:punct:]]*@@g;p}' "${l_installs_ini}") # A518266653472A7A

        # - update default profile setting in file profiles.ini
        sed -r -i '/\[Install/,/\[/{/Default=/{s@^([^=]+=).*@\1'"${l_user_profile_dir##*/}"'@g;}}' "${l_profiles_ini}" 2> /dev/null
        fnBase_OperationProcedureResult "${l_user_profile_dir}/"
    fi

    # - add custom user.js to hardening firefox
    # l_profiles_ini=$(find "${l_config_dir}" -type f -name 'profiles.ini' -print 2> /dev/null)
    if [[ -f "${l_profiles_ini}" ]]; then
        local l_default_profile_name
        l_default_profile_name=$(sed -r -n '/\[Install/,/\[/{/Default=/{s@^[^=]+=@@g;p}}' "${l_profiles_ini}")

        l_default_user_profile_dir=$(find "${l_config_dir}"  -type d -name "*${l_default_profile_name}" -print 2> /dev/null)

        if [[ -d "${l_default_user_profile_dir}" ]]; then
            fnBase_OperationProcedureStatement 'user.js updating'
            # https://github.com/pyllyukko/user.js/
            # https://github.com/ghacksuserjs/ghacks-user.js
            # https://ownyourbits.com/2018/09/08/customize-firefox-for-privacy-and-security-with-a-custom-user-js/
            # https://askubuntu.com/questions/313483/how-do-i-change-firefoxs-aboutconfig-from-a-shell-script
            local l_user_js_path="${l_default_user_profile_dir}/user.js"
            $download_method https://raw.githubusercontent.com/ghacksuserjs/ghacks-user.js/master/user.js > "${l_user_js_path}"
            if [[ -f "${l_user_js_path}" ]]; then
                chown "${login_user}" "${l_user_js_path}"
                chgrp "${login_user}" "${l_user_js_path}" 2> /dev/null
                chmod 640 "${l_user_js_path}"
            fi

            # enable location bar using search
            fn_UserJSConfiguration "${l_user_js_path}" 'keyword.enabled' 'true'

            # DNS-over-HTTPS (DoH)
            # https://support.mozilla.org/en-US/kb/firefox-dns-over-https
            # https://www.zdnet.com/article/how-to-enable-dns-over-https-doh-in-firefox/

            # DoH publicly available servers
            # https://github.com/curl/curl/wiki/DNS-over-HTTPS
            # https://kb.adguard.com/en/general/dns-providers

            # // user_pref("network.trr.mode", 0);
            # // user_pref("network.trr.bootstrapAddress", "");
            # // user_pref("network.trr.uri", "");

            # fn_UserJSConfiguration "${l_user_js_path}" 'network.trr.mode' '2'
            # fn_UserJSConfiguration "${l_user_js_path}" 'network.trr.uri' '"https://dns9.quad9.net/dns-query"'
            # fn_UserJSConfiguration "${l_user_js_path}" 'network.trr.bootstrapAddress' '"9.9.9.9"'

            fn_UserJSConfiguration "${l_user_js_path}" 'network.trr.mode' '5'

            # enable MPRIS support
            # https://superuser.com/questions/1350908/use-mpris-dbus-media-commands-within-firefox-on-linux#1549344
            # https://superuser.com/questions/948192/use-media-keys-for-soundcloud-youtube-etc-in-firefox/1547822#1547822
            # https://work.lisk.in/2020/05/06/linux-media-control.html
            fn_UserJSConfiguration "${l_user_js_path}" 'media.hardwaremediakeys.enabled' 'true'
            fn_UserJSConfiguration "${l_user_js_path}" 'dom.media.mediasession.enabled' 'true'
            fn_UserJSConfiguration "${l_user_js_path}" 'media.mediacontrol.stopcontrol.timer.ms' '86400000'  # ddefault is 60000 (60s)

            # enable/disable WebRender
            # https://www.omgubuntu.co.uk/2020/07/firefox-enable-webrender-linux
            # https://wiki.mozilla.org/Platform/GFX/Quantum_Render
            fn_UserJSConfiguration "${l_user_js_path}" 'gfx.webrender.all' 'false'

            fnBase_OperationProcedureResult "${l_user_js_path}"
        fi
    fi
}

fn_UserProfileGeneration(){
    local l_firefox_bin_path="${firefox_bin_path}"
    local l_config_dir="${login_user_home}/.mozilla/firefox"
    local l_profiles_ini=${l_profiles_ini:-"${l_config_dir}/profiles.ini"}
    local l_profile_name="${login_user}"

    local l_userjs_harden_script='https://gitlab.com/axdsop/nixnotes/raw/master/ShellScripts/Toolkits/Miscellaneous/firefox_user_js_secure_hardening.sh'
    local l_temp_save_path="/tmp/${l_userjs_harden_script##*/}"

    $download_method "${l_userjs_harden_script}" > "${l_temp_save_path}"
    
    if [[ -f "${l_temp_save_path}" ]]; then
        sudo -u "${login_user}" bash "${l_temp_save_path}" -s -p -b "${l_firefox_bin_path}" -u "${login_user}" -f "${base_function_path}"

        # old method
        # source "${l_temp_save_path}"
        # fn_UserProfileConfiguration "${l_firefox_bin_path}" "${l_config_dir}" "${l_config_dir}/profiles.ini" "${login_user}" '1'
        # function fn_UserProfileConfiguration lists in script
    fi

    [[ -f "${l_temp_save_path}" ]] && rm -f "${l_temp_save_path}"
}


#########  2-5. Desktop Configuration  #########
fn_DesktopConfiguration(){
    # single user ~/.local/share/applications /  global /usr/share/applications
    if [[ -d '/usr/share/applications' ]]; then
        fnBase_OperationProcedureStatement 'Desktop Configuration'

        local l_mozicon_path=${l_mozicon_path:-}

        if [[ -f "${installation_dir}/browser/icons/mozicon128.png" ]]; then
            l_mozicon_path="${installation_dir}/browser/icons/mozicon128.png"
        # from mozilla firefox 59.0.1
        elif [[ -f "${installation_dir}/browser/chrome/icons/default/default128.png" ]]; then
            l_mozicon_path="${installation_dir}/browser/chrome/icons/default/default128.png"
        fi

        [[ -n "${l_mozicon_path}" ]] && ln -sf "${l_mozicon_path}" "${pixmaps_png_path}"

        echo -e "[Desktop Entry]\nEncoding=UTF-8\nName=Firefox Web Browser\nGenericName[en]=Web Browser\nComment=Firefox web browser\nExec=${firefox_bin_path} %u\nIcon=${pixmaps_png_path}\nTerminal=false\nType=Application\nStartupWMClass=Firefox-bin\nMimeType=text/html;text/xml;application/xhtml+xml;application/vnd.mozilla.xul+xml;text/mml;application/x-xpinstall;x-scheme-handler/http;x-scheme-handler/https;x-scheme-handler/ftp;\nCategories=Network;WebBrowser;" > "${application_desktop_path}"

        # sed -i -r 's@application_name@'"$application_name"'@g' "${application_desktop_path}"
        # sed -i -r 's@installation_dir@'"$installation_dir"'@g' "${application_desktop_path}"
        fnBase_OperationProcedureResult "${application_desktop_path}"
    fi
}


#########  2-6. Operation Time Cost  #########
fn_TotalTimeCosting(){
    finish_time=$(date +'%s')        # End Time Of Operation
    total_time_cost=$((finish_time-start_time))   # Total Time Of Operation
    printf "\nTotal time cost is ${c_red}%s${c_normal} seconds!\n" "${total_time_cost}"
}


#########  3. Executing Process  #########
fn_InitializationCheck

fn_UninstallOperation
fn_VersionComparasion
fn_DownloadAndDecompressOperation
fn_UserProfileGeneration
fn_DesktopConfiguration
fn_TotalTimeCosting


#########  4. EXIT Singal Processing  #########
# trap "commands" EXIT # execute command when exit from shell
fn_TrapEXIT(){
    fnBase_UnsetVars
    unset lang
    unset installation_dir
    unset is_existed
    unset version_check
    unset source_pack_path
    unset change_language
    unset is_uninstall
    unset proxy_server_specify
}

trap fn_TrapEXIT EXIT

# Script End
