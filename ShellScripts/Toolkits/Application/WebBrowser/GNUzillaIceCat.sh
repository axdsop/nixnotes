#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails


# Official Site:
# - https://libreplanet.org/wiki/Group:IceCat/
# - https://www.gnu.org/software/gnuzilla/
# - http://git.savannah.gnu.org/cgit/gnuzilla.git


# Target: Automatically Install & Update GNU IceCat WebBrowser On GNU/Linux
# Developer: MaxdSre

# Change Log:
# - Oct 08, 2020 09:04 Thu ET - initialization check method update
# - Oct 08, 2019 16:16 Tue ET - change to new base functions
# - Jun 09, 2019 13:38 ET - first draft


#########  0-1. Singal Setting  #########
trap fnBase_TrapCleanUp INT QUIT

#########  0-2. Variables Setting  #########
# umask 022    # temporarily change umask value to 022
readonly official_site='https://www.gnu.org/software/gnuzilla/'
readonly download_page="https://ftp.gnu.org/gnu/gnuzilla/"
readonly software_fullname=${software_fullname:-'GNU IceCat'}
lang=${lang:-'en-US'}  #en-US, zh-TW
readonly os_type='gnulinux-x86_64'
readonly application_name=${application_name:-'GNUIceCat'}
installation_dir="/opt/${application_name}"
readonly pixmaps_png_path="/usr/share/pixmaps/${application_name}.png"
readonly application_desktop_path="/usr/share/applications/${application_name}.desktop"
is_existed=${is_existed:-1}      # Default value is 1， assume system has installed GNU IceCat

version_check=${version_check:-0}
# change_language=${change_language:-0}
source_pack_path=${source_pack_path:-}
is_uninstall=${is_uninstall:-0}
proxy_server_specify=${proxy_server_specify:-}



#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | sudo bash -s -- [options] ...

Installing / Updating GNU IceCat Web Browser On GNU/Linux!
This script requires superuser privileges (eg. root, su).

[available option]
    -h    --help, show help info
    -c    --check, check current stable release version
    -f pack_path    --specify latest release package save path (e.g. /PATH/*.tar.bz2) or dir (default is ~/Downloads/) in system
    -p [protocol:]ip:port    --proxy host (http|https|socks4|socks5), default protocol is http
    -U    --update/install base script content in '~/.axdsop/', default is no action
    -u    --uninstall, uninstall software installed
\e[0m"
# -l    --change language to 'zh-TW', default is 'en-US'
}

while getopts "hcf:lup:" option "$@"; do
    case "$option" in
        c ) version_check=1 ;;
        f ) source_pack_path="$OPTARG" ;;
        # l ) change_language=1 ;;
        u ) is_uninstall=1 ;;
        p ) proxy_server_specify="$OPTARG" ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done



#########  1-2 Preparation Operation  #########
fn_InitializationCheck(){
    local l_user_codebase_dir="$HOME/.config/axdsop"
    local l_base_function_temp_save_path=''
    local l_need_remove=0
    local l_base_function_url='https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/base_functions.sh'
    l_base_function_temp_save_path=$(find "${l_user_codebase_dir}" -type f -name "${l_base_function_url##*/}" -print 2> /dev/null)
    
    if [[ ! -f "${l_base_function_temp_save_path}" ]]; then
        local l_download_tool=${l_download_tool:-'wget -qO-'}
        [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_tool='curl -fsL'
        l_need_remove=1
        l_base_function_temp_save_path=$(mktemp -t XXXXXXX)
        ${l_download_tool} "${l_base_function_url}" > "${l_base_function_temp_save_path}"
    fi

    if [[ -f "${l_base_function_temp_save_path}" && -s "${l_base_function_temp_save_path}" ]]; then
        . "${l_base_function_temp_save_path}"
        [[ "${l_need_remove}" -eq 1 ]] && rm "${l_base_function_temp_save_path}"
    else
        echo "Sorry, fail to get base script path."
        exit
    fi

    # tput clear  # Echo the clear-screen sequence for the current terminal.
    # specify 1 means need sudo privilege
    fnBase_RunningEnvironmentCheck '1'

    fnBase_CommandExistCheckPhase 'bzip2' # CentOS/Debian/OpenSUSE: bzip2
    fnBase_CommandExistCheckPhase 'tar' # .tar.bz2
}


#########  2-1. Uninstall  #########
fn_UninstallOperation(){
    if [[ "${is_uninstall}" -eq 1 ]]; then
        if [[ -d "${installation_dir}" ]]; then
            [[ -f "${pixmaps_png_path}" ]] && rm -f "${pixmaps_png_path}"
            [[ -f "${application_desktop_path}" ]] && rm -f "${application_desktop_path}"

            [[ -d "${installation_dir}" ]] && rm -rf "${installation_dir}"
            [[ -d "${installation_dir}${bak_suffix}" ]] && rm -rf "${installation_dir}${bak_suffix}"

            local l_mozilla_config_dir="${login_user_home}/.mozilla/"
            local l_mozilla_cache_dir="${login_user_home}/.cache/mozilla/"

            [[ -d "${l_mozilla_config_dir%/}/icecat/" ]] && rm -rf "${l_mozilla_config_dir%/}/icecat/"
            [[ -d "${l_mozilla_config_dir%/}/extensions/" ]] && rm -rf "${l_mozilla_config_dir%/}/extensions/"
            [[ -d "${l_mozilla_config_dir%/}/systemextensionsdev/" ]] && rm -rf "${l_mozilla_config_dir%/}/systemextensionsdev/"

            [[ -d "${l_mozilla_cache_dir%/}/icecat/" ]] && rm -rf "${l_mozilla_cache_dir%/}/icecat/"

            # remove empty dir
            [[ -z $(find "${l_mozilla_config_dir}" -maxdepth 1 -not -path ${l_mozilla_config_dir} -print 2> /dev/null) && -d "${l_mozilla_config_dir}" ]] && rm -rf "${l_mozilla_config_dir}"
            [[ -z $(find "${l_mozilla_cache_dir}" -maxdepth 1 -not -path ${l_mozilla_cache_dir} -print 2> /dev/null) && -d "${l_mozilla_cache_dir}" ]] && rm -rf "${l_mozilla_cache_dir}"

            # https://wiki.mozilla.org/CA/AddRootToFirefox
            # [[ -d /usr/lib/mozilla ]] && rm -rf /usr/lib/mozilla
            # [[ -d /usr/lib64/mozilla ]] && rm -rf /usr/lib64/mozilla

            [[ -d "${installation_dir}" ]] || fnBase_ExitStatement "${software_fullname} is successfully removed from your system!"
        else
            fnBase_ExitStatement "${c_blue}Note${c_normal}: no ${software_fullname} is found in your system!"
        fi
    fi
}



#########  2-2. Latest & Local Version Check  #########
fn_UtilityLocalVersionDetection(){
    local l_path="${1:-}"
    local l_action=${2:-'--version'}
    local l_output=${l_output:-}

    # From version 60.0, it prompt error info:
    # Running Firefox as root in a regular user's session is not supported.  ($XAUTHORITY is /run/user/1000/gdm/Xauthority which is owned by maxdsre.)

    if [[ "${login_user}" == 'root' ]]; then
        l_output=$("${l_path}" "${l_action}" 2> /dev/null)
    else
        l_output=$(sudo -u "${login_user}" "${l_path}" "${l_action}" 2> /dev/null)
    fi
    l_output="${l_output##* }" # GNU IceCat 60.7.0
    echo "${l_output}"
}

fn_VersionComparasion(){
    fnBase_CentralOutputTitle "${software_fullname}"
    fnBase_OperationPhaseStatement 'Version Detection'

    # - Local version
    current_local_version=${current_local_version:-}
    if [[ -s "${installation_dir}/VERSION" ]]; then
        current_local_version=$(cut -d\| -f1 "${installation_dir}/VERSION")
    fi
    [[ -z "${current_local_version}" && -s "${installation_dir}/icecat" ]] && current_local_version=$(fn_UtilityLocalVersionDetection "${installation_dir}/icecat")
    [[ -z "${current_local_version}" ]] && is_existed=0
    if [[ "${is_existed}" -eq 1 ]]; then
        fnBase_OperationProcedureStatement 'Local version detecting'
        fnBase_OperationProcedureResult "${current_local_version}"
    fi

    # - Latest online version info
    fnBase_OperationProcedureStatement 'Online version detecting'
    online_release_version=$($download_method http://git.savannah.gnu.org/cgit/gnuzilla.git | sed -r -n 's@<\/[^>]+>@&\n@g;p' | sed -r -n '/\/tag\/\?h=/{s@.*href=[^>]+>v?([^<]+)<.*$@\1@g;p;q}')
    download_page_url="${download_page%/}/${online_release_version}/"

    local l_online_release_info=$($download_method "${download_page_url}" | sed -r -n '/'"${os_type}"'/{/.sig/d;s@<\/td>@&|@g;s@[[:space:]]*<[^>]+>[[:space:]]*@@g;s@^\|*@@g;p}')
    # icecat-60.7.0.en-US.gnulinux-x86_64.tar.bz2|2019-06-02 16:49|51M|&nbsp;|

    online_release_date=$(echo "${l_online_release_info}" | cut -d\| -f 2 | date -f - +"%b %d, %Y" 2>/dev/null)
    online_release_downloadlink=''

    if [[ -n "${online_release_version}" ]]; then
        online_release_downloadlink="${download_page_url}${l_online_release_info%%|*}"
        # online_release_date=$(date +'%b %d, %Y' --date="${l_online_release_info##*|}")
        fnBase_OperationProcedureResult "${online_release_version} - ${online_release_date}"
    else
        fnBase_OperationProcedureResult '' 1
        fnBase_ExitStatement ''
    fi

    # - Just online version check
    if [[ "${version_check}" -eq 1 ]]; then
        fnBase_ExitStatement ''

        # if [[ "${is_existed}" -eq 1 ]]; then
        #     fnBase_ExitStatement "Local existed version is ${c_red}${current_local_version}${c_normal}, Latest version online is ${c_red}${online_release_version}${c_normal} (${c_blue}${online_release_date}${c_normal})."
        # else
        #     fnBase_ExitStatement "Latest version online (${c_red}${online_release_version}${c_normal}), Release date ($c_red${online_release_date}$c_normal)."
        # fi
    fi

    # - Online & Local Version Comparasion
    if [[ "${is_existed}" -eq 1 ]]; then
        if [[ "${online_release_version}" == "${current_local_version}" ]]; then
            fnBase_ExitStatement "\nLatest version (${c_red}${online_release_version}${c_normal}) has been existed in your system."
        elif [[ $(fnBase_VersionNumberComparasion "${current_local_version}" "${online_release_version}" '<') -eq 1 ]]; then
            printf "\nExisted version local (${c_red}%s${c_normal}) < Latest version online (${c_red}%s${c_normal}).\n" "${current_local_version}" "${online_release_version}"
        fi
    # else
    #     printf "No %s find in your system!\n" "${software_fullname}"
    fi
}


#########  2-3. Download & Decompress Latest Software  #########
fn_GPGVerification(){
    # $? -- 0 is ok, 1 is fail
    local l_pack_path="${1:-}"
    local l_output=${l_output:-1}
    # https://lists.gnu.org/archive/html/info-gnu/2019-06/msg00001.html
    # GPG key fingerprint: 318C 679D 94F1 7700 CC84  7DE6 46A7 0073 E4E5 0D4E

    # gpg: key 46A70073E4E50D4E: public key "Ruben Rodriguez Perez <ruben@fsf.org>" imported

    # pub   rsa4096 2019-04-24 [C]
    #       318C679D94F17700CC847DE646A70073E4E50D4E
    # uid           [ unknown] Ruben Rodriguez Perez <ruben@fsf.org>
    # uid           [ unknown] Ruben Rodriguez Perez <ruben@gnu.org>
    # uid           [ unknown] Ruben Rodriguez Perez <ruben@trisquel.info>
    # sub   rsa2048 2019-04-24 [S] [expires: 2020-04-23]
    # sub   rsa2048 2019-04-24 [E] [expires: 2020-04-23]
    # sub   rsa4096 2019-04-24 [A] [expires: 2020-04-23]

    if [[ -f "${l_pack_path}" ]]; then
        local l_gpg_save_dir=$(mktemp -d -t "${mktemp_format}")
        local l_gpg_save_path="${l_gpg_save_dir}/gnuzilla-keyring.gpg"
        $download_method "https://savannah.gnu.org/project/memberlist-gpgkeys.php?group=gnuzilla&download=1" > "${l_gpg_save_path}"

        # import gpg
        local l_gpg_import_path="${l_gpg_save_dir}/gpg_import.gpg"
        gpg --no-default-keyring --keyring "${l_gpg_import_path}" --import "${l_gpg_save_path}" 2> /dev/null
        # download .sig file
        local l_sig_download_link="${online_release_downloadlink}.sig"
        local l_sig_save_path="${l_gpg_save_dir}/${online_release_downloadlink##*/}"
        $download_method "${l_sig_download_link}" > "${l_sig_save_path}"
        # verification
        [[ -n $(gpg --no-default-keyring --keyring "${l_gpg_import_path}" --verify "${l_sig_save_path}" "${l_pack_path}" 2>&1 | sed -r -n '/Good signature/{p}') ]] && l_output=0
        [[ -d "${l_gpg_save_dir}" ]] && rm -rf "${l_gpg_save_dir}"
    fi

    return "${l_output}"
}

fn_DownloadAndDecompressOperation(){
    local l_operation_type=${l_operation_type:-'installing'}
    [[ "${is_existed}" -eq 1 ]] && l_operation_type='updating'
    fnBase_OperationPhaseStatement "${l_operation_type^}"

    # [[ "${change_language}" -eq 1 ]] && lang='zh-TW'

    # https://ftp.gnu.org/gnu/gnuzilla/60.7.0/icecat-60.7.0.en-US.gnulinux-x86_64.tar.bz2
    # https://ftp.gnu.org/gnu/gnuzilla/60.7.0/icecat-60.7.0.en-US.gnulinux-x86_64.tar.bz2.sig
    local online_release_pack_name="${online_release_downloadlink##*/}"

    local l_is_need_download=${l_is_need_download:-1}
    local l_specified_pack_dir=${l_specified_pack_dir:-}

    # specified file path existed
    if [[ "${source_pack_path}" =~ .tar.bz2$ && -s "${source_pack_path}" ]]; then
        [[ "${source_pack_path##*/}" == "${online_release_pack_name}" ]] || fnBase_ExitStatement "Package ${c_red}${source_pack_path}${c_normal} is not same to latest release version name!"

    # specified dir or ~/Downloads existed
    elif [[ (-n "${source_pack_path}" && -d "${source_pack_path}" ) || -d "${user_download_dir}" ]]; then
        if [[ -d "${source_pack_path}" ]]; then
            l_specified_pack_dir="${source_pack_path}"
        elif [[ -d "${user_download_dir}" ]]; then
            l_specified_pack_dir="${user_download_dir}"
        fi

        l_specified_pack_dir="${l_specified_pack_dir%/}"

        if [[ -d "${l_specified_pack_dir}" && -f "${l_specified_pack_dir}/${online_release_pack_name}" ]]; then
            source_pack_path="${l_specified_pack_dir}/${online_release_pack_name}"
        fi
    fi

    # 1.1- existed package path in system
    if [[ -n "${source_pack_path}" && -f "${source_pack_path}" ]]; then
        fnBase_OperationProcedureStatement "Existed pack GPG verifying"

        if fn_GPGVerification "${source_pack_path}"; then
            fnBase_OperationProcedureResult "${c_red}${source_pack_path%/*}/${c_normal}"
            l_is_need_download=0
        else
            fnBase_OperationProcedureResult '' 1
        fi
    fi

    # 2 - Download Directly From Official Site
    local pack_save_path=${pack_save_path:-}

    if [[ "${l_is_need_download}" -eq 1 ]]; then
        fnBase_OperationProcedureStatement "Downloading ${c_red}${online_release_pack_name}${c_normal}"
        pack_save_path="${temp_save_dir}/${online_release_pack_name}"
        [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"

        $download_method "${online_release_downloadlink}" > "${pack_save_path}"

        if [[ -s "${pack_save_path}" ]]; then
            fnBase_OperationProcedureResult
        else
            [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
            fnBase_OperationProcedureResult '' 1
        fi

        fnBase_OperationProcedureStatement "GPG verification"

        if fn_GPGVerification "${pack_save_path}"; then
            fnBase_OperationProcedureResult '46A70073E4E50D4E'
        else
            [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
            fnBase_OperationProcedureResult '' 1
        fi

        # use variable "source_pack_path" to execute decompress operation, variable "pack_save_path" used for delete temporary download package
        source_pack_path="${pack_save_path}"
    fi

    # 3 - Decompress & Extract
    fnBase_OperationProcedureStatement 'Installing'
    local application_backup_path="${installation_dir}${bak_suffix}"
    [[ -d "${application_backup_path}" ]] && rm -rf "${application_backup_path}"

    [[ -d "${installation_dir}" ]] && mv "${installation_dir}" "${application_backup_path}"
    [[ -d "${installation_dir}" ]] || mkdir -p "${installation_dir}"
    tar xf "${source_pack_path}" -C "${installation_dir}" --strip-components=1 2> /dev/null
    chown -R root:root "${installation_dir}"

    local new_installed_version=${new_installed_version:-}
    new_installed_version=$(fn_UtilityLocalVersionDetection "${installation_dir}/icecat")

    if [[ "${online_release_version}" == "${new_installed_version}" ]]; then
        [[ -f "${pixmaps_png_path}" ]] && rm -f "${pixmaps_png_path}"
        [[ -f "${application_desktop_path}" ]] && rm -f "${application_desktop_path}"
        [[ -d "${application_backup_path}" ]] && rm -rf "${application_backup_path}"
        fnBase_OperationProcedureResult "${c_red}${installation_dir%/}/${c_normal}"

        # write '60.7.0|Jun 02, 2019' into $installation_dir/VERSION
        echo "${online_release_version}|${online_release_date}" > "${installation_dir}/VERSION"

        # copy newly downloaded pack to ~/Downloads
        local l_user_download_save_path=${l_user_download_save_path:-"${user_download_dir}/${source_pack_path##*/}"}
        if [[ -d "${user_download_dir}" && ! -f "${l_user_download_save_path}" ]]; then
            cp "${source_pack_path}" "${l_user_download_save_path}"
            chown "${login_user}" "${l_user_download_save_path}"
            chgrp -hR "${login_user}" "${l_user_download_save_path}" 2> /dev/null

            # removed old version package existed
            if [[ "${is_existed}" -eq 1 ]]; then
                local l_existed_old_pack_path=${l_existed_old_pack_path:-"${user_download_dir}/${online_release_pack_name//${online_release_version}/${current_local_version}}"}
                [[ -f "${l_existed_old_pack_path}" ]] && rm -f "${l_existed_old_pack_path}"
            fi
        fi

    else
        [[ -d "${installation_dir}" ]] && rm -rf "${installation_dir}"
        [[ "${is_existed}" -eq 1 ]] && mv "${application_backup_path}" "${installation_dir}"
        fnBase_OperationProcedureResult '' 1
    fi

    [[ -n "${pack_save_path}" && -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
}



#########  2-5. Desktop Configuration  #########
fn_DesktopConfiguration(){
    # single user ~/.local/share/applications /  global /usr/share/applications
    if [[ -d '/usr/share/applications' ]]; then
        fnBase_OperationProcedureStatement 'Desktop Configuration'

        local funcUserProfileConfigurationl_mozicon_path=${l_mozicon_path:-}

        if [[ -f "${installation_dir}/browser/icons/mozicon128.png" ]]; then
            l_mozicon_path="${installation_dir}/browser/icons/mozicon128.png"
        # from mozilla firefox 59.0.1
        elif [[ -f "${installation_dir}/browser/chrome/icons/default/default128.png" ]]; then
            l_mozicon_path="${installation_dir}/browser/chrome/icons/default/default128.png"
        fi

        [[ -n "${l_mozicon_path}" ]] && ln -sf "${l_mozicon_path}" "${pixmaps_png_path}"

        echo -e "[Desktop Entry]\nEncoding=UTF-8\nName=GNU IceCat Web Browser\nGenericName[en]=Web Browser\nComment=GNU IceCat web browser\nExec=installation_dir/icecat %u\nIcon=application_name.png\nTerminal=false\nType=Application\nStartupWMClass=IcaCat-bin\nMimeType=text/html;text/xml;application/xhtml+xml;application/vnd.mozilla.xul+xml;text/mml;application/x-xpinstall;x-scheme-handler/http;x-scheme-handler/https;x-scheme-handler/ftp;\nCategories=Network;WebBrowser;Application;" > "${application_desktop_path}"

        sed -i -r 's@application_name@'"$application_name"'@g' "${application_desktop_path}"
        sed -i -r 's@installation_dir@'"$installation_dir"'@g' "${application_desktop_path}"
        fnBase_OperationProcedureResult "${application_desktop_path}"
    fi
}


#########  2-6. Operation Time Cost  #########
fn_TotalTimeCosting(){
    finish_time=$(date +'%s')        # End Time Of Operation
    total_time_cost=$((finish_time-start_time))   # Total Time Of Operation
    printf "\nTotal time cost is ${c_red}%s${c_normal} seconds!\n" "${total_time_cost}"
}


#########  3. Executing Process  #########
fn_InitializationCheck

fn_UninstallOperation
fn_VersionComparasion
fn_DownloadAndDecompressOperation
fn_DesktopConfiguration
fn_TotalTimeCosting


#########  4. EXIT Singal Processing  #########
# trap "commands" EXIT # execute command when exit from shell
fn_TrapEXIT(){
    fnBase_UnsetVars
    unset lang
    unset installation_dir
    unset is_existed
    unset version_check
    unset source_pack_path
    # unset change_language
    unset is_uninstall
    unset proxy_server_specify
}

trap fn_TrapEXIT EXIT

# Script End
