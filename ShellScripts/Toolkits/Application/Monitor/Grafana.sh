#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails

# Official Site: https://grafana.com/
# Desc: Grafana is an open source metric analytics & visualization suite.
# Reference:
# - http://docs.grafana.org/
# - http://docs.grafana.org/installation/configuration/#admin-user

# Default Account
# - default user: admim
# - default password: admin

# Dashboard Demos
# http://play.grafana-zabbix.org
# https://play.grafana.org
# https://monitor.gitlab.net


# Target: Grafana Installation And Configuration On GNU/Linux (RHEL/CentOS/Fedora/Debian/Ubuntu/OpenSUSE and variants)
# Developer: MaxdSre

# Change Log:
# - Oct 08, 2020 08:55 Thu ET - initialization check method update
# - Oct 18, 2019 13:06 Fri ET - add service path remove in uninstall operation
# - Oct 15, 2019 12:25 Tue ET - change custom script url
# - Oct 09, 2019 12:40 Wed ET - change to new base functions
# - Dec 31, 2018 15:52 Mon ET - code reconfiguration, include base funcions from other file
# -  Oct 07, 2018 11:54 Sun +0800 - dashboard installation code optimization
# - June 14, 2018 10:05 ~ 15:11 Thu ET - code reconfiguration
# - May 16, 2018 10:39 Wed - remove dependency of command gawk
# - Jan 05, 2018 13:58 Fri +0800 - add paras setting, remove count_scalar setting
# - Dec 12, 2017 13:51 Tue +0800


#########  0-1. Singal Setting  #########
trap fnBase_TrapCleanUp INT QUIT

#########  0-2. Variables Setting  #########
# umask 022    # temporarily change umask value to 022
readonly official_site='https://grafana.com'
readonly download_page="${official_site}/grafana/download"

readonly custom_shellscript_url='https://gitlab.com/axdsop/nixnotes/raw/master/ShellScripts/Toolkits'
readonly os_check_script="${custom_shellscript_url}/GNULinux/gnuLinuxMachineInfoDetection.sh"
readonly online_service_script_sample_dir="${custom_shellscript_url}/_Configs/Application/grafana/initScript"
readonly customsize_dashboard_url="${custom_shellscript_url}//_Configs/Application/grafana/dashboard/Customsize_Dashboard.zip"

readonly software_fullname=${software_fullname:-'Grafana'}
readonly application_name=${application_name:-'grafana'}
# installation_dir=${installation_dir:-"/usr/share/${application_name}"}
installation_dir=${installation_dir:-"/opt/${software_fullname}"}
is_existed=${is_existed:-1}   # Default value is 1， assume system has installed Grafana
source_pack_path=${source_pack_path:-}
version_check=${version_check:-0}
mysql_percona_dashboard=${mysql_percona_dashboard:-0}
is_uninstall=${is_uninstall:-0}
proxy_server_specify=${proxy_server_specify:-}


#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | sudo bash -s -- [options] ...

Installing / Updating Grafana On GNU/Linux!
This script requires superuser privileges (eg. root, su).

[available option]
    -h    --help, show help info
    -c    --check, check current stable release version
    -f pack_path    --specify latest release package save path (e.g. /PATH/*.tar.gz) or dir (default is ~/Downloads/) in system
    -m    --add Percona monitor dashboard, default only add custom dashboard
    -u    --uninstall, uninstall software installed
    -p [protocol:]ip:port    --proxy host (http|https|socks4|socks5), default protocol is http
\e[0m"
}

while getopts "hcmf:up:" option "$@"; do
    case "$option" in
        c ) version_check=1 ;;
        m ) mysql_percona_dashboard=1 ;;
        f ) source_pack_path="$OPTARG" ;;
        u ) is_uninstall=1 ;;
        p ) proxy_server_specify="$OPTARG" ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done


#########  1-2 Preparation Operation  #########
fn_InitializationCheck(){
    local l_user_codebase_dir="$HOME/.config/axdsop"
    local l_base_function_temp_save_path=''
    local l_need_remove=0
    local l_base_function_url='https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/base_functions.sh'
    l_base_function_temp_save_path=$(find "${l_user_codebase_dir}" -type f -name "${l_base_function_url##*/}" -print 2> /dev/null)
    
    if [[ ! -f "${l_base_function_temp_save_path}" ]]; then
        local l_download_tool=${l_download_tool:-'wget -qO-'}
        [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_tool='curl -fsL'
        l_need_remove=1
        l_base_function_temp_save_path=$(mktemp -t XXXXXXX)
        ${l_download_tool} "${l_base_function_url}" > "${l_base_function_temp_save_path}"
    fi

    if [[ -f "${l_base_function_temp_save_path}" && -s "${l_base_function_temp_save_path}" ]]; then
        . "${l_base_function_temp_save_path}"
        [[ "${l_need_remove}" -eq 1 ]] && rm "${l_base_function_temp_save_path}"
    else
        echo "Sorry, fail to get base script path."
        exit
    fi

    # tput clear  # Echo the clear-screen sequence for the current terminal.
    # specify 1 means need sudo privilege
    fnBase_RunningEnvironmentCheck '1'

    fnBase_CommandExistCheckPhase 'gzip' # CentOS/Debian/OpenSUSE: gzip
    fnBase_CommandExistCheckPhase 'tar' # .tar.gz

    profile_path="${login_user_home}/.profile"
}


#########  2-1. Essential Variabls Setting  #########
fn_EssentialVarsConfiguration(){
    daemon_name=${daemon_name:-'grafana-server'}
    user_name=${user_name:-"${application_name}"}
    group_name=${group_name:-"${application_name}"}
    log_dir=${log_dir:-"/var/log/${application_name}"}
    data_dir=${data_dir:-"/var/lib/${application_name}"}
    config_dir=${config_dir:-"/etc/${application_name}"}
    config_path=${config_path:-"${config_dir}/${application_name}.ini"}
    run_dir=${run_dir:-"/var/run/${application_name}"}
    # /etc/sysconfig for .rpm, /etc/default for .deb
    sysconfig_dir=${sysconfig_dir:-'/etc/sysconfig'}
    [[ -d /etc/default ]] && sysconfig_dir='/etc/default'
    sysconfig_path=${sysconfig_path:-"${sysconfig_dir}/${daemon_name}"}
}


#########  2-1. Uninstall Operation  #########
fn_UninstallOperation(){
    if [[ "${is_uninstall}" -eq 1 ]]; then
        if fnBase_CommandExistIfCheck 'grafana-server'; then
            fnBase_SystemServiceManagement "${daemon_name}" 'stop'

            l_service_script_path="/etc/systemd/system/${daemon_name}.service"
            [[ -f "${l_service_script_path}" ]] || l_service_script_path="/etc/init.d/${daemon_name}"
            [[ -f "${l_service_script_path}" ]] && rm -f "${l_service_script_path}"
            fnBase_CommandExistIfCheck 'systemctl' && systemctl daemon-reload 2> /dev/null

            [[ -f /usr/sbin/grafana-server ]] && rm -f /usr/sbin/grafana-server
            [[ -f /usr/sbin/grafana-cli ]] && rm -f /usr/sbin/grafana-cli

            [[ -f "${sysconfig_path}" ]] && rm -f "${sysconfig_path}"

            [[ -d "${installation_dir}" ]] && rm -rf "${installation_dir}"
            [[ -d "${config_dir}" ]] && rm -rf "${config_dir}"
            [[ -d "${data_dir}" ]] && rm -rf "${data_dir}"
            [[ -d "${log_dir}" ]] && rm -rf "${log_dir}"

            # - remove user and group - prometheus
            if [[ -n $(sed -r -n '/^'"${user_name}"':/{p}' /etc/passwd) ]]; then
                userdel -fr "${user_name}" 2> /dev/null
                groupdel -f "${group_name}" 2> /dev/null
            fi

            fnBase_ExitStatement "${software_fullname} is successfully removed from your system!"
        else
            fnBase_ExitStatement "${c_blue}Note${c_normal}: no ${software_fullname} is found in your system!"
        fi
    fi
}


#########  2-2. Latest & Local Version Check  #########
fn_VersionComparasion(){
    fnBase_CentralOutputTitle "${software_fullname}"
    fnBase_OperationPhaseStatement 'Version Detection'

    # - Local version
    current_local_version=${current_local_version:-}
    if fnBase_CommandExistIfCheck 'grafana-server'; then
        if [[ -f '/usr/sbin/grafana-server' && -s '/usr/sbin/grafana-server' ]]; then
            current_local_version=$(/usr/sbin/grafana-server -v 2>&1 | sed -r -n 's@^[^[:digit:]]+([^[:space:]]+).*@\1@g;p')
        else
            current_local_version=$(grafana-server -v 2>&1 | sed -r -n 's@^[^[:digit:]]+([^[:space:]]+).*@\1@g;p')
        fi
    fi

    [[ -z "${current_local_version}" ]] && is_existed=0
    if [[ "${is_existed}" -eq 1 ]]; then
        fnBase_OperationProcedureStatement 'Local version detecting'
        fnBase_OperationProcedureResult "${current_local_version}"
    fi

    # - Latest online version info
    fnBase_OperationProcedureStatement 'Online version detecting'
    download_page_html=$(mktemp -t "${mktemp_format}")
    $download_method "${download_page}" | sed 's@<\/div>@\n@g;s@},@}\n@g' > "${download_page_html}"

    # github api latest release may be not the latest release sometimes
    # https://developer.github.com/v3/repos/releases/#get-the-latest-release
    # local l_release_online_info=${l_release_online_info:-}
    # l_release_online_info=$($download_method https://api.github.com/repos/grafana/grafana/releases/latest | sed -r -n '/"(name|published_at)"/{s@^[^:]*:[[:space:]]*@@g;s@,@@g;s@"@@g;s@v([[:digit:]]*)@\1@g;s@T[[:digit:]]+.*$@@g;p}' | sed ':a;N;$!ba;s@\n@|@g')
    # 5.1.3|2018-05-16

    l_release_online_info=$(sed -r -n '/"releaseDate"/{s@.*version":"([^"]+).*?releaseDate":"([^"]+).*$@\1|\2@g;p;q}' "${download_page_html}")
    # 5.2.0|2018-06-26T22:00:00.000Z

    online_release_version=${online_release_version:-}
    online_release_version="${l_release_online_info%%|*}"
    online_release_date=${online_release_date:-}
    # online_release_date="${l_release_online_info##*|}"
    # online_release_date="${l_release_online_info%%T*}"
    online_release_date=$(echo "${l_release_online_info##*|}" | date +"%b %d, %Y" -f - 2> /dev/null)

    if [[ -n "${online_release_version}" ]]; then
        fnBase_OperationProcedureResult "${online_release_version} - ${online_release_date}"
    else
        fnBase_OperationProcedureResult '' 1
    fi

    # - Just online version check
    if [[ "${version_check}" -eq 1 ]]; then
        fnBase_ExitStatement ''
    fi

    # - Online & Local Version Comparasion
    if [[ "${is_existed}" -eq 1 ]]; then
        if [[ "${online_release_version}" == "${current_local_version}" ]]; then
            fnBase_ExitStatement "\nLatest version (${c_red}${online_release_version}${c_normal}) has been existed in your system."
        elif [[ $(fnBase_VersionNumberComparasion "${current_local_version}" "${online_release_version}" '<') -eq 1 ]]; then
            printf "\nExisted version local (${c_red}%s${c_normal}) < Latest version online (${c_red}%s${c_normal}).\n" "${current_local_version}" "${online_release_version}"
        fi
    # else
    #     printf "No %s find in your system!\n" "${software_fullname}"
    fi
}


#########  2-3. Download & Decompress Latest Software  #########
fn_DownloadAndDecompressOperation(){
    local l_operation_type=${l_operation_type:-'installing'}
    [[ "${is_existed}" -eq 1 ]] && l_operation_type='updating'
    fnBase_OperationPhaseStatement "${l_operation_type^}"

    local online_release_pack_info=${online_release_pack_info:-}
    online_release_pack_info=$(sed -r -n '/linux-(x64|amd64).tar.*sha256/{s@\\u002F@/@g;s@\\n@@g;s@.*url":"([^"]+).*?sha256":"([^"]+).*$@\1|\2@g;p}' "${download_page_html}")
    # https://s3-us-west-2.amazonaws.com/grafana-releases/release/grafana-5.2.0.linux-amd64.tar.gz|b2ec1674f6c830b2529e92a1a422aae84bf32a77905fc2e238864053396c2947

    [[ -z "${online_release_pack_info}" ]] && fnBase_ExitStatement "${c_red}Sorry${c_normal}: fail to get package download link!"

    local online_release_downloadlink="${online_release_pack_info%%|*}"
    local online_release_dgst="${online_release_pack_info##*|}"
    local online_release_pack_name="${online_release_downloadlink##*/}"

    local l_is_need_download=${l_is_need_download:-1}
    local l_specified_pack_dir=${l_specified_pack_dir:-}

    # specified file path existed
    if [[ "${source_pack_path}" =~ .tar.gz$ && -s "${source_pack_path}" ]]; then
        [[ "${source_pack_path##*/}" == "${online_release_pack_name}" ]] || fnBase_ExitStatement "Package ${c_red}${source_pack_path}${c_normal} is not same to latest release version name!"

    # specified dir or ~/Downloads existed
    elif [[ (-n "${source_pack_path}" && -d "${source_pack_path}" ) || -d "${user_download_dir}" ]]; then
        if [[ -d "${source_pack_path}" ]]; then
            l_specified_pack_dir="${source_pack_path}"
        elif [[ -d "${user_download_dir}" ]]; then
            l_specified_pack_dir="${user_download_dir}"
        fi

        l_specified_pack_dir="${l_specified_pack_dir%/}"

        if [[ -d "${l_specified_pack_dir}" && -f "${l_specified_pack_dir}/${online_release_pack_name}" ]]; then
            source_pack_path="${l_specified_pack_dir}/${online_release_pack_name}"
        fi
    fi

    # 1.1- existed package path in system
    if [[ -n "${source_pack_path}" && -f "${source_pack_path}" ]]; then
        fnBase_OperationProcedureStatement 'Existed pack dgst verifying'

        if [[ -n "${online_release_dgst}" && $(fnBase_HashDigestVerification "${source_pack_path}" '256') == "${online_release_dgst}" ]]; then
            fnBase_OperationProcedureResult "${c_red}${source_pack_path%/*}/${c_normal}"
            l_is_need_download=0
        else
            fnBase_OperationProcedureResult '' 1
        fi
    fi

    # 2 - Download Directly From Official Site
    local pack_save_path=${pack_save_path:-}

    if [[ "${l_is_need_download}" -eq 1 ]]; then
        fnBase_OperationProcedureStatement "Downloading ${c_red}${online_release_pack_name}${c_normal}"
        pack_save_path="${temp_save_dir}/${online_release_pack_name}"
        [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"

        $download_method "${online_release_downloadlink}" > "${pack_save_path}"

        if [[ -s "${pack_save_path}" ]]; then
            fnBase_OperationProcedureResult
        else
            [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
            fnBase_OperationProcedureResult '' 1
        fi

        fnBase_OperationProcedureStatement "SHA256 dgst verification"
        if [[ -n "${online_release_dgst}" && $(fnBase_HashDigestVerification "${pack_save_path}" '256') == "${online_release_dgst}"  ]]; then
            fnBase_OperationProcedureResult
        else
            [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
            fnBase_OperationProcedureResult '' 1
        fi

        # use variable "source_pack_path" to execute decompress operation, variable "pack_save_path" used for delete temporary download package
        source_pack_path="${pack_save_path}"
    fi

    # 3 - Decompress & Compiling
    fnBase_OperationProcedureStatement 'Decompressing'
    local application_backup_path="${installation_dir}${bak_suffix}"
    [[ -d "${application_backup_path}" ]] && rm -rf "${application_backup_path}"

    [[ -d "${installation_dir}" ]] && mv "${installation_dir}" "${application_backup_path}"
    [[ -d "${installation_dir}" ]] || mkdir -p "${installation_dir}"

    tar xf "${source_pack_path}" -C "${installation_dir}" --strip-components=1 2> /dev/null

    local new_installed_version=${new_installed_version:-}
    [[ -f "${installation_dir}/bin/grafana-server" ]] && new_installed_version=$("${installation_dir}/bin/grafana-server" -v 2>&1 | sed -r -n 's@^[^[:digit:]]+([^[:space:]]+).*@\1@g;p')

    if [[ "${online_release_version}" == "${new_installed_version}" ]]; then
        [[ -d "${application_backup_path}" ]] && rm -rf "${application_backup_path}"
        fnBase_OperationProcedureResult "${c_red}${installation_dir%/}/${c_normal}"

        # copy newly downloaded pack to ~/Downloads
        local l_user_download_save_path=${l_user_download_save_path:-"${user_download_dir}/${source_pack_path##*/}"}
        if [[ -d "${user_download_dir}" && ! -f "${l_user_download_save_path}" ]]; then
            cp "${source_pack_path}" "${l_user_download_save_path}"
            chown "${login_user}" "${l_user_download_save_path}"

            # removed old version package existed
            if [[ "${is_existed}" -eq 1 ]]; then
                local l_existed_old_pack_path=${l_existed_old_pack_path:-"${user_download_dir}/${online_release_pack_name//${online_release_version}/${current_local_version}}"}
                [[ -f "${l_existed_old_pack_path}" ]] && rm -f "${l_existed_old_pack_path}"
            fi
        fi
    else
        [[ -d "${installation_dir}" ]] && rm -rf "${installation_dir}"
        [[ "${is_existed}" -eq 1 ]] && mv "${application_backup_path}" "${installation_dir}"
        fnBase_OperationProcedureResult '' 1
    fi

    [[ -n "${pack_save_path}" && -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
}


#########  2-4. Postinstallation Configuration  #########
fn_NormalUserOperation(){
    if [[ -z $(sed -r -n '/^'"${user_name}"':/{p}' /etc/passwd)  ]]; then
        fnBase_OperationProcedureStatement 'Create user & group'
        # create group
        groupadd -r "${group_name}" 2> /dev/null
        # create user without login privilege
        # -M, --no-create-home   do not create the user's home directory
        # -d, --home-dir HOME_DIR    home directory of the new account
        useradd -r -g "${group_name}" -s /bin/false -d "${installation_dir}" -c "${software_fullname}" "${user_name}" 2> /dev/null
        fnBase_OperationProcedureResult "${user_name}:${group_name}"
    fi
}

fn_DirPermissionConfiguration(){
    local l_dir="${1:-}"
    local l_mode=${2:-755}
    local l_user=${3:-"${user_name}"}
    local l_group=${4:-"${group_name}"}

    [[ -n "${l_dir}" && ! -d "${l_dir}" ]] && mkdir -p "${l_dir}"
    if [[ -d "${l_dir}" ]]; then
        chmod "${l_mode}" "${l_dir}"
        chown -R "${l_user}":"${l_group}" "${l_dir}"
    fi
}

fn_PostInstallationConfiguration(){
    fnBase_OperationPhaseStatement "Postinstallation configuration"

    fn_NormalUserOperation

    chown -R "${user_name}":"${group_name}" "${installation_dir}"

    if [[ -f "${installation_dir}/bin/grafana-server" ]]; then
        find "${installation_dir}"/ -type d -exec chmod 750 {} \;
        find "${installation_dir}"/ -type f -exec chmod 640 {} \;

        fn_DirPermissionConfiguration "${log_dir}"
        fn_DirPermissionConfiguration "${data_dir}"
        fn_DirPermissionConfiguration "${data_dir}/plugins"
        fn_DirPermissionConfiguration "${config_dir}"

        # - ./bin/{grafana-server,grafana-cli} --> /usr/sbin
        if [[ -f "${installation_dir}/bin/grafana-server" ]]; then
            fnBase_OperationProcedureStatement 'grafana-server path'
            mv -f "${installation_dir}/bin/grafana-server" /usr/sbin/
            chown root:root /usr/sbin/grafana-server
            chmod 755 /usr/sbin/grafana-server
            fnBase_OperationProcedureResult '/usr/sbin/grafana-server'
        fi

        if [[ -f "${installation_dir}/bin/grafana-cli" ]]; then
            fnBase_OperationProcedureStatement 'grafana-cli path'
            mv -f "${installation_dir}/bin/grafana-cli" /usr/sbin/
            chown root:root /usr/sbin/grafana-cli
            chmod 755 /usr/sbin/grafana-cli
            fnBase_OperationProcedureResult '/usr/sbin/grafana-cli'
        fi

        [[ -d "${installation_dir}/bin" ]] && rm -rf "${installation_dir}/bin"

        # - ./conf --> /etc/grafana
        if [[ -d "${installation_dir}/conf" && ! -f "${config_path}" ]]; then
            fnBase_OperationProcedureStatement 'Config path'
            cp -R "${installation_dir}"/conf/* "${config_dir}"
            chown -R "${user_name}":"${group_name}" "${config_dir}"
            [[ -f "${config_dir}/defaults.ini" ]] && mv "${config_dir}/defaults.ini" "${config_dir}/defaults.ini${bak_suffix}"
            [[ -f "${config_dir}/sample.ini" ]] && mv "${config_dir}/sample.ini" "${config_path}"
            fnBase_OperationProcedureResult "${config_path}"
        fi

        # sysconfig file
        fnBase_OperationProcedureStatement 'Sysconfig file'
        echo -e "user_name=${user_name}\nGRAFANA_GROUP=${group_name}\ninstallation_dir=${installation_dir}\nLOG_DIR=${log_dir}\nDATA_DIR=${data_dir}\nMAX_OPEN_FILES=10000\nCONF_DIR=${config_dir}\nCONF_FILE=${config_path}\nRESTART_ON_UPGRADE=true\nPLUGINS_DIR=${data_dir}/plugins\nPROVISIONING_CFG_DIR=${config_dir}/provisioning\n# Only used on systemd systems\nPID_FILE_DIR=${run_dir}" > "${sysconfig_path}"
        fnBase_OperationProcedureResult "${sysconfig_path}"
    fi
}

#########  2-5. Percona Dashboard  #########
# Note: Grafana 5.0 has no `[dashboards.json]` in grafana.ini
# https://community.grafana.com/t/release-notes-v5-0-x/5250
fn_SpecDashboardInstallation(){
    local l_name="${1:-}"
    local l_link="${2:-}"

    if [[ -n "${l_name}" && -n "${l_link}" && -d "${data_dir}" ]]; then
        fnBase_OperationProcedureStatement "${l_name} dashboard"
        local dashboard_datadir="${data_dir}/dashboards/${l_name,,}"
        [[ -d "${dashboard_datadir%/*}" ]] || mkdir "${dashboard_datadir%/*}"

        local pack_save_path="${temp_save_dir}/${l_link##*/}"
        case "${l_name,,}" in
            percona ) pack_save_path="${temp_save_dir}/grafana-dashboards-master.zip" ;;
        esac
        $download_method "${l_link}" > "${pack_save_path}"

        if [[ -s "${pack_save_path}" ]]; then
            unzip -q -d "${temp_save_dir}" "${pack_save_path}"
            [[ -d "${dashboard_datadir}" ]] && rm -rf "${dashboard_datadir}"
            mkdir "${dashboard_datadir}"
            cp -R "${pack_save_path%.*}/dashboards"/* "${dashboard_datadir}"
            chown -R "${user_name}":"${group_name}" "${dashboard_datadir%/*}"
            chmod 750 "${dashboard_datadir}"
            chmod 640 "${dashboard_datadir}"/*.json

            # need to change "datasource" from variable "${DS_PROMETHEUS}" to value Prometheus, unless mannually import it
            [[ "${l_name,,}" == 'maxdsre' ]] && sed -r -i '/"datasource"/{s@\$\{DS_PROMETHEUS\}@Prometheus@g}' "${dashboard_datadir}"/*.json

            local l_provisioning_conf_dir=${l_provisioning_conf_dir:-'/etc/grafana/provisioning/dashboards'}
            if [[ -d "${l_provisioning_conf_dir}" ]]; then
                echo -e "apiVersion: 1\n\nproviders:\n - name: '"${l_name}"'\n   orgId: 1\n   folder: ''\n   type: file\n   options:\n     path: ${data_dir}/dashboards/${l_name,,}\n" > "${l_provisioning_conf_dir}/${l_name,,}.yaml"
            fi
        fi

        [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
        [[ -d "${pack_save_path%.*}" ]] && rm -rf ${pack_save_path%.*}
        fnBase_OperationProcedureResult "${data_dir}/dashboards/${l_name,,}"
    fi
}

fn_DashboardInstallation(){
    fnBase_OperationPhaseStatement "Dashboards"

    # - deprecated
    # [dashboards.json]
    # enabled = true
    # path = /var/lib/grafana/dashboards
    # local config_path="${config_dir}/grafana.ini"
    # [[ -s "${config_path}" ]] && sed -r -i '/^\[dashboards.json\]$/,/^$/{s@^;?enabled.*$@enabled = true@g;s@;?(path.*)$@\1@g;}' "${config_path}"

    # - Custom Dashboard
    fn_SpecDashboardInstallation 'MaxdSre' "${customsize_dashboard_url}"

    # - Percona Dashboard
    # https://github.com/percona/grafana-dashboards

    # {
    #   "status": "error",
    #   "errorType": "bad_data",
    #   "error": "parse error at char 84: unknown function with name \"count_scalar\"",
    #   "message": "parse error at char 84: unknown function with name \"count_scalar\""
    # }

    # https://github.com/prometheus/prometheus/releases
    # Prometheus has removed count_scalar from v 2.0.0
    # https://github.com/percona/grafana-dashboards/pull/71
    # https://github.com/percona/grafana-dashboards/commit/38443776ebd6f13b1000b1ec3025fb39bc014c3c

    [[ "${mysql_percona_dashboard}" -eq 1 ]] && fn_SpecDashboardInstallation 'Percona' 'https://github.com/percona/grafana-dashboards/archive/master.zip'
}

#########  2-6. Config Directive Configuration  #########
fn_DirectiveSetting(){
    local l_section=${1:-}
    local l_item=${2:-}
    local l_val=${3:-}
    local l_action="${4:-0}"
    local l_path=${l_path:-"${config_path}"}
    if [[ -n "${l_section}" && -n "${l_item}" && -n "${l_val}" && -s "${l_path}" ]]; then
        [[ "${l_action}" -eq 0 ]] && fnBase_OperationProcedureStatement "[${l_section}] - ${l_item}"
        if [[ "${l_val}" =~ @ ]]; then
            sed -r -i '/\['"${l_section}"'\]/,/\[/{/^[[:space:]]*;?[[:space:]]*'"${l_item}"'[[:space:]]*=/{s/^[[:space:]]*;?[[:space:]]*([^=]+=).*/\1 '"${l_val}"'/g;}}' "${l_path}"
        else
            sed -r -i '/\['"${l_section}"'\]/,/\[/{/^[[:space:]]*;?[[:space:]]*'"${l_item}"'[[:space:]]*=/{s@^[[:space:]]*;?[[:space:]]*([^=]+=).*@\1 '"${l_val}"'@g;}}' "${l_path}"
        fi
        [[ "${l_action}" -eq 0 ]] && fnBase_OperationProcedureResult "${l_val}"
    fi
}

fn_ParametersConfiguration(){
    # http://docs.grafana.org/installation/configuration/
    if [[ -s "${config_path}" ]]; then
        fnBase_OperationPhaseStatement "${config_path##*/} configuration"

        # - Paths
        # - Server
        # - Database
        # - Session
        # fn_DirectiveSetting 'session' 'provider' 'file'
        # - Data proxy
        # - Analytics
        fn_DirectiveSetting 'analytics' 'reporting_enabled' 'false'
        fn_DirectiveSetting 'analytics' 'check_for_updates' 'true'
        # - Security
        # - Users
        # disable user signup / registration
        fn_DirectiveSetting 'users' 'allow_sign_up' 'false'
        # - Anonymous Auth
        # disable anonymous access
        fn_DirectiveSetting 'auth.anonymous' 'enabled' 'false'
        # - Github Auth
        # - Google Auth
        # - Generic OAuth
        # - Grafana.com Auth
        # - Auth Proxy
        # - Basic Auth
        # - Auth LDAP
        # - SMTP / Emailing
        # - Logging
        fn_DirectiveSetting 'log' 'level' 'warn'
        # - AMQP Event Publisher
        # - Alerting
        # - Internal Grafana Metrics
        # - Distributed tracing
        # - Grafana.com integration
        # - External image storage
    fi
}

#########  2-7. Service Script  #########
fn_ServiceScriptConfiguration(){
    fnBase_OperationPhaseStatement 'Service Script'
    fnBase_OperationProcedureStatement 'Service Script'
    # - init / service script
    local l_service_script_link=''
    local l_service_script_dir=''
    local l_service_script_path=''

    if fnBase_CommandExistIfCheck 'systemctl'; then
        l_service_script_dir='/etc/systemd/system'
        l_service_script_path="${l_service_script_dir}/${daemon_name}.service"

        [[ -f "${l_service_script_path}" ]] && fnBase_SystemServiceManagement "${l_service_script_path##*/}" 'stop'

        echo -e "[Unit]\nDescription=Grafana instance\nDocumentation=http://docs.grafana.org\nWants=network-online.target\nAfter=network-online.target\nAfter=postgresql.service mariadb.service mysql.service\n\n[Service]\nEnvironmentFile=${sysconfig_path}\nUser=${user_name}\nGroup=${group_name}\nType=notify\n# Type=simple\nRestart=on-failure\nWorkingDirectory=${installation_dir}\nRuntimeDirectory=${application_name}\nRuntimeDirectoryMode=0750\nExecStart=/usr/sbin/grafana-server --config=\${CONF_FILE} --pidfile=\${PID_FILE_DIR}/grafana-server.pid cfg:default.paths.logs=\${LOG_DIR} cfg:default.paths.data=\${DATA_DIR} cfg:default.paths.plugins=\${PLUGINS_DIR} cfg:default.paths.provisioning=\${PROVISIONING_CFG_DIR}\n\nLimitNOFILE=10000\nTimeoutStopSec=20\nUMask=0027\n\n[Install]\nWantedBy=multi-user.target" > "${l_service_script_path}"

        chmod 644 "${l_service_script_path}"
        systemctl daemon-reload 2> /dev/null
    else
        l_service_script_dir='/etc/init.d'
        l_service_script_path="${l_service_script_dir}/${daemon_name}"
        l_service_script_link="${online_service_script_sample_dir}/${daemon_name}.init"

        [[ -f "${l_service_script_path}" ]] && fnBase_SystemServiceManagement "${l_service_script_path##*/}" 'stop'

        $download_method "${l_service_script_link}" > "${l_service_script_path}"

        sed -r -i '/Configuraton Start/,/Configuraton End/{s@^(USER=).*$@\1'"${user_name}"'@g;s@^(GROUP=).*$@\1'"${group_name}"'@g;s@^(GRAFANA_HOME=).*$@\1'"${installation_dir}"'@g;s@^(CONF_DIR=).*$@\1'"${config_dir}"'@g;s@^(DATA_DIR=).*$@\1'"${data_dir}"'@g;s@^(LOG_DIR=).*$@\1'"${log_dir}"'@g;}' "${l_service_script_path}"
        chmod 755 "${l_service_script_path}"
    fi

    fnBase_SystemServiceManagement "${l_service_script_path##*/}" 'enable'
    fnBase_OperationProcedureResult "${l_service_script_path}"
}


#########  2-5. Operation Time Cost  #########
fn_TotalTimeCosting(){
    finish_time=$(date +'%s')        # End Time Of Operation
    total_time_cost=$((finish_time-start_time))   # Total Time Of Operation
    printf "\nTotal time cost is ${c_red}%s${c_normal} seconds!\n" "${total_time_cost}"
    echo -e "\nYou can visit ${c_yellow}http://localhost:3000${c_normal} in your browser with default user ${c_yellow}admin${c_normal} and password ${c_yellow}admin${c_normal}.\n"
}



#########  3. Executing Process  #########
fn_InitializationCheck

fn_EssentialVarsConfiguration
fn_UninstallOperation
fn_VersionComparasion
fn_DownloadAndDecompressOperation
fn_PostInstallationConfiguration
fn_DashboardInstallation
fn_ParametersConfiguration
fn_ServiceScriptConfiguration
fn_TotalTimeCosting



#########  4. EXIT Singal Processing  #########
# trap "commands" EXIT # execute command when exit from shell
fn_TrapEXIT(){
    fnBase_UnsetVars
    unset installation_dir
    unset is_existed
    unset version_check
    unset mysql_percona_dashboard
    unset source_pack_path
    unset is_uninstall
    unset daemon_name
    unset user_name
    unset group_name
    unset log_dir
    unset data_dir
    unset config_dir
    unset config_path
    unset run_dir
    unset sysconfig_dir
    unset sysconfig_path
}

trap fn_TrapEXIT EXIT


# - Problem Occurs
# Grafana HTTP Error Bad Gateway and Templating init failed errors
# http://stack-overflow.gq/questions/48338122/grafana-http-error-bad-gateway-and-templating-init-failed-errors#answer-48349643
# Solution: Data Sources --> HTTP --> Access --> change from `direct` to `proxy`

# Script End
