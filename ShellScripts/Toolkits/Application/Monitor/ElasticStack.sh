#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails


# Official Site: https://www.elastic.co
# Documentation
# - https://www.elastic.co/guide/en/elastic-stack/current/installing-elastic-stack.html

# Target: Elastic Stack Installation And Configuration On GNU/Linux
# Developer: MaxdSre

# Change Log:
# - Oct 08, 2020 08:54 Thu ET - initialization check method update
# - Oct 15, 2019 12:24 Tue ET - change custom script url
# - Oct 09, 2019 14:02 Wed ET - change to new base functions
# - Mar 26, 2019 09:28 ET - html page update, fix release info extraction function
# - Dec 30, 2018 19:59 ~ 21:33 Sun ET - code reconfiguration, include base funcions from other file
# - Jul 18, 2018 09:18 Wed ET - add version check
# - Jul 07, 2018 15:56 Sat ET - add Filebeat
# - May 25, 2018 13:15 Fri ET - code reconfiguration
# - Jan 11, 2018 15:10 Wed +0800 - add logstash, combined adjusting, cost 3 days
# - Jan 04, 2018 11:35 Thu +0800 - add kibana
# - Jan 03, 2018 18:50 Wed +0800 - just elasticsearch


# Plugin
# - IK Analysis for Elasticsearch   https://github.com/medcl/elasticsearch-analysis-ik

# Attention:
# java.lang.RuntimeException: can not run elasticsearch as root
# https://discuss.elastic.co/search?q=java.lang.RuntimeException%3A%20can%20not%20run%20elasticsearch%20as%20root

# ELK doesn't support JDK version > 8
# Java HotSpot(TM) 64-Bit Server VM warning: Option UseConcMarkSweepGC was deprecated in version 9.0 and will likely be removed in a future release.

# Detected a 6.x and above cluster: the `type` event field won't be used to determine the document _type {:es_version=>6}

# X-Pack  In versions 6.3 and later, X-Pack is included with the default distributions of Elastic Stack, with all free features enabled by default.


# ElasticSearch Test: curl -XGET 'localhost:9200/?pretty'
# Kibana Dashboard: http://127.0.0.1:5601
# Logstash pipeline config dir is /etc/elastic/logstash/conf.d/

# Elastic Search delete index
# https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-delete-index.html
# curl -XDELETE 'localhost:9200/twitter?pretty'
# curl -XGET 'localhost:9200/twitter?pretty'

# https://www.elastic.co/guide/en/logstash/current/config-examples.html

# sudo systemctl daemon-reload
# sudo systemctl start elastic_elasticsearch.service
# sudo systemctl start elastic_kibana.service
# sudo systemctl start elastic_logstash.service
# sudo systemctl start elastic_filebeat.service


#########  0-1. Singal Setting  #########
trap fnBase_TrapCleanUp INT QUIT

#########  0-2. Variables Setting  #########
# umask 022    # temporarily change umask value to 022
readonly official_site='https://www.elastic.co'
readonly download_page="${official_site}/downloads"
readonly custom_shellscript_url='https://gitlab.com/axdsop/nixnotes/raw/master/ShellScripts/Toolkits'
readonly online_service_script_sample_dir="${custom_shellscript_url}/_Configs/Application/elastic/initScript"

stack_name=${stack_name:-'elastic'}
software_fullname=${software_fullname:-'Elastic Stack'}
# installation_dir=${installation_dir:-"/opt/${stack_name}"}
installation_dir=${installation_dir:-"/opt/${software_fullname// /}"}
config_dir=${config_dir:-"/etc/${stack_name}"}
log_dir=${log_dir:-"/var/log/${stack_name}"}
run_dir=${run_dir:-"/var/run/${stack_name}"}
readonly data_dir_default="/var/lib/${stack_name}"
data_dir=${data_dir:-"${data_dir_default}"}

readonly elasticsearch_name='elasticsearch'
readonly logstash_name='logstash'
readonly kibana_name='kibana'
readonly filebeat_name='filebeat'

version_check=${version_check:-0}
is_uninstall=${is_uninstall:-0}
proxy_server_specify=${proxy_server_specify:-}

strict_mode_enable=${strict_mode_enable:-0}
source_pack_path=${source_pack_path:-}
elasticsearch_install=${elasticsearch_install:-0}
logstash_install=${logstash_install:-0}
kibana_install=${kibana_install:-0}
beats_install=${beats_install:-0}



#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | sudo bash -s -- [options] ...

Elastic Stack Installation And Configuration On GNU Linux!
This script requires superuser privileges (eg. root, su).

[available option]
    -h    --help, show help info
    -c    --check, check stack online release version
    -d data_dir    --set data dir, default is /var/lib/elastic
    -f pack_path    --specify latest release package save dir (default is ~/Downloads/) in system
    -e    --install elasticsearch, default is not install
    -k    --install kibana, default is not install
    -l    --install logstash, default is not install
    -b    --install filebeat, default is not install
    -r    --strict mode (delete config dir, data dir, user elastic)
    -u    --uninstall, uninstall software installed
    -p [protocol:]ip:port    --proxy host (http|https|socks4|socks5), default protocol is http
\e[0m"
}

while getopts "hcd:f:eklburp:" option "$@"; do
    case "$option" in
        c ) version_check=1 ;;
        d ) data_dir="$OPTARG" ;;
        f ) source_pack_path="$OPTARG" ;;
        e ) elasticsearch_install=1 ;;
        k ) kibana_install=1 ;;
        l ) logstash_install=1 ;;
        b ) beats_install=1 ;;
        u ) is_uninstall=1 ;;
        r ) strict_mode_enable=1 ;;
        p ) proxy_server_specify="$OPTARG" ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done


#########  1-2 Preparation Operation  #########
fn_InitializationCheck(){
    local l_user_codebase_dir="$HOME/.config/axdsop"
    local l_base_function_temp_save_path=''
    local l_need_remove=0
    local l_base_function_url='https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/base_functions.sh'
    l_base_function_temp_save_path=$(find "${l_user_codebase_dir}" -type f -name "${l_base_function_url##*/}" -print 2> /dev/null)
    
    if [[ ! -f "${l_base_function_temp_save_path}" ]]; then
        local l_download_tool=${l_download_tool:-'wget -qO-'}
        [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_tool='curl -fsL'
        l_need_remove=1
        l_base_function_temp_save_path=$(mktemp -t XXXXXXX)
        ${l_download_tool} "${l_base_function_url}" > "${l_base_function_temp_save_path}"
    fi

    if [[ -f "${l_base_function_temp_save_path}" && -s "${l_base_function_temp_save_path}" ]]; then
        . "${l_base_function_temp_save_path}"
        [[ "${l_need_remove}" -eq 1 ]] && rm "${l_base_function_temp_save_path}"
    else
        echo "Sorry, fail to get base script path."
        exit
    fi

    # tput clear  # Echo the clear-screen sequence for the current terminal.
    # specify 1 means need sudo privilege
    fnBase_RunningEnvironmentCheck '1'

    fnBase_CommandExistCheckPhase 'gzip' # CentOS/Debian/OpenSUSE: gzip
    fnBase_CommandExistCheckPhase 'tar' # .tar.gz

    # - Java environment
    if fnBase_CommandExistIfCheck 'java'; then
        JAVA_HOME=$(readlink -f /usr/bin/java | sed -r 's@(.*)/bin.*@\1@g')
        export JAVA_HOME
        [[ -z "${JAVA_HOME:-}" ]] && fnBase_ExitStatement "${c_red}Error${c_normal}, No environment variable ${c_blue}JAVA_HOME${c_normal} found!"
    else
        fnBase_ExitStatement "${c_red}Error${c_normal}, No ${c_blue}java${c_normal} command found!"
    fi

    ip_local=${ip_local:-'127.0.0.1'}
    fnBase_CommandExistIfCheck 'ip' && ip_local=$(ip route get 1 | sed -r -n '1{s@.*src[[:space:]]*([^[:space:]]+).*$@\1@g;p}')

    if fnBase_CommandExistIfCheck 'python3'; then
        python_name='python3'
    elif fnBase_CommandExistIfCheck 'python'; then
        python_name='python'
    fi
}


#########  2-1. Uninstall Operation  #########
fn_UninstallOperation(){
    if [[ "${is_uninstall}" -eq 1 ]]; then

        if [[ -d "${installation_dir}" ]]; then
            local l_service_script_dir='/etc/init.d'
            fnBase_CommandExistIfCheck 'systemctl' && l_service_script_dir='/etc/systemd/system'

            # - stop daemon running
            find "${l_service_script_dir}"/ -type f -name "${stack_name}*" -print | while IFS="" read -r line; do
                if [[ -n "${line}" ]]; then
                    service_name="${line##*/}"
                    service_name="${service_name%%.*}"
                    fnBase_SystemServiceManagement "${service_name}" 'stop'
                    # - remove system init script   SysV init / Systemd
                    [[ -f "${line}" ]] && rm -f "${line}"
                    unset service_name
                fi
            done

            # - remove data dir
            if [[ -s "${config_dir}/${elasticsearch_name}/${elasticsearch_name}.yml" ]]; then
                local elasticsearch_data_dir
                elasticsearch_data_dir=$(sed -r -n '/^path.data:/{s@.*:[[:space:]]([^[:space:]]+).*$@\1@g;p}' "${config_dir}/${elasticsearch_name}/${elasticsearch_name}".yml)
                [[ -n "${elasticsearch_data_dir}" && -d "${elasticsearch_data_dir}" ]] && rm -rf "${elasticsearch_data_dir}"
            fi

            if [[ "${strict_mode_enable}" -eq 1 ]]; then
                [[ -d "${config_dir}" ]] && rm -rf "${config_dir}"
                [[ -d "${data_dir}" ]] && rm -rf "${data_dir}"

                # - remove user and group - elastic
                if [[ -n $(sed -r -n '/^'"${stack_name}"':/{p}' /etc/passwd) ]]; then
                    userdel -fr "${stack_name}" 2> /dev/null
                    groupdel -f "${stack_name}" 2> /dev/null
                fi
            fi

            [[ -d "${log_dir}" ]] && rm -rf "${log_dir}"
            [[ -d "${run_dir}" ]] && rm -rf "${run_dir}"
            [[ -d "${installation_dir}" ]] && rm -rf "${installation_dir}"

            fnBase_CommandExistIfCheck 'systemctl' && systemctl daemon-reload 2> /dev/null
            fnBase_ExitStatement "Elastic stack is successfully removed from your system!"
        else
            fnBase_ExitStatement "${c_blue}Note${c_normal}: no ${stack_name} is found in your system!"
        fi
    fi
}


#########  2-2. Prerequisites  #########
fn_OnlineReleaseVersionDetails(){
    local l_stack_name="${1:-}"
    local l_product_link=''
    local l_output=''

    case "${l_stack_name}" in
        "${elasticsearch_name}" )
            # - elasticsearch 9200 9300
            l_product_link="${download_page}/elasticsearch"
            # Elasticsearch|6.5.4|2018-12-19T09:00:00-0800|https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-6.5.4.tar.gz|https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-6.5.4.tar.gz.sha512
        ;;
        "${logstash_name}" )
            # - logstash
            l_product_link="${download_page}/logstash"
            # Logstash|6.5.4|2018-12-19T09:00:00-0800|https://artifacts.elastic.co/downloads/logstash/logstash-6.5.4.tar.gz|https://artifacts.elastic.co/downloads/logstash/logstash-6.5.4.tar.gz.sha512
        ;;
        "${kibana_name}" )
            # - kibana 5601
            l_product_link="${download_page}/kibana"
            # Kibana|6.5.4|2018-12-19T09:00:00-0800|https://artifacts.elastic.co/downloads/kibana/kibana-6.5.4-linux-x86_64.tar.gz|https://artifacts.elastic.co/downloads/kibana/kibana-6.5.4-linux-x86_64.tar.gz.sha512
        ;;
        "${filebeat_name}" )
            # - filebeat
            l_product_link="${download_page}/beats/filebeat"
            # Filebeat|6.5.4|2018-12-19T09:00:00-0800|https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-6.5.4-linux-x86_64.tar.gz|https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-6.5.4-linux-x86_64.tar.gz.sha512
        ;;
    esac

    # $download_method "${download_page}"/elasticsearch | sed -r -n '/Version:/{n;s@[^>]+>([^<]+)<.*@\1@g;p};/Release date:/{n;s@[^>]+>([^<]+)<.*@\1@g;p};/.tar.gz/{/data[[:space:]]*=/d;s@.*href="([^"]+)".*$@\1@g;p};' | sed ':a;N;$!ba;s@\n@|@g' | sed 's@.*@'"${l_stack_name}"'|&@g;')
    # awk -F\| -v item='elasticsearch' 'BEGIN{OFS="|"}{"date --date=\""$2"\" +\"%F\"" | getline a; $2=a; print item,$0}'

    # sed ':a;N;$!ba;s@\n@\\n@g'

    # extract json format data from javascript function `var data =` in html page
    # l_output=$($download_method "${l_product_link}" 2> /dev/null | sed -r -n '/data[[:space:]]*=/{s@^[^=]+=[[:space:]]*@@g;p}' | ${python_name} -c $'import json,sys\nobj=json.load(sys.stdin)\nfor item in obj:\n    if item["product"]["subtitle"] == "GA Release":\n        url_list = [[k["url"],k["hash_url"]] for k in item["product_version"]["package"] if ("Linux" in k["title"] and "32" not in k["title"]) or ("TAR.GZ" in k["title"])]\n        print("{}|{}|{}|{}|{}".format(item["product"]["title"],item["product_version"]["version_number"],item["product_version"]["release_date"],url_list[0][0],url_list[0][1]))')

    l_output=$($download_method "${l_product_link}" 2> /dev/null | sed -r -n '/__NEXT_DATA__/{s@.*__NEXT_DATA__[^\{]+(.*?);__NEXT_LOADED_PAGES_.*$@\1@g;p}' | ${python_name} -c $'import json,sys\nobj=json.load(sys.stdin)\npageProps=obj["props"]["pageProps"]\nuid = [ i["uid"] for i in pageProps["entry"][0][0]["product"] if i["subtitle"] == "GA Release" ][0]\nfor item in pageProps["productVersions"][0]:\n    if item["product"][0] == uid:\n        url_list = [[k["url"],k["hash_url"]] for k in item["package"] if ("Linux" in k["title"] and "32" not in k["title"]) or ("TAR.GZ" in k["title"])]  \n        print("{}|{}|{}|{}|{}".format(item["title"],item["version_number"],item["date"],url_list[0][0],url_list[0][1]))\n        break')

    echo "${l_output}"
}


fn_ParaSpecifiedValVerification(){
    # - verify data dir
    fnBase_OperationProcedureStatement 'Data dir verification'
    if [[ -n "${data_dir}" ]]; then
        data_dir="${data_dir%/}"
        if [[ "${data_dir}" == "${data_dir_default}" || "${data_dir}" =~ ^/ ]]; then
            fnBase_OperationProcedureResult "${data_dir}"
        else
            fnBase_OperationProcedureResult "${data_dir}" 1
        fi
    fi
}

fn_NormalUserOperation(){
    if [[ -z $(sed -r -n '/^'"${stack_name}"':/{p}' /etc/passwd) ]]; then
        fnBase_OperationProcedureStatement 'Create user & group'
        # create group
        groupadd -r "${stack_name}" 2> /dev/null
        # create user without login privilege
        useradd -r -g "${stack_name}" -s /bin/false -d "${installation_dir}" -c "${software_fullname}" "${stack_name}" 2> /dev/null
        local l_optional_group=${l_optional_group:-'root'}
        [[ -n $(sed -r -n '/^adm:/{s@^([^:]+).*@\1@g;p}' /etc/gshadow 2> /dev/null) ]] && l_optional_group='adm'
        usermod -a -G "${l_optional_group}" "${stack_name}" 2> /dev/null

        user_name="${stack_name}"
        group_name="${stack_name}"
        fnBase_OperationProcedureResult "${user_name}:${group_name}"
    else
        user_name="${stack_name}"
        group_name="${stack_name}"
    fi
}

fn_Prerequisites(){
    fnBase_CentralOutputTitle "${software_fullname}"

    if [[ "${version_check}" -ne 1 ]]; then
        fnBase_OperationPhaseStatement 'Preparation'
        fn_ParaSpecifiedValVerification
        fn_NormalUserOperation
    fi
}


#########  2-3. Download & Decompress Latest Software  #########
fn_DirPermissionConfiguration(){
    local l_dir="${1:-}"
    local l_mode=${2:-755}
    local l_user=${3:-"${user_name}"}
    local l_group=${4:-"${group_name}"}

    [[ -n "${l_dir}" && ! -d "${l_dir}" ]] && mkdir -p "${l_dir}"
    if [[ -d "${l_dir}" ]]; then
        chmod "${l_mode}" "${l_dir}"
        chown -R "${l_user}":"${l_group}" "${l_dir}"
    fi
}

fn_ElasticDirectiveSetting(){
    local l_item="${1:-}"
    local l_val="${2:-}"
    local l_path="${3:-}"
    local l_action="${4:-0}"
    if [[ -n "${l_item}" && -n "${l_val}" && -s "${l_path}" ]]; then
        [[ "${l_action}" -eq 0 ]] && fnBase_OperationProcedureStatement "${l_item}"
        if [[ -n $(sed -r -n '/^[[:space:]]*#?[[:space:]]*'"${l_item}"'[[:space:]]*:/{p}' "${l_path}") ]]; then
            sed -r -i '/^[[:space:]]*#?[[:space:]]*'"${l_item}"'[[:space:]]*:/{s@^#?[[:space:]]*([^:]+:).*$@\1 '"${l_val}"'@g}' "${l_path}" 2> /dev/null
        else
            sed -r -i '$a '"${l_item}: ${l_val}"'' "${l_path}" 2> /dev/null
        fi
        [[ "${l_action}" -eq 0 ]] && fnBase_OperationProcedureResult "${l_val//\"}"
    fi
}

fn_FilebeatDirectiveSetting(){
    local l_section="${1:-}"
    local l_item="${2:-}"
    local l_val="${3:-}"
    local l_path="${4:-}"
    local l_action="${5:-0}"
    if [[ -n "${l_section}" && -n "${l_item}" && -n "${l_val}" && -s "${l_path}" ]]; then
        [[ "${l_action}" -eq 0 ]] && fnBase_OperationProcedureStatement "${l_section} - ${l_item}"
        if [[ -n $(sed -r -n '/^'"${l_section}"':/,/=+/{/^[[:space:]]*#?'"${l_item}"':[[:space:]]+/{p}}' "${l_path}") ]]; then
            case "${l_val}" in
                c|comment )
                    sed -r -i '/^'"${l_section}"':/,/=+/{/'"${l_item}"':[[:space:]]+/{s@^([[:space:]]*)#?(.*)$@\1\2@g;}}' "${l_path}"
                    l_val=$(sed -r -n '/^'"${l_section}"':/,/=+/{/'"${l_item}"':[[:space:]]+/{s@^[[:space:]]*#?[^:]+:[[:space:]]*(.*)$@\1@g;p}}' "${l_path}")
                    ;;
                * )
                    sed -r -i '/^'"${l_section}"':/,/=+/{/'"${l_item}"':[[:space:]]+/{s@^([[:space:]]*)#?([^:]+:).*@\1\2 '"${l_val}"'@g;}}' "${l_path}"
                    ;;
            esac
        fi

        [[ "${l_action}" -eq 0 ]] && fnBase_OperationProcedureResult "${l_val//\"}"
    fi
}

fn_ConfigDirectiveOptimization(){
    local l_product_name="${1:-}"
    local l_config_path="${config_dir}/${l_product_name}/${l_product_name}.yml"

    if [[ -f "${l_config_path}" ]]; then
        fnBase_OperationPhaseStatement "${l_config_path##*/}"

        local l_log_dir="${log_dir}/${l_product_name}"
        local l_data_dir="${data_dir}/${l_product_name}"
        fn_DirPermissionConfiguration "${l_config_path%/*}"
        fn_DirPermissionConfiguration "${l_log_dir}"
        fn_DirPermissionConfiguration "${l_data_dir}"

        [[ -f "${l_config_path}" && ! -f "${l_config_path}${bak_suffix}" ]] && cp "${l_config_path}" "${l_config_path}${bak_suffix}"
        chown "${user_name}":"${group_name}" "${l_config_path}${bak_suffix}"

        case "${l_product_name,,}" in
            "${elasticsearch_name}" )
                # - path
                fn_ElasticDirectiveSetting 'path.data' "${l_data_dir}" "${l_config_path}"
                fn_ElasticDirectiveSetting 'path.logs' "${l_log_dir}" "${l_config_path}"

                # - name
                # A node can only join a cluster when it shares its cluster.name with all the other nodes in the cluster. The default name is elasticsearch.
                local l_cluster_name=${l_cluster_name:-'elastic-elk'}
                fn_ElasticDirectiveSetting 'cluster.name' "${l_cluster_name}" "${l_config_path}"
                local l_node_name=${l_node_name:-"node-e-${ip_local//.}"}
                fn_ElasticDirectiveSetting 'node.name' "${l_node_name}" "${l_config_path}"

                # - network
                local l_network_host='127.0.0.1'
                fn_ElasticDirectiveSetting 'network.host' "${l_network_host}" "${l_config_path}"
                local l_http_port='9200-9299'
                fn_ElasticDirectiveSetting 'http.port' "${l_http_port}" "${l_config_path}"
                local l_transport_tcp_port='9300-9399'
                fn_ElasticDirectiveSetting 'transport.tcp.port' "${l_transport_tcp_port}" "${l_config_path}"

                # - memory
                local memory_lock_boolen='true'
                fn_ElasticDirectiveSetting 'bootstrap.memory_lock' "${memory_lock_boolen}" "${l_config_path}"

                # - discovery setting
                # Pass an initial list of hosts to perform discovery when new node is started:
                # The default list of hosts is ["127.0.0.1", "[::1]"]
                #discovery.zen.ping.unicast.hosts: ["host1", "host2"]
                # Prevent the "split brain" by configuring the majority of nodes (total number of master-eligible nodes / 2 + 1):
                #discovery.zen.minimum_master_nodes: 2

                # - gateway setting
                # Block initial recovery after a full cluster restart until N nodes are started:
                #gateway.recover_after_nodes: 3

                # - various Setting
                # Require explicit names when deleting indices:
                #action.destructive_requires_name: true
                ;;
            "${kibana_name}" )
                # https://www.elastic.co/guide/en/kibana/current/settings.html

                # - server.port: 5601
                fn_ElasticDirectiveSetting 'server.port' '5601' "${l_config_path}"
                # - server.host: "localhost"
                fn_ElasticDirectiveSetting 'server.host' "\"localhost\"" "${l_config_path}"
                # - server.basePath: ""
                # - server.maxPayloadBytes: 1048576
                fn_ElasticDirectiveSetting 'server.maxPayloadBytes' '1048576' "${l_config_path}"
                # - server.name: "your-hostname"
                fn_ElasticDirectiveSetting 'server.name' "\"${stack_name}_${kibana_name}_${ip_local//.}\"" "${l_config_path}"
                # - elasticsearch.url: "http://localhost:9200"
                fn_ElasticDirectiveSetting 'elasticsearch.url' "\"http://localhost:9200\"" "${l_config_path}"
                # - elasticsearch.preserveHost   default true, use server.host setting.
                fn_ElasticDirectiveSetting 'elasticsearch.preserveHost' 'true' "${l_config_path}"
                # - kibana.index: ".kibana"
                # fn_ElasticDirectiveSetting 'kibana.indext' "\".kibana\"" "${l_config_path}"

                # - kibana.defaultAppId: "home"
                fn_ElasticDirectiveSetting 'kibana.defaultAppId' "\"discover\"" "${l_config_path}"
                # - elasticsearch.username: "user"
                # - elasticsearch.password: "pass"
                # - server.ssl.enabled: false
                # - server.ssl.certificate: /path/to/your/server.crt
                # - server.ssl.key: /path/to/your/server.key
                # - elasticsearch.ssl.certificate: /path/to/your/client.crt
                # - elasticsearch.ssl.key: /path/to/your/client.key
                # - elasticsearch.ssl.certificateAuthorities: [ "/path/to/your/CA.pem" ]
                # - elasticsearch.ssl.verificationMode: full
                # - elasticsearch.pingTimeout: 1500
                # - elasticsearch.requestTimeout: 30000
                # - elasticsearch.requestHeadersWhitelist: [ authorization ]
                # - elasticsearch.customHeaders: {}
                # - elasticsearch.shardTimeout: 0
                # - elasticsearch.startupTimeout: 5000
                # - pid.file: /var/run/kibana.pid
                fn_ElasticDirectiveSetting 'pid.file' "${run_dir}/${kibana_name}.pid" "${l_config_path}"

                # - logging.dest: stdout
                # - logging.silent: false
                # - logging.quiet: false
                # - logging.verbose: false
                # - ops.interval: 5000
                # - i18n.defaultLocale: "en"
                ;;
            "${logstash_name}" )
                # https://www.elastic.co/guide/en/logstash/current/performance-troubleshooting.html
                # https://www.elastic.co/guide/en/logstash/current/tuning-logstash.html
                # https://www.elastic.co/guide/en/logstash/current/logstash-settings-file.html

                # - node.name: test
                fn_ElasticDirectiveSetting 'node.name' "node-l-${ip_local//.}" "${l_config_path}"
                # - path.data
                fn_ElasticDirectiveSetting 'path.data' "${l_data_dir}" "${l_config_path}"
                # ☆ - path.config  very important, must setting correctly
                # Where to fetch the pipeline configuration for the main pipeline
                # path.config: /etc/logstash/conf.d/*.conf
                fn_ElasticDirectiveSetting 'path.config' "${l_config_path%/*}/conf.d/*.conf" "${l_config_path}"
                # - path.logs
                fn_ElasticDirectiveSetting 'path.logs' "${l_log_dir}" "${l_config_path}"
                # - log.level: info
                #   * fatal / error / warn / info / debug / trace
                fn_ElasticDirectiveSetting 'log.level' 'warn' "${l_config_path}"

                # - http.host: "127.0.0.1"
                fn_ElasticDirectiveSetting 'http.host' "\"127.0.0.1\"" "${l_config_path}"
                # - http.port: 9600-9700
                fn_ElasticDirectiveSetting 'http.port' '9600-9700' "${l_config_path}"

                # config.test_and_exit: false
                fn_ElasticDirectiveSetting 'config.test_and_exit' 'false' "${l_config_path}" '1'
                # config.reload.automatic: false
                fn_ElasticDirectiveSetting 'config.reload.automatic' 'true' "${l_config_path}" '1'
                # config.reload.interval: 3s
                fn_ElasticDirectiveSetting 'config.reload.interval' '30s' "${l_config_path}" '1'
                # config.support_escapes: false
                fn_ElasticDirectiveSetting 'config.support_escapes' 'true' "${l_config_path}" '1'

                # pipeline.workers: 2
                fn_ElasticDirectiveSetting 'pipeline.workers' '2' "${l_config_path}" '1'
                # pipeline.output.workers: 1
                fn_ElasticDirectiveSetting 'pipeline.output.workers' '1' "${l_config_path}" '1'
                # pipeline.batch.size: 125
                fn_ElasticDirectiveSetting 'pipeline.batch.size' '150' "${l_config_path}" '1'
                # pipeline.batch.delay: 5   milliseconds
                fn_ElasticDirectiveSetting 'pipeline.batch.delay' '20' "${l_config_path}" '1'
                # pipeline.unsafe_shutdown: false

                # Internal queuing model, "memory" for legacy in-memory based queuing and "persisted" for disk-based acked queueing. Defaults is memory
                # queue.type: memory
                fn_ElasticDirectiveSetting 'queue.type' 'persisted' "${l_config_path}" '1'

                local l_enable_persist=0
                [[ -n $(sed -r -n '/^queue.type:[[:space:]]*persisted$/{p}' "${l_config_path}") ]] && l_enable_persist=1

                # - Only for queue.type: persisted  start
                if [[ "${l_enable_persist}" -eq 1 ]]; then
                    # path.queue:   Default is path.data/queue
                    # queue.page_capacity: 250mb     Default is 250mb
                    fn_ElasticDirectiveSetting 'queue.page_capacity' '250mb' "${l_config_path}" '1'
                    # queue.max_events: 0            Default is 0 (unlimited)
                    fn_ElasticDirectiveSetting 'queue.max_events' '120' "${l_config_path}" '1'
                    # queue.max_bytes: 1024mb        Default is 1024mb or 1gb
                    fn_ElasticDirectiveSetting 'queue.max_bytes' '1024mb' "${l_config_path}" '1'
                    # queue.checkpoint.acks: 1024   Default is 1024, 0 for unlimited
                    fn_ElasticDirectiveSetting 'queue.checkpoint.acks' '1024' "${l_config_path}" '1'
                    # queue.checkpoint.writes: 1024  Default is 1024, 0 for unlimited
                    fn_ElasticDirectiveSetting 'queue.checkpoint.writes' '1024' "${l_config_path}" '1'
                    # queue.checkpoint.interval: 1000  Default is 1000, 0 for no periodic checkpoint.  milliseconds
                    fn_ElasticDirectiveSetting 'queue.checkpoint.interval' '1200' "${l_config_path}" '1'
                fi
                # - Only for queue.type: persisted end

                # dead_letter_queue.enable: false
                # - Only for dead_letter_queue.enable: true  start
                # dead_letter_queue.max_bytes: 1024mb
                # path.dead_letter_queue:   Default is path.data/dead_letter_queue
                # - Only for dead_letter_queue.enable: true  end
                ;;
            "${filebeat_name}" )
                # https://www.elastic.co/guide/en/beats/filebeat/current/configuring-howto-filebeat.html

                # - Filebeat inputs
                fn_FilebeatDirectiveSetting 'filebeat.inputs' 'enabled' 'true' "${l_config_path}"
                # exclude_lines: ['^DBG']
                # fn_FilebeatDirectiveSetting 'filebeat.inputs' 'exclude_lines' 'c' "${l_config_path}" '1'
                # include_lines: ['^ERR', '^WARN']
                # fn_FilebeatDirectiveSetting 'filebeat.inputs' 'include_lines' 'c' "${l_config_path}" '1'
                # exclude_files: ['.gz$']
                # fn_FilebeatDirectiveSetting 'filebeat.inputs' 'exclude_files' 'c' "${l_config_path}" '1'

                # multiline.pattern: ^\[
                fn_FilebeatDirectiveSetting 'filebeat.inputs' 'multiline.pattern' 'c' "${l_config_path}"

                # multiline.negate: false
                # Defines if the pattern set under pattern should be negated or not. Default is false.
                fn_FilebeatDirectiveSetting 'filebeat.inputs' 'multiline.negate' 'true' "${l_config_path}"

                # multiline.match: after
                # Match can be set to "after" or "before". It is used to define if lines should be append to a pattern that was (not) matched before or after or as long as a pattern is not matched based on negate.
                # Note: After is the equivalent to previous and before is the equivalent to to next in Logstash
                fn_FilebeatDirectiveSetting 'filebeat.inputs' 'multiline.match' 'after' "${l_config_path}"

                # - Filebeat modules
                # path: ${path.config}/modules.d/*.yml
                # fn_FilebeatDirectiveSetting 'filebeat.config.modules' 'path' 'c' "${l_config_path}" '1'

                # reload.enabled: false
                fn_FilebeatDirectiveSetting 'filebeat.config.modules' 'reload.enabled' 'true' "${l_config_path}"

                # reload.period: 10s    Period on which files under path should be checked for changes
                fn_FilebeatDirectiveSetting 'filebeat.config.modules' 'reload.period' '15s' "${l_config_path}"

                # - Elasticsearch template setting
                # fn_FilebeatDirectiveSetting 'setup.template.settings' 'index.number_of_shards' '3' "${l_config_path}"
                fn_FilebeatDirectiveSetting 'setup.template.settings' 'index.number_of_shards' '5' "${l_config_path}"

                # index.codec: best_compression
                fn_FilebeatDirectiveSetting 'setup.template.settings' 'index.codec' 'best_compression' "${l_config_path}"

                # _source.enabled: false
                fn_FilebeatDirectiveSetting 'setup.template.settings' '_source.enabled' 'true' "${l_config_path}"

                # - General
                #name:
                #tags: ["service-X", "web-tier"]

                # - Dashboards
                # setup.dashboards.enabled: false
                fn_ElasticDirectiveSetting 'setup.dashboards.enabled' 'true' "${l_config_path}"
                # setup.dashboards.url:

                # - Kibana
                fn_FilebeatDirectiveSetting 'setup.kibana' 'host' 'c' "${l_config_path}"

                # - Elastic Cloud
                #cloud.id:

                #cloud.auth:
                # The cloud.auth setting overwrites the `output.elasticsearch.username` and `output.elasticsearch.password` settings. The format is `<user>:<pass>`.

                # - Outputs
                # - 1 - Elasticsearch output
                # hosts: ["localhost:9200"]
                # fn_FilebeatDirectiveSetting 'output.elasticsearch' 'hosts' 'c' "${l_config_path}"

                #protocol: "https"
                #username: "elastic"
                #password: "changeme"

                # - 2 - Logstash output
                #hosts: ["localhost:5044"]
                # fn_FilebeatDirectiveSetting 'output.logstash' 'hosts' 'c' "${l_config_path}"

                #ssl.certificate_authorities: ["/etc/pki/root/ca.pem"]
                # Optional SSL. By default is off.
                # List of root certificates for HTTPS server verifications

                #ssl.certificate: "/etc/pki/client/cert.pem"
                # Certificate for SSL client authentication

                #ssl.key: "/etc/pki/client/cert.key"
                # Client Certificate Key

                # - Logging
                #logging.level: debug
                # Available log levels are: error, warning, info, debug
                fn_ElasticDirectiveSetting 'logging.level' 'warning' "${l_config_path}"

                #logging.selectors: ["*"]
                # At debug level, you can selectively enable logging only for some components.
                # To enable all selectors use ["*"]. Examples of other selectors are "beat",  "publish", "service".

                # - Xpack Monitoring
                #xpack.monitoring.enabled: false
                # fn_ElasticDirectiveSetting 'xpack.monitoring.enabled' 'true' "${l_config_path}"

                #xpack.monitoring.elasticsearch:
                ;;
        esac
    fi
}

fn_LocalVersionDetection(){
    local l_service="${1:-}"
    local l_dir=${l_dir:-"${installation_dir}"}
    local l_output=''
    [[ -n "${l_service}" ]] && l_service="${l_service,,}"

    if [[ -n "${l_service}" && -d "${l_dir}/${l_service}" ]]; then
        local l_path=${l_path:-"${l_dir}/${l_service}/bin/${l_service}"}
        [[ -f "${l_path}" ]] || l_path="${l_dir}/${l_service}/${l_service}"
        if [[ -n "${l_path}" && -f "${l_path}" ]]; then
            l_output=$("${l_path}" --version 2> /dev/null | sed -r 's@^[^[:digit:]]+([[:digit:].]+),?.*@\1@g;')
            [[ -n "${l_output}" ]] || l_output=$("${l_path}" version 2> /dev/null | sed -r 's@^[^[:digit:]]+([[:digit:].]+),?.*@\1@g;')
        fi
    fi
    echo "${l_output}"
}

fn_CommonOperationProcedure(){
    local l_product_name="${1:-}"

    fnBase_CentralOutputTitle "Stack - ${l_product_name}" '1'
    fnBase_OperationPhaseStatement "${l_product_name^} Version"

    # - Local version
    local l_is_existed=${l_is_existed:-0}
    current_local_version=${current_local_version:-}
    if [[ -d "${installation_dir}/${l_product_name}" ]]; then
        current_local_version=$(fn_LocalVersionDetection "${l_product_name}")
        if [[ -n "${current_local_version}" ]]; then
            l_is_existed=1
            fnBase_OperationProcedureStatement 'Local version detecting'
            fnBase_OperationProcedureResult "${current_local_version}"
        fi
    fi

    # - Latest online version info
    fnBase_OperationProcedureStatement 'Online version detecting'
    local l_release_info=${l_release_info:-}
    # l_release_info=$(sed -r -n '/^'"${l_product_name,,}"'\|/p' "${online_packs_info}")
    l_release_info=$(fn_OnlineReleaseVersionDetails "${l_product_name}")

    local online_release_version
    online_release_version=$(echo "${l_release_info}" | cut -d\| -f 2)
    local online_release_date
    online_release_date=$(echo "${l_release_info}" | cut -d\| -f 3 | date +"%b %d, %Y" -f - 2> /dev/null)

    if [[ -n "${online_release_version}" ]]; then
        fnBase_OperationProcedureResult "${online_release_version} - ${online_release_date}"
    else
        fnBase_OperationProcedureResult '' 1
    fi

    # - Download & Decompress Latest Software
    if [[ "${version_check}" -ne 1 && -n "${online_release_version}" && "${current_local_version}" != "${online_release_version}" ]]; then
        local l_operation_type=${l_operation_type:-'installing'}
        [[ "${l_is_existed}" -eq 1 ]] && l_operation_type='updating'
        fnBase_OperationPhaseStatement "${l_operation_type} operation"

        local online_release_downloadlink
        online_release_downloadlink=$(echo "${l_release_info}" | cut -d\| -f 4)
        local online_release_dgst
        online_release_dgst=$($download_method "${l_release_info##*|}" 2> /dev/null | sed -r -n 's@^[[:space:]]*([^[:space:]]+).*$@\1@g;p')
        local online_release_pack_name
        online_release_pack_name="${online_release_downloadlink##*/}"

        local l_is_need_download=${l_is_need_download:-1}
        local l_specified_pack_dir=${l_specified_pack_dir:-}

        # specified dir or ~/Downloads existed
        if [[ (-n "${source_pack_path}" && -d "${source_pack_path}" ) || -d "${user_download_dir}" ]]; then
            if [[ -d "${source_pack_path}" ]]; then
                l_specified_pack_dir="${source_pack_path}"
            elif [[ -d "${user_download_dir}" ]]; then
                l_specified_pack_dir="${user_download_dir}"
            fi

            l_specified_pack_dir="${l_specified_pack_dir%/}"

            if [[ -d "${l_specified_pack_dir}" && -f "${l_specified_pack_dir}/${online_release_pack_name}" ]]; then
                source_pack_path="${l_specified_pack_dir}/${online_release_pack_name}"
            fi
        fi

        # 1.1- existed package path in system
        if [[ -n "${source_pack_path}" && -f "${source_pack_path}" ]]; then
            fnBase_OperationProcedureStatement 'Existed pack dgst verifying'

            if [[ -n "${online_release_dgst}" && $(fnBase_HashDigestVerification "${source_pack_path}" '512') == "${online_release_dgst}" ]]; then
                fnBase_OperationProcedureResult "${c_red}${source_pack_path%/*}/${c_normal}"
                l_is_need_download=0
            else
                fnBase_OperationProcedureResult '' 1
            fi
        fi

        # 2 - Download Directly From Official Site
        local pack_save_path=${pack_save_path:-}

        if [[ "${l_is_need_download}" -eq 1 ]]; then
            fnBase_OperationProcedureStatement "Downloading ${c_red}${online_release_pack_name}${c_normal}"
            pack_save_path="${temp_save_dir}/${online_release_pack_name}"
            [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"

            $download_method "${online_release_downloadlink}" > "${pack_save_path}"

            if [[ -s "${pack_save_path}" ]]; then
                fnBase_OperationProcedureResult
            else
                [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
                fnBase_OperationProcedureResult '' 1
            fi

            fnBase_OperationProcedureStatement "SHA512 dgst verification"
            if [[ -n "${online_release_dgst}" && $(fnBase_HashDigestVerification "${pack_save_path}" '512') == "${online_release_dgst}"  ]]; then
                fnBase_OperationProcedureResult
            else
                [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
                fnBase_OperationProcedureResult '' 1
            fi

            # use variable "source_pack_path" to execute decompress operation, variable "pack_save_path" used for delete temporary download package
            source_pack_path="${pack_save_path}"
        fi

        # - Decompress & Extract
        fnBase_OperationProcedureStatement 'Installing'
        local l_installation_dir="${installation_dir}/${l_product_name}"

        local l_application_backup_path="${l_installation_dir}${bak_suffix}"
        [[ -d "${l_application_backup_path}" ]] && rm -rf "${l_application_backup_path}"

        [[ -d "${l_installation_dir}" ]] && mv "${l_installation_dir}" "${l_application_backup_path}"
        [[ -d "${l_installation_dir}" ]] || mkdir -p "${l_installation_dir}"
        tar xf "${source_pack_path}" -C "${l_installation_dir}" --strip-components=1 2> /dev/null

        local new_installed_version=${new_installed_version:-}
        # new_installed_version=$("${l_installation_dir}/bin/${l_product_name}" --version | sed -r 's@^[^[:digit:]]+([[:digit:].]+),?.*@\1@g;')
        new_installed_version=$(fn_LocalVersionDetection "${l_product_name}")

        # check if installed
        if [[ "${new_installed_version}" != "${online_release_version}" ]]; then
            [[ -d "${l_installation_dir}" ]] && rm -rf "${l_installation_dir}"
            [[ "${l_is_existed}" -eq 1 ]] && mv "${l_application_backup_path}" "${l_installation_dir}"
            fnBase_OperationProcedureResult '' 1
        else
            chown -R "${user_name}":"${group_name}" "${l_installation_dir}"
            find "${l_installation_dir}"/ -type d -exec chmod 755 {} \;

            if [[ -d "${l_installation_dir}/bin" ]]; then
                rm -f "${l_installation_dir}"/bin/{*.bat,*.exe}
                find "${l_installation_dir}"/bin/ -type f -exec chmod 750 {} \;
            fi

            # configuration file in /etc/
            local l_config_dir="${config_dir}/${l_product_name}"

            if [[ ! -d "${l_config_dir}" ]]; then
                [[ -d "${l_config_dir}" ]] || mkdir -p "${l_config_dir}"

                if [[ ! -d "${l_installation_dir}/config" ]]; then
                    case "${l_product_name}" in
                        "${filebeat_name}" )
                            # /etc/filebeat/: fields.yml  filebeat.reference.yml  filebeat.yml  modules.d
                            mv "${l_installation_dir}"/*.yml "${l_config_dir}"
                            [[ -d "${l_installation_dir}/modules.d" ]] && mv "${l_installation_dir}/modules.d" "${l_config_dir}"
                            ;;
                    esac
                else
                    cp -f "${l_installation_dir}/config"/* "${l_config_dir}"

                    # Just for logstash
                    # https://www.elastic.co/guide/en/logstash/current/configuration-file-structure.html
                    if [[ "${l_product_name}" == "${logstash_name}" ]]; then
                        # https://www.elastic.co/guide/en/logstash/current/configuration.html
                        # https://www.elastic.co/guide/en/logstash/current/config-examples.html
                        # Logstash pipeline config dir is /etc/elastic/logstash/conf.d/, give a sample conf about syslog which is named 'logstash-syslog.conf.sample'

                        [[ -d "${l_config_dir}/conf.d" ]] || mkdir -p "${l_config_dir}/conf.d"
                        local l_sample_conf="${l_config_dir}/conf.d/logstash-simple.conf.sample"
                        echo "input { stdin { } }|output {|  elasticsearch { hosts => [\"127.0.0.1:9200\"] }|  stdout { codec => rubydebug }|}" > "${l_sample_conf}"
                        sed -r -i 's@\|@\n@g;' "${l_sample_conf}"

                        # syslog
                        # [WARN ][logstash.outputs.elasticsearch] Detected a 6.x and above cluster: the `type` event field won't be used to determine the document _type {:es_version=>6}
                        local l_syslog_conf="${l_config_dir}/conf.d/logstash-syslog.conf.sample"
                        echo "input {|  file {|    path => \"/var/log/syslog\"|    type => syslog|    start_position => \"beginning\"|  }|}||filter {|  if [type] == \"syslog\" {|    grok {|      match => { \"message\" => \"%{SYSLOGTIMESTAMP:syslog_timestamp} %{SYSLOGHOST:syslog_hostname} %{DATA:syslog_program}(?:\[%{POSINT:syslog_pid}\])?: %{GREEDYDATA:syslog_message}\" }|      add_field => [ \"received_at\", \"%{@timestamp}\" ]|      add_field => [ \"received_from\", \"%{host}\" ]|    }|    date {|      match => [ \"syslog_timestamp\", \"MMM  d HH:mm:ss\", \"MMM dd HH:mm:ss\" ]|    }|  }|}||output {|  elasticsearch {|    hosts => [\"127.0.0.1:9200\"]|    index => \"syslog-%{+YYYY.MM.dd}\"|  }|  stdout { codec => rubydebug }|}" > "${l_syslog_conf}"
                        sed -r -i 's@\|@\n@g;' "${l_syslog_conf}"

                        # ufw.log
                        # https://gist.github.com/thorrsson/8978e0b712ad637458c0
                        # https://help.ubuntu.com/community/UFW
                        # %{SYSLOGTIMESTAMP:ufw_date} %{DATA:ufw_hostname}: \[%{BASE10NUM:ufw_uptime}\] \[UFW %{WORD:ufw_event}\] IN=%{DATA:ufw_interfact_in} OUT=%{DATA:ufw_interfact_out} MAC=%{DATA:ufw_mac} SRC=%{IP:ufw_ip_src} DST=%{IP:ufw_ip_dst} LEN=%{INT:ufw_pack_len} TOS=%{DATA:ufw_tos} PREC=%{DATA:ufw_prec} TTL=%{INT:ufw_ttl} ID=%{INT:ufw_ID} PROTO=%{WORD:ufw_protocol}( SPT=%{INT:ufw_src_port})?( DPT=%{INT:ufw_dst_port})?( %{GREEDYDATA:ufw_tcp_opts})?
                    fi

                fi

                chown -R "${user_name}":"${group_name}" "${l_config_dir}"
                find "${l_config_dir}"/ -type f -exec chmod 644 {} \;
                find "${l_config_dir}"/ -type d -exec chmod 755 {} \;
            fi

            # remove backup dir , temporary file
            if [[ -d "${l_installation_dir}/bin" || -f "${l_installation_dir}/${l_product_name}" ]]; then
                [[ -d "${l_application_backup_path}" ]] && rm -rf "${l_application_backup_path}"

                # copy newly downloaded pack to ~/Downloads
                local l_user_download_save_path=${l_user_download_save_path:-"${user_download_dir}/${source_pack_path##*/}"}
                if [[ -d "${user_download_dir}" && ! -f "${l_user_download_save_path}" ]]; then
                    cp "${source_pack_path}" "${l_user_download_save_path}"
                    chown "${login_user}" "${l_user_download_save_path}"
                    chgrp -hR "${login_user}" "${l_user_download_save_path}" 2> /dev/null

                    # removed old version package existed
                    local l_existed_old_pack_path=${l_existed_old_pack_path:-"${user_download_dir}/${online_release_pack_name//${online_release_version}/${current_local_version}}"}
                    [[ -f "${l_existed_old_pack_path}" ]] && rm -f "${l_existed_old_pack_path}"
                fi
            fi

            [[ -d "${l_application_backup_path}" ]] && rm -rf "${l_application_backup_path}"
            [[ -n "${pack_save_path}" && -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
            fnBase_OperationProcedureResult "${c_red}${l_installation_dir%/}/${c_normal}"
        fi

        # - init script
        fnBase_OperationProcedureStatement 'system startup script'

        case "${l_product_name}" in
            "${elasticsearch_name}"|"${kibana_name}" )
                local l_service_script_dir
                local l_service_script_link
                local l_service_script_path

                if fnBase_CommandExistIfCheck 'systemctl'; then
                    l_service_script_dir='/etc/systemd/system'
                    l_service_script_link="${online_service_script_sample_dir}/${l_product_name}.service"
                    l_service_script_path="${l_service_script_dir}/${stack_name}_${l_product_name}.service"

                    [[ -f "${l_service_script_path}" ]] && fnBase_SystemServiceManagement "${l_service_script_path##*/}" 'stop'

                    $download_method "${l_service_script_link}" > "${l_service_script_path}"
                    sed -r -i '/^\[Service\]/,/^\[/{s@^(User=).*$@\1'"${user_name}"'@g;s@^(Group=).*$@\1'"${group_name}"'@g;}' "${l_service_script_path}"

                    case "${l_product_name}" in
                        "${kibana_name}" )
                            sed -r -i '/^\[Service\]/,/^\[/{s@^(ExecStart=).*$@\1'"${l_installation_dir}/bin/${l_product_name} \"-c ${config_dir}/${l_product_name}/${l_product_name}.yml\""'@g;}' "${l_service_script_path}"
                            ;;
                    esac

                    chmod 644 "${l_service_script_path}"
                else
                    l_service_script_dir='/etc/init.d'
                    l_service_script_path="${l_service_script_dir}/${stack_name}_${l_product_name}"
                    l_service_script_link="${online_service_script_sample_dir}/${l_product_name}.init"
                    [[ -f "${l_service_script_path}" ]] && fnBase_SystemServiceManagement "${l_service_script_path##*/}" 'stop'

                    $download_method "${l_service_script_link}" > "${l_service_script_path}"
                    sed -r -i '/Configuraton Start/,/Configuraton End/{s@^(USER=).*$@\1'"${user_name}"'@g;s@^(GROUP=).*$@\1'"${group_name}"'@g;s@^(STACK_NAME=).*$@\1'"${stack_name}"'@g;}' "${l_service_script_path}"

                    chmod 755 "${l_service_script_path}"
                fi
                ;;
            "${logstash_name}" )
                ### - startup.options - ###
                # is used by $LS_HOME/bin/system-install to create a custom startup script for Logstash.
                local l_install_startup_option_path="${l_installation_dir}/config/startup.options"
                local l_startup_option="${l_config_dir}/startup.options"

                if [[ -s "${l_startup_option}" ]]; then
                    # JAVACMD=/usr/bin/java
                    # local l_java_path='/usr/bin/java'
                    # sed -r -i '/^#?[[:space:]]*JAVACMD=/{s@^#?[[:space:]]*([^=]+=).*$@\1'"${l_java_path}"'@g;}' "${l_startup_option}"
                    # LS_HOME=/usr/share/logstash
                    sed -r -i '/^#?[[:space:]]*LS_HOME=/{s@^#?[[:space:]]*([^=]+=).*$@\1'"${l_installation_dir}"'@g;}' "${l_startup_option}"
                    # LS_SETTINGS_DIR=/etc/logstash
                    sed -r -i '/^#?[[:space:]]*LS_SETTINGS_DIR=/{s@^#?[[:space:]]*([^=]+=).*$@\1'"${l_config_dir}"'@g;}' "${l_startup_option}"

                    # LS_OPTS="--path.settings ${LS_SETTINGS_DIR}"
                    # local l_ls_opts="--path.settings ${LS_SETTINGS_DIR} -f"
                    # sed -r -i '/^#?[[:space:]]*LS_OPTS=/{s@^#?[[:space:]]*([^=]+=).*$@\1'"${l_ls_opts}"'@g;}' "${l_startup_option}"

                    # LS_JAVA_OPTS=""

                    # LS_PIDFILE=/var/run/logstash.pid
                    # pidfiles aren't used the same way for upstart and systemd; this is for sysv users.
                    local l_pid_file="${run_dir}/${l_product_name}.pid"
                    sed -r -i '/^#?[[:space:]]*LS_PIDFILE=/{s@^#?[[:space:]]*([^=]+=).*$@\1'"${l_pid_file}"'@g;}' "${l_startup_option}"

                    # LS_USER=logstash
                    # LS_GROUP=logstash
                    sed -r -i '/^#?[[:space:]]*LS_USER=/{s@^#?[[:space:]]*([^=]+=).*$@\1'"${user_name}"'@g;}' "${l_startup_option}"
                    sed -r -i '/^#?[[:space:]]*LS_GROUP=/{s@^#?[[:space:]]*([^=]+=).*$@\1'"${group_name}"'@g;}' "${l_startup_option}"

                    # LS_GC_LOG_FILE=/var/log/logstash/gc.log
                    # Enable GC logging by uncommenting the appropriate lines in the GC logging section in jvm.options
                    local l_gc_log_path="${log_dir}/${l_product_name}/gc.log"
                    sed -r -i '/^#?[[:space:]]*LS_GC_LOG_FILE=/{s@^#?[[:space:]]*([^=]+=).*$@\1'"${l_gc_log_path}"'@g;}' "${l_startup_option}"
                    # LS_OPEN_FILES=16384
                    local l_open_files='65535'
                    sed -r -i '/^#?[[:space:]]*LS_OPEN_FILES=/{s@^#?[[:space:]]*([^=]+=).*$@\1'"${l_open_files}"'@g;}' "${l_startup_option}"
                    # LS_NICE=19
                    local l_nice='19'
                    sed -r -i '/^#?[[:space:]]*LS_NICE=/{s@^#?[[:space:]]*([^=]+=).*$@\1'"${l_nice}"'@g;}' "${l_startup_option}"

                    # SERVICE_NAME="logstash"
                    # SERVICE_DESCRIPTION="logstash"
                    # Change these to have the init script named and described differently
                    # This is useful when running multiple instances of Logstash on the same physical box or vm
                    local l_service_name="${stack_name}_${l_product_name}"
                    local l_description="${l_product_name^}"
                    sed -r -i '/^#?[[:space:]]*SERVICE_NAME=/{s@^#?[[:space:]]*([^=]+=).*$@\1'"\"${l_service_name}\""'@g;}' "${l_startup_option}"
                    sed -r -i '/^#?[[:space:]]*SERVICE_DESCRIPTION=/{s@^#?[[:space:]]*([^=]+=).*$@\1'"\"${l_description}\""'@g;}' "${l_startup_option}"
                fi

                cp -f "${l_startup_option}" "${l_install_startup_option_path}"

                # - generate service script
                local l_generator_path="${installation_dir}/${l_product_name}/bin/system-install"
                [[ -s "${l_generator_path}" ]] && bash "${l_generator_path}" &> /dev/null

                l_service_script_path="${l_startup_option}"
                ;;
            "${filebeat_name}" )
                local l_service_script_dir
                local l_service_script_path

                if fnBase_CommandExistIfCheck 'systemctl'; then
                    l_service_script_dir='/etc/systemd/system'
                    l_service_script_path="${l_service_script_dir}/${stack_name}_${l_product_name}.service"

                    [[ -f "${l_service_script_path}" ]] && fnBase_SystemServiceManagement "${l_service_script_path##*/}" 'stop'

                    local l_binary_path=${l_binary_path:-"${installation_dir}/${l_product_name}/bin/${l_product_name}"}
                    [[ -f "${l_binary_path}" ]] || l_binary_path="${installation_dir}/${l_product_name}/${l_product_name}"

                    echo -e "[Unit]\nDescription=filebeat\nWants=network-online.target\nAfter=network-online.target\n\n[Service]\nUser=${user_name}\nGroup=${group_name}\nExecStart=${l_binary_path} -c ${l_config_dir}/filebeat.yml -path.home ${l_installation_dir} -path.config ${l_config_dir} -path.data ${data_dir}/${l_product_name} -path.logs ${log_dir}/${l_product_name}\nRestart=always\nLimitNOFILE=65535\n\n[Install]\nWantedBy=multi-user.target" > "${l_service_script_path}"

                    chmod 644 "${l_service_script_path}"
                fi
                ;;
        esac

        fnBase_OperationProcedureResult "${l_service_script_path}"

        # - configuration
        fn_DirPermissionConfiguration "${config_dir}"
        fn_DirPermissionConfiguration "${data_dir}"
        fn_DirPermissionConfiguration "${log_dir}"
        fn_DirPermissionConfiguration "${run_dir}"

        fn_ConfigDirectiveOptimization "${l_product_name}"
    fi
}

fn_CoreOperationProcedure(){
    [[ "${elasticsearch_install}" -eq 1 || "${version_check}" -eq 1 || -d "${installation_dir}/${elasticsearch_name}" ]] && fn_CommonOperationProcedure "${elasticsearch_name}"
    [[ "${kibana_install}" -eq 1 || "${version_check}" -eq 1 || -d "${installation_dir}/${kibana_name}" ]] && fn_CommonOperationProcedure "${kibana_name}"
    [[ "${logstash_install}" -eq 1 || "${version_check}" -eq 1 || -d "${installation_dir}/${logstash_name}" ]] && fn_CommonOperationProcedure "${logstash_name}"
    [[ "${beats_install}" -eq 1 || "${version_check}" -eq 1 || -d "${installation_dir}/${filebeat_name}" ]] && fn_CommonOperationProcedure "${filebeat_name}"
}


#########  2-5. Operation Time Cost  #########
fn_TotalTimeCosting(){
    finish_time=$(date +'%s')        # End Time Of Operation
    total_time_cost=$((finish_time-start_time))   # Total Time Of Operation
    printf "\nTotal time cost is ${c_red}%s${c_normal} seconds!\n" "${total_time_cost}"
}


#########  3. Executing Process  #########
fn_InitializationCheck

fn_UninstallOperation
fn_Prerequisites
fn_CoreOperationProcedure
fn_TotalTimeCosting


#########  4. EXIT Singal Processing  #########
# trap "commands" EXIT # execute command when exit from shell
fn_TrapEXIT(){
    fnBase_UnsetVars
    rm -rf /tmp/elasticsearch.* 2>/dev/null
    rm -rf /tmp/hsperfdata_root 2>/dev/null

    unset stack_name
    unset software_fullname
    unset installation_dir
    unset config_dir
    unset log_dir
    unset run_dir
    unset data_dir
    unset logstash_install
    unset kibana_install
    unset strict_mode_enable
    unset version_check
    unset is_uninstall
    unset proxy_server_specify
    unset source_pack_path
}

trap fn_TrapEXIT EXIT


# Script End
