#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails


# Official Site: https://prometheus.io
# Documentation
# - https://prometheus.io/docs/introduction/overview/
# - https://prometheus.io/docs/instrumenting/exporters/

# Metrics
# Prometheus - http://127.0.0.1:9090/metrics
# node_exporter - http://stretch:9100/metrics
# mysqld_exporter - http://stretch:9104/metrics

# Target: Pormethus & Exporters Installation And Configuration On GNU/Linux (RHEL/CentOS/Fedora/Debian/Ubuntu/OpenSUSE and variants)
# Developer: MaxdSre

# Change Log:
# - Oct 08, 2020 08:56 Thu ET - initialization check method update
# - Oct 15, 2019 12:29 Tue ET - change custom script url
# - Oct 09, 2019 12:40 Wed ET - change to new base functions
# - Dec 31, 2018 21:46 Mon ET - code reconfiguration, include base funcions from other file
# - Jul 06, 2018 10:35 Fri ET - change data dir to /var/lib/prometheus
# - Jul 03, 2018 12:10 Tue ET - code reconfiguration
# - Dec 15, 2017 16:48 Fri +0800


#Currently just support prometheus/mysqld_exporter/node_exporter

# For MongoDB
# https://github.com/percona/mongodb_exporter

# For MySQL
# https://github.com/prometheus/mysqld_exporter
# https://github.com/percona/mysqld_exporter

# For Redis
# https://github.com/oliver006/redis_exporter
# prometheus_redis_exporter.service
# ExecStart=/opt/Prometheus/Exporter/redis_exporter -redis.addr $IP:$PORT -redis.password $PASS


#########  0-1. Singal Setting  #########
trap fnBase_TrapCleanUp INT QUIT

#########  0-2. Variables Setting  #########
# umask 022    # temporarily change umask value to 022
readonly official_site='https://prometheus.io'
readonly download_page="${official_site}/download/"
readonly custom_shellscript_url='https://gitlab.com/axdsop/nixnotes/raw/master/ShellScripts/Toolkits'
readonly online_config_file_sample_dir="${custom_shellscript_url}/_Configs/Application/prometheus/config"
readonly online_service_script_sample_dir="${custom_shellscript_url}/_Configs/Application/prometheus/initScript"

software_fullname=${software_fullname:-'Prometheus'}
software_name=${software_name:-'prometheus'}
target_dir=${target_dir:-"/opt/${software_fullname}"}
server_dir=${server_dir:-"${target_dir}/Server"}
alertmanager_dir=${alertmanager_dir:-"${target_dir}/Alertmanager"}
exporter_dir=${exporter_dir:-"${target_dir}/Exporter"}
configuration_dir=${configuration_dir:-"/etc/${software_name}"}
readonly data_dir_default="/var/lib/${software_name}"
data_dir=${data_dir:-"${data_dir_default}"}
log_dir=${log_dir:-"/var/log/${software_name}"}
source_pack_path=${source_pack_path:-}

list_exporter_info=${list_exporter_info:-0}
server_install=${server_install:-0}
alertmanager_install=${alertmanager_install:-0}
pack_name_specify=${pack_name_specify:-''}
is_uninstall=${is_uninstall:-0}
proxy_server_specify=${proxy_server_specify:-}
strict_mode_enable=${strict_mode_enable:-0}


#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | sudo bash -s -- [options] ...

Pormethus & Exporters Installation And Configuration On GNU Linux!
This script requires superuser privileges (eg. root, su).

[available option]
    -h    --help, show help info
    -d data_dir    --set data dir, default is /var/lib/prometheus
    -l    --list info of Prometheus & its exporters (name,version,release date)
    -f pack_path    --specify latest release package save dir (default is ~/Downloads/) in system
    -i pack_name    --install pack_name listed from parameter '-l'
    -s    --install Prometheus server, default is not install
    -a    --install Prometheus alertmanager, default is not install
    -r    --use strict mode (create user prometheus, default is root) or delete user prometheus, remove datadir while uninstall
    -p [protocol:]ip:port    --proxy host (http|https|socks4|socks5), default protocol is http
    -u    --uninstall, uninstall software installed
\e[0m"
#     -c    --check, check current stable release version
}

while getopts "had:f:li:srup:" option "$@"; do
    case "$option" in
        a ) alertmanager_install=1 ;;
        d ) data_dir="$OPTARG" ;;
        f ) source_pack_path="$OPTARG" ;;
        l ) list_exporter_info=1 ;;
        i ) pack_name_specify="$OPTARG" ;;
        s ) server_install=1 ;;
        r ) strict_mode_enable=1 ;;
        u ) is_uninstall=1 ;;
        p ) proxy_server_specify="$OPTARG" ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done


#########  1-2 Preparation Operation  #########
fn_InitializationCheck(){
    local l_user_codebase_dir="$HOME/.config/axdsop"
    local l_base_function_temp_save_path=''
    local l_need_remove=0
    local l_base_function_url='https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/base_functions.sh'
    l_base_function_temp_save_path=$(find "${l_user_codebase_dir}" -type f -name "${l_base_function_url##*/}" -print 2> /dev/null)
    
    if [[ ! -f "${l_base_function_temp_save_path}" ]]; then
        local l_download_tool=${l_download_tool:-'wget -qO-'}
        [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_tool='curl -fsL'
        l_need_remove=1
        l_base_function_temp_save_path=$(mktemp -t XXXXXXX)
        ${l_download_tool} "${l_base_function_url}" > "${l_base_function_temp_save_path}"
    fi

    if [[ -f "${l_base_function_temp_save_path}" && -s "${l_base_function_temp_save_path}" ]]; then
        . "${l_base_function_temp_save_path}"
        [[ "${l_need_remove}" -eq 1 ]] && rm "${l_base_function_temp_save_path}"
    else
        echo "Sorry, fail to get base script path."
        exit
    fi


    # tput clear  # Echo the clear-screen sequence for the current terminal.
    # specify 1 means need sudo privilege
    fnBase_RunningEnvironmentCheck '1'

    fnBase_CommandExistCheckPhase 'gzip' # CentOS/Debian/OpenSUSE: gzip
    fnBase_CommandExistCheckPhase 'tar' # .tar.gz
}

fn_UtilityVersionInfo(){
    local l_path="${1:-}"
    local l_version_no=''
    if [[ -f "${l_path}" ]]; then
        l_version_no=$("${l_path}" --version 2>&1 | sed -r -n '/version[[:space:]]*[[:digit:]]+/{s@^[^[:digit:]]+([^[:space:]]+).*$@\1@g;p}')
    fi
    echo "${l_version_no}"
}


#########  2-1. Uninstall Operation  #########
fn_UninstallOperation(){
    if [[ "${is_uninstall}" -eq 1 ]]; then
        if [[ -d "${target_dir}" ]]; then
            local l_service_script_dir='/etc/init.d'
            fnBase_CommandExistIfCheck 'systemctl' && l_service_script_dir='/etc/systemd/system'

            # - stop daemon running
            find "${l_service_script_dir}"/ -type f -name "${software_name}*" -print | while IFS="" read -r line; do
                if [[ -n "${line}" ]]; then
                    service_name="${line##*/}"
                    service_name="${service_name%%.*}"
                    fnBase_SystemServiceManagement "${service_name}" 'stop'
                    # - remove system init script   SysV init / Systemd
                    [[ -f "${line}" ]] && rm -f "${line}"
                    unset service_name
                fi
            done

            # - remove data dir
            [[ -d "${log_dir}" ]] && rm -rf "${log_dir}"
            [[ -d "${configuration_dir}" ]] && rm -rf "${configuration_dir}"
            [[ -d "${target_dir}" ]] && rm -rf "${target_dir}"

            if [[ "${strict_mode_enable}" -eq 1 ]]; then
                [[ -d "${data_dir}" ]] && rm -rf "${data_dir}"

                # - remove user and group - prometheus
                if [[ -n $(sed -r -n '/^'"${software_name}"':/{p}' /etc/passwd) ]]; then
                    userdel -fr "${software_name}" 2> /dev/null
                    groupdel -f "${software_name}" 2> /dev/null
                fi
            fi

            fnBase_CommandExistIfCheck 'systemctl' && systemctl daemon-reload 2> /dev/null

            fnBase_ExitStatement "${software_fullname} is successfully removed from your system!"
        else
            fnBase_ExitStatement "${c_blue}Note${c_normal}: no ${software_fullname} is found in your system!"
        fi
    fi
}

#########  2-2. Extract Latest Package Info  #########
fn_LatestPacksInfoExtraction(){
    online_release_pack_info=$(mktemp -t "${mktemp_format}")

    $download_method "${download_page}" | sed -r -n '/<strong>/{s@\>[[:space:]]+\/?[[:space:]]*\<@|@g;s@>v@>@g;p}; /<h2 id="/{s@^[[:space:]]*@@g;s@<h2[^>]*>@&---@g;p}; /linux.*amd64/,/<\/tr>/{/(id|class)=/!d;s@.*href="([^"]*)".*@\1@g;p}' | sed ':a;N;$!ba;s@\n@|@g;s@[[:space:]]*<[^>]*>[[:space:]]*@@g;s@^---@@g;s@|*---@\n@g;' > "${online_release_pack_info}"
    # prometheus|2.3.1|2018-06-19|https://github.com/prometheus/prometheus/releases/download/v2.3.1/prometheus-2.3.1.linux-amd64.tar.gz|adb76021fcff8a2a8363de8739fcb7ff5647c2a0ff90b2c02dcb56cf0cd836f0

    [[ -s "${online_release_pack_info}" ]] || fnBase_ExitStatement "${c_red}Sorry${c_normal}: fail to extract packs info from page ${c_blue}${download_page}${c_normal}!"

    # list latest exporter info
    if [[ "${list_exporter_info}" -eq 1 ]]; then
        fnBase_CentralOutputTitle 'Prometheus & Exporters Info'
        # awk -F\| 'NF>1{printf("%-20s|%-8s|%s\n",$1,$2,$3)}' "${online_release_pack_info}"
        cut -d\| -f 1,2,3 "${online_release_pack_info}" | sed 's/|/ /g' | column -t
        exit
    fi

    # if [[ "${pack_name_specify,,}" == 'mysqld_exporter' ]]; then
    #     fnBase_CommandExistIfCheck 'mysql' || fnBase_ExitStatement "${c_red}Sorry${c_normal}: fail to find mysqld service on your system!"
    # fi
}

#########  2-3. Prerequisites  #########
fn_ParaSpecifiedValVerification(){
    if [[ "${server_install}" -eq 1 ]]; then
        fnBase_OperationPhaseStatement 'Parameters Specified Val Verification'

        # - verify data dir
        fnBase_OperationProcedureStatement 'Data dir verification'
        if [[ -n "${data_dir}" ]]; then
            data_dir="${data_dir%/}"
            if [[ "${data_dir}" == "${data_dir_default}" || "${data_dir}" =~ ^/ ]]; then
                fnBase_OperationProcedureResult "${data_dir}"
            else
                fnBase_OperationProcedureResult "${data_dir}" 1
            fi
        fi
    fi
}

fn_NormalUserOperation(){
    user_name=${user_name:-'root'}
    group_name=${group_name:-'root'}

    if [[ -z $(sed -r -n '/^'"${software_name}"':/{p}' /etc/passwd) ]]; then
        if [[ "${strict_mode_enable}" -eq 1  ]]; then
            fnBase_OperationProcedureStatement 'Create user & group'
            # create group
            groupadd -r "${software_name}" 2> /dev/null
            # create user without login privilege
            useradd -r -g "${software_name}" -s /bin/false -d "${target_dir}" -c 'Prometheus Server Daemon' "${software_name}"
            user_name="${software_name}"
            group_name="${software_name}"
            fnBase_OperationProcedureResult "${user_name}:${group_name}"
        fi
    else
        user_name="${software_name}"
        group_name="${software_name}"
    fi
}


#########  2-4. Download & Decompress Operation  #########
fn_DirPermissionConfiguration(){
    local l_dir="${1:-}"
    local l_mode=${2:-755}
    local l_user=${3:-"${user_name}"}
    local l_group=${4:-"${group_name}"}

    [[ -n "${l_dir}" && ! -d "${l_dir}" ]] && mkdir -p "${l_dir}"
    if [[ -d "${l_dir}" ]]; then
        chmod "${l_mode}" "${l_dir}"
        chown -R "${l_user}":"${l_group}" "${l_dir}"
    fi
}

fn_CommonOperationProcedure(){
    local l_pack_name="${1:-}"
    fnBase_CentralOutputTitle "${l_pack_name}" '1'
    fnBase_OperationPhaseStatement "${l_pack_name^} Version"

    # - Local version
    local l_is_existed=${l_is_existed:-0}
    current_local_version=${current_local_version:-}

    local l_target_dir=${l_target_dir:-}
    case "${l_pack_name}" in
        prometheus ) l_target_dir="${server_dir}" ;;
        alertmanager ) l_target_dir="${alertmanager_dir}" ;;
        * ) l_target_dir="${exporter_dir}" ;;
    esac

    if [[ -f "${l_target_dir}/${l_pack_name}" ]]; then
        current_local_version=$(fn_UtilityVersionInfo "${l_target_dir}/${l_pack_name}")
        if [[ -n "${current_local_version}" ]]; then
            l_is_existed=1
            fnBase_OperationProcedureStatement 'Local version detecting'
            fnBase_OperationProcedureResult "${current_local_version}"
        fi
    fi

    # - Latest online version info
    fnBase_OperationProcedureStatement 'Online version detecting'
    local l_release_info=${l_release_info:-}
    l_release_info=$(sed -r -n '/^'"${l_pack_name}"'\|/{p}' "${online_release_pack_info}")
    # prometheus|2.3.1|2018-06-19|https://github.com/prometheus/prometheus/releases/download/v2.3.1/prometheus-2.3.1.linux-amd64.tar.gz|adb76021fcff8a2a8363de8739fcb7ff5647c2a0ff90b2c02dcb56cf0cd836f0

    local online_release_version
    online_release_version=$(echo "${l_release_info}" | cut -d\| -f 2)
    local online_release_date
    online_release_date=$(echo "${l_release_info}" | cut -d\| -f 3 | date +'%b %d, %Y' -f - 2> /dev/null)

    if [[ -n "${online_release_version}" ]]; then
        fnBase_OperationProcedureResult "${online_release_version} - ${online_release_date}"
    else
        fnBase_OperationProcedureResult '' 1
    fi

    # - Download & Decompress Latest Software
    if [[ -n "${online_release_version}" && "${current_local_version}" != "${online_release_version}" ]]; then
        local l_operation_type=${l_operation_type:-'installing'}
        [[ "${l_is_existed}" -eq 1 ]] && l_operation_type='updating'
        fnBase_OperationPhaseStatement "${l_operation_type} operation"

        local online_release_downloadlink
        online_release_downloadlink=$(echo "${l_release_info}" | cut -d\| -f 4)
        local online_release_dgst
        online_release_dgst=$(echo "${l_release_info}" | cut -d\| -f 5)
        local online_release_pack_name
        online_release_pack_name="${online_release_downloadlink##*/}"

        local l_is_need_download=${l_is_need_download:-1}
        local l_specified_pack_dir=${l_specified_pack_dir:-}

        # specified dir or ~/Downloads existed
        if [[ (-n "${source_pack_path}" && -d "${source_pack_path}" ) || -d "${user_download_dir}" ]]; then
            if [[ -d "${source_pack_path}" ]]; then
                l_specified_pack_dir="${source_pack_path}"
            elif [[ -d "${user_download_dir}" ]]; then
                l_specified_pack_dir="${user_download_dir}"
            fi

            l_specified_pack_dir="${l_specified_pack_dir%/}"

            if [[ -d "${l_specified_pack_dir}" && -f "${l_specified_pack_dir}/${online_release_pack_name}" ]]; then
                source_pack_path="${l_specified_pack_dir}/${online_release_pack_name}"
            fi
        fi

        # 1.1- existed package path in system
        if [[ -n "${source_pack_path}" && -f "${source_pack_path}" ]]; then
            fnBase_OperationProcedureStatement 'Existed pack dgst verifying'

            if [[ -n "${online_release_dgst}" && $(fnBase_HashDigestVerification "${source_pack_path}" '256') == "${online_release_dgst}" ]]; then
                fnBase_OperationProcedureResult "${c_red}${source_pack_path%/*}/${c_normal}"
                l_is_need_download=0
            else
                fnBase_OperationProcedureResult '' 1
            fi
        fi

        # 2 - Download Directly From Official Site
        local pack_save_path=${pack_save_path:-}
        if [[ "${l_is_need_download}" -eq 1 ]]; then
            fnBase_OperationProcedureStatement "Downloading ${c_red}${online_release_pack_name}${c_normal}"
            pack_save_path="${temp_save_dir}/${online_release_pack_name}"
            [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"

            $download_method "${online_release_downloadlink}" > "${pack_save_path}"

            if [[ -s "${pack_save_path}" ]]; then
                fnBase_OperationProcedureResult
            else
                [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
                fnBase_OperationProcedureResult '' 1
            fi

            fnBase_OperationProcedureStatement "SHA256 dgst verification"
            if [[ -n "${online_release_dgst}" && $(fnBase_HashDigestVerification "${pack_save_path}" '256') == "${online_release_dgst}"  ]]; then
                fnBase_OperationProcedureResult
            else
                [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
                fnBase_OperationProcedureResult '' 1
            fi

            # use variable "source_pack_path" to execute decompress operation, variable "pack_save_path" used for delete temporary download package
            source_pack_path="${pack_save_path}"
        fi

        # - Decompress & Extract
        fnBase_OperationProcedureStatement 'Installing'
        local l_installation_dir=${l_installation_dir:-}
        local l_config_path=${l_config_path:-}
        local l_execution_file_path=${l_execution_file_path:-}

        case "${l_pack_name}" in
            prometheus )
                l_installation_dir="${server_dir}"
                l_config_path="${configuration_dir}/${software_name}.yml"
                ;;
            alertmanager )
                l_installation_dir="${alertmanager_dir}"
                ;;
            * )
                l_installation_dir="${exporter_dir}"
                case "${l_pack_name}" in
                    mysqld_exporter )
                        l_config_path="${configuration_dir}/.my.cnf"
                        [[ -f "${l_config_path}" ]] && rm -f "${l_config_path}"
                        ;;
                esac
                ;;
        esac

        local l_application_backup_path="${l_installation_dir}${bak_suffix}"

        case "${l_pack_name}" in
            prometheus|alertmanager )
                [[ -d "${l_application_backup_path}" ]] && rm -rf "${l_application_backup_path}"
                [[ -d "${l_installation_dir}" ]] && mv "${l_installation_dir}" "${l_application_backup_path}"
                ;;
        esac

        [[ -d "${l_installation_dir}" ]] || mkdir -p "${l_installation_dir}"
        tar xf "${source_pack_path}" -C "${l_installation_dir}" --strip-components=1 2> /dev/null

        local new_installed_version=${new_installed_version:-}
        new_installed_version=$(fn_UtilityVersionInfo "${l_installation_dir}/${l_pack_name}")

        if [[ "${new_installed_version}" != "${online_release_version}" ]]; then
            case "${l_pack_name}" in
                prometheus|alertmanager ) [[ -d "${l_installation_dir}" ]] && rm -rf "${l_installation_dir}" ;;
                * ) [[ -f "${l_installation_dir}/${l_pack_name}" ]] && rm -f "${l_installation_dir}/${l_pack_name}" ;;
            esac

            [[ "${l_is_existed}" -eq 1 && -d "${l_application_backup_path}" ]] && mv "${l_application_backup_path}" "${l_installation_dir}"

            fnBase_OperationProcedureResult '' 1
        else
            [[ -f "${l_installation_dir}/LICENSE" ]] && rm -f "${l_installation_dir}/LICENSE"
            [[ -f "${l_installation_dir}/NOTICE" ]] && rm -f "${l_installation_dir}/NOTICE"
            find "${l_installation_dir}" -type d -exec chmod 755 {} \;
            chown -R "${user_name}":"${group_name}" "${l_installation_dir}"
            chmod 750 "${l_installation_dir}/${l_pack_name}"

            # copy newly downloaded pack to ~/Downloads
            local l_user_download_save_path=${l_user_download_save_path:-"${user_download_dir}/${source_pack_path##*/}"}
            if [[ -d "${user_download_dir}" && ! -f "${l_user_download_save_path}" ]]; then
                cp "${source_pack_path}" "${l_user_download_save_path}"
                chown "${login_user}" "${l_user_download_save_path}"

                # removed old version package existed
                if [[ "${l_is_existed}" -eq 1 ]]; then
                    local l_existed_old_pack_path=${l_existed_old_pack_path:-"${user_download_dir}/${online_release_pack_name//${online_release_version}/${current_local_version}}"}
                    [[ -f "${l_existed_old_pack_path}" ]] && rm -f "${l_existed_old_pack_path}"
                fi
            fi

            [[ "${l_is_existed}" -eq 1 && -d "${l_application_backup_path}" ]] && rm -rf "${l_application_backup_path}"

            [[ -n "${pack_save_path}" && -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"

            fnBase_OperationProcedureResult "${c_red}${l_installation_dir%/}/${c_normal}"
        fi

        # - configuration file
        if [[ -f "${l_installation_dir}/${l_pack_name}" ]]; then
            fnBase_OperationProcedureStatement 'Configuration'
            fn_DirPermissionConfiguration "${configuration_dir}"
            fn_DirPermissionConfiguration "${log_dir}"

            if [[ -n "${l_config_path}" && ! -f "${l_config_path}" ]]; then
                case "${l_pack_name}" in
                    prometheus )
                        fn_DirPermissionConfiguration "${data_dir}"
                        $download_method "${online_config_file_sample_dir}/${l_config_path##*/}" > "${l_config_path}"
                        ;;
                    mysqld_exporter )
                        local l_login_mycnf=${l_login_mycnf:-"${login_user_home}/.my.cnf"}

                        if [[ -f "${l_login_mycnf}" ]]; then
                            # need to start mysql
                            local mysql_command=${mysql_command:-"mysql --defaults-file=${l_login_mycnf}"}
                            local export_host='127.0.0.1'
                            local export_user='p_exporter'
                            local export_passwd
                            export_passwd="Prometheus_Monitor@$(date +'%Y')"
                            $mysql_command -e "create user '${export_user}'@'${export_host}' identified by '${export_passwd}';" 2> /dev/null
                            $mysql_command -e "grant process,replication client, select on *.* to '${export_user}'@'${export_host}';" 2> /dev/null
                            $mysql_command -e "flush privileges;" 2> /dev/null
                            echo  -e "[client]\nuser=${export_user}\npassword=\"${export_passwd}\"\nhost=${export_host}" > "${l_config_path}"
                        else
                            fnBase_ExitStatement "${c_red}Sorry${c_normal}: fail to find mysql login conf ${c_blue}${l_login_mycnf}${c_normal}!"
                        fi
                        ;;
                esac

                chown "${user_name}":"${group_name}" "${l_config_path}"
                chmod 640 "${l_config_path}"
            fi

            fnBase_OperationProcedureResult "${l_config_path}"
        fi

        # - service script
        fnBase_OperationProcedureStatement 'system startup script'
        local l_service_script_dir
        local l_service_script_link
        local l_service_script_path

        if fnBase_CommandExistIfCheck 'systemctl'; then
            l_service_script_dir='/etc/systemd/system'
            l_service_script_link="${online_service_script_sample_dir}/${l_pack_name}.service"

            local l_service_name=${l_service_name:-"${l_pack_name}.service"}
            [[ "${l_pack_name}" != 'prometheus' ]] && l_service_name="${software_name}_${l_pack_name}.service"
            l_service_script_path="${l_service_script_dir}/${l_service_name}"

            [[ -f "${l_service_script_path}" ]] && fnBase_SystemServiceManagement "${l_service_script_path##*/}" 'stop'

            $download_method "${l_service_script_link}" > "${l_service_script_path}"
            sed -r -i '/^\[Service\]/,/^\[/{s@^(User=).*$@\1'"${user_name}"'@g;}' "${l_service_script_path}"

            case "${l_pack_name}" in
                prometheus )
                    sed -r -i '/ExecStart[[:space:]]*=/{s@^([^=]+[[:space:]]*=[[:space:]]*).*$@\1'"${l_installation_dir}/${l_pack_name} --config.file=${l_config_path} --storage.tsdb.path=${data_dir}"'@g;}' "${l_service_script_path}"
                    ;;
                mysqld_exporter )
                    sed -r -i '/ExecStart[[:space:]]*=/{s@^([^=]+[[:space:]]*=[[:space:]]*).*$@\1'"${l_installation_dir}/${l_pack_name} --config.my-cnf=${l_config_path}"'@g;}' "${l_service_script_path}"
                    ;;
                node_exporter )
					sed -r -i '/ExecStart[[:space:]]*=/{s@^([^=]+=[[:space:]]*)[^[:space:]]+(.*)$@\1'"${l_installation_dir}/${l_pack_name}"'\2@g;}' "${l_service_script_path}"
                    #sed -r -i '/ExecStart[[:space:]]*=/{s@^([^=]+[[:space:]]*=[[:space:]]*).*$@\1'"${l_installation_dir}/${l_pack_name}"' --log.level=warn@g;}' "${l_service_script_path}"
                    ;;
            esac

            chmod 644 "${l_service_script_path}"
        else
            l_service_script_dir='/etc/init.d'
            l_service_script_link="${online_service_script_sample_dir}/${l_pack_name}.init"

            local l_service_name=${l_service_name:-"${l_pack_name}"}
            [[ "${l_pack_name}" != 'prometheus' ]] && l_service_name="${software_name}_${l_pack_name}"
            l_service_script_path="${l_service_script_dir}/${l_service_name}"

            [[ -f "${l_service_script_path}" ]] && fnBase_SystemServiceManagement "${l_service_script_path##*/}" 'stop'

            $download_method "${l_service_script_link}" > "${l_service_script_path}"
            sed -r -i '/Configuraton Start/,/Configuraton End/{s@^(USER=).*$@\1'"${user_name}"'@g;s@^(GROUP=).*$@\1'"${group_name}"'@g;}' "${l_service_script_path}"
            if [[ "${l_pack_name}" == 'prometheus' ]]; then
                sed -r -i '/Configuraton Start/,/Configuraton End/{s@^(DATA_DIR=).*$@\1'"${data_dir}"'@g;}' "${l_service_script_path}"
            fi
            chmod 755 "${l_service_script_path}"
        fi

        [[ -s "${l_service_script_path}" ]] && fnBase_SystemServiceManagement "${l_service_script_path##*/}" 'enable'
        fnBase_OperationProcedureResult "${l_service_script_path}"
    fi

    unset current_local_version
}

fn_OperationProcedure(){
    fnBase_CentralOutputTitle "${software_fullname}"
    fn_LatestPacksInfoExtraction
    fn_ParaSpecifiedValVerification
    fn_NormalUserOperation

    # scrape timeout must not greater than scrape interval
    # err="Error loading config couldn't load configuration (--config.file=/etc/prometheus/prometheus.yml): parsing YAML file /etc/prometheus/prometheus.yml: scrape timeout greater than scrape interval for scrape config with job name \"prometheus\""

    [[ "${server_install}" -eq 1 || -f "${server_dir}/prometheus" ]] && fn_CommonOperationProcedure 'prometheus'

    # default install pack node_exporter
    fn_CommonOperationProcedure 'node_exporter'

    [[ "${alertmanager_install}" -eq 1 || -f "${exporter_dir}/alertmanager" ]] && fn_CommonOperationProcedure 'alertmanager'

    [[ "${pack_name_specify,,}" == 'mysqld_exporter' || -f "${exporter_dir}/mysqld_exporter" ]] && fn_CommonOperationProcedure 'mysqld_exporter'

    if [[ -n "${pack_name_specify}" ]]; then
        case "${pack_name_specify,,}" in
            node_exporter )
                echo ''
                ;;
            mysqld_exporter )
                # fn_CommonOperationProcedure 'mysqld_exporter'
                echo ''
                ;;
            * )
                echo -e "\n${c_red}Sorry${c_normal}: this script does not support ${c_yellow}${pack_name_specify}${c_normal} currently!"
                ;;
        esac
    fi
}


#########  2-5. Operation Time Cost  #########
fn_TotalTimeCosting(){
    finish_time=$(date +'%s')        # End Time Of Operation
    total_time_cost=$((finish_time-start_time))   # Total Time Of Operation
    printf "\nTotal time cost is ${c_red}%s${c_normal} seconds!\n" "${total_time_cost}"
}


#########  3. Executing Process  #########
fn_InitializationCheck

fn_UninstallOperation
fn_OperationProcedure
fn_TotalTimeCosting

#########  4. EXIT Singal Processing  #########
# trap "commands" EXIT # execute command when exit from shell
fn_TrapEXIT(){
    fnBase_UnsetVars
    unset software_name
    unset software_fullname
    unset target_dir
    unset source_pack_path
    unset server_dir
    unset alertmanager_dir
    unset exporter_dir
    unset configuration_dir
    unset log_dir
    unset user_name
    unset group_name
    unset list_exporter_info
    unset server_install
    unset alertmanager_install
    unset pack_name_specify
    unset is_uninstall
    unset proxy_server_specify
    unset strict_mode_enable
}

trap fn_TrapEXIT EXIT

# Script End
