#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails


# Official Site: https://golang.org/
# Documentation
# - https://github.com/joefitzgerald/go-plus#installing-missing-tools

# Target: Automatically Install & Update Golang On GNU/Linux
# Developer: MaxdSre

# Change Log:
# - Oct 08, 2020 08:58 Thu ET - initialization check method update
# - Oct 09, 2019 09:18 Wed ET - change to new base functions
# - Apr 27, 2019 16:08 Sat ET - add binfmt_misc support to use 'gorun' as an interpreter for golang code
# - Dec 30, 2018 21:49 Sun ET - code reconfiguration, include base funcions from other file
# - May 21, 2018 16:13 Mon ET - code reconfiguration
# - Oct 12, 2017 14:22 Thu +0800
# - Jun 07, 2017 10:33 Wed +0800
# - May 15, 2017 16:48 Mon ET
# - Feb 18, 2017 18:38 Sat +0800


#########  0-1. Singal Setting  #########
trap fnBase_TrapCleanUp INT QUIT

#########  0-2. Variables Setting  #########
# umask 022    # temporarily change umask value to 022
readonly official_site='https://golang.org'
readonly pack_download_page="${official_site}/dl/"        # Download Page，suffix with /
readonly release_note_page="${official_site}/doc/devel/release.html"
# downloadRedirectPage='https://storage.googleapis.com'           #actual download link
readonly os_type='linux-amd64'  # linux-amd64
readonly software_fullname='Golang'
readonly application_name='Golang'
installation_dir="/opt/${application_name}"      # Decompression & Installation Path Of Package
go_bin_path="${installation_dir}/bin/go"
gorun_bin_path='/usr/local/bin/gorun'
is_existed=${is_existed:-1}      # Default value is 1， assume system has installed Golang

source_pack_path=${source_pack_path:-}
go_path=${go_path:-}
binfmt_misc=${binfmt_misc:-0}
version_check=${version_check:-0}
is_uninstall=${is_uninstall:-0}
proxy_server_specify=${proxy_server_specify:-''}


#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | sudo bash -s -- [options] ...

Installing / Updating Golang Programming Language On GNU/Linux!
This script requires superuser privileges (eg. root, su).

[available option]
    -h    --help, show help info
    -c    --check, check current stable release version
    -f pack_path    --specify latest release package save path (e.g. /PATH/*.tar.gz) or dir (default is ~/Downloads/) in system
    -P GOPATH    --set GOPATH path, default is '~/Golang'
    -b    --enable binfmt_misc filesystem (/proc/sys/fs/binfmt_misc) to use 'gorun' as an interpreter for golang code, such as 'gorun /PATH/xxx.go'
    -u    --uninstall, uninstall software installed
    -p [protocol:]ip:port    --proxy host (http|https|socks4|socks5), default protocol is http
\e[0m"
}

while getopts "hcf:up:P:b" option "$@"; do
    case "$option" in
        c ) version_check=1 ;;
        f ) source_pack_path="$OPTARG" ;;
        u ) is_uninstall=1 ;;
        P ) go_path="$OPTARG" ;;
        b ) binfmt_misc=1 ;;
        p ) proxy_server_specify="$OPTARG" ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done

#########  1-2 Preparation Operation  #########
fn_InitializationCheck(){
    local l_user_codebase_dir="$HOME/.config/axdsop"
    local l_base_function_temp_save_path=''
    local l_need_remove=0
    local l_base_function_url='https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/base_functions.sh'
    l_base_function_temp_save_path=$(find "${l_user_codebase_dir}" -type f -name "${l_base_function_url##*/}" -print 2> /dev/null)
    
    if [[ ! -f "${l_base_function_temp_save_path}" ]]; then
        local l_download_tool=${l_download_tool:-'wget -qO-'}
        [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_tool='curl -fsL'
        l_need_remove=1
        l_base_function_temp_save_path=$(mktemp -t XXXXXXX)
        ${l_download_tool} "${l_base_function_url}" > "${l_base_function_temp_save_path}"
    fi

    if [[ -f "${l_base_function_temp_save_path}" && -s "${l_base_function_temp_save_path}" ]]; then
        . "${l_base_function_temp_save_path}"
        [[ "${l_need_remove}" -eq 1 ]] && rm "${l_base_function_temp_save_path}"
    else
        echo "Sorry, fail to get base script path."
        exit
    fi

    # tput clear  # Echo the clear-screen sequence for the current terminal.
    # specify 1 means need sudo privilege
    fnBase_RunningEnvironmentCheck '1'

    fnBase_CommandExistCheckPhase 'gzip' # CentOS/Debian/OpenSUSE: gzip
    fnBase_CommandExistCheckPhase 'tar' # .tar.gz

    profile_path="${login_user_home}/.profile"
}


#########  2-1. Uninstall  #########
fn_UninstallOperation(){
    if [[ "${is_uninstall}" -eq 1 ]]; then
        if [[ -d "${installation_dir}" ]]; then
            [[ -d "${installation_dir}" ]] && rm -rf "${installation_dir}"
            [[ -d "${installation_dir}${bak_suffix}" ]] && rm -rf "${installation_dir}${bak_suffix}"
            [[ -f "${gorun_bin_path}" ]] && rm -f "${gorun_bin_path}"

            [[ -f "${profile_path}" ]] && sed -i '/^#Golang Setting/,/^#Golang Setting End/d' "${profile_path}"
            [[ -d "${installation_dir}" ]] || fnBase_ExitStatement "${software_fullname} is successfully removed from your system!"
        else
            fnBase_ExitStatement "${c_blue}Note${c_normal}: no ${software_fullname} is found in your system!"
        fi
    fi
}


#########  2-2. Latest & Local Version Check  #########
fn_VersionComparasion(){
    fnBase_CentralOutputTitle 'The Go Programming Language'
    fnBase_OperationPhaseStatement 'Version Detection'

    # - Local version
    current_local_version=${current_local_version:-}
    fnBase_CommandExistIfCheck 'go' && current_local_version=$(go version 2>&1 | sed -r -n 's@.*go([[:digit:].]+).*$@\1@p')

    if [[ -z "${current_local_version}" && -d "${installation_dir}" ]]; then
        [[ -f "${go_bin_path}" ]] && current_local_version=$("${go_bin_path}" version 2>&1 | sed -r -n 's@.*go([[:digit:].]+).*$@\1@p')
        [[ -z "${current_local_version}" && -f "${installation_dir}/VERSION" ]] && current_local_version=$(sed -r -n 's@^go(.*)$@\1@p' "${installation_dir}/VERSION")
    fi

    [[ -z "${current_local_version}" ]] && is_existed=0
    if [[ "${is_existed}" -eq 1 ]]; then
        fnBase_OperationProcedureStatement 'Local version detecting'
        fnBase_OperationProcedureResult "${current_local_version}"
    fi

    # - Latest online version info
    fnBase_OperationProcedureStatement 'Online version detecting'
    download_page_html=$(mktemp -t "${mktemp_format}")
    $download_method "${pack_download_page}" > "${download_page_html}"

    online_release_version=$(sed -r -n '0,/toggleVisible/s@.*id="go(.*)">@\1@p' "${download_page_html}")
    online_release_date=$($download_method "${release_note_page}" | sed -r -n '/'"${online_release_version}"'.*\(released/{s@.*released[[:space:]]*([^)]*).*$@\1@g;p}' 2> /dev/null | date -f - +"%b %d, %Y" 2> /dev/null)

    if [[ -n "${online_release_version}" ]]; then
        fnBase_OperationProcedureResult "${online_release_version} - ${online_release_date}"
    else
        fnBase_OperationProcedureResult '' 1
    fi

    # - Just online version check
    if [[ "${version_check}" -eq 1 ]]; then
        fnBase_ExitStatement ''

        # if [[ "${is_existed}" -eq 1 ]]; then
        #     fnBase_ExitStatement "Local existed version is ${c_red}${current_local_version}${c_normal}, Latest version online is ${c_red}${online_release_version}${c_normal} (${c_blue}${online_release_date}${c_normal})."
        # else
        #     fnBase_ExitStatement "Latest version online (${c_red}${online_release_version}${c_normal}), Release date ($c_red${online_release_date}$c_normal)."
        # fi
    fi

    # - Online & Local Version Comparasion
    if [[ "${is_existed}" -eq 1 ]]; then
        if [[ "${online_release_version}" == "${current_local_version}" ]]; then
            fnBase_ExitStatement "\nLatest version (${c_red}${online_release_version}${c_normal}) has been existed in your system."
        elif [[ $(fnBase_VersionNumberComparasion "${current_local_version}" "${online_release_version}" '<') -eq 1 ]]; then
            printf "\nExisted version local (${c_red}%s${c_normal}) < Latest version online (${c_red}%s${c_normal}).\n" "${current_local_version}" "${online_release_version}"
        fi
    # else
    #     printf "No %s find in your system!\n" "${software_fullname}"
    fi
}


#########  2-3. Download & Decompress Latest Software  #########
fn_DownloadAndDecompressOperation(){
    local l_operation_type=${l_operation_type:-'installing'}
    [[ "${is_existed}" -eq 1 ]] && l_operation_type='updating'
    fnBase_OperationPhaseStatement "${l_operation_type^}"

    local online_release_pack_info=${online_release_pack_info:-}
    online_release_pack_info=$(sed -r -n '/Stable versions/,${/'"${online_release_version}"'.*'"$os_type"'/,/<\/tr>/{s@.*href="([^"]+)".*$@\1@g;s@[[:space:]]*<[^>]*>[[:space:]]*@@g;/^$/d;p}}' "${download_page_html}" | sed ':a;N;$!ba;s@\n@|@g;')
    # File name|Kind|OS|Arch|Size|SHA256 Checksum
    # https://dl.google.com/go/go1.10.2.linux-amd64.tar.gz|Archive|Linux|x86-64|126MB|4b677d698c65370afa33757b6954ade60347aaca310ea92a63ed717d7cb0c2ff

    [[ -z "${online_release_pack_info}" ]] && fnBase_ExitStatement "${c_red}Sorry${c_normal}: fail to get package download link!"

    online_release_pack_info="${online_release_pack_info%|}"
    local online_release_downloadlink="${online_release_pack_info%%|*}"
    local online_release_dgst="${online_release_pack_info##*|}"

    local online_release_pack_name="${online_release_downloadlink##*/}"

    local l_is_need_download=${l_is_need_download:-1}
    local l_specified_pack_dir=${l_specified_pack_dir:-}

    # specified file path existed
    if [[ "${source_pack_path}" =~ .tar.gz$ && -s "${source_pack_path}" ]]; then
        [[ "${source_pack_path##*/}" == "${online_release_pack_name}" ]] || fnBase_ExitStatement "Package ${c_red}${source_pack_path}${c_normal} is not same to latest release version name!"

    # specified dir or ~/Downloads existed
    elif [[ (-n "${source_pack_path}" && -d "${source_pack_path}" ) || -d "${user_download_dir}" ]]; then
        if [[ -d "${source_pack_path}" ]]; then
            l_specified_pack_dir="${source_pack_path}"
        elif [[ -d "${user_download_dir}" ]]; then
            l_specified_pack_dir="${user_download_dir}"
        fi

        l_specified_pack_dir="${l_specified_pack_dir%/}"

        if [[ -d "${l_specified_pack_dir}" && -f "${l_specified_pack_dir}/${online_release_pack_name}" ]]; then
            source_pack_path="${l_specified_pack_dir}/${online_release_pack_name}"
        fi
    fi

    # 1.1- existed package path in system
    if [[ -n "${source_pack_path}" && -f "${source_pack_path}" ]]; then
        fnBase_OperationProcedureStatement 'Existed pack dgst verifying'

        if [[ -n "${online_release_dgst}" && $(fnBase_HashDigestVerification "${source_pack_path}" '256') == "${online_release_dgst}" ]]; then
            fnBase_OperationProcedureResult "${c_red}${source_pack_path%/*}/${c_normal}"
            l_is_need_download=0
        else
            fnBase_OperationProcedureResult '' 1
        fi
    fi

    # 2 - Download Directly From Official Site
    local pack_save_path=${pack_save_path:-}

    if [[ "${l_is_need_download}" -eq 1 ]]; then
        fnBase_OperationProcedureStatement "Downloading ${c_red}${online_release_pack_name}${c_normal}"
        pack_save_path="${temp_save_dir}/${online_release_pack_name}"
        [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"

        $download_method "${online_release_downloadlink}" > "${pack_save_path}"

        if [[ -s "${pack_save_path}" ]]; then
            fnBase_OperationProcedureResult
        else
            [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
            fnBase_OperationProcedureResult '' 1
        fi

        fnBase_OperationProcedureStatement "SHA256 dgst verification"
        if [[ -n "${online_release_dgst}" && $(fnBase_HashDigestVerification "${pack_save_path}" '256') == "${online_release_dgst}"  ]]; then
            fnBase_OperationProcedureResult
        else
            [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
            fnBase_OperationProcedureResult '' 1
        fi

        # use variable "source_pack_path" to execute decompress operation, variable "pack_save_path" used for delete temporary download package
        source_pack_path="${pack_save_path}"
    fi

    # 3 - Decompress & Extract
    fnBase_OperationProcedureStatement 'Installing'
    local application_backup_path="${installation_dir}${bak_suffix}"
    [[ -d "${application_backup_path}" ]] && rm -rf "${application_backup_path}"

    [[ -d "${installation_dir}" ]] && mv "${installation_dir}" "${application_backup_path}"
    [[ -d "${installation_dir}" ]] || mkdir -p "${installation_dir}"
    tar xf "${source_pack_path}" -C "${installation_dir}" --strip-components=1 2> /dev/null

    local new_installed_version=${new_installed_version:-}
    new_installed_version=$(sed -r -n 's@^go(.*)$@\1@p' "${installation_dir}/VERSION")   # Just Installed Version In System

    if [[ "${online_release_version}" == "${new_installed_version}" ]]; then
        [[ -d "${application_backup_path}" ]] && rm -rf "${application_backup_path}"
        fnBase_OperationProcedureResult "${c_red}${installation_dir%/}/${c_normal}"

        # copy newly downloaded pack to ~/Downloads
        local l_user_download_save_path=${l_user_download_save_path:-"${user_download_dir}/${source_pack_path##*/}"}
        if [[ -d "${user_download_dir}" && ! -f "${l_user_download_save_path}" ]]; then
            cp "${source_pack_path}" "${l_user_download_save_path}"
            chown "${login_user}" "${l_user_download_save_path}"
            chgrp -hR "${login_user}" "${l_user_download_save_path}" 2> /dev/null

            # removed old version package existed
            if [[ "${is_existed}" -eq 1 ]]; then
                local l_existed_old_pack_path=${l_existed_old_pack_path:-"${user_download_dir}/${online_release_pack_name//${online_release_version}/${current_local_version}}"}
                [[ -f "${l_existed_old_pack_path}" ]] && rm -f "${l_existed_old_pack_path}"
            fi
        fi

    else
        [[ -d "${installation_dir}" ]] && rm -rf "${installation_dir}"
        [[ "${is_existed}" -eq 1 ]] && mv "${application_backup_path}" "${installation_dir}"
        fnBase_OperationProcedureResult '' 1
    fi

    chown -R root:root "${installation_dir}"
    [[ -n "${pack_save_path}" && -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
}


#########  2-4. Profile Configuration  #########
fn_ProfileConfiguration(){
    fnBase_OperationPhaseStatement 'Configuration'

    local l_profile_parse=${l_profile_parse:-'Golang Setting'}
    [[ -f "${profile_path}" ]] && sed -i '/^#'"${l_profile_parse}"' Start/,/^#'"${l_profile_parse}"' End/d' "${profile_path}"

    # - GOPATH
    fnBase_OperationProcedureStatement 'GOPATH'
    [[ -n "${go_path}" ]] || go_path="${login_user_home}/${application_name}"
    [[ -d "${go_path}" ]] || mkdir -p "${go_path}"
    chown -R "${login_user}" "${go_path}"
    chgrp -R "${login_user}" "${go_path}" &> /dev/null
    fnBase_OperationProcedureResult "${go_path}"

    # - Profile
    fnBase_OperationProcedureStatement 'GOROOT Profile'
    # GOROOT must be set only when installing to a custom location. /etc/profile or ~/.profile
    echo -e "#${l_profile_parse} Start\nexport GOROOT=installation_dir\nexport GOPATH=go_path\nexport PATH=\$PATH:installation_dir/bin:go_path/bin\n#${l_profile_parse} End" >> "${profile_path}"

    sed -i -r 's@installation_dir@'"${installation_dir}"'@g' "${profile_path}"

    sed -i -r 's@go_path@'"${go_path}"'@g' "${profile_path}"
    fnBase_OperationProcedureResult "${profile_path}"
}


#########  2-5. Mount binfmt_misc Filesystem  #########
fn_BinfmtMiscFileSystem(){
    # https://blog.cloudflare.com/using-go-as-a-scripting-language-in-linux/
    if [[ "${binfmt_misc}" -eq 1 ]]; then
        fnBase_OperationPhaseStatement 'gorun wrapper script'

        # Kernel version >= 4.14.x
        local current_kernel_version=''
        current_kernel_version=$(uname -r)
        [[ -n "${current_kernel_version}" ]] && current_kernel_version="${current_kernel_version%%-*}"

        if [[ -n "${current_kernel_version}" && $(fnBase_VersionNumberComparasion "${current_kernel_version}" '4.14' '>') -eq 1 ]]; then
            fnBase_OperationProcedureStatement 'binfmt_misc'
            # mount binfmt_misc, default is actived through systemd
            if [[ $(mount | sed -r -n '/binfmt_misc/{p}' | wc -l) -eq 0 ]]; then
                mount binfmt_misc -t binfmt_misc /proc/sys/fs/binfmt_misc 2> /dev/null
                fnBase_OperationProcedureResult '/proc/sys/fs/binfmt_misc'
            else
                fnBase_OperationProcedureResult 'mounted'
            fi

            # get gorun and place it so it will work
            fnBase_OperationProcedureStatement 'gorun install'
            local l_gorun_bin_path="${go_path}/bin/gorun"

            local l_use_sudo=${l_use_sudo:-''}
            [[ "${login_user}" == 'root' ]] || l_use_sudo='sudo'
            [[ -f "${l_gorun_bin_path}" && -s "${l_gorun_bin_path}" ]] || ${l_use_sudo} bash -c '(source '"${profile_path}"'; '${go_bin_path}' get github.com/erning/gorun 2> /dev/null)'

            if [[ -f "${l_gorun_bin_path}" ]]; then
                [[ -d "${gorun_bin_path%/*}" ]] || mkdir -p "${gorun_bin_path%/*}"
                ln -fs "${l_gorun_bin_path}" "${gorun_bin_path}"
                fnBase_OperationProcedureResult "${gorun_bin_path}"
            else
                fnBase_OperationProcedureResult '' 1
            fi

            # make .go scripts being run with `gorun` wrapper script
            fnBase_OperationProcedureStatement 'wrapper script'

            local l_binfmt_misc_register='/proc/sys/fs/binfmt_misc/register'
            if [[ -f "${l_binfmt_misc_register}" ]]; then
                echo ':golang:E::go::/usr/local/bin/gorun:OC' | tee "${l_binfmt_misc_register}" &> /dev/null
                fnBase_OperationProcedureResult "${l_binfmt_misc_register}"
            else
                fnBase_OperationProcedureResult '' 1
            fi

        fi
    fi
}

#########  2-6. Operation Time Cost  #########
fn_TotalTimeCosting(){
    finish_time=$(date +'%s')        # End Time Of Operation
    total_time_cost=$((finish_time-start_time))   # Total Time Of Operation
    printf "\nTotal time cost is ${c_red}%s${c_normal} seconds!\n" "${total_time_cost}"
    printf "\n${c_bold}${c_blue}%s${c_normal}: You need to relogin to make bash profile effect!\n" 'Notice'
}


#########  3. Executing Process  #########
fn_InitializationCheck

fn_UninstallOperation
fn_VersionComparasion
fn_DownloadAndDecompressOperation
fn_ProfileConfiguration
fn_BinfmtMiscFileSystem
fn_TotalTimeCosting



#########  4. EXIT Singal Processing  #########
# trap "commands" EXIT # execute command when exit from shell
fn_TrapEXIT(){
    fnBase_UnsetVars
    unset installation_dir
    unset source_pack_path
    unset go_path
    unset is_existed
    unset binfmt_misc
    unset version_check
    unset is_uninstall
    unset proxy_server_specify
    unset download_method
}

trap fn_TrapEXIT EXIT

# Script End
