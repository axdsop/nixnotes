#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails


# Important Oracle JDK License Update
# - The Oracle JDK License has changed for releases starting April 16, 2019.
# - Commercial license and support is available with a low cost Java SE Subscription.
# - Oracle also provides the latest OpenJDK release under the open source GPL License at jdk.java.net.
# - https://www.oracle.com/technetwork/java/javase/terms/license/javase-license.html


# Official Site:
# - https://www.oracle.com/technetwork/java/index.html
# - https://jdk.java.net/
# Installation:
# - https://www.oracle.com/technetwork/java/javase/downloads/index.html
# Documentation:
# - https://docs.oracle.com/javase
# - https://docs.oracle.com/en/java/javase/12/install/installation-jdk-linux-platforms.html

# Arch Linux
# https://wiki.archlinux.org/index.php/java
# Arch Linux officially supports the open source OpenJDK versions 7, 8, 10, 11, and 12. All these JVM can be installed without conflict and switched between using helper script 'archlinux-java'. Several other Java environments are available in AUR but are not officially supported.


#####################################################
#       Java Development Kit (JDK)                  #
#       Java Runtime Environment (JRE)              #
#       Java SE Runtime Environment (Server JRE)    #
#####################################################


# Target: Automatically Install & Configuring Oracle SE (Standard Edition)
# Developer: MaxdSre

# Change Log:
# - Oct 08, 2020 08:59 Thu ET - initialization check method update
# - Feb 16, 2020 10:25 Sun ET - HTML UI redesigned, stop maintain this scirpt.
# - Oct 09, 2019 09:18 Wed ET - change to new base functions
# - Sep 18, 2019 12:25 Wed ET - add support for JDK 13, get download url has parameter 'AuthParam'
# - Jun 21, 2019 13:10 Fri ET - add support for Arch Linux with archlinux-java
# - Mar 25, 2019 17:10 Mon ET - add support for Java 12, update http to https
# - Dec 31, 2018 20:58 Mon ET - code reconfiguration, include base funcions from other file
# - Nov 29, 2018 10:29 Thu ET - code optimization
# - Sep 26, 2018 14:43 Wed +0800 - add support for Java 11
# - Jul 26, 2018 11:34 Thu ET - add wget support for jdk download
# - Jun 01, 2018 12:19 Fri ET - code reconfiguration
# - Apr 6, 2018 20:35 Fri ET - fix release version info extraction problem as release page changed (add JDK 10), code optimization
# - Mar 06, 2018 17:45 Tue ET - auto find package in dir ~/Download/
# - Oct 30, 2017 23:53 Sat +0800 - add alternative mode, change output style
# - Oct 19, 2017 12:36 Thu +0800
# - Sep 22, 2017 16:59 Fri +0800
# - Aug 18, 2017 13:41 Fri +0800



#########  0-1. Singal Setting  #########
trap fnBase_TrapCleanUp INT QUIT

#########  0-2. Variables Setting  #########
# umask 022    # temporarily change umask value to 022
oracle_official_site=${oracle_official_site:-'https://www.oracle.com'}
# http://www.oracle.com/technetwork/java/javase/downloads/index.html
# https://www.oracle.com/java/technologies/javase-downloads.html
oracle_java_se_page=${oracle_java_se_page:-"${oracle_official_site}/technetwork/java/javase/downloads/index.html"}

readonly software_fullname=${software_fullname:-'Oracle JDK'}
readonly application_name=${application_name:-'OracleSEJDK'}
installation_dir="/opt/${application_name}"

oracle_jdk_include_path=${oracle_jdk_include_path:-'/usr/local/include/oracle_se_jdk'}
oracle_jdk_lib_path=${oracle_jdk_lib_path:-'/etc/ld.so.conf.d/oracle_se_jdk.conf'}
oracle_jdk_execute_path=${oracle_jdk_execute_path:-'/etc/profile.d/oracle_se_jdk.sh'}

is_existed=${is_existed:-1}      # Default value is 1， assume system has installed Oracle JDK
version_check=${version_check:-0}
version_specify=${version_specify:-}
source_pack_path=${source_pack_path:-}
alternative_mode=${alternative_mode:-0}
is_uninstall=${is_uninstall:-0}
proxy_server_specify=${proxy_server_specify:-}
alternative_command=${alternative_command:-}


#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
script [options] ...
script | sudo bash -s -- [options] ...

Installing / Configuring Oracle SE JDK On GNU/Linux!
This script requires superuser privileges (eg. root, su).

Attention: The Oracle JDK License has changed for releases starting April 16, 2019.
You need to login in to download JDK 8, JDK 11. This script still supports JDK12, JDK 13 download. (Stop maintaining this script on Feb 16, 2020 Sun.)

Oracle JDK License: https://www.oracle.com/downloads/licenses/javase-license1.html

[available option]
    -h    --help, show help info
    -c    --check, check Oracle JDK installed or not
    -f pack_path    --specify latest release package save path (e.g. /PATH/*.tar.gz) or dir (default is ~/Downloads/) in system
    -v version    --specify specific Oracle JDK version (e.g. 12, 13)
    -A    --using alternatives mode (alternatives/update-alternatives) if exists, default is using environment variables '\$PATH'
    -p [protocol:]ip:port    --proxy host (http|https|socks4|socks5), default protocol is http
    -u    --uninstall, uninstall software installed
\e[0m"
# -t pack_type    --choose packge type (jdk/jre/server jre), default is jdk
}

while getopts "hcf:p:v:Au" option "$@"; do
    case "$option" in
        c ) version_check=1 ;;
        f ) source_pack_path="$OPTARG" ;;
        u ) is_uninstall=1 ;;
        v ) version_specify="$OPTARG" ;;
        A ) alternative_mode=1 ;;
        p ) proxy_server_specify="$OPTARG" ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done


#########  1-2 Preparation Operation  #########
fn_InitializationCheck(){
    local l_user_codebase_dir="$HOME/.config/axdsop"
    local l_base_function_temp_save_path=''
    local l_need_remove=0
    local l_base_function_url='https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/base_functions.sh'
    l_base_function_temp_save_path=$(find "${l_user_codebase_dir}" -type f -name "${l_base_function_url##*/}" -print 2> /dev/null)
    
    if [[ ! -f "${l_base_function_temp_save_path}" ]]; then
        local l_download_tool=${l_download_tool:-'wget -qO-'}
        [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_tool='curl -fsL'
        l_need_remove=1
        l_base_function_temp_save_path=$(mktemp -t XXXXXXX)
        ${l_download_tool} "${l_base_function_url}" > "${l_base_function_temp_save_path}"
    fi

    if [[ -f "${l_base_function_temp_save_path}" && -s "${l_base_function_temp_save_path}" ]]; then
        . "${l_base_function_temp_save_path}"
        [[ "${l_need_remove}" -eq 1 ]] && rm "${l_base_function_temp_save_path}"
    else
        echo "Sorry, fail to get base script path."
        exit
    fi

    # tput clear  # Echo the clear-screen sequence for the current terminal.
    # specify 1 means need sudo privilege
    fnBase_RunningEnvironmentCheck '1'

    # fnBase_CommandExistCheckPhase 'gawk'
    fnBase_CommandExistCheckPhase 'gzip' # CentOS/Debian/OpenSUSE: gzip
    fnBase_CommandExistCheckPhase 'tar' # .tar.gz

    # shellcheck source=/dev/null
    [[ -f "${oracle_jdk_execute_path}" ]] && source "${oracle_jdk_execute_path}"

    # Arch Linux alternative tool 'archlinux-java'
    if [[ -f /etc/arch-release && -n $(sed -r -n '/^NAME=/{/Arch/{p}}' /etc/os-release 2> /dev/null) ]]; then
        # https://wiki.archlinux.org/index.php/java
        # package java-runtime-common provides this script, openjdk install path is /usr/lib/jvm/
        if fnBase_CommandExistIfCheck 'archlinux-java'; then
            # change function 'get_installed_javas' in file /usr/bin/archlinux-java
            # - origin:  $(find ${JVM_DIR} -mindepth 1 -maxdepth 1 -type d | sort)
            # - new:     $(find ${JVM_DIR} -mindepth 1 -maxdepth 1 -type d -o -type l | grep -v default | sort)
            local archlinux_java_script_bin_path=${archlinux_java_script_bin_path:-'/usr/bin/archlinux-java'}
            archlinux_java_script_bin_path=$(which archlinux-java 2> /dev/null || command -v archlinux-java 2> /dev/null)
            [[ -f "${archlinux_java_script_bin_path}" ]] && sed -r -i '/^get_installed_javas/,/^}/{/find.*depth/{s@(.*-type d)[^\|]+.*?(\|[[:space:]]*sort.*$)@\1 -o -type l | grep -v default \2@g;}}' "${archlinux_java_script_bin_path}"
        else
            # [[ "${alternative_mode}" -eq 1 ]]
            fnBase_ExitStatement "${c_red}Sorry${c_normal}, fail to find command ${c_yellow}archlinux-java${c_normal} in your ${c_yellow}Arch Linux${c_normal}, please install package ${c_red}${c_bold}java-runtime-common${c_normal} first."
        fi

        # get_installed_javas() {
        #   if [ -d ${JVM_DIR} ]; then
        #     for dir in $(find ${JVM_DIR} -mindepth 1 -maxdepth 1 -type d -o -type l | grep -v default | sort); do
        #     #for dir in $(find ${JVM_DIR} -mindepth 1 -maxdepth 1 -type d | sort); do
        #       if [ -x ${dir}/bin/java ]; then
        #         javas+=(${dir/${JVM_DIR}\/})
        #       else
        #         if [ -x ${dir}/jre/bin/java ]; then
        #         javas+=(${dir/${JVM_DIR}\/}/jre)
        #         fi
        #       fi
        #     done
        #   fi
        #   echo ${javas[@]}
        # }


        # create soft link to dir /usr/lib/jvm
        # ln -fs /opt/OracleSEJDK /usr/lib/jvm
    fi

    # alternatives / update-alternatives / archlinux-java
    if fnBase_CommandExistIfCheck 'alternatives'; then
        alternative_command='alternatives'
    elif fnBase_CommandExistIfCheck 'update-alternatives'; then
        alternative_command='update-alternatives'
    elif fnBase_CommandExistIfCheck 'archlinux-java'; then
        alternative_command='archlinux-java'
    fi

    if [[ "${alternative_mode}" -eq 1 ]]; then
        [[ -n "${alternative_command}" ]] || alternative_mode=0
    fi
}

fn_AlternativeModeConfiguration(){
    local l_command="${1:-}"
    local l_action="${2:-'install'}"
    local l_dir="${3:-"${installation_dir}"}"

    if [[ -n "${l_dir}" && -d "${l_dir}" && -n "${alternative_command}" ]]; then
        if [[ "${alternative_command}" != 'archlinux-java' ]]; then
            local l_command_path="${l_dir}/bin/${l_command}"

            case "${l_action}" in
                install )
                    if [[ -s "${l_command_path}" ]]; then
                        ${alternative_command} --install /usr/bin/"${l_command}" "${l_command}" "${l_command_path}" 99 &> /dev/null
                        ${alternative_command} --set "${l_command}" "${l_command_path}" &> /dev/null
                    fi
                    ;;
                remove|* )
                    if [[ -s "${l_command_path}" ]]; then
                        ${alternative_command} --remove "${l_command}" "${l_command_path}" &> /dev/null
                    fi
                    ;;
            esac
        else
            # archlinux-java <COMMAND>
            # COMMAND:
            # 	status		List installed Java environments and enabled one
            # 	get		Return the short name of the Java environment set as default
            # 	set <JAVA_ENV>	Force <JAVA_ENV> as default
            # 	unset		Unset current default Java environment
            # 	fix		Fix an invalid/broken default Java environment configuration
            case "${l_action}" in
                install )
                    ${alternative_command} set "${l_dir##*/}" 2> /dev/null
                    ;;
                remove|* )
                    ${alternative_command} unset 2> /dev/null
                    ${alternative_command} fix 2> /dev/null
                    ;;
            esac

        fi

    fi
}


#########  2-1. Uninstall  #########
fn_UninstallOperation(){
    if [[ "${is_uninstall}" -eq 1 ]]; then
        if [[ -d "${installation_dir}" ]]; then
            fn_AlternativeModeConfiguration 'java' 'remove'
            fn_AlternativeModeConfiguration 'javac' 'remove'
            fn_AlternativeModeConfiguration 'jar' 'remove'
            [[ -f "${oracle_jdk_include_path}" ]] && rm -f "${oracle_jdk_include_path}"
            [[ -f "${oracle_jdk_lib_path}" ]] && rm -f "${oracle_jdk_lib_path}"
            [[ -f "${oracle_jdk_execute_path}" ]] && rm -f "${oracle_jdk_execute_path}"
            rm -rf "${installation_dir}"

            if [[ "${alternative_command}" == 'archlinux-java' && -d /usr/lib/jvm ]]; then
                rm -f /usr/lib/jvm/"${installation_dir##*/}"
            fi
        else
            fnBase_ExitStatement "${c_red}Attention${c_normal}: No ${software_fullname} finds in your system!"
        fi

        [[ -d "${installation_dir}" ]] || fnBase_ExitStatement "${software_fullname} is successfully removed from your system!"
    fi
}

#########  2-2. Latest & Local Version Check  #########
fn_VersionComparasion(){
    fnBase_CentralOutputTitle "${software_fullname}"
    fnBase_OperationPhaseStatement 'Version Detection'

    # - Local version
    current_local_version=${current_local_version:-}
    # 10 / 9.0.4 / 8u162 / 8u161
    # Java 8 just has '-version', Java 9, 10 also has '--version'

    if [[ -d "${installation_dir}" ]]; then
        if [[ -s "${installation_dir}/relese" ]]; then
            current_local_version=$(sed -r -n '/^JAVA_VERSION=/{s@[^"]*"([^"]*)".*@\1@g;p}' "${installation_dir}/relese")
        elif [[ -s "${installation_dir}/bin/java" ]]; then
            current_local_version=$("${installation_dir}/bin/java" -version 2>&1 | sed -r -n '/version/{s@^[^"]+"([^"]+)".*$@\1@g;p}')
        else
            is_existed=0
        fi

        [[ -z "${current_local_version}" ]] && is_existed=0
    elif fnBase_CommandExistIfCheck 'java'; then
        is_existed=0
        #openjdk 1.8.0_141
        current_local_version=$(java -version |& sed -r -n '1{s@([^[:space:]]*)[[:space:]]*version[[:space:]]*"([^"]*)"@\1 \2@g;p}')
    else
        is_existed=0
    fi

    if [[ "${is_existed}" -eq 1 ]]; then
        fnBase_OperationProcedureStatement "Local version detecting"
        fnBase_OperationProcedureResult "${current_local_version}"
    else
        if [[ -n "${current_local_version}" ]]; then
            fnBase_OperationProcedureStatement "Existed Java version"
            fnBase_OperationProcedureResult "${current_local_version}"
        else
            if [[ "${is_uninstall}" -eq 1 ]]; then
                fnBase_ExitStatement "No Java Environment finds in your system!"
            # else
            #     echo -e "No Oracle Java Environment finds in your system!\n"
            fi
        fi
    fi

    # - Latest online version info
    fnBase_OperationProcedureStatement 'Online version detecting'
    online_release_pack_info=$(mktemp -t "${mktemp_format}")
    # empty file content
    [[ -s "${online_release_pack_info}" ]] && > "${online_release_pack_info}"

    # JDK11|/technetwork/java/javase/downloads/jdk11-downloads-5066655.html|Java SE 11 (LTS)
    # JDK10|/technetwork/java/javase/downloads/jdk10-downloads-4416644.html|Java SE 10.0.2
    # JDK8|/technetwork/java/javase/downloads/jdk8-downloads-2133151.html|Java SE 8u181

    # JDK11|/technetwork/java/javase/downloads/jdk11-downloads-5066655.html|Java SE 11.0.1(LTS)
    # JDK8|/technetwork/java/javase/downloads/jdk8-downloads-2133151.html|Java SE 8u191 / Java SE 8u192

    # JDK12|/technetwork/java/javase/downloads/jdk12-downloads-5295953.html|Java SE 12
    # JDK11|/technetwork/java/javase/downloads/jdk11-downloads-5066655.html|Java SE 11.0.2 (LTS)
    # JDK8|/technetwork/java/javase/downloads/jdk8-downloads-2133151.html|Java SE 8u201 / Java SE 8u202

    # sed -r -n '/JDK[[:space:]]*[[:digit:]]+/{s@<\/[^a>]*>@\n&@g;p}' | sed -r -n '/<a[[:space:]]+name=/!d;/href=/!d;s@.*<a[[:space:]]*name="([^"]+)"[[:space:]]*href="([^"]+)">([^<]+)<.*@\1|\2|\3@g;p'
    $download_method "${oracle_java_se_page}" | sed -r -n 's@<\/[^>]+>@&\n@g;p' | sed -r -n '/JDK[[:space:]]*[[:digit:]]+/{s@[[:space:]]*(<[^>]*>)[[:space:]]*@\1@g;s@<\/[^a>]*>@\n&@g;p}' | sed -r -n '/<a[[:space:]]+name.*href=/{s@.*<a[[:space:]]+name="([^"]+)"[[:space:]]+href="([^"]+)"[^>]*>([^<]+)<.*$@\1|\2|\3@g;p}' | while IFS="|" read -r jdk_type download_page_url version_info; do
        major_version="${jdk_type//JDK/}"
        # https://www.oracle.com/technetwork/java/javase/downloads/jdk12-downloads-5295953.html
        # https://www.oracle.com/technetwork/java/javase/downloads/jdk11-downloads-5066655.html
        # https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
        [[ "${download_page_url}" =~ ^/ ]] && download_page_url="${oracle_official_site}${download_page_url}"

        # remove prefix "Java SE ", suffix " (LTS)"
        pattern_list=${pattern_list:-}
        pattern_list=$(echo "${version_info}" | sed -r 's@^[^[:digit:]]*@@g;;s@(\/)[^[:digit:]]*@\1@g;s@[[:space:]]*\(.*$@@g;')

        # 8u161/ 8u162 ==> 8u161|8u162
        # 8u191 /8u192 ==> 8u191|8u192
        # 8u201 /8u202 ==> 8u201|8u202
        [[ "${pattern_list}" =~ / ]] && pattern_list=$(echo "${pattern_list}" | sed -r -n 's@[[:space:]]*@@g;s@\/@|@g;p')

        # title | size | filepath |sha256

        # Linux|171.43 MB|https://download.oracle.com/otn-pub/java/jdk/11.0.1+13/90cf5d8f270a4347a95050320eef3fb7/jdk-11.0.1_linux-x64_bin.tar.gz|e7fd856bacad04b6dbf3606094b6a81fa9930d6dbb044bbd787be7ea93abc885
        # Linux x64|182.87 MB|https://download.oracle.com/otn-pub/java/jdk/8u192-b12/750e1c8617c5452694857ad95c3ee230/jdk-8u192-linux-x64.tar.gz|6d34ae147fc5564c07b913b467de1411c795e290356538f22502f28b76a323c2
        # Linux x64|182.87 MB|https://download.oracle.com/otn-pub/java/jdk/8u191-b12/2787e4a523244c269598db4e85c51e0c/jdk-8u191-linux-x64.tar.gz|53c29507e2405a7ffdbba627e6d64856089b094867479edc5ede4105c1da0d65

        # Linux|181.21 MB|https://download.oracle.com/otn-pub/java/jdk/12+33/312335d836a34c7c8bba9d963e26dc23/jdk-12_linux-x64_bin.tar.gz|183d4d897bbf47bdae43b2bb4fc0465a9178209b5250555ac7fdb8fab6cc43a6
        # https://www.oracle.com/technetwork/java/javase/downloads/jdk11-downloads-5066655.html
        # Linux|171.32 MB|http://download.oracle.com/otn-pub/java/jdk/11.0.2+9/f51449fcd52f4d52b93a989c5c56ed3c/jdk-11.0.2_linux-x64_bin.tar.gz|7b4fd8ffcf53e9ff699d964a80e4abf9706b5bdb5644a765c2b96f99e3a2cdc8
        # https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
        # Linux x64|185.05 MB|https://download.oracle.com/otn-pub/java/jdk/8u202-b08/1961070e4c9b4e26a04e7f5a083f551e/jdk-8u202-linux-x64.tar.gz|9a5c32411a6a06e22b69c495b7975034409fa1652d03aeb8eb5b6f59fd4594e0
        # Linux x64|182.93 MB|https://download.oracle.com/otn-pub/java/jdk/8u201-b09/42970487e3af4f5aa5bca3f542482c60/jdk-8u201-linux-x64.tar.gz|cb700cc0ac3ddc728a567c350881ce7e25118eaf7ca97ca9705d4580c506e370
        # 12|12|https://www.oracle.com/technetwork/java/javase/downloads/jdk12-downloads-5295953.html|jdk-12_linux-x64_bin.tar.gz|https://download.oracle.com/otn-pub/java/jdk/12+33/312335d836a34c7c8bba9d963e26dc23/jdk-12_linux-x64_bin.tar.gz|183d4d897bbf47bdae43b2bb4fc0465a9178209b5250555ac7fdb8fab6cc43a6
        # 11|11.0.2|https://www.oracle.com/technetwork/java/javase/downloads/jdk11-downloads-5066655.html|jdk-11.0.2_linux-x64_bin.tar.gz|http://download.oracle.com/otn-pub/java/jdk/11.0.2+9/f51449fcd52f4d52b93a989c5c56ed3c/jdk-11.0.2_linux-x64_bin.tar.gz|7b4fd8ffcf53e9ff699d964a80e4abf9706b5bdb5644a765c2b96f99e3a2cdc8
        # 8|8u202|https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html|jdk-8u202-linux-x64.tar.gz|https://download.oracle.com/otn-pub/java/jdk/8u202-b08/1961070e4c9b4e26a04e7f5a083f551e/jdk-8u202-linux-x64.tar.gz|9a5c32411a6a06e22b69c495b7975034409fa1652d03aeb8eb5b6f59fd4594e0
        # 8|8u201|https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html|jdk-8u201-linux-x64.tar.gz|https://download.oracle.com/otn-pub/java/jdk/8u201-b09/42970487e3af4f5aa5bca3f542482c60/jdk-8u201-linux-x64.tar.gz|cb700cc0ac3ddc728a567c350881ce7e25118eaf7ca97ca9705d4580c506e370

        # shellcheck disable=SC2034
        $download_method "${download_page_url}" | sed -r -n '/linux-x64.*tar.gz/{/demos?/d;/('"${pattern_list}"')/{s@.*=@@g;s@\{@@g;s@,@\n@g;s@\};?@\n---@g;p}}' | sed -r '/MD5/d;s@[^:]+:[^"]*"([^"]*)".*@\1@g;s@\n@|@g;$d' | sed -r ':a;N;$!ba;s@\n@|@g;s@\|?---\|?@\n@g;' | sort -b -t"|" -k 3r,3 | while IFS="|" read -r title size filepath sha256_dgst; do
            local l_pack_name=${l_pack_name:-}
            l_pack_name="${filepath##*/}"
            local l_version_name=${l_version_name:-}
            l_version_name=$(echo "${l_pack_name}" | sed -r 's@_@-@g;s@jdk-([^-]+).*@\1@g')

            # major_version|version_name|download_page_url|pack_name|pack_download_link|pack_sha256_dgst

            # 11|11.0.1|http://www.oracle.com/technetwork/java/javase/downloads/jdk11-downloads-5066655.html|jdk-11.0.1_linux-x64_bin.tar.gz|https://download.oracle.com/otn-pub/java/jdk/11.0.1+13/90cf5d8f270a4347a95050320eef3fb7/jdk-11.0.1_linux-x64_bin.tar.gz|e7fd856bacad04b6dbf3606094b6a81fa9930d6dbb044bbd787be7ea93abc885
            # 8|8u192|http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html|jdk-8u192-linux-x64.tar.gz|https://download.oracle.com/otn-pub/java/jdk/8u192-b12/750e1c8617c5452694857ad95c3ee230/jdk-8u192-linux-x64.tar.gz|6d34ae147fc5564c07b913b467de1411c795e290356538f22502f28b76a323c2
            # 8|8u191|http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html|jdk-8u191-linux-x64.tar.gz|https://download.oracle.com/otn-pub/java/jdk/8u191-b12/2787e4a523244c269598db4e85c51e0c/jdk-8u191-linux-x64.tar.gz|53c29507e2405a7ffdbba627e6d64856089b094867479edc5ede4105c1da0d65

            # 12|12|https://www.oracle.com/technetwork/java/javase/downloads/jdk12-downloads-5295953.html|jdk-12_linux-x64_bin.tar.gz|https://download.oracle.com/otn-pub/java/jdk/12+33/312335d836a34c7c8bba9d963e26dc23/jdk-12_linux-x64_bin.tar.gz|183d4d897bbf47bdae43b2bb4fc0465a9178209b5250555ac7fdb8fab6cc43a6
            # 11|11.0.2|https://www.oracle.com/technetwork/java/javase/downloads/jdk11-downloads-5066655.html|jdk-11.0.2_linux-x64_bin.tar.gz|http://download.oracle.com/otn-pub/java/jdk/11.0.2+9/f51449fcd52f4d52b93a989c5c56ed3c/jdk-11.0.2_linux-x64_bin.tar.gz|7b4fd8ffcf53e9ff699d964a80e4abf9706b5bdb5644a765c2b96f99e3a2cdc8
            # 8|8u202|https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html|jdk-8u202-linux-x64.tar.gz|https://download.oracle.com/otn-pub/java/jdk/8u202-b08/1961070e4c9b4e26a04e7f5a083f551e/jdk-8u202-linux-x64.tar.gz|9a5c32411a6a06e22b69c495b7975034409fa1652d03aeb8eb5b6f59fd4594e0
            # 8|8u201|https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html|jdk-8u201-linux-x64.tar.gz|https://download.oracle.com/otn-pub/java/jdk/8u201-b09/42970487e3af4f5aa5bca3f542482c60/jdk-8u201-linux-x64.tar.gz|cb700cc0ac3ddc728a567c350881ce7e25118eaf7ca97ca9705d4580c506e370
            echo "${major_version}|${l_version_name}|${download_page_url}|${l_pack_name}|${filepath}|${sha256_dgst}" >> "${online_release_pack_info}"
        done
    done

    [[ -s "${online_release_pack_info}" ]] || fnBase_ExitStatement "${c_red}Sorry${c_normal}: fail to extract release version info from official page!"

    local l_version_list=''
    l_version_list=$(cut -d\| -f2 "${online_release_pack_info}" | sed ':a;N;$!ba;s@\n@, @g;')

    if [[ -n "${l_version_list}" ]]; then
        fnBase_OperationProcedureResult "${l_version_list}"
    else
        fnBase_OperationProcedureResult '' 1
    fi

    # - Just online version check
    if [[ "${version_check}" -eq 1 ]]; then
        if [[ "${is_existed}" -eq 1 ]]; then
            echo -e "\n${c_bold}${c_yellow}Local Version Detail Info${c_normal}:\n"
            java -version
        fi
        fnBase_ExitStatement ''
    fi

    # - Online & Local Version Comparasion
    if [[ "${is_existed}" -eq 1 && -n "${current_local_version}" ]]; then
        # 9 / 1.8.0_202 ==> 8u202
        # awk -F\| 'match($2,/'"${current_local_version//1.8.0_/8u}"'/){print $1}' "${online_release_pack_info}"
        if [[ -z $(sed -r -n '/\|'"${current_local_version//1.8.0_/8u}"'\|/{s@^([^\|]+).*@\1@g;p}'  "${online_release_pack_info}") ]]; then
            fnBase_ExitStatement "\n${c_red}Attention${c_normal}: local version ${c_yellow}${current_local_version}${c_normal} is not the latest release version!\nStrongly recommend uninstall existed version via ${c_yellow}-u${c_normal}, then reinstall newer version via this script!"
        else
            fnBase_ExitStatement "\nLatest release version has been existed in your system!"
        fi
    fi
}


#########  2-3. Version List Selection Menu  #########
# global variable - choose_version
fn_VersionSelectionMenu(){
    local IFS_BAK=${IFS_BAK:-"$IFS"}  # Backup IFS
    IFS="|" # Setting temporary IFS

    echo -e "\n${c_bold}${c_blue}Available Product Version List: ${c_normal}"
    PS3="${c_yellow}Choose version number(e.g. 1, 2,...): ${c_normal}"

    choose_version=${choose_version:-}
    # awk -F\| 'BEGIN{ORS="|"}{print $2}'
    select item in $(cut -d\| -f2 "${online_release_pack_info}" | sed ':a;N;$!ba;s@\n@|@g'); do
        choose_version="${item}"
        [[ -n "${choose_version}" ]] && break
    done < /dev/tty

    IFS=${IFS_BAK}  # Restore IFS
    unset IFS_BAK
    unset PS3
    printf "\nProduct version you choose is ${c_bold}${c_yellow}%s${c_normal}.\n\n" "${choose_version}"
}


#########  2-3. Download & Decompress Latest Software  #########
fn_DownloadAndDecompressOperation(){
    local l_operation_type=${l_operation_type:-'installing'}
    [[ "${is_existed}" -eq 1 ]] && l_operation_type='updating'
    fnBase_OperationPhaseStatement "${l_operation_type^}"
    # major_version|version_name|download_page_url|pack_name|pack_download_link|pack_sha256_dgst
    # 8|8u202|https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html|jdk-8u202-linux-x64.tar.gz|https://download.oracle.com/otn-pub/java/jdk/8u202-b08/1961070e4c9b4e26a04e7f5a083f551e/jdk-8u202-linux-x64.tar.gz|9a5c32411a6a06e22b69c495b7975034409fa1652d03aeb8eb5b6f59fd4594e0

    local online_release_downloadlink=${online_release_downloadlink:-}
    local online_release_dgst=${online_release_dgst:-}
    local online_release_pack_name
    # online_release_pack_name="${online_release_downloadlink##*/}"

    local l_is_need_download=${l_is_need_download:-1}
    local l_specified_pack_dir=${l_specified_pack_dir:-}

    # if version_specify not available, set its value to empty ''
    [[ "${version_specify}" =~ ^[1-9] && -n $(sed -r -n '/^'"${version_specify}"'\|/{p}' "${online_release_pack_info}") ]] || version_specify=''

    # - specified file path existed
    if [[ "${source_pack_path}" =~ .tar.gz$ && -s "${source_pack_path}" ]]; then
        [[ -z $(sed -r -n '/\|'"${source_pack_path##*/}"'\|/{s@^([^\|]+).*@\1@g;p}' "${online_release_pack_info}" 2> /dev/null) ]] && fnBase_ExitStatement "Package ${c_red}${source_pack_path}${c_normal} is not same to its origin name!"

    # - specified dir or ~/Downloads existed
    elif [[ (-n "${source_pack_path}" && -d "${source_pack_path}" ) || -d "${user_download_dir}" ]]; then
        if [[ -d "${source_pack_path}" ]]; then
            l_specified_pack_dir="${source_pack_path}"
        elif [[ -d "${user_download_dir}" ]]; then
            l_specified_pack_dir="${user_download_dir}"
        fi

        l_specified_pack_dir="${l_specified_pack_dir%/}"

        local l_release_pack_name=${l_release_pack_name:-}
        [[ -n "${version_specify}" ]] && l_release_pack_name=$(sed -r -n '/^'"${version_specify}"'\|/{p;q}' "${online_release_pack_info}" | cut -d\| -f4)

        # extract first matched package name existed
        if [[ -z "${l_release_pack_name}" ]]; then
            l_release_pack_name=$(
                cut -d\| -f4 "${online_release_pack_info}" | while read -r line; do
                    if [[ -f "${l_specified_pack_dir}/${line}" ]]; then echo "${line}"; break; else continue; fi
                done
            )
        fi

        if [[ -d "${l_specified_pack_dir}" && -f "${l_specified_pack_dir}/${l_release_pack_name}" ]]; then
            source_pack_path="${l_specified_pack_dir}/${l_release_pack_name}"
        fi
    fi

    # 1.1- existed jdk package path in system
    local l_specified_online_pack_info=${l_specified_online_pack_info:-}

    if [[ -n "${source_pack_path}" && -f "${source_pack_path}" ]]; then
        fnBase_OperationProcedureStatement "Verifying existed package"

        l_specified_online_pack_info=$(sed -r -n '/'"${source_pack_path##*/}"'/{p;q}' "${online_release_pack_info}")
        online_release_dgst=$(echo "${l_specified_online_pack_info}" | cut -d\| -f 6)

        if [[ -n "${online_release_dgst}" && $(fnBase_HashDigestVerification "${source_pack_path}" '256') == "${online_release_dgst}" ]]; then
            fnBase_OperationProcedureResult "${source_pack_path%/*}"
            l_is_need_download=0
        else
            fnBase_OperationProcedureResult '' 1
            # change method to selection menu via revoke function itself
            source_pack_path=''
            fn_DownloadAndDecompressOperation
        fi

    # 1.2 - manually specify jdk major version
    elif [[ -n  "${version_specify}" ]]; then
        # version_specify == major_version
        l_specified_online_pack_info=$(sed -r -n '/^'"${version_specify}"'\|/{p;q}' "${online_release_pack_info}")
        online_release_pack_name=$(echo "${l_specified_online_pack_info}" | cut -d\| -f 4)
        online_release_downloadlink=$(echo "${l_specified_online_pack_info}" | cut -d\| -f 5)
        online_release_dgst=$(echo "${l_specified_online_pack_info}" | cut -d\| -f 6)

    # 1.3 - version choose from selection menu list
    else
        fn_VersionSelectionMenu

        if [[ -n "${choose_version}" ]]; then
            l_specified_online_pack_info=$(sed -r -n '/\|'"${choose_version}"'\|/{p;q}' "${online_release_pack_info}")
            online_release_pack_name=$(echo "${l_specified_online_pack_info}" | cut -d\| -f 4)
            online_release_downloadlink=$(echo "${l_specified_online_pack_info}" | cut -d\| -f 5)
            online_release_dgst=$(echo "${l_specified_online_pack_info}" | cut -d\| -f 6)
        fi
    fi

    # 2 - Download Directly From Official Site
    local pack_save_path=${pack_save_path:-}
    if [[ "${l_is_need_download}" -eq 1 ]]; then
        # fnBase_OperationProcedureStatement 'Downloading'
        fnBase_OperationProcedureStatement "Downloading ${c_red}${online_release_pack_name}${c_normal}"
        pack_save_path="${temp_save_dir}/${online_release_pack_name}"
        [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"

        # https://www.tecmint.com/install-java-jdk-jre-in-linux/
        # https://gist.github.com/P7h/9741922
        # curl -fL --cookie/-H "oraclelicense=accept-securebackup-cookie" -O "${pack_download_link}"

        # Step 1. Get HTTP Headers from url 'https://download.oracle.com/otn-pub/java/jdk/xxx.tar.gz', Location is 'https://edelivery.oracle.com/otn-pub/java/jdk/xxx.tar.gz'
        # Step 2. Get HTTP Headers from url 'https://edelivery.oracle.com/otn-pub/java/jdk/xxx.tar.gz', Location has parameter 'AuthParam', this is the real download link.

        local l_http_headers=${l_http_headers:-}
        local l_authparam_url=${l_authparam_url:-}
        local l_edelivery_url=${online_release_downloadlink/download.oracle/edelivery.oracle}
        local l_download_tool_here=${l_download_tool_here:-}

        if [[ "${download_method}" =~ ^curl ]]; then
            l_authparam_url="$($download_method -I -H "Cookie: oraclelicense=accept-securebackup-cookie" "${l_edelivery_url}" 2>&1 | sed -r -n '/^[[:space:]]*Location/{s@^[^:]+:[[:space:]]*@@g;/http:/d;/following/d;p}')"

            # https://superuser.com/questions/489180/remove-r-from-echoing-out-in-bash-script#1215968
            $download_method -H "Cookie: oraclelicense=accept-securebackup-cookie" "${l_authparam_url%%[[:cntrl:]]}" > "${pack_save_path}"

        elif [[ "${download_method}" =~ ^wget ]]; then
            l_authparam_url=$($download_method -S --spider --header="Cookie: oraclelicense=accept-securebackup-cookie" "${l_edelivery_url}" 2>&1 | sed -r -n '/^[[:space:]]*Location/{s@^[^:]+:[[:space:]]*@@g;/http:/d;/following/d;p}')

            $download_method --header="Cookie: oraclelicense=accept-securebackup-cookie" "${l_authparam_url%%[[:cntrl:]]}" > "${pack_save_path}"
        fi

        if [[ -f "${pack_save_path}" && -n $(find "${pack_save_path%/*}" -type f -name "${pack_save_path##*/}" -size +1M -print 2> /dev/null) ]]; then
            fnBase_OperationProcedureResult
        else
            [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
            fnBase_OperationProcedureResult '' 1
            exit
        fi

        fnBase_OperationProcedureStatement "SHA256 dgst verification"
        if [[ -n "${online_release_dgst}" && $(fnBase_HashDigestVerification "${pack_save_path}" '256') == "${online_release_dgst}"  ]]; then
            fnBase_OperationProcedureResult
        else
            [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
            fnBase_OperationProcedureResult '' 1
            exit
        fi

        # use variable "source_pack_path" to execute decompress operation, variable "pack_save_path" used for delete temporary download package
        source_pack_path="${pack_save_path}"
    fi

    # 3 - Decompress & Extract
    fnBase_OperationProcedureStatement "Installing ${c_red}${source_pack_path##*/}${c_normal}"
    local application_backup_path="${installation_dir}${bak_suffix}"
    [[ -d "${application_backup_path}" ]] && rm -rf "${application_backup_path}"

    [[ -d "${installation_dir}" ]] && mv "${installation_dir}" "${application_backup_path}"
    [[ -d "${installation_dir}" ]] || mkdir -p "${installation_dir}"
    tar xf "${source_pack_path}" -C "${installation_dir}" --strip-components=1 2> /dev/null

    if [[ -s "${installation_dir}/bin/java" ]]; then
        [[ -d "${application_backup_path}" ]] && rm -rf "${application_backup_path}"
        fnBase_OperationProcedureResult "${c_red}${installation_dir%/}/${c_normal}"

        # copy newly downloaded pack to ~/Downloads
        local l_user_download_save_path=${l_user_download_save_path:-"${user_download_dir}/${source_pack_path##*/}"}
        if [[ -d "${user_download_dir}" && ! -f "${l_user_download_save_path}" ]]; then
            cp "${source_pack_path}" "${l_user_download_save_path}"
            chown "${login_user}" "${l_user_download_save_path}"
            chgrp -hR "${login_user}" "${l_user_download_save_path}" 2> /dev/null
        fi

        if [[ "${alternative_command}" == 'archlinux-java' && -d /usr/lib/jvm ]]; then
            ln -fs "${installation_dir}" /usr/lib/jvm
        fi
    else
        [[ -d "${installation_dir}" ]] && rm -rf "${installation_dir}"
        [[ "${is_existed}" -eq 1 ]] && mv "${application_backup_path}" "${installation_dir}"
        fnBase_OperationProcedureResult '' 1
    fi

    chown -R root:root "${installation_dir}"
    [[ -n "${pack_save_path}" && -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
}


#########  2-4. Execution PATH Configurarion  #########
fn_ExecutaionPathConfiguration(){
    fnBase_OperationProcedureStatement "Configurarion ${c_red}${oracle_jdk_execute_path}${c_normal}"
    # - add to PATH execution path
    echo "export JAVA_HOME=${installation_dir}" > "${oracle_jdk_execute_path}"
    [[ -d "${installation_dir}/jre" ]] && echo "export JRE_HOME=${installation_dir}/jre" >> "${oracle_jdk_execute_path}"

    if [[ "${alternative_mode}" -eq 1 ]]; then
        fn_AlternativeModeConfiguration 'java' 'install'
        fn_AlternativeModeConfiguration 'javac' 'install'
        fn_AlternativeModeConfiguration 'jar' 'install'
    else
        # - export include files
        [[ -d "${installation_dir}/include" ]] && ln -fsv "${installation_dir}/include" "${oracle_jdk_include_path}" 1> /dev/null

        # - export lib files
        if [[ -d "${installation_dir}/lib" ]]; then
            echo "${installation_dir}/lib" > "${oracle_jdk_lib_path}"
            # [[ -d "${installation_dir}/jre/lib" ]] && echo "${installation_dir}/jre/lib" >> "${oracle_jdk_lib_path}"
            ldconfig -v &> /dev/null
        fi

        echo "export PATH=${installation_dir}/bin:\$PATH" >> "${oracle_jdk_execute_path}"
    fi

    # if [[ -d "${installation_dir}/bin" ]]; then
        # if [[ -d "${installation_dir}/jre/bin" ]]; then
        #     echo "export JRE_HOME=${installation_dir}/jre" >> "${oracle_jdk_execute_path}"
        #     echo "export PATH=${installation_dir}/bin:${installation_dir}/jre/bin:\$PATH" >> "${oracle_jdk_execute_path}"
        # else
        #     echo "export PATH=${installation_dir}/bin:\$PATH" >> "${oracle_jdk_execute_path}"
        # fi    # end if
    # fi    # end if

    # https://github.com/koalaman/shellcheck/wiki/SC1090
    # shellcheck source=/dev/null
    source "${oracle_jdk_execute_path}" 1> /dev/null
    fnBase_OperationProcedureResult
}


#########  3. Operation Time Cost  #########
fn_TotalTimeCosting(){
    # pront java version info
    echo -e "\n${c_red}${software_fullname} version info:${c_normal}" && java -version

    if [[ "${alternative_mode}" -eq 1 ]]; then
        if [[ "${alternative_command}" == 'archlinux-java' ]]; then
            echo -e "\n${c_bold}${c_yellow}${alternative_command}${c_normal} available JAVA environment:"
            archlinux-java status
        else
            echo -e "\n${c_bold}${c_yellow}${alternative_command}${c_normal} ${c_red}mode info:${c_normal}"
            ${alternative_command} --display java
        fi
    fi

    echo ''

    if [[ "${is_existed}" -ne 1 ]]; then
        finish_time=$(date +'%s')        # End Time Of Operation
        total_time_cost=$((finish_time-start_time))   # Total Time Of Operation
        printf "\nTotal time cost is ${c_red}%s${c_normal} seconds!\n" "${total_time_cost}"
    fi    # end if
}


#########  4. Executing Process  #########
fn_InitializationCheck

fn_UninstallOperation
fn_VersionComparasion
fn_DownloadAndDecompressOperation
fn_ExecutaionPathConfiguration
fn_TotalTimeCosting


#########  4. EXIT Singal Processing  #########
# trap "commands" EXIT # execute command when exit from shell
fn_TrapEXIT(){
    fnBase_UnsetVars
    unset oracle_official_site
    unset oracle_java_se_page
    unset installation_dir
    unset oracle_jdk_include_path
    unset oracle_jdk_lib_path
    unset oracle_jdk_execute_path
    unset is_existed
    unset version_check
    unset version_specify
    unset source_pack_path
    unset is_uninstall
    unset proxy_server_specify
    unset current_local_version
    unset online_release_pack_info
    unset choose_version
}

trap fn_TrapEXIT EXIT

# Script End
