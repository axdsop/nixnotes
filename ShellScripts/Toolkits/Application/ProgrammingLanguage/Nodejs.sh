#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails

# Official Site: https://nodejs.org
# Documentation: https://nodejs.org/en/docs/

# Target: Automatically Install & Update Node.js On GNU/Linux
# Developer: MaxdSre

# Change Log:
# - Oct 08, 2020 08:58 Thu ET - initialization check method update
# - Nov 27, 2019 13:39 Wed ET - fix online release date extraction
# - Oct 09, 2019 09:18 Wed ET - change to new base functions
# - Dec 29, 2018 21:34 Sat ET - code reconfiguration, include base funcions from other file
# - May 22, 2018 11:05 Tue ET - code reconfiguration
# - Aug 23, 2017 11:46 Wed +0800
# - June 07, 2017 13:08 Wed +0800
# - May 22, 2017 02:34 Mon +0800


#########  0-1. Singal Setting  #########
trap fnBase_TrapCleanUp INT QUIT

#########  0-2. Variables Setting  #########
# umask 022    # temporarily change umask value to 022
readonly official_site='https://nodejs.org'
readonly download_lts_page="${official_site}/en/download/"
readonly download_edge_page="${download_lts_page%/}/current/"
readonly release_note_page="${official_site}/en/download/releases/"
readonly os_type='linux-x64'
readonly software_fullname='Nodejs'
readonly application_name='Nodejs'
installation_dir="/opt/${application_name}"
readonly version_info_path="${installation_dir}/version"
is_existed=${is_existed:-0}   # Default value is 0 check if system has installed Node.js

include_path="/usr/local/include/${application_name}"
ld_so_conf_path="/etc/ld.so.conf.d/${application_name}.conf"
profile_d_path="/etc/profile.d/${application_name}.sh"

version_check=${version_check:-0}
type_choose=${type_choose:-0}
source_pack_path=${source_pack_path:-}
is_uninstall=${is_uninstall:-0}
proxy_server_specify=${proxy_server_specify:-}


#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | sudo bash -s -- [options] ...

Installing / Updating Node.js (default LTS) On GNU/Linux!
This script requires superuser privileges (eg. root, su).

[available option]
    -h    --help, show help info
    -c    --check, check latest LTS release version
    -f pack_path    --specify latest release package save path (e.g. /PATH/*.tar.gz) or dir (default is ~/Downloads/) in system
    -t type    --choose version type (0 is LTS, 1 is Current version), default is 0
    -p [protocol:]ip:port    --proxy host (http|https|socks4|socks5), default protocol is http
    -u    --uninstall, uninstall software installed
\e[0m"
}

while getopts "hcf:t:up:" option "$@"; do
    case "$option" in
        c ) version_check=1 ;;
        f ) source_pack_path="$OPTARG" ;;
        t ) type_choose="$OPTARG" ;;
        u ) is_uninstall=1 ;;
        p ) proxy_server_specify="$OPTARG" ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done


#########  1-2 Preparation Operation  #########
fn_InitializationCheck(){
    local l_user_codebase_dir="$HOME/.config/axdsop"
    local l_base_function_temp_save_path=''
    local l_need_remove=0
    local l_base_function_url='https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/base_functions.sh'
    l_base_function_temp_save_path=$(find "${l_user_codebase_dir}" -type f -name "${l_base_function_url##*/}" -print 2> /dev/null)
    
    if [[ ! -f "${l_base_function_temp_save_path}" ]]; then
        local l_download_tool=${l_download_tool:-'wget -qO-'}
        [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_tool='curl -fsL'
        l_need_remove=1
        l_base_function_temp_save_path=$(mktemp -t XXXXXXX)
        ${l_download_tool} "${l_base_function_url}" > "${l_base_function_temp_save_path}"
    fi

    if [[ -f "${l_base_function_temp_save_path}" && -s "${l_base_function_temp_save_path}" ]]; then
        . "${l_base_function_temp_save_path}"
        [[ "${l_need_remove}" -eq 1 ]] && rm "${l_base_function_temp_save_path}"
    else
        echo "Sorry, fail to get base script path."
        exit
    fi

    # tput clear  # Echo the clear-screen sequence for the current terminal.
    # specify 1 means need sudo privilege
    fnBase_RunningEnvironmentCheck '1'

    fnBase_CommandExistCheckPhase 'xz' # Fedora/OpenSUSE: xz   Debian/Ubuntu: xz-utils
    fnBase_CommandExistCheckPhase 'tar' # .tar.xz
}


#########  2-1. Uninstall  #########
fn_UninstallOperation(){
    if [[ "${is_uninstall}" -eq 1 ]]; then
        if [[ -d "${installation_dir}" ]]; then
            [[ -d "${installation_dir}" ]] && rm -rf "${installation_dir}"
            [[ -d "${installation_dir}${bak_suffix}" ]] && rm -rf "${installation_dir}${bak_suffix}"

            local npm_path="${login_user_home}/.npm"
            [[ -d "${npm_path}" ]] && rm -rf "${npm_path}"    # ~/.npm/
            local node_repl_history_path="${login_user_home}/.node_repl_history"
            [[ -d "${node_repl_history_path}" ]] && rm -rf "${node_repl_history_path}"   # ~/.node_repl_history

            [[ -d "${include_path}" ]] && rm -rf "${include_path}"
            [[ -f "${ld_so_conf_path}" ]] && rm -rf "${ld_so_conf_path}"
            [[ -f "${profile_d_path}" ]] && rm -rf "${profile_d_path}"

            [[ -d "${installation_dir}" ]] || fnBase_ExitStatement "${software_fullname} is successfully removed from your system!"
        else
            fnBase_ExitStatement "${c_blue}Note${c_normal}: no ${software_fullname} is found in your system!"
        fi
    fi
}

#########  2-2. Latest & Local Version Check  #########
fn_VersionComparasion(){
    fnBase_CentralOutputTitle 'Node.js®'
    fnBase_OperationPhaseStatement 'Version Detection'

    # - Local version
    current_local_version=${current_local_version:-}
    local current_local_type=${current_local_type:-}

    if [[ -f "${installation_dir}/bin/node" ]]; then
        is_existed=1
        if [[ -f "${version_info_path}" && -n $(sed -r -n '/\|/{p}' "${version_info_path}") ]]; then
            current_local_type=$(cut -d\| -f 1 "${version_info_path}")
            current_local_version=$(cut -d\| -f 2 "${version_info_path}")
        else
            current_local_version=$("${installation_dir}/bin/node" --version | sed -r -n 's@v([[:digit:].]*)@\1@p')
        fi

        fnBase_OperationProcedureStatement "Local ${c_bold}${c_yellow}${current_local_type}${c_normal} version detecting"
        fnBase_OperationProcedureResult "${current_local_version}"
    fi

    # - Latest online version info
    local l_online_release_info=${l_online_release_info:-}
    local l_download_page
    case "${type_choose,,}" in
        1 ) type_choose='Current' ;;
        0|* ) type_choose='LTS' ;;
    esac

    [[ -n "${current_local_type}" && "${current_local_type}" != "${type_choose}" && "${type_choose}" != 'Current' ]] && type_choose="${current_local_type}"

    case "${type_choose}" in
        Current ) l_download_page="${download_edge_page}" ;;
        LTS ) l_download_page="${download_lts_page}" ;;
    esac

    fnBase_OperationProcedureStatement "Online ${c_bold}${c_yellow}${type_choose}${c_normal} version detecting"

    l_online_release_info=$($download_method "${l_download_page}" | sed -r -n '/Latest.*?Version/{s@.*Version:[[:space:]]*<[^>]*>([^<]+)<.*npm[[:space:]]*([[:digit:].]+).*@\1|\2@g;p}')
    # node ver|npm ver  -->  8.11.2|5.6.0    10.3.0|6.1.0
    online_release_version="${l_online_release_info%%|*}"

    if [[ -n "${online_release_version}" ]]; then
        online_release_date=$($download_method "${release_note_page}" | sed -r -n 's@<\/[^>]*>@&\n@g;p' | sed -r -n '/<table[^>]*>/,/<\/table>/{s@^[[:space:]]*@@g;s@[[:space:]]*$@@g;p}' | sed ':a;N;$!ba;s@\n@@g' | sed -r -n 's@<\/tr>@&\n@g;p' | sed -r -n '/data-label=.*?Version[^>]*>[^[:space:]]+[[:space:]]+'"${online_release_version}"'[[:space:]]*</{s@.*?Version[^>]*>([^<]+)<.*?<time>([^<]+)<.*@\2@g;p}' | date +'%b %d, %Y' -f - 2> /dev/null)
        fnBase_OperationProcedureResult "${online_release_version} - ${online_release_date}"
    else
        fnBase_OperationProcedureResult '' 1
        fnBase_ExitStatement "${c_red}Fatal error${c_normal}, fail to get online ${c_yellow}${type_choose}${c_normal} version!"
    fi

    # - Just online version check
    if [[ "${version_check}" -eq 1 ]]; then
        fnBase_ExitStatement ''
    fi

    # - Online & Local Version Comparasion
    if [[ "${is_existed}" -eq 1 ]]; then
        if [[ "${online_release_version}" == "${current_local_version}" ]]; then
            fnBase_ExitStatement "\nLatest ${c_yellow}${type_choose}${c_normal} version (${c_red}${online_release_version}${c_normal}) has been existed in your system."
        else
            local l_compare_result
            l_compare_result=$(fnBase_VersionNumberComparasion "${current_local_version}" "${online_release_version}" '<')
            case "${l_compare_result}" in
                0 )
                    fnBase_ExitStatement "\nExisted ${c_yellow}${current_local_type}${c_normal} version local (${c_red}${current_local_version}${c_normal}) > Latest ${c_yellow}${type_choose}${c_normal} version online (${c_red}${online_release_version}${c_normal})."
                    ;;
                1 )
                    printf "\nExisted ${c_yellow}%s${c_normal} version local (${c_red}%s${c_normal}) < Latest ${c_yellow}%s${c_normal} version online (${c_red}%s${c_normal}).\n" "${current_local_type}" "${current_local_version}" "${type_choose}" "${online_release_version}"
                    ;;
            esac
        fi
    # else
    #     printf "No %s find in your system.\n" "${software_fullname}"
    fi
}

#########  2-3. Download & Decompress Latest Software  #########
fn_DownloadAndDecompressOperation(){
    local l_operation_type=${l_operation_type:-'installing'}
    [[ "${is_existed}" -eq 1 ]] && l_operation_type='updating'
    fnBase_OperationPhaseStatement "${l_operation_type^}"

    # https://nodejs.org/dist/v10.1.0/node-v10.1.0-linux-x64.tar.xz
    # https://nodejs.org/dist/v10.1.0/SHASUMS256.txt
    local online_release_downloadlink
    online_release_downloadlink="${official_site}/dist/v${online_release_version}/node-v${online_release_version}-${os_type}.tar.xz"
    local online_release_pack_name=${online_release_pack_name:-"${online_release_downloadlink##*/}"}
    local online_release_dgst=${online_release_dgst:-}

    local l_is_need_download=${l_is_need_download:-1}
    local l_specified_pack_dir=${l_specified_pack_dir:-}

    # specified file path existed
    if [[ "${source_pack_path}" =~ .tar.xz$ && -s "${source_pack_path}" ]]; then
        [[ "${source_pack_path##*/}" == "${online_release_pack_name}" ]] || fnBase_ExitStatement "Package ${c_red}${source_pack_path}${c_normal} is not same to latest release version name!"

    # specified dir or ~/Downloads existed
    elif [[ (-n "${source_pack_path}" && -d "${source_pack_path}" ) || -d "${user_download_dir}" ]]; then
        if [[ -d "${source_pack_path}" ]]; then
            l_specified_pack_dir="${source_pack_path}"
        elif [[ -d "${user_download_dir}" ]]; then
            l_specified_pack_dir="${user_download_dir}"
        fi

        l_specified_pack_dir="${l_specified_pack_dir%/}"

        if [[ -d "${l_specified_pack_dir}" && -f "${l_specified_pack_dir}/${online_release_pack_name}" ]]; then
            source_pack_path="${l_specified_pack_dir}/${online_release_pack_name}"
        fi
    fi

    # https://github.com/nodejs/node#verifying-binaries
    # https://github.com/koalaman/shellcheck/wiki/SC2164
    # cd "${temp_save_dir}" || exit
    # grep node-vx.y.z.tar.gz SHASUMS256.txt | sha256sum -c - &> /dev/null
    # [[ $? -eq 0 ]] && echo 'approve' || echo 'fail'

    online_release_dgst=$($download_method "${online_release_downloadlink%/*}/SHASUMS256.txt" 2> /dev/null | sed -r -n '/[[:space:]]*'"${online_release_pack_name}"'[[:space:]]*$/{s@^[[:space:]]*@@g;s@^([^[:space:]]+).*$@\1@g;p}')

    # 1.1- existed package path in system
    if [[ -n "${source_pack_path}" && -f "${source_pack_path}" ]]; then
        fnBase_OperationProcedureStatement 'Existed pack dgst verifying'

        if [[ -n "${online_release_dgst}" && $(fnBase_HashDigestVerification "${source_pack_path}" '256') == "${online_release_dgst}" ]]; then
            fnBase_OperationProcedureResult "${c_red}${source_pack_path%/*}/${c_normal}"
            l_is_need_download=0
        else
            fnBase_OperationProcedureResult '' 1
        fi
    fi

    # 2 - Download Directly From Official Site
    local pack_save_path=${pack_save_path:-}
    if [[ "${l_is_need_download}" -eq 1 ]]; then
        fnBase_OperationProcedureStatement "Downloading ${c_red}${online_release_pack_name}${c_normal}"
        pack_save_path="${temp_save_dir}/${online_release_pack_name}"
        [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"

        $download_method "${online_release_downloadlink}" > "${pack_save_path}"

        if [[ -s "${pack_save_path}" ]]; then
            fnBase_OperationProcedureResult
        else
            [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
            fnBase_OperationProcedureResult '' 1
        fi

        fnBase_OperationProcedureStatement "SHA256 dgst verification"
        if [[ -n "${online_release_dgst}" && $(fnBase_HashDigestVerification "${pack_save_path}" '256') == "${online_release_dgst}"  ]]; then
            fnBase_OperationProcedureResult
        else
            [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
            fnBase_OperationProcedureResult '' 1
        fi

        # use variable "source_pack_path" to execute decompress operation, variable "pack_save_path" used for delete temporary download package
        source_pack_path="${pack_save_path}"
    fi

    # 3 - Decompress & Extract
    fnBase_OperationProcedureStatement "Installing"
    local application_backup_path="${installation_dir}${bak_suffix}"
    [[ -d "${application_backup_path}" ]] && rm -rf "${application_backup_path}"

    [[ -d "${installation_dir}" ]] && mv "${installation_dir}" "${application_backup_path}"
    [[ -d "${installation_dir}" ]] || mkdir -p "${installation_dir}"
    tar xf "${source_pack_path}" -C "${installation_dir}" --strip-components=1 2> /dev/null

    local new_installed_version=${new_installed_version:-}
    new_installed_version=$("${installation_dir}/bin/node" --version | sed -r -n 's@v([[:digit:].]*)@\1@p')

    if [[ "${online_release_version}" == "${new_installed_version}" ]]; then
        echo "${type_choose}|${online_release_version}|${online_release_date}" > "${version_info_path}"
        [[ -d "${application_backup_path}" ]] && rm -rf "${application_backup_path}"
        fnBase_OperationProcedureResult "${c_red}${installation_dir%/}/${c_normal}"

        # copy newly downloaded pack to ~/Downloads
        local l_user_download_save_path=${l_user_download_save_path:-"${user_download_dir}/${source_pack_path##*/}"}
        if [[ -d "${user_download_dir}" && ! -f "${l_user_download_save_path}" ]]; then
            cp "${source_pack_path}" "${l_user_download_save_path}"
            chown "${login_user}" "${l_user_download_save_path}"
            chgrp -hR "${login_user}" "${l_user_download_save_path}" 2> /dev/null

            # removed old version package existed
            if [[ "${is_existed}" -eq 1 ]]; then
                local l_existed_old_pack_path=${l_existed_old_pack_path:-"${user_download_dir}/${online_release_pack_name//${online_release_version}/${current_local_version}}"}
                [[ -f "${l_existed_old_pack_path}" ]] && rm -f "${l_existed_old_pack_path}"
            fi
        fi
    else
        [[ -d "${installation_dir}" ]] && rm -rf "${installation_dir}"
        [[ "${is_existed}" -eq 1 ]] && mv "${application_backup_path}" "${installation_dir}"
        fnBase_OperationProcedureResult '' 1
    fi

    chown -R root:root "${installation_dir}"
    [[ -n "${pack_save_path}" && -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
}


#########  2-4. Export include/lib/bin Path  #########
fn_PostInstallationConfiguration(){
    fnBase_OperationPhaseStatement 'Configuration'

    # - include files
    fnBase_OperationProcedureStatement 'Include files'
    ln -sv "${installation_dir}/include" "${include_path}" 1> /dev/null
    fnBase_OperationProcedureResult "${include_path}"

    # - lib files
    fnBase_OperationProcedureStatement 'Libary files'
    echo -e "${installation_dir}/lib" > "${ld_so_conf_path}"
    ldconfig -v &> /dev/null    # regenerate cache
    fnBase_OperationProcedureResult "${ld_so_conf_path}"

    # - bin path
    fnBase_OperationProcedureStatement 'Executing path'
    echo "export PATH=\$PATH:${installation_dir}/bin" > "${profile_d_path}"
    # shellcheck source=/dev/null
    source "${profile_d_path}" 2> /dev/null
    fnBase_OperationProcedureResult "${profile_d_path}"
}


#########  2-5. Operation Time Cost  #########
fn_TotalTimeCosting(){
    finish_time=$(date +'%s')        # End Time Of Operation
    total_time_cost=$((finish_time-start_time))   # Total Time Of Operation

    printf "\nTotal time cost is ${c_red}%s${c_normal} seconds!\n" "${total_time_cost}"
    printf "\n${c_bold}${c_blue}%s${c_normal}: You need to relogin to make bash profile effect!\n" 'Notice'
}


#########  3. Executing Process  #########
fn_InitializationCheck

fn_UninstallOperation
fn_VersionComparasion
fn_DownloadAndDecompressOperation
fn_PostInstallationConfiguration
fn_TotalTimeCosting


#########  4. EXIT Singal Processing  #########
# trap "commands" EXIT # execute command when exit from shell
fn_TrapEXIT(){
    fnBase_UnsetVars
    unset installation_dir
    unset is_existed
    unset include_path
    unset ld_so_conf_path
    unset profile_d_path
    unset version_check
    unset type_choose
    unset is_uninstall
    unset proxy_server_specify
}

trap fn_TrapEXIT EXIT

# Script End
