#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails


# Official Site: https://openresty.org/en/
# Github: https://github.com/openresty/openresty

# Target: Automatically Install & Update OpenResty On GNU/Linux
# Developer: MaxdSre

# Change Log:
# - Oct 08, 2020 09:08 Thu ET - initialization check method update
# - Oct 15, 2019 12:39 Tue ET - change custom script url
# - Oct 13, 2019 22:06 ET - change to new base functions
# - Dec 31, 2018 20:41 Mon ET - code reconfiguration, include base funcions from other file
# - Oct 19, 2017 17:16 Thu +0800


#########  0-1. Singal Setting  #########
trap fnBase_TrapCleanUp INT QUIT

#########  0-2. Variables Setting  #########
# umask 022    # temporarily change umask value to 022
readonly official_site='https://openresty.org/en/'
readonly download_page="${official_site}download.html"
readonly software_fullname=${software_fullname:-'OpenResty Web Platform'}
readonly application_name=${application_name:-'openresty'}
bak_suffix=${bak_suffix:-'_bak'}     # suffix word for file backup
is_existed=${is_existed:-0}   # Default value is 0， check if system has installed Nginx
readonly custom_shellscript_url='https://gitlab.com/axdsop/nixnotes/raw/master/ShellScripts/Toolkits'
readonly os_check_script="${custom_shellscript_url}/GNULinux/gnuLinuxMachineInfoDetection.sh"
profile_d_path="/etc/profile.d/${application_name}.sh"

version_check=${version_check:-0}
is_uninstall=${is_uninstall:-0}
os_detect=${os_detect:-0}
enable_firewall=${enable_firewall:-0}
proxy_server_specify=${proxy_server_specify:-}


#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | sudo bash -s -- [options] ...

Installing / Updating OpenResty Web Platform On GNU/Linux!
This script requires superuser privileges (eg. root, su).

[available option]
    -h    --help, show help info
    -c    --check, check current stable release version
    -o    --os info, detect os distribution info
    -p [protocol:]ip:port    --proxy host (http|https|socks4|socks5), default protocol is http
    -u    --uninstall, uninstall software installed
\e[0m"
}

while getopts "hcuolp:" option "$@"; do
    case "$option" in
        c ) version_check=1 ;;
        u ) is_uninstall=1 ;;
        o ) os_detect=1 ;;
        p ) proxy_server_specify="$OPTARG" ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done


#########  1-2 Preparation Operation  #########
fn_InitializationCheck(){
    local l_user_codebase_dir="$HOME/.config/axdsop"
    local l_base_function_temp_save_path=''
    local l_need_remove=0
    local l_base_function_url='https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/base_functions.sh'
    l_base_function_temp_save_path=$(find "${l_user_codebase_dir}" -type f -name "${l_base_function_url##*/}" -print 2> /dev/null)
    
    if [[ ! -f "${l_base_function_temp_save_path}" ]]; then
        local l_download_tool=${l_download_tool:-'wget -qO-'}
        [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_tool='curl -fsL'
        l_need_remove=1
        l_base_function_temp_save_path=$(mktemp -t XXXXXXX)
        ${l_download_tool} "${l_base_function_url}" > "${l_base_function_temp_save_path}"
    fi

    if [[ -f "${l_base_function_temp_save_path}" && -s "${l_base_function_temp_save_path}" ]]; then
        . "${l_base_function_temp_save_path}"
        [[ "${l_need_remove}" -eq 1 ]] && rm "${l_base_function_temp_save_path}"
    else
        echo "Sorry, fail to get base script path."
        exit
    fi

    # tput clear  # Echo the clear-screen sequence for the current terminal.
    # specify 1 means need sudo privilege
    fnBase_RunningEnvironmentCheck '1'

    if [[ "${os_detect}" -eq 1 ]]; then
        $download_method "${os_check_script}" | bash -s --
        exit
    fi
}


fn_OSInfoDetection(){
    local l_json_osinfo=${l_json_osinfo:-}
    l_json_osinfo=$(fnBase_OSInfoRetrieve "${os_check_script}")
    # [[ -z "${l_json_osinfo}" ]] && fnBase_ExitStatement "${c_red}Fatal${c_normal}: fail to extract os info!"

    distro_fullname=${distro_fullname:-}
    distro_fullname=$(fnBase_OSInfoDirectiveValExtraction 'distro_fullname' "${l_json_osinfo}")

    distro_name=${distro_name:-}
    distro_name=$(fnBase_OSInfoDirectiveValExtraction 'distro_name' "${l_json_osinfo}")
    distro_name="${distro_name%%-*}"    # centos, fedora

    family_name=${family_name:-}
    family_name=$(fnBase_OSInfoDirectiveValExtraction 'distro_family_own' "${l_json_osinfo}")

    codename=${codename:-}
    codename=$(fnBase_OSInfoDirectiveValExtraction 'distro_codename' "${l_json_osinfo}")

    version_id=${version_id:-}
    version_id=$(fnBase_OSInfoDirectiveValExtraction 'distro_version_id' "${l_json_osinfo}")

    ip_local=${ip_local:-}
    ip_local=$(fnBase_OSInfoDirectiveValExtraction 'ip_local' "${l_json_osinfo}")

    ip_public=${ip_public:-}
    ip_public=$(fnBase_OSInfoDirectiveValExtraction 'ip_public' "${l_json_osinfo}")
}


#########  2-1. Install/Update/Upgrade/Uninstall Operation Function  #########
fn_OperationProcedure(){
    case "${1:-}" in
        update ) local action=1 ;;
        upgrade ) local action=2 ;;
        remove ) local action=3 ;;
        * )  local action=0 ;; # default 0 installation
    esac

    case "${distro_name}" in
        rhel|centos|'rhel fedora'|fedora|amzn )
            source_file='/etc/yum.repos.d/openresty.repo'
            case "${action}" in
                0 )
                    [[ -f "${source_file}" && ! -f "${source_file}${bak_suffix}" ]] && cp -fp "${source_file}" "${source_file}${bak_suffix}"

                    echo -e "[openresty]\nname=Official OpenResty Open Source Repository for ${distro_name^^}\nbaseurl=https://openresty.org/package/${distro_name##* }/\$releasever/\$basearch\nskip_if_unavailable=False\ngpgcheck=1\nrepo_gpgcheck=1\ngpgkey=https://openresty.org/package/pubkey.gpg\nenabled=1\nenabled_metadata=1" > "${source_file}"

                    yum clean all 1> /dev/null
                    yum -q makecache fast 1> /dev/null
                    rpm --import https://openresty.org/package/pubkey.gpg &> /dev/null
                    yum -y install openresty openresty-resty openresty-openssl openresty-openssl-devel openresty-pcre openresty-pcre-devel openresty-zlib openresty-zlib-devel &> /dev/null
                     # openresty-opm openresty-doc
                    ;;
                1 )
                    yum -y update openresty openresty-resty &> /dev/null
                    ;;
                2 )
                    fn_OperationProcedure
                    ;;
                3 )
                    yum -y remove openresty* &> /dev/null
                    if [[ -f "${source_file}${bak_suffix}" ]]; then
                        mv "${source_file}${bak_suffix}" "$source_file"
                    else
                        [[ -f "${source_file}" ]] && rm -f "${source_file}"
                    fi
                    ;;
            esac
            ;;
        debian|ubuntu )
            local codename_list
            codename_list=$($download_method "${official_site}linux-packages.html" | sed -r -n '/<li>Ubuntu</,/<li>CentOS</{s@^[[:space:]]*@@g;/^</d;s@^[^[:space:]]+[[:space:]]+([^[:space:]]+).*@\L\1@g;p}' | sed ':a;N;$!ba;s@\n@|@g;')
            [[ "${codename_list}" =~ \|?"${codename,,}"\|? ]] || fnBase_ExitStatement "${c_red}Sorry${c_normal}: official repo does not support your system ${c_red}${distro_fullname}${c_normal}!"

            source_file='/etc/apt/sources.list.d/openresty.list'

            case "${action}" in
                0 )
                #Use https protocol, other it may prompt error
                [[ -f "${source_file}" ]] && cp -fp "${source_file}" "${source_file}${bak_suffix}"
                $download_method 'https://openresty.org/package/pubkey.gpg' | apt-key add - &> /dev/null   #method 1
                # apt-key adv --keyserver keyserver.ubuntu.com --recv-keys "$gpg_pub_key" &> /dev/null  #method 2

                if [[ "${distro_name}" == 'debian' ]]; then
                    echo "deb http://openresty.org/package/${distro_name} ${codename} openresty" > "${source_file}"
                    [[ "${codename}" = 'wheezy' ]] && sed -r -i "1i deb http://ftp.debian.org/debian wheezy-backports main" "${source_file}"
                else
                    echo "deb http://openresty.org/package/${distro_name} ${codename} main" > "${source_file}"
                fi

                # apt-get -y --force-yes install
                apt-get -y install apt-transport-https &> /dev/null
                apt-get update 1> /dev/null
                apt-get -y install libpcre3-dev libssl-dev perl &> /dev/null
                apt-get -y install openresty &> /dev/null
                fnBase_CommandExistIfCheck 'systemctl' || apt-get -y install sysv-rc-conf &> /dev/null   # same to chkconfig
                    ;;
                1 )
                    apt-get -y install --only-upgrade openresty &> /dev/null
                    ;;
                2 )
                    fn_OperationProcedure
                    ;;
                3 )
                    apt-get -y purge openresty &> /dev/null
                    apt-get -y autoremove 1> /dev/null
                    if [[ -f "${source_file}${bak_suffix}" ]]; then
                        mv "${source_file}${bak_suffix}" "$source_file"
                    else
                        [[ -f "${source_file}" ]] && rm -f "${source_file}"
                    fi
                    # apt-key del "${gpg_pub_key}" &> /dev/null
                    ;;
            esac
            ;;
        * )
            fnBase_ExitStatement "${c_red}Sorry${c_normal}: your ${c_red}${distro_fullname}${c_normal} may not be supported by OpenResty official repo currently!"
            ;;
    esac
}


#########  2-2. Local/Online Version Check  #########
fn_VersionLocalCheck(){
    if fnBase_CommandExistIfCheck 'resty'; then
        is_existed=1
        current_local_version=$(resty -v 2>&1 | sed -r -n '/^nginx version/{s@.*\/(.*)$@\1@g;p}')
    fi
}

fn_VersionOnlineCheck(){
    latest_version_online_info=$($download_method "${download_page}" | sed -r -n '/Lastest release/,/Legacy release/{/href=/{s@<\/a>@&\n@g;p}}' | sed ':a;N;$!ba;s@\n@@g' | sed -r -n 's@<\/?li>@&\n@g;p' | sed -r -n '/href=.*.tar.gz/{s@\&nbsp;@ @g;p}' | sed -r -n 's@<\/a[^>]*>@&\n@g;p' | sed -r -n '/href=/{s@^.*href="([^"]+)".*$@\1@g};{s@[^[:digit:]]+([^<]+)<.*@\1@g;};p' | sed ':a;N;$!ba;s@\n@|@g' | awk -F\| -v site="${official_site}" 'BEGIN{OFS="|"}{a=gensub(/^.*-(.*).tar.gz$/,"\\1","g",$1);"date --date=\""$NF"\" +\"%F\"" | getline b;print a,b,site$3,$1,$2}')

    # 1.13.6.2|2018-05-14|https://openresty.org/en/changelog-1013006.html|https://openresty.org/download/openresty-1.13.6.2.tar.gz|https://openresty.org/download/openresty-1.13.6.2.tar.gz.asc

    latest_version_online="${latest_version_online_info%%|*}"
    [[ -z "${latest_version_online}" ]] && fnBase_ExitStatement "${c_red}Sorry${c_normal}: fail to get latest online version on official site!"
    release_date=$(echo "${latest_version_online_info}" | cut -d\| -f2 | date +'%b %d, %Y' -f - 2> /dev/null)

    if [[ "${version_check}" -eq 1 ]]; then
        if [[ "${is_existed}" -eq 1 ]]; then
            fnBase_ExitStatement "Local existed version is ${c_red}${current_local_version}${c_normal}, Latest version online is ${c_red}${latest_version_online}${c_normal} (${c_blue}${release_date}${c_normal})!"
        else
            fnBase_ExitStatement "Latest version online (${c_red}${latest_version_online}${c_normal}), Release date ($c_red${release_date}$c_normal)!"
        fi
    fi

    if [[ "${is_existed}" -eq 1 ]]; then
        if [[ "${latest_version_online}" == "${current_local_version}" ]]; then
            fnBase_ExitStatement "Latest version (${c_red}${latest_version_online}${c_normal}) has been existed in your system!"
        elif [[ $(fnBase_VersionNumberComparasion "${current_local_version}" "${latest_version_online}" '<') -eq 1 ]]; then
            printf "Existed version local (${c_red}%s${c_normal}) < Latest  version online (${c_red}%s${c_normal})!\n" "${current_local_version}" "${latest_version_online}"
            fn_OperationProcedure "update"
        fi
    else
        printf "No %s find in your system!\n" "${software_fullname}"
        echo -e "Operation procedure will costs some time, just be patient!\n"
        fn_OperationProcedure    # install
    fi
}


#########  2-3. Uninstall  #########
fn_UninstallOperation(){
    if [[ "${is_uninstall}" -eq 1 ]]; then
        [[ "${is_existed}" -eq 1 ]] || fnBase_ExitStatement "${c_blue}Note${c_normal}: no ${software_fullname} is found in your system!"

        fn_OperationProcedure 'remove'
        [[ -d '/usr/local/openresty' ]] && rm -rf '/usr/local/openresty'
        [[ -d "${profile_d_path}" ]] && rm -rf "${profile_d_path}"

        fnBase_CommandExistIfCheck 'openresty' || fnBase_ExitStatement "${software_fullname} (v ${c_red}${current_local_version}${c_normal}) is successfully removed from your system!"
    fi
}


#########  2-4. Post-installation Configuration  #########
fn_PostInstallationConfiguration(){
    new_installed_version=$(resty -v 2>&1 | sed -r -n '/^nginx version/{s@.*\/(.*)$@\1@g;p}')

    if [[ "${latest_version_online}" != "${new_installed_version}" ]]; then
        fn_OperationProcedure 'remove'
        [[ "${is_existed}" -eq 1 ]] && operation_type='update' || operation_type='install'
        fnBase_ExitStatement "${c_red}Sorry${c_normal}: ${c_blue}${operation_type}${c_normal} operation is faily!"
    fi

    local nginx_conf_path
    nginx_conf_path='/usr/local/openresty/nginx/conf/nginx.conf'

    if [[ -f "${nginx_conf_path}" && ! -f "${nginx_conf_path}${bak_suffix}" ]]; then
        cp -f "${nginx_conf_path}" "${nginx_conf_path}${bak_suffix}"
        # $download_method "${conf_nginx_conf}" > "${nginx_conf_path}"
    fi

    local installation_dir='/usr/local/openresty'
    echo "export PATH=\$PATH:${installation_dir}/nginx/sbin" > "${profile_d_path}"
    echo "export PATH=\$PATH:${installation_dir}/luajit/bin/luajit" >> "${profile_d_path}"
    # shellcheck source=/dev/null
    source "${profile_d_path}" 2> /dev/null

    fnBase_SystemServiceManagement 'openresty' 'enable'

    # - check web dir
    local nginx_web_dir
    nginx_web_dir='/usr/local/openresty/nginx/html'

    echo "${distro_fullname}" >> "${nginx_web_dir}/index.html"

    if [[ -n "${ip_public}" ]]; then
        ip_address="${ip_public}"
    elif [[ -n "${ip_local}" && "${ip_public}" != "${ip_local}" ]]; then
        ip_address='127.0.0.1'
    fi

    if [[ $($download_method -I "${ip_address}" | awk '{print $2;exit}') == '200' ]]; then
        if [[ "$is_existed" -eq 1 ]]; then
            printf "%s was updated to version ${c_red}%s${c_normal} successfully!\n" "${software_fullname}" "${latest_version_online}"
        else
            printf "Installing %s version ${c_red}%s${c_normal} successfully!\n" "${software_fullname}" "${latest_version_online}"
        fi

        printf "Opening ${c_blue}%s${c_normal} in your browser to see welcome page!\n" "http://${ip_address}"
    fi
}

#########  2-5. Operation Time Cost  #########
fn_TotalTimeCosting(){
    finish_time=$(date +'%s')        # End Time Of Operation
    total_time_cost=$((finish_time-start_time))   # Total Time Of Operation
    printf "\nTotal time cost is ${c_red}%s${c_normal} seconds!\n" "${total_time_cost}"
}


#########  3. Executing Process  #########
fn_InitializationCheck

fn_OSInfoDetection

fn_VersionLocalCheck
fn_UninstallOperation
fn_VersionOnlineCheck
fn_PostInstallationConfiguration
fn_TotalTimeCosting


#########  4. EXIT Singal Processing  #########
# trap "commands" EXIT # execute command when exit from shell
fn_TrapEXIT(){
    fnBase_UnsetVars
    unset is_existed
    unset profile_d_path
    unset version_check
    unset is_uninstall
    unset os_detect
    unset proxy_server_specify
}

trap fn_TrapEXIT EXIT

# Script End
