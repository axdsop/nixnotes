#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails


# Target: Automatically Install & Update Nginx Web Server Via Package Manager On GNU/Linux
# Developer: MaxdSre

# Change Log:
# - Oct 08, 2020 09:08 Thu ET - initialization check method update
# - Oct 16, 2019 10:35 Wed ET - reverse list release info list
# - Oct 15, 2019 12:35 Tue ET - change custom script url
# - Oct 13, 2019 21:56 ET - change to new base functions
# - Dec 31, 2018 17:02 Mon ET - code reconfiguration, include base funcions from other file
# - Apr 13, 2018 16:31 ET - Refactoring, remove parameter '-m', add parameter '-t'
# - Dec 14, 2017 15:45 +0800
# - June 08, 2017 15:32 Thu +0800
# - May 17, 2017 12:02 ET
# - Mar 14, 2017 10:32~13:44 +0800
# - Feb 25, 2017 13:42 +0800


# Official Site:
# - https://nginx.org/en/
# - https://www.nginx.com
# - https://nginx.org/en/pgp_keys.html

# Installation:
# - https://nginx.org/en/linux_packages.html
# - https://www.nginx.com/resources/wiki/start/topics/tutorials/install/
# - https://docs.nginx.com/nginx/admin-guide/
# - https://docs.nginx.com/nginx/admin-guide/installing-nginx/installing-nginx-open-source/
# - https://www.nginx.com/blog/using-nginx-logging-for-application-performance-monitoring/
# - https://docs.nginx.com/nginx/admin-guide/security-controls/configuring-http-basic-authentication/
# - http://blog.csdn.net/dengjiexian123/article/details/53386586

# GIXY - Nginx configuration static analyzer
# https://github.com/yandex/gixy

# /etc/apt/trusted.gpg
# --------------------
#
# pub   rsa2048 2011-08-19 [SC] [expires: 2024-06-14]
#       573B FD6B 3D8F BC64 1079  A6AB ABF5 BD82 7BD9 BF62
# uid           [ unknown] nginx signing key <signing-key@nginx.com>
# sudo apt-key del '573B FD6B 3D8F BC64 1079  A6AB ABF5 BD82 7BD9 BF62'

# SELinux in enforcing mode
# - boolean
# semanage boolean -l | grep httpd
# - specify port other than 80
# semanage port -l | grep http
# semanage port -a -t http_port_t -p tcp 8080
# - specify root path other /usr/share/nginx/html/
# semanage fcontext -a -t httpd_sys_content_t "/data/nginx/html(/.*)?"



#########  0-1. Singal Setting  #########
trap fnBase_TrapCleanUp INT QUIT

#########  0-2. Variables Setting  #########
# umask 022    # temporarily change umask value to 022
readonly official_site='https://nginx.org'
readonly nginx_gpg_pub_key=${nginx_gpg_pub_key:-'ABF5BD827BD9BF62'}
release_type_choose=${release_type_choose:-}
software_fullname=${software_fullname:-'Nginx Web Server'}
is_existed=${is_existed:-0}   # Default value is 0， check if system has installed Nginx

readonly custom_shellscript_url='https://gitlab.com/axdsop/nixnotes/raw/master/ShellScripts/Toolkits'
readonly os_check_script="${custom_shellscript_url}/GNULinux/gnuLinuxMachineInfoDetection.sh"
readonly firewall_configuration_script="${custom_shellscript_url}/GNULinux/gnuLinuxFirewallRuleConfiguration.sh"
readonly conf_nginx_link="${custom_shellscript_url}/_Configs/Application/nginx/nginx.conf"
readonly conf_default_link="${custom_shellscript_url}/_Configs/Application/nginx/default.conf"
readonly conf_ssl_link="${custom_shellscript_url}/_Configs/Application/nginx/ssl.conf"
readonly release_version_info_link="${custom_shellscript_url}/_Configs/Application/nginx/nginx_relese_version_info.txt"

release_info_list=${release_info_list:-}
install_or_update=${install_or_update:-}  # 1 install, 2 update

version_check=${version_check:-0}
is_uninstall=${is_uninstall:-0}
strictly_remove=${strictly_remove:-0}
os_detect=${os_detect:-0}
list_release=${list_release:-0}
release_type_choose=${release_type_choose:-}
enable_firewall=${enable_firewall:-0}
proxy_server_specify=${proxy_server_specify:-}
is_show_parase=${is_show_parase:-1}


#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | sudo bash -s -- [options] ...

Installing / Updating Nginx Web Server (stable) On GNU/Linux!
This script requires superuser privileges (eg. root, su).

[available option]
    -h    --help, show help info
    -c    --check current (stable|mainline) release version
    -l    --list release info using Markdown format
    -o    --os info, detect os distribution info
    -t release_type    --choose release type (stable|mainline), default is stable
    -f    --enable firewall (iptable/firewalld/ufw/SuSEfirewall2)
    -p [protocol:]ip:port    --proxy host (http|https|socks4|socks5), default protocol is http
    -S    --strictly remove config dir, log dir, cache dir but exclude data dir while erase Nginx service, along with '-u'
    -u    --uninstall software installed
\e[0m"
}

while getopts "hcolt:fuSp:" option "$@"; do
    case "$option" in
        c ) version_check=1 ;;
        o ) os_detect=1 ;;
        l ) list_release=1 ;;
        t ) release_type_choose="$OPTARG" ;;
        f ) enable_firewall=1 ;;
        u ) is_uninstall=1 ;;
        S ) strictly_remove=1 ;;
        p ) proxy_server_specify="$OPTARG" ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done



#########  1-2 Preparation Operation  #########
fn_InitializationCheck(){
    local l_user_codebase_dir="$HOME/.config/axdsop"
    local l_base_function_temp_save_path=''
    local l_need_remove=0
    local l_base_function_url='https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/base_functions.sh'
    l_base_function_temp_save_path=$(find "${l_user_codebase_dir}" -type f -name "${l_base_function_url##*/}" -print 2> /dev/null)
    
    if [[ ! -f "${l_base_function_temp_save_path}" ]]; then
        local l_download_tool=${l_download_tool:-'wget -qO-'}
        [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_tool='curl -fsL'
        l_need_remove=1
        l_base_function_temp_save_path=$(mktemp -t XXXXXXX)
        ${l_download_tool} "${l_base_function_url}" > "${l_base_function_temp_save_path}"
    fi

    if [[ -f "${l_base_function_temp_save_path}" && -s "${l_base_function_temp_save_path}" ]]; then
        . "${l_base_function_temp_save_path}"
        [[ "${l_need_remove}" -eq 1 ]] && rm "${l_base_function_temp_save_path}"
    else
        echo "Sorry, fail to get base script path."
        exit
    fi

    # tput clear  # Echo the clear-screen sequence for the current terminal.
    # specify 1 means need sudo privilege
    fnBase_RunningEnvironmentCheck '1'

    if [[ "${os_detect}" -eq 1 ]]; then
        $download_method "${os_check_script}" | bash -s --
        exit
    fi
}


fn_OSInfoDetection(){
    local l_json_osinfo=${l_json_osinfo:-}
    l_json_osinfo=$(fnBase_OSInfoRetrieve "${os_check_script}")
    # [[ -z "${l_json_osinfo}" ]] && fnBase_ExitStatement "${c_red}Fatal${c_normal}: fail to extract os info!"

    distro_fullname=${distro_fullname:-}
    distro_fullname=$(fnBase_OSInfoDirectiveValExtraction 'distro_fullname' "${l_json_osinfo}")

    distro_name=${distro_name:-}
    distro_name=$(fnBase_OSInfoDirectiveValExtraction 'distro_name' "${l_json_osinfo}")
    distro_name="${distro_name%%-*}"    # centos, fedora

    family_name=${family_name:-}
    family_name=$(fnBase_OSInfoDirectiveValExtraction 'distro_family_own' "${l_json_osinfo}")

    codename=${codename:-}
    codename=$(fnBase_OSInfoDirectiveValExtraction 'distro_codename' "${l_json_osinfo}")

    version_id=${version_id:-}
    version_id=$(fnBase_OSInfoDirectiveValExtraction 'distro_version_id' "${l_json_osinfo}")

    ip_local=${ip_local:-}
    ip_local=$(fnBase_OSInfoDirectiveValExtraction 'ip_local' "${l_json_osinfo}")

    ip_public=${ip_public:-}
    ip_public=$(fnBase_OSInfoDirectiveValExtraction 'ip_public' "${l_json_osinfo}")

    pack_manager=${pack_manager:-}
    pack_manager=$(fnBase_OSInfoDirectiveValExtraction 'distro_pack_manager' "${l_json_osinfo}")
}


#########  2-0. Variables Preprocessing  #########
fn_VariablesPreprocessing(){
    [[ "${list_release}" -eq 1 || "${version_check}" -eq 1 ]] && is_show_parase=0

    # release type choose stable/mainline
    case "${release_type_choose,,}" in
        m|mainline ) release_type_choose='mainline' ;;
        s|stable|* ) release_type_choose='stable' ;;
    esac

    # repo path
    case "${distro_name,,}" in
        rhel|centos )
            repo_file_path='/etc/yum.repos.d/nginx.repo' ;;
        debian|ubuntu )
            repo_file_path='/etc/apt/sources.list.d/nginx.list' ;;
        sles )
            repo_file_path='/etc/zypp/repos.d/nginx.repo' ;;
        * )
            fnBase_ExitStatement "${c_red}Sorry${c_normal}: your ${c_yellow}${distro_fullname}${c_normal} may not be supported by Nginx official repo currently!"
            ;;
    esac

}

#########  2-1. Uninstall  #########
fn_UninstallOperation(){
    if [[ "${is_uninstall}" -eq 1 ]]; then
        if [[ "${strictly_remove}" -eq 1 ]]; then
            [[ -d '/etc/nginx' ]] && rm -rf /etc/nginx
            [[ -d 'var/log/nginx' ]] && rm -rf var/log/nginx
            [[ -d 'var/cache/nginx' ]] && rm -rf var/cache/nginx
        fi

        fnBase_CommandExistIfCheck 'nginx' || fnBase_ExitStatement "${c_red}Note${c_normal}: no ${software_fullname} is found in your system!"

        current_local_version=$(nginx -V 2>&1 | sed -r -n '1s@.*/([[:digit:].]*)@\1@p')

        fnBase_PackageManagerOperation 'remove' 'nginx*'
        [[ -f "${repo_file_path}" ]] && rm -f "${repo_file_path}"

        fnBase_CommandExistIfCheck 'nginx' || fnBase_ExitStatement "${software_fullname} (${c_red}${current_local_version}${c_normal}) is successfully removed from your system!"
        fnBase_ExitStatement ''
    fi
}


#########  2-2. Release Info List  #########
fn_ReleaseVersionExtraction(){
    local l_html_path=${1:-}

    if [[ -n "${l_html_path}" && -s "${l_html_path}" ]]; then
        local date_cursor=${date_cursor:-}
        local raw_version_info=${raw_version_info:-}

        raw_version_info=$(sed -r 's@<br><\/p>@\n@g;s@legacy stable@legacy@g;/>nginx-0.8.55</{s@<\/a>.*$@<\/a>@g;};/>nginx-0.8.55</astable' "${l_html_path}" | sed -r -n '/>nginx-([[:digit:]]+.)+[[:digit:]]+</{N;p}' | sed -r -n 'N;s@\n@@g;s@[[:space:]]*<[^>]*>@ @g;s@^[[:space:]]*@@g;s@[[:space:]]+@ @g;s@nginx-@@g;s@[[:space:]]*Here we go![[:space:]]*@ @g;s@[[:space:]]*version.*$@@g;s@[[:space:]]+@|@g;p')

        # empty, use to store version info
        echo '' > "${l_html_path}"

        echo "${raw_version_info}" | while IFS="|" read -r r_date r_version r_type; do
            if [[ "${r_date}" =~ [0-9]{4}(-[0-9]{2}){2} ]]; then
                echo "${r_date}|${r_version}|${r_type%%|*}" >> "${l_html_path}"
                date_cursor="${r_date}"
            else
                echo "${date_cursor}|${r_date}|${r_version%%|*}" >> "${l_html_path}"
                date_cursor=''
            fi
        done
    fi
}

fn_ReleaseInfoList(){
    if [[ "${is_show_parase}" -eq 1 ]]; then
        fnBase_CentralOutputTitle 'GNU/Linux Distribution Information'
        [[ -z "${distro_name}" ]] || fnBase_CentralOutput 'Distro Name' "${distro_name}"
        [[ -z "${version_id}" ]] || fnBase_CentralOutput 'Version ID' "${version_id}"
        [[ -z "${codename}" ]] || fnBase_CentralOutput "Code Name" "${codename}"
        [[ -z "${distro_fullname}" ]] || fnBase_CentralOutput 'Full Name' "${distro_fullname}"
        # fnBase_CentralOutputTitle

        fnBase_CentralOutputTitle 'Operation Processing, Just Be Patient'
        fnBase_OperationPhaseStatement 'Online Release Version'
        fnBase_OperationProcedureStatement 'Extracting'
    fi

    local release_info_html
    release_info_html=$(mktemp -t "${mktemp_format}")

    $download_method "${official_site}" > "${release_info_html}"
    fn_ReleaseVersionExtraction "${release_info_html}"
    release_info_list=$(cat "${release_info_html}")

    # empty file content
    > "${release_info_html}"
    $download_method "${release_version_info_link}" > "${release_info_html}"

    # if download from github failed, manually download via parallel or loop download
    if [[ ! -s "${release_info_html}" ]]; then
        local news_url_list=''
        news_url_list=$($download_method "${official_site}" | sed -r -n '/id="menu"/{s@<\/a>@&\n@g;p}' | sed -r -n '/[[:digit:]]+.html/!d;s@.*href="([^"]+)".*$@'"${official_site}"'/\1@g;p' | sed '1i'"${official_site}"'')

        if fnBase_CommandExistIfCheck 'parallel'; then
            # parallel downloading, fastest
            fnSub_OfficialSitePageDownload(){
                local l_link=${1:-}
                [[ -n "${l_link}" ]] && $download_method "${l_link}"
            }
            export download_method="${download_method}"
            export -f fnSub_OfficialSitePageDownload
            echo "${news_url_list}" | parallel -k -j 0 fnSub_OfficialSitePageDownload 2> /dev/null >> "${release_info_html}"
        else
            # loop downloading, consume at least 6 times more then parallel operation
            echo "${news_url_list}" | while read -r line; do
                $download_method "${line}" >> "${release_info_html}"
            done
        fi

        fn_ReleaseVersionExtraction "${release_info_html}"
    fi

    echo "${release_info_list}" >> "${release_info_html}"
    release_info_list=$(sort -t\| -k1,1r "${release_info_html}" | uniq 2> /dev/null)

    if [[ "${is_show_parase}" -eq 1 ]]; then
        if [[ -n "${release_info_list}"  ]]; then
            fnBase_OperationProcedureResult
        else
            fnBase_OperationProcedureResult '' 1
        fi
    else
        [[ -z "${release_info_list}" ]] && fnBase_ExitStatement "${c_red}Sorry${c_normal}: fail to extract release version info."
    fi

    if [[ "${list_release}" -eq 1 && -n "${release_info_list}" ]]; then
        [[ -f "${release_info_html}" ]] && rm -f "${release_info_html}"
        echo -e "${c_bold}${c_red}Nginx Release Version Info${c_normal}\n\nDate|Version|Type\n---|---|---"
        release_info_list=$(echo "${release_info_list}" | sort -t\| -r -k1,1)
        fnBase_ExitStatement "${release_info_list}"
    fi
}


#########  2-3. Local & Online Version Operation Comparasion #########
fn_VersionComparasion(){
    if [[ "${is_show_parase}" -eq 1 ]]; then
        fnBase_OperationPhaseStatement 'Check If Existed'
        fnBase_OperationProcedureStatement 'Local Detection'
    fi

    # - local version
    if fnBase_CommandExistIfCheck 'nginx'; then
        is_existed=1
        # "${BASH_VERSINFO[0]}" -ge 4 2>&1 <==> |&
        current_local_version=$(nginx -V 2>&1 | sed -r -n '1s@.*/([[:digit:].]*)@\1@p')
        current_local_version_type=$(echo "${release_info_list}" | sed -r -n '/\|'"${current_local_version}"'\|/{s@.*\|([^\|]+)$@\1@g;p}')
        [[ "${is_show_parase}" -eq 1 ]] && fnBase_OperationProcedureResult "${current_local_version_type} ${current_local_version}"
    else
        [[ "${is_show_parase}" -eq 1 ]] && fnBase_OperationProcedureResult 'not find'
    fi

    # - online version
    [[ "${is_show_parase}" -eq 1 ]] && fnBase_OperationProcedureStatement "Online ${release_type_choose^} Version"
    # https://www.linuxtopia.org/online_books/linux_tool_guides/the_sed_faq/sedfaq4_004.html
    local l_stable_ver_info
    l_stable_ver_info=$(echo "${release_info_list}" | sed -r -n '/\|stable$/{s@^([^\|]+)\|([^\|]+)\|.*@\1|\2@g;p;q}')
    local l_mainline_ver_info
    l_mainline_ver_info=$(echo "${release_info_list}" | sed -r -n '/\|mainline$/{s@^([^\|]+)\|([^\|]+)\|.*@\1|\2@g;p;q}')

    latest_version_online=''
    case "${release_type_choose,,}" in
        mainline ) latest_version_online="${l_mainline_ver_info##*|}" ;;
        stable|* ) latest_version_online="${l_stable_ver_info##*|}" ;;
    esac

    if [[ "${is_show_parase}" -eq 1 ]]; then
        if [[ -n "${latest_version_online}" ]]; then
            fnBase_OperationProcedureResult "${latest_version_online}"
        else
            fnBase_OperationProcedureResult '' 1
        fi
    else
        [[ -z "${latest_version_online}" ]] && fnBase_ExitStatement "${c_red}Sorry${c_normal}: fail to get latest ${release_type_choose} version on official site!"
    fi

    if [[ "${version_check}" -eq 1 ]]; then
        if [[ "${is_existed}" -eq 1 ]]; then
            echo -e "Local existed is ${c_red}${current_local_version_type}${c_normal} version (${c_red}${current_local_version}${c_normal})\n"
        else
            echo -e "No ${software_fullname} finds in your system.\n"
        fi

        fnBase_ExitStatement "Latest ${c_red}stable${c_normal} version (${c_red}${l_stable_ver_info##*|}${c_normal}), release date ($c_red${l_stable_ver_info%%|*}$c_normal)\nLatest ${c_red}mainline${c_normal} version (${c_red}${l_mainline_ver_info##*|}${c_normal}), release date ($c_red${l_mainline_ver_info%%|*}$c_normal)"
    fi

    if [[ "${is_existed}" -eq 1 ]]; then
        fnBase_OperationProcedureStatement 'Version Comparasion'

        if [[ $(fnBase_VersionNumberComparasion "${current_local_version}" "${latest_version_online}" '<') -eq 1 ]]; then
            # printf "Existed local $c_blue%s$c_normal version ($c_red%s$c_normal) < Latest $c_blue%s$c_normal online version ($c_red%s$c_normal).\n" "${current_local_version_type}" "${current_local_version}" "${release_type_choose}" "${latest_version_online}"
            fnBase_OperationProcedureResult "${current_local_version} < ${latest_version_online}"

            install_or_update='update'
        else
            fnBase_OperationProcedureResult "${current_local_version} >= ${latest_version_online}"
            # echo -e "Existed local ${c_blue}${current_local_version_type}${c_normal} version (${c_red}${current_local_version}${c_normal})\n"

            fnBase_ExitStatement "\nLatest ${c_red}stable${c_normal} version (${c_red}${l_stable_ver_info##*|}${c_normal}), release date ($c_red${l_stable_ver_info%%|*}$c_normal)\nLatest ${c_red}mainline${c_normal} version (${c_red}${l_mainline_ver_info##*|}${c_normal}), release date ($c_red${l_mainline_ver_info%%|*}$c_normal)"
        fi
    else
        # printf "No %s find in your system!\n" "${software_fullname}"
        install_or_update='install'
    fi

}


#########  2-4. Operation Install/Update/Upgrade  #########
fn_RepoConfiguration(){
    local l_release_type=${1:-'stable'}

    case "${l_release_type}" in
        stable ) l_release_type='' ;;
        mainline ) l_release_type='mainline/' ;;
    esac

    case "${distro_name}" in
        rhel|centos )
            echo -n -e "[nginx]\nname=nginx repo\nbaseurl=${official_site}/packages/${l_release_type}${distro_name}/${version_id%%.*}/\$basearch/\ngpgcheck=0\nenabled=1\n" > "${repo_file_path}"
            ;;
        debian|ubuntu )
            #Use https protocol, other it may prompt error
            $download_method "${official_site}/keys/nginx_signing.key" | apt-key add - &> /dev/null   #method 1
            # apt-key adv --keyserver keyserver.ubuntu.com --recv-keys "$nginx_gpg_pub_key" &> /dev/null  #method 2

            # https://nginx.org/packages/ubuntu/dists/
            # 17.10|Artful Aardvark|2017-10-19|July 2018
            # 17.04|Zesty Zapus|2017-04-13|January 2018
            [[ "${distro_name}" == 'ubuntu' && "${version_id}" == '17.10' ]] && codename='zesty'    # nginx not provide artful(17.10) repo

            echo -n -e "deb ${official_site}/packages/${l_release_type}${distro_name}/ ${codename} nginx\ndeb-src ${official_site}/packages/${distro_name}/ ${codename} nginx\n" > "${repo_file_path}"
            ;;
        sles )
            # https://nginx.org/packages/sles/12
            # https://nginx.org/packages/mainline/sles/12
            echo -n -e "[nginx]\nenabled=1\nautorefresh=1\nbaseurl=${official_site}/packages/${l_release_type}${distro_name}/12\ntype=rpm-md\ngpgcheck=0" > "${repo_file_path}"
            ;;
    esac
}

fn_OperationProcedure(){
    fnBase_OperationPhaseStatement "${software_fullname} Operation"

    case "${install_or_update,,}" in
        i|install )
            # install
            fnBase_OperationProcedureStatement 'Repository Configuration'
            fn_RepoConfiguration "${release_type_choose}"
            fnBase_OperationProcedureResult "${repo_file_path}"

            fnBase_OperationProcedureStatement 'Installing'
            fnBase_PackageManagerOperation
            fnBase_PackageManagerOperation 'install' 'nginx'
            # fnBase_OperationProcedureResult
            ;;
        u|update )
            # update
            local l_release_type_local=${l_release_type_local:-'stable'}
            [[ -n $(sed -r -n '/mainline/p' "${repo_file_path}") ]] && l_release_type_local='mainline'

            # local version < online version
            # stable  mainline               upgrade
            # stable  stable                 update
            # mainline  mainline             update
            # mainline  (stable < mainline)  update
            if [[ "${l_release_type_local}" == 'stable' && "${release_type_choose}" == 'mainline' ]]; then
                fnBase_OperationProcedureStatement 'Repository Generation'
                fn_RepoConfiguration "${release_type_choose}"
                fnBase_OperationProcedureResult "${repo_file_path}"
            fi

            fnBase_OperationProcedureStatement 'Upgrading'
            fnBase_PackageManagerOperation
            fnBase_PackageManagerOperation 'upgrade' 'nginx'
            # fnBase_OperationProcedureResult
            ;;
    esac

    # "${BASH_VERSINFO[0]}" -ge 4 2>&1 <==> |&
    local l_new_installed_version
    l_new_installed_version=$(nginx -V 2>&1 | sed -r -n '1s@.*/([[:digit:].]*)@\1@p')

    if [[ "${latest_version_online}" != "${l_new_installed_version}" ]]; then
        fnBase_PackageManagerOperation 'remove' 'nginx'
        local l_operation_type=${l_operation_type:-'install'}
        [[ "${is_existed}" -eq 1 ]] && l_operation_type='update'
        # fnBase_ExitStatement "${c_red}Sorry${c_normal}: ${c_blue}${l_operation_type}${c_normal} operation is faily!"
        fnBase_OperationProcedureResult '' 1
    else
        fnBase_OperationProcedureResult
    fi
}

fn_PostInstallationConfiguration(){
    if [[ "${is_existed}" -ne 1 ]]; then
        # Firewall Setting
        if [[ "${enable_firewall}" -eq 1 ]]; then
            fnBase_OperationProcedureStatement 'Firewall Rule Adding'
            $download_method "${firewall_configuration_script}" | bash -s -- -p '80' -s
            # $download_method "${firewall_configuration_script}" | bash -s -- -p '443' -s
            fnBase_OperationProcedureResult 'port 80'
        fi

        fnBase_OperationProcedureStatement 'Config File Configuration'
        local l_nginx_conf_path=${l_nginx_conf_path:-'/etc/nginx/nginx.conf'}
        local l_default_conf_path=${l_default_conf_path:-'/etc/nginx/conf.d/default.conf'}
        local l_ssl_conf_path=${l_ssl_conf_pathL:-'/etc/nginx/conf.d/ssl.conf'}

        if [[ -f "${l_nginx_conf_path}" && ! -f "${l_nginx_conf_path}${bak_suffix}" ]]; then
            cp -f "${l_nginx_conf_path}" "${l_nginx_conf_path}${bak_suffix}"
            $download_method "${conf_nginx_link}" > "${l_nginx_conf_path}"
        fi

        if [[ ! -f "${l_ssl_conf_path}${bak_suffix}" ]]; then
            $download_method "${conf_ssl_link}" > "${l_ssl_conf_path}${bak_suffix}"
        fi

        if [[ -f "${l_default_conf_path}" && ! -f "${l_default_conf_path}${bak_suffix}" ]]; then
            cp -f "${l_default_conf_path}" "${l_default_conf_path}${bak_suffix}"
            $download_method "${conf_default_link}" > "${l_default_conf_path}"
        fi
        fnBase_OperationProcedureResult "${l_default_conf_path}"

        if [[ -d '/var/cache/nginx' ]]; then
            mkdir -p /var/cache/nginx/{proxy_cache,proxy_temp} &> /dev/null
            chown -R nginx /var/cache/nginx
        fi

        fnBase_SystemServiceManagement 'nginx' 'enable'

        # - check web dir
        local nginx_web_dir
        nginx_web_dir='/usr/share/nginx/html'
        [[ -d "${nginx_web_dir}" ]] || nginx_web_dir=$(sed -r -n '/[[:space:]]*location[[:space:]]*\/[[:space:]]*\{/,/}/{/root/s@[[:space:]]*root[[:space:]]*(.*);@\1@p}' /etc/nginx/nginx.conf /etc/nginx/conf.d/*.conf 2> /dev/null)
        # - change owner/group
        chown -R nginx:nginx "${nginx_web_dir}" 2> /dev/null
        [[ -f "${nginx_web_dir}/index.html" ]] && echo "${distro_fullname}" >> "${nginx_web_dir}/index.html"
    fi

    local l_ip_address=${l_ip_address:-'127.0.0.1'}
    local l_http_status=${l_http_status:-0}

    case "${download_method%% *}" in
        curl ) l_http_status=$($download_method -I "${l_ip_address}" 2>&1 | sed -r -n '/^HTTP/{s@^[^[:space:]]*[[:space:]]*([[:digit:]]+).*@\1@g;p}') ;;
        wget ) l_http_status=$($download_method --spider "${l_ip_address}" 2>&1 | sed -r -n '/HTTP request sent/{s@^[^[:digit:]]*([[:digit:]]+).*$@\1@g;p}') ;;
    esac

    if [[ "${l_http_status}" == '200' ]]; then
        if [[ "$is_existed" -eq 1 ]]; then
            printf "\n%s was updated to ${c_blue}%s${c_normal} version ${c_red}%s${c_normal} successfully!\n" "${software_fullname}" "${release_type_choose}" "${latest_version_online}"
        else
            printf "\nInstalling %s ${c_blue}%s${c_normal} version ${c_red}%s${c_normal} successfully!\n" "${software_fullname}" "${release_type_choose}" "${latest_version_online}"
        fi

        if [[ -n "${ip_public}" && -n $(ip addr 2> /dev/null | sed -r -n '/[[:space:]]+'"${ip_public}"'\/?/{p}') ]]; then
            l_ip_address="${ip_public}"
        elif [[ -n "${ip_local}" ]]; then
            l_ip_address="${ip_local}"
        fi
        printf "Opening ${c_blue}%s${c_normal} in your browser to see welcome page!\n" "http://${l_ip_address}"
    else
        echo "${c_red}Faily to verificate service status${c_normal}"
    fi
}


#########  2-5. Operation Time Cost  #########
fn_TotalTimeCosting(){
    finish_time=$(date +'%s')        # End Time Of Operation
    total_time_cost=$((finish_time-start_time))   # Total Time Of Operation
    fnBase_ExitStatement "\nTotal time cost is ${c_red}${total_time_cost}${c_normal} seconds!"
}


#########  3. Executing Process  #########
fn_InitializationCheck

fn_OSInfoDetection

fn_VariablesPreprocessing
fn_UninstallOperation
fn_ReleaseInfoList
fn_VersionComparasion
fn_OperationProcedure
fn_PostInstallationConfiguration
fn_TotalTimeCosting


#########  4. EXIT Singal Processing  #########
# trap "commands" EXIT # execute command when exit from shell
fn_TrapEXIT(){
    fnBase_UnsetVars
    unset release_type_choose
    unset software_fullname
    unset is_existed
    unset release_info_list
    unset install_or_update
    unset version_check
    unset is_uninstall
    unset os_detect
    unset list_release
    unset enable_firewall
    unset proxy_server_specify
}

trap fn_TrapEXIT EXIT

# Script End
