#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails


# Official Site:
# - https://tomcat.apache.org/
# Requirement environment
# - http://www.oracle.com/technetwork/java/index.html
# - http://jdk.java.net/

# https://wiki.archlinux.org/index.php/Tomcat

# Tomcat Port
# - 8005 SHUTDOWN
# - 8009 AJP 1.3 Connector
# - 8080 HTTP   /PATH/webapps/ROOT/index.jsp

# Attention: Tomcat can not run as root

# Target: Automatically Install & Configuring Apache Tomcat
# Developer: MaxdSre

# Change Log:
# - Oct 08, 2020 09:07 Thu ET - initialization check method update
# - Oct 15, 2019 12:34 Tue ET - change custom script url
# - Oct 13, 2019 21:40 Sun ET - change to new base functions
# - Dec 30, 2018 16:55 Sun ET - code reconfiguration, include base funcions from other file
# - Jul 26, 2018 08:59 ~ 13:20 Thu ET - first draft


#########  0-1. Singal Setting  #########
trap fnBase_TrapCleanUp INT QUIT

#########  0-2. Variables Setting  #########
# umask 022    # temporarily change umask value to 022
official_site=${official_site:-'https://tomcat.apache.org'}
readonly custom_shellscript_url='https://gitlab.com/axdsop/nixnotes/raw/master/ShellScripts/Toolkits'
readonly os_check_script="${custom_shellscript_url}/GNULinux/gnuLinuxMachineInfoDetection.sh"
readonly firewall_configuration_script="${custom_shellscript_url}/GNULinux/gnuLinuxFirewallRuleConfiguration.sh"

readonly software_fullname=${software_fullname:-'Apache Tomcat'}
readonly software_name=${software_name:-'tomcat'}
readonly application_name=${application_name:-'ApacheTomcat'}
installation_dir="/opt/${application_name}"

readonly socket_port_default='8080'
socket_port=${socket_port:-"${socket_port_default}"}

is_existed=${is_existed:-1}      # Default value is 1， assume system has installed Apache Tomcat
version_check=${version_check:-0}
version_specify=${version_specify:-}
source_pack_path=${source_pack_path:-}

strict_mode_enable=${strict_mode_enable:-0}
enable_firewall=${enable_firewall:-0}
is_uninstall=${is_uninstall:-0}
proxy_server_specify=${proxy_server_specify:-}


#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
script [options] ...
script | sudo bash -s -- [options] ...

Installing / Configuring Apache Tomcat On GNU/Linux!
This script requires superuser privileges (eg. root, su).

[available option]
    -h    --help, show help info
    -c    --check, check Apache Tomcat installed or not
    -f pack_path    --specify latest release package save path (e.g. /PATH/*.tar.gz) or dir (default is ~/Downloads/) in system
    -v version    --specify specific Apache Tomcat version (e.g. 7, 8, 9)
    -s port    --set tomcat port number, default is 8080
    -F    --enable firewall rule (iptable/firewalld/ufw/SuSEfirewall2), default is disable
    -u    --uninstall, uninstall software installed
    -r    --strict mode enable (remove user tomcat while uninstall), along with '-u'
    -p [protocol:]ip:port    --proxy host (http|https|socks4|socks5), default protocol is http
\e[0m"
}

while getopts "hcf:v:s:Frup:" option "$@"; do
    case "$option" in
        c ) version_check=1 ;;
        f ) source_pack_path="$OPTARG" ;;
        u ) is_uninstall=1 ;;
        v ) version_specify="$OPTARG" ;;
        s ) socket_port="$OPTARG" ;;
        r ) strict_mode_enable=1 ;;
        F ) enable_firewall=1 ;;
        p ) proxy_server_specify="$OPTARG" ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done

#########  1-2 Preparation Operation  #########
fn_InitializationCheck(){
    local l_user_codebase_dir="$HOME/.config/axdsop"
    local l_base_function_temp_save_path=''
    local l_need_remove=0
    local l_base_function_url='https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/base_functions.sh'
    l_base_function_temp_save_path=$(find "${l_user_codebase_dir}" -type f -name "${l_base_function_url##*/}" -print 2> /dev/null)
    
    if [[ ! -f "${l_base_function_temp_save_path}" ]]; then
        local l_download_tool=${l_download_tool:-'wget -qO-'}
        [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_tool='curl -fsL'
        l_need_remove=1
        l_base_function_temp_save_path=$(mktemp -t XXXXXXX)
        ${l_download_tool} "${l_base_function_url}" > "${l_base_function_temp_save_path}"
    fi

    if [[ -f "${l_base_function_temp_save_path}" && -s "${l_base_function_temp_save_path}" ]]; then
        . "${l_base_function_temp_save_path}"
        [[ "${l_need_remove}" -eq 1 ]] && rm "${l_base_function_temp_save_path}"
    else
        echo "Sorry, fail to get base script path."
        exit
    fi

    # tput clear  # Echo the clear-screen sequence for the current terminal.
    # specify 1 means need sudo privilege
    fnBase_RunningEnvironmentCheck '1'

    # - Java environment
    if fnBase_CommandExistIfCheck 'java'; then
        JAVA_HOME=$(readlink -f /usr/bin/java | sed -r 's@(.*)/bin.*@\1@g')
        export JAVA_HOME
        [[ -z "${JAVA_HOME:-}" ]] && fnBase_ExitStatement "${c_red}Error${c_normal}, No environment variable ${c_blue}JAVA_HOME${c_normal} found!"
    else
        fnBase_ExitStatement "${c_red}Error${c_normal}, No ${c_blue}java${c_normal} command found!"
    fi

    fnBase_CommandExistCheckPhase 'gzip' # CentOS/Debian/OpenSUSE: gzip
    fnBase_CommandExistCheckPhase 'tar' # .tar.bz2
    fnBase_CommandExistCheckPhase 'gawk'
}


#########  2-1. Uninstall Operation  #########
fn_UninstallOperation(){
    if [[ "${is_uninstall}" -eq 1 ]]; then

        if [[ -d "${installation_dir}" ]]; then
            local l_service_script_dir='/etc/init.d'
            fnBase_CommandExistIfCheck 'systemctl' && l_service_script_dir='/etc/systemd/system'

            # - stop daemon running
            find "${l_service_script_dir}"/ -type f -name "${software_name}*" -print | while IFS="" read -r line; do
                if [[ -n "${line}" ]]; then
                    service_name="${line##*/}"
                    service_name="${service_name%%.*}"
                    fnBase_SystemServiceManagement "${service_name}" 'stop'
                    # - remove system init script   SysV init / Systemd
                    [[ -f "${line}" ]] && rm -f "${line}"
                    unset service_name
                fi
            done

            if [[ "${strict_mode_enable}" -eq 1 ]]; then
                # - remove user and group - tomcat
                if [[ -n $(sed -r -n '/^'"${software_name}"':/{p}' /etc/passwd) ]]; then
                    userdel -fr "${software_name}" 2> /dev/null
                    groupdel -f "${software_name}" 2> /dev/null
                fi
            fi

            [[ -d "${installation_dir}" ]] && rm -rf "${installation_dir}"

            fnBase_CommandExistIfCheck 'systemctl' && systemctl daemon-reload 2> /dev/null
            fnBase_ExitStatement "${software_fullname} is successfully removed from your system!"
        else
            fnBase_ExitStatement "${c_blue}Note${c_normal}: no ${software_fullname} is found in your system!"
        fi
    fi
}

#########  2-2. Latest & Local Version Check  #########
fn_VersionComparasion(){
    fnBase_CentralOutputTitle "${software_fullname}"
    fnBase_OperationPhaseStatement 'Version Detection'

    # - Local version
    current_local_version=${current_local_version:-}
    if [[ -d "${installation_dir}/bin" ]]; then
        local l_version_path=${l_version_path:-"${installation_dir}/bin/version.sh"}

        if [[ -s "${l_version_path}" ]]; then
            # Server version: Apache Tomcat/9.0.10
            current_local_version=$(bash "${l_version_path}" 2> /dev/null | sed -r -n '/^Server[[:space:]]+version:/{s@^[^[:digit:]]+(.*)@\1@g;p}')
        else
            is_existed=0
        fi
    else
        is_existed=0
    fi

    if [[ "${is_existed}" -eq 1 ]]; then
        fnBase_OperationProcedureStatement "Local version detecting"
        fnBase_OperationProcedureResult "${current_local_version}"
    else
        if [[ -n "${current_local_version}" ]]; then
            fnBase_OperationProcedureStatement "Existed ${software_fullname} version"
            fnBase_OperationProcedureResult "${current_local_version}"
        else
            if [[ "${is_uninstall}" -eq 1 ]]; then
                fnBase_ExitStatement "No ${software_fullname} finds in your system!"
            fi
        fi
    fi

    # - Latest online version info
    fnBase_OperationProcedureStatement 'Online version detecting'
    online_release_pack_info=$(mktemp -t "${mktemp_format}")
    # empty file content
    [[ -s "${online_release_pack_info}" ]] && > "${online_release_pack_info}"

    # Apache Tomcat Versions
    # https://tomcat.apache.org/whichversion.html
    # sed -r -n '/<table[[:space:]]+/,/<\/table>/{s@<\/tr>@---@g;/(<td>|---)/!d;s@[[:space:]]*<[^>]+>[[:space:]]*@@g;p}' | sed ':a;N;$!ba;s@\n@|@g;s@---|*@\n@g' | sed '/^$/d' | cut -d\| -f 7 | sed -r -n '/\(/d;p'

    local l_current_timestamp
    l_current_timestamp=$(date +'%s')

    $download_method "${official_site}" | sed -r -n '/pull-right.*?Tomcat[[:space:]]*[[:digit:]]+/{s@[[:space:]]*<[^>]+>[[:space:]]*@@g;s@^([[:digit:]-]+)[^[:digit:]]+([[:digit:].]+).*$@\2|\1@g;p};/href=.*>Download</{/[[:digit:]]+/!d;s@.*href="([^"]+)".*$@\1@g;p}' | sed -r -n 'N;s@\n@|@g;p' | sort -t"|" -k 1,1rn | while IFS="|" read -r t_version t_release_date t_version_url; do
        # 9.0.10|2018-06-25|https://tomcat.apache.org/download-90.cgi

        # The Apache Tomcat team announces that support for Apache Tomcat 8.0.x will end on 30 June 2018. - https://tomcat.apache.org/tomcat-80-eol.html
        # date --date='30 June 2018' +'%s'   # 1530331200
        if [[ "${t_version}" =~ ^8.0.[0-9]+ && "${l_current_timestamp}" -ge 1530331200 ]]; then
            continue
        else
            local l_pack_info=''
            l_pack_info=$($download_method "${t_version_url}" | sed -r -n '/'"${t_version}"'+.tar.gz/{/(sha1)/d;s@.*href="([^"]+)".*$@\1@g;p}' | sed ':a;N;$!ba;s@\n@|@g')
            local l_pack_name
            l_pack_name="${l_pack_info%%|*}"
            l_pack_name="${l_pack_name##*/}"
            echo "${t_version}|${t_release_date}|${l_pack_name}|${l_pack_info}" >> "${online_release_pack_info}"
            # version|date|pack_name|pack_download_link|pack_gpg_url|pack_sha512_dgst_url
            # 9.0.10|2018-06-25|apache-tomcat-9.0.10.tar.gz|http://apache.mirrors.tds.net/tomcat/tomcat-9/v9.0.10/bin/apache-tomcat-9.0.10.tar.gz|https://www.apache.org/dist/tomcat/tomcat-9/v9.0.10/bin/apache-tomcat-9.0.10.tar.gz.asc|https://www.apache.org/dist/tomcat/tomcat-9/v9.0.10/bin/apache-tomcat-9.0.10.tar.gz.sha512
        fi
    done

    [[ -s "${online_release_pack_info}" ]] || fnBase_ExitStatement "${c_red}Sorry${c_normal}: fail to extract release version info from official page!"

    local l_version_list=''
    l_version_list=$(cut -d\| -f1 "${online_release_pack_info}" | sed ':a;N;$!ba;s@\n@, @g;')

    if [[ -n "${l_version_list}" ]]; then
        fnBase_OperationProcedureResult "${l_version_list}"
        # awk -F\| '{printf("%12s | %s\n",$1,$2)}' "${online_release_pack_info}"
    else
        fnBase_OperationProcedureResult '' 1
    fi

    # - Just online version check
    if [[ "${version_check}" -eq 1 ]]; then
        fnBase_ExitStatement ''
    fi

    # - Online & Local Version Comparasion
    if [[ "${is_existed}" -eq 1 && -n "${current_local_version}" ]]; then
        if [[ -z $(sed -r -n '/^'"${current_local_version}"'\|/{p}' "${online_release_pack_info}") ]]; then
            printf "\nExisted version local (${c_red}%s${c_normal}) is not the latest version online.\n" "${current_local_version}"
        else
            fnBase_ExitStatement "\nLatest release version has been existed in your system!"
        fi
    fi
}


#########  2-3. Prerequisites  #########
fn_ParaSpecifiedValVerification(){
    fnBase_OperationPhaseStatement 'Parameters Specified Val Verification'

    # 1- verify if the port specified is available
    fnBase_OperationProcedureStatement 'Port No. verification'
    socket_port=$(echo "${socket_port}" | sed -r 's@[[:alpha:]]*@@g;s@[[:punct:]]*@@g;s@[[:blank:]]*@@g')
    if [[ -n "${socket_port}" ]]; then
        if [[ "${socket_port}" -eq "${socket_port_default}" ]]; then
            fnBase_OperationProcedureResult "${socket_port}"
        elif [[ "${socket_port}" =~ ^[1-9]{1}[0-9]{1,4}$ ]]; then
            local sys_port_start=${sys_port_start:-1024}
            local sys_port_end=${sys_port_end:-65535}

            if [[ "${socket_port}" -ge "${sys_port_start}" && "${socket_port}" -le "${sys_port_end}" ]]; then
                local port_service_info
                port_service_info=$(awk 'match($2,/^'"${socket_port}"'\/tcp$/){print $1,$2}' /etc/services 2> /dev/null)
                if [[ -n "${port_service_info}" ]]; then
                    fnBase_OperationProcedureResult "${port_service_info} in /etc/services" 1
                else
                    fnBase_OperationProcedureResult "${socket_port}"
                fi
            else
                fnBase_OperationProcedureResult "${socket_port}⊄(${sys_port_start},${sys_port_end}) out of range" 1
            fi

        else
            fnBase_OperationProcedureResult "${socket_port} illegal" 1
        fi

    fi
}

fn_NormalUserOperation(){
    if [[ -z $(sed -r -n '/^'"${software_name}"':/{p}' /etc/passwd) ]]; then
        fnBase_OperationProcedureStatement 'Create user & group'
        # create group
        groupadd -r "${software_name}" 2> /dev/null
        # create user without login privilege
        useradd -r -g "${software_name}" -s /bin/false -d "${installation_dir}" -c "${software_fullname}" "${software_name}" 2> /dev/null

        user_name="${software_name}"
        group_name="${software_name}"
        fnBase_OperationProcedureResult "${user_name}:${group_name}"
    else
        user_name="${software_name}"
        group_name="${software_name}"
    fi
}

fn_Prerequisites(){
    if [[ "${is_existed}" -ne 1 ]]; then
        fn_ParaSpecifiedValVerification
        fn_NormalUserOperation
    fi
}


#########  2-4. Download & Decompress Latest Software  #########
# global variable - choose_version
fn_VersionSelectionMenu(){
    local IFS_BAK=${IFS_BAK:-"$IFS"}  # Backup IFS
    IFS="|" # Setting temporary IFS

    echo -e "\n${c_bold}${c_blue}Available Product Version List: ${c_normal}"
    PS3="${c_yellow}Choose version number(e.g. 1, 2,...): ${c_normal}"

    choose_version=${choose_version:-}

    # cut -d\| -f1 "${online_release_pack_info}" | sed ':a;N;$!ba;s@\n@|@g'
    select item in $(sed -r -n 's@^([[:digit:]]{1}).*$@\1@g;p' "${online_release_pack_info}" | sed ':a;N;$!ba;s@\n@|@g'); do
        choose_version="${item}"
        [[ -n "${choose_version}" ]] && break
    done < /dev/tty

    IFS=${IFS_BAK}  # Restore IFS
    unset IFS_BAK
    unset PS3
    choose_version=$(sed -r -n '/^'"${choose_version}"'/{s@^([^\|]+).*$@\1@g;p}' "${online_release_pack_info}")
    printf "\nProduct version you choose is ${c_bold}${c_yellow}%s${c_normal}.\n\n" "${choose_version}"
}

fn_DownloadAndDecompressOperation(){
    local l_operation_type=${l_operation_type:-'installing'}
    [[ "${is_existed}" -eq 1 ]] && l_operation_type='updating'
    fnBase_OperationPhaseStatement "${l_operation_type^}"

    # version|date|pack_name|pack_download_link|pack_gpg_url|pack_sha512_dgst_url
    # 9.0.10|2018-06-25|apache-tomcat-9.0.10.tar.gz|http://apache.mirrors.tds.net/tomcat/tomcat-9/v9.0.10/bin/apache-tomcat-9.0.10.tar.gz|https://www.apache.org/dist/tomcat/tomcat-9/v9.0.10/bin/apache-tomcat-9.0.10.tar.gz.asc|https://www.apache.org/dist/tomcat/tomcat-9/v9.0.10/bin/apache-tomcat-9.0.10.tar.gz.sha512

    local online_release_dgst=${online_release_dgst:-}

    local online_release_downloadlink=${online_release_downloadlink:-}
    local online_release_pack_name

    local l_is_need_download=${l_is_need_download:-1}
    local l_specified_pack_dir=${l_specified_pack_dir:-}

    # if version_specify not available, set its value to empty ''
    [[ "${version_specify}" =~ ^[1-9] && -n $(sed -r -n '/^'"${version_specify}"'/{p}' "${online_release_pack_info}") ]] || version_specify=''

    # - specified file path existed
    if [[ "${source_pack_path}" =~ .tar.gz$ && -s "${source_pack_path}" ]]; then
        [[ -z $(sed -r -n '/\|'"${source_pack_path##*/}"'\|/{s@^([^\|]+).*@\1@g;p}') ]] && fnBase_ExitStatement "Package ${c_red}${source_pack_path}${c_normal} is not same to its origin name!"

    # - specified dir or ~/Downloads existed
    elif [[ (-n "${source_pack_path}" && -d "${source_pack_path}" ) || -d "${user_download_dir}" ]]; then
        if [[ -d "${source_pack_path}" ]]; then
            l_specified_pack_dir="${source_pack_path}"
        elif [[ -d "${user_download_dir}" ]]; then
            l_specified_pack_dir="${user_download_dir}"
        fi

        l_specified_pack_dir="${l_specified_pack_dir%/}"

        local l_release_pack_name=${l_release_pack_name:-}
        [[ -n "${version_specify}" ]] && l_release_pack_name=$(sed -r -n '/^'"${version_specify}"'\|/{p;q}' "${online_release_pack_info}" | cut -d\| -f3)

        # extract first matched package name existed
        if [[ -z "${l_release_pack_name}" ]]; then
            l_release_pack_name=$(
                cut -d\| -f3 "${online_release_pack_info}" | while read -r line; do
                    if [[ -f "${l_specified_pack_dir}/${line}" ]]; then echo "${line}"; break; else continue; fi
                done
            )
        fi

        if [[ -d "${l_specified_pack_dir}" && -f "${l_specified_pack_dir}/${l_release_pack_name}" ]]; then
            source_pack_path="${l_specified_pack_dir}/${l_release_pack_name}"
        fi
    fi

    # 1.1- existed jdk package path in system
    local l_specified_online_pack_info=${l_specified_online_pack_info:-}

    if [[ -n "${source_pack_path}" && -f "${source_pack_path}" ]]; then
        fnBase_OperationProcedureStatement "Verifying existed package"

        l_specified_online_pack_info=$(sed -r -n '/\|'"${source_pack_path##*/}"'\|/{p;q}' "${online_release_pack_info}")
        online_release_dgst=$($download_method "${l_specified_online_pack_info##*|}" 2> /dev/null | sed -r -n 's@^([^[:space:]]+).*$@\1@g;p')

        if [[ -n "${online_release_dgst}" && $(fnBase_HashDigestVerification "${source_pack_path}" '512') == "${online_release_dgst}" ]]; then
            fnBase_OperationProcedureResult "${source_pack_path%/*}"
            l_is_need_download=0
        else
            fnBase_OperationProcedureResult '' 1
            # change method to selection menu via revoke function itself
            source_pack_path=''
            fn_DownloadAndDecompressOperation
        fi

    # 1.2 - manually specify jdk major version
    elif [[ -n  "${version_specify}" ]]; then
        # version_specify == major_version
        l_specified_online_pack_info=$(sed -r -n '/^'"${version_specify}"'/{p;q}' "${online_release_pack_info}")
        online_release_pack_name=$(echo "${l_specified_online_pack_info}" | cut -d\| -f 3)
        online_release_downloadlink=$(echo "${l_specified_online_pack_info}" | cut -d\| -f 4 )
        online_release_dgst=$($download_method "${l_specified_online_pack_info##*|}" 2> /dev/null | sed -r -n 's@^([^[:space:]]+).*$@\1@g;p')

    # 1.3 - version choose from selection menu list
    else
        fn_VersionSelectionMenu

        if [[ -n "${choose_version}" ]]; then
            l_specified_online_pack_info=$(sed -r -n '/^'"${choose_version}"'/{p;q}' "${online_release_pack_info}")
            online_release_pack_name=$(echo "${l_specified_online_pack_info}" | cut -d\| -f 3)
            online_release_downloadlink=$(echo "${l_specified_online_pack_info}" | cut -d\| -f 4)
            online_release_dgst=$($download_method "${l_specified_online_pack_info##*|}" 2> /dev/null | sed -r -n 's@^([^[:space:]]+).*$@\1@g;p')
        fi
    fi

    # 2 - Download Directly From Official Site
    local pack_save_path=${pack_save_path:-}
    if [[ "${l_is_need_download}" -eq 1 ]]; then
        fnBase_OperationProcedureStatement 'Downloading'
        pack_save_path="${temp_save_dir}/${online_release_pack_name}"
        [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"

        $download_method "${online_release_downloadlink}" > "${pack_save_path}"

        if [[ -s "${pack_save_path}" ]]; then
            fnBase_OperationProcedureResult
        else
            [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
            fnBase_OperationProcedureResult '' 1
        fi

        fnBase_OperationProcedureStatement "SHA512 dgst verification"
        if [[ -n "${online_release_dgst}" && $(fnBase_HashDigestVerification "${pack_save_path}" '512') == "${online_release_dgst}"  ]]; then
            fnBase_OperationProcedureResult
        else
            [[ -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
            fnBase_OperationProcedureResult '' 1
        fi

        # use variable "source_pack_path" to execute decompress operation, variable "pack_save_path" used for delete temporary download package
        source_pack_path="${pack_save_path}"
    fi

    # 3 - Decompress & Extract
    fnBase_OperationProcedureStatement "Installing ${c_yellow}${source_pack_path##*/}${c_normal}"
    local application_backup_path="${installation_dir}${bak_suffix}"
    [[ -d "${application_backup_path}" ]] && rm -rf "${application_backup_path}"

    [[ -d "${installation_dir}" ]] && mv "${installation_dir}" "${application_backup_path}"
    [[ -d "${installation_dir}" ]] || mkdir -p "${installation_dir}"
    tar xf "${source_pack_path}" -C "${installation_dir}" --strip-components=1 2> /dev/null

    if [[ -s "${installation_dir}/bin/version.sh" ]]; then
        # remove file suffix with '.bat'
        find "${installation_dir}/bin/" -type f -name "*.bat" -exec rm -f {} \;

        [[ -d "${application_backup_path}" ]] && rm -rf "${application_backup_path}"
        fnBase_OperationProcedureResult "${c_red}${installation_dir%/}/${c_normal}"

        # copy newly downloaded pack to ~/Downloads
        local l_user_download_save_path=${l_user_download_save_path:-"${user_download_dir}/${source_pack_path##*/}"}
        if [[ -d "${user_download_dir}" && ! -f "${l_user_download_save_path}" ]]; then
            cp "${source_pack_path}" "${l_user_download_save_path}"
            chown "${login_user}" "${l_user_download_save_path}"
        fi
    else
        [[ -d "${installation_dir}" ]] && rm -rf "${installation_dir}"
        [[ "${is_existed}" -eq 1 ]] && mv "${application_backup_path}" "${installation_dir}"
        fnBase_OperationProcedureResult '' 1
    fi

    chown -R "${user_name}":"${group_name}" "${installation_dir}"
    if [[ -d "${installation_dir}/conf/" ]]; then
        chmod -R g+r "${installation_dir}/conf/"
        chmod g+x "${installation_dir}/conf/"
    fi
    [[ -n "${pack_save_path}" && -f "${pack_save_path}" ]] && rm -f "${pack_save_path}"
}


#########  2-5. Service Directive Setting  #########
fn_TomcatDirectiveConfiguration(){
    local l_sever_xml_path=${l_sever_xml_path:-"${installation_dir}/conf/server.xml"}
    if [[ -s "${l_sever_xml_path}" ]]; then
        fnBase_OperationPhaseStatement "server.xml configuration"

        # - HTTP Port
        fnBase_OperationProcedureStatement 'HTTP port'
        sed -r -i '/<.*?port="8080"/{s@^(.*port=")[^"]+(.*)$@\1'"${socket_port}"'\2 URIEncoding="UTF-8"@g;}' "${l_sever_xml_path}"
        fnBase_OperationProcedureResult "${socket_port}"

        # - Disable the WAR auto-deploy option.
        # <Host name="localhost"  appBase="webapps" unpackWARs="true" autoDeploy="true">
        fnBase_OperationProcedureStatement 'Disable the WAR auto-deploy option'
        sed -r -i '/autoDeploy/{s@^(.*autoDeploy=")[^"]+(.*)$@\1false\2@g;}' "${l_sever_xml_path}"
        fnBase_OperationProcedureResult 'false'
    fi

    # - /conf/tomcat-users.xml
    # <tomcat-users>
    #     <user username="admin" password="password" roles="..."/>
    # </tomcat-users>

    # - /webapps/host-manager/META-INF/context.xml

    # - /webapps/manager/META-INF/context.xml
}


#########  2-6. Service Script  #########
fn_ServiceScriptConfiguration(){
    fnBase_OperationProcedureStatement 'Service Script'
    # - init / service script
    local l_service_script_dir=''
    local l_service_script_path=''

    if fnBase_CommandExistIfCheck 'systemctl'; then
        l_service_script_dir='/etc/systemd/system'
        l_service_script_path="${l_service_script_dir}/${software_name}.service"

        [[ -f "${l_service_script_path}" ]] && fnBase_SystemServiceManagement "${l_service_script_path##*/}" 'stop'

        # https://unix.stackexchange.com/questions/207469/systemd-permission-issue-with-mkdir-execstartpre
        # https://serverfault.com/questions/779634/create-a-directory-under-var-run-at-boot
        echo -e "[Unit]\nDescription=Apache Tomcat Web Application Container\nAfter=network.target\n\n[Service]\nType=forking\nUser=${user_name}\nGroup=${group_name}\nUMask=0027\nRestartSec=10\nRestart=always\n\n# Environment=JAVA_HOME=/opt/OracleSEJDK\nEnvironment='JAVA_OPTS=-Djava.awt.headless=true -Djava.security.egd=file:/dev/./urandom'\nEnvironment=CATALINA_PID=${installation_dir}/temp/tomcat.pid\nEnvironment=CATALINA_HOME=${installation_dir}\nEnvironment=CATALINA_BASE=${installation_dir}\nEnvironment='CATALINA_OPTS=-Xms512M -Xmx1024M -server -XX:+UseParallelGC'\n\nExecStart=${installation_dir}/bin/startup.sh\nExecStop=${installation_dir}/bin/shutdown.sh\n\n[Install]\nWantedBy=multi-user.target" > "${l_service_script_path}"

        chmod 644 "${l_service_script_path}"
        systemctl daemon-reload 2> /dev/null
        fnBase_SystemServiceManagement "${l_service_script_path##*/}" 'start'
    fi

    fnBase_OperationProcedureResult "${l_service_script_path}"
}


#########  2-7. Security Enhancement  #########
fn_SecurityEnhancement(){
    [[ "${enable_firewall}" -eq 1 ]] && fnBase_OperationPhaseStatement 'Security Enhancement'

    # - firewall
    if [[ "${enable_firewall}" -eq 1 ]]; then
        fnBase_OperationProcedureStatement 'Firewall configuration'
        # $download_method "${firewall_configuration_script}" | bash -s -- -p "${socket_port}" -H "${remote_host_ip}" -s
        $download_method "${firewall_configuration_script}" | bash -s -- -p "${socket_port}" -s
        fnBase_OperationProcedureResult "${socket_port}"
    fi
}


#########  2-8. Post-installation Configuration  #########
fn_PostInstallationConfiguration(){
    # - direcitve setting
    fn_TomcatDirectiveConfiguration

    # - init / service script
    fn_ServiceScriptConfiguration

    # - security enhancement
    fn_SecurityEnhancement
}


#########  2-9. Operation Time Cost  #########
fn_TotalTimeCosting(){
    finish_time=$(date +'%s')        # End Time Of Operation
    total_time_cost=$((finish_time-start_time))   # Total Time Of Operation
    printf "\nTotal time cost is ${c_red}%s${c_normal} seconds!\n" "${total_time_cost}"
    echo -e "\nYou can visit ${c_yellow}http://127.0.0.1:${socket_port}${c_normal} in your web browser.\n"
}


#########  3. Executing Process  #########
fn_InitializationCheck

fn_UninstallOperation
fn_VersionComparasion
fn_Prerequisites
fn_DownloadAndDecompressOperation
fn_PostInstallationConfiguration
fn_TotalTimeCosting


#########  4. EXIT Singal Processing  #########
# trap "commands" EXIT # execute command when exit from shell
fn_TrapEXIT(){
    fnBase_UnsetVars
    unset oracle_official_site
    unset installation_dir
    unset socket_port
    unset is_existed
    unset version_check
    unset version_specify
    unset source_pack_path
    unset strict_mode_enable
    unset enable_firewall
    unset is_uninstall
    unset proxy_server_specify
}

trap fn_TrapEXIT EXIT


# Script End
