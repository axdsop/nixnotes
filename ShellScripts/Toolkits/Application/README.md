# Toolkits - Application

Application lists.

## TOC

1. [Contianer](#contianer)  
2. [Programming Language](#programming-language)  
3. [Web browser](#web-browser)  
4. [Text Editor](#text-editor)  
5. [Office Suites](#office-suites)  
6. [Data Science](#data-science)  
7. [Database](#database)  
8. [Web Server](#web-server)  
9. [Monitor](#monitor)  
10. [Security](#security)

## Contianer

* [Docker CE](./Container/Docker-CE.sh)

## Programming Language

* [Oracle SE JDK](./ProgrammingLanguage/OracleSEJDK.sh)
* [Golang](./ProgrammingLanguage/Golang.sh)
* [Node.js](./ProgrammingLanguage/Nodejs.sh)

## Web browser

* [Mozilla Firefox](./WebBrowser/MozillaFirefox.sh)
* [GNUzilla and IceCat](./WebBrowser/GNUzillaIceCat.sh)
* [SRWare Iron](./WebBrowser/SRWareIron.sh)
* [Tor browser](./WebBrowser/TorBrowser.sh)
<!-- * [Waterfox](./WebBrowser/Waterfox.sh) -->

## Text Editor

* [Atom](./TextEditor/AtomTextEditor.sh)
* [Visual Studio Code](./TextEditor/VisualStudioCode.sh)
* [VSCodium](./TextEditor/VSCodium.sh)
* [Sublime Text 3 Editor](./TextEditor/SublimeText.sh)
* [JetBrains PyCharm](./TextEditor/PyCharm.sh)

## Office Suites

* [Mozilla Thunderbird](./Office/MozillaThunderbird.sh)
* [FileZilla](./Office/FileZilla.sh)
* [LibreOffice](./Office/LibreOffice.sh)

## Data Science

* [Anaconda](./DataScience/Anaconda.sh)

## Database

* [MySQL/MariaDB/Percona](./Database/MySQLVariants.sh)
* [PostgreSQL](./Database/PostgreSQL.sh)
* [Redis](./Database/Redis.sh)
* [MongoDB](./Database/MongoDB.sh)

## Web Server

* [Apache Tomcat](./WebServer/ApacheTomcat.sh)
* [Nginx](./WebServer/NginxWebServer.sh)
* [OpenResty](./WebServer/OpenResty.sh)

## Monitor

* [Grafana](./Monitor/Grafana.sh)
* [Prometheus](./Monitor/Prometheus.sh)
* [Elastic Stack](./Monitor/ElasticStack.sh)

## Security

* [OpenVPN](./Security/OpenVPN.sh)
* [Google Authenticator](./Security/GoogleAuthenticator.sh)

<!-- End -->
