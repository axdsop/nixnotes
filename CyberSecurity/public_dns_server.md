# Public DNS Servers

DNS (Domain Name System) is a system which translates the domain names you enter in a browser to the IP addresses required to access those sites.

ISP will assign DNS servers while connecting to the internet, but these may not always be the best choice. Privacy and speed are two important reasons to try alternative ones.


## TOC

1. [Public DNS Lists](#public-dns-lists)  
2. [Details](#details)  
2.1 [AdGuard](#adguard)  
2.2 [CleanBrowsing](#cleanbrowsing)  
2.3 [Google](#google)  
2.4 [Cloudflare](#cloudflare)  
2.5 [Quad9](#quad9)  
2.6 [Cisco OpenDNS](#cisco-opendns)  
2.7 [Yandex](#yandex)  
3. [Shell Command](#shell-command)  
4. [Bibliography](#bibliography)  
5. [Change Log](#change-log)  


## Public DNS Lists


DNS Provider|Type|DNS(IPv4)|DNS(IPv6)|DNS-over-TLS|DNS-over-HTTPS|DNSCrypt|Comment
---|---|---|---|---|---|---|---
AdGuard|Default|94.140.14.14<br/>94.140.15.15|2a10:50c0::ad1:ff<br/>2a10:50c0::ad2:ff|dns.adguard-dns.com|https://dns.adguard-dns.com/dns-query|IPv4: 2.dnscrypt.default.ns1.adguard.com 94.140.14.14:5443|
AdGuard|Family protection|94.140.14.15<br/>94.140.15.16|2a10:50c0::bad1:ff<br/>2a10:50c0::bad2:ff|family.adguard-dns.com|https://family.adguard-dns.com/dns-query|IPv4: 2.dnscrypt.family.ns1.adguard.com  94.140.14.15:5443|Block adult content, add ad blocking and browsing security
CleanBrowsing|Family Filter|185.228.168.168<br/>185.228.169.168|2a0d:2a00:1::<br/>2a0d:2a00:2::|family-filter-dns.cleanbrowsing.org|https://doh.cleanbrowsing.org/doh/family-filter/|IPv4: cleanbrowsing.org 185.228.168.168:8443|Block adult, pornographic and explicit sites, including proxy & VPN domains and mixed content sites
CleanBrowsing|Adult Filter|185.228.168.10<br/>185.228.169.11|2a0d:2a00:1::1<br/>2a0d:2a00:2::1|adult-filter-dns.cleanbrowsing.org|https://doh.cleanbrowsing.org/doh/adult-filter/|IPv4: cleanbrowsing.org 185.228.168.10:8443|Block adult content and malicious and phishing domains
CleanBrowsing|Security Filter|185.228.168.9<br/>185.228.169.9|2a0d:2a00:1::2<br/>2a0d:2a00:2::2|security-filter-dns.cleanbrowsing.org|https://doh.cleanbrowsing.org/doh/security-filter/||Block phishing, spam and malicious domains
Google|Default|8.8.8.8<br/>8.8.4.4|2001:4860:4860::8888<br/>2001:4860:4860::8844|tls://dns.google|https://dns.google/dns-query||A free, global DNS resolution service that you can use as an alternative to your current DNS provider.
Cloudflare|Default|1.1.1.1<br/>1.0.0.1|2606:4700:4700::1111<br/>2606:4700:4700::1001|1dot1dot1dot1.cloudflare-dns.com|https://dns.cloudflare.com/dns-query||
Cloudflare|Malware blocking only|1.1.1.2<br/>1.0.0.2|2606:4700:4700::1112<br/>2606:4700:4700::1002|tls://security.cloudflare-dns.com|https://security.cloudflare-dns.com/dns-query||
Cloudflare|Malware and Adult blocking|1.1.1.3<br/>1.0.0.3|2606:4700:4700::1113<br/>2606:4700:4700::1003|tls://family.cloudflare-dns.com|https://family.cloudflare-dns.com/dns-query||
Quad9|Default|9.9.9.9<br/>149.112.112.112|2620:fe::fe<br/>2620:fe::9|tls://dns.quad9.net|https://dns.quad9.net/dns-query|IPv4: 2.dnscrypt-cert.quad9.net 9.9.9.9:8443|
Cisco OpenDNS|Standard|208.67.222.222<br/>208.67.220.220|2620:119:35::35<br/>2620:119:53::53||https://doh.opendns.com/dns-query|IPv4: 2.dnscrypt-cert.opendns.com 208.67.220.220| Protect device from malware
Cisco OpenDNS|FamilyShield|208.67.222.123<br/>208.67.220.123|||https://doh.familyshield.opendns.com/dns-query||Block adult contents
Yandex|Basic|77.88.8.8<br/>77.88.8.1|2a02:6b8::feed:0ff<br/>2a02:6b8:0:1::feed:0ff|||IPv4 2.dnscrypt-cert.browser.yandex.net 77.88.8.78:15353|Quick and reliable DNS
Yandex|Safe|77.88.8.88<br/>77.88.8.2|2a02:6b8::feed:bad<br/>2a02:6b8:0:1::feed:bad||||Protection from infected and fraudulent sites
Yandex|Family|77.88.8.7<br/>77.88.8.3|2a02:6b8::feed:a11<br/>2a02:6b8:0:1::feed:a11||||Protection from infected and fraudulent sites, block adult content and advertisements


## Details

### AdGuard

>[AdGuard DNS][adguard_dns] is a foolproof way to block Internet ads that does not require installing any applications. It is easy to use, absolutely free, easily set up on any device, and provides you with minimal necessary functions to block ads, counters, malicious websites, and adult content.

Default

Family protection

>Use the Family protection mode to block access to all websites with adult content and enforce safe search in the browser, in addition to the regular perks of ad blocking and browsing security.


### CleanBrowsing

More details in page [CleanBrowsing DNS Filters](https://cleanbrowsing.org/filters).


* Security Filter: Malicious domains blocked (phishing, malware).
* Adult Filter:  Adult domains blocked; Search Engines set to safe mode; +Security Filter
* Family Filter: Proxies, VPNs & Mixed Adult Content blocked; Youtube to safe mode; +Adult Filter

Family Filter

>Blocks access to all adult, pornographic and explicit sites. It also blocks proxy and VPN domains that are used to bypass the filters. Mixed content sites (like Reddit) are also blocked. Google, Bing and Youtube are set to the Safe Mode. Malicious and Phishing domains are blocked.

Adult Filter

>Blocks access to all adult, pornographic and explicit sites. It does not block proxy or VPNs, nor mixed-content sites. Sites like Reddit are allowed. Google and Bing are set to the Safe Mode. Malicious and Phishing domains are blocked.

Security Filter

>Blocks access to phishing, spam, malware and malicious domains. Our database of malicious domains is updated hourly and considered to be one of the best in the industry. Note that it does not block adult content.


* [Encrypted DNS - DNSCrypt Support](https://cleanbrowsing.org/guides/dnscrypt)
* [Encrypted DNS - DNS over HTTPS (DoH) Support](https://cleanbrowsing.org/guides/dnsoverhttps)
* [Encrypted DNS - DNS over TLS support](https://cleanbrowsing.org/guides/dnsovertls)


### Google

A free, global DNS resolution service that you can use as an alternative to your current DNS provider.

More details in page [Public DNS][google_dns].

* [DNS-over-TLS](https://developers.google.com/speed/public-dns/docs/dns-over-tls)
* [DNS-over-HTTPS (DoH)](https://developers.google.com/speed/public-dns/docs/doh/)


### Cloudflare

More details in page [1.1.1.1](https://1.1.1.1/dns/).

* [DNS over HTTPS](https://developers.cloudflare.com/1.1.1.1/dns-over-https/)
* [DNS over TLS](https://developers.cloudflare.com/1.1.1.1/dns-over-tls/)

### Quad9

More details in page [Quad9 DNS][quad9].

* [Does Quad9 implement DNSSEC?](https://www.quad9.net/faq/#Does_Quad9_implement_DNSSEC)
* [Does Quad9 support DNS over TLS?](https://www.quad9.net/faq/#Does_Quad9_support_DNS_over_TLS)
* [DoH with Quad9 DNS Servers](https://www.quad9.net/doh-quad9-dns-servers/)
* [DNSCrypt Now in Testing](https://www.quad9.net/privacy-dnscrypt-testing/)


### Cisco OpenDNS

More details in page [Use OpenDNS](https://www.opendns.com/home-internet-security/).

### Yandex

>[Yandex.DNS][yandex_dns] is a free, recursive DNS service. Yandex.DNS' servers are located in Russia, CIS countries, and Western Europe. Users' requests are processed by the nearest data center which provides high connection speeds.

More details in page [Technical details](https://dns.yandex.com/advanced/).

* Basic: Quick and reliable DNS
* Safe: Protection from virus and fraudulent content
* Family:Without adult content


## Shell Command

```bash
# list dns providers
sed -r -n '/^---/,/^$/{/^(---|$)/d;p}' | cut -d\| -f 1 | sort | uniq
```


## Bibliography

* [AdGuard - Known DNS Providers](https://adguard-dns.io/kb/general/dns-providers/)
* [DNS over HTTPS | curl](https://github.com/curl/curl/wiki/DNS-over-HTTPS)
* [Free and Public DNS Servers | lifewire](https://www.lifewire.com/free-and-public-dns-servers-2626062)

<!-- [Known DNS Providers | AdGuard Knowledgebase](https://kb.adguard.com/en/general/dns-providers) -->


## Change Log

* Oct 24, 2022 10:15 Mon ET
  * update DNS info list
* Oct 21, 2019 14:35 Mon ET
  * frist draft


[adguard_dns]:https://adguard.com/en/adguard-dns/overview.html
[yandex_dns]:https://dns.yandex.com/
[google_dns]:https://developers.google.com/speed/public-dns/
[quad9]:https://quad9.net/


<!-- End -->
