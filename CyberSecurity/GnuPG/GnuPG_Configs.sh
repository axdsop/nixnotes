#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails

# Target: GnuPG Configure Files Creatation
# Author: MaxdSre
# Date: Jul 18, 2019 Thu 14:35 ET - first draft
# Usage: bash $0 $GNUPGHOME


#########  0-1. GNUPGHOME Setting  #########
umask 022
gnugp_home_dir="${1:-}"
[[ -n "${gnugp_home_dir}" ]] || gnugp_home_dir="$HOME/.gnupg"
[[ -d "${gnugp_home_dir}" ]] || mkdir -m=700 -p "${gnugp_home_dir}"
# export GNUPGHOME="${gnugp_home_dir}"


#########  1. Configuration Files  #########
dirmngr_conf_path=""${gnugp_home_dir}/dirmngr.conf""
[[ -f "${dirmngr_conf_path}" ]] || tee "${dirmngr_conf_path}" 1> /dev/null << EOF
# https://www.gnupg.org/documentation/manuals/gnupg/Invoking-DIRMNGR.html

# --------------------------------- #
# dirmngr  if option --homedir dir is not used, the home directory defaults to ~/.gnupg.  https://www.gnupg.org/documentation/manuals/gnupg/Agent-Options.html

# Dirmngr-Options
# https://www.gnupg.org/documentation/manuals/gnupg/Dirmngr-Options.html

# Provide a certificate store to override the system default https://sks-keyservers.net/sks-keyservers.netCA.pem
# Use the root certificates in file for verification of the TLS certificates used with hkps (keyserver access over TLS).
hkp-cacert ${sks_keyserver_ca_path:-/etc/ssl/certs/sks-keyservers.netCA.pem}

# This is the server that gpg communicates with to receive keys, send keys, and search for keys.
keyserver hkps://hkps.pool.sks-keyservers.net

# Proxy
# https://gnupg.org/blog/20151224-gnupg-in-november-and-december.html
# use-tor
# keyserver hkp://jirk5u4osbsr34t5.onion

# honor-http-proxy
# http-proxy
EOF


# The default configuration file is named gpg-agent.conf and expected in the .gnupg directory directly below the home directory of the user.  https://www.gnupg.org/documentation/manuals/gnupg/Agent-Options.html
gpg_agent_conf_path="${gnugp_home_dir}/gpg-agent.conf"
[[ -f "${gpg_agent_conf_path}" ]] || tee "${gpg_agent_conf_path}" 1> /dev/null << EOF
# Agent-Options
# https://www.gnupg.org/documentation/manuals/gnupg/Agent-Options.html

default-cache-ttl 1800
max-cache-ttl 3600

allow-preset-passphrase
# /usr/bin/pinentry-curses
pinentry-program /usr/bin/pinentry-curses
# pinentry-mode loopback

# The OpenSSH Agent protocol is always enabled, but gpg-agent will only set the SSH_AUTH_SOCK variable if this flag is given.
# enable-ssh-support
# default-cache-ttl-ssh 1800
# max-cache-ttl-ssh 3600
EOF


# https://wiki.archlinux.org/index.php/GnuPG#Tips_and_tricks
# https://raw.githubusercontent.com/ioerror/duraconf/master/configs/gnupg/gpg.conf
# https://blog.tinned-software.net/create-gnupg-key-with-sub-keys-to-sign-encrypt-authenticate/
gpg_conf_path="${gnugp_home_dir}/gpg.conf"
[[ -f "${gpg_conf_path}" ]] || tee "${gpg_conf_path}" 1> /dev/null << EOF
# Option Summary
# https://www.gnupg.org/documentation/manuals/gnupg/GPG-Options.html

# --------------------------------- #
# 1. OpenPGP-Configuration-Options
# https://www.gnupg.org/documentation/manuals/gnupg/GPG-Configuration-Options.html
# gpg always requires the agent.
use-agent
# Assume that command line arguments are given as UTF-8 strings.
utf8-strings
# Add an "0x" to either to include an "0x" at the beginning of the key ID, as in 0x99242560. Note that this option is ignored if the option --with-colons is used.
keyid-format 0xlong
# Display the calculated validity of user IDs during key listings. Defaults to yes.
list-options show-uid-validity
# Display the calculated validity of the user IDs on the key that issued the signature.
verify-options show-uid-validity

# When verifying a signature made from a subkey, ensure that the cross certification "back signature" on the subkey is present and valid. This protects against a subtle attack against subkeys that can sign.
require-cross-certification

# Key Server
# keyserver hkps://hkps.pool.sks-keyservers.net

# Provide a certificate store to override the system default https://sks-keyservers.net/sks-keyservers.netCA.pem
# gpg: keyserver option 'ca-cert-file' is obsolete; please use 'hkp-cacert' in dirmngr.conf
# dirmngr --homedir dir  Set the name of the home directory to dir. If this option is not used, the home directory defaults to ~/.gnupg.  https://www.gnupg.org/documentation/manuals/gnupg/Agent-Options.html
#keyserver-options ca-cert-file=${sks_keyserver_ca_path:-/etc/ssl/certs/sks-keyservers.netCA.pem}

#keyserver-options http-proxy=socks5-hostname://127.0.0.1:9050
keyserver-options include-revoked
keyserver-options no-honor-keyserver-url

# Option verbose, debug, check-cert ca-cert-file have no more function since GnuPG 2.1. Use the dirmngr configuration options instead.


# --------------------------------- #
# 2. GPG-Key-related-Options
# https://www.gnupg.org/documentation/manuals/gnupg/GPG-Key-related-Options.html


# --------------------------------- #
# 3. GPG-Input-and-Output
# https://www.gnupg.org/documentation/manuals/gnupg/GPG-Input-and-Output.html

# Export the smallest key possible. This removes all signatures except the most recent self-signature on each user ID.
export-options export-minimal
# Print key fingerprint
with-fingerprint


# --------------------------------- #
# 4. OpenPGP-Protocol-Options
# https://www.gnupg.org/documentation/manuals/gnupg/OpenPGP-Options.html

# Limits the algorithms used
# list in gpg --version
personal-cipher-preferences AES256 AES192 AES
personal-digest-preferences SHA512 SHA384 SHA256
personal-compress-preferences ZLIB BZIP2 ZIP Uncompressed

# s2k passphrases for symmetric encryption
s2k-cipher-algo AES256
s2k-digest-algo SHA512
# how passphrases for symmetric encryption are mangled. 0 is a plain passphrase will be used, 1 adds a salt (which should not be used) to the passphrase, a 3 (the default) iterates the whole process a number of times (see --s2k-count).
s2k-mode 3
# This value may range between 1024 and 65011712 inclusive. --s2k-count is only meaningful if --s2k-mode is set to the default of 3.
s2k-count 35011712


# --------------------------------- #
# 5. Compliance-Options
# https://www.gnupg.org/documentation/manuals/gnupg/Compliance-Options.html


# --------------------------------- #
# 6. GPG-Esoteric-Options
# https://www.gnupg.org/documentation/manuals/gnupg/GPG-Esoteric-Options.html

# Set the list of default preferences
default-preference-list SHA512 SHA384 SHA256 AES256 AES192 AES ZLIB BZIP2 ZIP Uncompressed

# Set message digest algorithm used when signing a key.
cert-digest-algo SHA512

# Disable the passphrase cache used for symmetrical en- and decryption. This cache is based on the message specific salt value (cf. --s2k-mode). Add from version 2.2.7 https://github.com/gpg/gnupg/blob/master/NEWS
# no-symkey-cache

# Do not put the recipient key IDs into encrypted messages. This helps to hide the receivers of the message and is a limited countermeasure against traffic analysis.2 On the receiving side, it may slow down the decryption process because all available secret keys must be tried.
throw-keyids

# Avoid information leaked
no-emit-version
no-comments
EOF


#########  2. Files Permission Change  #########
find "${gnugp_home_dir}" -maxdepth 1 -type f -name '*.conf' -exec chmod 640 {} \;

# Script End