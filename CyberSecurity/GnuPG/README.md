# GNU Privacy Guard (GnuPG) Simple Tutorial

**Attention**: GitHub must use `gpg2` to generate keypair, otherwise [Signing commits with GPG](https://help.github.com/articles/signing-commits-with-gpg/) will not be verified.

Official documentation [Using the GNU Privacy Guard](https://www.gnupg.org/documentation/manuals/gnupg/).

WikiPedia introduction [GNU Privacy Guard](https://en.wikipedia.org/wiki/GNU_Privacy_Guard "WikiPedia").

GnuPG [homepage](https://gnupg.org/#sec-1) says:

>GnuPG is a complete and free implementation of the OpenPGP standard as defined by [RFC4880](https://www.ietf.org/rfc/rfc4880.txt) (also known as PGP). GnuPG allows you to encrypt and sign your data and communications; it features a versatile key management system, along with access modules for all kinds of public key directories.
>
>GnuPG, also known as *GPG*, is a command line tool with features for easy integration with other applications. A wealth of frontend applications and libraries are available. GnuPG also provides support for S/MIME and Secure Shell (ssh).

[Invoking GPG](https://www.gnupg.org/documentation/manuals/gnupg/Invoking-GPG.html#Invoking-GPG) says:

>There are two main versions of GnuPG: GnuPG `1.x` and GnuPG `2.x`. GnuPG 2.x supports modern encryption algorithms and thus should be preferred over GnuPG 1.x.

[Pretty Good Privacy](https://en.wikipedia.org/wiki/Pretty_Good_Privacy 'WikiPedia') (PGP) is an *encryption program* that provides cryptographic privacy and authentication for data communication.

## Operation Notes

1. [Introduction](./Notes/0-0_Introduction.md)
2. [Getting Latest Release](./Notes/1-0_ReleaseInfo.md)
3. [Configuration](./Notes/1-1_Configuration.md)
4. [Generation Method](./Notes/1-2_GenerationMethod.md)
5. [Operation Commands](./Notes/1-3_OperationCommand.md)
6. [Usage Example](./Notes/2-1_UsageExample.md)


## Shell Scripts

I write a [shell script](./GnuPG.sh) to automatically generate gpg key.

Usage

```bash
# help info
curl -fsL https://gitlab.com/axdsop/nixnotes/raw/master/CyberSecurity/GnuPG/GnuPG.sh | bash -s -- -h

# example
# curl -fsL https://gitlab.com/axdsop/nixnotes/raw/master/CyberSecurity/GnuPG/GnuPG.sh | bash -s -- -n 'User_test' -m 'email@test.com' -C 'Comment_test' -e 2y -t rsa -b 4096 -p 'passphraseXXXX' -a
```


## Bibliography

* [ArchLinux GnuPG](https://wiki.archlinux.org/index.php/GnuPG)
* [yubico - PGP](https://developers.yubico.com/PGP/)
* [OpenPGP Best Practices](https://riseup.net/en/security/message-security/openpgp/best-practices)
* OpenPGP: Create a New GnuPG Key ([part1](https://www.inovex.de/blog/openpgp-create-a-new-gnupg-key-1/), [part2](https://www.inovex.de/blog/openpgp-create-a-new-gnupg-key-2/))
* [itemis - OpenPGP on the Job](https://blogs.itemis.com/topic/openpgp)
* [What is a good general purpose GnuPG key setup?](https://security.stackexchange.com/questions/31594/what-is-a-good-general-purpose-gnupg-key-setup)
* [Can Elliptic Curve Cryptography be Trusted? A Brief Analysis of the Security of a Popular Cryptosystem](https://www.isaca.org/Journal/archives/2016/volume-3/Pages/can-elliptic-curve-cryptography-be-trusted.aspx)
* [Create GnuPG key with sub-keys to sign, encrypt, authenticate](https://blog.tinned-software.net/create-gnupg-key-with-sub-keys-to-sign-encrypt-authenticate/)
* [Using a GPG key for SSH Authentication](https://ryanlue.com/posts/2017-06-29-gpg-for-ssh-auth)
* [Serve Your GPG key Instead of an SSH key](https://www.linode.com/docs/security/authentication/gpg-key-for-ssh-authentication/#serve-your-gpg-key-instead-of-an-ssh-key)
* [GPG Tutorial | devdungeon](https://www.devdungeon.com/content/gpg-tutorial)


## Change Log

* Oct 24, 2019 09:45 Thu ET
  * comment `no-symkey-cache` which is added from version `2.2.4`
* Aug 12, 2019 10:48 Mon ET
  * fuction *GPG signature verification* code refactoring
* Aug 10, 2019 09:23 Sat ET
  * Update GPG version to `2.2.17`, add usage example
* Jul 17, 2019 15:03 Wed ET
  * Script optimization
* Jan 23, 2019 17:24 Wed ET
  * re-write documentation
* Mar 08, 2020 19:35 Sun ET
  * Update GPG version to `2.2.19`
* Mar 29, 2020 08:51 Sun ET
  * Add note about 'show-only-fpr-mbox'
* Apr 05, 2020 10:05 Sun ET
  * cutstom function GPG signature verification integrate download tool detection
* Apr 19, 2020 Sun 10:55 ET
  * Seperating sections



<!-- End -->