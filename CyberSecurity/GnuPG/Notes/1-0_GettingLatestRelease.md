# Getting GnuPG

Checking GnuPG online release version and local installed version.

## TOC

1. [Version Check](#version-check)  
1.1 [Online Release Version](#online-release-version)  
1.2 [Local Existed Version](#local-existed-version)  
2. [PGP Signature Verifying](#pgp-signature-verifying)  
3. [Installation](#installation)  
3.1 [For GnuPG](#for-gnupg)  
3.2 [For dirmngr](#for-dirmngr)  
4. [Change Log](#change-log)  


## Version Check

### Online Release Version

Online release version

```bash
download_method='wget -qO-'
[[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && download_method='curl -fsSL'

official_site='https://gnupg.org'
download_page="${official_site}/download/index.html"

# $download_method "${download_page}" | sed -r -n '/<tbody>/,/<\/tbody>/{/href=.*?GnuPG</,/<\/tr>/{/<td/{s@[[:space:]]*<[^>]+>[[:space:]]*@@g;p}}}' | sed ':a;N;$!ba;s@\n@|@g' | cut -d\| --output-delimiter=' ' -f 2,3
# 2.2.17 2019-07-09

$download_method "${download_page}" | sed -r -n '/id="sec-1-1"/,/id="sec-1-2"/{/<tbody>/,/<\/tbody>/{/GnuPG/,/<\/tr>/{/href=.*tar.*/{s@.*href="([^"]+)".*@'"${official_site}"'\1@g;};s@<\/tr>@,,@g;s@<[^>]*>@@g;p}}}' | sed ':a;N;$!ba;s@\n@|@g' | sed -r -n 's@\|?,,$@@g;s@\|?,,\|?@\n@g;p'
```

Name|Version|Date|Size|Tarball|Signature
---|---|---|---|---|---
GnuPG|2.2.20|2020-03-20|6627k|https://gnupg.org/ftp/gcrypt/gnupg/gnupg-2.2.20.tar.bz2|https://gnupg.org/ftp/gcrypt/gnupg/gnupg-2.2.20.tar.bz2.sig
GnuPG 1.4|1.4.23|2018-06-11|3661k|https://gnupg.org/ftp/gcrypt/gnupg/gnupg-1.4.23.tar.bz2|https://gnupg.org/ftp/gcrypt/gnupg/gnupg-1.4.23.tar.bz2.sig

<!-- GnuPG|2.2.19|2019-12-07|6596k|https://gnupg.org/ftp/gcrypt/gnupg/gnupg-2.2.19.tar.bz2|https://gnupg.org/ftp/gcrypt/gnupg/gnupg-2.2.19.tar.bz2.sig
GnuPG 1.4|1.4.23|2018-06-11|3661k|https://gnupg.org/ftp/gcrypt/gnupg/gnupg-1.4.23.tar.bz2|https://gnupg.org/ftp/gcrypt/gnupg/gnupg-1.4.23.tar.bz2.sig -->


### Local Existed Version

Local installed version

```bash
gpg2 --version
```

Output

```txt
gpg (GnuPG) 2.2.20
libgcrypt 1.8.5
Copyright (C) 2020 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

Home: /home/maxdsre/.gnupg
Supported algorithms:
Pubkey: RSA, ELG, DSA, ECDH, ECDSA, EDDSA
Cipher: IDEA, 3DES, CAST5, BLOWFISH, AES, AES192, AES256, TWOFISH,
        CAMELLIA128, CAMELLIA192, CAMELLIA256
Hash: SHA1, RIPEMD160, SHA256, SHA384, SHA512, SHA224
Compression: Uncompressed, ZIP, ZLIB, BZIP2
```


## PGP Signature Verifying

Using personal custom function [GPGSignatureVerification](/CyberSecurity/GnuPG/Notes/2-1_UsageExample.md#package-signature-verifying) to verify PGP signature.

Signature key comes from official documentation:

* [Integrity Check](https://gnupg.org/download/integrity_check.html)
* [Signature Key](https://gnupg.org/signature_key.html)

Usage

```bash
gz_image_name='gnupg-2.2.20.tar.bz2'

# fn_GPGSignatureVerification 'ISOPATH' 'SIGPATH' 'PGPPATH'
fn_GPGSignatureVerification "${gz_image_name}"{,.sig} 'https://gnupg.org/signature_key.html'
```

Output

```txt
Importing PGP key
gpg: keybox '/tmp/LWzmTnTX/gpg_import.gpg' created
gpg: key 249B39D24F25E3B6: public key "Werner Koch (dist sig)" imported
gpg: key 2071B08A33BD3F06: public key "NIIBE Yutaka (GnuPG Release Key) <gniibe@fsij.org>" imported
gpg: key 04376F3EE0856959: public key "David Shaw (GnuPG Release Signing Key)" imported
gpg: key BCEF7E294B092E28: public key "Andre Heinecke (Release Signing Key)" imported
gpg: key 53B620D01CE0C630: public key "Werner Koch (dist sig) <dd9jn@gnu.org>" imported
gpg: key 68B7AB8957548DCD: public key "Werner Koch (gnupg sig) <dd9jn@gnu.org>" imported
gpg: Total number processed: 6
gpg:               imported: 6

Verifying signature
gpg: Signature made Fri 20 Mar 2020 12:21:11 PM EDT
gpg:                using RSA key D8692123C4065DEA5E0F3AB5249B39D24F25E3B6
gpg: Good signature from "Werner Koch (dist sig)" [unknown]
gpg: WARNING: This key is not certified with a trusted signature!
gpg:          There is no indication that the signature belongs to the owner.
Primary key fingerprint: D869 2123 C406 5DEA 5E0F  3AB5 249B 39D2 4F25 E3B6

Temporary directory /tmp/LWzmTnTX has been removed.
```


## Installation

Installing via package manager.

### For GnuPG

```bash
# Debian / Ubuntu
sudo apt-get install -y gnupg2

# RHEL/CentOS
sudo yum install -y gnupg2

# Fedora
sudo dnf install -y gnupg2

# OpenSUSE
sudo zypper in -y gpg2

# Alpine Linux
sudo apk add gnupg

# Arch Linux
sudo pacman -S --noconfirm gnupg
```

### For dirmngr

```bash
# dirmngr is used for network access by gpg, gpgsm, and dirmngr-client, among other tools.
# rhel 7 / amzn2 has no dirmngr paceage
# https://pkgs.org/download/dirmngr


# dirmngr --version
dirmngr (GnuPG) 2.2.20
Copyright (C) 2020 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
```


## Change Log

* Apr 19, 2020 Sun 07:50 ET
  * first draft

[gnupg]: https://gnupg.org "The GNU Privacy Guard"

<!-- End -->