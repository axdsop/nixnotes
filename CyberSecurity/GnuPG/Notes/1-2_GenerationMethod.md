# GnuPG Key Generation Method

By default the key generated is **primary key**. It is recommended to just keep `Certify` capability for primary key while using *subkeys* for other capabilities of daily use. Primary secret key is `ultimately trusted` for creator by default.

## TOC

1. [Via interactive mode](#via-interactive-mode)  
2. [Via qucik mode](#via-qucik-mode)  
3. [Via unattended key generation (unsolved)](#via-unattended-key-generation-unsolved)  
4. [Change Log](#change-log)  


## Via interactive mode

The generation procedure is *interactive mode* be default. You may consider use [Unattended key generation][unattend_key_generation] to generate silently via `--batch`.

Using command `gpg2 --expert --full-gen-key` to generate GnuPG key.

```bash
# gpg-connect-agent reloadagent /bye 1> /dev/null
gpg-connect-agent killagent /bye 2> /dev/null

gpg2 --expert --full-gen-key
# gpg: key 0x7524759BB4133994 marked as ultimately trusted

secret_key_id=$(gpg2 --with-fingerprint --list-secret-key  2>&1 | sed -r -n '/^sec/{s@^[^\/]+/([^[:space:]]+).*$@\1@g;p;q}')

gpg2 --expert --edit-key "${secret_key_id}"
# add subkeys
gpg> addkey
# Signature key --> S
# Encryption key --> E
# Authentication key --> A

# save and quit
gpg> save

# Edit
# > passwd       # change the passphrase
# > clean        # compact any user ID that is no longer usable (e.g revoked or expired)
# > revkey       # revoke a key
# > addkey       # add a subkey to this key
# > expire       # change the key expiration time
# > adduid       # add additional names, comments, and email addresses
# > addphoto     # add photo to key (must be JPG, 240x288 recommended, enter full path to image when prompted)
```


## Via qucik mode

According to

* [4.1.3 How to manage your keys](https://www.gnupg.org/documentation/manuals/gnupg/OpenPGP-Key-Management.html)
* [serverfault - Add second sub-key to unattended GPG key](https://serverfault.com/questions/818289/add-second-sub-key-to-unattended-gpg-key#962553)


>--quick-gen-key (--quick-generate-key) user-id [algo [usage [expire]]]
>
>--quick-add-key fpr [algo [usage [expire]]]

```bash
# algo: rsa, rsa4096, dsa, elg, ed25519, cv25519
# usage: sign, auth, encr (encrypt)
# expire: never, none, seconds=N, Nd, Nw, Nm, Ny, YYYY-MM-DD, YYYYMMDDThhmmss

# cv25519 for encrypt, ed25519 for cert, sign, auth
```

Command example

```bash
# gpg-connect-agent help /bye

# gpg-connect-agent reloadagent /bye 1> /dev/null   # https://wiki.archlinux.org/index.php/GnuPG#Reload_the_agent
gpg-connect-agent killagent /bye 2> /dev/null

# --passphrase, --passphrase-fd, or --passphrase-file
# Note  that  since  Version  2.0  this passphrase is only used if the option --batch has also been given. Since Version 2.1 the --pinentry-mode also needs to be set to loopback.

passphrase='1Qaz@wSx'
user_val='User_test'
comment_val='Comment_test'
email_val='email@test.com'

# For RSA 4096
gpg2 --batch --passphrase "${passphrase}" --quick-gen-key "${user_val} (${comment_val}) <${email_val}>" rsa4096 cert 1y
fpr=$(gpg2 --list-options show-only-fpr-mbox --list-secret-keys | sed -r -n '$!d;s@^([^[:space:]]+).*@\1@g;p')

gpg2 --batch --pinentry-mode=loopback --passphrase "${passphrase}" --quick-add-key $fpr rsa4096 sign 1y
gpg2 --batch --pinentry-mode=loopback --passphrase "${passphrase}" --quick-add-key $fpr rsa4096 auth 1y
gpg2 --batch --pinentry-mode=loopback --passphrase "${passphrase}" --quick-add-key $fpr rsa4096 encrypt 1y


# For ECC
gpg2 --batch --passphrase "${passphrase}" --quick-gen-key "${user_val} (${comment_val}) <${email_val}>" ed25519 cert 1y
fpr=$(gpg2 --list-options show-only-fpr-mbox --list-secret-keys | sed -r -n '$!d;s@^([^[:space:]]+).*@\1@g;p')

gpg2 --batch --pinentry-mode=loopback --passphrase "${passphrase}" --quick-add-key $fpr ed25519 sign 1y
gpg2 --batch --pinentry-mode=loopback --passphrase "${passphrase}" --quick-add-key $fpr ed25519 auth 1y
gpg2 --batch --pinentry-mode=loopback --passphrase "${passphrase}" --quick-add-key $fpr cv25519 encrypt 1y
```

## Via unattended key generation (unsolved)

Details in official documentation [Unattended key generation][unattend_key_generation].

```bash
export GNUPGHOME="$(mktemp -d)"
cat > foo <<EOF
     %echo Generating a basic OpenPGP key
     Key-Type: RSA
     Key-Length: 4096
     Key-Usage: cert
     Subkey-Type: RSA
     Subkey-Length: 4096
     Subkey-Usage: sign
     Name-Real: Joe Tester
     Name-Comment: with stupid passphrase
     Name-Email: joe@foo.bar
     Expire-Date: 6m
     Passphrase: 1Qaz@wsX
     # Do a commit here, so that we can later print "done" :-)
     %commit
     %echo done
EOF


# Space or comma delimited list of key usages. Allowed values are 'encrypt', 'sign', and 'auth'. This is used to generate the key flags. Please make sure that the algorithm is capable of this usage. Note that OpenPGP requires that all primary keys are capable of certification, so no matter what usage is given here, the 'cert' flag will be on. If no 'Key-Usage' is specified and the 'Key-Type' is not 'default', all allowed usages for that particular algorithm are used; if it is not given but 'default' is used the usage will be 'sign'.

gpg --batch --generate-key foo

# gpg --list-secret-keys
```

For batch generate ECC key, see [How to batch generate ECC key](http://gnupg.10057.n7.nabble.com/How-to-batch-generate-ECC-key-td54242.html). Aliases for curve names see [ecc-curves.c](https://github.com/gpg/libgcrypt/blob/master/cipher/ecc-curves.c#L38).


## Change Log

* Apr 19, 2020 Sun 10:25 ET
  * first draft

[unattend_key_generation]: https://www.gnupg.org/documentation/manuals/gnupg/Unattended-GPG-key-generation.html

<!-- End -->