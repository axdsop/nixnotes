# GnuPG Introduction

Official documentation [Using the GNU Privacy Guard](https://www.gnupg.org/documentation/manuals/gnupg/).

WikiPedia introduction [GNU Privacy Guard](https://en.wikipedia.org/wiki/GNU_Privacy_Guard "WikiPedia").

GnuPG [homepage](https://gnupg.org/#sec-1) says:

>GnuPG is a complete and free implementation of the OpenPGP standard as defined by [RFC4880](https://www.ietf.org/rfc/rfc4880.txt) (also known as PGP). GnuPG allows you to encrypt and sign your data and communications; it features a versatile key management system, along with access modules for all kinds of public key directories.
>
>GnuPG, also known as *GPG*, is a command line tool with features for easy integration with other applications. A wealth of frontend applications and libraries are available. GnuPG also provides support for S/MIME and Secure Shell (ssh).

[Invoking GPG](https://www.gnupg.org/documentation/manuals/gnupg/Invoking-GPG.html#Invoking-GPG) says:

>There are two main versions of GnuPG: GnuPG `1.x` and GnuPG `2.x`. GnuPG 2.x supports modern encryption algorithms and thus should be preferred over GnuPG 1.x.

[Pretty Good Privacy](https://en.wikipedia.org/wiki/Pretty_Good_Privacy 'WikiPedia') (PGP) is an *encryption program* that provides cryptographic privacy and authentication for data communication.


## TOC

1. [Key Algorithms](#key-algorithms)  
2. [Environment Variables](#environment-variables)  
2.1 [GNUPGHOME](#gnupghome)  
2.2 [GPG-AGENT](#gpg-agent)  
3. [Change Log](#change-log)  


## Key Algorithms

[GnuPG][gnupg] supports different actions for a key.

Constant|Character|Action|Explanation
---|---|---|---
PUBKEY_USAGE_CERT|C|Certify | manage key
PUBKEY_USAGE_ENC|E|Encrypt | encrypt data
PUBKEY_USAGE_SIG|S|Sign | sign data
PUBKEY_USAGE_AUTH|A|Authenticate | authentication

Constant names see GitHub [code](https://github.com/gpg/gnupg/blob/master/g10/packet.h#L49).

>By default, the primary key has the `Certify` and the `Sign` capabilities. The `Encrypt` capability is provided by a subkey. Subkeys are bound to the master key pair.

[GnuPG][gnupg] provides 4 kinds of key algorithms: [RSA][rsa], [DSA][dsa], [ElGamal][elgamal], [ECC][ecc].

Key algorithm | Possible actions | Default allowed
---|---|---
ECDSA/EdDSA | Sign, Certify, Encrypt, Authenticate | Sign, Certify
RSA | Sign, Certify, Encrypt, Authenticate | Sign, Certify, Encrypt
DSA | Sign, Certify, Authenticate | Sign, Certify


List available elliptic curves

```bash
openssl ecparam -list_curves

gpg --with-colons --list-config curve
# cfg:curve:cv25519;ed25519;nistp256;nistp384;nistp521;brainpoolP256r1;brainpoolP384r1;brainpoolP512r1;secp256k1
```

For elliptic curves, `cv25519` for *encrypt*, `ed25519` for *cert*, *sign*, *auth*.

## Environment Variables

### GNUPGHOME

`~/.gnupg` is the default home directory which is used if neither the environment variable `GNUPGHOME` nor the option `--homedir` is given. Details in [Ephemeral home directories](https://www.gnupg.org/documentation/manuals/gnupg/Ephemeral-home-directories.html).

### GPG-AGENT

Before importing private key, you must execute `export GPG_TTY=$(tty)`. Otherwise it will prompt error info

>error sending to agent: Inappropriate ioctl for device

More details in official documentation [Invoking GPG-AGENT](https://www.gnupg.org/documentation/manuals/gnupg/Invoking-GPG_002dAGENT.html).

```bash
# When calling the gpg-agent component gpg sends a set of environment variables to gpg-agent. The names of these variables can be listed using the command:
gpg-connect-agent 'getinfo std_env_names' /bye | awk '$1=="D" {print $2}'
```


## Change Log

* Apr 19, 2020 Sun 10:45 ET
  * first draft

[gnupg]: https://gnupg.org "The GNU Privacy Guard"
[rsa]: https://en.wikipedia.org/wiki/RSA_(cryptosystem) "RSA (Rivest–Shamir–Adleman)"
[dsa]: https://en.wikipedia.org/wiki/Digital_Signature_Algorithm "Digital Signature Algorithm"
[elgamal]: https://en.wikipedia.org/wiki/ElGamal_encryption "ElGamal encryption system"
[ecc]: https://en.wikipedia.org/wiki/Elliptic-curve_cryptography "Elliptic-curve cryptography"

<!-- End -->