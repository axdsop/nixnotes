# Getting GnuPG

Checking GnuPG online release version and local installed version.

## TOC

1. [Version Check](#version-check)  
1.1 [Online Release Version](#online-release-version)  
1.2 [Local Existed Version](#local-existed-version)  
2. [PGP Signature Verifying](#pgp-signature-verifying)  
3. [Installation](#installation)  
3.1 [For GnuPG](#for-gnupg)  
3.2 [For dirmngr](#for-dirmngr)  
4. [Change Log](#change-log)  


## Version Check

### Online Release Version

Online release version

```bash
download_method='wget -qO-'
[[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && download_method='curl -fsSL'

official_site='https://gnupg.org'
download_page="${official_site}/download/index.html"

# $download_method "${download_page}" | sed -r -n '/<tbody>/,/<\/tbody>/{/href=.*?GnuPG</,/<\/tr>/{/<td/{s@[[:space:]]*<[^>]+>[[:space:]]*@@g;p}}}' | sed ':a;N;$!ba;s@\n@|@g' | cut -d\| --output-delimiter=' ' -f 2,3
# 2.2.17 2019-07-09

$download_method "${download_page}" | sed -r -n '/id="sec-1-1"/,/id="sec-1-2"/{/<tbody>/,/<\/tbody>/{/GnuPG/,/<\/tr>/{/href=.*tar.*/{s@.*href="([^"]+)".*@'"${official_site}"'\1@g;};s@<\/tr>@,,@g;s@<[^>]*>@@g;p}}}' | sed ':a;N;$!ba;s@\n@|@g' | sed -r -n 's@\|?,,$@@g;s@\|?,,\|?@\n@g;p'
```

Name|Version|Date|Size|Tarball|Signature
---|---|---|---|---|---
GnuPG|2.4.3|2023-07-04|7179k|https://gnupg.org/ftp/gcrypt/gnupg/gnupg-2.4.3.tar.bz2|https://gnupg.org/ftp/gcrypt/gnupg/gnupg-2.4.3.tar.bz2.sig
GnuPG Desktop|2.4.3.0|2023-07-14|214M|https://gnupg.orghttps://download.gnupg.com/files/gnupg/gnupg-desktop-2.4.3.0.tar.bz2|https://gnupg.orghttps://download.gnupg.com/files/gnupg/gnupg-desktop-2.4.3.0.tar.bz2.sig
GnuPG 1.4|1.4.23|2018-06-11|3661k|https://gnupg.org/ftp/gcrypt/gnupg/gnupg-1.4.23.tar.bz2|https://gnupg.org/ftp/gcrypt/gnupg/gnupg-1.4.23.tar.bz2.sig

<!-- GnuPG|2.4.0|2022-12-16|7487k|https://gnupg.org/ftp/gcrypt/gnupg/gnupg-2.4.0.tar.bz2|https://gnupg.org/ftp/gcrypt/gnupg/gnupg-2.4.0.tar.bz2.sig
GnuPG (LTS)|2.2.41|2022-12-09|7142k|https://gnupg.org/ftp/gcrypt/gnupg/gnupg-2.2.41.tar.bz2|https://gnupg.org/ftp/gcrypt/gnupg/gnupg-2.2.41.tar.bz2.sig
GnuPG Desktop|2.4.0.0|2022-12-19|170M|https://gnupg.orghttps://download.gnupg.com/files/gnupg/gnupg-desktop-2.4.0.0.tar.bz2|https://gnupg.orghttps://download.gnupg.com/files/gnupg/gnupg-desktop-2.4.0.0.tar.bz2.sig
GnuPG 1.4|1.4.23|2018-06-11|3661k|https://gnupg.org/ftp/gcrypt/gnupg/gnupg-1.4.23.tar.bz2|https://gnupg.org/ftp/gcrypt/gnupg/gnupg-1.4.23.tar.bz2.sig -->

<!-- GnuPG|2.2.20|2020-03-20|6627k|https://gnupg.org/ftp/gcrypt/gnupg/gnupg-2.2.20.tar.bz2|https://gnupg.org/ftp/gcrypt/gnupg/gnupg-2.2.20.tar.bz2.sig
GnuPG 1.4|1.4.23|2018-06-11|3661k|https://gnupg.org/ftp/gcrypt/gnupg/gnupg-1.4.23.tar.bz2|https://gnupg.org/ftp/gcrypt/gnupg/gnupg-1.4.23.tar.bz2.sig -->


### Local Existed Version

Local installed version

```bash
gpg2 --version
```

Output

```txt
# platform: Arch Linux

gpg (GnuPG) 2.2.40
libgcrypt 1.10.1-unknown
Copyright (C) 2022 g10 Code GmbH
License GNU GPL-3.0-or-later <https://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

Home: /home/maxdsre/.gnupg
Supported algorithms:
Pubkey: RSA, ELG, DSA, ECDH, ECDSA, EDDSA
Cipher: IDEA, 3DES, CAST5, BLOWFISH, AES, AES192, AES256, TWOFISH,
        CAMELLIA128, CAMELLIA192, CAMELLIA256
Hash: SHA1, RIPEMD160, SHA256, SHA384, SHA512, SHA224
Compression: Uncompressed, ZIP, ZLIB, BZIP2

# platform: MacOS

gpg (GnuPG) 2.4.3
libgcrypt 1.10.3
Copyright (C) 2023 g10 Code GmbH
License GNU GPL-3.0-or-later <https://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

Home: /Users/maxdsre/.gnupg
Supported algorithms:
Pubkey: RSA, ELG, DSA, ECDH, ECDSA, EDDSA
Cipher: IDEA, 3DES, CAST5, BLOWFISH, AES, AES192, AES256, TWOFISH,
        CAMELLIA128, CAMELLIA192, CAMELLIA256
Hash: SHA1, RIPEMD160, SHA256, SHA384, SHA512, SHA224
Compression: Uncompressed, ZIP, ZLIB, BZIP2
```


## PGP Signature Verifying

Using personal custom function [GPGSignatureVerification](/CyberSecurity/GnuPG/Notes/2-1_UsageExample.md#package-signature-verifying) to verify PGP signature.

Signature key comes from official documentation:

* [Integrity Check](https://gnupg.org/download/integrity_check.html)
* [Signature Key](https://gnupg.org/signature_key.html)

Usage

```bash
gz_image_name='gnupg-2.4.0.tar.bz2'

# fn_GPGSignatureVerification 'ISOPATH' 'SIGPATH' 'PGPPATH'
fn_GPGSignatureVerification "${gz_image_name}"{,.sig} 'https://gnupg.org/signature_key.html'
```

Output

For version *2.4.3*

```txt
Importing PGP key
gpg: keybox '/tmp/6KR9fumU/gpg_import.gpg' created
gpg: key 0xBCEF7E294B092E28: 1 signature not checked due to a missing key
gpg: /home/chuchu/.gnupg/trustdb.gpg: trustdb created
gpg: key 0xBCEF7E294B092E28: public key "Andre Heinecke (Release Signing Key)" imported
gpg: key 0x528897B826403ADA: 4 signatures not checked due to missing keys
gpg: key 0x528897B826403ADA: public key "Werner Koch (dist signing 2020)" imported
gpg: key 0xE98E9B2D19C6C8BD: 2 signatures not checked due to missing keys
gpg: key 0xE98E9B2D19C6C8BD: public key "Niibe Yutaka (GnuPG Release Key)" imported
gpg: key 0x549E695E905BA208: 1 signature not checked due to a missing key
gpg: key 0x549E695E905BA208: public key "GnuPG.com (Release Signing Key 2021)" imported
gpg: Total number processed: 4
gpg:               imported: 4
gpg: no ultimately trusted keys found

Verifying signature
gpg: Signature made Tue 04 Jul 2023 07:24:57 AM PDT
gpg:                using EDDSA key 6DAA6E64A76D2840571B4902528897B826403ADA
gpg: Good signature from "Werner Koch (dist signing 2020)" [unknown]
gpg: WARNING: This key is not certified with a trusted signature!
gpg:          There is no indication that the signature belongs to the owner.
Primary key fingerprint: 6DAA 6E64 A76D 2840 571B  4902 5288 97B8 2640 3ADA

Temporary directory /tmp/6KR9fumU has been removed.
```

For version *2.4.0*

```txt
Importing PGP key
gpg: keybox '/tmp/MiuChOWK/gpg_import.gpg' created
gpg: key 0xBCEF7E294B092E28: 1 signature not checked due to a missing key
gpg: key 0xBCEF7E294B092E28: public key "Andre Heinecke (Release Signing Key)" imported
gpg: key 0x528897B826403ADA: 4 signatures not checked due to missing keys
gpg: key 0x528897B826403ADA: public key "Werner Koch (dist signing 2020)" imported
gpg: key 0xE98E9B2D19C6C8BD: 2 signatures not checked due to missing keys
gpg: key 0xE98E9B2D19C6C8BD: public key "Niibe Yutaka (GnuPG Release Key)" imported
gpg: key 0x549E695E905BA208: 1 signature not checked due to a missing key
gpg: key 0x549E695E905BA208: public key "GnuPG.com (Release Signing Key 2021)" imported
gpg: Total number processed: 4
gpg:               imported: 4
gpg: no ultimately trusted keys found

Verifying signature
gpg: Signature made Fri 16 Dec 2022 12:24:40 PM EST
gpg:                using EDDSA key 6DAA6E64A76D2840571B4902528897B826403ADA
gpg: Good signature from "Werner Koch (dist signing 2020)" [unknown]
gpg: WARNING: This key is not certified with a trusted signature!
gpg:          There is no indication that the signature belongs to the owner.
Primary key fingerprint: 6DAA 6E64 A76D 2840 571B  4902 5288 97B8 2640 3ADA

Temporary directory /tmp/MiuChOWK has been removed.
```


## Installation

Installing via package manager.

### For GnuPG

MacOS

```bash
brew install gnupg
```

GNU/Linux

```bash
# Debian / Ubuntu
sudo apt-get install -y gnupg2

# RHEL/CentOS
sudo yum install -y gnupg2

# Fedora
sudo dnf install -y gnupg2

# OpenSUSE
sudo zypper in -y gpg2

# Alpine Linux
sudo apk add gnupg

# Arch Linux
sudo pacman -S --noconfirm gnupg
```

### For dirmngr

```bash
# dirmngr is used for network access by gpg, gpgsm, and dirmngr-client, among other tools.
# rhel 7 / amzn2 has no dirmngr paceage
# https://pkgs.org/download/dirmngr


# dirmngr --version
dirmngr (GnuPG) 2.2.40
Copyright (C) 2022 g10 Code GmbH
License GNU GPL-3.0-or-later <https://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
```


## Change Log

* Jan 03, 2023 Tue 13:20
  * update release version
* Apr 19, 2020 Sun 07:50 ET
  * first draft


<!-- End -->