# GnuPG Operation Command

[GnuPG][gnupg] operation command sample, key generation procedure see note [Generation Method](./1-2_GenerationMethod.md).

## TOC

1. [Get key info](#get-key-info)  
2. [Key check](#key-check)  
3. [Key export](#key-export)  
4. [Sending key to keyserver](#sending-key-to-keyserver)  
5. [Reimporting subkey](#reimporting-subkey)  
6. [Cache passphrase](#cache-passphrase)  
7. [Change Passphrase](#change-passphrase)  
8. [Sign file](#sign-file)  
9. [Encrypt](#encrypt)  
10. [Change Log](#change-log)  


## Get key info

```bash
# List the specified secret keys.
gpg2 -K
gpg2 --list-secret-keys

# sec => 'SECret key'
# ssb => 'Secret SuBkey'
# pub => 'PUBlic key'
# sub => 'public SUBkey'

# get key id
gpg2 --with-fingerprint --list-secret-key --keyid-format LONG

# get primary key and sub-key fingerprint
gpg2 --list-keys --with-colons | awk -F: '/fpr:/ {print $(NF-1)}'

# get latest generated key fingerprint
# Attention: --list-option "show-only-fpr-mbox" which is added from version GnuPG 2.2.12 (Dec 14, 2018), and Ubuntu 18.04 uses GnuGP version 2.2.4 <Dec 14, 2018>
gpg2 --list-options show-only-fpr-mbox --list-secret-keys | sed -r -n '$!d;s@^([^[:space:]]+).*@\1@g;p'
```


## Key check

```bash
# Command 'hokey' not found, but can be installed with: apt/pacman hopenpgp-tools
gpg2 --export --no-armor "${secret_key_id}" | hokey lint
```

## Key export

```bash
# export the public key  --export-options export-clean,export-minimal
gpg2 --export --armor -o ecc_pub.asc "${secret_key_id}"

# export the private key
gpg2 --export-secret-keys --armor -o ecc_sec.asc "${secret_key_id}"
# export the secret subkeys
gpg2 --export-secret-subkeys --armor -o ecc_sec_sub.asc "${secret_key_id}"

# get key id from public key file
gpg2 --show-keys ecc_pub.asc
gpg2 --with-fingerprint ecc_pub.asc

# get key id from secret key file
gpg2 --show-keys ecc_sec.asc
```

Generating revocation key

```bash
# interactive
# https://debian-administration.org/article/450/Generating_a_revocation_certificate_with_gpg
gpg2 --gen-revoke --armor -o ecc_revocation_cert.asc "${secret_key_id}"

# non-interactive
# generating revocation certs non-interactively
# https://lists.gnupg.org/pipermail/gnupg-users/2015-May/053559.html
# https://lists.gnupg.org/pipermail/gnupg-users/2015-May/053562.html
echo -e "Y\n0\n\nY\n" | gpg2 --gen-revoke --armor --command-fd 0 -o ecc_revocation_cert.asc "${secret_key_id}"
```

## Sending key to keyserver

```bash
# - Send public keys (optional)
gpg2 --send-keys "${secret_key_id}"
gpg2 --search "${secret_key_id}"
gpg2 --recv-keys "${secret_key_id}"
gpg2 --refresh-keys

# - Revoke keys (optional)
gpg2 --import ecc_revocation_cert.asc
gpg2 --send-keys "${secret_key_id}"
```

## Reimporting subkey

```bash
# - Delete
gpg2 --delete-secret-keys "${secret_key_id}"

# Keep your primary key entirely offline
# https://riseup.net/en/security/message-security/openpgp/best-practices#keep-your-primary-key-entirely-offline
mv "${gnupg_home}" "${gnupg_home_bak}"

# restart the agent, or it will prompt error 'error sending to agent: No such file or directory', as it need directory '~/.gnupg/private-keys-v1.d'
# https://stackoverflow.com/questions/44837437/gpg2-import-of-gpg1-secret-key-fails-gpg-2-1-15-ubuntu-17-10
# https://wiki.archlinux.org/index.php/GnuPG#Reload_the_agent
# gpg-connect-agent reloadagent /bye 1> /dev/null
gpg-connect-agent killagent /bye 1> /dev/null

# - Reimport (secret sub keys)
export GPG_TTY=$(tty)
gpg2 --import ecc_sec_sub.asc

# Starting with GnuPG 2.1.0 the use of gpg-agent and pinentry is required, which may break backwards compatibility for passphrases piped in from STDIN using the --passphrase-fd 0 commandline option.
# https://wiki.archlinux.org/index.php/GnuPG#Unattended_passphrase
# echo '1Qaz@Wsx' | gpg2 --import --batch --yes --passphrase-fd 0 ecc_sec_sub.asc

# Don't use this option (--passphrase-file) if you can avoid it.
# gpg2 --import --pinentry-mode loopback --passphrase-file=/tmp/password_file ecc_sec_sub.asc

cp "${gnupg_home_bak}/*.conf" "${gnupg_home}"

# reimport public keyring
gpg2 --homedir "${gnupg_home_bak}" --export | gpg2 --import

# reimport trust db
gpg2 --homedir "${gnupg_home_bak}" --export-ownertrust | gpg2 --import-ownertrust

# remove backup GPG directory, which will clear *all* secret keys
rm -rf "${gnupg_home_bak}"


gpg2 --list-secret-keys "${secret_key_id}"
# Note the hash (#) after the sec tag which indicates that the primary key is currently not usable.
```

## Cache passphrase

```bash
# cache passphrase for the whole session,  The passphrase will be stored until gpg-agent is restarted. If you set up default-cache-ttl value, it will take precedence.
# https://www.gnupg.org/documentation/manuals/gnupg/gpg_002dpreset_002dpassphrase.html
keygrip=$(gpg2 --with-keygrip -K 2>&1 | sed -r -n '/^sec/,/^uid/{/keygrip/I{s@^[^=]+=[[:space:]]*([^[:space:]]*).*$@\1@g;p;q}}')
/usr/lib/gnupg/gpg-preset-passphrase --preset "${keygrip}"
```

## Change Passphrase

Ubuntu official doc [Changing your Passphrase](https://help.ubuntu.com/community/GnuPrivacyGuardHowto#Changing_your_Passphrase)

```sh
# - import secret key, then

gpg2 --edit-key "${secret_key_id}"

gpg> passwd

# Enter the current passphrase when prompted.
# Enter the new passphrase twice when prompted.

gpg> save
```

If you want to change expire date

```sh
gpg2 --edit-key "${secret_key_id}"

# list sec & sub keys
gpg> list

# for primary key
gpg> expire
# Changing expiration time for the primary key.

# for subkeys
# select multiple subkeys
gpg> key 1
gpg> key 2
gpg> key 3
gpg> expire
# Are you sure you want to change the expiration time for multiple subkeys? (y/N)

gpg> save
```

Export

```sh
# secret_key_id=${secret_key_id:-}
secret_key_id=$(gpg2 --list-secret-keys | sed -r -n '/^sec/{s@^[^\/]+\/([^[:space:]]+).*$@\1@g;p;q}')

gpg_key_type=${gpg_key_type:-'ecc'}
gpg_key_info=$(gpg2 --with-fingerprint --list-secret-key --keyid-format LONG "${secret_key_id}" 2> /dev/null)
email_info=$(sed -r -n '/^uid[[:space:]]+/{s@^[^<]+<([^>]*).*$@\1@g;p}' <<< "${gpg_key_info}")
prefix_format="${email_info}_$(date +'%Y-%m-%d')_${gpg_key_type^^}"

# export the public key  --export-options export-clean,export-minimal
gpg2 --export --armor -o ${prefix_format}_pub.asc "${secret_key_id}"
# export the private key
gpg2 --export-secret-keys --armor -o ${prefix_format}_sec.asc "${secret_key_id}"
# export the secret subkeys
gpg2 --export-secret-subkeys --armor -o ${prefix_format}_sec_sub.asc "${secret_key_id}"
# generate revocation key
echo -e "Y\n0\n\nY\n" | gpg2 --gen-revoke --armor --command-fd 0 -o ${prefix_format}_revocation_cert.asc "${secret_key_id}"
```

## Sign file

```bash
gpg2 --detach-sign /tmp/aaa  # /tmp/aaa.sig
gpg2 --verify /tmp/aaa.sig

gpg2 --detach-sign -a /tmp/aaa  # /tmp/aaa.asc
gpg2 --verify /tmp/aaa.asc
```

## Encrypt

```bash
gpg2 -r flower@gmail.com -a --encrypt /tmp/aaa # /tmp/aaa.asc

gpg2 --output /tmp/test --decrypt aaa.asc # decrypt file content to file /tmp/test
```


## Change Log

* Apr 19, 2020 Sun 10:30 ET
  * first draft


[gnupg]: https://gnupg.org "The GNU Privacy Guard"

<!-- End -->