# GnuPG Usage Example

## TOC

1. [Package Signature Verifying](#package-signature-verifying)  
1.1 [Custom Function](#custom-function)  
1.2 [Example](#example)  
2. [Change Log](#change-log)  

## Package Signature Verifying

![manjaro check download pipeline](https://manjaro.org/img/download/check_download_pipeline.png)

### Custom Function

```bash
# custom function, all operation is in ephemeral dir under /tmp/

fn_GPGSignatureVerification(){
    # https://gitlab.com/axdsop/nixnotes/tree/master/CyberSecurity/GnuPG#package-signature-verifying

    # Attention: if GPG key is included in sig file, then package file and sig file must in same dir, because gpg assumes package file (signed data) is saved in the same path. Otherwise it will prompt error info "gpg: no signed data", "gpg: can't hash datafile: No data" while import key

    local l_file_path="${1:-}"
    local l_sig_path="${2:-}"
    local l_gpg_path="${3:-}"
    local l_download_tool='wget -qO-'
    [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && l_download_tool='curl -fsSL'

    if [[ -n "${l_file_path}" && -f "${l_file_path}" ]]; then
        # echo -e "File: \e[33m${l_file_path}\e[0m\nSig: \e[33m${l_sig_path}\e[0m\n"
        local l_gpg_temp_save_dir=$(mktemp -d -t XXXXXXXX)
        local l_gpg_import_path="${l_gpg_temp_save_dir}/gpg_import.gpg"
        local l_file_dir="$(dirname "${l_file_path}")"
        local l_sig_need_remove=${l_sig_need_remove:-0}

        echo -e "\e[33mImporting PGP key\e[0m"

        if [[ -n "${l_gpg_path}" ]]; then
            local l_gpg_need_remove=${l_gpg_need_remove:-0}

            if [[ "${l_gpg_path}" =~ ^https?:// ]]; then
                local l_gpg_path_temp="${l_gpg_temp_save_dir}/keyring.gpg"
                $l_download_tool "${l_gpg_path}" > "${l_gpg_path_temp}"
                l_gpg_path="${l_gpg_path_temp}"
                l_gpg_need_remove=1
            fi
            # import gpg key
            gpg --no-default-keyring --keyring "${l_gpg_import_path}" --import "${l_gpg_path}"
             # 2> /dev/null

            [[ "${l_gpg_need_remove}" -eq 1 && -f "${l_gpg_path}" ]] && rm -f "${l_gpg_path}"

            if [[ "${l_sig_path}" =~ ^https?:// ]]; then
                local l_sig_path_temp="${l_gpg_temp_save_dir}/${l_sig_path##*/}"
                # local l_sig_path_temp="${l_file_dir}/${l_sig_path##*/}"
                $l_download_tool "${l_sig_path}" > "${l_sig_path_temp}"
                l_sig_path="${l_sig_path_temp}"
                l_sig_need_remove=1
            fi

        else
            # gpg key included in .sig file
            if [[ "${l_sig_path}" =~ ^https?:// ]]; then
                # keep .sig and package file (signed data) in same dir
                # local l_sig_path_temp="${l_gpg_temp_save_dir}/${l_sig_path##*/}"
                local l_sig_path_temp="${l_file_dir}/${l_sig_path##*/}"
                $l_download_tool "${l_sig_path}" > "${l_sig_path_temp}"
                l_sig_path="${l_sig_path_temp}"
                l_sig_need_remove=1
            fi
            # import gpg key
            # gpg: assuming signed data in '/tmp/gnupg-2.2.17.tar.bz2'
            gpg --no-default-keyring --keyring "${l_gpg_import_path}" --keyserver-options auto-key-retrieve --verify "${l_sig_path}"
        fi

        echo -e "\n\e[33mVerifying signature\e[0m"
        gpg --no-default-keyring --keyring "${l_gpg_import_path}" --verify "${l_sig_path}" "${l_file_path}"
         # 2> /dev/null | sed -r -n '/Good signature/{p}'

        [[ "${l_sig_need_remove}" -eq 1 && -f "${l_sig_path}" ]] && rm -f "${l_sig_path}"

        if [[ -d "${l_gpg_temp_save_dir}" ]]; then
            rm -rf "${l_gpg_temp_save_dir}"
            echo -e "\nTemporary directory \e[33m${l_gpg_temp_save_dir}\e[0m has been removed."
        fi
    else
        echo "Usage: GPGSignatureVerification 'PACK' 'PACK.sig' ['GPG KEY']"
    fi
}
# 2>&1 | sed -r -n '/Verifying signature/,${/Good signature/{p}}'

# Usage
# fn_GPGSignatureVerification 'PACK' 'PACK.sig' ['GPG KEY']
```

### Example

Here choose GnuPG package as testing example

Name|Version|Date|Size|Tarball|Signature
---|---|---|---|---|---
GnuPG|2.4.0|2022-12-16|7487k|https://gnupg.org/ftp/gcrypt/gnupg/gnupg-2.4.0.tar.bz2|https://gnupg.org/ftp/gcrypt/gnupg/gnupg-2.4.0.tar.bz2.sig
GnuPG (LTS)|2.2.41|2022-12-09|7142k|https://gnupg.org/ftp/gcrypt/gnupg/gnupg-2.2.41.tar.bz2|https://gnupg.org/ftp/gcrypt/gnupg/gnupg-2.2.41.tar.bz2.sig

<!-- GnuPG|2.3.4|2021-12-20|7411k|https://gnupg.org/ftp/gcrypt/gnupg/gnupg-2.3.4.tar.bz2|https://gnupg.org/ftp/gcrypt/gnupg/gnupg-2.3.4.tar.bz2.sig -->
<!-- GnuPG|2.2.20|2020-03-20|6627k|https://gnupg.org/ftp/gcrypt/gnupg/gnupg-2.2.20.tar.bz2|https://gnupg.org/ftp/gcrypt/gnupg/gnupg-2.2.20.tar.bz2.sig -->


Executing command

```bash
# fn_GPGSignatureVerification 'ISOPATH' 'SIGPATH' 'PGPPATH'
fn_GPGSignatureVerification /tmp/gnupg-2.4.0.tar.bz2 'https://gnupg.org/ftp/gcrypt/gnupg/gnupg-2.4.0.tar.bz2.sig' 'https://gnupg.org/signature_key.html'
```

Output

```txt
Importing PGP key
gpg: keybox '/tmp/MiuChOWK/gpg_import.gpg' created
gpg: key 0xBCEF7E294B092E28: 1 signature not checked due to a missing key
gpg: key 0xBCEF7E294B092E28: public key "Andre Heinecke (Release Signing Key)" imported
gpg: key 0x528897B826403ADA: 4 signatures not checked due to missing keys
gpg: key 0x528897B826403ADA: public key "Werner Koch (dist signing 2020)" imported
gpg: key 0xE98E9B2D19C6C8BD: 2 signatures not checked due to missing keys
gpg: key 0xE98E9B2D19C6C8BD: public key "Niibe Yutaka (GnuPG Release Key)" imported
gpg: key 0x549E695E905BA208: 1 signature not checked due to a missing key
gpg: key 0x549E695E905BA208: public key "GnuPG.com (Release Signing Key 2021)" imported
gpg: Total number processed: 4
gpg:               imported: 4
gpg: no ultimately trusted keys found

Verifying signature
gpg: Signature made Fri 16 Dec 2022 12:24:40 PM EST
gpg:                using EDDSA key 6DAA6E64A76D2840571B4902528897B826403ADA
gpg: Good signature from "Werner Koch (dist signing 2020)" [unknown]
gpg: WARNING: This key is not certified with a trusted signature!
gpg:          There is no indication that the signature belongs to the owner.
Primary key fingerprint: 6DAA 6E64 A76D 2840 571B  4902 5288 97B8 2640 3ADA

Temporary directory /tmp/MiuChOWK has been removed.
```

<details><summary>Click to expand outdated versions</summary>

For version *2.3.4*

```txt
Importing PGP key
gpg: keybox '/tmp/dDGZ5Dpd/gpg_import.gpg' created
gpg: key 0xBCEF7E294B092E28: 1 signature not checked due to a missing key
gpg: key 0xBCEF7E294B092E28: public key "Andre Heinecke (Release Signing Key)" imported
gpg: key 0x528897B826403ADA: 4 signatures not checked due to missing keys
gpg: key 0x528897B826403ADA: public key "Werner Koch (dist signing 2020)" imported
gpg: key 0xE98E9B2D19C6C8BD: 2 signatures not checked due to missing keys
gpg: key 0xE98E9B2D19C6C8BD: public key "Niibe Yutaka (GnuPG Release Key)" imported
gpg: key 0x549E695E905BA208: 1 signature not checked due to a missing key
gpg: key 0x549E695E905BA208: public key "GnuPG.com (Release Signing Key 2021)" imported
gpg: Total number processed: 4
gpg:               imported: 4
gpg: no ultimately trusted keys found

Verifying signature
gpg: Signature made Mon 20 Dec 2021 04:52:45 PM EST
gpg:                using EDDSA key 6DAA6E64A76D2840571B4902528897B826403ADA
gpg: Good signature from "Werner Koch (dist signing 2020)" [unknown]
gpg: WARNING: This key is not certified with a trusted signature!
gpg:          There is no indication that the signature belongs to the owner.
Primary key fingerprint: 6DAA 6E64 A76D 2840 571B  4902 5288 97B8 2640 3ADA
gpg: Signature made Tue 21 Dec 2021 01:20:39 AM EST
gpg:                using EDDSA key AC8E115BF73E2D8D47FA9908E98E9B2D19C6C8BD
gpg: Good signature from "Niibe Yutaka (GnuPG Release Key)" [unknown]
gpg: WARNING: This key is not certified with a trusted signature!
gpg:          There is no indication that the signature belongs to the owner.
Primary key fingerprint: AC8E 115B F73E 2D8D 47FA  9908 E98E 9B2D 19C6 C8BD

Temporary directory /tmp/dDGZ5Dpd has been removed.
```

</details>

String *Good signature* indicates gpg verification approves.

Or just extracting target phrase statement *Good signature*.

```bash
fn_GPGSignatureVerification /tmp/gnupg-2.4.0.tar.bz2 'https://gnupg.org/ftp/gcrypt/gnupg/gnupg-2.4.0.tar.bz2.sig' 'https://gnupg.org/signature_key.html' 2>&1 | sed -r -n '/Verifying signature/,${/Good signature/{p}}'

# gpg: Good signature from "Werner Koch (dist sig)" [unknown]
```


## Change Log

* Jan 03, 2023 Tue 13:20
  * update release version
* Apr 19, 2020 Sun 10:40 ET
  * first draft

<!-- End -->