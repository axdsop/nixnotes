#!/usr/bin/env bash
# shellcheck disable=SC2230,SC2155,SC2119,SC2120
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails


# Target: GnuPG key generation and operation
# Developer: MaxdSre

# Change Log:
# - Mar 29, 2020 Sun 08:52 ET - change exported keys name format
# - Mar 28, 2020 Sat 19:29 ET - fix Ubuntu 18.04 use GnuPG version 2.2.4 has no -list-option "show-only-fpr-mbox" which is added from version GnuPG 2.2.12 (Dec 14, 2018)
# - Oct 24, 2019 Thu 09:48 ET - add custom function CommandExistIfCheck, VersionNumberComparasion
# - Sep 26, 2019 Thu 20:33 ET - change custom function name format, change hokey check prompt info
# - Jul 28, 2019 Sun 21:58 ET - change key export path, config url fix
# - Jul 18, 2019 Thu 14:35 ET - first draft


# Similar project
# - https://gist.github.com/jirutka/8dc567ed4d7b4585111996242aa573a8

# Note the hash (#) after the sec tag which indicates that the primary key is currently not usable.


#########  0-1. Variables Setting  #########
umask 022
# - Color
readonly c_bold="$(tput bold 2> /dev/null)"
# https://misc.flogisoft.com/bash/tip_colors_and_formatting
# black 0, red 1, green 2, yellow 3, blue 4, magenta 5, cyan 6, gray 7
# \e[31m need to use `echo -e`
readonly c_red="$(tput setaf 1 2> /dev/null)"     # c_red='\e[31;1m'
readonly c_green="$(tput setaf 2 2> /dev/null)"    # c_blue='\e[32m'
readonly c_yellow="$(tput setaf 3 2> /dev/null)"    # c_blue='\e[33m'
readonly c_blue="$(tput setaf 4 2> /dev/null)"    # c_blue='\e[34m'
readonly c_magenta="$(tput setaf 5 2> /dev/null)"    # c_magenta='\e[35m'
readonly c_cyan="$(tput setaf 6 2> /dev/null)"    # c_cyan='\e[36m'
readonly c_gray="$(tput setaf 7 2> /dev/null)"    # c_gray='\e[37m'
readonly c_normal="$(tput sgr0 2> /dev/null)"     # c_normal='\e[0m'

version_check=${version_check:-0}
action_type=${action_type:-0}
# gnupg homedir default is ~/.gnupg/ if GNUPGHOME not set
gnupg_home=${gnupg_home:-"$HOME/.gnupg"}
key_export_dir=${key_export_dir:-''}
user_name=${user_name:-'GnuPG'}
user_email=${user_email:-''}
user_comment=${user_comment:-''}
expire_date=${expire_date:-0}
key_type=${key_type:-'rsa'}
key_length=${key_length:-'2048'}
key_usage=${key_usage:-'sign,auth,encrypt'}
key_passphrase=${key_passphrase:-''} # GPG_PASSPHRASE
key_export_path_suffix='KeyManagement/GnuPG'
readonly custom_shellscript_url='https://gitlab.com/axdsop/nixnotes/raw/master'
readonly gnupg_config_url="${custom_shellscript_url}/CyberSecurity/GnuPG/GnuPG_Configs.sh"

# https://sks-keyservers.net/overview-of-pools.php
sks_keyserver_ca_link='https://sks-keyservers.net/sks-keyservers.netCA.pem'
sks_keyserver_ca_path="/etc/ssl/certs/${sks_keyserver_ca_link##*/}"


#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | bash -s -- [options] ...

GnuPG Configuration & Key Operation On GNU/Linux!

Attention: This script may not work properly if your GnuPG is not the latest version. This script approves testing on Ubuntu 18.04 (GnuGP version 2.2.4 <Dec 14, 2018>).

[available option]
    -h    --help, show help info
    -c    --check, check current online and local GnuPG version
    -d gnupg_home    --specify GnuPG home dir, default is '~/.gnupg'
    -s key_export_dir    --specify export dir, default is '~/Documents/${key_export_path_suffix}' or '~/${key_export_path_suffix}', overwite if existed same name file
    -a    --specify re-import action (import generated subkeys into gnupg_home), default is just export generated key into key_export_dir
    -n name    --specify user id's real name, default is 'GnuPG'
    -m email   --specify user id's email, default is empty
    -C comment    --specify user id's comment, default is empty
    -e expire_date    --specify key expiration date, default is 0 (no expire). Available format: seconds=N, Nd, Nw, Nm, Ny, YYYY-MM-DD, YYYYMMDDThhmmss
    -t key_type    --specify primary key and subkey type, default is 'rsa'. Available type: rsa, dsa, ecc
    -b key_length    --specify key length, default and min value is 2048 bits, max value is 4096 bits, just for rsa, dsa
    -u key_usage    --specify subkey usage, default is 'sign,auth,encrypt', dsa just support 'sign,auth'. Space or comma delimited list of key usages. Available types: sign ,auth, encrypt. Primary key is 'cert' by default.
    -p passphrase    --specify key passphrase, default is empty
\e[0m"
}

while getopts "hcad:s:n:m:C:e:t:b:u:p:" option "$@"; do
    case "$option" in
        c ) version_check=1 ;;
        a ) action_type=1 ;;
        d ) gnupg_home="$OPTARG" ;;
        s ) key_export_dir="$OPTARG" ;;
        n ) user_name="$OPTARG" ;;
        m ) user_email="$OPTARG" ;;
        C ) user_comment="$OPTARG" ;;
        e ) expire_date="$OPTARG" ;;
        t ) key_type="$OPTARG" ;;
        b ) key_length="$OPTARG" ;;
        u ) key_usage="$OPTARG" ;;
        p ) key_passphrase="$OPTARG" ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done


#########  1-0. Initial Functions  #########
fn_CommandExistIfCheck(){
    # $? -- 0 is find, 1 is not find
    local l_name="${1:-}"
    local l_output=${l_output:-1}
    [[ -n "${l_name}" && -n $(which "${l_name}" 2> /dev/null || command -v "${l_name}" 2> /dev/null) ]] && l_output=0
    return "${l_output}"
}

fn_VersionNumberComparasion(){
    local l_item1="${1:-0}"    # old val
    local l_item2="${2:-0}"    # new val
    local l_operator=${3:-'<'}
    local l_output=${l_output:-0}

    if [[ -n "${l_item1}" && -n "${l_item2}" ]]; then
        local l_arr1=( ${l_item1//./ } )
        local l_arr2=( ${l_item2//./ } )
        local l_arr1_len=${l_arr1_len:-"${#l_arr1[@]}"}
        local l_arr2_len=${l_arr2_len:-"${#l_arr2[@]}"}
        local l_max_len=${l_max_len:-"${l_arr1_len}"}
        [[ "${l_arr1_len}" -lt "${l_arr2_len}" ]] && l_max_len="${l_arr2_len}"

        for (( i = 0; i < "${l_max_len}"; i++ )); do
            [[ "${l_arr1_len}" -lt $(( i+1 )) ]] && l_arr1[$i]=0
            [[ "${l_arr2_len}" -lt $(( i+1 )) ]] && l_arr2[$i]=0

            if [[ "${l_arr1[i]}" -lt "${l_arr2[i]}" ]]; then
                case "${l_operator}" in
                    '>' ) l_output=0 ;;
                    '<'|* ) l_output=1 ;;
                esac
                break
            elif [[ "${l_arr1[i]}" -gt "${l_arr2[i]}" ]]; then
                case "${l_operator}" in
                    '>' ) l_output=1 ;;
                    '<'|* ) l_output=0 ;;
                esac
                break
            else
                continue
            fi
        done
    fi

    echo "${l_output}"
}


#########  1-1. Configuration files  #########
fn_InitializationCheck(){
    gpg_bin=${gpg_bin:-}
    if fn_CommandExistIfCheck 'gpg2'; then
        gpg_bin='gpg2'
    elif fn_CommandExistIfCheck 'gpg'; then
        gpg_bin='gpg'
    else
        echo "Sorry, this script needs package ${c_yellow}gnupg/gpg2/gnupg2${c_normal}."
        exit
    fi

    download_method='wget -qO-' # curl -fsL
    fn_CommandExistIfCheck 'curl' && download_method='curl -fsL'
}

fn_VariablesVerification(){
    mktemp_format='gnupg_XXXXXXXXX'
    rm -rf /tmp/${mktemp_format%%_*}*
    gnupg_temp_home=$(mktemp -d -t "${mktemp_format}")
    # gnupg_home_bak="${gnupg_home}_bak"

    export GNUPGHOME="${gnupg_temp_home}"

    if [[ -z "${key_export_dir}" ]]; then
        key_export_dir="$HOME/${key_export_path_suffix}"
        [[ -d "$HOME/Documents" ]] && key_export_dir="$HOME/Documents/${key_export_path_suffix}"
    fi

    [[ -d "${gnupg_home}" ]] || (mkdir -p "${gnupg_home}" && chmod 700 "${gnupg_home}")

    [[ -d "${key_export_dir}" ]] || (mkdir -p "${key_export_dir}" && chmod 700 "${key_export_dir}")

    # key_type=$(tr '[:upper:]' '[:lower:]' <<< "${key_type}")

    case "${key_type,,}" in
        rsa|dsa|ecc ) key_type="${key_type,,}" ;;
        * ) key_type='rsa' ;;
    esac

    if [[ "${key_length}" -lt 2048 ]]; then
        key_length=2048
    elif [[ "${key_length}" -gt 4096 ]]; then
        key_length=4096
    fi

    # remove white space
    key_passphrase="${key_passphrase// /}"
}

fn_VersionComparasion(){
    # - Local version
    current_local_version=${current_local_version:-}
    current_local_version=$($gpg_bin --version 2> /dev/null | sed -r -n '/^gpg/{s@.*[[:space:]]+([^[:space:]]+)$@\1@g;p}')
    [[ -n ${current_local_version} ]] && echo -e "Local version is ${c_yellow}${current_local_version}${c_normal}.\n"

    if [[ "${version_check}" -eq 1 ]]; then
        # - Latest online version info
        online_release_info=${online_release_info:-}
        online_release_date=${online_release_date:-}
        online_release_version=${online_release_version:-}
        online_release_info=$($download_method https://gnupg.org/download/index.html | sed -r -n '/<tbody>/,/<\/tbody>/{/href=.*?GnuPG</,/<\/tr>/{/<td/{s@[[:space:]]*<[^>]+>[[:space:]]*@@g;p}}}' | sed ':a;N;$!ba;s@\n@|@g' | cut -d\| --output-delimiter=' ' -f 2,3)
        # 2.2.17 2019-07-09

        if [[ -n "${online_release_info}" ]]; then
            online_release_version=$(echo "${online_release_info}" | cut -d' ' -f1)
            online_release_date=$(echo "${online_release_info}" | cut -d' ' -f2 | date +'%b %d, %Y' -f - 2> /dev/null)
            echo "Online release version is ${c_yellow}${online_release_version}${c_normal} (${c_yellow}${online_release_date}${c_normal})."
        else
            echo "Fail to extract online release info."
        fi

        exit
    fi
}


#########  1-2. Configuration files  #########
fn_ConfigurationFiles(){
    [[ -f "${sks_keyserver_ca_path}" && -s "${sks_keyserver_ca_path}" ]] || $download_method "${sks_keyserver_ca_link}" | sudo tee "${sks_keyserver_ca_path}" 1> /dev/null

    $download_method "${gnupg_config_url}" | bash -s -- "${gnupg_temp_home}"

    if [[ $(find "${gnupg_temp_home}" -type f -name '*.conf' -print | wc -l) -eq 0 ]]; then
        echo "Sorry, fail to create configuration file from url ${gnupg_config_url}"
        exit
    fi

    local l_gpg_conf_path="${gnupg_temp_home}/gpg.conf"
    
    if [[ -f "${l_gpg_conf_path}" ]]; then
        [[ $(fn_VersionNumberComparasion "${current_local_version}" '2.2.7' '<') -eq 1 ]] || sed -r -i '/no-symkey-cache/{s@^#*[[:space:]]*@@g;}' "${l_gpg_conf_path}"
    fi    
}

fn_GPGConnectAgentOperation(){
    local l_action=${1:-'k'}

    case "${l_action,,}" in
        r|reloadagent ) l_action='reloadagent' ;;
        k|killagent ) l_action='killagent' ;;
    esac

    [[ -n "${l_action}" ]] && gpg-connect-agent "${l_action}" /bye &> /dev/null

    # gpg-connect-agent reloadagent /bye 1> /dev/null
    # gpg-connect-agent killagent /bye 1> /dev/null
    # OK closing connection
}

#########  2-1. GPG Key Generation  #########
fn_GPGKeyGeneration(){
    fn_GPGConnectAgentOperation # killagent

    fpr_str=${fpr_str:-}
    user_id_info=${user_id_info:-}
    [[ -n "${user_name}" ]] && user_id_info="${user_name}"
    [[ -n "${user_comment}" ]] && user_id_info="${user_id_info} (${user_comment})"
    [[ -n "${user_email}" ]] && user_id_info="${user_id_info} <${user_email}>"
    [[ -n "${user_id_info}" ]] && echo -e "User id info is ${c_yellow}${user_id_info}${c_normal}.\n"

    user_id_list=$($gpg_bin --list-secret-keys | sed -r -n '/^uid/{s@.*?\][[:space:]]*(.*)$@\1@g;p}')
    if [[ -n $(echo "${user_id_list}" | sed -n '/^'"${user_id_info}"'$/{p}') ]]; then
        echo "Attention: existed keys has user id info ${c_yellow}${user_id_info}${c_normal}."
        exit
    fi

    # method 1 - interactive mode
    # $gpg_bin --expert --full-gen-key

    # method 2 - quick mode
    echo -e "\nGenerating ${c_yellow}${key_type^^}${c_normal} key"

    # --list-option "show-only-fpr-mbox" added from version 2.2.12 (Dec 14, 2018), Ubuntu 18.04 use version 2.2.4
    # https://github.com/gpg/gnupg/blob/master/NEWS#L270
    # https://wiki.gnupg.org/WKDHosting
    local l_show_only_fpr_mbox=1
    [[ $(fn_VersionNumberComparasion "${current_local_version}" '2.2.12' '<') -eq 1 ]] && l_show_only_fpr_mbox=0

    case "${key_type}" in
        rsa|dsa )
            algo_str="${key_type}${key_length}"

            $gpg_bin --batch --passphrase "${key_passphrase}" --quick-gen-key "${user_id_info}" "${algo_str}" cert "${expire_date}"
          
            if [[ "${l_show_only_fpr_mbox}" -eq 1 ]]; then
                fpr_str=$($gpg_bin --list-options show-only-fpr-mbox --list-secret-keys | sed -r -n '$!d;s@^([^[:space:]]+).*@\1@g;p')
            else
                fpr_str=$($gpg_bin --list-secret-keys | sed -r -n '/fingerprint.*=/{s@^[^=]*=[[:space:]]*@@g;p}')
            fi
            
            [[ "${key_usage}" =~ sign ]] && $gpg_bin --batch --pinentry-mode=loopback --passphrase "${key_passphrase}" --quick-add-key "${fpr_str}" "${algo_str}" sign "${expire_date}"
            [[ "${key_usage}" =~ auth ]] && $gpg_bin --batch --pinentry-mode=loopback --passphrase "${key_passphrase}" --quick-add-key "${fpr_str}" "${algo_str}" auth "${expire_date}"

            # DSA doesn't support encrypt
            # gpg: WARNING: some OpenPGP programs can't handle a DSA key with this digest size
            # gpg: Key generation failed: Wrong key usage
            [[ "${key_type}" != 'dsa' && "${key_usage}" =~ encrypt ]] && $gpg_bin --batch --pinentry-mode=loopback --passphrase "${key_passphrase}" --quick-add-key "${fpr_str}" "${algo_str}" encrypt "${expire_date}"
            ;;
        ecc )
            # nistp256, nistp384, nistp512
            $gpg_bin --batch --passphrase "${key_passphrase}" --quick-gen-key "${user_id_info}" ed25519 cert "${expire_date}"

            if [[ "${l_show_only_fpr_mbox}" -eq 1 ]]; then
                fpr_str=$($gpg_bin --list-options show-only-fpr-mbox --list-secret-keys | sed -r -n '$!d;s@^([^[:space:]]+).*@\1@g;p')
            else
                fpr_str=$($gpg_bin --list-secret-keys | sed -r -n '/fingerprint.*=/{s@^[^=]*=[[:space:]]*@@g;p}')
            fi


            [[ "${key_usage}" =~ sign ]] && $gpg_bin --batch --pinentry-mode=loopback --passphrase "${key_passphrase}" --quick-add-key "${fpr_str}" ed25519 sign "${expire_date}"
            [[ "${key_usage}" =~ auth ]] && $gpg_bin --batch --pinentry-mode=loopback --passphrase "${key_passphrase}" --quick-add-key "${fpr_str}" ed25519 auth "${expire_date}"
            [[ "${key_usage}" =~ encrypt ]] && $gpg_bin --batch --pinentry-mode=loopback --passphrase "${key_passphrase}" --quick-add-key "${fpr_str}" cv25519 encrypt "${expire_date}"
            ;;
    esac

    echo -e "\n${c_green}List secret keys${c_normal}"
    $gpg_bin --list-secret-keys

    if [[ -n $(which hokey 2> /dev/null || command -v hokey 2> /dev/null) ]]; then
        echo -e "\n${c_green}hopenpgp-tools${c_normal} checking"
        $gpg_bin --export --no-armor "${fpr_str}" | hokey lint 2> /dev/null
    fi
    # [[ -n $(hokey --version 2> /dev/null) ]] && $gpg_bin --export --no-armor "${fpr_str}" | hokey lint 2> /dev/null
}

#########  2-2. GPG Key Export  #########
fn_KeyExport(){
    echo -e "\nExported keys saved dir ${c_yellow}${key_export_dir}${c_normal}.\n"

    local l_keyname_format=${l_keyname_format:-"${user_name}"}
    [[ -n "${user_email}" ]] && l_keyname_format="${user_email}"
    l_keyname_format="${l_keyname_format}"_$(date +'%F')

    key_export_prefix="${key_export_dir}/${l_keyname_format}_${key_type^^}"

    # - generate revocation key
    # https://debian-administration.org/article/450/Generating_a_revocation_certificate_with_gpg
    # generating revocation certs non-interactively
    # https://lists.gnupg.org/pipermail/gnupg-users/2015-May/053559.html
    # https://lists.gnupg.org/pipermail/gnupg-users/2015-May/053562.html
    revocation_cert_save_path="${key_export_prefix}_revocation_cert.asc"
    [[ -f "${revocation_cert_save_path}" ]] && rm -f "${revocation_cert_save_path}"
    echo -e "Y\n0\n\nY\n" | $gpg_bin --gen-revoke --armor --no-tty --pinentry-mode loopback --passphrase "${key_passphrase}" --command-fd 0 -o "${revocation_cert_save_path}" "${fpr_str}"
     # 2> /dev/null

    # - public key
    # export the public key  --export-options export-clean,export-minimal
    public_cert_save_path="${key_export_prefix}_pub.asc"
    [[ -f "${public_cert_save_path}" ]] && rm -f "${public_cert_save_path}"
    $gpg_bin --export --armor --passphrase "${key_passphrase}" -o "${public_cert_save_path}" "${fpr_str}"

    # export the secret key, secret subkeys
    # GnuPG may ask you to enter the passphrase for the key. This is required,  because the internal protection method of the secret key is different from the one specified by the OpenPGP protocol.
    # without '--pinentry-mode loopback' it will prompt passphrase message
    secret_key_save_path="${key_export_prefix}_sec.asc"
    [[ -f "${secret_key_save_path}" ]] && rm -f "${secret_key_save_path}"
    $gpg_bin --export-secret-keys --armor --pinentry-mode loopback --passphrase "${key_passphrase}" -o "${secret_key_save_path}" "${fpr_str}"

    secret_subkeys_save_path="${key_export_prefix}_sec_sub.asc"
    [[ -f "${secret_subkeys_save_path}" ]] && rm -f "${secret_subkeys_save_path}"
    $gpg_bin --export-secret-subkeys --armor --pinentry-mode loopback --passphrase "${key_passphrase}" -o "${secret_subkeys_save_path}" "${fpr_str}"

    echo -e "To operate newly generated key, execute\n\n${c_bold}${c_green}export GNUPGHOME=${gnupg_temp_home}${c_normal}\n\n"
}

#########  2-3. GPG Key Re-import  #########
fn_KeyReImport(){
    # copy configuration file to $gnupg_home
    find "${gnupg_temp_home}" -type f -name '*.conf' -exec cp -f {} "${gnupg_home}" \;

    if [[ "${action_type}" -eq 1 ]]; then
        echo -e "Importing generated key into ${c_yellow}${gnupg_home}${c_normal} (sec#)\n"

        # restart the agent, or it will prompt error 'error sending to agent: No such file or directory', as it need directory '~/.gnupg/private-keys-v1.d'
        # https://stackoverflow.com/questions/44837437/gpg2-import-of-gpg1-secret-key-fails-gpg-2-1-15-ubuntu-17-10
        # https://wiki.archlinux.org/index.php/GnuPG#Reload_the_agent
        # gpg-connect-agent reloadagent /bye 1> /dev/null
        fn_GPGConnectAgentOperation # killagent
        export GNUPGHOME=${gnupg_home}

        # import (secret sub keys)
        export GPG_TTY=$(tty)
        echo "${c_green}Import subkeys${c_normal}"
        $gpg_bin --import --pinentry-mode=loopback --passphrase "${key_passphrase}" "${secret_subkeys_save_path}"

        # Starting with GnuPG 2.1.0 the use of gpg-agent and pinentry is required, which may break backwards compatibility for passphrases piped in from STDIN using the --passphrase-fd 0 commandline option.
        # https://wiki.archlinux.org/index.php/GnuPG#Unattended_passphrase
        # echo '1Qaz@Wsx' | gpg2 --import --batch --yes --passphrase-fd 0 ecc_sec_sub.asc

        # fn_GPGConnectAgentOperation
        # fn_GPGConnectAgentOperation 'r' # reloadagent

        echo -e "\n${c_green}Import public keyring${c_normal}"
        # reimport public keyring
        $gpg_bin --homedir "${gnupg_temp_home}" --export | $gpg_bin --import

        echo -e "\n${c_green}Import trust db${c_normal}"
        # reimport trust db
        $gpg_bin --homedir "${gnupg_temp_home}" --export-ownertrust | $gpg_bin --import-ownertrust

        # list key
        echo -e "\n${c_green}List imported key${c_normal}"
        $gpg_bin --list-secret-keys "${fpr_str}"
        # Note the hash (#) after the sec tag which indicates that the primary key is currently not usable.
    fi
}


# upload
# $ gpg --send-key $KEYID
# $ gpg --keyserver pgp.mit.edu --send-key $KEYID
# $ gpg --keyserver keys.gnupg.net --send-key $KEYID
# $ gpg --keyserver hkps://keyserver.ubuntu.com:443 --send-key $KEYID


#########  3. Executing Process  #########
fn_InitializationCheck
fn_VersionComparasion
fn_VariablesVerification
fn_ConfigurationFiles
fn_GPGKeyGeneration
fn_KeyExport
fn_KeyReImport


# Script End
