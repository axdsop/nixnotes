# Secure Nginx With Let's Encrypt Free SSL Certificate

Issuing Let's Encrypt free SSL certificate for Nginx via [acme.sh][acmesh].

## TOC

1. [Preparation](#preparation)  
1.1 [VPS](#vps)  
1.2 [Nginx](#nginx)  
1.2.1 [Install](#install)  
1.2.2 [Operating Commands](#operating-commands)  
1.3 [Domain Name Resolution](#domain-name-resolution)  
1.3.1 [Namechecp API KEY](#namechecp-api-key)  
2. [Issuing SSL Certificate](#issuing-ssl-certificate)  
2.1 [Via acme.sh](#via-acmesh)  
3. [Security Optimization](#security-optimization)  
3.1 [OCSP Stapling](#ocsp-stapling)  
3.2 [HTTP Public Key Pinning](#http-public-key-pinning)  
3.2.1 [For Existing Certificate](#for-existing-certificate)  
3.2.2 [Creating A Backup CSR](#creating-a-backup-csr)  
3.3 [ssl_dhparam](#ssldhparam)  
3.4 [ssl_session_ticket_key](#sslsessionticketkey)  
4. [Nginx Configs](#nginx-configs)  
4.1 [nginx conf](#nginx-conf)  
4.2 [ssl conf](#ssl-conf)  
5. [Firewall Setting](#firewall-setting)  
5.1 [Network Based](#network-based)  
5.1.1 [DigitalOcean Cloud Firewalls](#digitalocean-cloud-firewalls)  
5.2 [Host Based](#host-based)  
5.2.1 [ufw](#ufw)  
5.2.2 [iptables](#iptables)  
6. [Nginx Security Check](#nginx-security-check)  
6.1 [SSL](#ssl)  
6.2 [HTTP Headers](#http-headers)  
7. [Error Occruring](#error-occruring)  
8. [Tutorials](#tutorials)  
9. [Change Logs](#change-logs)  


## Preparation

### VPS

I deploy a VPS on [Digital Ocean][digitalocean], droplet info

item|detail
---|---
OS|Debian 10.8 (buster)
Kernel|4.19.0-14-cloud-amd64
Private IP|192.168.8.3
Public IP|67.205.141.249
Floating IP|138.197.227.211

System update via custom [init script](/CloudComputing/Providers/DigitalOcean/Scripts/doctl_sys_init.sh).

Firewall rules defined in DigitalOcean Cloud Firewalls.

The following operations are executed as user *root* via `sudo -i`.

```sh
# run login shell as user root
sudo -i
```

### Nginx

#### Install

Nginx official doc [Install](https://www.nginx.com/resources/wiki/start/topics/tutorials/install/)

For Debin/Ubuntu

```sh
# apt source save path /etc/apt/sources.list.d/nginx.list
distro_name=$(sed -r -n '/^ID=/{s@^[^=]*=@@g;p}' /etc/os-release)
code_name=$(sed -r -n '/^VERSION_CODENAME=/{s@^[^=]*=@@g;p}' /etc/os-release)

echo -e "deb https://nginx.org/packages/${distro_name}/ ${code_name} nginx\ndeb-src https://nginx.org/packages/${distro_name}/ ${code_name} nginx" > /etc/apt/sources.list.d/nginx.list

apt-key adv --keyserver keyserver.ubuntu.com --recv-keys ABF5BD827BD9BF62
apt-get -yq update
apt-get install -y nginx

systemctl enable --now nginx

# Version 1.18.0
```

If fail to start Nginx service properly, you may need to reboot to make Nginx work.

#### Operating Commands

Checking Nginx version info

<details><summary>Click to expand command</summary>

```sh
# Via nginx -v
sudo nginx -v 2>&1 | awk -v FS='/' '{print $NF}'
sudo nginx -v 2>&1 | sed -r -n 's@.*/(.*)@\1@p'

# Via nginx -V
sudo nginx -V 2>&1 | awk -v FS='/' '{print $NF;exit}'
sudo nginx -V 2>&1 | sed -r -n '1 s@.*/(.*)@\1@p'

# - Bash 4+
sudo nginx -v |& awk -v FS='/' '{print $NF}'
sudo nginx -v |& sed -r -n 's@.*/(.*)@\1@p'

sudo nginx -V |& awk -v FS='/' '{print $NF;exit}'
sudo nginx -V |& sed -r -n '1 s@.*/(.*)@\1@p'
```

</details>

Checking Nginx config path

```sh
sudo nginx -V 2>&1 | sed -r -n 's@.*conf-path=(.*) --error.*@\1@p'
```

Controlling Nginx service via command `nginx -s signal`, details see official doc [Starting, Stopping, and Reloading Configuration](https://nginx.org/en/docs/beginners_guide.html#control).

```sh
# fast shutdown
sudo nginx -s stop

# graceful shutdown (recommended)
sudo nginx -s quit

# reloading the configuration file
sudo nginx -s reload

# reopening the log files
sudo nginx -s reopen
```

### Domain Name Resolution

You need to add *A* record in your domain dns (here is NameCheap). If you don't know how to operate, plear read the following tutorials:

* [NameCheap - How can I set up an A (address) record for my domain?](https://www.namecheap.com/support/knowledgebase/article.aspx/319/2237/how-can-i-set-up-an-a-address-record-for-my-domain/)
* [NameCheap - Which record type option should I choose for the information I’m about to enter?](https://www.namecheap.com/support/knowledgebase/article.aspx/579/2237/which-record-type-option-should-i-choose-for-the-information-im-about-to-enter/)
* [Digital Ocean - How to Create, Edit, and Delete DNS Records](https://www.digitalocean.com/docs/networking/dns/how-to/manage-records/)

Domain name is `maxdsre.com`.

Type|Host|Value|TTL
---|---|---|---
A Record|@|138.197.227.211|automatic
A Record|www|138.197.227.211|automatic
A Record |*|138.197.227.211|automatic

`*` means a wildcard record that matches requests for non-existent subdomains.


You can also ass a *CNAME* record (*www* -> *maxdsre.com*), this is same to *A* record (*www* -> *138.197.227.211*).

#### Namechecp API KEY

To operate domain via api, you need to generate [API key](https://www.namecheap.com/support/api/intro.aspx) from NameCheap. Details see [acme.sh][acmesh]'s wiki [dnsapi#53-use-namecheap](https://github.com/acmesh-official/acme.sh/wiki/dnsapi#53-use-namecheap)

Then export these envrionment variables, they will be used by [acme.sh][acmesh].

```sh
export NAMECHEAP_USERNAME="**************"
export NAMECHEAP_API_KEY="***************"
export NAMECHEAP_SOURCEIP="67.205.141.249"
```

## Issuing SSL Certificate

Variables definition

```sh
domain_name='maxdsre.com'  # Change to your domain
email_specify='XXXX@XXXX.XXX'  # Change to you E-mail
```

SSL certs save path `/etc/nginx/ssl.d/`

```sh
ssl_cert_dir="/etc/nginx/ssl.d/${domain_name%%.*}"
[[ -d "${ssl_cert_dir}" ]] || mkdir -pv "${ssl_cert_dir}"
```

### Via acme.sh

[acme.sh][acmesh] is a pure Unix shell script implementing ACME client protocol.

If you use *sudo*, it will prompts error info

>It seems that you are using sudo, please read this link first: https://github.com/acmesh-official/acme.sh/wiki/sudo

```sh
# Nginx Web Root /usr/share/nginx/html
# The cert will be renewed every 60 days by default.

# - install
# https://letsencrypt.org/docs/client-options/
curl -fsSL https://get.acme.sh | sh -s email="${email_specify}"
# All certs will be placed in folder ~/.acme.sh/.

# Script path ~/.acme.sh/acme.sh, create soft link to /usr/local/bin/
ln -fs ~/.acme.sh/acme.sh /usr/local/bin/

# Upgrade script
acme.sh --upgrade

# - issue cert (Wildcard ECC)
# DNS API mode
# https://github.com/acmesh-official/acme.sh/wiki/dnsapi#53-use-namecheap
# The username and password will be saved in ~/.acme.sh/account.conf and will be reused when needed.

# https://github.com/acmesh-official/acme.sh#10-issue-ecc-certificates
# ec-256 (prime256v1, "ECDSA P-256")
# ec-384 (secp384r1, "ECDSA P-384")
# ec-521 (secp521r1, "ECDSA P-521", which is not supported by Let's Encrypt yet.)

acme.sh --issue --dns dns_namecheap -d "${domain_name}" -d "*.${domain_name}" --keylength ec-384

# install cert (For Nginx)
# https://github.com/acmesh-official/acme.sh/#3-install-the-cert-to-apachenginx-etc
acme.sh --install-cert --ecc -d "${domain_name}" \
--cert-file ${ssl_cert_dir}/cert.pem  \
--key-file ${ssl_cert_dir}/key.pem  \
--fullchain-file ${ssl_cert_dir}/fullchain.pem \
--reloadcmd "service nginx force-reload"

# Your cert is in: /root/.acme.sh/maxdsre.com_ecc/maxdsre.com.cer
# Your cert key is in: /root/.acme.sh/maxdsre.com_ecc/maxdsre.com.key
# The intermediate CA cert is in: /root/.acme.sh/maxdsre.com_ecc/ca.cer
# And the full chain certs is there: /root/.acme.sh/maxdsre.com_ecc/fullchain.cer
# r
```

[acme.sh][acmesh] auto generates a cron job (`crontab -l`), you can also force to renew via parameter *--force*.


```crontab
# origin
33 0 * * * "/root/.acme.sh"/acme.sh --cron --home "/root/.acme.sh" > /dev/null

# force renew every 2 months
# Add '--force' to force to renew.
33 0 1 */2 * "/root/.acme.sh"/acme.sh --cron --home "/root/.acme.sh" --force > /dev/null

# Add '--force' to force to renew, add '--ecc' for ECC cert.
#33 0 1 */2 * "/root/.acme.sh"/acme.sh --cron --home "/root/.acme.sh" --force --ecc > /dev/null
```

<details><summary>Click to expand renew output</summary>

```txt
[Sun 05 Sep 2021 09:46:46 AM EDT] ===Starting cron===
[Sun 05 Sep 2021 09:46:46 AM EDT] Renew: 'maxdsre.com'
...
...
[Sun 05 Sep 2021 09:46:47 AM EDT] Your cert is in  /root/.acme.sh/maxdsre.com_ecc/maxdsre.com.cer
[Sun 05 Sep 2021 09:46:47 AM EDT] Your cert key is in  /root/.acme.sh/maxdsre.com_ecc/maxdsre.com.key
[Sun 05 Sep 2021 09:46:47 AM EDT] The intermediate CA cert is in  /root/.acme.sh/maxdsre.com_ecc/ca.cer
[Sun 05 Sep 2021 09:46:47 AM EDT] And the full chain certs is there:  /root/.acme.sh/maxdsre.com_ecc/fullchain.cer
[Sun 05 Sep 2021 09:46:47 AM EDT] Installing cert to:/etc/nginx/ssl.d/maxdsre/cert.pem
[Sun 05 Sep 2021 09:46:47 AM EDT] Installing key to:/etc/nginx/ssl.d/maxdsre/key.pem
[Sun 05 Sep 2021 09:46:47 AM EDT] Installing full chain to:/etc/nginx/ssl.d/maxdsre/fullchain.pem
[Sun 05 Sep 2021 09:46:47 AM EDT] Run reload cmd: service nginx force-reload
[Sun 05 Sep 2021 09:46:48 AM EDT] Reload success
[Sun 05 Sep 2021 09:46:48 AM EDT] ===End cron===
```

</details>

The following commands haven't tested

```sh
# - Renew cert
acme.sh --renew -d "${domain_name}" --force --ecc

# stop renewal
acme.sh --remove -d "${domain_name}" --ecc
```

Generated keys save in path */root/.acme.sh/XXXXXX.XXX_ecc/*

Type|Name
---|---
Cert|XXXXXX.XXX.cer
Cert Key|XXXXXX.XXX.key
Intermediate CA cert|ca.cer
Full chain certs|fullchain.cer


## Security Optimization

### OCSP Stapling

[OCSP stapling](https://en.wikipedia.org/wiki/OCSP_stapling 'Online Certificate Status Protocol') is a TLS/SSL extension which aims to improve the performance of SSL negotiation while maintaining visitor privacy.

Digital Ocean community has a tutorial [How To Configure OCSP Stapling on Apache and Nginx](https://www.digitalocean.com/community/tutorials/how-to-configure-ocsp-stapling-on-apache-and-nginx).

It needs *root CA* and *intermediate CA* provided by [Let's Encrypt](https://letsencrypt.org/).

* [Let's Encrypt Root and Intermediate Certificates](https://letsencrypt.org/2015/06/04/isrg-ca-certs.html)
* [Chain of Trust](https://letsencrypt.org/certificates/)


```sh
# Let's Encrypt Root and Intermediate Certificates

# Active Root Certificates
# ISRG Root X1 (DST Root CA X3 cross-signed)
https://letsencrypt.org/certs/isrg-root-x1-cross-signed.pem

# Active Intermediate Certificates
# Let’s Encrypt R3 (IdenTrust cross-signed)
https://letsencrypt.org/certs/lets-encrypt-r3-cross-signed.pem
```

Merging them into single file *letsencrypt-ca-cert.pem*.

```sh
root_cert_url='https://letsencrypt.org/certs/isrg-root-x1-cross-signed.pem'
intermediate_cert_url='https://letsencrypt.org/certs/lets-encrypt-r3-cross-signed.pem'

wget -qO- "${root_cert_url}" "${intermediate_cert_url}" | tee ${ssl_cert_dir}/letsencrypt-ca-cert.pem > /dev/null
```

Nginx config parameters are *ssl_stapling*, *ssl_stapling_verify*, *ssl_trusted_certificate*.

Testing if `CSP Stapling` works

```sh
# reload Nginx conf
nginx -t && nginx -s reload

echo QUIT | openssl s_client -connect maxdsre.com:443 -status 2> /dev/null | sed -r -n '/^OCSP response/,/Next Update/p'
```

Output

<details><summary>Click to expand output</summary>

```txt
OCSP response:
======================================
OCSP Response Data:
    OCSP Response Status: successful (0x0)
    Response Type: Basic OCSP Response
    Version: 1 (0x0)
    Responder Id: C = US, O = Let's Encrypt, CN = R3
    Produced At: Mar  2 17:19:00 2021 GMT
    Responses:
    Certificate ID:
      Hash Algorithm: sha1
      Issuer Name Hash: 48DAC9A0FB2BD32D4FF0DE68D2F567B735F9B3C4
      Issuer Key Hash: 142EB317B75856CBAE500940E61FAF9D8B14C2C6
      Serial Number: 04D5A05E87ACB90B712C08362DFD199558E2
    Cert Status: good
    This Update: Mar  2 17:00:00 2021 GMT
    Next Update: Mar  9 17:00:00 2021 GMT
```

</details>

### HTTP Public Key Pinning

This is a optional option, because Mozilla doc [HTTP Public Key Pinning (HPKP)](https://developer.mozilla.org/en-US/docs/Web/HTTP/Public_Key_Pinning) says:

>**This feature is no longer recommended**. Though some browsers might still support it, it may have already been removed from the relevant web standards, may be in the process of being dropped, or may only be kept for compatibility purposes. Avoid using it, and update existing code if possible; see the compatibility table at the bottom of this page to guide your decision. Be aware that this feature may cease to work at any time.

[Raymii](https://raymii.org/s/)'s blog [HTTP Public Key Pinning Extension HPKP for Apache, NGINX and Lighttpd](https://raymii.org/s/articles/HTTP_Public_Key_Pinning_Extension_HPKP.html) introduces *HTTP Public Key Pinning* (HPKP).

If you wanna use this feture, it needs 2 steps and needs cert file *${ssl_cert_dir}/cert.pem* generated by Let's Encrypt.

#### For Existing Certificate

Existing cert is *${ssl_cert_dir}/cert.pem*

There are two methods to generate base64 format string, method 2 is preferred.

```sh
# Method 1 - Via intermediate key
openssl x509 -noout -in ${ssl_cert_dir}/cert.pem -pubkey | openssl asn1parse -noout -inform pem -out /tmp/public.key
openssl dgst -sha256 -binary /tmp/public.key | openssl enc -base64
rm -f /tmp/public.key

# Method 2 - Directly
openssl x509 -pubkey < ${ssl_cert_dir}/cert.pem | openssl pkey -pubin -outform der | openssl dgst -sha256 -binary | base64
```

Generated string is

```txt
tcX5hYyanlC2m/NR+6q2sF58TKQqx4QnCGIi8z/25vc=
```

#### Creating A Backup CSR

Generating a custom CSR

```sh
openssl genrsa -out ${ssl_cert_dir}/hpkp_backup.key 4096

openssl req -new -key ${ssl_cert_dir}/hpkp_backup.key -sha256 -out ${ssl_cert_dir}/hpkp_backup.csr

openssl req -pubkey < ${ssl_cert_dir}/hpkp_backup.csr | openssl pkey -pubin -outform der | openssl dgst -sha256 -binary | base64
```

It wil prompts to enter information while generating file *.csr*.

<details><summary>Click to expand prompt information</summary>

```txt
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [AU]:US
State or Province Name (full name) [Some-State]:NY
Locality Name (eg, city) []:NewYork
Organization Name (eg, company) [Internet Widgits Pty Ltd]:MaxdSre
Organizational Unit Name (eg, section) []:DevOps
Common Name (e.g. server FQDN or YOUR name) []:maxdsre.com
Email Address []:maxdsre@test.com

Please enter the following 'extra' attributes
to be sent with your certificate request
A challenge password []:
An optional company name []:
```

</details>

The second generated string is

```txt
JHHWBFWLm3e0t+K9A3AhVtrq3dMnFzkLflTbZxBhZqA=
```

The final format ist

```sh
add_header Public-Key-Pins 'pin-sha256="tcX5hYyanlC2m/NR+6q2sF58TKQqx4QnCGIi8z/25vc=";pin-sha256="JHHWBFWLm3e0t+K9A3AhVtrq3dMnFzkLflTbZxBhZqA="; max-age=2592000; includeSubDomains';
```

### ssl_dhparam

Stack Exchange has a question [What's the purpose of DH Parameters?](https://security.stackexchange.com/questions/94390/whats-the-purpose-of-dh-parameters).

This command will takes a long time, may be several minutes.

```sh
openssl dhparam -out ${ssl_cert_dir}/dhparam.pem 4096
```

Nginx conf parameter is *ssl_dhparam*.


### ssl_session_ticket_key

See [ngx_stream_ssl_module](http://nginx.org/en/docs/stream/ngx_stream_ssl_module.html#ssl_session_ticket_key) says

>The file must contain 80 or 48 bytes of random data.

Other it wil prompts error info via `nginx -t`

>nginx: [emerg] "/PAT/...ticket.key" must be 48 or 80 bytes in /etc/nginx/nginx.conf:82


```sh
openssl rand 80 > ${ssl_cert_dir}/ssl_session_ticket.key
```

Nginx conf parameter is *ssl_session_ticket_key*.


## Nginx Configs

Cert file list

```sh
$ tree /etc/nginx/ssl.d/

/etc/nginx/ssl.d/
└── XXXXXX
    ├── cert.pem
    ├── dhparam.pem
    ├── fullchain.pem
    ├── hpkp_backup.csr
    ├── hpkp_backup.key
    ├── key.pem
    ├── letsencrypt-ca-cert.pem
    └── ssl_session_ticket.key

1 directory, 8 files
```

Backup default conf

```sh
# backup default conf
cp -p /etc/nginx/nginx.conf{,.origin}
mv /etc/nginx/conf.d/default.conf{,.origin}

# create section path
mkdir -p /etc/nginx/conf.d/section/
```

### nginx conf

Default path */etc/nginx/nginx.conf*

<details><summary>Click to expand nginx.conf</summary>

```conf
user  nginx;

# worker_processes 1 or N or auto
worker_processes 2;
worker_rlimit_nofile 65536;
pid /var/run/nginx.pid;

events {
    worker_connections   65536;
    use epoll;
    multi_accept on;
}

http {
    include /etc/nginx/mime.types;
    default_type  application/octet-stream;
    charset  utf-8;
    autoindex_localtime on;
    server_tokens off;  # Disable version info
    autoindex off;  # Disable file auto index
    sendfile on;
    tcp_nopush on;
    tcp_nodelay on;

    limit_req_zone $binary_remote_addr zone=one:10m rate=1r/s;

    # https://nginx.org/en/docs/http/ngx_http_log_module.html
    # If the format is not specified then the predefined 'combined' format is used.
    # log_format combined '$remote_addr - $remote_user [$time_local] '
    #                     '"$request" $status $body_bytes_sent '
    #                     '"$http_referer" "$http_user_agent"';

    # Custom format name 'main'
    # "$request" == "$request_method $request_uri $server_protocol"
    log_format main '$remote_addr|$time_iso8601|$server_protocol|$request_method|$status|$body_bytes_sent|rt=$request_time uct="$upstream_connect_time" uht="$upstream_header_time" urt="$upstream_response_time"|$http_x_forwarded_for|$http_user_agent|$http_referer|$request_uri';

    error_log  /var/log/nginx/error.log warn;  # [debug|info|notice|warn|error|crit|alert|emerg]
    access_log /var/log/nginx/access.log main if=$logable;
    #Conditional Logging # https://nginx.org/en/docs/http/ngx_http_map_module.html
    map $status $logable {
        ~^[23]  0;
        default 1;
    }

    # Concurrency Connections
    # http://nginx.org/en/docs/http/ngx_http_limit_conn_module.html
    # limit_conn_zone $binary_remote_addr zone=addr:10m;
    limit_conn_zone $binary_remote_addr zone=perip:10m;
    limit_conn_zone $server_name zone=perserver:10m;
    # limit_conn perip 40;
    # http://nginx.org/en/docs/http/ngx_http_limit_req_module.html

    # Keep Alive
    keepalive_timeout 50;
    keepalive_requests 100000;
    # Timeouts
    client_header_timeout  3m;
    client_body_timeout    3m;
    send_timeout 60s;
    # Buffer Size
    client_body_buffer_size      128k;
    client_max_body_size         2m;
    client_header_buffer_size    1k;
    large_client_header_buffers  4 4k;
    output_buffers               1 32k;
    postpone_output              1460;
    #Close connection on Missing Client Response
    reset_timedout_connection on;
    # Static Asset Serving
    open_file_cache max=1000 inactive=20s;
    open_file_cache_valid 30s;
    open_file_cache_min_uses 5;
    open_file_cache_errors off;
    # gzip compression
    gzip on;
    gzip_vary on;
    gzip_comp_level 5;
    gzip_buffers     16 8k;
    gzip_min_length 1000;
    gzip_proxied    expired no-cache no-store private auth;
    gzip_types      text/css application/javascript application/x-javascript text/javascript text/plain text/xml application/json application/vnd.ms-fontobject application/x-font-opentype application/x-font-truetype application/x-font-ttf application/xml font/eot font/opentype font/otf image/svg+xml image/vnd.microsoft.icon;
    gzip_disable    "MSIE [1-6]\.";
    gzip_static on;

    # HTTP proxy
    proxy_set_header Host $host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

    # Hide X-Powered-By
    proxy_hide_header X-Powered-By;

    include /etc/nginx/conf.d/*.conf;  # global server configs
    include /etc/nginx/sites-enabled/*.conf;  # specific server configs
    include /etc/nginx/conf.d/section/ssl.conf; # https://serverfault.com/questions/1097716/tls-1-3-not-working-on-nginx-1-21-with-openssl-1-1-1n
}
```

</details>

### ssl conf

Nginx conf path */etc/nginx/conf.d/section/ssl.conf*

Seperating to make it reuseable in other configs (e.g. subdomain).

<details><summary>Click to expand ssl.conf</summary>

```conf
# ssl section
    ssl_certificate /etc/nginx/ssl.d/maxdsre/fullchain.pem;
    ssl_certificate_key /etc/nginx/ssl.d/maxdsre/key.pem;
    ssl_dhparam /etc/nginx/ssl.d/maxdsre/dhparam.pem;
    ssl_protocols TLSv1.2 TLSv1.3;
    ssl_prefer_server_ciphers on;
    ssl_ciphers "ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES256-GCM-SHA384:AES256-GCM-SHA384:TLS_AES_256_GCM_SHA384:TLS_CHACHA20_POLY1305_SHA256";
    #ssl_ciphers "TLS_AES_128_GCM_SHA256:TLS_AES_256_GCM_SHA384:TLS_CHACHA20_POLY1305_SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384";  # https://wiki.mozilla.org/Security/Server_Side_TLS
    ssl_ecdh_curve secp384r1;  # key exchange curve for ECDHE ciphers. Nginx >= 1.1.0  # prime256v1:secp384r1 for openssl version >= 1.0.2  # secp384r1:prime256v1:X25519;

    ssl_session_cache shared:SSL:10m;
    ssl_session_timeout 10m;
    ssl_session_tickets on;
    ssl_session_ticket_key /etc/nginx/ssl.d/maxdsre/ssl_session_ticket.key; #the list of certificates will not be sent to clients

    #OCSP Stapling Configuration
    ssl_stapling on;
    ssl_stapling_verify on; # Enables verification of OCSP responses by the server
    ssl_trusted_certificate /etc/nginx/ssl.d/maxdsre/letsencrypt-ca-cert.pem;  #Let's Encrypt Root & Intermediate Certificates

    # Google DNS, Cloudflare, Quad9 DNS, Dyn DNS
    resolver 8.8.8.8 8.8.4.4 1.1.1.1 9.9.9.9 216.146.35.35 216.146.36.36 valid=300s;
    resolver_timeout 5s;

    # HSTS (ngx_http_headers_module is required) (63072000 seconds)
    add_header Strict-Transport-Security "max-age=63072000; includeSubdomains; preload";
    add_header Content-Security-Policy "default-src 'none'; script-src 'unsafe-inline' 'self'; font-src 'self'; connect-src 'self'; img-src 'self'; style-src 'unsafe-inline' 'self'; base-uri 'self'; manifest-src 'self'; form-action 'self'";  # https://content-security-policy.com
    add_header X-Content-Type-Options "nosniff" always;
    add_header X-Frame-Options "DENY";  # DENY、SAMEORIGIN、ALLOW-FROM https://example.com/;
    add_header X-XSS-Protection "1; mode=block" always;
    add_header X-Robots-Tag none;
    add_header X-Download-Options noopen;
    add_header X-Permitted-Cross-Domain-Policies none;
    add_header Referrer-Policy strict-origin;
    # add_header Public-Key-Pins 'pin-sha256="tcX5hYyanlC2m/NR+6q2sF58TKQqx4QnCGIi8z/25vc=";pin-sha256="JHHWBFWLm3e0t+K9A3AhVtrq3dMnFzkLflTbZxBhZqA="; max-age=2592000; includeSubDomains'; # optional, no longer recommended by Mozilla https://developer.mozilla.org/en-US/docs/Web/HTTP/Public_Key_Pinning
```

</details>

### default conf

Nginx conf path */etc/nginx/conf.d/default.conf*

Don't specify *http2* after *listen 80*, otherwise it'll fail to redirect http request to https.

<details><summary>Click to expand default.conf</summary>

```conf
# Forbiden access via IP directly instead of domain name
server {
    server_name _;

    listen 80 default_server;
    listen [::]:80 default_server ipv6only=on;

    listen 443 ssl http2 default_server;
    listen [::]:443 ssl http2 default_server ipv6only=on;
    ssl_reject_handshake on;  # from version 1.19.4
    #include /etc/nginx/conf.d/section/ssl.conf;

    # return 444;
    rewrite ^/(.*)$ https://www.google.com/search?q=$1 break;
    #return 301 https://https://www.google.com/search?q=$request_uri;
}

# Redirect http to https
server {
    listen 80;
    #listen [::]:80 ipv6only=on;
    server_name maxdsre.com;
    return 301 https://$server_name$request_uri;
}

server {
    listen 443 ssl http2;
    # listen [::]:443 ssl http2 ipv6only=on;

    server_name maxdsre.com;
    #access_log  /var/log/nginx/log/access.log main; # 'main' is custom log format name
    root   /usr/share/nginx/html;

    include /etc/nginx/conf.d/section/ssl.conf;

    #if ($scheme = http) {
    #    return 301 https://$host$request_uri;
    #}

    #http://nginx.org/en/docs/http/ngx_http_stub_status_module.html
    location /nginx_status {
        stub_status on;
        access_log off;
        #allow xxx.xxx.xxx.xxx; # allowed accessing IP
        allow 127.0.0.1;
        deny all;
    }

    # Check file exists or not
    #http://nginx.org/en/docs/http/ngx_http_core_module.html#try_files
    #http://stackoverflow.com/questions/17798457/how-can-i-make-this-try-files-directive-work#17800131
    #location / {
    #    try_files $uri $uri/ =444;
    #}

    # remove errors from favicon from nginx server logs
    location = /favicon.ico {
        return 204;
        log_not_found off;
    }

    # The ngx_http_core_module module supports embedded variables with names matching the Apache Server variables.
    # https://nginx.org/en/docs/http/ngx_http_core_module.html#variables

    # In regular expression
    # '~' symbol for case-sensitive matching;
    # '~*' for case-insensitive matching;

    # Disable unwanted HTTP methods
    # 405 A request was made of a resource using a request method not supported by that resource;
    if ($request_method !~ ^(GET|HEAD|POST)$)
    {
        return 405;  # 405 Method Not Allowed
    }

    # Deny protocol HTTP/1.1 HTTP/1.0
    # $server_protocol ~* "HTTP/1*"
    if ($server_protocol = '') {
        return 444;  # 444 No Response, close the connection immediately and return nothing.
    }
    if ($server_protocol ~ "HTTP/1.1") {
        return 444;
    }
    if ($server_protocol ~ "HTTP/1.0") {
        rewrite ^/(.*)$ https://www.google.com/search?q=$1 break;
        # return 444;
    }

    # Deny Certain User-Agents or Bots:
    if ($http_user_agent ~* LWP::Simple|Wget|Curl|Python|python-requests|Scrapy|HttpClient|libwww-perl) {
        return 403;  # 403 Forbidden
    }
    if ($http_user_agent ~* (GPTBot|msnbot|Purebot|Baiduspider|Lipperhey|Mail.Ru|scrapbot|archive.org_bot|ia_archiver) ) {
        return 403;  # 403 Forbidden
    }
    if ($http_user_agent ~ "Bytespider|FeedDemon|JikeSpider|Indy Library|Alexa Toolbar|AskTbFXTV|AhrefsBot|CrawlDaddy|CoolpadWebkit|Java|Feedly|UniversalFeedParser|ApacheBench|Microsoft URL Control|Swiftbot|ZmEu|oBot|jaunty|Python-urllib|lightDeckReports Bot|YYSpider|DigExt|YisouSpider|HttpClient|MJ12bot|heritrix|EasouSpider|Ezooms|^$" ) {
        return 403;  # 403 Forbidden
    }

    # Block PHP bruteforce
    location ~* /(wp-admin|wp-content|phpunit)/ {
        log_not_found off;
        access_log off;
        return 444;
    }

    location ~* ^/(wp-admin|wp-login|wp-comments-posts|xmlrpc|index)\.php {
        log_not_found off;
        access_log off;
        # deny all;
        return 444;
    }

    location ~* ^/robots.txt$ {
        log_not_found off;
        access_log off;
        # deny all;
        return 444;
    }

    # Blocking Referral Spam
    #if ( $http_referer ~* #(jewelry|viagra|nude|girl|nudit|casino|poker|porn|sex|teen|babes) ) {
    # return 403;
    # }

    # Stop Hotlinking
    # location ~ .(gif|png|jpe?g)$ {
    #     valid_referers none blocked example.com *.example.com;
    #     if ($invalid_referer) {
    #         return   403;
    #     }
    # }

    # Deny execution of scripts
    # deny scripts inside writable directories
    # location ~* /(images|cache|media|logs|tmp)/.*.(php|pl|py|jsp|asp|sh|cgi)$ {
    #     return 403;
    #     error_page 403 /403_error.html;
    # }

    # file cache
    # location ~* .(woff|eot|ttf|svg|mp4|webm|jpg|jpeg|png|gif|bmp|ico|css|js)$ {
    #     expires 365d;
	# 	log_not_found off;
	# 	access_log off;
    # }
	# location ~ ^/favicon\.ico$ {
	# 	root /usr/share/nginx/html;
	# }

}
```

</details>

Checking and reloading Nginx conf

```sh
# check conf if has syntax error
nginx -t

# reload Nginx conf
nginx -s reload
```


## Firewall Setting

Configuring firewall rules

### Network Based

#### DigitalOcean Cloud Firewalls

Assuming cloud firewall uuid is *b07aa6e0-1276-443b-a2f3-eaae4debb85e*

```sh
firewall_uuid='b07aa6e0-1276-443b-a2f3-eaae4debb85e '

# Allow inbound to TCP/80 (http) from any IPv4/IPv6
doctl compute firewall add-rules "${firewall_uuid}" --inbound-rules "protocol:tcp,ports:80,address:0.0.0.0/0,address:::/0"

# Allow inbound to TCP/443 (https) from any IPv4/IPv6
doctl compute firewall add-rules "${firewall_uuid}" --inbound-rules "protocol:tcp,ports:443,address:0.0.0.0/0,address:::/0"
```

### Host Based

#### ufw

For Debian/Ubuntu, you can choose *ufw* as firewall configuration tool.

```sh
sudo -i

# install
apt-get install -y ufw

ufw default deny incoming
ufw default allow outgoing

# ufw allow ssh  # or # ufw limit ssh
# ufw limit proto tcp from any to any port 22


# ufw allow proto tcp from any to any port 80,443
ufw allow http  # port 80
ufw allow https  # port 443


ufw --force enable # non-interactive
systemctl enable ufw.service
systemctl is-enabled ufw.service
ufw status verbose
```

#### iptables

Checking current existed rules

```sh
sudo iptables --line-numbers -nL
```

Adding the following rules before rule `reject-with icmp-host-prohibited`, assuming the number of this rlue is number *5*.

```sh
#input rule
sudo iptables -t filter -I INPUT 5 -p tcp -m multiport --dports 80,443 -m state --state NEW,ESTABLISHED -j ACCEPT

#output rule
sudo iptables -t filter -I OUTPUT -p tcp -m multiport --sports 80,443 -m state --state ESTABLISHED -j ACCEPT

#simple block DDoS
#sudo iptables -t filter -I INPUT 5 -p tcp -m multiport --dports 80,443 -m connlimit --connlimit-upto 5 -m limit --limit 10/minute --limit-burst 100 -m state --state NEW,ESTABLISHED -j ACCEPT
```

Saving rules to file */etc/sysconfig/iptables*.

```sh
sudo service iptables save
```


## Nginx Security Check

SSL security check via

* [Analyse your HTTP response headers](https://securityheaders.io/)
* [SSL Server Test](https://www.ssllabs.com/ssltest/)

### SSL

Result is `A+`.

![SSL Server Rating](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CyberSecurity/2021-03-02_SSLServerTest/Server_Rating.png)


<details><summary>Click to expand SSL Server summary</summary>

![SSL Server Summary](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CyberSecurity/2021-03-02_SSLServerTest/Server_Summary.png)

</details>


### HTTP Headers

Result is `A`.

![SSL Header Rating](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CyberSecurity/2021-03-02_SSLServerTest/Header_Rating.png)

<details><summary>Click to expand SSL Header summary</summary>

![SSL Header Summary](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CyberSecurity/2021-03-02_SSLServerTest/Header_Summary.png)

</details>


## Error Occruring

Error *too many failed authorizations recently*

> There is a Failed Validation limit of 5 failures per account, per hostname, per hour. -- [Rate Limits](https://letsencrypt.org/docs/rate-limits/)

Error *OCSP ERROR: Exception: connect timed out [http://r3.o.lencr.org]* on SSLLabs

Same issues:

* [OCSP Error when testing new website](https://community.letsencrypt.org/t/ocsp-error-when-testing-new-website/143473)
* [OCSP ERROR on SSLLabs](https://community.letsencrypt.org/t/ocsp-error-on-ssllabs/143975)


## Tutorials

* [nixCraft - How to configure Nginx with Let’s Encrypt on Debian/Ubuntu Linux](https://www.cyberciti.biz/faq/how-to-configure-nginx-with-free-lets-encrypt-ssl-certificate-on-debian-or-ubuntu-linux/)
* [Raymii - Strong SSL Security on nginx](https://raymii.org/s/tutorials/Strong_SSL_Security_On_nginx.html)
* [Nginx - Configuring HTTPS servers](https://nginx.org/en/docs/http/configuring_https_servers.html)
* [proxy_pass url 反向代理的坑](https://xuexb.github.io/learn-nginx/example/proxy_pass.html)
* [How to Disable Direct IP Access in Nginx (HTTP & HTTPS)](https://www.codedodle.com/disable-direct-ip-access-nginx.html)


## Change Logs

* Dec 23, 2016 01:37 Fri Asia/Shanghai
  * first draft
* Mar 02, 2021 12:40 Tue ET
  * rewrite
* Sep 05, 2021 10:05 Sun ET
  * add acme.sh cert renew info


[digitalocean]:https://www.digitalocean.com "DititalOcean - The developer cloud"
[acmesh]:https://github.com/acmesh-official/acme.sh

<!-- End -->