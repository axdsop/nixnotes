# Setting Up YubiKey


## TOC

1. [YubiKey Manager](#yubikey-manager)  
1.1 [Arch Linux](#arch-linux)  
1.1.1 [Installing](#installing)  
1.1.2 [Uninstalling](#uninstalling)  
2. [YubiKey Detail](#yubikey-detail)  
2.1 [ykman info](#ykman-info)  
2.2 [GPG card status](#gpg-card-status)  
2.3 [pcsc_scan](#pcscscan)  
3. [OpenPGP Operation](#openpgp-operation)  
3.1 [Unblock YubiKey Via Resetting Applet (optional)](#unblock-yubikey-via-resetting-applet-optional)  
3.2 [ykman openpgp](#ykman-openpgp)  
3.3 [gpg card edit](#gpg-card-edit)  
3.4 [Key Transfer](#key-transfer)  
3.4.1 [Key generation](#key-generation)  
3.4.2 [Transfer](#transfer-1)  
3.4.3 [Verification](#verification)  
4. [Complete operation procedure](#complete-operation-procedure)  
5. [OpenSSH Operation](#openssh-operation)  
5.1 [Errors](#errors)  
6. [Troubleshooting](#troubleshooting)  
6.1 [No such device](#no-such-device)  
6.2 [Bad secret key](#bad-secret-key)  
6.3 [Bad PIN](#bad-pin)  
6.4 [Failed connecting to YubiKey](#failed-connecting-to-yubikey)  
6.5 [Permission required](#permission-required)  
7. [Bibliography](#bibliography)  
8. [Change Log](#change-log)  


## YubiKey Manager

The [YubiKey Manager CLI][yubikey_manager] tool *ykman* can be used to configure all aspects of the YubiKey. For more usage information and examples, see the [YubiKey Manager CLI User Manual][ykman_manual].

Here just show how to *ykman* install on [Arch Linux][archlinux], if you wanna install it on Ubuntu or MacOS, see [YubiKey Manager](https://www.yubico.com/products/services-software/download/yubikey-manager/), [YubiKey Manager CLI]([yubikey_manager]).


### Arch Linux

[Arch Linux][archlinux] provides [wiki](https://wiki.archlinux.org/index.php/YubiKey) about [YubiKey][yubico].

[Arch Linux][archlinux] wiki [Smartcards#Scan for card reader](https://wiki.archlinux.org/index.php/Smartcards#Scan_for_card_reader) says:

>Install `pcsc-tools` and start the *pcsc_scan* utility, then connect the Smart card reader and finally insert a card.

[Arch Linux][archlinux] wiki [YubiKey#Installation](https://wiki.archlinux.org/index.php/YubiKey#Installation) says:

> [YubiKey Manager][yubikey_manager] — Python library and command-line tool (`ykman`) for configuring a YubiKey over all USB connections.
>
>Note: After you install the *yubikey-manager* ( which can be called by `ykman` in cli ), you need to enable *pcscd.service* to get it running

Page [YubiKey Manager CLI][yubikey_manager] says:
>[Experimental Bash completion](https://developers.yubico.com/yubikey-manager/#_bash_completion) for the command line tool is available, but not enabled by default.


#### Installing

```bash
sudo pacman -Syyu --noconfirm
sudo pacman -S --noconfirm gnupg  # GnuPG
sudo pacman -S --noconfirm pcsc-tools
sudo pacman -S --noconfirm yubikey-manager
# sudo pacman -S --noconfirm yubikey-manager-qt  # GUI (optional)

# enable bash tab completion
# source <(_YKMAN_COMPLETE=source ykman | sudo tee /etc/bash_completion.d/ykman)
bash_completion_dir='/usr/share/bash-completion/completions/'
# [[ -d '/etc/bash_completion.d/' ]] && bash_completion_dir='/etc/bash_completion.d/'
source <(_YKMAN_COMPLETE=source ykman | sudo tee "${bash_completion_dir%/}/ykman")

# enable service pcscd.service
sudo systemctl start pcscd.service
sudo systemctl enable pcscd.service
systemctl status pcscd.service
```

If enable bash completion, it will prompts error info while upgrading. You need to remove it by first.

```bash
yubikey-manager: /usr/share/bash-completion/completions/ykman exists in filesystem
Errors occurred, no packages were upgraded.
```


#### Uninstalling

```bash
sudo pacman -R --noconfirm yubikey-manager

# remove unneeded dependencies
pacman -Qtdq | xargs sudo pacman -Rns --noconfirm
```


## YubiKey Detail

You can [verify your YubiKey](https://www.yubico.com/genuine/) via Web browser.

![verify your YubiKey](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CyberSecurity/2019-07-25_YubiKey/2019-07-25_10-00-21_YubiKey_Verification.png)

You can [test your YubiKey with Yubico OTP](https://demo.yubico.com/otp/verify).

![test your YubiKey with Yubico OTP](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/CyberSecurity/2019-07-25_YubiKey/2019-07-25_10-10-44_Yubico_OTP.png)


### ykman info

Show version

```bash
ykman --version
```

<details><summary>Click to expand output</summary>

```txt
YubiKey Manager (ykman) version: 3.1.2
Libraries:
    libykpers 1.20.0
    libusb 1.0.24
```

</details>

Show general information

```bash
ykman info
# ykman --device $(ykman list --serials) info
```

<details><summary>Click to expand output</summary>

```txt
Device type: YubiKey 5 Nano
Serial number: 10331013
Firmware version: 5.1.2
Form factor: Nano (USB-A)
Enabled USB interfaces: OTP+FIDO+CCID

Applications
OTP             Enabled
FIDO U2F        Enabled
OpenPGP         Enabled
PIV             Enabled
OATH            Enabled
FIDO2           Enabled
```

</details>

### GPG card status

```bash
gpg --card-status
```

<details><summary>Click to expand output</summary>

```txt
Reader ...........: Yubico YubiKey OTP FIDO CCID 00 00
Application ID ...: D2760001240102010006103310130000
Application type .: OpenPGP
Version ..........: 2.1
Manufacturer .....: Yubico
Serial number ....: 10331013
Name of cardholder: [not set]
Language prefs ...: [not set]
Salutation .......:
URL of public key : [not set]
Login data .......: [not set]
Signature PIN ....: forced
Key attributes ...: rsa2048 rsa2048 rsa2048
Max. PIN lengths .: 127 127 127
PIN retry counter : 3 0 3
Signature counter : 0
Signature key ....: [none]
Encryption key....: [none]
Authentication key: [none]
General key info..: [none]
```

</details>


### pcsc_scan

```bash
pcsc_scan -n
```

<details><summary>Click to expand output</summary>

```txt
Using reader plug'n play mechanism
Scanning present readers...
0: Yubico YubiKey OTP+FIDO+CCID 00 00

Sun Mar  8 16:19:50 2020
 Reader 0: Yubico YubiKey OTP+FIDO+CCID 00 00
  Event number: 0
  Card state: Card inserted, Exclusive Mode,
  ATR: 3B FD 13 00 00 81 31 FE 15 80 73 C0 21 C0 57 59 75 62 69 4B 65 79 40
```

</details>


## OpenPGP Operation

Official documents

* [Arch Linux - GnuPG](https://wiki.archlinux.org/index.php/GnuPG)
* [YubiKey - PGP](https://developers.yubico.com/PGP/)
* [GnuPG SmartcardHOWTO](https://gnupg.org/documentation/howtos.html#sec-1-2)

[YubiKey 5 Series Technical Manual](https://support.yubico.com/support/solutions/articles/15000012643-yubikey-manager-cli-ykman-user-guide#ykman_openpgp0ahtmf) says:

>The OpenPGP application provides an OpenPGP compatible smart card according to version 2.0 of the specification. This can be used with compatible PGP software such as GnuPG (GPG) and can store one PGP key each for authentication, signing, and encryption. Similar to the PIV / Smart Card touch policy, the OpenPGP application can also be set to require the metal contact be touched to authorize an operation.

Default Values

type|default value
---|---
PIN | 123456
Admin PIN | 12345678

Supported Algorithms: `RSA 1024`, `RSA 2048`, `RSA 3072`, `RSA 4096`.

Note: `RSA 3072` and `RSA 4096` require GnuPG version 2.0 or higher.

[Using Your YubiKey with OpenPGP](https://support.yubico.com/support/solutions/articles/15000006420-using-your-yubikey-with-openpgp#Applicable_Productsgwwpae) says:

>If you haven't set a User PIN or an Admin PIN for OpenPGP, the default values are `123456` and `12345678`, respectively. If the User PIN and/or Admin PIN have been changed and are not known, the OpenPGP Applet can be reset by following [this article](https://support.yubico.com/support/solutions/articles/15000006421-resetting-the-openpgp-applet-on-the-yubikey).


### Unblock YubiKey Via Resetting Applet (optional)

If you wanna reset applet, you can execute the following commands

```bash
# if prompt error, try to execute 'gpgconf --kill gpg-agent'
[[ -n $(ykman openpgp info 1> /dev/null) ]] && gpgconf --kill gpg-agent

echo 'y' | ykman openpgp reset
ykman openpgp info
```

If you mess up the admin pin (e.g. `PIN retry counter` is `0` ), you need to reset the PGP applet. Official document [Reset the applet](https://developers.yubico.com/ykneo-openpgp/ResetApplet.html), [Resetting the OpenPGP Applet on the YubiKey](https://support.yubico.com/support/solutions/articles/15000006421-resetting-the-openpgp-applet-on-the-yubikey)

**Attention**: This operation will delete the OpenPGP keys from the YubiKey device.

>To make things easier you may want to create a script. To do so, put the following into a script file and run it using gpg-connect-agent (gpg-connect-agent -r FILE).

```bash
yubikey_hex_script=$(mktemp -t XXXXXX)

tee "${yubikey_hex_script}" 1> /dev/null << EOF
/hex
scd serialno
scd apdu 00 20 00 81 08 40 40 40 40 40 40 40 40
scd apdu 00 20 00 81 08 40 40 40 40 40 40 40 40
scd apdu 00 20 00 81 08 40 40 40 40 40 40 40 40
scd apdu 00 20 00 81 08 40 40 40 40 40 40 40 40
scd apdu 00 20 00 83 08 40 40 40 40 40 40 40 40
scd apdu 00 20 00 83 08 40 40 40 40 40 40 40 40
scd apdu 00 20 00 83 08 40 40 40 40 40 40 40 40
scd apdu 00 20 00 83 08 40 40 40 40 40 40 40 40
scd apdu 00 e6 00 00
scd apdu 00 44 00 00
/echo Card has been successfully reset.
/echo The factory default PIN is 123456 and Admin PIN 12345678.
/bye
EOF

gpg-connect-agent -r "${yubikey_hex_script}"
[[ -f "${yubikey_hex_script}" ]] && rm -f "${yubikey_hex_script}"

# close connection
gpg-connect-agent killagent /bye 2> /dev/null  # OK closing connection
gpg-connect-agent updatestartuptty /bye > /dev/null
```

After this, close the connection, remove the card and reinsert it again. The applet should be reset.


### ykman openpgp

Configuring command `openpgp`

```bash
# Display status of OpenPGP application.
ykman openpgp info

# Reset OpenPGP application.
# https://support.yubico.com/support/solutions/articles/15000006421-resetting-the-openpgp-applet-on-the-yubikey
# ykman openpgp reset
echo 'y' | ykman openpgp reset

# WARNING! This will delete all stored OpenPGP keys and data and restore factory settings? [y/N]: y
# Resetting OpenPGP data, don't remove your YubiKey...
# Success! All data has been cleared and default PINs are set.
# PIN:         123456
# Reset code:  NOT SET
# Admin PIN:   12345678

admin_pin_default='12345678'

# Set PIN, Reset Code and Admin PIN retries.
# PIN-RETRIES RESET-CODE-RETRIES ADMIN-PIN-RETRIES
# ykman openpgp set-pin-retries 3 3 3
ykman openpgp set-pin-retries -a ${admin_pin_default} -f 3 3 3

# Set touch policy for OpenPGP keys.
# ykman openpgp set-touch [OPTIONS] KEY POLICY
# KEY     Key slot to set (sig, enc, aut or att).
# POLICY  Touch policy to set (on, off, fixed, cached or cached-fixed).
# ykman openpgp set-touch aut on
ykman openpgp set-touch -a ${admin_pin_default} -f sig on
ykman openpgp set-touch -a ${admin_pin_default} -f enc on
ykman openpgp set-touch -a ${admin_pin_default} -f aut on

ykman openpgp info

# Touch policies
# Signature key           Off --> ON
# Encryption key          Off --> ON
# Authentication key      Off --> ON
```

If your PIN or Admin PIN retries num is 0, when you transfer you key to Yubikey, it will prompts *gpg: KEYTOCARD failed: Bad PIN* like [Yibico Forum - Cannot reset PIN using gpg-connect-agent](https://forum.yubico.com/viewtopic78fa.html).

### gpg card edit

```bash
# Show the content of the smart card
gpg --card-status

gpg --card-edit
```

Optional fields list

List pharse | Command | Available values |Comment
---|---|---|---
Name of cardholder | `name` | surname, giving name | change card holder's name
Language prefs | `lang` | |change the language preferences
Sex | `sex` | (M)ale, (F)emale or space | change card holder's sex
URL of public key | `url` | | change URL to retrieve key
Login data | `login` | | change the login name
Signature PIN | `forcesig` | | toggle the signature force PIN flag
Key attributes | `key-attr` | rsa, ecc | change the key attribute


Change *PIN* and *Admin PIN* (optional)

Type|Default Value
---|---
PIN|123456
Admin PIN|12345678


```txt
gpg/card> admin
Admin commands are allowed

gpg/card> help
quit           quit this menu
admin          show admin commands
help           show this help
list           list all available data
name           change card holder's name
url            change URL to retrieve key
fetch          fetch the key specified in the card URL
login          change the login name
lang           change the language preferences
salutation     change card holder's salutation
cafpr          change a CA fingerprint
forcesig       toggle the signature force PIN flag
generate       generate new keys
passwd         menu to change or unblock the PIN
verify         verify the PIN and list all data
unblock        unblock the PIN using a Reset Code
factory-reset  destroy all keys and data
kdf-setup      setup KDF for PIN authentication
key-attr       change the key attribute

gpg/card> passwd
gpg: OpenPGP card no. D2760001240102010006103310130000 detected

1 - change PIN
2 - unblock PIN
3 - change Admin PIN
4 - set the Reset Code
Q - quit

Your selection? 1
PIN changed.

1 - change PIN
2 - unblock PIN
3 - change Admin PIN
4 - set the Reset Code
Q - quit

Your selection? 3
PIN changed.

1 - change PIN
2 - unblock PIN
3 - change Admin PIN
4 - set the Reset Code
Q - quit

Your selection? q

gpg/card>
```

Change the key attribute (need to input Admin PIN,used by *gpg/card> generate*)

```txt
gpg/card> key-attr
Changing card key attribute for: Signature key
Please select what kind of key you want:
   (1) RSA
   (2) ECC
Your selection? 1
What keysize do you want? (2048) 4096
The card will now be re-configured to generate a key of 4096 bits
Changing card key attribute for: Encryption key
Please select what kind of key you want:
   (1) RSA
   (2) ECC
Your selection? 1
What keysize do you want? (2048) 4096
The card will now be re-configured to generate a key of 4096 bits
Changing card key attribute for: Authentication key
Please select what kind of key you want:
   (1) RSA
   (2) ECC
Your selection? 1
What keysize do you want? (2048) 4096
The card will now be re-configured to generate a key of 4096 bits

gpg/card>
```

List configuration

```txt
gpg/card> list

Reader ...........: Yubico YubiKey OTP FIDO CCID 00 00
Application ID ...: D2760001240102010006103310130000
Version ..........: 2.1
Manufacturer .....: Yubico
Serial number ....: 10331013
Name of cardholder: [not set]
Language prefs ...: [not set]
Sex ..............: unspecified
URL of public key : [not set]
Login data .......: [not set]
Signature PIN ....: not forced
Key attributes ...: rsa4096 rsa4096 rsa4096
Max. PIN lengths .: 127 127 127
PIN retry counter : 3 0 3
Signature counter : 0
Signature key ....: [none]
Encryption key....: [none]
Authentication key: [none]
General key info..: [none]

gpg/card>
```

### Key Transfer

Official document [Using Your YubiKey with OpenPGP](https://support.yubico.com/support/solutions/articles/15000006420-using-your-yubikey-with-openpgp).

#### Key generation

If you don't know how to generate gpg key, please read my note [CyberSecurity/GnuPG](./CyberSecurity/GnuPG/README.md).

You can use my shell script [GnuPG.sh](./Script/CyberSecurity/GnuPG/GnuPG.sh) to generate GPG keys

```bash
curl -fsL https://gitlab.com/axdsop/nixnotes/raw/master/CyberSecurity/GnuPG/GnuPG.sh | bash -s -- -h

# If you use parameter '-d' to specify custom GNUPGHOME dir, such as '/tmp/gnupg_AAAAA'. Remember to exexute 'export GNUPGHOME=/tmp/gnupg_AAAAA' to make it effect.
# export GNUPGHOME=/tmp/gnupg_AAAAA
```


Generated key info

```txt
sec#  rsa4096/0x6DC7C6DFDBACA766 2020-03-09 [C] [expires: 2021-03-09]
      Key fingerprint = 20DC 7ED4 2DC8 3649 1EE3  C8B7 6DC7 C6DF DBAC A766
uid                   [ultimate] User_test (Comment_test) <email@test.com>
ssb   rsa4096/0xCD4369EF27C16EDD 2020-03-09 [S] [expires: 2021-03-09]
ssb   rsa4096/0x84946736BC905B46 2020-03-09 [A] [expires: 2021-03-09]
ssb   rsa4096/0x9C49ED6FED76AEFE 2020-03-09 [E] [expires: 2021-03-09]
```

Punctuation symbol `#` in `sec#` means the secret part of the master key has been removed from keyring, therefore the master key is not usable anymore to modifying or adding subkeys.

#### Transfer

Transfer key from GPG to YubiKey, it will prompts *key passphrase* and *Admin PIN*.

```bash
# gpgconf --kill gpg-agent

secret_key_id=$(gpg2 --with-fingerprint --list-secret-key  2>&1 | sed -r -n '/^sec/{s@^[^\/]+/([^[:space:]]+).*$@\1@g;p;q}')
echo "secret key id ${secret_key_id}"

key_fpr=$(gpg --list-options show-only-fpr-mbox --list-secret-keys | sed -r -n '$!d;s@^([^[:space:]]+).*@\1@g;p')
echo "key_fpr ${key_fpr}"

gpg2 --edit-key "${key_fpr}"
```

If will prompts Admin PIN just one time, but it will also prompts passpharse every time if your GPG keys has passpharse.

Signature key

```txt
Secret subkeys are available.

pub  rsa4096/0x6DC7C6DFDBACA766
     created: 2020-03-09  expires: 2021-03-09  usage: C
     trust: ultimate      validity: ultimate
ssb  rsa4096/0xCD4369EF27C16EDD
     created: 2020-03-09  expires: 2021-03-09  usage: S
ssb  rsa4096/0x84946736BC905B46
     created: 2020-03-09  expires: 2021-03-09  usage: A
ssb  rsa4096/0x9C49ED6FED76AEFE
     created: 2020-03-09  expires: 2021-03-09  usage: E
[ultimate] (1). User_test (Comment_test) <email@test.com>

gpg> key 1

pub  rsa4096/0x6DC7C6DFDBACA766
     created: 2020-03-09  expires: 2021-03-09  usage: C
     trust: ultimate      validity: ultimate
ssb* rsa4096/0xCD4369EF27C16EDD
     created: 2020-03-09  expires: 2021-03-09  usage: S
ssb  rsa4096/0x84946736BC905B46
     created: 2020-03-09  expires: 2021-03-09  usage: A
ssb  rsa4096/0x9C49ED6FED76AEFE
     created: 2020-03-09  expires: 2021-03-09  usage: E
[ultimate] (1). User_test (Comment_test) <email@test.com>

gpg> keytocard
Please select where to store the key:
   (1) Signature key
   (3) Authentication key
Your selection? 1

pub  rsa4096/0x6DC7C6DFDBACA766
     created: 2020-03-09  expires: 2021-03-09  usage: C
     trust: ultimate      validity: ultimate
ssb* rsa4096/0xCD4369EF27C16EDD
     created: 2020-03-09  expires: 2021-03-09  usage: S
ssb  rsa4096/0x84946736BC905B46
     created: 2020-03-09  expires: 2021-03-09  usage: A
ssb  rsa4096/0x9C49ED6FED76AEFE
     created: 2020-03-09  expires: 2021-03-09  usage: E
[ultimate] (1). User_test (Comment_test) <email@test.com>

```

Authentication key

```txt
gpg> key 1

pub  rsa4096/0x6DC7C6DFDBACA766
     created: 2020-03-09  expires: 2021-03-09  usage: C
     trust: ultimate      validity: ultimate
ssb  rsa4096/0xCD4369EF27C16EDD
     created: 2020-03-09  expires: 2021-03-09  usage: S
ssb  rsa4096/0x84946736BC905B46
     created: 2020-03-09  expires: 2021-03-09  usage: A
ssb  rsa4096/0x9C49ED6FED76AEFE
     created: 2020-03-09  expires: 2021-03-09  usage: E
[ultimate] (1). User_test (Comment_test) <email@test.com>

gpg> key 2

pub  rsa4096/0x6DC7C6DFDBACA766
     created: 2020-03-09  expires: 2021-03-09  usage: C
     trust: ultimate      validity: ultimate
ssb  rsa4096/0xCD4369EF27C16EDD
     created: 2020-03-09  expires: 2021-03-09  usage: S
ssb* rsa4096/0x84946736BC905B46
     created: 2020-03-09  expires: 2021-03-09  usage: A
ssb  rsa4096/0x9C49ED6FED76AEFE
     created: 2020-03-09  expires: 2021-03-09  usage: E
[ultimate] (1). User_test (Comment_test) <email@test.com>

gpg> keytocard
Please select where to store the key:
   (3) Authentication key
Your selection? 3

pub  rsa4096/0x6DC7C6DFDBACA766
     created: 2020-03-09  expires: 2021-03-09  usage: C
     trust: ultimate      validity: ultimate
ssb  rsa4096/0xCD4369EF27C16EDD
     created: 2020-03-09  expires: 2021-03-09  usage: S
ssb* rsa4096/0x84946736BC905B46
     created: 2020-03-09  expires: 2021-03-09  usage: A
ssb  rsa4096/0x9C49ED6FED76AEFE
     created: 2020-03-09  expires: 2021-03-09  usage: E
[ultimate] (1). User_test (Comment_test) <email@test.com>

```

Encryption key

```txt
gpg> key 2

pub  rsa4096/0x6DC7C6DFDBACA766
     created: 2020-03-09  expires: 2021-03-09  usage: C
     trust: ultimate      validity: ultimate
ssb  rsa4096/0xCD4369EF27C16EDD
     created: 2020-03-09  expires: 2021-03-09  usage: S
ssb  rsa4096/0x84946736BC905B46
     created: 2020-03-09  expires: 2021-03-09  usage: A
ssb  rsa4096/0x9C49ED6FED76AEFE
     created: 2020-03-09  expires: 2021-03-09  usage: E
[ultimate] (1). User_test (Comment_test) <email@test.com>

gpg> key 3

pub  rsa4096/0x6DC7C6DFDBACA766
     created: 2020-03-09  expires: 2021-03-09  usage: C
     trust: ultimate      validity: ultimate
ssb  rsa4096/0xCD4369EF27C16EDD
     created: 2020-03-09  expires: 2021-03-09  usage: S
ssb  rsa4096/0x84946736BC905B46
     created: 2020-03-09  expires: 2021-03-09  usage: A
ssb* rsa4096/0x9C49ED6FED76AEFE
     created: 2020-03-09  expires: 2021-03-09  usage: E
[ultimate] (1). User_test (Comment_test) <email@test.com>

gpg> keytocard
Please select where to store the key:
   (2) Encryption key
Your selection? 2

pub  rsa4096/0x6DC7C6DFDBACA766
     created: 2020-03-09  expires: 2021-03-09  usage: C
     trust: ultimate      validity: ultimate
ssb  rsa4096/0xCD4369EF27C16EDD
     created: 2020-03-09  expires: 2021-03-09  usage: S
ssb  rsa4096/0x84946736BC905B46
     created: 2020-03-09  expires: 2021-03-09  usage: A
ssb* rsa4096/0x9C49ED6FED76AEFE
     created: 2020-03-09  expires: 2021-03-09  usage: E
[ultimate] (1). User_test (Comment_test) <email@test.com>

gpg> list

pub  rsa4096/0x6DC7C6DFDBACA766
     created: 2020-03-09  expires: 2021-03-09  usage: C
     trust: ultimate      validity: ultimate
ssb  rsa4096/0xCD4369EF27C16EDD
     created: 2020-03-09  expires: 2021-03-09  usage: S
ssb  rsa4096/0x84946736BC905B46
     created: 2020-03-09  expires: 2021-03-09  usage: A
ssb* rsa4096/0x9C49ED6FED76AEFE
     created: 2020-03-09  expires: 2021-03-09  usage: E
[ultimate] (1). User_test (Comment_test) <email@test.com>

gpg> save

```

#### Verification

```bash
gpg --list-secret-keys
gpg -K
```

Output

```txt
sec#  rsa4096/0x6DC7C6DFDBACA766 2020-03-09 [C] [expires: 2021-03-09]
      Key fingerprint = 20DC 7ED4 2DC8 3649 1EE3  C8B7 6DC7 C6DF DBAC A766
uid                   [ultimate] User_test (Comment_test) <email@test.com>
ssb>  rsa4096/0xCD4369EF27C16EDD 2020-03-09 [S] [expires: 2021-03-09]
ssb>  rsa4096/0x84946736BC905B46 2020-03-09 [A] [expires: 2021-03-09]
ssb>  rsa4096/0x9C49ED6FED76AEFE 2020-03-09 [E] [expires: 2021-03-09]
```

Punctuation symbol `>` in `ssb>` indicates the sub-keys have been moved to YubiKey.


```bash
gpg --card-status
```

Output

```txt
Reader ...........: Yubico YubiKey OTP FIDO CCID 00 00
Application ID ...: D2760001240102010006103310130000
Application type .: OpenPGP
Version ..........: 2.1
Manufacturer .....: Yubico
Serial number ....: 10331013
Name of cardholder: [not set]
Language prefs ...: [not set]
Salutation .......:
URL of public key : [not set]
Login data .......: [not set]
Signature PIN ....: forced
Key attributes ...: rsa4096 rsa4096 rsa4096
Max. PIN lengths .: 127 127 127
PIN retry counter : 3 0 3
Signature counter : 0
Signature key ....: 7F5D B637 CC84 7AE1 E800  C638 CD43 69EF 27C1 6EDD
      created ....: 2020-03-09 17:53:23
Encryption key....: 81B4 27EF 57D6 135D D716  444A 9C49 ED6F ED76 AEFE
      created ....: 2020-03-09 17:53:29
Authentication key: 3400 431B BD66 2685 B0BD  7BAA 8494 6736 BC90 5B46
      created ....: 2020-03-09 17:53:25
General key info..: sub  rsa4096/0xCD4369EF27C16EDD 2020-03-09 User_test (Comment_test) <email@test.com>
sec#  rsa4096/0x6DC7C6DFDBACA766  created: 2020-03-09  expires: 2021-03-09
ssb>  rsa4096/0xCD4369EF27C16EDD  created: 2020-03-09  expires: 2021-03-09
                                  card-no: 0006 10331013
ssb>  rsa4096/0x84946736BC905B46  created: 2020-03-09  expires: 2021-03-09
                                  card-no: 0006 10331013
ssb>  rsa4096/0x9C49ED6FED76AEFE  created: 2020-03-09  expires: 2021-03-09
                                  card-no: 0006 10331013
```


## Complete operation procedure

Smart card configuration

```bash
# if prompt error, try to execute 'gpgconf --kill gpg-agent'
# ykman openpgp info
[[ -n $(ykman openpgp info 1> /dev/null) ]] && gpgconf --kill gpg-agent

echo 'y' | ykman openpgp reset
ykman openpgp info

# Change touch policy
admin_pin_now='12345678'
ykman openpgp set-pin-retries -a ${admin_pin_now} -f 5 5 5
ykman openpgp set-touch -a ${admin_pin_now} -f sig on
ykman openpgp set-touch -a ${admin_pin_now} -f enc on
ykman openpgp set-touch -a ${admin_pin_now} -f aut on
ykman openpgp info

gpg --card-status

gpg --card-edit

gpg/card> admin
# Admin commands are allowed

# Change PIN, Admin PIN (optional)
gpg/card> passwd
gpg/card> verify

# Change card key attribute for: Signature/Encryption/Authentication key
gpg/card> key-attr

gpg/card> quit

gpg --card-status
```

Generating GPG keys

```bash
passphrase_specify='1Qaz@wSx'
user_specify='User_test'
comment_specify='Comment_test'
email_specify='email@test.com'
expire_date_specify='1y'
gnupg_home_specify=$(mktemp -t -d GnuPG_XXXXXX)

echo "GnuPG home specify ${gnupg_home_specify}"

# generate gpg key via my shell script
# rm -rf ~/Documents/KeyManagement/GnuPG/ ~/.gnupg/ /tmp/gnupg_*

# User_test (Comment_test) <email@test.com>
curl -fsL https://gitlab.com/axdsop/nixnotes/raw/master/CyberSecurity/GnuPG/GnuPG.sh | bash -s -- -n "${user_specify}" -m "${email_specify}" -C "${comment_specify}" -e "${expire_date_specify}" -t rsa -b 4096 -p "${passphrase_specify}" -a -d "${gnupg_home_specify}"

# prompt info include, without sec#, just sec
# export GNUPGHOME=/tmp/gnupg_XXXXXXXXX

# with sec#
export GNUPGHOME="${gnupg_home_specify}"

gpg2 -K
# gpg2 --list-secret-keys

# gpg2 --show-keys /PATH/pub.asc can also get key id
secret_key_id=$(gpg2 --with-fingerprint --list-secret-key  2>&1 | sed -r -n '/^sec/{s@^[^\/]+/([^[:space:]]+).*$@\1@g;p;q}')
echo "secret key id ${secret_key_id}"

key_fpr=$(gpg --list-options show-only-fpr-mbox --list-secret-keys | sed -r -n '$!d;s@^([^[:space:]]+).*@\1@g;p')
echo "key_fpr: ${key_fpr}"
```

Transfer GPG key to smart card YubiKey

Please re-insert YubiKey card, or it will prompts error info

>gpg: selecting card failed: No such device
>gpg: OpenPGP card not available: No such device


If will prompts Admin PIN just one time, but it will also prompts passpharse every time if your GPG keys has passpharse.

```bash
gpg --edit-key "${key_fpr}"

# (1) Signature key
gpg> key 1
gpg> keytocard

# (3) Authentication key
gpg> key 1
gpg> key 2
gpg> keytocard

# (2) Encryption key
gpg> key 2
gpg> key 3
gpg> keytocard

gpg> key 3
gpg> list
gpg> save

# Show the content of the smart card.
gpg --card-status
```

Copying configuarion file to `~/.gnupg`

```bash
gnupg_default_dir="$HOME/.gnupg/"

if [[ ! -d "${gnupg_default_dir}" ]]; then
     mkdir -p "${gnupg_default_dir}"
     chown $USER "${gnupg_default_dir}"
     chmod 700 "${gnupg_default_dir}"
fi

find "${gnupg_home_specify}" -type f -name "*.conf" | while read -r line; do
     new_path="${gnupg_default_dir%/}/${line##*/}"
     [[ -f "${new_path}" ]] || cp "${line}" "${new_path}"
     # chmod 640 "${new_path}"
done
```

## OpenSSH Operation

* [GnuPG#SSH_agent](https://wiki.archlinux.org/index.php/GnuPG#SSH_agent)
* [yubico - SSH](https://developers.yubico.com/PGP/SSH_authentication/)


```bash
sed -r -i '/enable-ssh-support/{s@^#*[[:space:]]*@@g;}' ~/.gnupg/gpg-agent.conf
sed -r -i '/default-cache-ttl-ssh/{s@^#*[[:space:]]*@@g;}' ~/.gnupg/gpg-agent.conf
sed -r -i '/max-cache-ttl-ssh/{s@^#*[[:space:]]*@@g;}' ~/.gnupg/gpg-agent.conf

# import public key
# gpg --import /PATH/public.asc

[[ -n $(ykman openpgp info 1> /dev/null) ]] && gpgconf --kill gpg-agent

# restart agent, if skip this step, it will use gui-based prompt while using 'ssh-add'
# gpg-connect-agent killagent /bye 1> /dev/null
gpg-connect-agent reloadagent /bye 1> /dev/null


# Could not open a connection to your authentication agent.
# https://wiki.archlinux.org/index.php/GnuPG#SSH_agent

tee -a ~/.bashrc 1> /dev/null << EOF
# GPG For SSH Start
export GPG_TTY="$(tty)"
unset SSH_AGENT_PID
if [ "${gnupg_SSH_AUTH_SOCK_by:-0}" -ne $$ ]; then
  export SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"
fi
gpgconf --launch gpg-agent
# GPG For SSH End
EOF

# export GPG_TTY="$(tty)"
# export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
# gpgconf --launch gpg-agent

source ~/.bashrc

# Could not add identity "XXXXXXX": agent refused operation
gpg-connect-agent updatestartuptty /bye > /dev/null

# file ~/.gnupg/sshcontrol stores approved keys
ssh-add /PATH/id_ed25519

# Lists public key parameters of all identities currently represented by the agent
ssh-add -L
# add 'ssh-rsa ********** cardno:000610331013' into GitHub/GitLab profile setting SSH Keys.

# Lists fingerprints of all identities currently represented by the agent.
ssh-add -l

# ~/.gitconfig
git config --global user.name "USERNAME"
git config --global user.email "EMAIL"
git config --global credential.helper 'cache --timeout=7200'
git config --global core.autocrlf input
git config --global core.editor vim
git config --global push.default simple
git config --global gpg.program gpg2
git config --global commit.gpgsign true

# gpg2 --list-secret-keys --keyid-format LONG
# GPG key ID that starts with sec
git config --global user.signingkey 0x6DC7C6DFDBACA766

# -T    Disable pseudo-terminal allocation.
# ssh -T git@gitlab.com
ssh -o StrictHostKeyChecking=no -T git@gitlab.com

ssh -T -p 443 -o StrictHostKeyChecking=no git@ssh.github.com

gpg --armor --export 0x6DC7C6DFDBACA766
# https://gitlab.com/help/user/project/repository/gpg_signed_commits/index.md
# copy the public key and add it in GitHub/GitLab profile setting GPG Keys.
```

**Notice**: You need to import the public key via `gpg2 --import`, or it will prompts error info while you're committing verified message.

>error: gpg failed to sign the data
>fatal: failed to write commit object


### Errors

If prompt errors

```txt
fatal: Could not read from remote repository.

Please make sure you have the correct access rights
and the repository exists.
```

Solution is execute

```bash
gpg-connect-agent updatestartuptty /bye > /dev/null
```


## Troubleshooting

### No such device

```txt
gpg: selecting openpgp failed: No such device
gpg: OpenPGP card not available: No such device
```

Solution [Troubleshooting GPG "No such device"](https://support.yubico.com/support/solutions/articles/15000014892-troubleshooting-gpg-no-such-device-)

```bash
gpgconf --kill gpg-agent
```

### Bad secret key

```txt
gpg: KEYTOCARD failed: Bad secret key
```

### Bad PIN

```txt
gpg: KEYTOCARD failed: Bad PIN
```

### Failed connecting to YubiKey

```txt
Error: Failed connecting to YubiKey 5 [OTP+FIDO+CCID]
```


### Permission required

```txt
Error: Failed connecting to YubiKey 5 [OTP+FIDO+CCID]. Make sure the application have the required permissions.
```


## Bibliography

* [YubiKey 4 series GPG and SSH setup guide](https://gist.github.com/ageis/14adc308087859e199912b4c79c4aaa4)
* [GitHub drduh/YubiKey-Guide](https://github.com/drduh/YubiKey-Guide)
* [GitHub foundriesio/gpg-key-signing](https://github.com/foundriesio/gpg-key-signing)
* [A guide to setting up & managing GPG keys on a Yubikey 4](https://disjoint.ca/til/2017/10/05/a-guide-to-setting-up--managing-gpg-keys-on-a-yubikey-4/)
* [OpenPGP On The Job � Part 7: Improved Security With YubiKey](https://blogs.itemis.com/en/openpgp-on-the-job-part-7-improved-security-with-yubikey)


## Change Log

* Jul 16, 2019 Tue 09:54 ET
  * First draft
* Jul 24, 2019 Wed 22:45 ET
  * Add OpenPGP and OpenSSH operation
* Mar 08, 2020 Sun 21:20 ET
  * Operation records update, add reset applet.
* Mar 09, 2020 Mon 16:51 ET
  * Optimize whole procedure


[archlinux]:https://www.archlinux.org/
[aur]:https://wiki.archlinux.org/index.php/Arch_User_Repository
[archlinux_smartcards]:https://wiki.archlinux.org/index.php/Smartcards
[yubico]:https://www.yubico.com/ "Yubico | YubiKey Strong Two Factor Authentication for Business and Individual Use."
[dev_yubico]:https://developers.yubico.com/
[yubikey_manager]:https://developers.yubico.com/yubikey-manager/
[ykman_manual]:https://support.yubico.com/support/solutions/articles/15000012643-yubikey-manager-cli-ykman-user-guide
[yubikey_5_manual]:https://support.yubico.com/support/solutions/articles/15000014219-yubikey-5-series-technical-manual
[yubikey_5_nano]:https://support.yubico.com/support/solutions/articles/15000014182-yubikey-5-nano
[yubikey_5_nfc]:https://support.yubico.com/support/solutions/articles/15000014174--yubikey-5-nfc


<!-- End -->
