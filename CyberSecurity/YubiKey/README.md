# YubiKey Simple Usage

The [YubiKey][yubico] is a hardware authentication device manufactured by Yubico that supports one-time passwords, public-key encryption and authentication, and the Universal 2nd Factor (U2F) and FIDO2 protocols developed by the FIDO Alliance.

I bought a [YubiKey 5 NFC](https://support.yubico.com/support/solutions/articles/15000014174--yubikey-5-nfc) and a [YubiKey 5 Nano][yubikey_5_nano] from Amazon. I use them as a SmartCard for storing GPG keys (sign, auth, ecrypt), which can also be used for Git, SSH.

From OpenSSH [version 8.2](https://www.openssh.com/releasenotes.html#8.2) (Feb 14, 2020), OpenSSH supports FIDO /U2F hardware authenticators. But [YubiKey][yubico] needs [firmware 5.2.3](https://www.yubico.com/blog/whats-new-in-yubikey-firmware-5-2-3/) to support `ed25519-sk` key type.


## TOC

1. [Official Resources](#official-resources)  
1.1 [Firmware Upgrading](#firmware-upgrading)  
2. [Operation Notes](#operation-notes)  
3. [YubiKey Support Scenario](#yubikey-support-scenario)  
4. [Change Log](#change-log)  


## Official Resources

* [Yubico Developers](https://developers.yubico.com/)
* [Yubico Support](https://support.yubico.com/support/home)
  * [YubiKey 5 Series Quick Start Guide](https://support.yubico.com/support/solutions/articles/15000014330-yubikey-5-series-quick-start-guide)
  * [YubiKey 5 Series Technical Manual][yubikey_5_manual]
* [YubiKey Manager CLI (ykman) User Manual][ykman_manual]


### Firmware Upgrading

Official document [Upgrading YubiKey Firmware](https://support.yubico.com/support/solutions/articles/15000006434-upgrading-yubikey-firmware) says:

>It is currently not possible to upgrade YubiKey firmware. To prevent attacks on the YubiKey which might compromise its security, the YubiKey does not permit its firmware to be accessed or altered.


## Operation Notes

* [Setting Up](./1-0_SettingUp.md)


## YubiKey Support Scenario

[YubiKey][yubico] is a small USB dongle with one button and an LED to communicate with you. It works with hundreds of enterprise, developer and consumer applications, out-of-the-box and with no client software.

![yubico support scenario](https://www.yubico.com/wp-content/uploads/2018/04/why-yubico-logos-img-v2@2x.jpg)


## Change Log

* Jul 16, 2019 Tue 09:54 ET
  * First draft
* Apr 17, 2020 Fri 06:38 ET
  * Add [setting up](./1-0_SettingUp.md) note


[archlinux]:https://www.archlinux.org/
[aur]:https://wiki.archlinux.org/index.php/Arch_User_Repository
[archlinux_smartcards]:https://wiki.archlinux.org/index.php/Smartcards
[yubico]:https://www.yubico.com/ "Yubico | YubiKey Strong Two Factor Authentication for Business and Individual Use."
[dev_yubico]:https://developers.yubico.com/
[yubikey_manager]:https://developers.yubico.com/yubikey-manager/
[ykman_manual]:https://support.yubico.com/support/solutions/articles/15000012643-yubikey-manager-cli-ykman-user-guide
[yubikey_5_manual]:https://support.yubico.com/support/solutions/articles/15000014219-yubikey-5-series-technical-manual
[yubikey_5_nano]:https://support.yubico.com/support/solutions/articles/15000014182-yubikey-5-nano
[yubikey_5_nfc]:https://support.yubico.com/support/solutions/articles/15000014174--yubikey-5-nfc

<!-- End -->
