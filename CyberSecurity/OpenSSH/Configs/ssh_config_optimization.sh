#!/usr/bin/env bash


# copy key to remote server
# via ssh-copy-id
# ssh-copy-id -i ~/.ssh/id_ed25519.pub -p 22 XXX@192.168.0.XXX
# manually
# cat $HOME/.ssh/id_ed25519.pub | ssh -p 22 XXX@192.168.0.XXX '(umask 077; [[ -d ~/.ssh ]] || mkdir ~/.ssh; cat >> ~/.ssh/authorized_keys)'


#########  1. Paramater Vairables  #########

cp /home/flying/Documents/Projects/nix_life/CyberSecurity/OpenSSH/Configs/sshd_config /tmp/test_para_list
sshd_para_list_path=/tmp/test_para_list
ssh_config_sample=/home/flying/Documents/Projects/nix_life/CyberSecurity/OpenSSH/Configs/ssh_config

#########  1. Base Functions  #########
fn_Preparation(){
    # - Login user info
    # $EUID is 0 if run as root, default is user id, use command `who` to detect login user name
    # $USER exist && $SUDO_USER not exist, then use $USER
    [[ -n "${USER:-}" && -z "${SUDO_USER:-}" ]] && login_user="${USER:-}" || login_user="${SUDO_USER:-}"
    [[ -z "${login_user}" ]] && login_user=$(logname 2> /dev/null)
    # [[ -z "${login_user}" ]] && login_user=$(who 2> /dev/null | cut -d' ' -f 1)
    # not work properly while using sudo
    [[ -z "${login_user}" ]] && login_user=$(id -u -n 2> /dev/null)
    [[ -z "${login_user}" ]] && login_user=$(ps -eo 'uname,comm,cmd' | sed -r -n '/xinit[[:space:]]+\/(root|home)\//{s@^([^[:space:]]+).*@\1@g;p;q}')

    # awk -F: 'match($1,/^'"${login_user}"'$/){print $6}' /etc/passwd
    # sed -n '/^'"${login_user}"':/{p}' /etc/passwd | cut -d: -f6
    [[ "${login_user}" == 'root' ]] && login_user_home='/root' || login_user_home="/home/${login_user}"
    [[ -f /etc/passwd ]] && login_user_home=$(sed -r -n '/^'"${login_user}"':/{p}' /etc/passwd | cut -d: -f6)
    login_user_home="${login_user_home%/}"

    # - Download Method
    download_method='wget -qO-'
    [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && download_method='curl -fsSL'

    # - OpenSSH Version
    ssh_intalled_version=''
    ssh_intalled_version=$(ssh -V 2>&1 | sed -r -n 's@.*_([[:digit:].]{3}).*@\1@p')
}

fn_VersionNumberComparasion(){
    local l_item1="${1:-0}"    # old val
    local l_item2="${2:-0}"    # new val
    local l_operator=${3:-'<'}
    local l_output=${l_output:-0}

    if [[ -n "${l_item1}" && -n "${l_item2}" ]]; then
        local l_arr1=( ${l_item1//./ } )
        local l_arr2=( ${l_item2//./ } )
        local l_arr1_len=${l_arr1_len:-"${#l_arr1[@]}"}
        local l_arr2_len=${l_arr2_len:-"${#l_arr2[@]}"}
        local l_max_len=${l_max_len:-"${l_arr1_len}"}
        [[ "${l_arr1_len}" -lt "${l_arr2_len}" ]] && l_max_len="${l_arr2_len}"

        for (( i = 0; i < "${l_max_len}"; i++ )); do
            [[ "${l_arr1_len}" -lt $(( i+1 )) ]] && l_arr1[$i]=0
            [[ "${l_arr2_len}" -lt $(( i+1 )) ]] && l_arr2[$i]=0

            if [[ "${l_arr1[i]}" -lt "${l_arr2[i]}" ]]; then
                case "${l_operator}" in
                    '>' ) l_output=0 ;;
                    '<'|* ) l_output=1 ;;
                esac
                break
            elif [[ "${l_arr1[i]}" -gt "${l_arr2[i]}" ]]; then
                case "${l_operator}" in
                    '>' ) l_output=1 ;;
                    '<'|* ) l_output=0 ;;
                esac
                break
            else
                continue
            fi
        done
    fi

    echo "${l_output}"
}

fn_OpenSSHDirectiveSetting(){
    local l_item="${1:-}"
    local l_val="${2:-}"
    # sshd_config directive lists
    local l_list="${3:-}"
    local l_path=${4:-'/etc/ssh/sshd_config'}

    local l_action=${l_action:-0}

    if [[ -n "${l_item}" && -n "${l_val}" && -s "${l_path}" ]]; then
        if [[ -s "${l_list}" ]]; then
            [[ -n $(sed -r -n '/^'"${l_item}"'$/p' "${l_list}" 2> /dev/null) ]] && l_action=1
        else
            l_action=1
        fi
    fi

    if [[ "${l_action}" -eq 1 ]]; then
        local l_record_origin=${l_record_origin:-}
        local l_record_origin_comment=${l_record_origin_comment:-}
        # if result has more then one line, use double quote "" wrap it
        # - whole line start with keyword, format 'PermitEmptyPasswords no'

        l_record_origin=$(sed -r -n '0,/^'"${l_item}"'[[:space:]]+/{/^'"${l_item}"'[[:space:]]+/{s@[[:space:]]*$@@;p}}' "${l_path}" 2> /dev/null)
        # l_record_origin=$(sed -r -n '/^'"${l_item}"'[[:space:]]+/{s@[[:space:]]*$@@g;p}' "${l_path}" 2> /dev/null)

        if [[ -z "${l_record_origin}" ]]; then
            # - whole line include keyword start with "#", format '#PermitEmptyPasswords no'
            l_record_origin_comment=$(sed -r -n '/^#[[:space:]]*'"${l_item}"'[[:space:]]+/{s@[[:space:]]*$@@;p}' "${l_path}" 2> /dev/null)
            # Omit '# Ciphers and keying'
            [[ "${l_item}" == 'Ciphers' && "${l_record_origin_comment}" =~ 'Ciphers and' ]] && l_record_origin_comment=''

            if [[ -z "${l_record_origin_comment}" ]]; then
                # append at the end of file
                sed -i -r '$a '"${l_item} ${l_val}"'' "${l_path}" 2> /dev/null
            else
                # change sed delimiter
                if [[ "${l_val}" =~ @ ]]; then
                    sed -i -r '0,/^#[[:space:]]*'"${l_item}"'[[:space:]]+/{/^#[[:space:]]*'"${l_item}"'[[:space:]]+/{s/.*/'"${l_item} ${l_val}"'/}}' "${l_path}" 2> /dev/null
                else
                    sed -i -r '0,/^#[[:space:]]*'"${l_item}"'[[:space:]]+/{/^#[[:space:]]*'"${l_item}"'[[:space:]]+/{s@.*@'"${l_item} ${l_val}"'@}}' "${l_path}" 2> /dev/null
                fi

                # append at the end of the directive which is commented by #
                # sed -i -r '/^#[[:space:]]*'"${l_item}"'[[:space:]]+/a '"${l_item} ${l_val}"'' "${l_path}" 2> /dev/null
                # remove commented line
                # sed -i -r '/^#[[:space:]]*'"${l_item}"'[[:space:]]+/d' "${l_path}" 2> /dev/null
            fi
        else
            if [[ "${l_record_origin##* }" != "${l_val}" ]]; then
                # change sed delimiter
                if [[ "${l_val}" =~ @ ]]; then
                    sed -i -r '/^'"${l_item}"'[[:space:]]+/{s/.*/'"${l_item} ${l_val}"'/;}' "${l_path}" 2> /dev/null
                else
                    sed -i -r '/^'"${l_item}"'[[:space:]]+/{s@.*@'"${l_item} ${l_val}"'@;}' "${l_path}" 2> /dev/null
                fi
            fi
        fi
    fi
}


#########  1. Base Functions  #########

fn_SSHD_Para_Lists_Operation(){
    local l_key="${1:-}"
    local l_val="${2:-}"
    local l_path="${3:-}"

    [[ -n "${l_key}" && -n "${l_val}" && -f "${l_path}" ]] && sed -r -i '/'"${l_key}"'\|/{s@^([^\|]+\|).*@\1'"${l_val}"'@g}' "${l_path}" 2> /dev/null   
}

fn_OpenSSHAlgorithmsComparasion(){
    local l_type="${1:-}"
    local l_list_origin="${2:-}"
    local l_command=''
    local l_list_new=''

    # from 'man sshd_config'
    case "${l_type}" in
        Ciphers) l_command='cipher' ;;
        KexAlgorithms) l_command='kex' ;;
        MACs) l_command='mac' ;;
        PubkeyAcceptedKeyTypes|HostKeyAlgorithms|HostbasedAcceptedKeyTypes) l_command='key' ;;
    esac
    
    if [[ -n "${l_type}" && -n "${l_list_origin}" ]]; then
        IFS=',' read -r -a val_arr <<< "${l_list_origin}"
        IFS=' ' read -r -a key_arr <<< $(ssh -Q "${l_command}" | xargs)

        local l_list_new=''

        for i in "${!val_arr[@]}"; do
            for j in "${@key_arr[@]}"; do
                if [[ "${val_arr[i]}" == "${key_arr[j]}" ]]; then
                    l_list_new="${l_list_new},${val_arr[i]}"
                    unset key_arr[j]
                    break
                fi
            done
        done

        l_list_new="${l_list_new#,}"
    fi
    
    echo "${l_list_new}"
}


#########  1. OpenSSH Configuration  #########
fn_SSH_Client_Configuration(){
    # private key mode 600, public key mode 640
    # (umask 077; ssh_config_path="${login_user_home}/.ssh"; [[ -d "${ssh_config_path}" ]] || mkdir -p "${ssh_config_path}"; mkdir -p "${ssh_config_path}/sockets"; chown -R "${login_user}" "${ssh_config_path}")
    cp "${ssh_config_sample}" "${ssh_config_path}/config"
}


fn_SSH_Server_Configuration(){
    local l_sshd_conf=${1:-'/etc/ssh/sshd_config'}

    # sshd_config supported directive lists
    local l_sshd_directive_list
    l_sshd_directive_list=$(mktemp -t XXXXXXXX)
    man sshd_config 2> /dev/null | sed -r -n 's@^H\w@@g;p' | sed -r -n '/^[[:space:]]{1,8}[[:upper:]]{1}[[:alpha:]]+[[:space:]]*/{s@^[[:space:]]*([[:alpha:]]+).*@\1@g;p}' > "${l_sshd_directive_list}"
    [[ -z "${l_sshd_directive_list}" ]] && : > "${l_sshd_directive_list}"

    local l_ssh_banner='/etc/ssh/sshd_banner'
    [[ -s "${l_ssh_banner}" ]] || echo -e "#################################################################\n##                 Welcome to GNU/Linux World                  ##\n##         All connections are monitored and recorded          ##\n##   Unauthorized user is prohibited, Disconnect immediately   ##\n#################################################################\n" > "${l_ssh_banner}"
    chmod 644 "${l_ssh_banner}"
    echo "Banner|${l_ssh_banner}" >> "${sshd_para_list_path}"

    # Log sftp level file access (read/write/etc.) that would not be easily logged otherwise.
    # https://unix.stackexchange.com/questions/61580/sftp-gives-an-error-received-message-too-long-and-what-is-the-reason#answer-327284
    # https://serverfault.com/questions/660160/openssh-difference-between-internal-sftp-and-sftp-server
    # Subsystem sftp /usr/lib/openssh/sftp-server
    if [[ -n $(sed -n -r '/^#?Subsystem[[:space:]]+sftp[[:space:]]+/p' "${l_sshd_conf}") ]]; then
        sed -r -i '/^#?Subsystem[[:space:]]+sftp[[:space:]]+/{s@^#?(Subsystem[[:space:]]+sftp)[[:space:]]+.*$@\1 internal-sftp -l INFO@g;}' "${l_sshd_conf}"
    else
        sed -i -r '$a Subsystem sftp internal-sftp -l INFO' "${l_sshd_conf}"
    fi

    # Supported HostKey algorithms by order of preference
    sed -i -r 's@^#?(HostKey /etc/ssh/ssh_host_rsa_key)$@\1@' "${l_sshd_conf}"
    # sed -i -r 's@^#?(HostKey /etc/ssh/ssh_host_ecdsa_key)$@\1@' "${l_sshd_conf}"

    if [[ $(fn_VersionNumberComparasion "${ssh_intalled_version}" '6.7' '<') -eq 1 ]]; then
        fn_SSHD_Para_Lists_Operation 'UsePrivilegeSeparation' 'yes' "${sshd_para_list_path}"
        fn_SSHD_Para_Lists_Operation 'KexAlgorithms' 'diffie-hellman-group-exchange-sha256' "${sshd_para_list_path}"
        fn_SSHD_Para_Lists_Operation 'Ciphers' 'aes256-ctr,aes192-ctr,aes128-ctr' "${sshd_para_list_path}"
        fn_SSHD_Para_Lists_Operation 'MACs' 'hmac-sha2-512,hmac-sha2-256' "${sshd_para_list_path}"
    else
        # version >= 6.7
        sed -i -r 's@^#?(HostKey /etc/ssh/ssh_host_ed25519_key)$@\1@' "${l_sshd_conf}"
        fn_SSHD_Para_Lists_Operation 'UsePrivilegeSeparation' 'sandbox' "${sshd_para_list_path}"
        # fn_SSHD_Para_Lists_Operation 'AuthenticationMethods' 'publickey' "${sshd_para_list_path}"
    fi
    

    sed -r -n '/^[^#]+.*\|/{p}' "${sshd_para_list_path}" 2> /dev/null | while IFS="|" read -r para_key para_val; do

        case "${para_key}" in
            'AllowGroups')
                local l_group_path='/etc/gshadow'
                [[ -f '/etc/group' ]] && l_group_path='/etc/group' # zypper
                [[ -z $(sed -n '/^'"${para_val}"':/{s@^([^:]+).*$@\1@g;p}' "${l_group_path}" 2> /dev/null) ]] && groupadd "${para_val}" &>/dev/null
                gpasswd -a "${login_user}" "${para_val}" &>/dev/null
                # Disable root Login via SSH 
                gpasswd -d root "${para_val}" &>/dev/null
            ;;
            'Ciphers|MACs|KexAlgorithms|PubkeyAcceptedKeyTypes|HostKeyAlgorithms|HostbasedAcceptedKeyTypes')
                # filter unsupported algorithms
                para_val=$(fn_OpenSSHAlgorithmsComparasion "${para_key}" "${para_val}")
            ;;
        esac
       
        fn_OpenSSHDirectiveSetting "${para_key}" "${para_val}" "${l_sshd_directive_list}" "${l_sshd_conf}"
    done

    [[ -f "${l_sshd_directive_list}" ]] && rm -f "${l_sshd_directive_list}"
}




#########  1. Executation  #########

fn_Preparation
fn_SSH_Server_Configuration /tmp/sshd_config