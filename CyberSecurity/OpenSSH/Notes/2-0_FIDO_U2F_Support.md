# OpenSSH FIDO/U2F Support

From OpenSSH [version 8.2](https://www.openssh.com/releasenotes.html#8.2) (Feb 14, 2020), [OpenSSH][openssh] supports FIDO /U2F hardware authenticators.

U2F/FIDO are open standards for inexpensive two-factor authentication hardware that are widely used for website authentication. In OpenSSH FIDO devices are supported by new public key types `ecdsa-sk` and `ed25519-sk`, along with corresponding certificate types.

Generating a FIDO key requires the token be attached, and will usually require the user tap the token to confirm the operation.


## TOC

1. [Prerequisite](#prerequisite)  
1.1 [libfido2](#libfido2)  
2. [Generation](#generation)  
2.1 [Operating Commands](#operating-commands)  
3. [Operating Procedures](#operating-procedures)  
4. [Error Occuring](#error-occuring)  
4.1 [Key enrollment failed](#key-enrollment-failed)  
5. [Bibliography](#bibliography)  


## Prerequisite

### libfido2

OpenSSH includes a middleware ("SecurityKeyProvider=internal") with support for USB tokens. It is automatically enabled in OpenBSD and may be enabled in portable OpenSSH via the configure flag `--with-security-key-builtin`. If the internal middleware is enabled then it is automatically used by default. This internal middleware requires that libfido2 (https://github.com/Yubico/libfido2)and its dependencies be installed. Otherwise it will prompts error info

>/usr/lib/ssh/ssh-sk-helper: error while loading shared libraries: libfido2.so.1: cannot open shared object file: No such file or directory


```bash
sudo pacman -S --noconfirm libfido2
```


## Generation

Note: FIDO/U2F tokens are required to implement the ECDSA-P256 `ecdsa-sk` key type, but hardware support for Ed25519 `ed25519-sk` is less common. Similarly, not all hardware tokens support some of the optional features such as resident keys.

### Operating Commands

```bash
ssh-keygen -t ecdsa-sk -f ~/.ssh/id_ecdsa_sk

# YubiKey needs firmware 5.2.3 or higher to support FIDO2, otherwise it'll prompts error 'Key enrollment failed: requested feature not supported'.
# Check Yubikey firmware version via command 'ykman info'
ssh-keygen -t ed25519-sk -f ~/.ssh/id_ed25519_sk
```


## Operating Procedures

```bash
#ssh-keygen -t ecdsa-sk -f ~/.ssh/id_ecdsa_sk -C 'FIDO/U2F Testing'

Generating public/private ecdsa-sk key pair.
You may need to touch your authenticator to authorize key generation.
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /home/maxdsre/.ssh/id_ecdsa_sk
Your public key has been saved in /home/maxdsre/.ssh/id_ecdsa_sk.pub
The key fingerprint is:
SHA256:WQuazp20B8xXbyolFf5Ik1oc6XdYHMor5WIDgnDSjSs FIDO/U2F Testing
The key's randomart image is:
+-[ECDSA-SK 256]--+
|   o..o     o. o.|
|    +o..   oo+. o|
|     .......X+ o |
|   E . =.+.B+=+ .|
|    . o S ==o++. |
|     o o =.o+o   |
|      o + o .    |
|         . .     |
|                 |
+----[SHA256]-----+
```


```bash
# cat id_ecdsa_sk.pub
sk-ecdsa-sha2-nistp256@openssh.com AAAAInNrLWVjZHNhLXNoYTItbmlzdHAyNTZAb3BlbnNzaC5jb20AAAAIbmlzdHAyNTYAAABBBNRXQYnVqIiXHMk7+1BUD90s2XywYNlX4gqfij16aRIbgA1csbizQocY8YS1xDszpsEvt1qpckuYseK6MvXEp4kAAAAEc3NoOg== FIDO/U2F Testing

# cat id_ecdsa_sk
-----BEGIN OPENSSH PRIVATE KEY-----
b3BlbnNzaC1rZXktdjEAAAAACmFlczI1Ni1jdHIAAAAGYmNyeXB0AAAAGAAAABDAMBNWOD
xNrvvM82w5W5DOAAAAEAAAAAEAAAB/AAAAInNrLWVjZHNhLXNoYTItbmlzdHAyNTZAb3Bl
bnNzaC5jb20AAAAIbmlzdHAyNTYAAABBBNRXQYnVqIiXHMk7+1BUD90s2XywYNlX4gqfij
16aRIbgA1csbizQocY8YS1xDszpsEvt1qpckuYseK6MvXEp4kAAAAEc3NoOgAAAPBCHK01
M5Nd2F0676gzReRLKlKKL/d61G617RCI/wCrVMkL6MyxjjVSXbA8Pn98Ec4CfcepOA4PhI
wrG0Kvevra0n5glT3ip2ezHU1ycJmLqDUFNzMrfY6L1avAZS2yBskvEaroQUKD65LBx5Ff
vA6+LP6odJvvGGu5T/ZDyWzl5sB4kwZnkJbVMpSCKmlxmBP/kMOwrq3CIjhZY6pkDqxZBf
QK6DHqcS6jzLOWq1uMWrjtyDbiEaJqXQmy0VwkGU3NxT5rHbEXXIzw4INc9xhX0kmzCAfK
Q7Bd2DzhbGdjI7nRBJg5OxfPBnH+zcsoEQg=
-----END OPENSSH PRIVATE KEY-----
```


## Error Occuring

### Key enrollment failed

```bash
ssh-keygen -vv -t ed25519-sk
```

Error info

```txt
Generating public/private ed25519-sk key pair.
You may need to touch your authenticator to authorize key generation.
debug1: start_helper: starting /usr/lib/ssh/ssh-sk-helper
debug1: sshsk_enroll: provider "internal", device "(null)", application "ssh:", userid "(null)", flags 0x01, challenge len 0
debug1: sshsk_enroll: using random challenge
debug1: ssh_sk_enroll: using device /dev/hidraw3
debug1: ssh_sk_enroll: fido_dev_make_cred: FIDO_ERR_UNSUPPORTED_ALGORITHM
debug1: sshsk_enroll: provider "internal" returned failure -2
debug1: ssh-sk-helper: Enrollment failed: requested feature not supported
debug1: ssh-sk-helper: reply len 8
debug1: client_converse: helper returned error -59
Key enrollment failed: requested feature not supported
```

[YubiKey][yubico] needs [firmware 5.2.3](https://www.yubico.com/blog/whats-new-in-yubikey-firmware-5-2-3/) to support `ed25519-sk` key type.

But my yubikey version is `5.1.2`.

```bash
# ykman info | sed -r -n '/firmware/Ip'
Firmware version: 5.1.2

# lsusb -v 2>/dev/null | sed -r -n '/yubikey/I,/^$/{/bcdDevice/I{s@^[^[:digit:]]*@@g;p}}'
5.12
```

And official document [Upgrading YubiKey Firmware](https://support.yubico.com/support/solutions/articles/15000006434-upgrading-yubikey-firmware) says:

>It is currently not possible to upgrade YubiKey firmware. To prevent attacks on the YubiKey which might compromise its security, the YubiKey does not permit its firmware to be accessed or altered.


## Bibliography

* [Securing SSH with the YubiKey](https://developers.yubico.com/SSH/)
* [OpenSSH eases admin hassles with FIDO U2F token supportl](https://nakedsecurity.sophos.com/2020/02/19/openssh-eases-admin-hassles-with-fido-u2f-token-support/)
* [How to configure SSH with YubiKey Security Keys U2F Authentication on Ubuntu 18.04](https://cryptsus.com/blog/how-to-configure-openssh-with-yubikey-security-keys-u2f-otp-authentication-ed25519-sk-ecdsa-sk-on-ubuntu-18.04.html)


[openssh]:https://www.openssh.com/
[yubico]:https://www.yubico.com/ "Yubico | YubiKey Strong Two Factor Authentication for Business and Individual Use."

<!-- End -->