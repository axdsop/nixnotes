# Getting OpenSSH Release Info

## TOC

1. [Version Check](#version-check)  
1.1 [Local version](#local-version)  
1.2 [Online version](#online-version)  
2. [PGP Signature Verifying](#pgp-signature-verifying)  


## Version Check

* [OpenSSH for OpenBSD](https://www.openssh.com/openbsd.html)
* [OpenSSH Mirrors](https://www.openssh.com/ftp.html)

### Local version

Local installed version

```sh
ssh -V
# OpenSSH_8.6p1, OpenSSL 1.1.1k  25 Mar 2021

ssh -V 2>&1 | sed -r -n 's@.*_([[:digit:].]{3}).*@\1@p'
# 8.6
```

### Online version

Online release version

version|release date|release note|url|sha256|sig url
---|---|----|---|----|---
OpenSSH 8.6|Apr 19, 2021|https://www.openssh.com/txt/release-8.6|https://cdn.openbsd.org/pub/OpenBSD/OpenSSH/openssh-8.6.tar.gz|ihmgdEgKfCBRpC0qzdQRwYownrpBf+rsihvk4Rmim8M=|https://cdn.openbsd.org/pub/OpenBSD/OpenSSH/openssh-8.6.tar.gz.asc

Release note says:

>The PGP key used to sign the releases is available from the mirror sites: https://cdn.openbsd.org/pub/OpenBSD/OpenSSH/RELEASE_KEY.asc

You can also use the following Shell script to extract the latest online release version

<details><summary>Click to expand code snippet</summary>

```sh
download_method='wget -qO-'
[[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && download_method='curl -fsSL'

official_site='https://www.openssh.com/'
# mirrors_site="${official_site}ftp.html"

# Just release version and release date
# $download_method "${official_site}" | sed -r -n '/released/{s@.*href="([^"]+)"[^>]*>([^<]+).*released[[:space:]]*([^<]+).*$@\2|\3@g;p}'
# OpenSSH 8.6|April 19, 2021

# Include package info
${download_method} "${official_site}" | sed -r -n '/released/{s@.*href="([^"]+)"[^>]*>([^<]+).*released[[:space:]]*([^<]+).*$@\2|\3|'"${official_site}"'\1@g;p}' | while IFS="|" read -r r_version r_date r_note; do
    # OpenSSH 8.6|April 19, 2021|https://www.openssh.com/txt/release-8.6

    sha_dgst_info=$(${download_method} "${r_note}" | sed -r -n '/SHA.*-'"${r_version##* }"'\./{/sha1/Id;s@^.*?(SHA[^[:space:]]+)[^(]*\(([^)]*)[^=]*=[[:space:]]*(.*)$@\2|\1|\3@g;p}')
    # openssh-8.6.tar.gz|SHA256|ihmgdEgKfCBRpC0qzdQRwYownrpBf+rsihvk4Rmim8M=

    # mirror site  https://www.openssh.com/ftp.html
    # default  https://ftp.openbsd.org/pub/OpenBSD/OpenSSH/
    # Fastly (CDN)  https://cdn.openbsd.org/pub/OpenBSD/OpenSSH/
    p_url="https://cdn.openbsd.org/pub/OpenBSD/OpenSSH/$(cut -d\| -f1 <<< "${sha_dgst_info}")"

    # The PGP key used to sign the releases is available as RELEASE_KEY.asc from the mirror sites.
    p_sig_url="${p_url}.asc"

    r_date=$(echo "${r_date}" | date -f - +"%b %d, %Y" -u)

    # version|release date|release note|url|sha256|sig url
    echo "${r_version}|${r_date}|${r_note}|${p_url}|${sha_dgst_info##*|}|${p_sig_url}"
done
```

</details>


## PGP Signature Verifying

Using personal custom function [GPGSignatureVerification](/CyberSecurity/GnuPG/Notes/2-1_UsageExample.md#package-signature-verifying) to verify PGP signature.

Usage

```bash
gz_image_name='openssh-8.6.tar.gz'

# fn_GPGSignatureVerification 'ISOPATH' 'SIGPATH' 'GPGPATH'
fn_GPGSignatureVerification "${gz_image_name}"{,.asc} 'https://cdn.openbsd.org/pub/OpenBSD/OpenSSH/RELEASE_KEY.asc'
```

Output

```txt
Importing PGP key
gpg: keybox '/tmp/Lm64SEeG/gpg_import.gpg' created
gpg: key 0xCE8ECB0386FF9C48: 2 signatures not checked due to missing keys
gpg: key 0xCE8ECB0386FF9C48: public key "Damien Miller (Personal Key) <djm@mindrot.org>" imported
gpg: key 0xA2B989F511B5748F: 3 signatures not checked due to missing keys
gpg: key 0xA2B989F511B5748F: public key "Damien Miller <dmiller@vitnet.com.sg>" imported
gpg: key 0xA819A2D8691EF8DA: 1 signature not checked due to a missing key
gpg: key 0xA819A2D8691EF8DA: public key "Damien Miller (Personal Key) <djm@mindrot.org>" imported
gpg: key 0xD3E5F56B6D920D30: 14 signatures not checked due to missing keys
gpg: key 0xD3E5F56B6D920D30: public key "Damien Miller <djm@mindrot.org>" imported
gpg: key 0x2A3F414E736060BA: public key "Damien Miller <djm@mindrot.org>" imported
gpg: Total number processed: 5
gpg:               imported: 5
gpg: no ultimately trusted keys found

Verifying signature
gpg: Signature made Fri 16 Apr 2021 12:06:24 AM EDT
gpg:                using RSA key 7168B983815A5EEF59A4ADFD2A3F414E736060BA
gpg: Good signature from "Damien Miller <djm@mindrot.org>" [unknown]
gpg: WARNING: This key is not certified with a trusted signature!
gpg:          There is no indication that the signature belongs to the owner.
Primary key fingerprint: 7168 B983 815A 5EEF 59A4  ADFD 2A3F 414E 7360 60BA

Temporary directory /tmp/Lm64SEeG has been removed.
```

<details><summary>Click to expand version 8.3</summary>

Release note ([8.3](https://www.openssh.com/txt/release-8.3))says:

>The PGP key used to sign the releases is available as RELEASE_KEY.asc from the mirror sites.

Usage

```bash
gz_image_name='openssh-8.3.tar.gz'

# fn_GPGSignatureVerification 'ISOPATH' 'SIGPATH' 'GPGPATH'
fn_GPGSignatureVerification "${gz_image_name}"{,.asc}
```

Output

```txt
Importing PGP key
gpg: keybox '/tmp/21Y6IUCt/gpg_import.gpg' created
gpg: assuming signed data in 'openssh-8.3.tar.gz'
gpg: Signature made Tue 26 May 2020 11:09:37 PM EDT
gpg:                using RSA key 59C2118ED206D927E667EBE3D3E5F56B6D920D30
gpg: key 0xD3E5F56B6D920D30: 19 signatures not checked due to missing keys
gpg: key 0xD3E5F56B6D920D30: public key "Damien Miller <djm@mindrot.org>" imported
gpg: public key of ultimately trusted key 0xDAA7DE7E855BB3F9 not found
gpg: marginals needed: 3  completes needed: 1  trust model: pgp
gpg: depth: 0  valid:   1  signed:   0  trust: 0-, 0q, 0n, 0m, 0f, 1u
gpg: Total number processed: 1
gpg:               imported: 1
gpg: Good signature from "Damien Miller <djm@mindrot.org>" [unknown]
gpg: WARNING: This key is not certified with a trusted signature!
gpg:          There is no indication that the signature belongs to the owner.
Primary key fingerprint: 59C2 118E D206 D927 E667  EBE3 D3E5 F56B 6D92 0D30

Verifying signature
gpg: Signature made Tue 26 May 2020 11:09:37 PM EDT
gpg:                using RSA key 59C2118ED206D927E667EBE3D3E5F56B6D920D30
gpg: Good signature from "Damien Miller <djm@mindrot.org>" [unknown]
gpg: WARNING: This key is not certified with a trusted signature!
gpg:          There is no indication that the signature belongs to the owner.
Primary key fingerprint: 59C2 118E D206 D927 E667  EBE3 D3E5 F56B 6D92 0D30

Temporary directory /tmp/21Y6IUCt has been removed.
```

</details>

## Change Log

* Apr 17, 2020 Fri 08:12 ET
  * first draft
* Apr 26, 2021 Mon 10:45 ET
  * update version to *8.6*


[openssh]:https://www.openssh.com/
[openbsd]:https://www.openbsd.org/

<!-- End -->