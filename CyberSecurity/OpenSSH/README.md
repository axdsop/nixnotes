# OpenSSH Simple Tutorial


[OpenSSH][openssh] is the premier connectivity tool for remote login with the SSH protocol. It encrypts all traffic to eliminate eavesdropping, connection hijacking, and other attacks. In addition, OpenSSH provides a large suite of secure tunneling capabilities, several authentication methods, and sophisticated configuration options.

[OpenSSH][openssh] is developed by a few developers of the [OpenBSD Project][openbsd] and made available under a BSD-style license.


## Operation Note

* [Getting Latest OpenSSH](./Notes/1-0_GettingLatestRelease.md)
* [FIDO/U2F Support](./Notes/2-0_FIDO_U2F_Support.md) (From OpenSSH [version 8.2](https://www.openssh.com/releasenotes.html#8.2) *Feb 14, 2020*)


<!-- Client|Server|Command|Explanation
---|---|---|---
Ciphers|Ciphers|ssh -Q cipher|list of available ciphers
HostKeyAlgorithms| |ssh -Q HostKeyAlgorithms|list of available key types
KexAlgorithms|KexAlgorithms|ssh -Q kex|list of available key exchange algorithms
MACs|MACs|ssh -Q mac|list of available MAC algorithms
PubkeyAcceptedKeyTypes| |ssh -Q PubkeyAcceptedKeyTypes|list of available key types
||HostbasedAcceptedKeyTypes<br>HostKeyAlgorithms<br>PubkeyAcceptedKeyTypes|ssh -Q key|list of available key types -->


## Bibliography

* [Understanding ssh-agent and ssh-add | Jon Cairns](http://blog.joncairns.com/2013/12/understanding-ssh-agent-and-ssh-add/)
* [The pitfalls of using ssh-agent, or how to use an agent safely](http://rabexc.org/posts/pitfalls-of-ssh-agents)


[openssh]:https://www.openssh.com/
[openbsd]:https://www.openbsd.org/

<!-- End -->