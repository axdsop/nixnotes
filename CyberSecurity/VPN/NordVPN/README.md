# NordVPN Usage

[NordVPN][nordvpn] officially provides [.deb](https://repo.nordvpn.com/deb/) and [.rpm](https://repo.nordvpn.com/yum/) package for GNU/Linux ([RHEL/CentOS](https://support.nordvpn.com/Connectivity/Linux/1325529362/Installing-and-using-NordVPN-on-RHEL-and-CentOS-Linux.htm), [Fedora](https://support.nordvpn.com/Connectivity/Linux/1325529112/Installing-and-using-NordVPN-on-Fedora-and-QubesOS-Linux.htm), [openSUSE](https://support.nordvpn.com/Connectivity/Linux/1325530072/Installing-and-using-NordVPN-on-openSUSE-Linux.htm), [Debian/Ubuntu/Linux Mint](https://support.nordvpn.com/Connectivity/Linux/1325531132/Installing-and-using-NordVPN-on-Debian-Ubuntu-and-Linux-Mint.htm)) except [Arch Linux][archlinux].


## Usage

* [NordVPN On Arch Linux](./ForArchLinux.md)


## Change Log

* Apr 16, 2020 Thu 19:20 ET
  * first draft


[archlinux]:https://www.archlinux.org/
[nordvpn]:https://nordvpn.com/
[nordlynx]:https://nordvpn.com/blog/nordlynx-protocol-wireguard/ "NordLynx – the new solution for a fast and secure VPN connection"
[aur]:https://wiki.archlinux.org/index.php/Arch_User_Repository
[nordvpn_bin_aur]:https://aur.archlinux.org/packages/nordvpn-bin/
[openvpn]:https://wiki.archlinux.org/index.php/OpenVPN
[wireguard]:https://wiki.archlinux.org/index.php/WireGuard


<!-- End -->