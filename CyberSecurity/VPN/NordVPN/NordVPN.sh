#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails

# Official Site
# - https://nordvpn.com/
# - https://wiki.archlinux.org/index.php/NordVPN
# - https://aur.archlinux.org/packages/nordvpn-bin/


# Target: Setting Up NordVPN CLI Client On Arch Linux
# Documents: https://gitlab.com/axdsop/nixnotes/blob/master/CyberSecurity/VPN/NordVPN/README.md
# Developer: MaxdSre

# Change Log:
# - Mar 29, 2023 Wed 14:25 ET - version selection menu add installed mark
# - Aug 02, 2021 Mon 15:26 ET - version selection menu add date
# - Apr 06, 2021 Tue 10:26 ET - fix release Package info extraction issue
# - Jan 26, 2020 Tue 19:05 ET - solve release version lists order issue (e.g. 3.8.1, 3.8.10)
# - Jun 21, 2020 Sun 14:57 ET - change exit statement via function 'fn_ExitStatement', code optimization
# - Apr 20, 2020 Mon 17:05 ET - disable killswitch at version 3.7.1, from this version Nord VPN doesn't provides release version change log any more
# - Dec 05, 2019 Thu 08:55 ET - disable killswitch from version 3.5.0-1
# - Oct 31, 2019 Thu 12:58 ET - seperate local version detection as a custom function, from version 3.4.0-1 user doesn't need to install WireGuard� manually.
# - Oct 21, 2019 Mon 09:02 ET - fix login credential configuration via 'nordvpn account' output
# - Oct 12, 2019 Sat 10:23 ET - add execute privilege check
# - Aug 28, 2019 Wed 21:12 ET - change file PKGBUILD checksum values operating method to while loop via architecture_relationship list
# - Aug 22, 2019 Thu 10:22 ET - update functions name format, resolve 'Stdin/Stdout should be terminal' via '/dev/tty', add login retry counts
# - Aug 12, 2019 Mon 09:24 ET - remove wireguard utilities via 'pacman -Qs'
# - Aug 07, 2019 Wed 07:17 ET - add changelog info while comparing online and local version
# - Aug 03, 2019 Sat 16:03 ET - add technology NordLynx instead of OpenVPN, support specify login credential file
# - Jul 13, 2019 Sat 12:54 ~ 15:15 ET - first draft


# Whoops! Permission denied accessing /run/nordvpn/nordvpnd.sock
# Run 'usermod -aG nordvpn $USER' to fix this issue and log out of OS afterwards for this to take an effect.

# From version 3.16.0 (2023-03-10), add utility 'nordfileshared'.


#########  0-1. Variables Setting  #########
# - Color
readonly c_bold="$(tput bold 2> /dev/null)"
# https://misc.flogisoft.com/bash/tip_colors_and_formatting
# black 0, red 1, green 2, yellow 3, blue 4, magenta 5, cyan 6, gray 7
# \e[31m need to use `echo -e`
readonly c_red="$(tput setaf 1 2> /dev/null)"     # c_red='\e[31;1m'
readonly c_green="$(tput setaf 2 2> /dev/null)"    # c_green='\e[32m'
readonly c_yellow="$(tput setaf 3 2> /dev/null)"    # c_yellow='\e[33m'
readonly c_blue="$(tput setaf 4 2> /dev/null)"    # c_blue='\e[34m'
readonly c_magenta="$(tput setaf 5 2> /dev/null)"    # c_magenta='\e[35m'
readonly c_cyan="$(tput setaf 6 2> /dev/null)"    # c_cyan='\e[36m'
readonly c_gray="$(tput setaf 7 2> /dev/null)"    # c_gray='\e[37m'
readonly c_normal="$(tput sgr0 2> /dev/null)"     # c_normal='\e[0m'

version_check=${version_check:-0}
version_specify=${version_specify:-0}
login_credential_path=${login_credential_path:-}
technology_specify=${technology_specify:-0}
download_method=${download_method:-} # curl -fsL / wget -qO-
is_uninstall=${is_uninstall:-0}
sleep_time=${sleep_time:-3} # seconds

readonly packages_release_page='https://repo.nordvpn.com/deb/nordvpn/debian/pool/main/'
readonly version_num_limit=20


#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | bash -s -- [options] ...

Installing / Updating NordVPN® CLI Client On Arch Linux!

Documents: https://gitlab.com/axdsop/nixnotes/blob/master/CyberSecurity/VPN/NordVPN/README.md

Please not run as root, or it will prompts error info:
Running makepkg as root is not allowed as it can cause permanent, catastrophic damage to your system.

From v3.8.8 (Dec 17, 2020), NordVPN run daemon with user group 'nordvpn'.

[available option]
    -h    --help, show help info
    -c    --check, check current stable release version
    -f login_credential_path    --specify login credential path which contains Email/Username and Password (One line format 'USERNAME PASSWORD' seperated by white space)
    -s    --specify release version from selection menu (just list latest ${version_num_limit} release versions), default is latest version.
    -t    --specify technology NordLynx (from version 3.3.0-0 (Jul 31, 2019)), default is OpenVPN
    -u    --uninstall, uninstall NordVPN installed, can use along with '-t'
\e[0m"
}

while getopts "hcf:stu" option "$@"; do
    case "$option" in
        c ) version_check=1 ;;
        f ) login_credential_path="$OPTARG" ;;
        s ) version_specify=1 ;;
        t ) technology_specify=1 ;;
        u ) is_uninstall=1 ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done


#########  1-2 Preparation Operation  #########
fn_CommandExistIfCheck(){
    # $? -- 0 is find, 1 is not find
    local l_name="${1:-}"
    local l_output=${l_output:-1}
    [[ -n "${l_name}" && -n $(which "${l_name}" 2> /dev/null || command -v "${l_name}" 2> /dev/null) ]] && l_output=0
    return "${l_output}"
}

fn_ExitStatement(){
    local l_str="$*"
    if [[ -n "${l_str}" ]]; then
        echo -e "${l_str}\n"
    fi
    exit 0
}

fn_VersionNumberComparasion(){
    local l_item1="${1:-0}"    # old val
    local l_item2="${2:-0}"    # new val
    local l_operator=${3:-'<'}
    local l_output=${l_output:-0}

    if [[ -n "${l_item1}" && -n "${l_item2}" ]]; then
        local l_arr1=( ${l_item1//./ } )
        local l_arr2=( ${l_item2//./ } )
        local l_arr1_len=${l_arr1_len:-"${#l_arr1[@]}"}
        local l_arr2_len=${l_arr2_len:-"${#l_arr2[@]}"}
        local l_max_len=${l_max_len:-"${l_arr1_len}"}
        [[ "${l_arr1_len}" -lt "${l_arr2_len}" ]] && l_max_len="${l_arr2_len}"

        for (( i = 0; i < "${l_max_len}"; i++ )); do
            [[ "${l_arr1_len}" -lt $(( i+1 )) ]] && l_arr1[$i]=0
            [[ "${l_arr2_len}" -lt $(( i+1 )) ]] && l_arr2[$i]=0

            if [[ "${l_arr1[i]}" -lt "${l_arr2[i]}" ]]; then
                case "${l_operator}" in
                    '>' ) l_output=0 ;;
                    '<'|* ) l_output=1 ;;
                esac
                break
            elif [[ "${l_arr1[i]}" -gt "${l_arr2[i]}" ]]; then
                case "${l_operator}" in
                    '>' ) l_output=1 ;;
                    '<'|* ) l_output=0 ;;
                esac
                break
            else
                continue
            fi
        done
    fi

    echo "${l_output}"
}

fn_LocalVersionDetection(){
    local l_output=''
    fn_CommandExistIfCheck 'nordvpn' && l_output=$(nordvpn --version 2> /dev/null | sed -r -n 's@^[^[:digit:]]+(.*)$@\1@g;p')
    echo "${l_output}"
}


fn_InitializationCheck(){
    if [[ "$UID" -eq 0 ]]; then
        fn_ExitStatement "${c_red}Sorry${c_normal}: please run this script as normal user whthout root privilege."
    fi

    # - Download method
    if fn_CommandExistIfCheck 'curl'; then
        download_method='curl -fsL'
    elif fn_CommandExistIfCheck 'wget'; then
        download_method='wget -qO-'
    else
        fn_ExitStatement "Please install ${c_red}curl${c_normal} or ${c_red}wget${c_normal} firstly."
    fi

    # - System architecture
    # amd64, i386, arm64, armel, armhf
    # x86_64 --> amd64, i686 --> i386, armv7h --> armhf, armv6h --> armel, aarch64 --> arm64

    machine_name=$(uname -m)
    case "${machine_name}" in
        x86_64|amd64|i?86_64) pack_arch='amd64' ;;
        i?86|x86 ) pack_arch='i386' ;;
        aarch64|arm64 ) pack_arch='arm64' ;;
        armv7* ) pack_arch='armhf' ;;
        armv6* ) pack_arch='armel' ;;
        * )
            fn_ExitStatement "Sorry, this script doesn't support you system arch '${machine_name}'."
        ;;
    esac
}

#########  2-0. Uninstalling  #########
fn_Uninstalling(){
    if [[ "${is_uninstall}" -eq 1 ]]; then
        if fn_CommandExistIfCheck 'nordvpn'; then
            echo -e '\nUninstalling'
            sudo systemctl stop nordvpnd
            sleep "${sleep_time}"
            sudo pacman -R --noconfirm nordvpn-bin
            [[ -d /var/lib/nordvpn/ ]] && sudo rm -rf /var/lib/nordvpn/
            [[ -d /var/log/nordvpn/ ]] && sudo rm -rf /var/log/nordvpn/
            sudo rm -rf /run/nordvpn*
            [[ -d ~/.config/nordvpn/ ]] && rm -rf ~/.config/nordvpn/
            [[ -d ~/.cache/yay/nordvpn-bin/ ]] && rm -rf ~/.cache/yay/nordvpn-bin/
            sudo systemctl daemon-reload

            # https://wiki.archlinux.org/index.php/Pacman/Tips_and_tricks#Listing_packages
            # List packages by regex: pacman -Qs regex.
            pacman -Qs wireguard- | sed -r -n '/\//{s@[^\/]+\/([^[:space:]]+).*$@\1@g;p}' | xargs sudo pacman -Rns --noconfirm 2> /dev/null
            # sudo pacman -R --noconfirm wireguard-tools wireguard-arch 2> /dev/null

            # remove unneeded dependencies
            pacman -Qtdq | xargs sudo pacman -Rns --noconfirm 2> /dev/null
        else
            echo "Sorry, no NordVPN installed."
        fi

        exit
    fi
}

#########  2-1. Latest & Local Version Check  #########
fn_VersionSelectionMenu(){
    local l_release_list="${1:-}"
    local l_local_version="${2:-}"
    local IFS_BAK=${IFS_BAK:-"$IFS"}  # Backup IFS
    IFS="|" # Setting temporary IFS

    echo -e "\n${c_bold}${c_blue}Available Release Version List: ${c_normal}"
    PS3="${c_yellow}Choose version number(e.g. 1, 2,...): ${c_normal}"

    # 3.10.0-1 (2021-05-26)
    select item in $(sed -r -n 's@^([^\|]+)\|([^\|]+).*@\1 (\2)@g;p' <<< "${l_release_list}" | sed -r -n '/^'"${l_local_version}"'[[:space:]]+/{s@\)@ i&@g};p' | sort -t. -k1,1rn -k2,2rn -k3,3rn -k4,4rn | head -n "${version_num_limit}" | sed ':a;N;$!ba;s@\n@|@g'); do
        choose_version="${item%% *}"
        [[ -n "${choose_version}" ]] && break
    done < /dev/tty

    IFS=${IFS_BAK}  # Restore IFS
    unset IFS_BAK
    unset PS3
    printf "\nOnline release version you choose is ${c_bold}${c_yellow}%s${c_normal}.\n\n" "${choose_version}"
}

fn_VersionComparasion(){
    # - Local version
    current_local_version=${current_local_version:-}
    current_local_version=$(fn_LocalVersionDetection)
    # current_local_version=$(nordvpn --version 2> /dev/null | sed -r -n 's@^[^[:digit:]]+(.*)$@\1@g;p')
    [[ -n "${current_local_version}" ]] && echo "Local installed version is ${c_yellow}${current_local_version}${c_normal}."

    # - Online release version
    online_release_version=${online_release_version:-}
    release_version_list=${release_version_list:-}

    local l_pack_changelog_info=''
    l_pack_changelog_info=$(${download_method} "${packages_release_page}" 2> /dev/null | sed -r -n '/href=.*/{/'"${pack_arch}"'/!d;/changelog/!d;s@.*href="([^"]+)".*@\1@g;s@^[^_]+_([^_]+).*@\1|&@g;p}' | sort -t. -k1,1rn -k2,2rn -k3,3rn -k4,4rn)
    # 3.10.0-1|nordvpn_3.10.0-1_amd64.changelog
    [[ -z "${l_pack_changelog_info}" ]] && fn_ExitStatement "\n${c_red}Sorry${c_normal}: fail to extract online release info from page ${c_yellow}${packages_release_page}${c_normal}."

    local l_latest_changelog=''
    l_latest_changelog=$(head -n 1 <<< "${l_pack_changelog_info}" | cut -d\| -f2)
    local l_latest_changelog_url=''
    l_latest_changelog_url="${packages_release_page%/}/${l_latest_changelog}"
    local l_latest_changelog_info=''
    l_latest_changelog_info=$(${download_method} "${l_latest_changelog_url}" 2> /dev/null)

    # extract release date from change log
    local l_release_date_info=''
    l_release_date_info=$(sed -r -n '/^(nordvpn|[[:space:]]*-{2,})/I!d;/^nordvpn/{s@^[^[:digit:]]+([[:digit:]\.]+).*@\1@g};/-{2,}/{s@^[[:space:]]*-{2,}[[:space:]]*@@g};p' <<< "${l_latest_changelog_info}" | sed 'N;s@\n@|@g' | sort -t. -k1,1rn -k2,2rn -k3,3rn -k4,4rn | while IFS="|" read -r v_no v_date; do echo "${v_no}|$(date -d "${v_date}" +'%F' -u)"; done)
    # 3.10.0|2021-05-26
    [[ -z "${l_release_date_info}" ]] && fn_ExitStatement "\n${c_red}Sorry${c_normal}: fail to extract online release date info from changelog."

    release_version_list=$(while IFS="|" read -r v_no v_logname; do release_date=$(sed -r -n '/^'"${v_no%%-*}"'-?/{s@^[^\|]+\|@@g;p;q}' <<< "${l_release_date_info}"); echo "${v_no}|${release_date}|${v_logname}"; done <<< "$l_pack_changelog_info")
    # 3.16.1|2023-03-24|nordvpn_3.16.1_amd64.changelog
    # 3.10.0-1|2021-05-26|nordvpn_3.10.0-1_amd64.changelog

    # manually specify relase varion
    if [[ "${version_specify}" -eq 1 ]]; then
        choose_version=${choose_version:-}
        fn_VersionSelectionMenu "${release_version_list}" "${current_local_version}"
        online_release_version="${choose_version}"
    # latest release version
    else
        online_release_info=$(head -n 1 <<< "${release_version_list}")
        if [[ -n "${online_release_info}" ]]; then
            online_release_version=$(cut -d\| -f1 <<< "${online_release_info}")
            online_release_date=$(cut -d\| -f2 <<< "${online_release_info}")
            echo "Latest online version is ${c_yellow}${online_release_version}${c_normal} (${c_yellow}${online_release_date}${c_normal})."
        fi

        # list changelog (from vesion 3.7.1, Nord VPN stop provides release change log)
        # https://repo.nordvpn.com/deb/nordvpn/debian/pool/main/nordvpn_3.10.0-1_amd64.changelog
        echo -e "\nChange Log (if still provides)\n\n${c_yellow}${l_latest_changelog_url}${c_normal}\n"
        sed -r -n '/^nordvpn.*?'"${online_release_version%-*}"'/,/^[[:space:]]*-{2,}/{p}' <<< "${l_latest_changelog_info}"
        echo ''

        [[ "${version_check}" -eq 1 ]] && exit 0

        if [[ "${current_local_version}" == "${online_release_version}" ]]; then
            [[ "${is_uninstall}" -eq 1 ]] || exit 0
        fi
    fi

    if [[ -n "${current_local_version}" ]]; then
        [[ $(systemctl is-active nordvpnd 2> /dev/null) == 'active' ]] && sudo systemctl stop nordvpnd
    fi
}

#########  2-2. Pakcage Building  #########
fn_PackageBuiliding(){
    # - Extracting available packages info
    packages_detail_page='https://repo.nordvpn.com/deb/nordvpn/debian/dists/stable/main/'
    release_info_per_arch=''
    # release_info_per_arch=$(${download_method} "${packages_detail_page}" | sed -r -n '/href=.*binary/{s@.*href="([^"]+)".*$@\1@g;p}' | while read -r line; do
    #     # binary-amd64/
    #     packages_url="${packages_detail_page}${line}Packages"
    #     ${download_method} "${packages_url}" | sed -r -n '/^Version:[[:space:]]*'"${online_release_version}"'/,/^$/{/^(Version|Depends|Architecture|Filename|SHA256|SHA1|MD5sum):/{/^Depends:.*\|/{s@[[:space:]]*\|[[:space:]]*[^[:space:]]+@,@g;};/^Filename:/{s@.*/(.*)$@'"${packages_release_page}"'\1@g;};s@^[^:]+:[[:space:]]+@@g;p}}' | sed ':a;N;$!ba;s@\n@|@g'
    # done
    # )
    # Version|Depends|Architecture|Filename|SHA256|SHA1|MD5sum
    # 3.3.0-4|procps, iproute2, iptables, xsltproc|amd64|https://repo.nordvpn.com/deb/nordvpn/debian/pool/main/nordvpn_3.3.0-4_amd64.deb|508722c2bb4943dad260ebbecd0af8d4d115330f55dbf2bc32f3b74a47f125a7|0d3859a13f1db79d7fbc93d3617a6f7d4b99321c|1147e808cdcc3ef557dce45d605647f5

    local l_packages_url=''
    l_packages_url=$(${download_method} "${packages_detail_page}" | sed -r -n '/href=.*binary.*?'"${pack_arch}"'/{s@.*href="([^"]+)".*$@'"${packages_detail_page}"'\1Packages@g;p}')

    local l_package_info=''
    l_package_info=$(${download_method} "${l_packages_url}" | sed -r -n '/^Version:[[:space:]]*'"${online_release_version}"'/,/^$/{/^(Version|Depends|Architecture|Filename|SHA256|SHA1|MD5sum):/{/^Depends:.*\|/{s@[[:space:]]*\|[[:space:]]*[^[:space:]]+@,@g;};/^Filename:/{s@^([^:]+:).*/(.*)$@\1 '"${packages_release_page}"'\2@g;};s@^([^:]+):[[:space:]]*(.*)$@\1\|\2@g;p}}')

    # Version|3.9.1-1

    # Version|Depends|Architecture|Filename|SHA256|SHA1|MD5sum
    release_info_per_arch="$(sed -r -n '/^Version\|/{s@^[^\|]+\|@@g;p}'<<< "${l_package_info}")|$(sed -r -n '/^Depends\|/{s@^[^\|]+\|@@g;p}'<<< "${l_package_info}")|$(sed -r -n '/^Architecture\|/{s@^[^\|]+\|@@g;p}'<<< "${l_package_info}")|$(sed -r -n '/^Filename\|/{s@^[^\|]+\|@@g;p}'<<< "${l_package_info}")|$(sed -r -n '/^SHA256\|/{s@^[^\|]+\|@@g;p}'<<< "${l_package_info}")|$(sed -r -n '/^SHA1\|/{s@^[^\|]+\|@@g;p}'<<< "${l_package_info}")|$(sed -r -n '/^MD5sum\|/{s@^[^\|]+\|@@g;p}'<<< "${l_package_info}")"

    # - AUR Snapshot Download
    # https://aur.archlinux.org/packages/nordvpn-bin/
    snapshot_url='https://aur.archlinux.org/cgit/aur.git/snapshot/nordvpn-bin.tar.gz'
    snapshot_save_dir=$(mktemp -d -t XXXXXXXX)
    echo -e "\nExtracting snapshot file into temporary directory ${c_yellow}${snapshot_save_dir}${c_normal}."
    # https://unix.stackexchange.com/questions/85194/how-to-download-an-archive-and-extract-it-without-saving-the-archive-to-disk
    ${download_method} "${snapshot_url}" | tar xz --strip-component=1 -C "${snapshot_save_dir}" 2> /dev/null
    pkgbuild_file_path=${pkgbuild_file_path:-"${snapshot_save_dir}/PKGBUILD"}

    if [[ -f "${pkgbuild_file_path}" ]]; then
        cp -f "${pkgbuild_file_path}" "${pkgbuild_file_path}.old"
    else
        echo "${c_red}Sorry${c_normal}: fail to extract file PKGBUILD from link ${snapshot_url}."
        [[ -d "${snapshot_save_dir}" ]] && rm -rf "${snapshot_save_dir}"
        exit
    fi
    # - PKGBUILD Value Updating
    # pkgver
    # ==> ERROR: pkgver is not allowed to contain colons, forward slashes, hyphens or whitespace.
    sed -r -i '/^pkgver=/{s@([^=]+=).*@\1'"${online_release_version//-/_}"'@g}' "${pkgbuild_file_path}"

    # depends
    depends_list=$(echo "${release_info_per_arch}" | cut -d\| -f2 | sort | uniq | sed -r -n 's@[[:space:]]*xsltproc,@libxslt,@g;s@[[:space:]]*,[[:space:]]*@ @g;s@^@'\''@g;s@$@'\''@g;s@[[:space:]]+@'\'' '\''@g;p')
    # package libxslt provides xsltproc  https://www.archlinux.org/packages/extra/x86_64/libxslt/files/
    # sed -r -i '/^depends=/{s@([^=]+=).*@\1\('"${depends_list}"'\)@g}' "${pkgbuild_file_path}"

    if [[ $(fn_VersionNumberComparasion "${current_local_version%%-*}" '3.4.0' '<') -eq 1 ]]; then
        # convert lowercase to uppercase
        sed -r -i '/^optdepends=/{s@wireguard-module[[:space:]]*:@\U&@g}' "${pkgbuild_file_path}"
    fi

    # Here assume using the same checksum
    # Version|Depends|Architecture|Filename|SHA256|SHA1|MD5sum
    local l_checksum_type=${l_checksum_type:-'sha256'} # sha256, sha1, md5
    local l_field_num=${l_field_num:-5} # identify field in variable release_info_per_arch

    # sed \L == tr "[:upper:]" "[:lower:]"
    l_checksum_type=$(sed -r -n '/sums_[^=]+=/{s@(.*?)sums[^=]+=.*@\L\1@g;p}' "${pkgbuild_file_path}" | sort | uniq)

    case "${l_checksum_type}" in
        sha256 ) l_field_num=5 ;;
        sha1 ) l_field_num=6 ;;
        md5 ) l_field_num=7 ;;
    esac

    # https://wiki.archlinux.org/index.php/PKGBUILD#arch
    # An array of architectures that the PKGBUILD is intended to build and work on. Arch officially supports only x86_64, but other projects may support other architectures. For example, Arch Linux 32 provides support for i686 and Arch Linux ARM provides support for arm (armv5), armv6h (armv6 hardfloat), armv7h (armv7 hardfloat), and aarch64 (armv8 64-bit).

    # architecture relationship table
    # nordvpn - archlinux
    # i386 - i686
    # armhf - armv7h
    # armel - armv6h
    # arm64 - aarch64
    # amd64 - x86_64
    architecture_relationship="i386 i686\narmhf armv7h\narmel armv6h\narm64 aarch64\namd64 x86_64"

    echo -e "${architecture_relationship}" | while IFS=" " read -r nordvpn_arch archlinux_arch; do
        checksum_val=''
        checksum_val=$(echo "${release_info_per_arch}" | sed '/'"${nordvpn_arch}"'/!d' | cut -d\| -f"${l_field_num}")
        [[ -n "${checksum_val}" ]] && sed -r -i '/^'"${l_checksum_type}"'sums_'"${archlinux_arch}"'=/{s@([^=]+=).*@\1\('\'''${checksum_val}''\''\)@g;}' "${pkgbuild_file_path}"
    done

    # Old method
    # _i686 i386
    # local checksum_i386=${checksum_i386:-}
    # checksum_i386=$(echo "${release_info_per_arch}" | sed '/i386/!d' | cut -d\| -f"${l_field_num}")
    # [[ -n "${checksum_i386}" ]] && sed -r -i '/^'"${l_checksum_type}"'sums_i686=/{s@([^=]+=).*@\1\('\'''${checksum_i386}''\''\)@g;}' "${pkgbuild_file_path}"

    cd "${snapshot_save_dir}"

    # .SRCINFO files may be generated using makepkg
    makepkg --printsrcinfo > .SRCINFO
    echo -e "\nMaking Package Process\n"
    # -i, --install    Install package after successful build
    # -s, --syncdeps   Install missing dependencies with pacman
    # -c, --clean      Clean up work files after build
    # --noconfirm      Do not ask for confirmation when resolving dependencies
    makepkg -sic --noconfirm

    echo -e "\nInstalling via ${c_bold}${c_red}pacman -U${c_normal}\n"
    sudo pacman -U --noconfirm nordvpn-bin-*.pkg.tar*

    [[ -f /usr/share/bash-completion/completions/nordvpn ]] && sudo chmod 644 /usr/share/bash-completion/completions/nordvpn

    nordvpn --version

    cd ..
    [[ -d "${snapshot_save_dir}" ]] && rm -rf "${snapshot_save_dir}"
    [[ -d /tmp/makepkg/ ]] && rm -rf /tmp/makepkg/
}

#########  2-3. Service Configuration  #########
fn_LoginCredentialConfiguration(){
    local l_retry_count=${1:-0}

    if [[ "${l_retry_count}" -gt 3 ]]; then
        fn_ExitStatement "\nSorry, retry counts must <=3.\nPlease login via command ${c_bold}${c_yellow}nordvpn account${c_normal} manually."
    fi

    # nordvpn account
    
    # You are not logged in.

    # Account Information:
    # Email Address: xxxx@xxxx.xxx
    # VPN Service: Active (Expires on Xxx xth, xxxx)

    if [[ $(nordvpn account | sed -r -n '/(account|email|vpn|expire)/I{p}' | wc -l) -eq 0 ]]; then
        local l_username=''
        local l_password=''
        local l_is_continue=1

        # format 'USERNAME PASSWORD'
        if [[ -n "${login_credential_path}" && -f "${login_credential_path}" ]]; then
            l_username=$(head -n1 "${login_credential_path}" | cut -d' ' -f1)
            l_password=$(head -n1 "${login_credential_path}" | cut -d' ' -f2)
        fi

        if [[ -n "${l_username}" && -n "${l_password}" ]]; then
            # --username value, -u value  Specify a user account
            # --password value, -p value  Specify the password for the user specified in --username
            nordvpn login --username "${l_username}" --password "${l_password}"

            if [[ $? -eq 0 ]]; then
                l_is_continue=0
            else
                login_credential_path=''
                ((l_retry_count++))
                fn_LoginCredentialConfiguration "${l_retry_count}"
            fi
        fi

        if [[ "${l_is_continue}" -eq 1 ]]; then
            # interactive mode
            nordvpn connect # avoid input username/password multi times
            # nordvpn login

            if [[ $? -ne 0 ]]; then
                ((l_retry_count++))
                fn_LoginCredentialConfiguration "${l_retry_count}"
            fi
            nordvpn disconnect &> /dev/null
        fi < /dev/tty
        # prompt error info: Please enter your login details. Stdin/Stdout should be terminal   solve method is use '/dev/tty'
    fi  < /dev/tty
}

fn_ServiceConfiguration(){
    # NordLynx is introducted from nordvpn (3.3.0) stable, need to install WireGuard manually.
    # From nordvpn (3.4.0) stable, to start using NordLynx, you no longer have to download the WireGuard� package.

    if [[ "${technology_specify}" -eq 1 && $(fn_VersionNumberComparasion "${current_local_version%%-*}" '3.3.0' '<') -eq 0 && $(fn_VersionNumberComparasion "${current_local_version%%-*}" '3.4.0' '<') -eq 1 ]]; then
        echo -e "\nWireGuard Installation"
        # https://wiki.archlinux.org/index.php/WireGuard#Installation
        sudo pacman -Sy --noconfirm wireguard-tools wireguard-arch linux-headers

        # 'nordvpn set technology NordLynx' prompts error info
        # Missing NordLynx kernel module or configuration utility.
    fi

    if fn_CommandExistIfCheck 'systemctl'; then
        echo -e "\nService Status Configuration"
        sudo systemctl daemon-reload
        sudo systemctl restart nordvpnd
        sleep "${sleep_time}"
        sudo systemctl status nordvpnd
    fi

    echo -e "\nLogin Credential"
    fn_LoginCredentialConfiguration

    # account info
    echo ''
    nordvpn account

    if [[ -z "${current_local_version}" ]]; then
        sudo systemctl enable nordvpnd

        echo -e "\nNordVPN Setting"
        # just for OpenVPN
        if [[ "${technology_specify}" -eq 1 ]]; then
            # OpenVPN/NordLynx
            nordvpn set technology NordLynx
        else
            nordvpn set technology OpenVPN
            nordvpn set protocol TCP
            nordvpn set obfuscate disabled
        fi

        nordvpn set notify enabled
        nordvpn set cybersec disabled
        nordvpn set dns 9.9.9.9
        nordvpn set analytics disabled 2> /dev/null  # Help us improve by sending anonymous aggregate data: crash reports, OS version, marketing performance, and feature usage data - nothing that could identify you.

        nordvpn set autoconnect disabled

        # If enable killswitch, the Internet will be blocked unitl killswitch is disabled (version 3.7.1)
        nordvpn set killswitch disabled

        # if [[ $(fn_VersionNumberComparasion "${current_local_version%%-*}" '3.5.0' '<') -eq 1 ]]; then
        #     nordvpn set killswitch enabled
        # else
        #     nordvpn set killswitch disabled
        # fi

        echo ''
        nordvpn settings        
    fi
}



#########  3. Executing Process  #########
fn_InitializationCheck
fn_Uninstalling
fn_VersionComparasion
fn_PackageBuiliding
fn_ServiceConfiguration


# nordvpn account
# We were not able to fetch your account data. Please check your internet connection and try again. If the issue persists, please contact our customer support.
# Too many login attempts. Type 'nordvpn login' to start over.

# Script End