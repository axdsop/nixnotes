# Installing NordVPN CLI Client On Arch Linux

[Arch Linux][archlinux] provides [wiki](https://wiki.archlinux.org/index.php/NordVPN) about [NordVPN][nordvpn]. [Arch Linux][archlinux] user can install it via [AUR][aur] package [nordvpn-bin][nordvpn_bin_aur]. The packages invoked by [nordvpn-bin][nordvpn_bin_aur] are listed in [deb page](https://repo.nordvpn.com/deb/nordvpn/debian/pool/main/).

~~At July 04, 2019, [nordvpn-bin][nordvpn_bin_aur] was flagged as [out of date](https://aur.archlinux.org/login/?referer=https%3A%2F%2Faur.archlinux.org%2Fpkgbase%2Fnordvpn-bin%2Fflag-comment%2F) which version is `3.1.0-6` (11-Jun-2019). The latest online release version is `3.2.0-0` (04-Jul-2019).~~

~~As [nordvpn-bin][nordvpn_bin_aur] is not update by maintainer in time, just like user *erkexzcx*'s [comment](https://aur.archlinux.org/packages/nordvpn-bin/#comment-705532).~~

So I decide to upgrade it manually based on *PKGBUILD* in [nordvpn-bin][nordvpn_bin_aur].


## TOC

1. [Analysis](#analysis)  
1.1 [Login Credential](#login-credential)  
1.2 [Technology](#technology)  
1.3 [Firewall Rule](#firewall-rule)  
1.3.1 [Command whitelist](#command-whitelist)  
1.4 [Router Setting](#router-setting)  
1.4.1 [Under OpenVPN](#under-openvpn)  
1.4.2 [Under nordlynx](#under-nordlynx)  
2. [Shell Script](#shell-script)  
3. [Operation](#operation)  
3.1 [Latest Release Version](#latest-release-version)  
3.2 [Package Details](#package-details)  
3.3 [Architecture Relationship](#architecture-relationship)  
3.4 [AUR Snapshot](#aur-snapshot)  
3.5 [PKGBUILD](#pkgbuild)  
3.5.1 [PKGBUILD File](#pkgbuild-file)  
3.5.2 [PKGBUILD Update](#pkgbuild-update)  
3.6 [Installing](#installing)  
4. [Uninstalling](#uninstalling)  
5. [NordVPN configuration](#nordvpn-configuration)  
5.1 [Settings Explanation](#settings-explanation)  
5.2 [Show Settings](#show-settings)  
5.3 [Configuration File](#configuration-file)  
6. [Bibliography](#bibliography)  
7. [Change Log](#change-log)  


## Analysis

Package [nordvpn-bin][nordvpn_bin_aur] is extracted from official [.deb](https://repo.nordvpn.com/deb/nordvpn/debian/pool/main/) package. I download its [snapshot](https://aur.archlinux.org/cgit/aur.git/snapshot/nordvpn-bin.tar.gz), update correspond values in file `PKGBUILD`, then use command `makepkg`, `pacman -U` to install/upgrade it.

### Login Credential

After version `3.1.0-6` (Jun 11, 2019), the login credential file *~/.config/nordvpn/auth* doesn't show as plain text any more. To avoid inputing login username/password multi times, you may need to use `nordvpn connect` to input login username/password instead of `nordvpn login`.

From version `3.3.0-0` (Jul 31, 2019), [NordVPN][nordvpn] saves login credential info into file *~/.config/nordvpn/nordvpn.conf* which is only to save `nordvpn settings` before.

### Technology

By default, [NordVPN][nordvpn] uses [OpenVPN][openvpn] protocol on GNU/Linux.

From version `3.3.0-0` (Jul 31, 2019), [NordVPN][nordvpn] introduces [NordLynx][nordlynx] which is based on [WireGuard®][wireguard] ([tutorial](https://support.nordvpn.com/1362931332/)). You can enable it via command `nordvpn set technology NordLynx`.

From nordvpn `3.4.0-1` (Oct 21, 2019), user doesn't need to install [WireGuard®][wireguard] package as a dependency to use [NordLynx][nordlynx].

![nordlynx nordvpn launches new wireguard based protocol](https://vpnpro.com/wp-content/uploads/NordLynx-NordVPN-launches-new-WireGuard-based-protocol.jpg "https://vpnpro.com/blog/nordlynx-nordvpn-launches-new-wireguard-based-protocol/")

If set the technology to [NordLynx][nordlynx], it will not need to set `protocol`, `obfuscate`. They are just for [OpenVPN][openvpn].

For version [3.3.0-0, 3.4.0-1), you may need to install [WireGuard][wireguard] manually.

<details>
<summary>Click to expand WireGuard installation procedure</summary>

[WireGuard][wireguard] official page [Installation](https://www.wireguard.com/install/#arch-module-tools) says:

>sudo pacman -S wireguard-tools wireguard-arch
>
>Instead of `wireguard-arch`, one may choose `wireguard-lts` or `wireguard-dkms`+`linux-headers`, depending on which kernel is used.

[wireguard-arch](https://www.archlinux.org/packages/community/x86_64/wireguard-arch/), [wireguard-lts](https://www.archlinux.org/packages/community/x86_64/wireguard-lts/), [wireguard-dkms](https://www.archlinux.org/packages/community/x86_64/wireguard-dkms/) are in conflict with each other. Choosing depends on which kernel is used.

```bash
# List Kernel Info
pacman -Q linux

# Optional dependencies for wireguard-tools
#     openresolv: for DNS functionality [installed]
#     wireguard-dkms: wireguard module, built by dkms
#     wireguard-arch: wireguard module for linux [pending]
#     wireguard-lts: wireguard module for linux-lts

# - For Default Kernel
# wireguard-arch Wireguard module for Arch Kernel
sudo pacman -S --noconfirm wireguard-tools wireguard-arch

# - For LTS Kernel
# wireguard-lts Wireguard module for LTS Kernel
sudo pacman -S --noconfirm wireguard-tools wireguard-lts

# - For DKMS (Dynamic Kernel Module Suppor) Variant For Other Kernels.
# wireguard-dkms next generation secure network tunnel
sudo pacman -S --noconfirm wireguard-tools wireguard-dkms linux-headers
```

You can also use **WIREGUARD-MODULE** to select manually which provides these three providers.

```bash
# default choose 'wireguard-arch'
# sudo pacman -S --noconfirm WIREGUARD-MODULE

sudo pacman -S WIREGUARD-MODULE
```

Output

```bash
:: There are 3 providers available for WIREGUARD-MODULE:
:: Repository community
   1) wireguard-arch  2) wireguard-dkms  3) wireguard-lts

Enter a number (default=1):
```

</details>


### Firewall Rule

If you use *ufw* as firewall application, it will be setted to `inactive` by [NordVPN][nordvpn] daemon. Firewall rules saved in backup file `/var/lib/nordvpn/backup/iptables.rules`.

Some discussions

* ~~[nordvpn-bin aur comment](https://aur.archlinux.org/packages/nordvpn-bin/#comment-701784)~~
* [HowTo Understand why NordVPN disables UFW](https://forum.manjaro.org/t/howto-understand-why-nordvpn-disables-ufw/96135)

#### Command whitelist

[NordVPN][nordvpn] provides command `whitelist` to add or remove an option from a whitelist ([usage](https://support.nordvpn.com/Connectivity/Linux/1325531132/Installing-and-using-NordVPN-on-Debian-Ubuntu-and-Linux-Mint.htm#Settings)).

```bash
# Open incoming port 22 protocol TCP
nordvpn whitelist add port 22 protocol TCP

# Remove the rule added
nordvpn whitelist remove port 22 protocol TCP

# Add a rule to whitelist your specified subnet (address should be in CIDR notation)
nordvpn whitelist add subnet 192.168.0.0/24

# Remove a rule to whitelist your specified subnet.
nordvpn whitelist remove subnet 192.168.0.0/24
```

### Router Setting

#### Under OpenVPN

```bash
# from log file /var/log/nordvpn/daemon.log

# - Connect
# TUN/TAP device tun0 opened
# TUN/TAP TX queue length set to 100
/sbin/ip link set dev tun0 up mtu 1500
/sbin/ip addr add dev tun0 10.8.1.4/24 broadcast 10.8.1.255
/sbin/ip route add 5.181.233.45/32 via 192.168.0.1
/sbin/ip route add 0.0.0.0/1 via 10.8.1.1
/sbin/ip route add 128.0.0.0/1 via 10.8.1.1

# - Disconnect
/sbin/ip route del 5.181.233.45/32
/sbin/ip route del 0.0.0.0/1
/sbin/ip route del 128.0.0.0/1
# Closing TUN/TAP interface
/sbin/ip addr del dev tun0 10.8.1.4/24
```

#### Under nordlynx

It prompts the following info

>The active UFW firewall on your system prevents us from setting up our firewall properly. We have disabled UFW for the duration of your VPN connection and enabled our firewall to ensure your online security. Your custom UFW rules are imported to our firewall ruleset.

From version `3.3.0-0` (Jul 31, 2019)

```bash
# from log file /var/log/nordvpn/daemon.log

# - Connect
ip link add nordvpn-us2959 type wireguard
wg setconf nordvpn-us2959 /tmp/nordvpn-us2959021165238
ip -4 address add 10.5.0.2/16 dev nordvpn-us2959
ip link set mtu 1420 up dev nordvpn-us2959
wg set nordvpn-us2959 fwmark 51820
ip -4 route add 0.0.0.0/0 dev nordvpn-us2959 table 51820
ip -4 rule add not fwmark 51820 table 51820
ip -4 rule add table main suppress_prefixlength 0

# - Disconnect
ip -4 rule delete table 51820
ip -4 rule delete table main suppress_prefixlength 0
ip link delete nordvpn-us2959
```

From nordvpn `3.4.0-1` (Oct 21, 2019)

```bash
# from log file /var/log/nordvpn/daemon.log

# - Connect
address :10.5.0.2/16
ip -4 address add 10.5.0.2/16 dev nordvpn-us3837
ip link set mtu 1420 up dev nordvpn-us3837
ip rule add to 172.98.93.217 lookup main
ip -4 route add 0.0.0.0/0 dev nordvpn-us3837 table 51820
ip -4 rule add not fwmark 51820 table 51820
ip -4 rule add table main suppress_prefixlength 0

# - Disconnect
ip rule delete to 172.98.93.217
ip -4 rule delete table 51820
ip -4 rule delete table main suppress_prefixlength 0
ip rule delete to 172.98.93.217
```

Checking network connections status via `nmcli`

```bash
# nmcli d
DEVICE           TYPE       STATE         CONNECTION
nordvpn-ca679    wireguard  connected     nordvpn-ca679

# nmcli c show --active
NAME            UUID                                  TYPE       DEVICE
nordvpn-ca679  4dc51d8a-b89f-4105-a67f-d3d72676f1e4  wireguard  nordvpn-ca679
```


## Shell Script

I write a [Shell script](./NordVPN.sh) to set up [NordVPN][nordvpn] on [Arch Linux][archlinux]. It is based on [nordvpn-bin][nordvpn_bin_aur] which is available in the [AUR][aur].

Usage

```bash
download_method='curl -fsL' # wget -qO-

$download_method https://gitlab.com/axdsop/nixnotes/raw/master/CyberSecurity/VPN/NordVPN/NordVPN.sh | bash -s -- -h

# $download_method https://axdlog.com/script/nordvpn.sh | bash -s -- -h
```

You can also add it into file *~/.bashrc* as a alias command.

```bash
alias archlinux_aur_nordvpn="curl -fsL https://gitlab.com/axdsop/nixnotes/raw/master/CyberSecurity/VPN/NordVPN/NordVPN.sh | bash -s --"
```

Help info

```txt
Usage:
    script [options] ...
    script | bash -s -- [options] ...

Installing / Updating NordVPNÂ® CLI Client On Arch Linux!

Documents: https://gitlab.com/axdsop/nixnotes/blob/master/CyberSecurity/VPN/NordVPN/README.md

Please not run as root, or it will prompts error info:
Running makepkg as root is not allowed as it can cause permanent, catastrophic damage to your system.

From v3.8.8 (Dec 17, 2020), NordVPN run daemon with user group 'nordvpn'.

[available option]
    -h    --help, show help info
    -c    --check, check current stable release version
    -f login_credential_path    --specify login credential path which contains Email/Username and Password (One line format 'USERNAME PASSWORD' seperated by white space)
    -s    --specify release version from selection menu (just list latest ${version_num_limit} release versions), default is latest version.
    -t    --specify technology NordLynx (from version 3.3.0-0 (Jul 31, 2019)), default is OpenVPN
    -u    --uninstall, uninstall NordVPN installed, can use along with '-t'
```


## Operation

### Latest Release Version

```bash
download_method='wget -qO-'
[[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && download_method='curl -fsSL'

# extracting latest release version
packages_release_page='https://repo.nordvpn.com/deb/nordvpn/debian/pool/main/'
release_list=$($download_method "${packages_release_page}" | sed -r -n '/.deb</{/release/d; s@.*href=[^_]+_([^_]+).*<\/a>[[:space:]]*([^[:space:]]+).*$@\1|\2@g;p}' | sort | uniq)
release_info=$(echo "${release_list}" | sed '$!d')
```

<details>
<summary>Click to expand NordVPN release version list</summary>

Version|Release Date
---|---
3.9.1-1|31-Mar-2021
3.9.0-1|22-Mar-2021
3.8.9|28-Dec-2020
3.8.8|17-Dec-2020
3.8.7|25-Nov-2020
3.8.6|03-Nov-2020
3.8.5|22-Sep-2020
3.8.4|10-Sep-2020
3.8.3|08-Sep-2020
3.8.2|08-Sep-2020
3.8.1|04-Sep-2020
3.8.0|03-Sep-2020
3.7.4|30-Jun-2020
3.7.3|03-Jun-2020
3.7.2|06-May-2020
3.7.1|17-Apr-2020
3.7.0-3|18-Mar-2020
3.7.0-2|12-Mar-2020
3.6.1-1|11-Feb-2020
3.6.0-5|05-Feb-2020
3.6.0-4|27-Jan-2020
3.6.0-3|16-Jan-2020
3.6.0-2|10-Jan-2020
3.6.0-1|06-Jan-2020
3.5.0-2|20-Dec-2019
3.5.0-1|05-Dec-2019
3.4.0-1|21-Oct-2019
3.3.1-3|11-Sep-2019
3.3.1-2|29-Aug-2019
3.3.1-1|19-Aug-2019
3.3.0-4|06-Aug-2019
3.3.0-3|02-Aug-2019
3.3.0-2|01-Aug-2019
3.3.0-1|31-Jul-2019
3.3.0-0|31-Jul-2019
3.2.0-4|12-Jul-2019
3.2.0-3|10-Jul-2019
3.2.0-2|05-Jul-2019
3.2.0-1|04-Jul-2019
3.2.0-0|04-Jul-2019
3.1.0-6|11-Jun-2019
3.1.0-5|05-Jun-2019
3.1.0-4|04-Jun-2019
3.1.0-3|04-Jun-2019
3.1.0-2|04-Jun-2019
3.1.0-1|04-Jun-2019
3.1.0-0|04-Jun-2019

</details>


### Package Details

File *Packages* [example](https://repo.nordvpn.com/deb/nordvpn/debian/dists/stable/main/binary-amd64/Packages)

```txt
Package: nordvpn
Version: 3.8.6
Depends: procps, iproute2 | iproute, ipset, iptables, xsltproc, ca-certificates
Section: custom
Priority: optional
Architecture: amd64
Essential: no
Installed-Size: 38620
Maintainer: https://nordvpn.com/
Description: Protect your privacy online and access media content with no regional restrictions. Strong encryption and no-log policy with 3000+ servers in 60+ countries.
Size: 10447124
Filename: pool/main/nordvpn_3.8.6_amd64.deb
SHA256: 5313a6d2813bc44aed2653ba08a63614a44bb2c568505e5e781d9b4de420c426
SHA1: 5358ac6b6f338f7209b9f04e23603a242eef8144
MD5sum: 63c920e52908e0943d71b6b8c59821d6
```

Extracting available packages info

```bash
release_version=$(echo "${release_info}" |cut -d\| -f1)
# release_version='3.4.0-1'
packages_detail_page='https://repo.nordvpn.com/deb/nordvpn/debian/dists/stable/main/'
release_info_per_arch=''
release_info_per_arch=$($download_method "${packages_detail_page}" | sed -r -n '/href=.*binary/{s@.*href="([^"]+)".*$@\1@g;p}' | while read -r line; do
    # binary-amd64/
    packages_url="${packages_detail_page}${line}Packages"
    $download_method "${packages_url}" | sed -r -n '/^Version:[[:space:]]*'"${release_version}"'/,/^$/{/^(Version|Depends|Architecture|Filename|SHA256|SHA1|MD5sum):/{/^Depends:.*\|/{s@[[:space:]]*\|[[:space:]]*[^[:space:]]+@,@g;};/^Filename:/{s@.*/(.*)$@'"${packages_release_page}"'\1@g;};s@^[^:]+:[[:space:]]+@@g;p}}' | sed ':a;N;$!ba;s@\n@|@g'
done
)

echo "${release_info_per_arch}"

# Version|Depends|Architecture|Filename|SHA256|SHA1|MD5sum
# 3.8.6|procps, iproute2, ipset, iptables, xsltproc, ca-certificates|amd64|https://repo.nordvpn.com/deb/nordvpn/debian/pool/main/nordvpn_3.8.6_amd64.deb|5313a6d2813bc44aed2653ba08a63614a44bb2c568505e5e781d9b4de420c426|5358ac6b6f338f7209b9f04e23603a242eef8144|63c920e52908e0943d71b6b8c59821d6
```

Output table

Version|Depends|Architecture|Filename|SHA256|SHA1|MD5sum
---|---|---|---|---|---|---
3.8.6|procps, iproute2, ipset, iptables, xsltproc, ca-certificates|amd64|https://repo.nordvpn.com/deb/nordvpn/debian/pool/main/nordvpn_3.8.6_amd64.deb|5313a6d2813bc44aed2653ba08a63614a44bb2c568505e5e781d9b4de420c426|5358ac6b6f338f7209b9f04e23603a242eef8144|63c920e52908e0943d71b6b8c59821d6
3.8.6|procps, iproute2, ipset, iptables, xsltproc, ca-certificates|arm64|https://repo.nordvpn.com/deb/nordvpn/debian/pool/main/nordvpn_3.8.6_arm64.deb|00e2f89f9c7b9261dc74b73e644f431b9db57c8327885856b82f35caaa707f8d|3594ed7eb371c965ee4686200c70be6575ea2ea4|43ece2bd13cd90105625973ac2e1ab63
3.8.6|procps, iproute2, ipset, iptables, xsltproc, ca-certificates|armel|https://repo.nordvpn.com/deb/nordvpn/debian/pool/main/nordvpn_3.8.6_armel.deb|1fc68974efe88e7ba077b2584fa4448587090ee173447b4fc7f4aa169c34c2f7|efaaa6cb9689c9821e59b2e52ccfe92085e2616a|1523dbaf845d00bf8ad0e8049bf7a4e6
3.8.6|procps, iproute2, ipset, iptables, xsltproc, ca-certificates|armhf|https://repo.nordvpn.com/deb/nordvpn/debian/pool/main/nordvpn_3.8.6_armhf.deb|3aa68bb0f5209a510bb29ad3cf66e93c42e4846ed28b0bdddae3536e35100326|83bd21d5c33f90fe4a621f19a07ce04c09ef46f5|ffaea9035d4d3dcb623965ee3a949fa6
3.8.6|procps, iproute2, ipset, iptables, xsltproc, ca-certificates|i386|https://repo.nordvpn.com/deb/nordvpn/debian/pool/main/nordvpn_3.8.6_i386.deb|cb2867848a82b6f5bb0fd54759a31d92bb9cfcde6b34e277ab367556ca8360a9|474deade4164cf5407512b22964478f7fe3216af|a62677474e0860d17482f017fc9183b8


### Architecture Relationship

[PKGBUILD#arch](https://wiki.archlinux.org/index.php/PKGBUILD#arch) says:
>An array of architectures that the PKGBUILD is intended to build and work on. Arch officially supports only x86_64, but other projects may support other architectures. For example, Arch Linux 32 provides support for i686 and Arch Linux ARM provides support for arm (armv5), armv6h (armv6 hardfloat), armv7h (armv7 hardfloat), and aarch64 (armv8 64-bit).

Nordvpn Arch | Arch Linux Arch
---|---
i386 | i686
armhf | armv7h
armel | armv6h
arm64 | aarch64
amd64 | x86_64


### AUR Snapshot

Downloading snapshot from AUR page [nordvpn-bin][nordvpn_bin_aur].

```bash
cd /tmp/
curl -fsL -O https://aur.archlinux.org/cgit/aur.git/snapshot/nordvpn-bin.tar.gz
tar xf nordvpn-bin.tar.gz
cd nordvpn-bin

# ls
# nordvpn-bin.install
# PKGBUILD
# .SRCINFO
```

About `.SRCINFO`, see wiki [.SRCINFO](https://wiki.archlinux.org/index.php/.SRCINFO)

```bash
# .SRCINFO files may be generated using makepkg
makepkg --printsrcinfo > .SRCINFO
```

### PKGBUILD

[PKGBUILD#arch](https://wiki.archlinux.org/index.php/PKGBUILD#arch) says:

>An array of architectures that the PKGBUILD is intended to build and work on. Arch officially supports only `x86_64`, but other projects may support other architectures. For example, Arch Linux 32 provides support for `i686` and Arch Linux ARM provides support for `arm` (armv5), `armv6h` (armv6 hardfloat), `armv7h` (armv7 hardfloat), and `aarch64` (armv8 64-bit).

#### PKGBUILD File

File `PKGBUILD` from compressed file *nordvpn-bin.tar.gz*

But it may use different checksum method, e.g.

* version [3.1.0_6](https://aur.archlinux.org/cgit/aur.git/diff/?h=nordvpn-bin&id=11c650737f9e528a4fb8bd9ad32db4ff54c46d5f) use `sha256sums`;
* version [3.3.0_3](https://aur.archlinux.org/cgit/aur.git/diff/?h=nordvpn-bin&id=a13eb50a77e261d96a3f9279511de1229a49b5b9) use `md5sums`;
* version [3.3.0_4](https://aur.archlinux.org/cgit/aur.git/diff/?h=nordvpn-bin&id=de34c35b2b8dcfba8db9c2e1f4927a99c6aebd66) use `sha256sums`.

#### PKGBUILD Update

PKGBUILD value update

```bash
cp PKGBUILD{,.old}

# pkgver
# ==> ERROR: pkgver is not allowed to contain colons, forward slashes, hyphens or whitespace.
sed -r -i '/^pkgver=/{s@([^=]+=).*@\1'"${release_version//-/_}"'@g}' PKGBUILD

# depends
depends_list=$(echo "${release_info_per_arch}" | cut -d\| -f2 | sort | uniq | sed -r -n 's@xsltproc@libxslt@g;s@[[:space:]]*,[[:space:]]*@ @g;s@^@'\''@g;s@$@'\''@g;s@[[:space:]]+@'\'' '\''@g;p')
# package libxslt provides xsltproc  https://www.archlinux.org/packages/extra/x86_64/libxslt/files/
sed -r -i '/^depends=/{s@([^=]+=).*@\1\('"${depends_list}"'\)@g}' PKGBUILD


# Version|Depends|Architecture|Filename|SHA256|SHA1|MD5sum
# 3.3.0-4|procps, iproute2, iptables, xsltproc|amd64|https://repo.nordvpn.com/deb/nordvpn/debian/pool/main/nordvpn_3.3.0-4_amd64.deb|508722c2bb4943dad260ebbecd0af8d4d115330f55dbf2bc32f3b74a47f125a7|0d3859a13f1db79d7fbc93d3617a6f7d4b99321c|1147e808cdcc3ef557dce45d605647f5

checksum_type=${checksum_type:-'sha256'} # sha256, sha1, md5
field_num=${field_num:-5} # identify field in variable release_info_per_arch

checksum_type=$(sed -r -n '/sums_[^=]+=/{s@(.*?)sums[^=]+=.*@\1@g;p}' PKGBUILD | sort | uniq | tr "[:upper:]" "[:lower:]")

case "${checksum_type}" in
    sha256 ) field_num=5 ;;
    sha1 ) field_num=6 ;;
    md5 ) field_num=7 ;;
esac

# https://wiki.archlinux.org/index.php/PKGBUILD#arch
# An array of architectures that the PKGBUILD is intended to build and work on. Arch officially supports only x86_64, but other projects may support other architectures. For example, Arch Linux 32 provides support for i686 and Arch Linux ARM provides support for arm (armv5), armv6h (armv6 hardfloat), armv7h (armv7 hardfloat), and aarch64 (armv8 64-bit).

# architecture relationship table
# nordvpn - archlinux
# i386 - i686
# armhf - armv7h
# armel - armv6h
# arm64 - aarch64
# amd64 - x86_64
architecture_relationship="i386 i686\narmhf armv7h\narmel armv6h\narm64 aarch64\namd64 x86_64"

echo -e "${architecture_relationship}" | while IFS=" " read -r nordvpn_arch archlinux_arch; do
    checksum_val=''
    checksum_val=$(echo "${release_info_per_arch}" | sed '/'"${nordvpn_arch}"'/!d' | cut -d\| -f"${field_num}")
    [[ -n "${checksum_val}" ]] && sed -r -i '/^'"${checksum_type}"'sums_'"${archlinux_arch}"'=/{s@([^=]+=).*@\1\('\'''${checksum_val}''\''\)@g;}' PKGBUILD
done

# Old method
# _i686 i386
# checksum_i386=$(echo "${release_info_per_arch}" | sed '/i386/!d' | cut -d\| -f"${field_num}")
# sed -r -i '/^'"${checksum_type}"'sums_i686=/{s@([^=]+=).*@\1\('\'''${checksum_i386}''\''\)@g;}' PKGBUILD
```

### Installing

```bash
# .SRCINFO files may be generated using makepkg
makepkg --printsrcinfo > .SRCINFO
makepkg
sudo pacman -U
# _x86_64 amd64
checksum_amd64=$(echo "${release_info_per_arch}" | sed '/amd64/!d' | cut -d\| -f"${field_num}")
sed -r -i '/^'"${checksum_type}"'sums_x86_64=/{s@([^=]+=).*@\1\('\'''${checksum_amd64}''\''\)@g;}' PKGBUILD

# _armv7h armhf
checksum_armhf=$(echo "${release_info_per_arch}" | sed '/armhf/!d' | cut -d\| -f"${field_num}")
sed -r -i '/^'"${checksum_type}"'sums_armv7h=/{s@([^=]+=).*@\1\('\'''${checksum_armhf}''\''\)@g;}' PKGBUILD

# _aarch64 aarch64
checksum_aarch64=$(echo "${release_info_per_arch}" | sed '/aarch64/!d' | cut -d\| -f"${field_num}")
sed -r -i '/^'"${checksum_type}"'sums_aarch64=/{s@([^=]+=).*@\1\('\'''${checksum_aarch64}''\''\)@g;}' PKGBUILD
 --noconfirm nordvpn-bin-*.pkg.tar.xz
nordvpn --version

cd ..
rm -rf /tmp/nordvpn-bin*
```

If you wanna use technology [WireGuard®][wireguard].

```bash
# Version range [3.3.0-0, 3.4.0-1)
# technology OpenVPN or NordLynx
# https://wiki.archlinux.org/index.php/WireGuard#Installation
sudo pacman -Sy --noconfirm wireguard-tools wireguard-arch

# Optional dependencies for wireguard-tools
#     openresolv: for DNS functionality [installed]
#     wireguard-dkms: wireguard module, built by dkms
#     wireguard-arch: wireguard module for linux [pending]
#     wireguard-lts: wireguard module for linux-lts
```

From version `3.8.8` (Dec 17, 2020), [NordVPN][nordvpn] begins to run daemon with `nordvpn` group while socket is only accesible to users that belong to `nordvpn` group or `root`.

```bash
sudo groupadd -r nordvpn

# sudo gpasswd -a $USER nordvpn
sudo usermod -aG nordvpn $USER
```

## Uninstalling

```bash
sudo systemctl stop nordvpnd
sudo systemctl daemon-reload

sudo pacman -R nordvpn-bin
# sudo yay -R nordvpn-bin

sudo groupdel nordvpn # from version 3.8.8

sudo rm -rf /var/lib/nordvpn/
sudo rm -rf /var/log/nordvpn/
sudo rm -rf /run/nordvpn*
rm -rf ~/.config/nordvpn/
[[ -d ~/.cache/yay/nordvpn-bin/ ]] && rm -rf ~/.cache/yay/nordvpn-bin/
```

If you wanna remove [WireGuard®][wireguard]

```bash
# - Method 1
# https://wiki.archlinux.org/index.php/Pacman/Tips_and_tricks#Listing_packages
pacman -Qs wireguard- | sed -r -n '/\//{s@[^\/]+\/([^[:space:]]+).*$@\1@g;p}' | xargs sudo pacman -Rns --noconfirm 2> /dev/null

# - Method 2
# For Default Kernel
sudo pacman -R --noconfirm wireguard-tools wireguard-arch
# For LTS Kernel
sudo pacman -R --noconfirm wireguard-tools wireguard-lts
# For Other Kernel
sudo pacman -R --noconfirm wireguard-tools wireguard-dkms
```


## NordVPN configuration

Attention: version `3.5.0-1` (Dec 05, 2019) *Introducing the revamped internet Kill Switch for ultra security: when enabled, it will block your device from accessing the internet when you're not connected to VPN*. If you enabled *killswitch*, while you try to reconnect to VPN, it may prompt the following errors: *Whoops! We couldn't connect you to 'XXXX XXXXXX'. Please try again. If the problem persists, contact our customer support*. So I decide to diable *killswitch* from version `3.5.0-1`.


```bash
# service start
sudo systemctl daemon-reload
sudo systemctl start nordvpnd
sudo systemctl status nordvpnd
sudo systemctl enable nordvpnd


# login before version

nordvpn connect # avoid input username/password multi times
# Please enter your login details.
# Email / Username:
# Password:
# Welcome to NordVPN! You can now connect to VPN by using 'nordvpn connect'.

# nordvpn login

nordvpn disconnect

# OpenVPN/NordLynx
nordvpn set technology NordLynx

# if change technology to NordLynx, omit 'protocol', 'obfuscate'
nordvpn set protocol TCP
nordvpn set obfuscate disabled

nordvpn set autoconnect disabled

nordvpn set cybersec disabled
nordvpn set dns 9.9.9.9


# from version 3.5.0-1, disable killswitch
nordvpn set killswitch disabled
```

### Settings Explanation

```bash
# Set the technology to OpenVPN or NordLynx.
# nordvpn set technology OpenVPN/NordLynx
nordvpn set technology NordLynx

# If set the technology to NordLynx, it will not need to set 'protocol', 'obfuscate'. They are just for OpenVPN.

# Sets the protocol
# nordvpn set protocol [protocol]
nordvpn set protocol TCP

# Enables or disables obfuscation. When enabled, this feature allows to bypass network traffic sensors which aim to detect usage of the protocol and log, throttle or block it.  
# nordvpn set obfuscate [enabled]/[disabled]
# obfuscate server is less then non-obfuscate server
nordvpn set obfuscate disabled


# Enables or disables auto-connect. When enabled, this feature will automatically try to connect to VPN on operating system startup.
# nordvpn set autoconnect [enabled]/[disabled] [[country]/[server]/[country_code]/[city]/[group] or [country] [city]]
nordvpn set autoconnect disabled

# Enables or disables CyberSec. When enabled, the CyberSec feature will automatically block suspicious websites so that no malware or other cyber threats can infect your device. Additionally, no flashy ads will come into your sight.
# nordvpn set cybersec [enabled]/[disabled]
nordvpn set cybersec enabled

# Enables or disables Kill Switch. This security feature blocks your device from accessing the Internet outside the secure VPN tunnel, in case connection with a VPN server is lost.
# nordvpn set killswitch [enabled]/[disabled]
# from version 3.5.0-1, disable killswitch
nordvpn set killswitch enabled

# Enables or disables notifications
# nordvpn set notify [arguments...]
# nordvpn set notify

# Sets custom DNS servers
# nordvpn set dns [servers]/[disabled]
nordvpn set dns disabled
```

### Show Settings

Show current setting via `nordvpn settings`

Defaul use technology *OpenVPN*

```txt
Protocol: TCP
Kill Switch: enabled
CyberSec: enabled
Obfuscate: disabled
Notify: enabled
Auto-connect: disabled
DNS: disabled
```

Using technology [WireGuard®][wireguard] (not need settings `protocol`, `obfuscate`)

```txt
Kill Switch: enabled
CyberSec: enabled
Notify: enabled
Auto-connect: disabled
DNS: disabled
```


### Configuration File

Version `3.1.0-6`

```bash
# configuration file
~/.config/nordvpn/{auth,cli.log,nordvpn.conf}

cat ~/.config/nordvpn/nordvpn.conf | json_pp
{
   "cybersec" : true,
   "id" : xxxxxxxx,
   "kill_switch" : true,
   "obfuscate" : true,
   "protocol" : "tcp",
   "whitelist" : {
      "ports" : {
         "tcp" : [],
         "udp" : []
      },
      "subnets" : []
   }
}
```


## Bibliography

Blog [sleeplessbeastie's notes](https://blog.sleeplessbeastie.eu/search/?query=nordvpn "Personal notes about Linux, applications and programming.") provides a serious of tutorials about [NordVPN][nordvpn] usage.

* [How to use NordVPN command-line utility](https://blog.sleeplessbeastie.eu/2019/02/04/how-to-use-nordvpn-command-line-utility/)
* [How to use public NordVPN API](https://blog.sleeplessbeastie.eu/2019/02/18/how-to-use-public-nordvpn-api/)


## Change Log

* Jul 08, 2019 Mon 13:47 ET
  * First draft
* Jul 13, 2019 Sat 15:20 ET
  * Add NordVPN set up Shell script
* Jul 29, 2019 Mon 11:53 ET
  * Add firewall rule
* Aug 03, 2019 Sat 11:33 ET
  * Upgrade version to `3.3.0` which introduces technology [NordLynx][nordlynx] based on [WireGuard®][wireguard]
* Aug 12, 2019 Mon 09:25 ET
  * Add [WireGuard®][wireguard] installation details and *Bibliography*
* Aug 27, 2019 Tue 10:04 ET
  * Upgrade version to `3.3.1-1`
* Aug 28, 2019 Wed 21:23 ET
  * Add architecture relationship list
* Sep 11, 2019 Wed 21:16 ET
  * Upgrade version to `3.3.1-3`
* Sep 22, 2019 Sun 10:23 ET
  * Add firewall rule under *nordlynx*
* Oct 29, 2019 Tue 15:55 ET
  * Add network connections status under *nordlynx*
* Oct 31, 2019 Tue 12:35 ET
  * Upgrade version to `3.4.0-1`, from this version user doesn't need to install [WireGuard®][wireguard] manually.
* Nov 02, 2019 Sat 15:48 ET
  * Add new firewall rule via `ip` under *nordlynx* from `3.4.0-1`
* Nov 03, 2019 Sun 21:21 ET
  * Add `whitelist` command usage
* Dec 05, 2019 Thu 08:40 ET
  * Upgrade version to `3.5.0-1`, disable killswitch
* Dec 18, 2020 Fri 09:48 ET
  * From version `3.8.8`, nordvpn runs daemon with `nordvpn` group


[archlinux]:https://www.archlinux.org/
[nordvpn]:https://nordvpn.com/
[nordlynx]:https://nordvpn.com/blog/nordlynx-protocol-wireguard/ "NordLynx – the new solution for a fast and secure VPN connection"
[aur]:https://wiki.archlinux.org/index.php/Arch_User_Repository
[nordvpn_bin_aur]:https://aur.archlinux.org/packages/nordvpn-bin/
[openvpn]:https://wiki.archlinux.org/index.php/OpenVPN
[wireguard]:https://wiki.archlinux.org/index.php/WireGuard


<!-- End -->
