# Setting Up Operating System - Ubuntu

[Ubuntu][ubuntu] supports [Raspberry Pi][raspberrypi] which provides both 64-bit (arm64) and 32-bit (armhf). Recommended version is Ubuntu 20.04 LTS. For [Raspberry Pi 4][raspberrypi4], the 64 bit version should be faster.

For server version, `ubuntu` is the default login user name and corresponding password. You need to change the password after first login.

This note records how to use [Ubuntu][ubuntu] server ARM image to install operating system on [Raspberry Pi 4][raspberrypi4].

Screen shot

![ubuntu server on Raspberry Pi 4](https://gitlab.com/axdsop/nixnotes/-/raw/image/blob-image/RaspberryPi/2020-06-02_Ubuntu_Server/2020-06-02-000635_ubuntu_server_on_respberrypi4.png)


## TOC

1. [Official Documents](#official-documents)  
2. [Image Preparation](#image-preparation)  
2.1 [Image Choose](#image-choose)  
2.2 [Image Verifying](#image-verifying)  
2.3 [Writing Bootable Drive](#writing-bootable-drive)  
3. [Preinstallation](#preinstallation)  
3.1 [Getting setup with Wi-Fi](#getting-setup-with-wi-fi)  
4. [Postinstallation](#postinstallation)  
4.1 [SUDO Configuration](#sudo-configuration)  
4.2 [Creating New Normal User](#creating-new-normal-user)  
4.3 [Static IP Setting Up](#static-ip-setting-up)  
4.4 [Timezone & Locale](#timezone-locale)  
4.5 [APT Sources List](#apt-sources-list)  
4.6 [UFW Firewall](#ufw-firewall)  
4.7 [System Update](#system-update)  
4.8 [Remove cloud-init](#remove-cloud-init)  
4.9 [Service systemd-resolved](#service-systemd-resolved)  
5. [Reboot](#reboot)  
6. [Bibliography](#bibliography)  
7. [Change Log](#change-log)  


## Official Documents

* [Ubuntu Wiki - RaspberryPi](https://wiki.ubuntu.com/ARM/RaspberryPi)
* [How to install Ubuntu on your Raspberry Pi][ubuntu_install_on_rasp]
* [How to create an Ubuntu Server SDcard for Raspberry Pi](https://ubuntu.com/tutorials/how-to-sdcard-ubuntu-server-raspberry-pi)
* [How to verify your Ubuntu download][ubuntu_verify_tutorial]


## Image Preparation

Ubuntu for Raspberry Pi download page

><https://ubuntu.com/download/raspberry-pi>

directly download link

><https://cdimage.ubuntu.com/releases/>


### Image Choose

Raspberry Pi 4 support info (64-bit)

Release|Server|Desktop
---|:---:|:---:
20.04.1 LTS|Y|N
20.10|Y|Y

Here choose Ubuntu 20.04 LTS (Raspberry Pi 4 64-bit), directly download page is <http://cdimage.ubuntu.com/releases/20.04/release/>, you can also download *SHA256SUMS*, *SHA256SUMS.gpg* to verify download ISO images.

File|Size|Url
---|---|---
ubuntu-20.04-preinstalled-server-arm64+raspi.img.xz|667M|http://cdimage.ubuntu.com/releases/20.04/release/ubuntu-20.04-preinstalled-server-arm64+raspi.img.xz
SHA256SUMS|541|http://cdimage.ubuntu.com/releases/20.04/release/SHA256SUMS
SHA256SUMS.gpg|819|http://cdimage.ubuntu.com/releases/20.04/release/SHA256SUMS.gpg


### Image Verifying

See note [Getting Ubuntu](/GNULinux/Distros/Ubuntu/Notes/1-0_GettingUbuntu.md).


### Writing Bootable Drive

Creating the bootable flash drive with the verified image.


```bash
device_path='/dev/sdX' # via lsblk, fdisk -l, e.g. /dev/sda, /dev/sdb
xz_image_name='ubuntu-20.04-preinstalled-server-arm64+raspi.img.xz' # locate in ~/Downloads/

img_name="${xz_image_name%.*}"

# - method 1
# decompress image .img from img.xz
[[ -f "${img_name}" ]] || unxz -d -k -v "${xz_image_name}"
# copying the image to the SD card
sudo dd if="${img_name}" of="${device_path}" bs=4M status=progress oflag=sync # conv=fsync

# - method 2 (fail to work)
# Copying a compressed image to the SD card directly
# unxz -d -k -v "${xz_image_name}" | sudo dd of="${device_path}" bs=4M status=progress oflag=sync # conv=fsync
```

output

```bash
# unxz
ubuntu-20.04-preinstalled-server-arm64+raspi.img.xz (1/1)
  100 %     667.0 MiB / 3,054.4 MiB = 0.218    60 MiB/s       0:51

# dd
3200253952 bytes (3.2 GB, 3.0 GiB) copied, 47 s, 68.1 MB/s
763+1 records in
763+1 records out
3202802688 bytes (3.2 GB, 3.0 GiB) copied, 47.5335 s, 67.4 MB/s
```


## Preinstallation

Utility `curl` has been installed by default.

### Getting setup with Wi-Fi

Following tutorial [How to install Ubuntu on your Raspberry Pi - Wi-Fi or Ethernet](https://ubuntu.com/tutorials/how-to-install-ubuntu-on-your-raspberry-pi#3-wifi-or-ethernet). With the SD card still inserted in your laptop, open a file manager and locate the *system-boot* partition (e.g. /dev/sda1) on the card. It contains initial configuration files that will be loaded during the first boot.

Editing the *network-config* file to add your Wi-Fi credentials.

To do so, uncomment (remove the `#` at the beginning) and edit the following lines:

```bash
# This file contains a netplan-compatible configuration which cloud-init
# will apply on first-boot. Please refer to the cloud-init documentation and
# the netplan reference for full details:
#
# https://cloudinit.readthedocs.io/
# https://netplan.io/reference
#
# Some additional examples are commented out below

version: 2
ethernets:
  eth0:
    dhcp4: true
    optional: true
wifis:
  wlan0:
    dhcp4: true
    optional: true
    access-points:
      <wifi network name>:
        password: "<wifi password>"
```

Note: network name must be enclosed in quotation marks. Just like

```bash
wifis:
  wlan0:
  dhcp4: true
  optional: true
  access-points:
    "home network":
      password: "123456789"
```

Be careful of the syntax indent. After installation successfully, the configuration info will be saved in file */etc/netplan/50-cloud-init.yaml*.

Save the file and extract the card from your laptop. During the first boot, your Raspberry Pi will automatically connect to this network.


## Postinstallation

`ubuntu` is the default login user name and corresponding passwd. You need to change the password after first login.

Clear existed journal log

```bash
# temporarily clean log
du -hs /var/log/journal/
sudo journalctl --rotate
sudo journalctl -m --vacuum-time=1s

# just keep latest 150MB log
journal_log_size='150M'
sudo sed -r -i '/SystemMaxUse=/{s@^#*[[:space:]]*([^=]+=).*@\1'"${journal_log_size}"'@}' /etc/systemd/journald.conf
```

### SUDO Configuration

Granting normal user root privilege which in group `sudo` without prompt password.

```bash
sudo_config='/etc/sudoers'
[[ -f "${sudo_config}"_bak ]] || sudo cp  "${sudo_config}"{,_bak}

# # Allow members of group sudo to execute any command
# %sudo   ALL=(ALL:ALL) ALL
[[ -f "${sudo_config}" ]] && sudo sed -r -i 's@^#*[[:space:]]*(%sudo[[:space:]]+ALL=\(ALL[^\)]*\)[[:space:]]+)ALL.*@\1NOPASSWD:ALL,!/bin/su@' "${sudo_config}"
```

### Creating New Normal User

This is an optional option. If you don't wanna use the default user `ubuntu`, you can create new normal user.

```bash
normal_user_new='pi'
normal_user_password="Ubuntu@$(date +'%Y')"  # If this year is 2020, then password is 'Ubuntu@2020'

login_shell=${login_shell:-'/bin/bash'}
# grant sudo privilege
sudo useradd -m -N -G sudo,adm,video,audio -s "${login_shell}" "${normal_user_new}"
# uid=1000(ubuntu) gid=1000(ubuntu) groups=1000(ubuntu),4(adm),20(dialout),24(cdrom),25(floppy),27(sudo),29(audio),30(dip),44(video),46(plugdev),115(netdev),118(lxd)

# usermod -s "${login_shell}" "${normal_user_new}" # change login shell
echo "${normal_user_new}:${normal_user_password}" | sudo chpasswd
# echo -e "${normal_user_password}\n${normal_user_password}" | sudo passwd "${normal_user_new}"

# optional operation
chage -d0 "${normal_user_new}"  # new created user have to change passwd when first login

# WARNING: Your password has expired.
# You must change your password now and login again!
# Changing password for pi.
# Current password:
# New password:
# Retype new password:
# passwd: password updated successfully
```

If you create new normal user, please logout and login with new normal user (not default user `ubuntu`).

### Static IP Setting Up

#### Ubuntu Desktop (not work)

http://manpages.ubuntu.com/manpages/groovy/man5/netplan.5.html

```bash
sudo cp /etc/netplan/10-rpi-ethernet-eth0.yaml{,.bak}
```

Change file *10-rpi-ethernet-eth0.yaml* like following configuration

Default

```yml
network:
  ethernets:
    eth0:
      # Rename the built-in ethernet device to "eth0"
      match:
        driver: bcmgenet smsc95xx lan78xx
      set-name: eth0
      dhcp4: true
      optional: true
```

```yml
network:
  ethernets:
    eth0:
      # Rename the built-in ethernet device to "eth0"
      match:
        driver: bcmgenet smsc95xx lan78xx
      set-name: eth0
      dhcp4: true
      optional: true
  version: 2
  wifis:
    wlan0:
      access-points:
        <wifi ssid name>:  # Change to your WIFI SSID and corresponding password
          password: "<wifi password>"
      dhcp4: false
      # optional: true
      addresses: [192.168.0.150/24]
      gateway4: 192.168.0.1
      nameservers:
        addresses: [9.9.9.9,1.1.1.1]
```


If you also wanna get a dynamic ip, just set `dhcp4: true`.

Don't forget to execute command `netplan apply`.

```bash
sudo netplan apply

# Warning: The unit file, source configuration file or drop-ins of netplan-wpa-wlan0.service changed on disk. Run 'systemctl daemon-reload' to reload units.
```

#### Ubuntu Server

Ubuntu server use [cloud-init][cloud_init] to manage network configuration.

>Introduced in Ubuntu 16.10 (Yakkety Yak), [netplan](https://netplan.io/) has been the default network configuration tool in Ubuntu since 17.10 (Artful Aardvark). netplan consumes [Networking Config Version 2](https://cloudinit.readthedocs.io/en/latest/topics/network-config-format-v2.html#network-config-v2) input and renders network configuration for supported backends such as *systemd-networkd* and *NetworkManager*.


```bash
sudo sh -c "echo \"network: {config: disabled}\" > /etc/cloud/cloud.cfg.d/99-disable-network-config.cfg"

sudo cp /etc/netplan/50-cloud-init.yaml{,.old}
sudo mv /etc/netplan/50-cloud-init.yaml /etc/netplan/01-netcfg.yaml
```

Change file *01-netcfg.yaml* like following configuration

```yml
network:
  ethernets:
    eth0:
      dhcp4: true
      optional: true
  version: 2
  wifis:
    wlan0:
      access-points:
        <wifi ssid name>:  # Change to your WIFI SSID and corresponding password
          password: "<wifi password>"
      dhcp4: false
      # optional: true
      addresses: [192.168.0.150/24]
      gateway4: 192.168.0.1
      nameservers:
        addresses: [9.9.9.9,1.1.1.1]
```

If you also wanna get a dynamic ip, just set `dhcp4: true`.

Don't forget to execute command `netplan apply`.

```bash
sudo netplan apply

# Warning: The unit file, source configuration file or drop-ins of netplan-wpa-wlan0.service changed on disk. Run 'systemctl daemon-reload' to reload units.
```

If you're remote login via SSH, you may need to re-login with new IP addr.

### Timezone & Locale

```bash
# - Country Code
country_code=${country_code:-'US'} # US or United States
# ipinfo.io   The limit is 1,000 requests per day from an IP address.
# ip-api.com  The limit is 150 requests per minute from an IP address.
country_code=$(timeout 3 curl ipinfo.io/country 2> /dev/null || timeout 3 curl ip-api.com/line/?fields=countryCode 2> /dev/null)


# - Timezone
time_zone=${time_zone:-'America/New_York'}
time_zone=$(timeout 3 curl ipinfo.io/timezone 2> /dev/null || timeout 3 curl ip-api.com/line/?fields=timezone 2> /dev/null)
sudo timedatectl set-timezone "${time_zone}"
# sudo ln -sf /usr/share/zoneinfo/${time_zone} /etc/localtime

# - Locale
locale_default=${locale_default:-'en_US.UTF-8'}  # zh_TW.UTF-8 / zh_CN.UTF-8
sudo sed -r -i '/'"${locale_default}"'/{s@^#*[[:space:]]*@@g}' /etc/locale.gen
# generate locales from /etc/locale.gen
sudo locale-gen
localectl list-locales

# - Hostname
cloud_inif_cfg='/etc/cloud/cloud.cfg'
[[ -f "${cloud_inif_cfg}" ]] && sudo sed -r -i '/preserve_hostname:/{s@^([^:]+:[[:space:]]*).*@\1true@g}' "${cloud_inif_cfg}"

hostname_default=${hostname_default:-'RaspberryPi4'}
sudo hostnamectl set-hostname "${hostname_default}"
# https://wiki.archlinux.org/index.php/Network_configuration#Set_the_hostname
sudo sh -c "echo ${hostname_default} > /etc/hostname"
# https://wiki.archlinux.org/index.php/Network_configuration#Local_hostname_resolution
sudo sh -c "echo -e \"127.0.0.1   localhost\n::1         localhost\n127.0.1.1   ${hostname_default}.localdomain  ${hostname_default}\" >> /etc/hosts"
hostnamectl
```

### APT Sources List

For Ubuntu Server, file */etc/apt/sources.list* is written by [cloud-init](https://cloud-init.io/) on first boot of an instance modifications made here will not survive a re-bundle.

If you wish to make changes you can:

1. add `apt_preserve_sources_list: true` to */etc/cloud/cloud.cfg* or do the same in user-data
2. add sources in */etc/apt/sources.list.d*
3. make changes to template file */etc/cloud/templates/sources.list.tmpl*

Here choosing method 1.

```bash
cloud_inif_cfg='/etc/cloud/cloud.cfg'
sudo sed -r -i '/apt_preserve_sources_list:/d' "${cloud_inif_cfg}" 2> /dev/null
[[ -f "${cloud_inif_cfg}" ]] && sudo sh -c "echo \"apt_preserve_sources_list: true\" >> ${cloud_inif_cfg}"
```

```bash
# support https source list
sudo apt-get install -y apt-transport-https

ubuntu_port_url=${ubuntu_port_url:-'http://ports.ubuntu.com/ubuntu-ports'}
ubuntu_port_url='https://mirrors.xtom.com/ubuntu-ports/'
source_list='/etc/apt/sources.list'
[[ -f "${source_list}_bak" ]] || sudo cp "${source_list}"{,_bak}

code_name=${code_name:-'focal'}
code_name=$(sed -r -n '/codename/I{s@^[^=]*=@@g;p;q}' /etc/os-release 2> /dev/null)
sudo tee "${source_list}" 1> /dev/null << EOF
deb ${ubuntu_port_url} ${code_name} main restricted universe multiverse
deb ${ubuntu_port_url} ${code_name}-updates main restricted universe multiverse
deb ${ubuntu_port_url} ${code_name}-backports main restricted universe multiverse
deb ${ubuntu_port_url} ${code_name}-security main restricted universe multiverse
EOF
```

### UFW Firewall

[ufw and iptables on buster](https://raspberrypi.stackexchange.com/questions/100598/ufw-and-iptables-on-buster)

```bash
sudo apt-get install -y ufw
sudo ufw default deny incoming
sudo ufw default allow outgoing

# Disable ipv6
[[ -f /etc/default/ufw ]] && sudo sed -r -i '/^IPV6=/{s@(IPV6=).*@\1no@g;}' /etc/default/ufw 2> /dev/null

# SSH
sudo ufw limit ssh # or # ufw allow ssh
# sudo ufw limit/allow from 192.168.0.0/24 to any port 22

sudo ufw --force enable # non-interactive
# sudo ufw enable # interactive  Command may disrupt existing ssh connections. Proceed with operation (y|n)?

sudo ufw status verbose

sudo systemctl enable ufw.service
sudo systemctl is-enabled ufw.service
```


### System Update

```bash
sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get dist-upgrade -y
```

```bash
sudo rpi-eeprom-update -a
```

Unneeded packages

```bash
sudo apt-get purge -y snapd \
  busybox* \
  plymouth
```

Remove packages no longer depended

```bash
sudo apt-get --purge -y autoremove
```

Install essential packages

```bash
sudo apt-get install -y bash bash-completion \
 curl \
 xdg-user-dirs \
 git \
 exfat-fuse

# vim text editor
sudo apt-get install -y vim

# terminal multiplexer
sudo apt-get install -y tmux
# https://aur.archlinux.org/packages/tmux-bash-completion-git/
[[ -f /usr/share/bash-completion/completions/tmux ]] || sudo curl -fsSL -o /usr/share/bash-completion/completions/tmux https://raw.githubusercontent.com/imomaliev/tmux-bash-completion/master/completions/tmux
```

If you run docker

```bash
# https://docs.docker.com/engine/install/ubuntu/
cd /tmp
curl -fsSL https://get.docker.com -o get-docker.sh
sudo bash get-docker.sh

# add login user into group docker
sudo usermod -aG docker $USER
```

Disable unneeded services

```bash
sudo systemctl disable unattended-upgrades.service
sudo systemctl disable NetworkManager-wait-online.service

sudo systemctl disable rpi-eeprom-update.service
sudo systemctl mask rpi-eeprom-update.service
```


### Remove cloud-init

For Ubuntu Server, *cloud-init* will slow down system boot time. If you don't need, you can remove it via blog [How to remove cloud-init from Ubuntu Server 20.04](https://www.networkreverse.com/2020/06/how-to-remove-cloud-init-from-ubuntu.html).

```bash
# to update this file, run dpkg-reconfigure cloud-init
# datasource_list: [ NoCloud, ConfigDrive, OpenNebula, DigitalOcean, Azure, AltCloud, OVF, MAAS, GCE, OpenStack, CloudSigma, SmartOS, Bigstep, Scaleway, AliYun, Ec2, CloudStack, Hetzner, IBMCloud, Oracle, Exoscale, RbxCloud, None ]
echo 'datasource_list: [ None ]' | sudo -s tee /etc/cloud/cloud.cfg.d/90_dpkg.cfg

sudo apt-get purge -y cloud-init
[[ -d /etc/cloud/ ]] && sudo rm -rf /etc/cloud/
[[ -d /var/lib/cloud/ ]] && sudo rm -rf /var/lib/cloud/
```

Boot time comparasion

**Before** (via `systemd-analyze time`)

>Startup finished in 6.553s (kernel) + 17.595s (userspace) = 24.149s
>graphical.target reached after 16.098s in userspace

Via `systemd-analyze blame`

>3.003s cloud-init-local.service
>2.226s cloud-init.service
>1.718s cloud-config.service
>1.486s cloud-final.service

**After** (via `systemd-analyze time`)

>Startup finished in 6.529s (kernel) + 4.192s (userspace) = 10.722s
>graphical.target reached after 4.162s in userspace


### Service systemd-resolved

Service `systemd-resolved.service` listens port *53*, LLMNR(Link-Local Multicast Name Resolution) listens port *5535* by default. If you wanna free up these ports.

```bash
systemd_resolve_path='/etc/systemd/resolved.conf'

[[ -f "${systemd_resolve_path}.bak" ]] || cp "${systemd_resolve_path}"{,.bak}

dns_server_list='9.9.9.9 1.1.1.1'  # or just 9.9.9.9

sed -r -i '/^#*DNS=/{s@^#*([^=]+=).*@\19.9.9.9 1.1.1.1@g}; /^#*(LLMNR|DNSStubListener)=/{s@^#*([^=]+=).*@\1no@g}' "${systemd_resolve_path}"

[[ -f /run/systemd/resolve/resolv.conf ]] && ln -sf /run/systemd/resolve/resolv.conf /etc/resolv.conf
```


## Reboot

Reboot system.

```bash
sudo reboot
```

Login via default user `ubuntu` or new created normal user.

```bash
# - xdg user dirs generation, executing after `locale-gen`
xdg-user-dirs-update

# - personal custom boost tool
# axdsop boost tool intallation, running as normal user (pi)
curl -fsL https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/axdsop_boost_tool.sh | bash -s -- -i
# customsize configuration deployment
bash ~/.config/axdsop/deploy.sh -h
```

After reboot system, you may also consider remove older kernel

```bash
dpkg -l | sed -r -n '/linux-(image|headers)-?/{s@^i*[[:space:]]*([^[:space:]]+).*@\1@g;/'"$(uname -r)"'/d;p}' | xargs sudo apt-get purge -y
sudo apt-get --purge -y autoremove
```


## Bibliography

* [cloud-init Network Configuration](https://cloudinit.readthedocs.io/en/latest/topics/network-config.html)
* [Configure Ubuntu Server 18.04 to use a static IP address](https://graspingtech.com/ubuntu-server-18.04-static-ip/)
* [How to setup a static IP on Ubuntu Server 18.04](https://askubuntu.com/questions/1029531/how-to-setup-a-static-ip-on-ubuntu-server-18-04)


## Change Log

* May 31, 2020 Sun 17:45 ET
  * First draft
* Jun 01, 2020 Mon 21:31 ET
  * Add WIFI setting up and postinstallation configuration
* Jun 02, 2020 Tue 09:30 ET
  * Add normal user create, journal log clean, SUDO configuration
* Sep 02, 2020 Wed 15:21 ET
  * Add `cloud-init` remove action
* Sep 03, 2020 Thu 13:21 ET
  * Add section `systemd-resolved`


[raspberrypi]:https://www.raspberrypi.org/
[raspberrypi4]:https://www.raspberrypi.org/products/raspberry-pi-4-model-b/
[ubuntu]:https://ubuntu.com/ "The leading operating system for PCs, IoT devices, servers and the cloud."
[ubuntu_rasp]:https://ubuntu.com/download/raspberry-pi
[ubuntu_verify_tutorial]:https://ubuntu.com/tutorials/tutorial-how-to-verify-ubuntu
[ubuntu_install_on_rasp]:https://ubuntu.com/tutorials/how-to-install-ubuntu-on-your-raspberry-pi
[cloud_init]:https://cloud-init.io/ "The standard for customising cloud instances"

<!-- End -->