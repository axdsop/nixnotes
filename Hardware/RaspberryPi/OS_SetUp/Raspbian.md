# Setting Up Operating System - Raspbian

This note records how to use [Raspbian][raspbian] image to install operating system on [Raspberry Pi 4][raspberrypi4].

## TOC

1. [Image Choose](#image-choose)  
2. [Preparation](#preparation)  
2.1 [Raspbian Image](#raspbian-image)  
2.2 [Download Link](#download-link)  
2.3 [Image Verification](#image-verification)  
2.3.1 [Via Hash Digest](#via-hash-digest)  
2.3.2 [Via GPG](#via-gpg)  
2.4 [Copying Image](#copying-image)  
3. [Configuration](#configuration)  
3.1 [Raspbian Mirrors (optional)](#raspbian-mirrors-optional)  
3.2 [Bootloader EEPROM Upgrade (optional)](#bootloader-eeprom-upgrade-optional)  
3.3 [Firmware Upgrade (optional)](#firmware-upgrade-optional)  
3.4 [System Update](#system-update)  
4. [Networking Configuration](#networking-configuration)  
4.1 [Wireless Lan](#wireless-lan)  
4.2 [Static IP Setting Up](#static-ip-setting-up)  
5. [Security](#security)  
5.1 [UFW Firewall](#ufw-firewall)  
6. [Optimization](#optimization)  
7. [Error Occuring](#error-occuring)  
7.1 [bcmgenet](#bcmgenet)  
7.2 [udisksd](#udisksd)  
7.3 [lightdm](#lightdm)  
8. [Change Log](#change-log)  


## Image Choose

[Raspberry Pi][raspberrypi] officially provides 2 methods to [install](https://www.raspberrypi.org/documentation/installation/) operating system: [NOOBS][noobs] and [Raspberry Pi OS][raspbian] (Raspbian).

>[NOOBS][noobs] is an easy operating system installer which contains Raspbian and LibreELEC. It also provides a selection of alternative operating systems which are then downloaded from the internet and installed.
>
>[Raspberry Pi OS][raspbian] (previously called Raspbian) is the Foundation�s official supported operating system.

Attention: both of them are use a 32-bit LPAE kernel and a 32-bit userland.

[Formatting an SDXC card for use with NOOBS](https://www.raspberrypi.org/documentation/installation/sdxc_formatting.md) says:
>According to the [SD specifications](https://www.sdcard.org/developers/overview/capacity/), any SD card larger than 32GB is an **SDXC** card and has to be formatted with the *exFAT* filesystem.
>
>The Raspberry Pi's bootloader, built into the GPU and non-updateable, only has support for reading from FAT filesystems (both FAT16 and FAT32), and is unable to boot from an exFAT filesystem. So if you want to use NOOBS on a card that is 64GB or larger, you need to reformat it as FAT32 first before copying the NOOBS files to it.

This means that an SD (or micro SD) card that has a capacity over 32GB is not suitable for [NOOBS][noobs]. So I choose [Raspberry Pi OS][raspbian] (Raspbian).

From Feb 2022 ([link](https://www.raspberrypi.com/news/raspberry-pi-os-64-bit/)), [Raspberry Pi OS][raspbian] officially support 64-bit (beta beta test [page](https://www.raspberrypi.org/forums/viewtopic.php?t=275370)).

[Raspberry Pi OS][raspbian] (Raspbian) type list (Page [Operating system images](https://www.raspberrypi.com/software/operating-systems/))

Arch|Type|Download Link<br>(directly)|Note
---|---|---|---
armhf (64-bit)|Desktop and recommended software|[raspios_full_arm64](https://downloads.raspberrypi.org/raspios_full_arm64/images/)|Image with desktop and recommended software based on Debian 11 (bullseye)
armhf (64-bit)|Desktop|[raspios_arm64](https://downloads.raspberrypi.org/raspios_arm64/images/)|Image with desktop based on Debian 11 (bullseye)
armhf (64-bit)|Light|[raspios_lite_arm64](https://downloads.raspberrypi.org/raspios_lite_arm64/images/)|Minimal image based on Debian 11 (bullseye)
armhf (32-bit)|Desktop and recommended software|[raspios_full_armhf](https://downloads.raspberrypi.org/raspios_full_armhf/images/)|Image with desktop and recommended software based on Debian 11 (bullseye)
armhf (32-bit)|Desktop|[raspios_armhf](https://downloads.raspberrypi.org/raspios_armhf/images/)|Image with desktop based on Debian 11 (bullseye)
armhf (32-bit)|Light|[raspios_lite_armhf](https://downloads.raspberrypi.org/raspios_lite_armhf/images/)|Minimal image based on Debian 11 (bullseye)

~~arm64 (64-bit)||[raspios_arm64](https://downloads.raspberrypi.org/raspios_arm64/images/)|beta test [forum](https://www.raspberrypi.org/forums/viewtopic.php?t=275370)~~


## Preparation

[Using Raspberry Pi Imager](https://www.raspberrypi.com/documentation/computers/getting-started.html#using-raspberry-pi-imager) is the easiest method for GUI user (Mac OS, Windows, Linux Desktop) to download the image automatically and install it to the SD card.

### Raspbian Image

Online release version detection

```bash
raspbian_url='https://www.raspberrypi.org/software/operating-systems/'

download_method='wget -qO-'
[[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && download_method='curl -fsSL'

$download_method "${raspbian_url}" | sed -r -n 's@<\/[^>]*>@&\n@g;s@<div[^>]*>@\n&@g;p' | sed -r -n '/"c-software-os__download-links"/,/<\/div[^>]*>/{/^<\/div[^>]*>/{s@.*@---@g};s@.*<a href="([^"]+)".*@\1@g;/.torrent/d;/^https?:/{s@.*@Download:&@g};p};/c-software-os"/,/<\/div[^>]*>/{/^$/d;/c-software-os"/,/^<\/div>[[:space:]]$/{/^<\/div[^>]*>/d;/class="c-software-os_/{/sha[[:digit:]]+-switch/d;/sha[[:digit:]]+-value/{s@(.*?_)(sha[[:digit:]]+)(-value*)@\1>\2:<\3@g};s@<!--[^>]*>@@g;/_heading"/{s@<[^>]*>@@g;s@.*@Title:&@g};s@<[^>]*>@@g;p}}}' | sed -r -n '/:/{s@^[^:]+:[[:space:]]*@@g};p' | sed ':a;N;$!ba;s@\n@|@g' | sed -r -n 's@\|?-{3,}@\n@g;p' | sed '/^$/d'


# Title:Raspberry Pi OS with desktop and recommended software
# Release date: December 2nd 2020
# Kernel version: 5.4
# Size: 2,949MB
# sha256:cacfd32ae4bc08708a65982abab70c7c99975ae5a9c309b11c4fcf88bf03ac86
# Download:https://downloads.raspberrypi.org/raspios_full_armhf/images/raspios_full_armhf-2020-12-04/2020-12-02-raspios-buster-armhf-full.zip
```

Output

Title|Release date|32/64 bit|Kernel version|OS Version|Size|SHA-256|Zip url|Archive page
---|---|---|---|---|---|---|---|---
Raspberry Pi OS with desktop|May 3rd 2023|32-bit|6.1|11 (bullseye)|872MB|38a66ed777a1f4e4c07f7dcb2b2feadfda4503d5e139f675758e0e91d34ed75f|https://downloads.raspberrypi.org/raspios_armhf/images/raspios_armhf-2023-05-03/2023-05-03-raspios-bullseye-armhf.img.xz|https://downloads.raspberrypi.org/raspios_armhf/images/
|Raspberry Pi OS with desktop and recommended software|May 3rd 2023|32-bit|6.1|11 (bullseye)|2,701MB|67abd3bc034faf85b59b8e4a28982cb0ab1bc0504877ec3d426e05f6402ed225|https://downloads.raspberrypi.org/raspios_full_armhf/images/raspios_full_armhf-2023-05-03/2023-05-03-raspios-bullseye-armhf-full.img.xz|https://downloads.raspberrypi.org/raspios_full_armhf/images/
|Raspberry Pi OS Lite|May 3rd 2023|32-bit|6.1|11 (bullseye)|364MB|b5e3a1d984a7eaa402a6e078d707b506b962f6804d331dcc0daa61debae3a19a|https://downloads.raspberrypi.org/raspios_lite_armhf/images/raspios_lite_armhf-2023-05-03/2023-05-03-raspios-bullseye-armhf-lite.img.xz|https://downloads.raspberrypi.org/raspios_lite_armhf/images/
|Raspberry Pi OS with desktop|May 3rd 2023|64-bit|6.1|11 (bullseye)|818MB|e7c0c89db32d457298fbe93195e9d11e3e6b4eb9e0683a7beb1598ea39a0a7aa|https://downloads.raspberrypi.org/raspios_arm64/images/raspios_arm64-2023-05-03/2023-05-03-raspios-bullseye-arm64.img.xz|https://downloads.raspberrypi.org/raspios_arm64/images/
|Raspberry Pi OS Lite|May 3rd 2023|64-bit|6.1|11 (bullseye)|308MB|bf982e56b0374712d93e185780d121e3f5c3d5e33052a95f72f9aed468d58fa7|https://downloads.raspberrypi.org/raspios_lite_arm64/images/raspios_lite_arm64-2023-05-03/2023-05-03-raspios-bullseye-arm64-lite.img.xz|https://downloads.raspberrypi.org/raspios_lite_arm64/images/
|Raspberry Pi OS (Legacy) with desktop|May 3rd 2023|32-bit|5.10|10 (buster)|788MB|9f72726cc19abce9238a0586d5644f6df56378f2214cd55c96d417b2717daf5b|https://downloads.raspberrypi.org/raspios_oldstable_armhf/images/raspios_oldstable_armhf-2023-05-03/2023-05-03-raspios-buster-armhf.img.xz|https://downloads.raspberrypi.org/raspios_oldstable_armhf/images/
|Raspberry Pi OS Lite (Legacy)|May 3rd 2023|32-bit|5.10|10 (buster)|321MB|3d210e61b057de4de90eadb46e28837585a9b24247c221998f5bead04f88624c|https://downloads.raspberrypi.org/raspios_oldstable_lite_armhf/images/raspios_oldstable_lite_armhf-2023-05-03/2023-05-03-raspios-buster-armhf-lite.img.xz|https://downloads.raspberrypi.org/raspios_oldstable_lite_armhf/images/
|Debian Bullseye with Raspberry Pi Desktop|July 1st 2022|32-bit|5.10|11 (bullseye)|3,440MB|5fa906df25e600bf7d7e6a5eb7b0e9b6605e60992ee6c8efe79bc99e7c2452bd|https://downloads.raspberrypi.org/rpd_x86/images/rpd_x86-2022-07-04/2022-07-01-raspios-bullseye-i386.iso|https://downloads.raspberrypi.org/rpd_x86/images/


<details><summary>Click to expand outdated release info</summary>

Year 2022

Title|Release date|32/64 bit|Kernel version|OS Version|Size|SHA-256|Zip url|Archive page
---|---|---|---|---|---|---|---|---
Raspberry Pi OS with desktop|January 28th 2022|32-bit|5.10|11 (bullseye)|1,246MB|a67e372dc8b3a66a00756b13031cb0be894483fc45ee9a27a2fc1bf2720eddeb|https://downloads.raspberrypi.org/raspios_armhf/images/raspios_armhf-2022-01-28/2022-01-28-raspios-bullseye-armhf.zip|https://downloads.raspberrypi.org/raspios_armhf/images/
|Raspberry Pi OS with desktop and recommended software|January 28th 2022|32-bit|5.10|11 (bullseye)|3,267MB|cfe1aca4190beb5803ecbe4125891d32ef38a11df85d06a6b3db364225f9cc96|https://downloads.raspberrypi.org/raspios_full_armhf/images/raspios_full_armhf-2022-01-28/2022-01-28-raspios-bullseye-armhf-full.zip|https://downloads.raspberrypi.org/raspios_full_armhf/images/
|Raspberry Pi OS Lite|January 28th 2022|32-bit|5.10|11 (bullseye)|482MB|f6e2a3e907789ac25b61f7acfcbf5708a6d224cf28ae12535a2dc1d76a62efbc|https://downloads.raspberrypi.org/raspios_lite_armhf/images/raspios_lite_armhf-2022-01-28/2022-01-28-raspios-bullseye-armhf-lite.zip|https://downloads.raspberrypi.org/raspios_lite_armhf/images/
|Raspberry Pi OS with desktop|January 28th 2022|64-bit|5.10|11 (bullseye)|1,135MB|c6f583fab8ed8d84bdf272d095c821fa70d2a0b434ba78432648f69b661d3783|https://downloads.raspberrypi.org/raspios_arm64/images/raspios_arm64-2022-01-28/2022-01-28-raspios-bullseye-arm64.zip|https://downloads.raspberrypi.org/raspios_arm64/images/
|Raspberry Pi OS Lite|January 28th 2022|64-bit|5.10|11 (bullseye)|435MB|d694d2838018cf0d152fe81031dba83182cee79f785c033844b520d222ac12f5|https://downloads.raspberrypi.org/raspios_lite_arm64/images/raspios_lite_arm64-2022-01-28/2022-01-28-raspios-bullseye-arm64-lite.zip|https://downloads.raspberrypi.org/raspios_lite_arm64/images/
|Raspberry Pi OS (Legacy) with desktop|January 28th 2022|32-bit|5.10|10 (buster)|1,116MB|281914c65cffd855feda798276185d9f1925eabb0f2d2b0321d4c6398dad69c0|https://downloads.raspberrypi.org/raspios_oldstable_armhf/images/raspios_oldstable_armhf-2022-01-28/2022-01-28-raspios-buster-armhf.zip|https://downloads.raspberrypi.org/raspios_oldstable_armhf/images/
|Raspberry Pi OS Lite (Legacy)|January 28th 2022|32-bit|5.10|10 (buster)|451MB|84c7d41b6d0a9fc8e6163045842a1bae7cc57cef36808d3d350fdf18e9b3f50e|https://downloads.raspberrypi.org/raspios_oldstable_lite_armhf/images/raspios_oldstable_lite_armhf-2022-01-28/2022-01-28-raspios-buster-armhf-lite.zip|https://downloads.raspberrypi.org/raspios_oldstable_lite_armhf/images/
|Debian Buster with Raspberry Pi Desktop|January 11th 2021|32-bit|4.19|10 (buster)|2,948MB|c78c8dca8ca80ffbac90f4cedfedb6793b37b06df307b0c87e778eef3842a9be|https://downloads.raspberrypi.org/rpd_x86/images/rpd_x86-2021-01-12/2021-01-11-raspios-buster-i386.iso|https://downloads.raspberrypi.org/rpd_x86/images/

Year 2021

Title|Release date|Kernel version|Size|SHA-256|Zip url
---|---|---|---|---|---
Raspberry Pi OS with desktop and recommended software|March 4th 2021|5.10|2,868MB|c0256f411cbb39d2c5a41d68832cfada4c053689c059ded1ff660803e9fc8891|https://downloads.raspberrypi.org/raspios_full_armhf/images/raspios_full_armhf-2021-03-25/2021-03-04-raspios-buster-armhf-full.zip
|Raspberry Pi OS with desktop|March 4th 2021|5.10|1,175MB|d3de1a33d2d4f4990345b6369960b04c70b577519e6f25f4d7ec601e305e932a|https://downloads.raspberrypi.org/raspios_armhf/images/raspios_armhf-2021-03-25/2021-03-04-raspios-buster-armhf.zip
|Raspberry Pi OS Lite|March 4th 2021|5.10|442MB|ea92412af99ec145438ddec3c955aa65e72ef88d84f3307cea474da005669d39|https://downloads.raspberrypi.org/raspios_lite_armhf/images/raspios_lite_armhf-2021-03-25/2021-03-04-raspios-buster-armhf-lite.zip
|Debian Buster with Raspberry Pi Desktop|January 11th 2021|4.19|2,948MB|c78c8dca8ca80ffbac90f4cedfedb6793b37b06df307b0c87e778eef3842a9be|https://downloads.raspberrypi.org/rpd_x86/images/rpd_x86-2021-01-12/2021-01-11-raspios-buster-i386.iso

</details>

For old download page <https://www.raspberrypi.org/downloads/raspbian/>

<details><summary>Click to expand extract command</summary>

```bash
raspbian_url='https://www.raspberrypi.org/downloads/raspbian/'

download_method='wget -qO-'
[[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && download_method='curl -fsSL'

$download_method "${raspbian_url}" | sed -r -n '/class="image-info"/,/<\/li>/{/dl-zip/{s@.*href="([^"]+)".*@\1@g;p};/class="image-info"/{s@.*@&^@g;};/class="image-/{/href=/{s@.*href="([^"]+)".*@\1@g;};s@[[:space:]]*<[^>]*>[[:space:]]*@@g;/^$/d;/:/{/http/d;/^[^(http)]/s@[^:]+:(.*)@\1@g};p}}' | sed ':a;N;$!ba;s@\n@|@g' | sed -r -n 's@\|?\^\|@\n\n@g;p' | sed '/^$/d'
```

Output

Title|Version|Release date|Kernel version|Size|Zip url|SHA-256
---|---|---|---|---|---|---
Image with desktop and recommended software based on Debian Buster|August 2020|2020-08-20|5.4|2531 MB|https://downloads.raspberrypi.org/raspios_full_armhf_latest|24342f3668f590d368ce9d57322c401cf2e57f3ca969c88cf9f4df238aaec41f
Image with desktop based on Debian Buster|August 2020|2020-08-20|5.4|1133 MB|https://downloads.raspberrypi.org/raspios_armhf_latest|9d658abe6d97f86320e5a0288df17e6fcdd8776311cc320899719aa805106c52
Minimal image based on Debian Buster|August 2020|2020-08-20|5.4|435 MB|https://downloads.raspberrypi.org/raspios_lite_armhf_latest|4522df4a29f9aac4b0166fbfee9f599dab55a997c855702bfe35329c13334668

<!-- Image with desktop and recommended software based on Debian Buster|May 2020|2020-05-27|4.19|2523 MB|https://downloads.raspberrypi.org/raspios_full_armhf_latest|fdbd6f5b5b7e1fa5e724bd8747c5109801442e9200144d0a1941c9e73c93dd61
Image with desktop based on Debian Buster|May 2020|2020-05-27|4.19|1128 MB|https://downloads.raspberrypi.org/raspios_armhf_latest|b9a5c5321b3145e605b3bcd297ca9ffc350ecb1844880afd8fb75a7589b7bd04
Minimal image based on Debian Buster|May 2020|2020-05-27|4.19|432 MB|https://downloads.raspberrypi.org/raspios_lite_armhf_latest|f5786604be4b41e292c5b3c711e2efa64b25a5b51869ea8313d58da0b46afc64 -->

</details>

### Download Link

For the old download page, you may need to extract the real download link.

```bash
raw_link='https://downloads.raspberrypi.org/raspios_armhf_latest'

curl -fsL -I "${raw_link}" | sed -r -n '/^Location:/Ip' | sed '$!d;s@^[^:]*:[[:space:]]*@@g;'

# https://downloads.raspberrypi.org/raspios_armhf/images/raspios_armhf-2021-03-25/2021-03-04-raspios-buster-armhf.zip
```


### Image Verification

Official site provides zip image hash digest (SHA-256) ang GPG signature.

#### Via Hash Digest

SHA-256 hash digest

```bash
image_name='2021-03-04-raspios-buster-armhf.zip'
# https://downloads.raspberrypi.org/raspios_armhf/images/raspios_armhf-2021-03-25/2021-03-04-raspios-buster-armhf.zip.sha256

# method 1 - via sha256sum
sha256sum "${image_name}"
# d3de1a33d2d4f4990345b6369960b04c70b577519e6f25f4d7ec601e305e932a  2021-03-04-raspios-buster-armhf.zip

# method 2 - via openssl
openssl dgst -sha256 "${image_name}"
# SHA256(2021-03-04-raspios-buster-armhf.zip)= d3de1a33d2d4f4990345b6369960b04c70b577519e6f25f4d7ec601e305e932a
```

<details><summary>Click to expand outupt</summary>

```sh
# curl -fsSL https://downloads.raspberrypi.org/raspios_armhf/images/raspios_armhf-2021-03-25/2021-03-04-raspios-buster-armhf.zip.sha256
d3de1a33d2d4f4990345b6369960b04c70b577519e6f25f4d7ec601e305e932a  2021-03-04-raspios-buster-armhf.zip

# method 1 - via sha256sum
d3de1a33d2d4f4990345b6369960b04c70b577519e6f25f4d7ec601e305e932a  2021-03-04-raspios-buster-armhf.zip

# method 2 - via openssl
SHA256(2021-03-04-raspios-buster-armhf.zip)= d3de1a33d2d4f4990345b6369960b04c70b577519e6f25f4d7ec601e305e932a
```

</details>


#### Via GPG

Using personal custom function [GPGSignatureVerification](/CyberSecurity/GnuPG/Notes/2-1_UsageExample.md#package-signature-verifying) to verify PGP signature.

PGP signature file `.sig` doesn't contain GPG signing key, from official forum post [Looking for public key used to sign Stretch image](https://www.raspberrypi.org/forums/viewtopic.php?t=272042), *Raspberry Pi Downloads Signing Key* is <https://www.raspberrypi.org/raspberrypi_downloads.gpg.key>.

Usage

```bash
# fn_GPGSignatureVerification 'ISOPATH' 'SIGPATH' 'GPGPATH'
fn_GPGSignatureVerification '2021-03-04-raspios-buster-armhf.zip'{,.sig} 'https://www.raspberrypi.org/raspberrypi_downloads.gpg.key'

fn_GPGSignatureVerification '2021-03-04-raspios-buster-armhf.zip' 'https://downloads.raspberrypi.org/raspios_armhf/images/raspios_armhf-2021-03-25/2021-03-04-raspios-buster-armhf.zip.sig' 'https://www.raspberrypi.org/raspberrypi_downloads.gpg.key'
```

Keywords `Good signature` indicates signature verification approves.

<details><summary>Click to expand outupt</summary>

```sh
Importing PGP key
gpg: keybox '/tmp/CNBYhZs6/gpg_import.gpg' created
gpg: key 0x8738CD6B956F460C: 1 signature not checked due to a missing key
gpg: key 0x8738CD6B956F460C: public key "Raspberry Pi Downloads Signing Key" imported
gpg: Total number processed: 1
gpg:               imported: 1
gpg: no ultimately trusted keys found

Verifying signature
gpg: Signature made Tue 23 Mar 2021 08:23:37 AM EDT
gpg:                using RSA key 54C3DD610D9D1B4AF82A37758738CD6B956F460C
gpg: Good signature from "Raspberry Pi Downloads Signing Key" [unknown]
gpg: WARNING: This key is not certified with a trusted signature!
gpg:          There is no indication that the signature belongs to the owner.
Primary key fingerprint: 54C3 DD61 0D9D 1B4A F82A  3775 8738 CD6B 956F 460C

Temporary directory /tmp/CNBYhZs6 has been removed.
```

</details>


### Copying Image

Using command `dd` to copy image to the SD card.

```bash
device_path='/dev/sdX' # via lsblk, fdisk -l, e.g. /dev/sda, /dev/sdb
zip_name="2021-03-04-raspbian-buster-armhf.zip" # locate in ~/Downloads/
# need utility 'unzip'

# - method 1: Copying a zipped image to the SD card
unzip -p "${zip_name}" | sudo dd of="${device_path}" bs=4M status=progress conv=fsync

# - method 2: Copying the image to the SD card
img_name="${zip_name%.*}.img"
[[ -f "${img_name}" ]] || unzip "${zip_name}"
sudo dd if="${img_name}" of="${device_path}" bs=4M status=progress conv=fsync
```


## Configuration

Official documentation [Configuration](https://www.raspberrypi.org/documentation/configuration/)

* [raspi-config](https://www.raspberrypi.org/documentation/configuration/raspi-config.md)

```sh
sudo raspi-config
```

### Raspbian Mirrors (optional)

Choosing mirror site from [Raspbian Mirrors][raspbianmirrors] then replace it into configuration file */etc/apt/sources.list*.

For *arm64*, default source site is <http://deb.debian.org/>.
For *armhf*, default source site is <http://raspbian.raspberrypi.org/raspbian/>, but sometimes its download speed is too slow.

<!-- The following lists have been tested. (North America) -->
<!-- Continent|Country|Mirror|Deb/Deb-src -->

To replace the default mirror site

For *arm64*

```bash
raspi_list_path='/etc/apt/sources.list'
[[ -f "${raspi_list_path}.origin" ]] || sudo cp -f "${raspi_list_path}"{,.origin}
[[ -f "${raspi_list_path}" ]] && sudo sed -r -i '/https?:/{s@https?:@https:@g}' "${raspi_list_path}"
```

For *armhf*

```bash
fn_Source_List_Replace(){
    local l_new_mirror_site="${1:-}"
    if [[ "${l_new_mirror_site}" =~ ^https?:// ]]; then
        source_list='/etc/apt/sources.list'
        [[ -f "${source_list}.origin" ]] || sudo cp -f "${source_list}"{,.origin}
        sudo sed -r -i '/^deb/{s@https?:[^[:space:]]+@'"${l_new_mirror_site}"'@g;}' "${source_list}"
    fi
}


# new_mirror_site='http://raspbian.raspberrypi.org/raspbian/' # default source
# new_mirror_site='https://raspbian.freemirror.org/raspbian/'  # Canada
new_mirror_site='https://raspbian.mirror.constant.com/raspbian/'  # United States

fn_Source_List_Replace "${new_mirror_site}"
```

For *raspi.list*

```bash
raspi_list_path='/etc/apt/sources.list.d/raspi.list'

[[ -f "${raspi_list_path}" ]] && sudo sed -r -i '/https?:/{s@https?:@https:@g}' "${raspi_list_path}"
```

### Bootloader EEPROM Upgrade (optional)

[rpi-eeprom](https://github.com/raspberrypi/rpi-eeprom) contains the scripts and pre-compiled binaries used to create the rpi-eeprom package which is used to update the Raspberry Pi 4 bootloader and VLI USB xHCI controller EEPROMs. More descriptions see official documentation [Raspberry Pi 4 boot EEPROM](https://www.raspberrypi.com/documentation/computers/raspberry-pi.html#raspberry-pi-4-boot-eeprom).

<!-- https://www.raspberrypi.org/documentation/hardware/raspberrypi/booteeprom.md -->


```bash
# rpi-eeprom-update  - Checks whether the Raspberry Pi bootloader EEPROM is up-to-date and updates the EEPROM

# reading the current EEPROM version
vcgencmd bootloader_version

# check available update
sudo rpi-eeprom-update

sudo rpi-eeprom-update -a
# sudo reboot
```

<details><summary>Click to expand output</summary>

```sh
# sudo rpi-eeprom-update -a
*** INSTALLING EEPROM UPDATES ***

BOOTLOADER: update available
   CURRENT: Thu  3 Sep 12:11:43 UTC 2020 (1599135103)
    LATEST: Wed 11 Jan 17:40:52 UTC 2023 (1673458852)
   RELEASE: default (/lib/firmware/raspberrypi/bootloader/default)
            Use raspi-config to change the release.

  VL805_FW: Dedicated VL805 EEPROM
     VL805: up to date
   CURRENT: 000138c0
    LATEST: 000138c0
   CURRENT: Thu  3 Sep 12:11:43 UTC 2020 (1599135103)
    UPDATE: Wed 11 Jan 17:40:52 UTC 2023 (1673458852)
    BOOTFS: /boot
Using recovery.bin for EEPROM update

EEPROM updates pending. Please reboot to apply the update.
To cancel a pending update run "sudo rpi-eeprom-update -r".
```

```bash
# vcgencmd bootloader_version
# - old
Sep  3 2020 13:11:43
version c305221a6d7e532693cc7ff57fddfc8649def167 (release)
timestamp 1599135103
update-time 0
capabilities 0x00000000

# - new
2023/05/11 07:26:03
version 4fd8f1f3f7a05f7756edb1d3f15ffd7e428981f5 (release)
timestamp 1683786363
update-time 1692410961
capabilities 0x0000007f
```

</details>


```sh
# disable automatic updates
sudo systemctl mask rpi-eeprom-update

# re-enable automatic update
# sudo systemctl unmask rpi-eeprom-update
```

### Firmware Upgrade (optional)

[rpi-update](https://github.com/Hexxeh/rpi-update) is a tool to get the latest bleeding-edge [firmware][rpi-firmware] and kernel for [Raspberry Pi][raspberrypi]. Other expert options (e.g. *SKIP_KERNEL*) see [rpi-update#options](https://github.com/Hexxeh/rpi-update#options).


```bash
# sudo rpi-update

sudo SKIP_KERNEL=1 rpi-update  # skip kernel update
```

Attention info

```txt
#############################################################
WARNING: This update bumps to rpi-6.1.y linux tree
See: https://forums.raspberrypi.com/viewtopic.php?t=344246

'rpi-update' should only be used if there is a specific
reason to do so - for example, a request by a Raspberry Pi
engineer or if you want to help the testing effort
and are comfortable with restoring if there are regressions.

DO NOT use 'rpi-update' as part of a regular update process.
##############################################################
```

### System Update

```bash
sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get dist-upgrade -y
sudo apt-get autoremove -y
```

Solving problems listed in section [Error Occuring](#error-occuring).

```bash
sudo apt-get install -y libblockdev-crypto2 libblockdev-mdraid2 accountsservice
```

Essential packages

```bash
sudo apt-get install -y bash bash-completion \
                   curl \
                   gawk \
                   git \
                   gnupg2 \
                   vim \
                   vnstat \
                   iperf \
                   jq \
                   exfat-fuse

# (optional) terminal multiplexer
sudo apt-get install -y tmux
[[ -f /usr/share/bash-completion/completions/tmux ]] || sudo curl -fsSL -o /usr/share/bash-completion/completions/tmux https://raw.githubusercontent.com/imomaliev/tmux-bash-completion/master/completions/tmux

# (optional)
sudo apt-get install -y firefox-esr  # firefox web browser
sudo apt-get install -y libreoffice

# vim text editor
sudo apt-get install -y vim
```

Disable swap

Soultion from forum post [Permanently disable swap on Raspbian Buster](https://forums.raspberrypi.com/viewtopic.php?t=244130).

```sh
sudo systemctl disable --now dphys-swapfile.service
```

User Management

```sh
# current login user group info

# id pi
# uid=1000(pi) gid=1000(pi) groups=1000(pi),4(adm),20(dialout),24(cdrom),27(sudo),29(audio),44(video),46(plugdev),60(games),100(users),104(input),106(render),108(netdev),999(spi),998(i2c),997(gpio),117(lpadmin)
uid=1000(pi) gid=1000(pi) groups=1000(pi),4(adm),20(dialout),24(cdrom),27(sudo),29(audio),44(video),46(plugdev),60(games),100(users),102(input),105(render),106(netdev),995(spi),994(i2c),993(gpio),115(lpadmin)

# for new normal user
sudo usermod -a -G adm,dialout,cdrom,sudo,audio,video,plugdev,games,users,input,netdev,gpio,i2c,spi $NEW_USER
```

```sh
# axdsop boost tool intallation, running as normal user (pi)

curl -fsL https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/axdsop_boost_tool.sh | bash -s -- -i

# customsize configuration deployment

bash ~/.config/axdsop/deploy.sh -h
```


## Networking Configuration

Resources

* [Setting up Raspberry Pi WiFi with Static IP on Raspbian Stretch Lite](https://electrondust.com/2017/11/25/setting-raspberry-pi-wifi-static-ip-raspbian-stretch-lite/)
* [How to Setup a Raspberry Pi Static IP Address](https://pimylifeup.com/raspberry-pi-static-ip-address/)
* [Configuring wifi in Linux with wpa_supplicant](https://shapeshed.com/linux-wifi/)


### Wireless Lan

To set up a wireless Lan, please read official documentation [Setting up a wireless LAN via the command line](https://www.raspberrypi.org/documentation/configuration/wireless/wireless-cli.md).

WIFI connection info saved in file */etc/wpa_supplicant/wpa_supplicant.conf*.

Utility `wpa_passphrase` can be used to generate an encrypted PSK.


```bash
# - scan available wireless networks
sudo iwlist wlan0 scan

wpa_supplicant_conf='/etc/wpa_supplicant/wpa_supplicant.conf'
# change your own WIFI ssid and corresponding password
wifi_ssid=''
wifi_ssid_pass=''

# method 1 - prompt password input
# wpa_passphrase "${wifi_ssid}" >> /etc/wpa_supplicant/wpa_supplicant.conf
# wpa_passphrase "${wifi_ssid}" | sudo tee -a /etc/wpa_supplicant/wpa_supplicant.conf > /dev/null

# method 2
sudo tee -a "${wpa_supplicant_conf}" 1> /dev/null << EOF
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
country=US

network={
        ssid="${wifi_ssid}"
        psk="${wifi_ssid_pass}"
        priority=1
        #key_mgmt=WPA-PSK
}

# If has >1 network, just specify 'network={}' with different priority value
#network={
#       ssid=
#       psk=
#       priority=0
#}
EOF

# - reconfigure the interface
sudo wpa_cli -i wlan0 reconfigure

sudo wpa_cli -i wlan0 list_networks
```

If you wanna disable wireless function, please read blog

* [How to disable Wi-Fi on Raspberry Pi? (7 ways, Lite/Desktop)](https://raspberrytips.com/disable-wifi-raspberry-pi/)
* [How to Disable your Raspberry Pi�s Wi-Fi](https://pimylifeup.com/raspberry-pi-disable-wifi/)

```bash
# temporarily  
sudo ifconfig wlan0 down
sudo ifconfig wlan0 up

sudo rfkill block wifi
sudo rfkill unblock wifi
```

### Static IP Setting Up

By default [Raspberry Pi 4][raspberrypi4] gets dynamic IP by DHCP service. If you wanna set up static IP, please read official documentation [TCP/IP networking](https://www.raspberrypi.org/documentation/configuration/tcpip/) or post [StackExchange - How do I set up networking/WiFi/static IP address?](https://raspberrypi.stackexchange.com/questions/37920/how-do-i-set-up-networking-wifi-static-ip-address).

Configuration file is */etc/dhcpcd.conf*.

Network interface name

Type|Interface
---|---
WiFi|wlan0
Ethernet|eth0

Setting up

```bash
dhcp_conf='/etc/dhcpcd.conf'

ip_address='192.168.0.150/24'
ip_router='192.168.0.1'
ip_dns='9.9.9.9 1.1.1.1'

# interface wlan0, eth0
wlan_interface=${wlan_interface:-'wlan0'}
wlan_interface=$(sed -r -n '/INTERFACE/I{s@^[^=]+=@@g;p;q}' /sys/class/net/wl*/uevent 2> /dev/null)
# [[ -n "${interface}" ]] || interface=$(ip route 2> /dev/null | sed -r -n '/default/{s@.*?dev[[:space:]]*([^[:space:]]+).*@\1@p}')

# eth_interface=${eth_interface:-'eth0'}
# eth_interface=$(sed -r -n '/INTERFACE/I{s@^[^=]+=@@g;p;q}' /sys/class/net/eth*/uevent 2> /dev/null)

sudo sed -r -i '/Custom Static IP Start/,/Custom Static IP End/d' "${dhcp_conf}" 2> /dev/null

sudo tee -a "${dhcp_conf}" 1> /dev/null << EOF
# Custom Static IP Start
interface ${wlan_interface}
static ip_address=${ip_address}
static routers=${ip_router}
static domain_name_servers=${ip_dns}
# Custom Static IP End
EOF

sudo systemctl restart dhcpcd.service
# sudo reboot
```

Configuring both *wlan0* and *eth0*.

<details><summary>Click to expand</summary>

```bash
# Custom Static IP Start
interface eth0
static ip_address=192.168.0.150/24
static routers=192.168.0.1
static domain_name_servers=9.9.9.9 1.1.1.1

interface wlan0
static ip_address=192.168.0.152/24
static routers=192.168.0.1
static domain_name_servers=9.9.9.9 1.1.1.1
# Custom Static IP End
```

</details>


## Security

Official documentation [Securing your Raspberry Pi](https://www.raspberrypi.com/documentation/computers/configuration.html#securing-your-raspberry-pi)

<!-- https://www.raspberrypi.org/documentation/configuration/security.md -->

* Change your default password
* Changing your username
* Make `sudo` require a password
* Ensure you have the latest security fixes
* Improving SSH security
* Install a firewall
* Installing fail2ban


### UFW Firewall

[ufw and iptables on buster](https://raspberrypi.stackexchange.com/questions/100598/ufw-and-iptables-on-buster)

```bash
update-alternatives --list iptables
# /usr/sbin/iptables-legacy
# /usr/sbin/iptables-nft

sudo update-alternatives --set iptables /usr/sbin/iptables-legacy
# update-alternatives: using /usr/sbin/iptables-legacy to provide /usr/sbin/iptables (iptables) in manual mode

sudo apt-get install -y ufw
sudo ufw default deny incoming
sudo ufw default allow outgoing

# Disable ipv6
[[ -f /etc/default/ufw ]] && sudo sed -r -i '/^IPV6=/{s@(IPV6=).*@\1no@g;}' /etc/default/ufw 2> /dev/null

# SSH
sudo ufw limit ssh  # or ufw allow ssh
# sudo ufw limit from 192.168.0.0/24 proto tcp to any port 22 comment "SSH Rule"  # limit/allow

sudo ufw --force enable # non-interactive
# sudo ufw enable # interactive  Command may disrupt existing ssh connections. Proceed with operation (y|n)?

sudo ufw status verbose

sudo systemctl enable --now ufw.service
sudo systemctl is-enabled ufw.service
```

## Optimization

Tutorials

* https://feriman.com/raspberry-pi-optimization-hardware-and-software/

Check boot time

```sh
# systemd-analyze --system
Startup finished in 2.133s (kernel) + 14.522s (userspace) = 16.656s
graphical.target reached after 8.515s in userspace

# systemd-analyze critical-chain

# systemd-analyze blame
```

Disable bluetooth service *hciuart.service*

* https://forums.raspberrypi.com/viewtopic.php?t=286786
* https://di-marco.net/blog/it/2020-04-18-tips-disabling_bluetooth_on_raspberry_pi/

```sh
sudo cp -p /boot/config.txt{,.origin}

sudo tee -a /boot/config.txt 1> /dev/null << EOF
# Disable Bluetooth
dtoverlay=disable-bt

EOF

sudo systemctl disable hciuart.service
sudo systemctl disable bluealsa.service
sudo systemctl disable bluetooth.service

# Remove package BlueZ  https://wiki.archlinux.org/title/Bluetooth
sudo apt-get purge bluez -y
sudo apt-get autoremove -y
```

Overclocking

* https://magpi.raspberrypi.com/articles/how-to-overclock-raspberry-pi-4
* https://www.makeuseof.com/how-to-overclock-raspberry-pi-4/
* https://beebom.com/how-overclock-raspberry-pi-4/

```sh
sudo tee -a /boot/config.txt 1> /dev/null << EOF
# Overclocking for Raspiberry4 4GB
over_voltage=6
arm_freq=2100
gpu_freq=750
#force_turbo=1

EOF
```


## Error Occuring

### bcmgenet

>raspberrypi kernel: [    0.326157] bcmgenet fd580000.genet: failed to get enet clock
>
>raspberrypi kernel: [    0.326190] bcmgenet fd580000.genet: failed to get enet-wol clock
>
>raspberrypi kernel: [    0.326206] bcmgenet fd580000.genet: failed to get enet-eee clock

Same problem in [Ethernet sometimes fails to start (no carrier) on Raspberry Pi 4 on kernel 4.19.66-v7l+](https://github.com/raspberrypi/linux/issues/3195), find no usable solution.


### udisksd

>raspberrypi udisksd[364]: failed to load module crypto: libbd_crypto.so.2: cannot open shared object file: No such file or directory
>
>raspberrypi udisksd[364]: failed to load module mdraid: libbd_mdraid.so.2: cannot open shared object file: No such file or directory

Solution sees [udisks2 service complains about missing libblockdev plugins](https://github.com/pop-os/desktop/issues/22).

```bash
sudo apt-get install -y libblockdev-crypto2 libblockdev-mdraid2
```

### lightdm

>raspberrypi lightdm[446]: Error getting user list from org.freedesktop.Accounts: GDBus.Error:org.freedesktop.DBus.Error.ServiceUnknown: The name org.freedesktop.Accounts was not provided by any .service files
>
>raspberrypi lightdm[446]: Could not enumerate user data directory /var/lib/lightdm/data: Error opening directory '/var/lib/lightdm/data': No such file or directory

Solution sees [XFCE4 broken after update to 4.16](https://www.reddit.com/r/debian/comments/8vj576/xfce4_broken_after_update_to_416/), [lightdm: Error getting user list from org.freedesktop.Accounts](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=837979).

```bash
sudo apt-get install -y accountsservice
```


## Change Log

* Sep 11, 2019 Wed 21:51 ET
  * first draft
* Sep 16, 2019 Mon 15:26 ET
  * add project *BabyCam - Logitech C920 PRO HD Webcam*
* Oct 10, 2019 Thu 08:30 ET
  * Upgrade version to `2019-09-26`
* Oct 11, 2019 Fri 14:50 ET
  * Add available North America mirror lists
* Oct 27, 2019 Sun 16:10 ET
  * Add `rpi-update` to upgrade firmware.
* Oct 27, 2019 Sun 21:06 ET
  * Add error occuring
* Oct 28, 2019 Mon 09:37 ET
  * Seperating Raspbian operation from README
* Nov 14, 2019 Thu 13:09 ET
  * Add vimrc, axdsop boost script, ufw rule set
* Mar 06, 2020 Fri 18:58 ET
  * Upgrade version to `2020-02-13`
* May 28, 2020 Thu 08:18 ET
  * Upgrade version to `2020-05-27`, Raspberry Pi launch [8GB variant](https://www.raspberrypi.org/blog/8gb-raspberry-pi-4-on-sale-now-at-75/), Raspbian rename to *Raspberry Pi OS*, introduce [64 bits beta test version](https://www.raspberrypi.org/forums/viewtopic.php?f=117&t=275370).
* Sep 02, 2020 Wed 07:55 ET
  * Add image download link and verification section
* Oct 23, 2020 Fri 10:15 ET
  * Update network configuration section
* Dec 05, 2020 Sat 14:07 ET
  * Update extract method for new designed download page ([old](https://www.raspberrypi.org/downloads/raspbian/), [new](https://www.raspberrypi.org/software/operating-systems/))
* May 08, 2021 Sat 13:35 ET
  * Add GPG verification
* Mar 10, 2022 Thu 20:17 ET
  * Add 64 bit image list
* Aug 18, 2023 Fri 22:45 ET
  * update doc


[raspberrypi]:https://www.raspberrypi.org/
[raspberrypi4]:https://www.raspberrypi.org/products/raspberry-pi-4-model-b/
[noobs]:https://www.raspberrypi.org/downloads/noobs/
[raspbian]:https://www.raspberrypi.org/software/operating-systems/
<!-- [raspbian]:https://www.raspberrypi.org/downloads/raspbian/ -->
[raspbianmirrors]:https://www.raspbian.org/RaspbianMirrors
[rpi-firmware]:https://github.com/Hexxeh/rpi-firmware "Firmware files for the Raspberry Pi"


<!-- End -->