# Setting Up Operating System - Manjaro (Not Complete)

This note records how to use [Manjaro][manjaro] image to install operating system on [Raspberry Pi 4][raspberrypi4].

## TOC

1. [Getting Manjaro](#getting-manjaro)  
1.1 [Writing Bootable Drive](#writing-bootable-drive)  
2. [Installation](#installation)  
3. [Postinstallation](#postinstallation)  
4. [Change Log](#change-log)  


## Getting Manjaro

See note [Getting Manjaro](/GNULinux/Distros/Manjaro/Notes/1-0_GettingManjaro.md).

### Writing Bootable Drive

Official document [Writing to a USB Stick in Linux](https://wiki.manjaro.org/index.php?title=Burn_an_ISO_File#Burning_to_CD.2FDVD_in_Linux).


```bash
device_path='/dev/sdX' # via lsblk, fdisk -l, e.g. /dev/sda, /dev/sdb
xz_image_name='Manjaro-ARM-kde-plasma-rpi4-20.10.img.xz' # locate in ~/Downloads/

img_name="${xz_image_name%.*}"

# - method 1
# decompress image .img from img.xz
[[ -f "${img_name}" ]] || unxz -d -k -v "${xz_image_name}"
# copying the image to the SD card
sudo dd if="${img_name}" of="${device_path}" bs=4M status=progress oflag=sync # conv=fsync

# - method 2
# Copying a compressed image to the SD card directly
# unxz -d -k -v "${xz_image_name}" | sudo dd of="${device_path}" bs=4M status=progress oflag=sync # conv=fsync
```


## Installation

Inserting SD card into motherboard slot on [Raspberry Pi 4][raspberrypi4], powering on, the `Finish Manjaro ARM Install` window will appear in the centre of the screen.

You're required to input the following info.

Assuming new normal login user name is `pi`.

Item|Value
---|---
New user|`pi`
Additional groups|
Keyboadr Layout|`us`
Timezone|`America/New_York`
Language|`en_US.UTF-8`


## Postinstallation

See note [Postinstallation Setting Up](/GNULinux/Distros/Manjaro/Notes/1-1_PostinstallationSettingUp.md).


## Change Log

* Oct 28, 2019 Mon 20:17 ET
  * first draft
* Oct 29, 2019 Tue 21:03 ET
  * Add postinstallation operation
* Apr 16, 2020 Thu 19:35 ET
  * Move postinstallation operation into seperate note


[raspberrypi]:https://www.raspberrypi.org/
[raspberrypi4]:https://www.raspberrypi.org/products/raspberry-pi-4-model-b/
[archlinux]:https://www.archlinux.org "A lightweight and flexible Linux® distribution"
[manjaro]:https://manjaro.org/ "Manjaro - enjoy the simplicity"


<!-- End -->
