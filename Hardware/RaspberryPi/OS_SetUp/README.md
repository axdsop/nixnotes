# Raspberry Pi OS Setting Up

The followings are official documentations

* [Installing operating system images](https://www.raspberrypi.org/documentation/installation/installing-images/README.md)
* [Installing operating system images on Linux](https://www.raspberrypi.org/documentation/installation/installing-images/linux.md)


## List

No.|Distros|Operation Note
---|---|---
0|*Docker*|[Docker Note](./Docker.md "Docker")
1|Raspbian|[Raspbian Note](./Raspbian.md "Raspbian")
2|Manjaro|[Manjaro Note](./Manjaro.md "Manjaro")
3|Ubuntu|[Ubuntu Note](./Ubuntu.md "Ubuntu")


## Change Log

* Oct 28, 2019 Mon 09:43 ET
  * first draft
* Oct 29, 2019 Tue 23:04 ET
  * Add Manjaro


[raspberrypi]:https://www.raspberrypi.org/
[raspberrypi4]:https://www.raspberrypi.org/products/raspberry-pi-4-model-b/
[raspbian]:https://www.raspberrypi.org/downloads/raspbian/


<!-- End -->
