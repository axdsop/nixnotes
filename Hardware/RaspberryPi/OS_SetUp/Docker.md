# Docker On Raspberry Pi 4

Official documentation site: https://docs.docker.com

## TOC

1. [Docker](#docker)  
1.1 [Install](#install)  
2. [Docker Compose](#docker-compose)  
2.1 [Install](#install-1)  
2.1.1 [Via docker](#via-docker)  
2.1.2 [Via pip](#via-pip)  
2.1.3 [Via Shell script](#via-shell-script)  
3. [Example](#example)  
4. [Change Log](#change-log)  


## Docker

[Docker][docker] offical repository provides pre-built binary packages (.deb, .rpm), however it just support platforms CentOS, Debian, Fedora, Ubuntu, Raspbian.

Official provides convenience script <https://get.docker.com> to install edge version of [Docker][docker] Engine.

### Install

```bash
# https://docs.docker.com/engine/install/ubuntu/
cd /tmp
curl -fsSL https://get.docker.com -o get-docker.sh
sudo bash get-docker.sh

# add login user into group docker
sudo usermod -aG docker $USER
```


## Docker Compose

[Docker Compose][compose] official documentation https://docs.docker.com/compose/.

It is normally installed from pre-built binaries, downloaded from the GitHub release page for the project. Sadly, those are not available for the ARM architecture.


### Install

#### Via docker

[docker/compose](https://hub.docker.com/r/docker/compose)

```bash
docker pull docker/compose:1.27.2
```

#### Via pip

```bash
# Install required packages
sudo apt update
sudo apt install -y python3-pip libffi-dev

# Install Docker Compose from pip (using Python3)
# This might take a while
sudo pip3 install docker-compose

$ sudo apt install libffi-dev libssl-dev python3 python3-pip
$ sudo apt remove python-configparser
$ sudo pip3 install docker-compose
```


#### Via Shell script

This script downloads pre-built binary file from its [GitHub release](https://github.com/docker/compose/releases) page.

Note: just works for Linux (x86_64).

```bash
#!/usr/bin/env bash

# https://docs.docker.com/compose/install/
# https://docs.docker.com/compose/completion/
# https://api.github.com/repos/docker/compose/releases/latest

if [[ $(uname -s) == 'Linux' && $(uname -m) == 'x86_64' ]]; then
  download_command='wget -qO-'
  [[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && download_command='curl -fsSL'
else
  echo 'Sorry, this script just works for Linux (x86_64).'
  exit 0
fi

compose_release_info=$($download_command https://api.github.com/repos/docker/compose/releases/latest | sed -r -n '1,/"assets"/{/"(name|published_at)":/{p}}; /"browser_download_url":.*'$(uname -s)'.*?'$(uname -m)'/{p}' | sed -r -n 's@^[^:]*:[[:space:]]*@@g;s@,$@@g;s@"@@g;s@T[[:digit:]:]+.*Z?$@@g;p' | sed ':a;N;$!ba;s@\n@|@g')
# 1.27.0|2020-09-07|https://github.com/docker/compose/releases/download/1.27.0/docker-compose-Linux-x86_64|https://github.com/docker/compose/releases/download/1.27.0/docker-compose-Linux-x86_64.sha256

compose_release_version=$(cut -d\| -f 1 <<< "${compose_release_info}")
compose_release_date=$(cut -d\| -f 2 <<< "${compose_release_info}")
compose_release_binary=$(cut -d\| -f 3 <<< "${compose_release_info}")
compose_release_binary_sha=$(cut -d\| -f 4 <<< "${compose_release_info}")

echo "Latest available release version ${compose_release_version} (${compose_release_date})."

# - Version comparasion
compose_local_version=${compose_local_version:-}
compose_local_version=$(docker-compose --version 2> /dev/null | sed -r -n 's@.*version[[:space:]]*([^,]+).*@\1@g;p')

if [[ "${compose_local_version}" == "${compose_release_version}" ]]; then
  # docker-compose version 1.27.0, build 980ec85b
  echo "Local version is the latest version."
  exit 0
else
  echo "Operation processing ..."
fi

# - Download latest docker-compose
cd /tmp
$download_command "${compose_release_binary}" > "${compose_release_binary##*/}"

# SHA256 digest check
[[ -n $($download_command "${compose_release_binary_sha}" | sha256sum -c - | sed -n '/:.*OK/{p}') ]] || exit
# docker-compose-Linux-x86_64: OK

sudo mv -f "${compose_release_binary##*/}" /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
# sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

# - Download docker-compose command completion
if [[ $SHELL =~ \/bash$ ]]; then
  bash_completion_dir=${bash_completion_dir:-'/usr/share/bash-completion/completions'}
  [[ -d /etc/bash_completion.d/ ]] && bash_completion_dir='/etc/bash_completion.d'

  [[ -d "${bash_completion_dir}" ]] && $download_command https://raw.githubusercontent.com/docker/compose/${compose_release_version}/contrib/completion/bash/docker-compose | sudo tee "${bash_completion_dir}"/docker-compose 1> /dev/null
  sudo chmod 644 "${bash_completion_dir}"/docker-compose
fi

docker-compose --version
```

## Example

Running [Nextcloud][nextcloud]

```bash
sudo mkdir -pv /data/nextcloud
sudo chown -R www-data:www-data /data/nextcloud
sudo chmod 750 /data/nextcloud
sudo usermod -a -G www-data $USER

# docker run -d -p 8080:80 --name nextcloud --restart unless-stopped -v nextcloud:/var/www/html nextcloud
# data save path: /var/lib/docker/volumes/nextcloud

docker run -d -p 8080:80 --name nextcloud --restart unless-stopped -v nextcloud:/var/www/html -v /data/nextcloud:/var/www/html/data nextcloud

# sudo ufw allow proto tcp from 192.168.0.0/24 to any port 8080
```


## Change Log

* Sep 09, 2020 Wed 06:05 ET
  * First draft


[docker]:https://www.docker.com "Empowering App Development for Developers | Docker"
[compse]:https://github.com/docker/compose "Define and run multi-container applications with Docker"
[nextcloud]:https://nextcloud.com/ "Nextcloud is the most deployed on-premises file share and collaboration platform."

<!-- End -->