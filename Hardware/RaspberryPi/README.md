# Raspberry Pi Operation Notes

The [Raspberry Pi][raspberrypi] is a series of small single-board computers developed by the [Raspberry Pi Foundation](https://en.wikipedia.org/wiki/Raspberry_Pi_Foundation).


## TOC

1. [OS Setting Up](#os-setting-up)  
1.1 [OS List](#os-list)  
2. [Preparation](#preparation)  
3. [Official Resources](#official-resources)  
3.1 [Unofficial Resources](#unofficial-resources)  
4. [Personal Project](#personal-project)  
5. [Change Log](#change-log)  


The latest version is [Raspberry Pi 4][raspberrypi4].

<!-- ![raspberrypi 4 borad](https://www.raspberrypi.org/homepage-9df4b/static/pi4-labelled@2x-0894491e6de97a282dde5a5010cc8b61.png) -->

![RaspberryPI 4 Borad](https://www.raspberrypi.org/homepage-9df4b/static/raspberry-pi-4-labelled@2x-c1a040c7511610e7274e388432a458c4.png)


## OS Setting Up

The followings are official documentations

* [Installing operating system images](https://www.raspberrypi.org/documentation/installation/installing-images/README.md)
* [Installing operating system images on Linux](https://www.raspberrypi.org/documentation/installation/installing-images/linux.md)

### OS List

No.|Distros|Operation Note
---|---|---
0|[*Docker*][docker]|[Docker Note](./OS_SetUp/Docker.md "Docker")
1|[Raspbian][raspbian]|[Raspbian Note](./OS_SetUp/Raspbian.md "Raspbian")
2|[Manjaro][manjaro]|[Manjaro Note](./OS_SetUp/Manjaro.md "Manjaro")
3|[Ubuntu][ubuntu]|[Ubuntu Note](./OS_SetUp/Ubuntu.md "Ubuntu")


## Preparation

Essential accessories to [set up](https://www.raspberrypi.org/documentation/setup/) [Raspberry Pi][raspberrypi]:

* [Raspberry Pi 4][raspberrypi4]
* [SD Cards](https://www.raspberrypi.org/documentation/installation/sd-cards.md) (>=8GB)
* Micro-HDMI to HDMI Cable
* [Keyboard](https://www.raspberrypi.org/products/raspberry-pi-keyboard-and-hub/) and [Mouse](https://www.raspberrypi.org/products/raspberry-pi-mouse/)
* micro USB [power supply](https://www.raspberrypi.org/products/type-c-power-supply/)


## Official Resources

Documentation

* [Get started with Raspberry Pi](https://projects.raspberrypi.org/en/pathways/getting-started-with-raspberry-pi)
* [Raspberry Pi Documentation](https://www.raspberrypi.org/documentation/)

Books & magazines

* [The MagPi](https://www.raspberrypi.org/magpi/issues/)
* [HackSpace](https://hackspace.raspberrypi.org/issues)
* [Wireframe](https://wireframe.raspberrypi.org/issues)

The followings official documentations teach you how to set up your Raspberry Pi

* [Setting up your Raspberry Pi](https://projects.raspberrypi.org/en/projects/raspberry-pi-setting-up)
* [Using your Raspberry Pi](https://projects.raspberrypi.org/en/projects/raspberry-pi-using)


### Unofficial Resources

* [Pi My Life Up](https://pimylifeup.com/)


## Change Log

* Sep 11, 2019 Wed 21:51 ET
  * first draft
* Sep 16, 2019 Mon 15:26 ET
  * add project *BabyCam - Logitech C920 PRO HD Webcam*
* Oct 10, 2019 Thu 08:30 ET
  * Upgrade version to `2019-09-26`
* Oct 11, 2019 Fri 14:50 ET
  * Add available North America mirror lists
* Oct 27, 2019 Sun 16:10 ET
  * Add `rpi-update` to upgrade firmware.
* Oct 27, 2019 Sun 21:06 ET
  * Add error occuring
* Oct 28, 2019 Mon 09:17 ET
  * Seperating Raspbian operation from README
* Oct 29, 2019 Tue 23:04 ET
  * Add Manjaro


[raspberrypi]:https://www.raspberrypi.org/
[raspberrypi4]:https://www.raspberrypi.org/products/raspberry-pi-4-model-b/
[raspbian]:https://www.raspberrypi.org/downloads/raspbian/
[manjaro]:https://manjaro.org/ "Manjaro - enjoy the simplicity"
[ubuntu]:https://ubuntu.com/ "The leading operating system for PCs, IoT devices, servers and the cloud."
[docker]:https://www.docker.com "Empowering App Development for Developers | Docker"

<!-- End -->