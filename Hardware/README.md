# Hardware Intro

This section lists hardware I owned include laptop and [Raspberry Pi][raspberrypi].

## Raspberry Pi

Raspberry Pi [note](./RaspberryPi/)

1. [OS Setting Up](./RaspberryPi/OS_SetUp/)

## Laptops

Brand|Own Date|Note
---|---|---
HUAWEI MateBook X Pro (i7 MX150)|May 16, 2019|[Spec](./Laptop/MBXP/)


## Change Log

* Apr 15, 2021 Thu 08:40 ET
  * first draft

[raspberrypi]:https://www.raspberrypi.org/

<!-- End -->