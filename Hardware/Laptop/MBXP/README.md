# HUAWEI MateBook X Pro

My current laptop is HUAWEI [MateBook X Pro][mbxp] (i7 MX150) which bought at [Microsoft Store](https://www.microsoft.com/en-us/store/b/home) on May 16, 2019. At the same day, the U.S. government added [Huawei][huawei] to [BIS Entity List](https://www.federalregister.gov/documents/2019/05/21/2019-10616/addition-of-entities-to-the-entity-list). A few days later, Microsoft [removed Huawei laptop from its  store](https://www.theverge.com/2019/5/21/18634240/microsoft-huawei-matebook-x-pro-laptop-store-windows-ban-no-comment) silently.

Whatever, it is a perfect and fantastic laptop. Here I'll call it `MBXP`.

It is shipped with Microsoft [Windows 10][windows] by default. Since [Huawei][huawei] isn't a [vendor](https://fwupd.org/lvfs/vendorlist) of [LVFS][lvfs] team, BIOS can't not be [updated](https://consumer.huawei.com/en/support/how-to/detail-troubleshooting/?resourceId=en-us00698596) by service *fwupd* on GNU/Linux. I'll preserve existed [Windows 10][windows] operating system, install GNU/Linux as a dual boot.

Version|Official Page
---|---
2018|~~[Features](https://consumer.huawei.com/en/laptops/matebook-x-pro/)~~<br/>~~[Specifications](https://consumer.huawei.com/en/laptops/matebook-x-pro/specs/)~~<br/>[Product Support](https://consumer.huawei.com/en/support/laptops/matebook-x-pro/)

<!-- ## TOC -->

## Note

1. MBXP [Product History](./Notes/0_ProductHistory.md)
2. [MBXP 2018](./Notes/1_MBXP_2018.md)


## Change Log

* Jun 25, 2019 14:55 Tue ET
  * first draft
* Oct 29, 2019 15:59 Tue ET
  * Update system info json output
* Apr 13, 2020 13:28 Mon ET
  * Update BIOS online version from `1.28` to `1.29`
* Oct 09, 2020 14:30 Fri ET
  * Note rewrite, add MBXP 2019, 2020 snapshot, add table collapse show
* Nov 22, 2020 08:00 Sun ET
  * Add battery protection
* Apr 15, 2021 10:22 Thu ET
  * Orchestrate sections into seperate notes


[huawei]:https://www.huawei.com "Huawei - Building a Fully Connected, Intelligent World"
[mbxp]:https://consumer.huawei.com/en/laptops/matebook-x-pro/
[windows]:https://www.microsoft.com/en-us/windows


<!-- End -->