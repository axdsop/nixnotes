# HUAWEI MateBook X Pro Product History

## TOC

1. [Official Page](#official-page)  
1.1 [Product History](#product-history)  
2. [Snapshot](#snapshot)  
2.1 [Features](#features)  
2.2 [Specifications](#specifications)  
3. [Change Log](#change-log)  


## Official Page

**MateBook X Pro** (i7 MX150) is the first generation aka *HUAWEI MateBook X Pro 2018*. ~~For US consumer, official site page is [matebook-x-pro](https://consumer.huawei.com/us/tablets/matebook-x-pro/). (deadlink Oct 09, 2020) ~~

### Product History

Version|Official Page|Reviews
---|---|---
2018|~~[Features](https://consumer.huawei.com/en/laptops/matebook-x-pro/)~~<br/>~~[Specifications](https://consumer.huawei.com/en/laptops/matebook-x-pro/specs/)~~<br/>[Product Support](https://consumer.huawei.com/en/support/laptops/matebook-x-pro/)|[PCMag](https://www.pcmag.com/reviews/huawei-matebook-x-pro)<br/>[CNET](https://www.cnet.com/reviews/huawei-matebook-x-pro-review/)<br/>[DigitalTrends](https://www.digitaltrends.com/laptop-reviews/huawei-matebook-x-pro-review/)
2019|[Features](https://consumer.huawei.com/en/laptops/matebook-x-pro-2019/)<br/>[Specifications](https://consumer.huawei.com/en/laptops/matebook-x-pro-2019/specs/)<br/>[Product Support](https://consumer.huawei.com/en/support/laptops/matebook-x-pro-2019/)|[PCMag](https://www.pcmag.com/reviews/huawei-matebook-x-pro-2019)<br/>[DigitalTrends](https://www.digitaltrends.com/laptop-reviews/huawei-matebook-x-pro-2019-review/)
2020|[Features](https://consumer.huawei.com/en/laptops/matebook-x-pro-2020/)<br/>[Specifications](https://consumer.huawei.com/en/laptops/matebook-x-pro-2020/specs/)<br/>[Product Support](https://consumer.huawei.com/en/support/laptops/matebook-x-pro-2020/)|[PCMag](https://www.pcmag.com/reviews/huawei-matebook-x-pro-2020)
2021|[Features](https://consumer.huawei.com/en/laptops/matebook-x-pro-2021/)<br/>[Specifications](https://consumer.huawei.com/en/laptops/matebook-x-pro-2021/specs/)<br/>Product Support|[tom's Hardware](https://www.tomshardware.com/reviews/huawei-matebook-x-pro-2021)<br/>[Android Authority](https://www.androidauthority.com/huawei-matebook-x-pro-2021-review-1211991/)

</details>


## Snapshot

### Features

<details><summary>Click to expand MBXP</summary>

![MBXP Features](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/Laptop/MBXP/MBXP_Features.jpg)

</details>

<details><summary>Click to expand MBXP 2019</summary>

![MBXP 2019 Features](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/Laptop/MBXP/MBXP_2019_Features.jpg)

</details>

<details><summary>Click to expand MBXP 2020</summary>

![MBXP 2020 Features](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/Laptop/MBXP/MBXP_2020_Features.jpg)

</details>

### Specifications


<details><summary>Click to expand MBXP</summary>

![MBXP Specifications](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/Laptop/MBXP/MBXP_Specifications.png)

</details>

<details><summary>Click to expand MBXP 2019</summary>

![MBXP 2019 Specifications](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/Laptop/MBXP/MBXP_2019_Specifications.png)

</details>

<details><summary>Click to expand MBXP 2020</summary>

![MBXP 2020 Specifications](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/Laptop/MBXP/MBXP_2020_Specifications.png)

</details>

<details><summary>Click to expand MBXP 2021</summary>

![MBXP 2020 Specifications](https://gitlab.com/axdsop/nixnotes/raw/image/blob-image/Laptop/MBXP/MBXP_2021_Specifications.png)

</details>


## Change Log

* Jun 25, 2019 14:55 Tue ET
  * first draft
* Apr 15, 2021 09:36 Thu ET
  * move product history section into seperate note


[huawei]:https://www.huawei.com "Huawei - Building a Fully Connected, Intelligent World"

<!-- End -->