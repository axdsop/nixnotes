# Ubuntu Post-installation Optimization

This note will focus on system update.

If package *sudo* isn't installed or current login user isn't in *sudo* group, please execute `su root` to change to user *root*. Then executing the following commands.

## TOC

1. [APT](#apt)  
1.1 [Respotirory Configuration](#respotirory-configuration)  
1.2 [System Update](#system-update)  
2. [Package Management](#package-management)  
2.1 [Packages Install](#packages-install)  
2.1.1 [sudo](#sudo)  
2.1.2 [ufw](#ufw)  
2.1.3 [Other](#other)  
2.2 [Package Remove](#package-remove)  
2.2.1 [Snaps](#snaps)  
2.2.2 [Apport](#apport)  
3. [Service Management](#service-management)  
3.1 [systemd-analyze tool](#systemd-analyze-tool)  
3.2 [Service Disable](#service-disable)  
3.3 [Service Modify](#service-modify)  
3.3.1 [systemd-resolved](#systemd-resolved)  
4. [Power Management](#power-management)  
4.1 [TLP](#tlp)  
4.2 [CPU Frequency Scaling](#cpu-frequency-scaling)  
4.3 [Touch Screen Disable](#touch-screen-disable)  
4.3.1 [Temporarily](#temporarily)  
4.3.2 [Permanently](#permanently)  
5. [GRUB](#grub)  
6. [Log Management](#log-management)  
6.1 [Systemd Journal](#systemd-journal)  
7. [Reboot](#reboot)  
8. [GNOME Management](#gnome-management)  
8.1 [Essential Packages](#essential-packages)  
8.2 [GNOME Keyring](#gnome-keyring)  
8.3 [GNOME Pinentry](#gnome-pinentry)  
8.4 [gsettings Configuration](#gsettings-configuration)  
9. [Change Log](#change-log)  


## APT

Ubuntu has several [package management](https://ubuntu.com/server/docs/package-management) tool, such as `apt`, `aptitude`, `dpkg`.

The `apt` command is a powerful command-line tool, which works with Ubuntu’s **Advanced Packaging Tool** (APT).

### Respotirory Configuration

Configuration of the Advanced Packaging Tool (APT) system repositories is stored in the */etc/apt/sources.list* file and the */etc/apt/sources.list.d* directory.

For Pop!_OS

```bash
apt_source_dir=='/etc/apt/sources.list.d'

# pop-os-apps.sources
sed -r -i '/URIs:/{s@http:@https:@g}' "${pop-os-apps.sources}/pop-os-apps.sources" 2> /dev/null
# https://apt.pop-os.org/proprietary

# system76-ubuntu-pop-groovy.list
# http://ppa.launchpad.net/system76/pop/ubuntu/

# system.sources
sed -r -i '/https?:/{s@(.*?)https?:[^[:space:]]+(.*)$@\1https://mirrors.xtom.com/ubuntu/\2@g}' "${pop-os-apps.sources}/system.sources" 2> /dev/null
# http://us.archive.ubuntu.com/ubuntu/ -> https://mirrors.xtom.com/ubuntu/
```

### System Update

```bash
# clean package cache
apt -yq clean all

# fetch package information
apt -yq update

# system upgrade
apt -yq upgrade
apt -yq dist-upgrade
```

## Package Management

### Packages Install

#### sudo

sudo privilege (need logout and relogin to make it effect).

```bash
# sudo - Provide limited super user privileges to specific users

default_sudoer_path='/etc/sudoers'

[[ -f "${default_sudoer_path}" ]] || apt install -y sudo
[[ -f "${default_sudoer_path}".old ]] || cp "${default_sudoer_path}"{,.old}

# set no password
if [[ -f "${default_sudoer_path}" ]]; then
    # - For using group 'sudo' (Debian/Ubuntu)
    sed -r -i 's@#*[[:space:]]*(%sudo[[:space:]]+ALL=\(ALL:ALL\)[[:space:]]+ALL)@# \1@;/%sudo ALL=NOPASSWD:ALL/d;/group sudo/a %sudo ALL=NOPASSWD:ALL,!/bin/su' "${default_sudoer_path}"

    # - For using group 'wheel'
    # sudo ccccccktlejguvhcbgldejjrfefufdbejfhvfjegvhin
    # sudo sed -r -i 's@#*[[:space:]]*(%wheel[[:space:]]+ALL=\(ALL\)[[:space:]]+ALL)@# \1@;s@#*[[:space:]]*(%wheel[[:space:]]+ALL=\(ALL\)[[:space:]]+NOPASSWD: ALL).*@\1,!/bin/su@' "${default_sudoer_path}"
fi
```

#### ufw

UFW Firewall

```bash
apt install -y ufw
ufw default deny incoming
ufw default allow outgoing

# Disable ipv6
# [[ -f /etc/default/ufw ]] && sed -r -i '/^IPV6=/{s@(IPV6=).*@\1no@g;}' /etc/default/ufw 2> /dev/null

# If allow SSH
# ufw allow ssh
# ufw allow from 192.168.0.0/24 to any port 22
# ufw limit from 192.168.0.0/24 to any port 22

ufw --force enable # non-interactive
# ufw enable # interactive  Command may disrupt existing ssh connections. Proceed with operation (y|n)?

systemctl enable ufw.service
systemctl is-enabled ufw.service
ufw status verbose
```


#### Other

Essential Packages

```bash
# apt-transport-https - transitional package for https support
apt install -y apt-transport-https

# bash - GNU Bourne Again SHell
# bash-completion - programmable completion for the bash shell
apt install -y bash bash-completion

# curl - command line tool for transferring data with URL syntax
apt install -y curl

# gawk - GNU awk, a pattern scanning and processing language
# sed - GNU stream editor for filtering/transforming text
apt install -y gawk sed

# git - the stupid content tracker
apt install -y git

# openssh-client - secure shell (SSH) client, for secure access to remote machines
apt install -y openssh-client

# gnupg2 - GNU privacy guard - a free PGP replacement (dummy transitional package)
apt install -y gnupg2
```

Optional packagess

```bash
# vim-runtime - Vi IMproved - Runtime files
# neovim - heavily refactored vim fork
apt install -y vim
# apt install -y neovim

# exfat-fuse - read and write exFAT driver for FUSE
apt install -y exfat-fuse

# tmux - terminal multiplexer
apt install -y tmux
# https://aur.archlinux.org/packages/tmux-bash-completion-git/
[[ -f /usr/share/bash-completion/completions/tmux ]] || sudo curl -fsSL -o /usr/share/bash-completion/completions/tmux https://raw.githubusercontent.com/imomaliev/tmux-bash-completion/master/completions/tmux

# Font CJK
# fonts-hanazono - Japanese TrueType mincho font by KAGE system and FontForge
apt install -y fonts-hanazono  # 花園明朝(HanaMin) or ttf-hanazono

# Input Method
apt install -y ibus ibus-rime

# parallel - build and execute command lines from standard input in parallel
apt install -y parallel
echo 'will cite' | parallel --citation 2> /dev/null
```

### Package Remove

To auto remove unneeded packages

```bash
# r: the package was marked for removal
# c: the configuration files are currently present in the system
dpkg --list 2> /dev/null | sed -r -n '/^rc/{s@^rc[[:space:]]*([^[:space:]]+).*@\1@g;p}' | xargs apt purge -y

# remove unneeded packages
apt -yq autoremove
```

#### Snaps

Snaps are app packages for desktop, cloud and IoT that are easy to install, secure, cross-platform and dependency-free.

* *snap* is both the command line interface and the application package format
* *snapd* is the background service that manages and maintains your snaps


```bash
# https://ubuntu.com/blog/tag/snapd
# https://ubuntu.com/tutorials/create-your-first-snap
# https://snapcraft.io/docs

# remove snapd: gnome-software-plugin-snap* snapd*
# snap gnome list: gnome-calculator gnome-characters gnome-logs gnome-system-monitor

# gnome-calculator - GNOME desktop calculator
# gnome-characters - character map application
# gnome-logs - viewer for the systemd journal.
# gnome-system-monitor - Process viewer and system resource monitor for GNOME

if [[ -n $(snap --version 2> /dev/null) ]]; then
    l_snap_gnome_list=''
    l_snap_gnome_list=$(snap list 2> /dev/null | sed -r -n '/^gnome-[^[:digit:]]+/{s@^([^[:space:]]+).*$@\1@g;p}' | sed ':a;N;$!ba;s@\n@ @g')
    if [[ -n "${l_snap_gnome_list}" ]]; then
        echo 'Remove service snapd'
        snap remove ${l_snap_gnome_list}
        apt -y purge snapd
        apt -y install ${l_snap_gnome_list}
        apt -y autoremove 1> /dev/null
    fi
fi
```

#### Apport

Apport is the reporting system for crashes and failures in Ubuntu.

```bash
# https://wiki.ubuntu.com/Apport
apt -y purge apport
```


## Service Management

### systemd-analyze tool

*systemd-analyze* is an analyze and debug system manager

```bash
# print the time spent in the kernel before userspace has been reached, the time spent in the initial RAM disk (initrd) before normal system userspace has been reached, and the time normal system userspace took to initialize.
systemd-analyze time

# print a list of all running units, ordered by the time they took to initialize.
systemd-analyze blame

# print a tree of the time-critical chain of units
systemd-analyze critical-chain

# print an SVG graphic detailing which system services have been started at what time, highlighting the time they spent on initialization.
systemd-analyze plot > /tmp/plot.svg  # open in web browser
```

<details>
<summary>Click to expand time spend info</summary>

```bash
$systemd-analyze time
Startup finished in 7.645s (firmware) + 249ms (loader) + 5.492s (kernel) + 10.407s (userspace) = 23.795s
graphical.target reached after 10.394s in userspace

$systemd-analyze blame
5.528s apparmor.service
3.974s plymouth-quit-wait.service
3.423s fwupd.service
1.460s bolt.service
 507ms systemd-logind.service
...
...

$systemd-analyze critical-chain
The time when unit became active or started is printed after the "@" character.
The time the unit took to start is printed after the "+" character.

graphical.target @10.394s
└─multi-user.target @10.394s
  └─plymouth-quit-wait.service @6.419s +3.974s
    └─systemd-user-sessions.service @6.409s +6ms
      └─network.target @6.407s
        └─networking.service @6.314s +92ms
          └─apparmor.service @760ms +5.528s
            └─local-fs.target @759ms
              └─boot-efi.mount @742ms +16ms
                └─dev-nvme0n1p5.device @676ms
```

</details>


### Service Disable

```bash
# systemctl list-unit-files --state=enabled
# systemctl list-unit-files --type service --state enabled,generated
# systemctl list-units --type=service --state=running

# https://help.ubuntu.com/community/AppArmor
systemctl disable --now apparmor.service

# https://askubuntu.com/questions/1119167/slow-boot-issue-due-to-plymouth-quit-wait-service-ubuntu-18-04#1168249
# 3.974s plymouth-quit-wait.service
# systemctl list-dependencies --reverse plymouth-quit-wait.service

# NetworkManager
systemctl disable --now NetworkManager-wait-online.service
systemctl mask NetworkManager-wait-online.service

systemctl disable --now systemd-networkd-wait-online.service
systemctl disable --now bootsplash-hide-when-booted.service

# Firmware update daemon (Linux Vendor Firmware Service)
# 3.423s fwupd.service
systemctl disable --now fwupd.service
systemctl mask fwupd.service
# systemctl disable --now fwupd-refresh.service
systemctl disable --now fwupd-refresh.timer
systemctl mask fwupd-refresh.timer

# fstrim - discard unused blocks on a mounted filesystem
# /usr/sbin/fstrim --fstab --verbose --quiet
# fstrim.service fstrim.timer
# systemctl disable --now fstrim.timer

# apt-daily.service apt-daily-upgrade.service
systemctl disable --now apt-daily.timer
systemctl disable --now apt-daily-upgrade.timer

# bolt - system daemon to manage thunderbolt 3 devices
# 1.460s bolt.service
systemctl disable --now bolt.service
systemctl mask bolt.service

# unattended-upgrades - automatic installation of security upgrades
systemctl disable --now unattended-upgrades.service

# Bluetooth
systemctl disable --now bluetooth.service
systemctl disable --now attach-bluetooth.service # Attach Bluetooth Adapter

# Cups
systemctl disable --now cups-browsed.service
systemctl disable --now cups.service

# Samba
systemctl disable --now smb.service  # Samba SMB Daemon
systemctl disable --now nmb.service  # Samba NMB Daemon
```

### Service Modify

#### systemd-resolved

Service `systemd-resolved.service` listens port *53*, LLMNR(Link-Local Multicast Name Resolution) listens port *5535* by default. If you wanna free up these ports.

```bash
systemd_resolve_path='/etc/systemd/resolved.conf'

[[ -f "${systemd_resolve_path}.bak" ]] || cp "${systemd_resolve_path}"{,.bak}

dns_server_list='9.9.9.9 1.1.1.1'  # or just 9.9.9.9

sed -r -i '/^#*DNS=/{s@^#*([^=]+=).*@\19.9.9.9 1.1.1.1@g}; /^#*(LLMNR|DNSStubListener)=/{s@^#*([^=]+=).*@\1no@g}' "${systemd_resolve_path}"

[[ -f /run/systemd/resolve/resolv.conf ]] && ln -sf /run/systemd/resolve/resolv.conf /etc/resolv.conf

# systemctl restart NetworkManager
```

## Power Management

Ubuntu official tutorial [Power & battery](https://help.ubuntu.com/stable/ubuntu-help/power.html.en)

### TLP

[TLP](https://linrunner.de/tlp/) is a tool to optimize Linux laptop battery Life.

```bash
# https://askubuntu.com/questions/1078939/ubuntu-18-04-battery-life#1134726
# powertop - diagnose issues with power consumption and management
apt install -y powertop
powertop --auto-tune  #  Set all tunable options to their good setting without interaction.

# https://linrunner.de/tlp/installation/ubuntu.html
# https://itsfoss.com/reduce-overheating-laptops-linux/

[[ -n $(which tlp 2> /dev/null || command -v tlp 2> /dev/null) ]] || apt install -y tlp tlp-rdw

# /etc/default/tlp ia a bsolete system-wide configuration file. It is evaluated only when /etc/tlp.conf is non-existent.
default_tlp_path=${default_tlp_path:-'/etc/default/tlp'}
[[ -f /etc/tlp.conf ]] && default_tlp_path='/etc/tlp.conf'

if [[ -f "${default_tlp_path}" ]]; then
    [[ -f "${default_tlp_path}".old ]] || cp "${default_tlp_path}"{,.old}

    sed -r -i '/^#*[[:space:]]*TLP_ENABLE=/{s@^#*[[:space:]]*([^=]+=).*@\11@g;}; /^#*[[:space:]]*TLP_DEFAULT_MODE=/{s@^#*[[:space:]]*([^=]+=).*@\1BAT@g;}; /^#*[[:space:]]*TLP_PERSISTENT_DEFAULT=/{s@^#*[[:space:]]*([^=]+=).*@\11@g;}' "${default_tlp_path}"

    systemctl enable --now tlp.service
    systemctl enable --now tlp-sleep.service

    # https://wiki.archlinux.org/index.php/TLP#Radio_Device_Wizard_(tlp-rdw)
    # systemd-rfkill.service, systemd-rfkill.socket, systemd-rfkill - Load and save the RF kill switch state at boot and change
    systemctl mask --now systemd-rfkill.service
    systemctl mask --now systemd-rfkill.socket
fi

# tlp start  # manually start

# tlp-stat -s
# Notice: tlp.service is not enabled -- invoke "systemctl enable tlp.service" to correct this!
# Notice: tlp-sleep.service is not enabled -- invoke "systemctl enable tlp-sleep.service" to correct this!
# Notice: systemd-rfkill.service is not masked -- invoke "systemctl mask systemd-rfkill.service" to correct this!
# Notice: systemd-rfkill.socket is not masked -- invoke "systemctl mask systemd-rfkill.socket" to correct this!
```


### CPU Frequency Scaling

```bash
# https://www.thinkwiki.org/wiki/How_to_use_cpufrequtils
# https://www.thinkwiki.org/wiki/How_to_make_use_of_Dynamic_Frequency_Scaling

# cpufrequtils - utilities to deal with the cpufreq Linux kernel feature
apt install -y tlp cpufrequtils

# Determines available cpufreq governors. (performance powersave)
cpufreq-info -g

cpufreq-set -g powersave
# cpufreq-set -g performance

# cpufreq-info - Utility to retrieve cpufreq kernel information
cpufreq-info
```

### Touch Screen Disable

How to disable touchscreen

* [How to disable touchscreen permanently on ubuntu 18.04](https://askubuntu.com/questions/1038248/how-to-disable-touchscreen-permanently-on-ubuntu-18-04/1038259)
* [How to disable the touchscreen drivers permanently on Ubuntu 17.10](https://phpocean.com/tutorials/computer-skills/how-to-disable-the-touchscreen-drivers-permanently-on-ubuntu-17-10/63)


#### Temporarily

Temporarily disable touch screen

[xinput](https://wiki.ubuntu.com/X/Config/Input) is a utility to configure and test X input devices.

>xinput is a utility to list available input devices, query information about a device and change input device settings.

<details>
<summary>Click to expand xinput list</summary>

```bash
$xinput --list

⎡ Virtual core pointer                          id=2    [master pointer  (3)]
⎜   ↳ Virtual core XTEST pointer                id=4    [slave  pointer  (2)]
⎜   ↳ SYNA2393:00 06CB:19AC                     id=12   [slave  pointer  (2)]
⎜   ↳ SYNA1D31:00 06CB:CD48 Mouse               id=13   [slave  pointer  (2)]
⎜   ↳ SYNA1D31:00 06CB:CD48 Touchpad            id=14   [slave  pointer  (2)]
⎣ Virtual core keyboard                         id=3    [master keyboard (2)]
    ↳ Virtual core XTEST keyboard               id=5    [slave  keyboard (3)]
    ↳ Power Button                              id=6    [slave  keyboard (3)]
    ↳ Video Bus                                 id=7    [slave  keyboard (3)]
    ↳ Video Bus                                 id=8    [slave  keyboard (3)]
    ↳ Power Button                              id=9    [slave  keyboard (3)]
    ↳ HD Camera: HD Camera                      id=10   [slave  keyboard (3)]
    ↳ Huawei WMI hotkeys                        id=15   [slave  keyboard (3)]
    ↳ AT Translated Set 2 keyboard              id=16   [slave  keyboard (3)]
```

</details>

Device `SYNA2393:00 06CB:19AC` is touchscreen, id is `12`.

```bash
# xinput disable/enable
# method 1 - via id
xinput disable 12

# method 1 - via name
xinput disable 'SYNA2393:00 06CB:19AC'
```

#### Permanently

Permanently disable touch screen

```bash
# /usr/share/X11/xorg.conf.d/40-libinput.conf

# Section "InputClass"
#         Identifier "libinput touchscreen catchall"
#         MatchIsTouchscreen "on"
#         MatchDevicePath "/dev/input/event*"
#         Driver "libinput"
# EndSection


# Change MatchIsTouchscreen val from 'on' to 'off'
sed -r -i '/MatchIsTouchscreen/{s@"on"@"off"@g;}' /usr/share/X11/xorg.conf.d/40-libinput.conf
```

## GRUB

```bash
default_grub_path='/etc/default/grub'

grub_timeout_val=6
# The resolution used on graphical terminal
# https://askubuntu.com/questions/54067/how-do-i-safely-change-grub2-screen-resolution#54068
grub_gfxmode_val='1024x768' # 1600x1200x32 800x600

if [[ -f "${default_grub_path}" ]]; then
    [[ -f "${default_grub_path}".old ]] || cp "${default_grub_path}"{,.old}

    # Change grub boot menu font
    sed -r -i '/^#*GRUB_TIMEOUT=/{s@^#*@@g;s@^([^=]+=).*@\1'"${grub_timeout_val}"'@g;}' "${default_grub_path}"

    # Change grub boot menu font
    sed -r -i '/^#*GRUB_GFXMODE=/{s@^#*@@g;s@^([^=]+=).*@\1'"${grub_gfxmode_val}"'@g;}' "${default_grub_path}"

    # Disable service plymouth when boot
    # GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"    https://askubuntu.com/questions/1076178/plymouth-taking-a-lot-of-time-during-boot
    sed -r -i '/^#*[[:space:]]*GRUB_CMDLINE_LINUX_DEFAULT=.*splash/{s@^#*[[:space:]]*@@g;s@(.*?=).*$@\1""@g;}' "${default_grub_path}"

    # https://wiki.archlinux.org/index.php/GRUB#Additional_arguments
    # Uncomment to disable generation of recovery mode menu entries
    sed -r -i '/^#*[[:space:]]*GRUB_DISABLE_RECOVERY=/{s@^#*@@g;}' "${default_grub_path}"

    update-grub 2> /dev/null
fi
```


## Log Management

### Systemd Journal

Clear existed [journal](https://wiki.archlinux.org/index.php/Systemd/Journal) log

```bash
# temporarily clean log
du -hs /var/log/journal/
journalctl --rotate
journalctl -m --vacuum-time=1s

# just keep latest 150MB log
journal_log_size='150M'
sed -r -i '/SystemMaxUse=/{s@^#*[[:space:]]*([^=]+=).*@\1'"${journal_log_size}"'@}' /etc/systemd/journald.conf
```

## Reboot

Reboot system then login via normal user.

```bash
reboot
```

After reboot and login

```bash
# - xdg user dirs generation, executing after `locale-gen`
xdg-user-dirs-update

# - personal custom boost tool
# axdsop boost tool intallation, running as normal user (pi)
curl -fsL https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/axdsop_boost_tool.sh | bash -s -- -i
# customsize configuration deployment
bash ~/.config/axdsop/deploy.sh -h

# To install i3 stack
# bash ~/.config/axdsop/deploy.sh -i
```

You may also consider remove older kernel

```bash
dpkg -l | sed -r -n '/linux-(image|headers)-?/{s@^i*[[:space:]]*([^[:space:]]+).*@\1@g;/'"$(uname -r)"'/d;p}' | xargs sudo apt -y purge

sudo apt purge --auto-remove -y
```

For ibus input method

```bash
ibus-setup

# Disable input method auto launch
# im-launch - launch input method and execute session program
if [[ -f /etc/xdg/autostart/im-launch.desktop ]]; then
  local_autostart_dir="$HOME/.config/autostart"
  [[ -d "${local_autostart_dir}" ]] || mkdir -p "${local_autostart_dir}"

  cp -f /etc/xdg/autostart/im-launch.desktop "${local_autostart_dir}"
  echo 'Hidden=true' >> "${local_autostart_dir}/im-launch.desktop"
fi

# ibus-daemon - daemon program for ibus
ibus-daemon -drR
```

## GNOME Management

Please operate as normal user.

### Essential Packages

```bash
# gnome-tweaks - tool to adjust advanced configuration settings for GNOME
# gnome-shell-extensions - Extensions to extend functionality of GNOME Shell

sudo apt install -y gnome-tweaks gnome-shell-extensions
```

### GNOME Keyring

[GNOME Keyring](https://wiki.gnome.org/Projects/GnomeKeyring) is a collection of components in GNOME that store secrets, passwords, keys, certificates and make them available to applications. You can manage the contents of GNOME Keyring using [Seahorse](https://wiki.gnome.org/Apps/Seahorse).

Disable keyring daemon components

```bash
# https://wiki.archlinux.org/index.php/GNOME/Keyring#Disable_keyring_daemon_components

if [[ -f /etc/xdg/autostart/gnome-keyring-ssh.desktop ]]; then
  local_autostart_dir="$HOME/.config/autostart"
  [[ -d "${local_autostart_dir}" ]] || mkdir -p "${local_autostart_dir}"

  cp -f /etc/xdg/autostart/gnome-keyring-ssh.desktop "${local_autostart_dir}"
  echo 'Hidden=true' >> "${local_autostart_dir}/gnome-keyring-ssh.desktop"
fi
```

### GNOME Pinentry

GPG prompts GUI passphrase by default, here force GPG to use console-mode pinentry to prompt for passwords.

```bash
# https://stackoverflow.com/questions/17769831/how-to-make-gpg-prompt-for-passphrase-on-cli/53641081#53641081

# - method 1 (non-interactive)
sudo update-alternatives --set pinentry /usr/bin/pinentry-curses

# - method 2 (interactive)
sudo update-alternatives --config pinentry

# There are 2 choices for the alternative pinentry (providing /usr/bin/pinentry).

#   Selection    Path                      Priority   Status
# ------------------------------------------------------------
# * 0            /usr/bin/pinentry-gnome3   90        auto mode
#   1            /usr/bin/pinentry-curses   50        manual mode
#   2            /usr/bin/pinentry-gnome3   90        manual mode

# Press <enter> to keep the current choice[*], or type selection number:
```

### GNOME Tracker

For GNOME Tracker disable

```bash
# https://gist.github.com/vancluever/d34b41eb77e6d077887c
local_autostart_dir="$HOME/.config/autostart"
[[ -d "${local_autostart_dir}" ]] || mkdir -p "${local_autostart_dir}"

cp -f /etc/xdg/autostart/tracker-*.desktop "${local_autostart_dir}"

for item in "${local_autostart_dir}"/tracker-*.desktop; do echo 'Hidden=true' >> "${item}"; done

tracker daemon -t
rm -rf ~/.cache/tracker ~/.local/share/tracker
tracker status
```

### gsettings Configuration

```bash
# gsettings list-schemas
# gsettings list-keys
# gsettings list-schemas | sort | while IFS="" read -r schema; do gsettings list-keys "${schema}" | sort | while IFS="" read -r key; do echo "${schema}|${key}|$(gsettings get ${schema} ${key} 2>/dev/null)"; done; done
```

Executing script [gsettings.sh](/GNULinux/Distros/gsettings.sh) to optimize GNOME applications.


## Change Log

* May 31, 2020 Sun 20:36 ET
  * First draft
* Oct 31, 2020 Sat 15:07 ET
  * Rewrite


[ubuntu]:https://ubuntu.com/ "The leading operating system for PCs, IoT devices, servers and the cloud."
[popos]:https://pop.system76.com/

<!-- End -->