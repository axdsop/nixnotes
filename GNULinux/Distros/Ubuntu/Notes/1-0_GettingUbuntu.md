# Getting Ubuntu

[Ubuntu][ubuntu] is an popular GNU/Linux distribution based on Debian. Latest release version is [Ubuntu 20.04 LTS](https://wiki.ubuntu.com/FocalFossa/ReleaseNotes) (Focal Fossa).


## TOC

1. [Introduction](#introduction)  
1.1 [Official Documentations](#official-documentations)  
1.2 [Downloading Page](#downloading-page)  
2. [Downloading](#downloading)  
3. [Image Verifying](#image-verifying)  
3.1 [PGP Signature Verifying](#pgp-signature-verifying)  
3.2 [Hash Digest Verfying](#hash-digest-verfying)  
4. [Bootable Flash Drive](#bootable-flash-drive)  
5. [Change Log](#change-log)  


## Introduction

[Ubuntu][ubuntu] is an open source software operating system that runs from the desktop, to the cloud, to all your internet connected things.

Platform|Architecture|Url
---|---|---
Server|amd64|https://ubuntu.com/server
Desktop|amd64|https://ubuntu.com/desktop
Cloud||https://ubuntu.com/cloud
Container||https://ubuntu.com/containers
IoT|armhf/arm64|https://ubuntu.com/internet-of-things


### Official Documentations

* [Official Ubuntu Documentation](https://help.ubuntu.com/)
* [Ubuntu Tutorials](https://ubuntu.com/tutorials)
  * [How to verify your Ubuntu download][ubuntu_verify_tutorial]


### Downloading Page

Platform|Download Page|Direct Link
---|---|---
Server|https://ubuntu.com/download/server|https://releases.ubuntu.com/
Desktop|https://ubuntu.com/download/desktop|https://releases.ubuntu.com/
Raspberry Pi|https://ubuntu.com/download/raspberry-pi|http://cdimage.ubuntu.com/releases/

[Ubuntu][ubuntu] also provides [official CD Mirrors for Ubuntu](https://launchpad.net/ubuntu/+cdmirrors). Site [xTom](https://mirrors.xtom.com/ubuntu-releases/) is a relative good choice.


## Downloading

Here choose *Ubuntu 20.04 LTS (Focal Fossa)* as  a example.

File|Save Name
---|---
[ubuntu-20.04-live-server-amd64.iso](https://releases.ubuntu.com/20.04/ubuntu-20.04-live-server-amd64.iso)|ubuntu-20.04-live-server-amd64.iso
[ubuntu-20.04-amd64-SHA256SUMS](https://releases.ubuntu.com/20.04/SHA256SUMS)|ubuntu-20.04-amd64-SHA256SUMS
[ubuntu-20.04-amd64-SHA256SUMS.gpg](https://releases.ubuntu.com/20.04/SHA256SUMS.gpg)|ubuntu-20.04-amd64-SHA256SUMS.gpg


```bash
# save dir ~/Downloads/
download_site='https://releases.ubuntu.com'
release_version='20.04'
arch_name='amd64'
iso_url="${download_site}/${release_version}/ubuntu-${release_version}-live-server-${arch_name}.iso"
sha256sum_url="${download_site}/${release_version}/SHA256SUMS"
sha256sum_sig_url="${sha256sum_url}.gpg"

# downloading tool
download_method='wget -qO-'
[[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && download_method='curl -fsSL'

# downloading command
$download_method "${iso_url}" > "${iso_url##*/}"    # ubuntu-20.04-live-server-amd64.iso
$download_method "${sha256sum_url}" > ubuntu-${release_version}-${arch_name}-"${sha256sum_url##*/}"    # ubuntu-20.04-amd64-SHA256SUMS
$download_method "${sha256sum_sig_url}" > ubuntu-${release_version}-${arch_name}-"${sha256sum_sig_url##*/}"    # ubuntu-20.04-amd64-SHA256SUMS.gpg
```

## Image Verifying

Check downloaded image for errors.

Note: file *SHA256SUMS.gpg* is just a signature file, it doesn't contain GPG public key. So we need to find out GPG public key issued from [Ubuntu][ubuntu] by first.

Official tutorial [How to verify your Ubuntu download][ubuntu_verify_tutorial] ([source 2](https://discourse.ubuntu.com/t/how-to-verify-your-ubuntu-download/14010)) shows Ubuntu GGP key is [0x46181433FBB75451](https://keyserver.ubuntu.com/pks/lookup?search=0x46181433FBB75451&fingerprint=on&op=index) (deprecated), [0xD94AA3F0EFE21092](https://keyserver.ubuntu.com/pks/lookup?search=0xD94AA3F0EFE21092&fingerprint=on&op=index).

```bash
gpg --keyid-format long --keyserver hkp://keyserver.ubuntu.com --recv-keys 0x46181433FBB75451 0xD94AA3F0EFE21092

# public key for 0xD94AA3F0EFE21092 rsa4096/843938df228d22f7b3742bc0d94aa3f0efe21092
https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x843938df228d22f7b3742bc0d94aa3f0efe21092
```

We'll use this public key to verify downloaded file.

### PGP Signature Verifying

Using personal custom function [GPGSignatureVerification](/CyberSecurity/GnuPG/Notes/2-1_UsageExample.md#package-signature-verifying) to verify PGP signature.

Usage

```bash
sha256sum_file='ubuntu-20.04-amd64-SHA256SUMS'
sha256sum_sig_file="${sha256sum_file}.gpg"
gpg_url='https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x843938df228d22f7b3742bc0d94aa3f0efe21092'

# fn_GPGSignatureVerification 'ISOPATH' 'SIGPATH' 'GPGPATH'
fn_GPGSignatureVerification "${sha256sum_file}" "${sha256sum_sig_file}" "${gpg_url}"
```

Output

```bash
Importing PGP key
gpg: keybox '/tmp/niejJIR8/gpg_import.gpg' created
gpg: key 0xD94AA3F0EFE21092: 66 signatures not checked due to missing keys
gpg: key 0xD94AA3F0EFE21092: public key "Ubuntu CD Image Automatic Signing Key (2012) <cdimage@ubuntu.com>" imported
gpg: Total number processed: 1
gpg:               imported: 1
gpg: public key of ultimately trusted key 0xDAA7DE7E855BB3F9 not found
gpg: marginals needed: 3  completes needed: 1  trust model: pgp
gpg: depth: 0  valid:   1  signed:   0  trust: 0-, 0q, 0n, 0m, 0f, 1u

Verifying signature
gpg: Signature made Thu 23 Apr 2020 09:46:21 AM EDT
gpg:                using RSA key 0xD94AA3F0EFE21092
gpg: Good signature from "Ubuntu CD Image Automatic Signing Key (2012) <cdimage@ubuntu.com>" [unknown]
gpg: WARNING: This key is not certified with a trusted signature!
gpg:          There is no indication that the signature belongs to the owner.
Primary key fingerprint: 8439 38DF 228D 22F7 B374  2BC0 D94A A3F0 EFE2 1092

Temporary directory /tmp/niejJIR8 has been removed.
```

### Hash Digest Verfying

After verifying file *SHA256SUMS* is issued by [Ubuntu][ubuntu]. Now checking if the image is integrated. Assuming these files saved in same directory.

```bash
target_name='ubuntu-20.04-live-server-amd64.iso'

grep "${target_name}" ubuntu-20.04-amd64-SHA256SUMS | shasum -a 256 -c
```

Output

```bash
ubuntu-20.04-live-server-amd64.iso: OK
```

## Bootable Flash Drive

Creating the bootable flash drive with the ISO image.

For [*nix](https://en.wikipedia.org/wiki/Unix-like), just using command `dd`

```bash
iso_path="$HOME/Downloads/ubuntu-20.04-live-server-amd64.iso"
usb_path='/dev/sdX' # via command 'fdisk -l', e.g. /dev/sda, /dev/sdb

# dd if=path/to/archlinux.iso of=/dev/sdx bs=4M status=progress oflag=sync
dd if="${iso_path}" of="${usb_path}" bs=4M status=progress oflag=sync && sync
```

Official documentation [Installing Ubuntu 20.04](https://help.ubuntu.com/lts/installation-guide/) shows detailed information for a variety of methods for installing Ubuntu. And official tutorial [Install Ubuntu desktop](https://ubuntu.com/tutorials/tutorial-install-ubuntu-desktop).


## Change Log

* May 31, 2020 Sun 20:36 ET
  * First draft


[raspberrypi]:https://www.raspberrypi.org/
[ubuntu]:https://ubuntu.com/ "The leading operating system for PCs, IoT devices, servers and the cloud."
[ubuntu_verify_tutorial]:https://ubuntu.com/tutorials/tutorial-how-to-verify-ubuntu


<!-- End -->