# Ubuntu

[Ubuntu][ubuntu] is an open source software operating system that runs from the desktop, to the cloud, to all your internet connected things.


## Operation Notes

1. [Getting Ubuntu](./Notes/1-0_GettingUbuntu.md): including real downloading link, ISO file gpg signature verifcation, making bootable flash drive.
2. [System Optimization](./Notes/2-0_SystemOptimization.md)


## Change Log

* Mar 25, 2020 Wed 19:29 ET
  * first draft
* May 31, 2020 Sun 20:43 ET
  * Add note *Getting Ubuntu*
* Oct 31, 2020 Sat 15:38 ET
  * Add note *System Optimization*

[ubuntu]:https://ubuntu.com/ "The leading operating system for PCs, IoT devices, servers and the cloud | Ubuntu"

<!-- End -->