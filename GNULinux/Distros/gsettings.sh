#!/usr/bin/env bash

# Gnome setting start
[[ -n $(which gsettings 2> /dev/null || command -v gsettings 2> /dev/null) ]] || exit

# - Desktop
# /usr/share/glib-2.0/schemas/org.gnome.desktop.background.gschema.xml
gsettings set org.gnome.desktop.background show-desktop-icons false

# Calendar & Date & Time
gsettings set org.gnome.desktop.calendar show-weekdate true
gsettings set org.gnome.desktop.datetime automatic-timezone false
gsettings set org.gnome.desktop.interface clock-format '24h'
gsettings set org.gnome.desktop.interface clock-show-weekday true
gsettings set org.gnome.desktop.interface clock-show-date true
gsettings set org.gnome.desktop.interface clock-show-seconds false

# /usr/share/glib-2.0/schemas/org.gnome.desktop.lockdown.gschema.xml
# gsettings set org.gnome.desktop.lockdown disable-application-handlers false
# gsettings set org.gnome.desktop.lockdown disable-command-line false
# gsettings set org.gnome.desktop.lockdown disable-lock-screen false
# gsettings set org.gnome.desktop.lockdown disable-log-out false
# gsettings set org.gnome.desktop.lockdown disable-printing false
# gsettings set org.gnome.desktop.lockdown disable-print-setup false
# gsettings set org.gnome.desktop.lockdown disable-save-to-disk false
# gsettings set org.gnome.desktop.lockdown disable-user-switching false
# gsettings set org.gnome.desktop.lockdown user-administration-disabled false

# /usr/share/glib-2.0/schemas/org.gnome.desktop.media-handling.gschema.xml
gsettings set org.gnome.desktop.media-handling autorun-never false
gsettings set org.gnome.desktop.media-handling automount true
gsettings set org.gnome.desktop.media-handling automount-open true

# /usr/share/glib-2.0/schemas/org.gnome.desktop.peripherals.gschema.xml
# Touchpad
gsettings set org.gnome.desktop.peripherals.touchpad click-method 'fingers' # none, areas, fingers, default
gsettings set org.gnome.desktop.peripherals.touchpad disable-while-typing true
gsettings set org.gnome.desktop.peripherals.touchpad edge-scrolling-enabled true
gsettings set org.gnome.desktop.peripherals.touchpad left-handed 'mouse'
gsettings set org.gnome.desktop.peripherals.touchpad natural-scroll true
gsettings set org.gnome.desktop.peripherals.touchpad send-events 'enabled'
# gsettings set org.gnome.desktop.peripherals.touchpad speed 0.0
gsettings set org.gnome.desktop.peripherals.touchpad tap-and-drag true
gsettings set org.gnome.desktop.peripherals.touchpad tap-to-click true
gsettings set org.gnome.desktop.peripherals.touchpad two-finger-scrolling-enabled true

# Desktop Privacy
# /usr/share/glib-2.0/schemas/org.gnome.desktop.privacy.gschema.xml
[[ -f "${login_user_home}/.local/share/recently-used.xbel" ]] && rm -f "${login_user_home}/.local/share/recently-used.xbel"
gsettings set org.gnome.desktop.privacy disable-camera false
gsettings set org.gnome.desktop.privacy disable-microphone false
gsettings set org.gnome.desktop.privacy disable-sound-output false
gsettings set org.gnome.desktop.privacy hide-identity false
gsettings set org.gnome.desktop.privacy old-files-age 7
gsettings set org.gnome.desktop.privacy recent-files-max-age 0
gsettings set org.gnome.desktop.privacy remember-app-usage false
gsettings set org.gnome.desktop.privacy remember-recent-files false
gsettings set org.gnome.desktop.privacy remove-old-temp-files true
gsettings set org.gnome.desktop.privacy remove-old-trash-files true
gsettings set org.gnome.desktop.privacy report-technical-problems false
gsettings set org.gnome.desktop.privacy send-software-usage-stats false
gsettings set org.gnome.desktop.privacy show-full-name-in-top-bar true


# - Ubuntu dock panel
gsettings set org.gnome.shell.extensions.dash-to-dock dock-position BOTTOM
gsettings set org.gnome.shell.extensions.dash-to-dock dash-max-icon-size 32
# gsettings set org.gnome.shell.extensions.dash-to-dock transparency-mode FIXED
gsettings set org.gnome.shell.extensions.dash-to-dock extend-height true
gsettings set org.gnome.shell.extensions.dash-to-dock unity-backlit-items false

# - Login Screen
gsettings set org.gnome.login-screen allowed-failures 3
# gsettings set org.gnome.login-screen banner-message-enable false
# gsettings set org.gnome.login-screen banner-message-text ''
gsettings set org.gnome.login-screen disable-restart-buttons true
gsettings set org.gnome.login-screen disable-user-list false
gsettings set org.gnome.login-screen enable-fingerprint-authentication true
gsettings set org.gnome.login-screen enable-password-authentication true
gsettings set org.gnome.login-screen enable-smartcard-authentication true
# gsettings set org.gnome.login-screen fallback-logo ''
# gsettings set org.gnome.login-screen logo ''

# screen lock time
# https://askubuntu.com/questions/1042641/how-to-set-custom-lock-screen-time-in-ubuntu-18-04
gsettings set org.gnome.desktop.session idle-delay 300

gsettings set org.gnome.desktop.screensaver idle-activation-enabled true
gsettings set org.gnome.desktop.screensaver lock-delay 60
gsettings set org.gnome.desktop.screensaver lock-enabled true
gsettings set org.gnome.desktop.screensaver logout-enabled false
gsettings set org.gnome.desktop.screensaver show-full-name-in-top-bar true
gsettings set org.gnome.desktop.screensaver status-message-enabled true
gsettings set org.gnome.desktop.screensaver user-switch-enabled true

# /usr/share/glib-2.0/schemas/org.gnome.desktop.sound.gschema.xml
gsettings set org.gnome.desktop.sound allow-volume-above-100-percent false
gsettings set org.gnome.desktop.sound event-sounds true
gsettings set org.gnome.desktop.sound input-feedback-sounds false


# - Windows
# Titlebar Buttons (default close)
gsettings set org.gnome.desktop.wm.preferences button-layout 'appmenu:minimize,maximize,close'

# - Workspaces
# gsettings set org.gnome.desktop.wm.preferences num-workspaces 4
gsettings set org.gnome.shell.overrides dynamic-workspaces true
gsettings set org.gnome.shell.overrides workspaces-only-on-primary true

# evince - pdf reader
# /usr/share/glib-2.0/schemas/org.gnome.Evince.gschema.xml
# https://gitlab.gnome.org/GNOME/evince/blob/master/data/org.gnome.Evince.gschema.xml
gsettings set org.gnome.Evince allow-links-change-zoom true
gsettings set org.gnome.Evince auto-reload true
gsettings set org.gnome.Evince override-restrictions true
gsettings set org.gnome.Evince page-cache-size 300
gsettings set org.gnome.Evince show-caret-navigation-message true
gsettings set org.gnome.Evince.Default continuous true
gsettings set org.gnome.Evince.Default dual-page false
gsettings set org.gnome.Evince.Default dual-page-odd-left false
gsettings set org.gnome.Evince.Default fullscreen false
gsettings set org.gnome.Evince.Default show-sidebar false
gsettings set org.gnome.Evince.Default show-toolbar false
gsettings set org.gnome.Evince.Default sizing-mode 'fit-width' # fit-page, fit-width, free, automatic

# gedit
# /usr/share/glib-2.0/schemas/org.gnome.gedit.gschema.xml
gsettings set org.gnome.gedit.preferences.editor auto-indent false
gsettings set org.gnome.gedit.preferences.editor auto-save true
gsettings set org.gnome.gedit.preferences.editor auto-save-interval 5
# gsettings set org.gnome.gedit.preferences.editor background-pattern 'none'
# gsettings set org.gnome.gedit.preferences.editor bracket-matching false
# gsettings set org.gnome.gedit.preferences.editor create-backup-copy false
gsettings set org.gnome.gedit.preferences.editor display-line-numbers true
gsettings set org.gnome.gedit.preferences.editor display-overview-map false
gsettings set org.gnome.gedit.preferences.editor display-right-margin false
gsettings set org.gnome.gedit.preferences.editor editor-font 'Monospace 14'
gsettings set org.gnome.gedit.preferences.editor ensure-trailing-newline true
gsettings set org.gnome.gedit.preferences.editor highlight-current-line true
gsettings set org.gnome.gedit.preferences.editor insert-spaces true
gsettings set org.gnome.gedit.preferences.editor max-undo-actions 2000
gsettings set org.gnome.gedit.preferences.editor restore-cursor-position true
gsettings set org.gnome.gedit.preferences.editor right-margin-position 80
gsettings set org.gnome.gedit.preferences.editor scheme 'solarized-light'
gsettings set org.gnome.gedit.preferences.editor search-highlighting true
gsettings set org.gnome.gedit.preferences.editor smart-home-end 'after'
gsettings set org.gnome.gedit.preferences.editor syntax-highlighting true
gsettings set org.gnome.gedit.preferences.editor tabs-size 4
gsettings set org.gnome.gedit.preferences.editor use-default-font false
# gsettings set org.gnome.gedit.preferences.editor wrap-last-split-mode 'word'
# gsettings set org.gnome.gedit.preferences.editor wrap-mode 'word'
# gsettings set org.gnome.gedit.preferences.encodings candidate-encodings ['']
gsettings set org.gnome.gedit.preferences.ui bottom-panel-visible false
gsettings set org.gnome.gedit.preferences.ui max-recents 0
gsettings set org.gnome.gedit.preferences.ui show-tabs-mode 'auto' # never, always, auto
gsettings set org.gnome.gedit.preferences.ui side-panel-visible false
gsettings set org.gnome.gedit.preferences.ui statusbar-visible true
gsettings set org.gnome.gedit.preferences.ui toolbar-visible true

# gnome-terminal
# /usr/share/glib-2.0/schemas/org.gnome.Terminal.gschema.xml
gsettings set org.gnome.Terminal.Legacy.Settings confirm-close true
gsettings set org.gnome.Terminal.Legacy.Settings default-show-menubar false
# gsettings set org.gnome.Terminal.Legacy.Settings headerbar @mb nothing
gsettings set org.gnome.Terminal.Legacy.Settings menu-accelerator-enabled true
gsettings set org.gnome.Terminal.Legacy.Settings mnemonics-enabled false
gsettings set org.gnome.Terminal.Legacy.Settings new-terminal-mode 'tab' # tab, window
gsettings set org.gnome.Terminal.Legacy.Settings schema-version 3
gsettings set org.gnome.Terminal.Legacy.Settings shell-integration-enabled true
gsettings set org.gnome.Terminal.Legacy.Settings shortcuts-enabled true
gsettings set org.gnome.Terminal.Legacy.Settings tab-policy 'automatic'
gsettings set org.gnome.Terminal.Legacy.Settings tab-position 'top'
gsettings set org.gnome.Terminal.Legacy.Settings theme-variant 'system'
gsettings set org.gnome.Terminal.Legacy.Settings unified-menu true

# Gnome setting end
