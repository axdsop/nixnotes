# GNU/Linux Distros


## Distro List

Distro|Download|Notes|Script
---|---|---|---
[Arch Linux](./ArchLinux)|[Download](./ArchLinux/Notes/1-0_Prerequisites.md)|[Notes](./ArchLinux/Notes/)|[install script](./ArchLinux/alis.sh)
[Manjaro](./Manjaro)|[Download](./Manjaro/Notes/1-0_GettingManjaro.md)|[Notes](./Manjaro/Notes/)|
[Ubuntu](./Ubuntu)|[Download](./Ubuntu/Notes/1-0_GettingUbuntu.md)|[Notes](./Ubuntu/Notes/)|
[Kali Linux](./KaliLinux)|[Download](./KaliLinux/Notes/1-0_GettingKaliLinux.md)|[Notes](./KaliLinux/Notes/)|


<!-- End -->