# Manjaro Editions List

There are four official editions (Xfce, KDE, GNOME, Architect)of Manjaro available, as well as a number of unofficial “community” editions. It also supports ARM editions.


## List

Generating date *Oct 05, 2021 Tues*

Type|Editions|Url|Sig Url|ISO SHA1|Minimal Url|Minimal Sig Url|Minimal ISO SHA1
---|---|---|---|---|---|---|---
Official|GNOME 21.1.4|https://download.manjaro.org/gnome/21.1.4/manjaro-gnome-21.1.4-210927-linux513.iso|https://download.manjaro.org/gnome/21.1.4/manjaro-gnome-21.1.4-210927-linux513.iso.sig|c0073504e7ac1e0882a443e1da1c94585930826d|https://download.manjaro.org/gnome/21.1.4/manjaro-gnome-21.1.4-minimal-210927-linux513.iso|https://download.manjaro.org/gnome/21.1.4/manjaro-gnome-21.1.4-minimal-210927-linux513.iso.sig|f970fba92de433aeeb974a8c44feebd3383a40c1|https://download.manjaro.org/gnome/21.1.4/manjaro-gnome-21.1.4-minimal-210927-linux54.iso|https://download.manjaro.org/gnome/21.1.4/manjaro-gnome-21.1.4-minimal-210927-linux54.iso.sig|c7271b8511cd01986ad73c4af562c1b0c8d0f7f2
Official|KDE Plasma 21.1.4|https://download.manjaro.org/kde/21.1.4/manjaro-kde-21.1.4-210927-linux513.iso|https://download.manjaro.org/kde/21.1.4/manjaro-kde-21.1.4-210927-linux513.iso.sig|e2cbe340d14e98149dfeb1a3ea390fcf8f664c25|https://download.manjaro.org/kde/21.1.4/manjaro-kde-21.1.4-minimal-210927-linux513.iso|https://download.manjaro.org/kde/21.1.4/manjaro-kde-21.1.4-minimal-210927-linux513.iso.sig|4d00079c0ef6ec6e77f41c163674f375e96a7cbb|https://download.manjaro.org/kde/21.1.4/manjaro-kde-21.1.4-minimal-210927-linux54.iso|https://download.manjaro.org/kde/21.1.4/manjaro-kde-21.1.4-minimal-210927-linux54.iso.sig|606f754313e5b861b325321900a45382fe31fb12
Official|XFCE 21.1.4|https://download.manjaro.org/xfce/21.1.4/manjaro-xfce-21.1.4-210927-linux513.iso|https://download.manjaro.org/xfce/21.1.4/manjaro-xfce-21.1.4-210927-linux513.iso.sig|f5a0747dba599984237884fc47453ed1d43f5da7|https://download.manjaro.org/xfce/21.1.4/manjaro-xfce-21.1.4-minimal-210927-linux513.iso|https://download.manjaro.org/xfce/21.1.4/manjaro-xfce-21.1.4-minimal-210927-linux513.iso.sig|626681fbb00085af4f4c2c829c489d37ee6dc427|https://download.manjaro.org/xfce/21.1.4/manjaro-xfce-21.1.4-minimal-210927-linux54.iso|https://download.manjaro.org/xfce/21.1.4/manjaro-xfce-21.1.4-minimal-210927-linux54.iso.sig|e7aa9a6b40806391f12f8d4b32936ce5db68f39b
Community|Budgie 21.1.2|https://download.manjaro.org/budgie/21.1.2/manjaro-budgie-21.1.2-210907-linux513.iso|https://download.manjaro.org/budgie/21.1.2/manjaro-budgie-21.1.2-210907-linux513.iso.sig|21ccc9263ce58ceb734a63a639cbe41960616be9|https://download.manjaro.org/budgie/21.1.2/manjaro-budgie-21.1.2-minimal-210907-linux513.iso|https://download.manjaro.org/budgie/21.1.2/manjaro-budgie-21.1.2-minimal-210907-linux513.iso.sig|e0ba1d706504553fa15ef6f76c41c0cd177001a8
Community|Cinnamon 21.1.2|https://download.manjaro.org/cinnamon/21.1.2/manjaro-cinnamon-21.1.2-210907-linux513.iso|https://download.manjaro.org/cinnamon/21.1.2/manjaro-cinnamon-21.1.2-210907-linux513.iso.sig|d671f364ea0e0552119ec99dd7c1dcfc3ceaf133|https://download.manjaro.org/cinnamon/21.1.2/manjaro-cinnamon-21.1.2-minimal-210907-linux513.iso|https://download.manjaro.org/cinnamon/21.1.2/manjaro-cinnamon-21.1.2-minimal-210907-linux513.iso.sig|1e18c97c499f5ff3c7997a51d3a36f7d3ee13a2b
Community|Deepin 21.1.2|https://download.manjaro.org/deepin/21.1.2/manjaro-deepin-21.1.2-210907-linux513.iso|https://download.manjaro.org/deepin/21.1.2/manjaro-deepin-21.1.2-210907-linux513.iso.sig|3ade66c95529796307d32c9e7fb376d919088dab|https://download.manjaro.org/deepin/21.1.2/manjaro-deepin-21.1.2-minimal-210907-linux513.iso|https://download.manjaro.org/deepin/21.1.2/manjaro-deepin-21.1.2-minimal-210907-linux513.iso.sig|f11939dcfe233c90c7bfb48161db501796873a90
Community|I3 21.1.2|https://download.manjaro.org/i3/21.1.2/manjaro-i3-21.1.2-210907-linux513.iso|https://download.manjaro.org/i3/21.1.2/manjaro-i3-21.1.2-210907-linux513.iso.sig|ef1f872538b78a5cdba30407ab992755bb460dc0|https://download.manjaro.org/i3/21.1.2/manjaro-i3-21.1.2-minimal-210907-linux513.iso|https://download.manjaro.org/i3/21.1.2/manjaro-i3-21.1.2-minimal-210907-linux513.iso.sig|b74e791b54ba524e4a609c8232674fd7d74c9b42
Community|MATE 21.1.2|https://download.manjaro.org/mate/21.1.2/manjaro-mate-21.1.2-210907-linux513.iso|https://download.manjaro.org/mate/21.1.2/manjaro-mate-21.1.2-210907-linux513.iso.sig|4b135ae10186bfbd3339ea3ba86f167ed7753682|https://download.manjaro.org/mate/21.1.2/manjaro-mate-21.1.2-minimal-210907-linux513.iso|https://download.manjaro.org/mate/21.1.2/manjaro-mate-21.1.2-minimal-210907-linux513.iso|128b6a09e04a14f49f770a52a2a349fd1e886f94
ARM|Khadas Edge-V Pro KDE Plasma 20.06|https://osdn.net/projects/manjaro-arm/storage/edgev/kde-plasma/20.06/Manjaro-ARM-kde-plasma-edgev-20.06.img.xz|https://osdn.net/projects/manjaro-arm/storage/edgev/kde-plasma/20.06/Manjaro-ARM-kde-plasma-edgev-20.06.img.xz.sig|0e53be1943c2f0b0d4785c1010aa9e63afa3f9d6
ARM|Khadas Edge-V Pro XFCE 20.06|https://osdn.net/projects/manjaro-arm/storage/edgev/xfce/20.06/Manjaro-ARM-xfce-edgev-20.06.img.xz|https://osdn.net/projects/manjaro-arm/storage/edgev/xfce/20.06/Manjaro-ARM-xfce-edgev-20.06.img.xz.sig|e9dabbe3146da2c8d3e1f36e6461d049aa5e1d18
ARM|Khadas Vim 1 I3 20.10|https://osdn.net/projects/manjaro-arm/storage/vim1/i3/20.10/Manjaro-ARM-i3-vim1-20.10.img.xz|https://osdn.net/projects/manjaro-arm/storage/vim1/i3/20.10/Manjaro-ARM-i3-vim1-20.10.img.xz.sig|fe4ceae3fce30eb3008c6a4a773c443d1e8d1bda
ARM|Khadas Vim 1 KDE Plasma 20.10|https://osdn.net/projects/manjaro-arm/storage/vim1/kde-plasma/20.10/Manjaro-ARM-kde-plasma-vim1-20.10.img.xz|https://osdn.net/projects/manjaro-arm/storage/vim1/kde-plasma/20.10/Manjaro-ARM-kde-plasma-vim1-20.10.img.xz.sig|c9c70b6e2f5910cd72719e86d74f44969e80ff3d
ARM|Khadas Vim 1 Sway 20.10|https://osdn.net/projects/manjaro-arm/storage/vim1/sway/20.10/Manjaro-ARM-sway-vim1-20.10.img.xz|https://osdn.net/projects/manjaro-arm/storage/vim1/sway/20.10/Manjaro-ARM-sway-vim1-20.10.img.xz.sig|a81b1d227d2a4907a5a9837a3f841118397f0cdb
ARM|Khadas Vim 1 XFCE 20.10|https://osdn.net/projects/manjaro-arm/storage/vim1/xfce/20.10/Manjaro-ARM-xfce-vim1-20.10.img.xz|https://osdn.net/projects/manjaro-arm/storage/vim1/xfce/20.10/Manjaro-ARM-xfce-vim1-20.10.img.xz.sig|fd8f90cca82bd30b8a8e4ce8529304fbb0ea1bcf
ARM|Khadas Vim 2 Gnome 21.08|https://github.com/manjaro-arm/vim2-images/releases/download/21.08/Manjaro-ARM-gnome-vim2-21.08.img.xz|38719d5a254b7e7fa0ed3c466c21e6f5dc929534
ARM|Khadas Vim 2 KDE Plasma 21.08|https://github.com/manjaro-arm/vim2-images/releases/download/21.08/Manjaro-ARM-kde-plasma-vim2-21.08.img.xz|d920773b6089c9329bc516bacbc226af3e253bb2
ARM|Khadas Vim 2 MATE 21.08|https://github.com/manjaro-arm/vim2-images/releases/download/21.08/Manjaro-ARM-mate-vim2-21.08.img.xz|d6429633f764c753c85b47da003feab15baa5099
ARM|Khadas Vim 2 Minimal 21.08|https://github.com/manjaro-arm/vim2-images/releases/download/21.08/Manjaro-ARM-minimal-vim2-21.08.img.xz|6a1d7460220e0dcffb8bec1f5d73b0b2a4a6007c
ARM|Khadas Vim 2 Sway 21.08|https://github.com/manjaro-arm/vim2-images/releases/download/21.08/Manjaro-ARM-sway-vim2-21.08.img.xz|64d501b07e1e40944d020393e3360808d75fbeab
ARM|Khadas Vim 2 XFCE 21.08|https://github.com/manjaro-arm/vim2-images/releases/download/21.08/Manjaro-ARM-xfce-vim2-21.08.img.xz|cf6c07c5582edc5f1b5f3d600386d28d0b631464
ARM|Khadas Vim 3 Gnome 21.08|https://github.com/manjaro-arm/vim3-images/releases/download/21.08/Manjaro-ARM-gnome-vim3-21.08.img.xz|568631fa854aeda16fe2413e62834b9ea6465f55
ARM|Khadas Vim 3 KDE Plasma 21.08|https://github.com/manjaro-arm/vim3-images/releases/download/21.08/Manjaro-ARM-kde-plasma-vim3-21.08.img.xz|823b671239d63b4d85a2d844557750e5a0038cbe
ARM|Khadas Vim 3 MATE 21.08|https://github.com/manjaro-arm/vim3-images/releases/download/21.08/Manjaro-ARM-mate-vim3-21.08.img.xz|4e964f0a740a7b6d57573e531b9d9bb97ed2f52f
ARM|Khadas Vim 3 Minimal 21.08|https://github.com/manjaro-arm/vim3-images/releases/download/21.08/Manjaro-ARM-minimal-vim3-21.08.img.xz|7f108e342f51214d8784c6ce4ffef3440e4548c0
ARM|Khadas Vim 3 Sway 21.08|https://github.com/manjaro-arm/vim3-images/releases/download/21.08/Manjaro-ARM-sway-vim3-21.08.img.xz|0c581399c006518f74460dc2b03fedce763d1064
ARM|Khadas Vim 3 XFCE 21.08|https://github.com/manjaro-arm/vim3-images/releases/download/21.08/Manjaro-ARM-xfce-vim3-21.08.img.xz|349f37fc42251f2ef71bd4bed3f950bff150232f
ARM|Odroid C4 Gnome 21.08|https://github.com/manjaro-arm/oc4-images/releases/download/21.08/Manjaro-ARM-gnome-oc4-21.08.img.xz|4c7edb3f1689ba003d44264e79121de810195e14
ARM|Odroid C4 KDE Plasma 21.08|https://github.com/manjaro-arm/oc4-images/releases/download/21.08/Manjaro-ARM-kde-plasma-oc4-21.08.img.xz|741858dd623b545700881ac3c42f90060c208411
ARM|Odroid C4 MATE 21.08|https://github.com/manjaro-arm/oc4-images/releases/download/21.08/Manjaro-ARM-mate-oc4-21.08.img.xz|dd6b421a28a35c9e011663e28b31e9b20a60cb8b
ARM|Odroid C4 Minimal 21.08|https://github.com/manjaro-arm/oc4-images/releases/download/21.08/Manjaro-ARM-minimal-oc4-21.08.img.xz|301699f086df6e08c7525ad4e328e893096a18a7
ARM|Odroid C4 Sway 21.08|https://github.com/manjaro-arm/oc4-images/releases/download/21.08/Manjaro-ARM-sway-oc4-21.08.img.xz|ce4a150ad6f7e8acce49a463d2d1633634f49cd6
ARM|Odroid C4 XFCE 21.08|https://github.com/manjaro-arm/oc4-images/releases/download/21.08/Manjaro-ARM-xfce-oc4-21.08.img.xz|a563f4e48d9a5fc6e87aaef950cf640eb45a0719
ARM|Odroid N2 Gnome 21.08|https://github.com/manjaro-arm/on2-images/releases/download/21.08/Manjaro-ARM-gnome-on2-21.08.img.xz|1b31548627ddd9d983d349559fcceec098889ddf
ARM|Odroid N2 KDE Plasma 21.08|https://github.com/manjaro-arm/on2-images/releases/download/21.08/Manjaro-ARM-kde-plasma-on2-21.08.img.xz|2d6fb20bcc0899f2d349e57bb9c14b501a3aacc5
ARM|Odroid N2 MATE 21.08|https://github.com/manjaro-arm/on2-images/releases/download/21.08/Manjaro-ARM-mate-on2-21.08.img.xz|e3d520ba260544d9ef63a020e896e708e5444df3
ARM|Odroid N2 Minimal 21.08|https://github.com/manjaro-arm/on2-images/releases/download/21.08/Manjaro-ARM-minimal-on2-21.08.img.xz|bb24962429cee4ab6169f7dadb0ddd2daf0667b7
ARM|Odroid N2 Sway 21.08|https://github.com/manjaro-arm/on2-images/releases/download/21.08/Manjaro-ARM-sway-on2-21.08.img.xz|b06075c612026d0aae02e1bca7c4345fd265723f
ARM|Odroid N2 XFCE 21.08|https://github.com/manjaro-arm/on2-images/releases/download/21.08/Manjaro-ARM-xfce-on2-21.08.img.xz|a568911b40f3a7e6b3e472144c5781bd7dfcea4e
ARM|Odroid N2&#43; Gnome 21.08|https://github.com/manjaro-arm/on2-plus-images/releases/download/21.08/Manjaro-ARM-gnome-on2-plus-21.08.img.xz|def8f5ec348a6af23805a6b13171dab1a8b36eec
ARM|Odroid N2&#43; KDE Plasma 21.08|https://github.com/manjaro-arm/on2-plus-images/releases/download/21.08/Manjaro-ARM-kde-plasma-on2-plus-21.08.img.xz|a44958e6b0337f444f51307bf4e69b24d8b24ec8
ARM|Odroid N2&#43; MATE 21.08|https://github.com/manjaro-arm/on2-plus-images/releases/download/21.08/Manjaro-ARM-mate-on2-plus-21.08.img.xz|b24900db9611fc5b8ef26fd4b7de6888409a4397
ARM|Odroid N2&#43; Minimal 21.08|https://github.com/manjaro-arm/on2-plus-images/releases/download/21.08/Manjaro-ARM-minimal-on2-plus-21.08.img.xz|f17dd016123ed99566350b32121aa20353c7c9c5
ARM|Odroid N2&#43; Sway 21.08|https://github.com/manjaro-arm/on2-plus-images/releases/download/21.08/Manjaro-ARM-sway-on2-plus-21.08.img.xz|b585184f37f54d963c1499ba8ddf868f1a113195
ARM|Odroid N2&#43; XFCE 21.08|https://github.com/manjaro-arm/on2-plus-images/releases/download/21.08/Manjaro-ARM-xfce-on2-plus-21.08.img.xz|4be9593b63e9b82694bfc601c70a36e122bc4908
ARM|Pine H64 Gnome 21.08|https://github.com/manjaro-arm/pine-h64-images/releases/download/21.08/Manjaro-ARM-gnome-pine-h64-21.08.img.xz|e12d8567f4f9ca9c3d82e8171d91290b9c162ab8
ARM|Pine H64 KDE Plasma 21.08|https://github.com/manjaro-arm/pine-h64-images/releases/download/21.08/Manjaro-ARM-kde-plasma-pine-h64-21.08.img.xz|40f8761c71ffbc9e2e16df0382d8dc9d8016b005
ARM|Pine H64 MATE 21.08|https://github.com/manjaro-arm/pine-h64-images/releases/download/21.08/Manjaro-ARM-mate-pine-h64-21.08.img.xz|fcda09db64a6ecc444434b6c0cbad376d7020f5b
ARM|Pine H64 Minimal 21.08|https://github.com/manjaro-arm/pine-h64-images/releases/download/21.08/Manjaro-ARM-minimal-pine-h64-21.08.img.xz|d89a4819beacb589c767e36dbd14725b77d02cef
ARM|Pine H64 Sway 21.08|https://github.com/manjaro-arm/pine-h64-images/releases/download/21.08/Manjaro-ARM-sway-pine-h64-21.08.img.xz|d369937777a43eba38d3257859a05f17b0e53356
ARM|Pine H64 XFCE 21.08|https://github.com/manjaro-arm/pine-h64-images/releases/download/21.08/Manjaro-ARM-xfce-pine-h64-21.08.img.xz|87b33b82f08d89f338412c2a68d7213e9af8729e
ARM|Pine64 LTS Gnome 21.08|https://github.com/manjaro-arm/pine64-lts-images/releases/download/21.08/Manjaro-ARM-gnome-pine64-lts-21.08.img.xz|aa7af52a8b39432696ba137d3f13ab10ea9b37e3
ARM|Pine64 LTS KDE Plasma 21.08|https://github.com/manjaro-arm/pine64-lts-images/releases/download/21.08/Manjaro-ARM-kde-plasma-pine64-lts-21.08.img.xz|cf56a2b0e2e7b3b8b1d69efe8860e7d573e10aea
ARM|Pine64 LTS MATE 21.08|https://github.com/manjaro-arm/pine64-lts-images/releases/download/21.08/Manjaro-ARM-mate-pine64-lts-21.08.img.xz|449a351af223fb5c428bdfb6dd2c140d5915285b
ARM|Pine64 LTS Minimal 21.08|https://github.com/manjaro-arm/pine64-lts-images/releases/download/21.08/Manjaro-ARM-minimal-pine64-lts-21.08.img.xz|604636e7acfe10258a8bc843dc193b5a21507a4a
ARM|Pine64 LTS Sway 21.08|https://github.com/manjaro-arm/pine64-lts-images/releases/download/21.08/Manjaro-ARM-sway-pine64-lts-21.08.img.xz|ff5597b295b3be09117941faaa4f349a8a8468d6
ARM|Pine64 LTS XFCE 21.08|https://github.com/manjaro-arm/pine64-lts-images/releases/download/21.08/Manjaro-ARM-xfce-pine64-lts-21.08.img.xz|62a6c480382880ade872caac4da2ff7d7338d630
ARM|Pinebook Gnome 21.08|https://github.com/manjaro-arm/pinebook-images/releases/download/21.08/Manjaro-ARM-gnome-pinebook-21.08.img.xz|0e4769e932c024ee2460feddae0910b346f94448
ARM|Pinebook KDE Plasma 21.08|https://github.com/manjaro-arm/pinebook-images/releases/download/21.08/Manjaro-ARM-kde-plasma-pinebook-21.08.img.xz|1aba7b7a2584dfb92254f4e6cc8c0b5bee6b980d
ARM|Pinebook MATE 21.08|https://github.com/manjaro-arm/pinebook-images/releases/download/21.08/Manjaro-ARM-mate-pinebook-21.08.img.xz|e035c7a35534d5b05ff25eb57d16bd36d4cad53f
ARM|Pinebook Minimal 21.08|https://github.com/manjaro-arm/pinebook-images/releases/download/21.08/Manjaro-ARM-minimal-pinebook-21.08.img.xz|59536da278c186b7c3a3533dcc6fd25ef0336324
ARM|Pinebook Pro Gnome 21.08|https://github.com/manjaro-arm/pbpro-images/releases/download/21.08/Manjaro-ARM-gnome-pbpro-21.08.img.xz|0459ac5bbad70a0e45d46d19cf726b6938eb67d5
ARM|Pinebook Pro KDE Plasma 21.08|https://github.com/manjaro-arm/pbpro-images/releases/download/21.08/Manjaro-ARM-kde-plasma-pbpro-21.08.img.xz|af0f0596bd555c066ba08c0ccf9330f33c157c9a
ARM|Pinebook Pro MATE 21.08|https://github.com/manjaro-arm/pbpro-images/releases/download/21.08/Manjaro-ARM-mate-pbpro-21.08.img.xz|af42708fde8dee231477e8af1d1682ba06ddb55f
ARM|Pinebook Pro Minimal 21.08|https://github.com/manjaro-arm/pbpro-images/releases/download/21.08/Manjaro-ARM-minimal-pbpro-21.08.img.xz|d0404fe400a21e6453a2dfca2088d2fca66d11fb
ARM|Pinebook Pro Sway 21.08|https://github.com/manjaro-arm/pbpro-images/releases/download/21.08/Manjaro-ARM-sway-pbpro-21.08.img.xz|72e6dde26c4f241b7064cee4ec9b04cc4d0f6f3f
ARM|Pinebook Pro XFCE 21.08|https://github.com/manjaro-arm/pbpro-images/releases/download/21.08/Manjaro-ARM-xfce-pbpro-21.08.img.xz|2ed9eeb3cbdfdc8c632ac520f306f8e174625535
ARM|Pinebook Sway 21.08|https://github.com/manjaro-arm/pinebook-images/releases/download/21.08/Manjaro-ARM-sway-pinebook-21.08.img.xz|14a4d16f955b6997da1775ab997c24b3711e94ee
ARM|Pinebook XFCE 21.08|https://github.com/manjaro-arm/pinebook-images/releases/download/21.08/Manjaro-ARM-xfce-pinebook-21.08.img.xz|859f481f5860e715ecc374203deb40aacffe4d18
ARM|Raspberry Pi 4 Gnome 21.08|https://github.com/manjaro-arm/rpi4-images/releases/download/21.08/Manjaro-ARM-gnome-rpi4-21.08.img.xz|803ec8e357e89661e76424cbbb3e7c1c93b66e77
ARM|Raspberry Pi 4 KDE Plasma 21.08|https://github.com/manjaro-arm/rpi4-images/releases/download/21.08/Manjaro-ARM-kde-plasma-rpi4-21.08.img.xz|f3b6625daaf4a15deffa4f37e0bb56b06ed4894b
ARM|Raspberry Pi 4 MATE 21.08|https://github.com/manjaro-arm/rpi4-images/releases/download/21.08/Manjaro-ARM-mate-rpi4-21.08.img.xz|67623fb559c8a6d3bed8952213f76b9b423dbdb5
ARM|Raspberry Pi 4 Minimal 21.08|https://github.com/manjaro-arm/rpi4-images/releases/download/21.08/Manjaro-ARM-minimal-rpi4-21.08.img.xz|ff9fe977ea990286ebe880cf901700cdc5b6ee67
ARM|Raspberry Pi 4 Sway 21.08|https://github.com/manjaro-arm/rpi4-images/releases/download/21.08/Manjaro-ARM-sway-rpi4-21.08.img.xz|982713b8ec5f07dc30b90b177f17bfc86a6adc8e
ARM|Raspberry Pi 4 XFCE 21.08|https://github.com/manjaro-arm/rpi4-images/releases/download/21.08/Manjaro-ARM-xfce-rpi4-21.08.img.xz|a3250ffbdbf6c96873973f049dc5a168dff43c47
ARM|Rock Pi 4B Gnome 21.08|https://github.com/manjaro-arm/rockpi4b-images/releases/download/21.08/Manjaro-ARM-gnome-rockpi4b-21.08.img.xz|00d6e2519475a9fb10868ada76ac074d3d6135b1
ARM|Rock Pi 4B KDE Plasma 21.08|https://github.com/manjaro-arm/rockpi4b-images/releases/download/21.08/Manjaro-ARM-kde-plasma-rockpi4b-21.08.img.xz|0371a5b740836a1ca8e945a8865a2b6119725388
ARM|Rock Pi 4B MATE 21.08|https://github.com/manjaro-arm/rockpi4b-images/releases/download/21.08/Manjaro-ARM-mate-rockpi4b-21.08.img.xz|7d4140b5aead1084d8528283c4728eb752b684d7
ARM|Rock Pi 4B Minimal 21.08|https://github.com/manjaro-arm/rockpi4b-images/releases/download/21.08/Manjaro-ARM-minimal-rockpi4b-21.08.img.xz|d9cebf5299128c0e0e976b8256e74b7f9618958b
ARM|Rock Pi 4B Sway 21.08|https://github.com/manjaro-arm/rockpi4b-images/releases/download/21.08/Manjaro-ARM-sway-rockpi4b-21.08.img.xz|131c4db2f613d5eab96384840a32bd5895801126
ARM|Rock Pi 4B XFCE 21.08|https://github.com/manjaro-arm/rockpi4b-images/releases/download/21.08/Manjaro-ARM-xfce-rockpi4b-21.08.img.xz|7132325b60874635ea353587872bd22c43c5020e
ARM|Rock Pi 4C Gnome 21.08|https://github.com/manjaro-arm/rockpi4c-images/releases/download/21.08/Manjaro-ARM-gnome-rockpi4c-21.08.img.xz|1cbd764a64ed85a502c381187351bd5ce052f4ec
ARM|Rock Pi 4C KDE Plasma 21.08|https://github.com/manjaro-arm/rockpi4c-images/releases/download/21.08/Manjaro-ARM-kde-plasma-rockpi4c-21.08.img.xz|973c71cab14efd61db61d456d5bb3d758a008e3c
ARM|Rock Pi 4C MATE 21.08|https://github.com/manjaro-arm/rockpi4c-images/releases/download/21.08/Manjaro-ARM-mate-rockpi4c-21.08.img.xz|b755fcd9198e005b5929c1d52fbced91cf8a86db
ARM|Rock Pi 4C Minimal 21.08|https://github.com/manjaro-arm/rockpi4c-images/releases/download/21.08/Manjaro-ARM-minimal-rockpi4c-21.08.img.xz|bcc97a5d4dfc7f0e613b3bf74e3d87b23ed9ccf9
ARM|Rock Pi 4C Sway 21.08|https://github.com/manjaro-arm/rockpi4c-images/releases/download/21.08/Manjaro-ARM-sway-rockpi4c-21.08.img.xz|9124c16857f52cff7db3b4e22f595e4985c86492
ARM|Rock Pi 4C XFCE 21.08|https://github.com/manjaro-arm/rockpi4c-images/releases/download/21.08/Manjaro-ARM-xfce-rockpi4c-21.08.img.xz|5b537b36c74dc0428c7dcba12c471905ea781380
ARM|Rock64 Gnome 21.08|https://github.com/manjaro-arm/rock64-images/releases/download/21.08/Manjaro-ARM-gnome-rock64-21.08.img.xz|3796dcc1aef7bdb158343a2d80a0697fe9c652f3
ARM|Rock64 KDE Plasma 21.08|https://github.com/manjaro-arm/rock64-images/releases/download/21.08/Manjaro-ARM-kde-plasma-rock64-21.08.img.xz|0dc6c5508e91579633409f864ae8ce1fc823662a
ARM|Rock64 MATE 21.08|https://github.com/manjaro-arm/rock64-images/releases/download/21.08/Manjaro-ARM-mate-rock64-21.08.img.xz|c52d3fd63050dc6fe056adaa804ddf5d8f303707
ARM|Rock64 Minimal 21.08|https://github.com/manjaro-arm/rock64-images/releases/download/21.08/Manjaro-ARM-minimal-rock64-21.08.img.xz|ec12fdd12c4f3a0f416935568239cba62555c1eb
ARM|Rock64 Sway 21.08|https://github.com/manjaro-arm/rock64-images/releases/download/21.08/Manjaro-ARM-sway-rock64-21.08.img.xz|12599939a15ab39bdfa1fe65c4a761f05a3fad68
ARM|Rock64 XFCE 21.08|https://github.com/manjaro-arm/rock64-images/releases/download/21.08/Manjaro-ARM-xfce-rock64-21.08.img.xz|cc5351d6a15ff22343ac5b71592df38e30e07355
ARM|RockPro64 Gnome 21.08|https://github.com/manjaro-arm/rockpro64-images/releases/download/21.08/Manjaro-ARM-gnome-rockpro64-21.08.img.xz|6b9ad0d451bc632f9e11e0d2c82cd907ed57ba4c
ARM|RockPro64 KDE Plasma 21.08|https://github.com/manjaro-arm/rockpro64-images/releases/download/21.08/Manjaro-ARM-kde-plasma-rockpro64-21.08.img.xz|2f58446cd0b865d51f8172d8e9f7c73b9fed28fc
ARM|RockPro64 MATE 21.08|https://github.com/manjaro-arm/rockpro64-images/releases/download/21.08/Manjaro-ARM-mate-rockpro64-21.08.img.xz|c4665d3949016105c7a5ad31245353aadb6de8b8
ARM|RockPro64 Minimal 21.08|https://github.com/manjaro-arm/rockpro64-images/releases/download/21.08/Manjaro-ARM-minimal-rockpro64-21.08.img.xz|b1b2b74381a14757ea775868467857e122bcb540
ARM|RockPro64 Sway 21.08|https://github.com/manjaro-arm/rockpro64-images/releases/download/21.08/Manjaro-ARM-sway-rockpro64-21.08.img.xz|fad04a92e829e02049b8db991ae9fc086e6464df
ARM|RockPro64 XFCE 21.08|https://github.com/manjaro-arm/rockpro64-images/releases/download/21.08/Manjaro-ARM-xfce-rockpro64-21.08.img.xz|c8cb2dcf2af103699632b9d74c468cbed4c27dbc


## Command

```bash
# change generating date
sed -r -i '/^Generating date/{s@.*@Generating date \*'"$(date +'%b %d, %Y %a')"'\*@g}' EditionsList.md

# remove existed list contents
sed -r -i '/^-{3}\|/,/^$/{/^[[:alpha:]]/{d}}' EditionsList.md
```

<!-- End -->