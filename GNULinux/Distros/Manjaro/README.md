# Manjaro Operation Notes

[Manjaro][manjaro] is an accessible, friendly, open-source Linux distribution and community. Based on [Arch Linux][archlinux], providing all the benefits of cutting-edge software combined with a focus on getting started quickly, automated tools to require less manual intervention, and help readily available when needed. Manjaro is suitable for both newcomers and experienced Linux users.

There are four official editions (Xfce, KDE, GNOME, Architect)of Manjaro available, as well as a number of unofficial �community� editions. It also supports ARM editions.


## TOC

1. [Official Resource](#official-resource)  
2. [Operation Notes](#operation-notes)  
3. [Change Log](#change-log)  


## Official Resource

* Arch Linux [wiki][archlinux_wiki]
* [Manjaro Wiki](https://wiki.manjaro.org/index.php)
* Page [User guide](https://manjaro.org/support/userguide/)

PDF [Manjaro-User-Guide.pdf](https://de.osdn.net/projects/manjaro/storage/Manjaro-User-Guide.pdf) (Nov 01, 2018)


Using the following command to extract its real downloading link

```bash
curl -fsSL -I https://de.osdn.net/projects/manjaro/storage/Manjaro-User-Guide.pdf | sed -r -n '/location:/{p}' | sed '$!d;s@^[^:]*:[[:space:]]*@@g;'

# 01-Nov-2018 07:36
# https://mirrors.xtom.com/osdn/storage/g/m/ma/manjaro/Manjaro-User-Guide.pdf
```

## Operation Notes

* [Getting Manjaro](./Notes/1-0_GettingManjaro.md): including real downloading link extraction, ISO file gpg signature verifcation, making bootable flash drive.
* [Postinstallation Setting Up](./Notes/1-1_PostinstallationSettingUp.md): postinstallation operation


## Change Log

* Mar 06, 2020 Fri 21:02 ET
  * first draft
* Apr 16, 2020 Thu 09:30 ET
  * Moving note *Getting Manjaro* into sub directory [Notes](./Notes)


[manjaro]:https://manjaro.org/ "Manjaro - enjoy the simplicity"
[archlinux]:https://www.archlinux.org "A lightweight and flexible Linux® distribution"
[archlinux_wiki]:https://wiki.archlinux.org/

<!-- End -->
