# Setting Up Manjaro

[Manjaro][manjaro] is based on [Arch Linux][archlinux], just a little difference.


The following commands need to be runnig as ~~`sudo` privilege or~~ `root` user except commands `makepkg` and `yay`. Otherwise it will prompts error info


## TOC

1. [Perparation](#perparation)  
2. [User Management](#user-management)  
2.1 [sudo permission](#sudo-permission)  
2.1.1 [Exception](#exception)  
2.2 [Autologin Enable (optional)](#autologin-enable-optional)  
2.2.1 [For XFCE](#for-xfce)  
2.2.2 [For KDE Plasma](#for-kde-plasma)  
2.3 [New User (optional)](#new-user-optional)  
3. [Package Management](#package-management)  
3.1 [Reflector](#reflector)  
3.2 [Multilib](#multilib)  
3.3 [System Update](#system-update)  
3.4 [AUR](#aur)  
3.5 [Essential Pakcages](#essential-pakcages)  
4. [Unneeded Service](#unneeded-service)  
5. [Timezone And Locale](#timezone-and-locale)  
6. [Input Method And Fonts](#input-method-and-fonts)  
7. [Firewall](#firewall)  
8. [Change Log](#change-log)  

## Perparation

```bash
echo "$USER" > /tmp/login_user
echo "$HOME" > /tmp/login_user_home

# -i, --login    run login shell as the target user; a command may also be specified
sudo -i

login_user=$(cat /tmp/login_user)
login_user_home=$(cat /tmp/login_user_home)
[[ -f /tmp/login_user ]] && rm -f /tmp/login_user
[[ -f /tmp/login_user_home ]] && rm -f /tmp/login_user_home
```

## User Management

See Arch Linux note [System Administration](/GNULinux/Distros/ArchLinux/Notes/2-1_System_Administration.md)

### sudo permission

[sudo](https://wiki.archlinux.org/index.php/Sudo) group name on [Arch Linux][archlinux] and [Manjaro][manjaro] is `wheel`.

Make sure package `sudo` is installed.

```bash
# package sudo exists in group package 'base-devel'
[[ -f /etc/sudoers ]] || pacman -S --noconfirm sudo

# pacman -S --noconfirm vim
# export EDITOR=vim
# visudo
```

Changing file */etc/sudoers* like [Arch Linux][archlinux].


```bash
# Skip password prompt for current user
[[ -f /etc/sudoers.bak ]] || cp /etc/sudoers{,.bak}

[[ -f /etc/sudoers ]] && sed -r -i 's@#*[[:space:]]*(%wheel[[:space:]]+ALL=\(ALL\)[[:space:]]+ALL)@# \1@;s@#*[[:space:]]*(%wheel[[:space:]]+ALL=\(ALL\)[[:space:]]+NOPASSWD: ALL).*@\1,!/bin/su@' /etc/sudoers
```

But it seems not work. Another mehtod is just add specific user

```bash
user_name='pi'

sudo tee -a /etc/sudoers 1> /dev/null << EOF
$user_name ALL=(ALL) NOPASSWD: ALL
EOF
```

Rebooting system for applying setting.

#### Exception

If not working, executing the following commands which following post [Bypass prompt root permissions password everywhere](https://forum.manjaro.org/t/howto-bypass-prompt-root-permissions-password-everywhere/42661) from official forum.

```bash
# https://forum.antergos.com/topic/5589/nopasswd-all-in-sudoers-file-but-still-asking-for-password
# remove file /etc/sudoers.d/10-installer which contains '%wheel ALL=(ALL) ALL', Arch Linux doesn't has this file
[[ -f /etc/sudoers.d/10-installer ]] && rm -f /etc/sudoers.d/10-installer

# Skip password prompt for current user
[[ -f /etc/sudoers.bak ]] && cp /etc/sudoers.bak /etc/sudoers

# echo "Defaults:$login_user !authenticate" >> /etc/sudoers
tee -a /etc/sudoers 1> /dev/null << EOF
Defaults:$login_user !authenticate
EOF

# Create new Polkit rule
tee /etc/polkit-1/rules.d/49-nopasswd_global.rules 1> /dev/null << EOF
/* Allow members of the wheel group to execute any actions
 * without password authentication, similar to "sudo NOPASSWD:"
 */
polkit.addRule(function(action, subject) {
    if (subject.isInGroup("wheel")) {
        return polkit.Result.YES;
    }
});
EOF
```

Just for KDE Plasma

```bash
kdesurc_path="${login_user_home}/.config/kdesurc"
tee "${kdesurc_path}" 1> /dev/null << EOF
[super-user-command]
super-user-command=sudo
EOF

chown "${login_user}":"${login_user}" "${kdesurc_path}"

tee -a /etc/environment 1> /dev/null << EOF
KDE_FULL_SESSION=true
KDE_SESSION_VERSION=5
EOF
```

### Autologin Enable (optional)

To login automatically without entering password.

#### For XFCE

**XFCE** uses [LightDM](https://wiki.archlinux.org/index.php/LightDM) as desktop display manager. Following official document [Enabling autologin](https://wiki.archlinux.org/index.php/LightDM#Enabling_autologin) and forum post [Enable Autologin in XFCE](https://forum.manjaro.org/t/enable-autologin-in-xfce/34489).

```bash
lightdm_conf='/etc/lightdm/lightdm.conf'

if [[ -f "${lightdm_conf}" ]]; then
    sed -r -i '/^\[Seat:/,/\[/{/autologin-user[[:space:]]*=/{s@^#*[[:space:]]*([^=]+=).*@\1'"${login_user}"'@g;}}' "${lightdm_conf}"
    groupadd -r autologin
    # gpasswd -a "${login_user}" autologin
    usermod -a -G autologin "${login_user}"
fi
```

#### For KDE Plasma

**KDE** chose [SDDM](https://wiki.archlinux.org/index.php/SDDM) to be the successor of the KDE Display Manager for KDE Plasma 5. Following official document [Autologin](https://wiki.archlinux.org/index.php/SDDM#Autologin) and forum post [turn off auto login on kde spin](https://forum.manjaro.org/t/turn-off-auto-login-on-kde-spin/117116/11).

```bash
# Change file /etc/sddm.conf.d/kde_settings.conf

sed -r -i '/\[Autologin/,/^User/{/^User/{s@.*@&,'"${login_user}"'@g;s@=,@=@g;}}' /etc/sddm.conf.d/kde_settings.conf
```

Here has a issue: after entering GUI interface, it will prompts *KDE Wallet Service* needs to input user password again.

![KDE Wallet Service](https://i.stack.imgur.com/Jb0ut.png)

Following ArchWiki [Unlock KDE Wallet automatically on login](https://wiki.archlinux.org/index.php/KDE_Wallet#Unlock_KDE_Wallet_automatically_on_login) doesn't work.

>The wallet cannot be unlocked when using autologin.


### New User (optional)

Add new normal user and add it into sudo group *wheel*. This user is forced to change password while first login in.

```bash
normal_user_name=${normal_user_name:-'manjaro'}
default_password="Manjaro_$(date +'%Y')"  # eg This year is 2020, password is Manjaro_2020

login_shell=${login_shell:-'/bin/bash'}
useradd -m -N -G wheel -s "${login_shell}" "${normal_user_name}"
# useradd -mN -G wheel "${normal_user_name}"
# usermod -s "${login_shell}" "${normal_user_name}" # change login shell

echo "${normal_user_name}:${default_password}" | chpasswd
# echo -e "${default_password}\n${default_password}" | passwd "${normal_user_name}"

# chage -d0 "${normal_user_name}"  # optional, new created user have to change passwd when first login
```

## Package Management

See Arch Linux notes [Mirror Selection](/GNULinux/Distros/ArchLinux/Notes/1-3_Installation.md), [Package Management](/GNULinux/Distros/ArchLinux/Notes/2-2_Package_Management.md).

### Reflector

~~Using [Reflector](https://wiki.archlinux.org/index.php/Reflector) to generate fastest mirror list into file */etc/pacman.d/mirrorlist* automatically.~~

It seems that [Reflector package **not** in Repository](https://forum.manjaro.org/t/reflector-package-not-in-repository/63464) on [Manjaro][manjaro].

File */etc/pacman.d/mirrorlist* contains one line

>Use pacman-mirrors to modify

[Pacman-mirrors][pacman-mirrors] is a [Manjaro][manjaro] specific utility for generating and maintaining the system mirrorlist. [Pacman-mirrors][pacman-mirrors] uses the information available on the [Manjaro Repository](https://repo.manjaro.org/) page.

[Manjaro][manjaro] has service `pamac-mirrorlist.service`, timer `pamac-mirrorlist.timer`, service is diabled and timer is enabled by default.

```bash
# /usr/lib/systemd/system/pamac-mirrorlist.service
[Unit]
Description=Generate mirrorlist
Wants=network-online.target
After=network-online.target

[Service]
Type=oneshot
ExecStart=/usr/bin/pacman-mirrors -f8


# /usr/lib/systemd/system/pamac-mirrorlist.timer
[Unit]
Description=Generate mirrorlist weekly

[Timer]
OnCalendar=Thu *-*-* 7:00:00
RandomizedDelaySec=15h
Persistent=true

[Install]
WantedBy=timers.target
```

Here using custom pamac-mirrorlist service

```bash
# configure file: /etc/pacman-mirrors.conf
systemctl disable pamac-mirrorlist.service
systemctl disable pamac-mirrorlist.timer


# - Temporarily
# Available countries list: Australia, Denmark, Germany, Switzerland, United_Kingdom, United_States
country_choose='United_States'

pacman-mirrors -g -m rank -B stable -a -c "${country_choose}" -t 3

# - Permanmently
# https://wiki.archlinux.org/index.php/Reflector#Systemd_service
tee /etc/systemd/system/reflector.service 1>/dev/null << EOF
[Unit]
Description=Pacman mirrorlist update
Wants=network-online.target
# After=network-online.target

[Service]
Type=oneshot
ExecStart=/usr/bin/pacman-mirrors -g -m rank -b stable -t 3

[Install]
RequiredBy=multi-user.target
EOF

# sed ':a;N;$!ba;s@\n@\\n@g' /etc/systemd/system/reflector.service
# echo -e "[Unit]\nDescription=Pacman mirrorlist update\nWants=network-online.target\n# After=network-online.target\n\n[Service]\nType=oneshot\nExecStart=/usr/bin/pacman-mirrors -g -m rank -b stable -t 3\n\n[Install]\nRequiredBy=multi-user.target" > /etc/systemd/system/reflector.service

# https://wiki.archlinux.org/index.php/Reflector#Systemd_timer
# https://wiki.archlinux.org/index.php/Systemd/Timers
tee /etc/systemd/system/reflector.timer 1>/dev/null <<EOF
[Unit]
Description=Run pacman-mirrors periodically

[Timer]
OnBootSec=20min
OnUnitActiveSec=8h
RandomizedDelaySec=30min
# OnCalendar=weekly
# RandomizedDelaySec=15h
Persistent=true

[Install]
WantedBy=timers.target
EOF

# sed ':a;N;$!ba;s@\n@\\n@g' /etc/systemd/system/reflector.timer
# echo -e "[Unit]\nDescription=Run pacman-mirrors periodically\n\n[Timer]\nOnBootSec=20min\nOnUnitActiveSec=8h\nRandomizedDelaySec=30min\n# OnCalendar=weekly\n# RandomizedDelaySec=15h\nPersistent=true\n\n[Install]\nWantedBy=timers.target" > /etc/systemd/system/reflector.timer

systemctl disable reflector.service
systemctl enable reflector.timer # just enable reflector.timer
```

### Multilib

Enable multilib (optional, if you plan on using 32-bit applications).

```bash
# https://wiki.archlinux.org/index.php/Multilib
# https://wiki.archlinux.org/index.php/Official_repositories#Enabling_multilib
[[ -f /etc/pacman.conf ]] && sed -r -i '/^#*\[multilib\]/,/^$/{/^#+/{s@^#*@@g;}}' /etc/pacman.conf
```

### System Update

System update

```bash
# https://wiki.archlinux.org/index.php/System_maintenance#Avoid_certain_pacman_commands
# Avoid doing partial upgrades. In other words, never run pacman -Sy; instead, always use pacman -Syu.
# Passing two --refresh or -y flags will force a refresh of all package databases, even if they appear to be up-to-date.  -- man pacman
pacman -Syyu --noconfirm

# pacman -Qtdq | xargs pacman -Rns --noconfirm 2> /dev/null
# pacman -Sc --noconfirm
```

### AUR

Arch User Repository (AUR)

```bash
# AUR needs package base-devel
pacman -S --needed --noconfirm base-devel
```

makepkg is a script to automate the building of packages. The requirements for using the script are a build-capable Unix platform and a PKGBUILD.

```bash
makepkg_conf='/etc/makepkg.conf'

# Enable multi-core processors (number 8)
processor_num=${processor_num:-0}
processor_num=$(nproc 2> /dev/null) # /usr/bin/nproc is owned by coreutils
[[ "${processor_num}" -eq 0 ]]&& processor_num=$(sed -r -n '/^processor/{p}' /proc/cpuinfo | wc -l)
[[ "${processor_num}" -gt 0 ]] && sed -r -i '/MAKEFLAGS=/{s@^#@@g;s@([^=]+=).*@\1"-j4"@g;}' "${makepkg_conf}"

# https://wiki.archlinux.org/index.php/Makepkg#Utilizing_multiple_cores_on_compression
# COMPRESSXZ     # xz    -T threads, --threads=threads    Specify  the  number of worker threads to use.  Setting threads to a special value 0 makes xz use as many threads as there are CPU cores on the system.
sed -r -i '/^COMPRESSXZ/{s@(.*-c).*?(-z.*)$@\1 -T 0 \2@g;}' "${makepkg_conf}"

# Building from files in memory
# The BUILDDIR variable can be temporarily exported to makepkg to set the build directory to an existing tmpfs.
# https://wiki.archlinux.org/index.php/Tmpfs#Usage
sed -r -i '/^#*BUILDDIR=/{s@^#*@@g;}' "${makepkg_conf}"

# Create uncompressed packages
# https://wiki.archlinux.org/index.php/Makepkg#Use_other_compression_algorithms
# https://wiki.manjaro.org/index.php?title=Makepkg#Create_uncompressed_packages
sed -r -i '/^#*PKGEXT=/{s@^#*([^=]+=).*@\1'\''.pkg.tar'\'' # .pkg.tar.xz create uncompressed packages@g;}' "${makepkg_conf}"

# Enable ccache for makepkg
# https://wiki.archlinux.org/index.php/Ccache
pacman -S --noconfirm ccache
# ccache -s # ~/.ccache/ccache.conf /etc/ccache.conf  max_size = 5.0G
# ccache -C # clear cache completely
sed -r -i '/^BUILDENV=/{s@!(ccache)@\1@g;}' "${makepkg_conf}"
```

Choosing [Yet another Yogurt](https://github.com/Jguer/yay "An AUR Helper written in Go") (yay) to install packages in AUR.

```bash
temp_yay_dir='/tmp/yay-bin'
mkdir -p "${temp_yay_dir}"
chmod 777 "${temp_yay_dir}" # directory yay-bin needs write permission

# - mothod 1
pacman -S --noconfirm git # base-devel see https://wiki.archlinux.org/index.php/Arch_User_Repository#Prerequisites
git clone https://aur.archlinux.org/yay-bin.git "${temp_yay_dir}"

# - method 2
# curl -fsL https://aur.archlinux.org/cgit/aur.git/snapshot/yay-bin.tar.gz | tar xfz - -C "${temp_yay_dir}" --strip-components=1


cd "${temp_yay_dir}"
# ERROR: Running makepkg as root is not allowed as it can cause permanent, catastrophic damage to your system
sudo -u "${login_user}" makepkg -si --noconfirm

# -s/--syncdeps automatically resolves and installs any dependencies with pacman before building. If the package depends on other AUR packages, you will need to manually install them first.
# -i/--install installs the package if it is built successfully. Alternatively the built package can be installed with pacman -U package_name.pkg.tar.xz.
cd ..
[[ -d "${temp_yay_dir}" ]] && rm -rf "${temp_yay_dir}"

# If run yay as root/sudo, prompt error info 'Please avoid running yay as root/sudo.' and 'Refusing to install AUR Packages as root, Abourting.'.
sudo -u "${login_user}" yay -Syu --cleanafter --noconfirm

# yay -Yc --noconfirm
# yay -Ps
```

### Essential Pakcages

```bash
# https://wiki.archlinux.org/index.php/File_systems
pacman -S --noconfirm exfat-utils
pacman -S --noconfirm bash bash-completion openssh gawk sed curl

pacman -S --noconfirm vlc

# vim editor
pacman -S --noconfirm vim

# axdsop boost tool intallation, running as normal user
curl -fsL https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/axdsop_boost_tool.sh | bash -s -- -i
# bash ~/.config/axdsop/deploy.sh -h
```

Unneeded packages

```bash
pacman -R --noconfirm smplayer mpv mntray
```

## Unneeded Service

```bash
# https://askubuntu.com/questions/795226/how-to-list-all-enabled-services-from-systemctl
# systemctl list-unit-files --state=enabled
# systemctl list-unit-files --type service --state enabled,generated
# systemctl list-units --type=service --state=running

systemctl disable systemd-networkd-wait-online.service
systemctl disable NetworkManager-wait-online.service
systemctl disable bootsplash-hide-when-booted.service

# Bluetooth
systemctl disable bluetooth.service
systemctl disable attach-bluetooth.service # Attach Bluetooth Adapter

# Samba
systemctl disable smb.service # Samba SMB Daemon
systemctl disable nmb.service # Samba NMB Daemon
```


## Timezone And Locale

See Arch Linux note [Configure The System](/GNULinux/Distros/ArchLinux/Notes/1-4_Configure_The_System.md).

timezone setting

```bash
# https://wiki.archlinux.org/index.php/System_time#Time_zone
time_zone=${time_zone:-'America/New_York'}
# ipinfo.io   The limit is 1,000 requests per day from an IP address.
# ip-api.com  The limit is 150 requests per minute from an IP address.
time_zone=$(timeout 3 curl ipinfo.io/timezone 2> /dev/null || timeout 3 curl ip-api.com/line/?fields=timezone 2> /dev/null)

ln -sf /usr/share/zoneinfo/${time_zone} /etc/localtime

# run hwclock(8) to generate /etc/adjtime
hwclock --systohc

# internet time synchronize
timedatectl set-ntp true
timedatectl status
```

locale setting

```bash
# https://wiki.archlinux.org/index.php/Locale

# find a list of enabled locales
locale -a

country_code=${country_code:-'US'} # US or United States
# ipinfo.io    The limit is 1,000 requests per day from an IP address.
# ip-api.com  The limit is 150 requests per minute from an IP address.
country_code=$(timeout 3 curl ipinfo.io/country 2> /dev/null || timeout 3 curl ip-api.com/line/?fields=countryCode 2> /dev/null)
sed -r -i '/^#?[^[:space:]]+_'"${country_code^^}"'\.UTF-8/{s@^#?[[:space:]]*@@g;}' /etc/locale.gen

# zh_TW.UTF-8 / zh_CN.UTF-8
# sed -r -i '/^#?[^[:space:]]+_CN\.UTF-8/{s@^#?[[:space:]]*@@g;}' /etc/locale.gen
# sed -r -i '/^#?[^[:space:]]+_TW\.UTF-8/{s@^#?[[:space:]]*@@g;}' /etc/locale.gen

locale_default=${locale_default:-'en_US.UTF-8'}  # zh_TW.UTF-8 / zh_CN.UTF-8
locale_list=$(sed -r -n '/^#?[^[:space:]]+_'"${country_code^^}"'\.UTF-8/{s@^#?[[:space:]]*@@g;s@^([^[:space:]]+).*$@\1@g;p}' /etc/locale.gen)
locale_list_count=$(echo "${locale_list}" | wc -l)
[[ "${locale_list_count}" -eq 1 ]] && locale_default="${locale_list}"

# generate locales from /etc/locale.gen
locale-gen

localectl list-locales

# Setting the system locale
# https://wiki.archlinux.org/index.php/Locale#Setting_the_system_locale
# The /etc/locale.conf file configures system-wide locale settings. It is read at early boot by systemd(1).   # https://jlk.fjfi.cvut.cz/arch/manpages/man/locale.conf.5
# if file /etc/locale.conf not exists or not locale, gnome-terminal will fail to start
echo "LANG=${locale_default}" > /etc/locale.conf
# localectl set-locale LANG=${locale_default}

export LANG="${locale_default}"
```

## Input Method And Fonts

Input method - Fcitx (not work ?)

```bash
pacman -S --noconfirm fcitx-rime fcitx-im fcitx-configtool
# Run fcitx-config-gtk3 after fcitx-configtool is installed. Unset Only Show Current Language if you want to enable an input method for a different language.
echo -e "GTK_IM_MODULE=fcitx\nQT_IM_MODULE=fcitx\nXMODIFIERS=@im=fcitx" > "${login_user_home}/.pam_environment"  # ~/.xprofile ?
# fcitx-remote is a commandline tool that can be used to control the fcitx state.
# fcitx-remote -r
```

Fonts

```bash
# https://wiki.archlinux.org/index.php/Fonts

pacman -S --noconfirm ttf-freefont wqy-microhei # # 1.47 MiB
# ttf-hanazono - 花園明朝(HanaMin) 20.9 MB
pacman -S --noconfirm ttf-hanazono
# pacman -S --noconfirm adobe-source-han-sans-otc-fonts adobe-source-han-serif-otc-fonts # 176 MiB
# pacman -S --noconfirm noto-fonts noto-fonts-cjk # 146 MiB
# sudo -u "${normal_user_default}" yay -S --cleanafter --noconfirm ttf-consolas-with-yahei
```

## Firewall

Choosing `ufw` as firewall utility, see Arch Linux note [Networking](/GNULinux/Distros/ArchLinux/Notes/2-7_Networking.md).

```bash
# https://help.ubuntu.com/community/UFW

pacman -S --noconfirm ufw
# Disable ipv6
[[ -f /etc/default/ufw ]] && sed -r -i '/^IPV6=/{s@(IPV6=).*@\1no@g;}' /etc/default/ufw 2> /dev/null
```

Reboot then executing the following commands, otherwise it will pormpts error message like post [iptables problem with ufw](https://forum.manjaro.org/t/solved-iptables-problem-with-ufw/43182).


```bash
# setting default rule (deny incoming, allow outgoing)
ufw default deny incoming
ufw default allow outgoing

# allow SSH
ufw allow ssh
# ufw allow from 192.168.0.0/24 to any port 22
# ufw limit from 192.168.0.0/24 to any port 22


ufw --force enable # non-interactive
# ufw enable # interactive  Command may disrupt existing ssh connections. Proceed with operation (y|n)?

ufw status verbose

systemctl enable ufw.service
systemctl is-enabled ufw.service
```

## Static IP

Try official wiki [Setting Static IP Address](https://wiki.manjaro.org/index.php/Networking#Setting_Static_IP_Address), blog [How to setup a static IP address on Manjaro Linux](https://linuxconfig.org/how-to-setup-a-static-ip-address-on-manjaro-linux). But it doesn't work via command line method.


## Change Log

* Oct 28, 2019 Mon 20:17 ET
  * first draft
* Oct 29, 2019 Tue 21:03 ET
  * Add postinstallation operation
* Oct 30, 2019 09:29 Wed ET
  * Add *Pacman-mirrors* systemd automation for Manjaro
* Oct 30, 2019 23:35 Wed ET
  * Running commands in `sudo -i`, commands optimization
* Nov 01, 2019 14:23 Fri ET
  * Add autologin in XFCE desktop environment
* Mar 06, 2020 Fri 19:06 ET
  * Upgrade version to `20.02`
* Mar 07, 2020 Sat 11:05 ET
  * Add operation for KDE Plasma, fix sudo configuration
* Apr 16, 2020 Thu 19:20 ET
  * Move postinstallation operation into seperate note


[manjaro]:https://manjaro.org/ "Manjaro - enjoy the simplicity"
[archlinux]:https://www.archlinux.org "A lightweight and flexible LinuxÂ® distribution"
[pacman]:https://wiki.archlinux.org/index.php/Pacman
[pacman-mirrors]:https://wiki.manjaro.org/Pacman-mirrors

<!-- End -->
