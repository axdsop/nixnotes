# Configure The System

This note is based official document [Installation guide][installation_guide]. It is a guide for installing [Arch Linux][archlinux] from the live system booted with the official installation image.

## TOC

1. [Fstab](#fstab)  
2. [Chroot](#chroot)  
3. [Time zone](#time-zone)  
4. [Localization](#localization)  
5. [Network configuration](#network-configuration)  
6. [Initramfs](#initramfs)  
7. [Root password](#root-password)  
8. [Boot loader](#boot-loader)  
9. [Reboot](#reboot)  
10. [Change Log](#change-log)  


## Fstab

Generating an fstab file

>Check the resulting file in */mnt/etc/fstab* afterwards, and edit it in case of errors.

```bash
genfstab -U /mnt >> /mnt/etc/fstab
```

## Chroot

Change root into the new system

[Chroot#Using_arch-chroot](https://wiki.archlinux.org/title/Chroot#Using_arch-chroot) says:

>The bash script `arch-chroot` is part of the `arch-install-scripts` package. Before it runs `/usr/bin/chroot`, the script mounts api filesystems like */proc* and makes */etc/resolv.conf* available from the chroot.

```bash
# https://wiki.archlinux.org/title/chroot#Using_arch-chroot
arch-chroot /mnt

# https://wiki.archlinux.org/title/Chroot#Run_graphical_applications_from_chroot
# $DISPLAY
```


## Time zone

```bash
# https://wiki.archlinux.org/title/System_time#Time_zone
time_zone=${time_zone:-'America/New_York'}
# ipinfo.io   The limit is 1,000 requests per day from an IP address.
# ip-api.com  The limit is 150 requests per minute from an IP address.
time_zone=$(timeout 3 curl ipinfo.io/timezone 2> /dev/null || timeout 3 curl ip-api.com/line/?fields=timezone 2> /dev/null)

ln -sf /usr/share/zoneinfo/${time_zone} /etc/localtime

# run hwclock(8) to generate /etc/adjtime
hwclock --systohc

# internet time synchronize
# https://wiki.archlinux.org/title/Systemd-timesyncd
# https://unix.stackexchange.com/questions/336566/how-to-fix-time-in-arch-linux
timesyncd_conf='/etc/systemd/timesyncd.conf'

if [[ -f "${timesyncd_conf}" ]]; then
    # Sometimes domain pool.ntp.org fail to resolve, so add other ntp servers provides by nist, windows, apple, google
    ntp_list='0.pool.ntp.org 1.pool.ntp.org 2.pool.ntp.org'
    # 0.arch.pool.ntp.org 1.arch.pool.ntp.org 2.arch.pool.ntp.org 3.arch.pool.ntp.org 
    ntp_fallback_list='time.nist.gov time.windows.com time.apple.com time.google.com'
    sed -r -i '/\[Time\]/,${/^#*[[:space:]]*NTP=/{s@.*@NTP='"${ntp_list}"'@g}; /^#*[[:space:]]*FallbackNTP=/{s@.*@FallbackNTP='"${ntp_fallback_list}"'@g}}' "${timesyncd_conf}"
fi

timedatectl set-ntp true
systemctl restart systemd-timesyncd.service
timedatectl status
timedatectl show-timesync --all
```


## Localization

```bash
# https://wiki.archlinux.org/title/Locale

# find a list of enabled locales
locale -a

country_code=${country_code:-'US'} # US or United States
# ipinfo.io    The limit is 1,000 requests per day from an IP address.
# ip-api.com  The limit is 150 requests per minute from an IP address.
country_code=$(timeout 3 curl ipinfo.io/country 2> /dev/null || timeout 3 curl ip-api.com/line/?fields=countryCode 2> /dev/null)
sed -r -i '/^#?[^[:space:]]+_'"${country_code^^}"'\.UTF-8/{s@^#?[[:space:]]*@@g;}' /etc/locale.gen

# zh_TW.UTF-8 / zh_CN.UTF-8
# sed -r -i '/^#?[^[:space:]]+_CN\.UTF-8/{s@^#?[[:space:]]*@@g;}' /etc/locale.gen
# sed -r -i '/^#?[^[:space:]]+_TW\.UTF-8/{s@^#?[[:space:]]*@@g;}' /etc/locale.gen

locale_default=${locale_default:-'en_US.UTF-8'}  # zh_TW.UTF-8 / zh_CN.UTF-8
locale_list=$(sed -r -n '/^#?[^[:space:]]+_'"${country_code^^}"'\.UTF-8/{s@^#?[[:space:]]*@@g;s@^([^[:space:]]+).*$@\1@g;p}' /etc/locale.gen)
locale_list_count=$(echo "${locale_list}" | wc -l)
[[ "${locale_list_count}" -eq 1 ]] && locale_default="${locale_list}"

# generate locales from /etc/locale.gen
locale-gen

localectl list-locales

# Setting the system locale
# https://wiki.archlinux.org/title/Locale#Setting_the_system_locale
# The /etc/locale.conf file configures system-wide locale settings. It is read at early boot by systemd(1).   # https://jlk.fjfi.cvut.cz/arch/manpages/man/locale.conf.5
# if file /etc/locale.conf not exists or not locale, gnome-terminal will fail to start
echo "LANG=${locale_default}" > /etc/locale.conf
# localectl set-locale LANG=${locale_default}

export LANG="${locale_default}"
```


## Network configuration

```bash
hostname_default=${hostname_default:-'MBXP'}

# Create the hostname file
# https://wiki.archlinux.org/title/Network_configuration#Set_the_hostname
echo "${hostname_default}" > /etc/hostname

# Add matching entries to hosts(5)
# https://wiki.archlinux.org/title/Network_configuration#Local_hostname_resolution
echo -e "127.0.0.1   localhost\n::1         localhost\n127.0.1.1   ${hostname_default}.localdomain  ${hostname_default}" >> /etc/hosts
```


## Initramfs

Creating a new initramfs is usually not required, because `mkinitcpio` was run on installation of the `linux` package with `pacstrap`.

```bash
# For LVM, system encryption or RAID, modify mkinitcpio.conf(5) and recreate the initramfs image
# https://wiki.archlinux.org/title/Mkinitcpio#Image_creation_and_activation
mkinitcpio -p linux
```


## Root password

```bash
default_password="ArchLinux_$(date +'%Y')"  # If this year is 2019, password is ArchLinux_2019

# method 1  For apt
echo "root:${default_password}" | chpasswd
# method 2  For zypper
# echo -e "${default_password}\n${default_password}" | passwd root

# new created user have to change passwd when first login
# Don't execute 'chage -d0 root' here, it will prompts error 'You are required to change your password immediately (administrator enforced)', 'useradd: PAM: Authentication token is no longer valid; new one required'
# chage -d0 root
```


## Boot loader

[GRUB](https://www.gnu.org/software/grub/) (GRand Unified Bootloader) is a multi-boot loader.

[GRUB - UEFI systems](https://wiki.archlinux.org/title/GRUB#Installation_2) says:

>When installing to use UEFI it is important to boot the installation media in UEFI mode, otherwise *efibootmgr* will not be able to add the GRUB UEFI boot entry. Installing to the fallback boot path will still work even in BIOS mode since it does not touch the NVRAM.
>
>To boot from a disk using UEFI, an EFI system partition is required. Follow [EFI system partition#Check](https://wiki.archlinux.org/title/EFI_system_partition#Check_for_an_existing_partition) for an existing partition to find out if you have one already, otherwise you need to create it.

[GRUB#Detecting_other_operating_systems](https://wiki.archlinux.org/title/GRUB#Detecting_other_operating_systems) says:

>To have *grub-mkconfig* search for other installed systems and automatically add them to the menu, install the `os-prober` package and mount the partitions that contain the other systems. Then re-run *grub-mkconfig*.
>
>Partitions containing Windows will be automatically discovered by `os-prober`. However, NTFS partitions may not always be detected when mounted with the default Linux drivers. If GRUB is not detecting it, try installing `ntfs-3g` and remounting.

In order to [dual boot with Windows](https://wiki.archlinux.org/title/Talk:Dual_boot_with_Windows#Begin), we need to install package `os-prober`, `ntfs-3g` to detect it.


```bash
# https://wiki.archlinux.org/title/GRUB#Detecting_other_operating_systems
pacman -S --noconfirm os-prober ntfs-3g
```

Install the packages `grub` and `efibootmgr`: GRUB is the bootloader while efibootmgr is used by the GRUB installation script to write boot entries to NVRAM.

```bash
# https://wiki.archlinux.org/title/GRUB#Installation_2
pacman -S --noconfirm grub efibootmgr

os-prober
```

Install the GRUB EFI application `grubx64.ef`i to */boot* and install its modules to */boot/grub/x86_64-efi/*.

```bash
# https://wiki.archlinux.org/title/GRUB#Installation_2
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=grub
```

If you have an Intel or AMD CPU, enable microcode updates.

```bash
# https://wiki.archlinux.org/title/Arch_boot_process#Boot_loader

# https://wiki.archlinux.org/title/Microcode
# amd-ucode / intel-ucode
model_name=$(sed -r -n '/model name/I{s@.*@\L&@p;q}' /proc/cpuinfo 2> /dev/null)
if [[ "${model_name}" =~ intel ]]; then
    pacman -S --noconfirm intel-ucode
elif [[ "${model_name}" =~ amd ]]; then
    pacman -S --noconfirm amd-ucode
fi
```

Change grub boot menu font

```bash
[[ -f /etc/default/grub ]] && sed -r -i '/^#*GRUB_GFXMODE/{s@^#*@@g;s@^([^=]+=).*@\11600x1200x32@g;}' /etc/default/grub
```

Disable submenu

```bash
# https://wiki.archlinux.org/title/GRUB/Tips_and_tricks#Disable_submenu
sed -r -i '/^#*[[:space:]]*GRUB_DISABLE_SUBMENU=/d' /etc/default/grub
sed -r -i '/^#*[[:space:]]*GRUB_TIMEOUT=/a GRUB_DISABLE_SUBMENU=y' /etc/default/grub
```

`grub-mkconfig` will automatically detect the microcode update and configure GRUB appropriately. After installing the microcode package, regenerate the GRUB config to activate loading the microcode update.

Use the grub-mkconfig tool to generate */boot/grub/grub.cfg*

```bash
# https://wiki.archlinux.org/title/GRUB#Generated_grub.cfg
# https://wiki.archlinux.org/title/Microcode#Automatic_method
# Enabling early microcode updates
grub-mkconfig -o /boot/grub/grub.cfg

# check if Windows 10 entry exists
# cat /boot/grub/grub.cfg
```

[GRUB#Additional_arguments](https://wiki.archlinux.org/title/GRUB#Additional_arguments) says:

>For generating the GRUB recovery entry you have to ensure that `GRUB_DISABLE_RECOVERY` is not set to `true` in */etc/default/grub*.

```bash
# Uncomment to disable generation of recovery mode menu entries
[[ -f /etc/default/grub ]] && sed -r -i '/GRUB_DISABLE_RECOVERY=/{s@^@#@g;}' /etc/default/grub
```


## Reboot

>Exit the chroot environment by typing `exit` or pressing <kbd>Ctrl</kbd>+<kbd>D</kbd>.
>
>Optionally manually unmount all the partitions with `umount -R /mnt`: this allows noticing any "busy" partitions, and finding the cause with fuser.
>
>Finally, restart the machine by typing `reboot`: any partitions still mounted will be automatically unmounted by systemd. Remember to remove the installation media and partitFormatthen login into the new system with the root account.

```bash
# exit the chroot environment
exit

# unmount all the partitions
umount -R /mnt

# restart machine
reboot
```


## Change Log

* Jun 25, 2019 19:42 Tue ET
  * First draft


[archlinux]:https://www.archlinux.org "A lightweight and flexible Linux� distribution"
[installation_guide]:https://wiki.archlinux.org/title/Installation_guide "Guide through the process of installing Arch Linux."


<!-- End -->
