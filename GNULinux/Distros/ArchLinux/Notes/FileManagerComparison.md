# Arch Linux File Manager Comparison

In order to install as less packages as possible in i3 environment. I choose file manager `Thunar`.


## Official Resources

Arch Linux wiki

* [Category:File managers](https://wiki.archlinux.org/title/Category:File_managers)
* [List of applications/Utilities](https://wiki.archlinux.org/title/List_of_applications/Utilities#File_managers)


```bash
# remove unneeded packages
pacman -Qtdq | xargs sudo pacman -Rns --noconfirm
yay -Yc --noconfirm

# list installed packages summary info
yay -Ps
pacman -Qm
```

## File Manager

### Thunar

[Thunar](https://wiki.archlinux.org/title/Thunar) is a modern file manager for the [Xfce](wiki.archlinux.org/index.php/Xfce) Desktop Environment.

#### Packages

```bash
sudo pacman -S thunar
# Essential dependencies: exo, libxfce4ui

# The binary file 'xfce4-about' is provided by 'libxfce4ui' which is required by package 'exo' (Application library for Xfce)

Optional dependencies for thunar
    gvfs: for trash support, mounting with udisk and remote filesystems
    xfce4-panel: for trash applet
    tumbler: for thumbnail previews
    thunar-volman: manages removable devices
    thunar-archive-plugin: create and deflate archives
    thunar-media-tags-plugin: view/edit id3/ogg tags
```


[exo](https://www.archlinux.org/packages/extra/x86_64/exo/) is an application library for Xfce. It provides binary utility `exo-open`, `exo-preferred-applications`.

According to Arch Linux wiki [Xfce](https://wiki.archlinux.org/title/Xfce#Preferred_Applications_preferences_have_no_effect), we know `exo-open` can ack like [xdg-open](https://wiki.archlinux.org/title/Xdg-open) to open a preferred application for a given file or URL.

Desktop file path `/usr/share/applications/exo-*.desktop`

Name|Icon|Exec|
---|---|---
Preferred Applications|preferences-desktop-default-applications|`exo-preferred-applications`
File Manager|system-file-manager|`exo-open --launch FileManager %u`
Mail Reader|emblem-mail|`exo-open --launch MailReader %u`
Terminal Emulator|utilities-terminal|`exo-open --launch TerminalEmulator`
Web Browser|web-browser|`exo-open --launch WebBrowser %u`

If you don't wanna show these in selection menu. You can rename these desktop file.

```bash
sudo find /usr/share/applications/ -type f -name 'exo-*.desktop' -exec mv {}{,.hide} \;

sudo mv thunar-settings.desktop{,.hide}
sudo mv xfce4-about.desktop{,.hide}
```

##### tumbler

```bash
# tumbler: for thumbnail previews
sudo pacman -S tumbler  # just 1 package

Optional dependencies for tumbler
    ffmpegthumbnailer: for video thumbnails
    poppler-glib: for PDF thumbnails # Poppler is a library for rendering PDF files, and examining or modifying their structure.
    libgsf: for ODF thumbnails
    libopenraw: for RAW thumbnails
    freetype2: for font thumbnails [installed]
```

Type|Package|Dependencies<br/>Download Size|Note
---|---|---|---
video thumbnails|ffmpegthumbnailer|**39**<br/>190.13 MiB (installed size)|
PDF thumbnails|poppler-glib|2<br/>1.57 MiB|
ODF thumbnails|libgsf|1<br/>0.31 MiB|
RAW thumbnails|libopenraw|1<br/>0.59 MiB
font thumbnails|freetype2|1<br/>1.58 MiB


##### thunar-archive-plugin

```bash
# thunar-archive-plugin: create and deflate archives
sudo pacman -S thunar-archive-plugin  # just 1 package

Optional dependencies for thunar-archive-plugin
    file-roller
    engrampa
    ark
    xarchiver
```

Archive Plugin — Plugin which allows you to create and extract archive files using contextual menu items.

Archive Tool|Package|Dependencies<br/>Download Size|Desktop Environment
---|----|----|---
File Roller|file-roller|1<br/>3.99 MiB|GNome
Engrampa|engrampa|1<br/>1.18 MiB|MATE
Xarchiver|xarchiver|1<br/>0.49 MiB|GTK+ frontend to various command line archivers
Ark|ark|**67**<br/>48.33 MiB|KDE


#### Install

Total install **13** packages with 24.45 MiB installed size.

```bash
sudo pacman -S --noconfirm thunar

sudo pacman -S --noconfirm tumbler freetype2 libgsf libopenraw \
                           poppler-glib
                           # ffmpegthumbnailer

sudo pacman -S --noconfirm thunar-archive-plugin \
                           xarchiver

# ffmpegthumbnailer need to install another (38+1) packages

# optional operation
applications_dir=${applications_dir:-'/usr/share/applications'}
sudo find "${applications_dir}" -type f -name 'exo-*.desktop' -exec mv {}{,.hide} \;
sudo mv ${applications_dir}/thunar-settings.desktop{,.hide}
sudo mv ${applications_dir}/xfce4-about.desktop{,.hide}

# https://wiki.archlinux.org/title/Xdg-utils
# https://bbs.archlinux.org/viewtopic.php?id=157033
# xdg-mime query default inode/directory
xdg-mime default thunar.desktop inode/directory 2> /dev/null # for thunar
```

Remove operation

```bash
sudo pacman -R --noconfirm thunar-archive-plugin xarchiver
sudo pacman -R --noconfirm tumbler libgsf libopenraw poppler-glib
# sudo pacman -R --noconfirm ffmpegthumbnailer
sudo pacman -R --noconfirm thunar exo libxfce4ui


applications_dir=${applications_dir:-'/usr/share/applications'}
sudo find "${applications_dir}" -type f -name 'exo-*.desktop*' -exec rm -rf {} \;
sudo rm -f ${applications_dir}/thunar-settings.desktop*
sudo rm -f ${applications_dir}/xfce4-about.desktop*
```


### PCManFM

[PCManFM](https://wiki.archlinux.org/title/PCManFM) is a free file manager application and the standard file manager of [LXDE](https://wiki.archlinux.org/title/LXDE).

#### Packages

By default is `pcmanfm` package, `pcmanfm-gtk3` for the GTK3 version.

```bash
sudo pacman -S pcmanfm-gtk3
# Essential dependencies: libfm,libfm-extra,libfm-gtk3,lxmenu-data,menu-cache
# Total (5+1) packages with 6.42 MiB installed size

# libfm has config file ~/.config/libfm/libfm.conf

Optional dependencies for pcmanfm-gtk3
    gvfs: for trash support, mounting with udisks and remote filesystems
    xarchiver: archive management
```

#### Install

Total install **13** packages with 24.45 MiB installed size.

```bash
sudo pacman -S --noconfirm pcmanfm-gtk3 \
                           xarchiver
```

Remove operation

```bash
sudo pacman -R --noconfirm xarchiver
sudo pacman -R --noconfirm pcmanfm-gtk3
```


### Nautilus  

[Nautilus](https://wiki.archlinux.org/title/GNOME/Files) is the default file manager for [GNOME](https://wiki.gnome.org/).

#### Packages

```bash
sudo pacman -S nautilus
```

It needs to install **56** packages with 19.39 MiB download size and 116.90 MiB installed size.

So it isn't a good choice.


### Dolphin

[Dolphin](https://wiki.archlinux.org/title/Dolphin) is the default file manager of [KDE](https://wiki.archlinux.org/title/KDE).


#### Packages

```bash
sudo pacman -S dolphin
```

It needs to install **133** packages with 68.66 MiB download size and 460.88 MiB installed size.

So it isn't a good choice.


## Change Log

* Nov 23, 2020 Mon 12:28 ET
  * first draft


<!-- End -->