# Graphical user interface

This note is based official document [General recommendations][general_recommendations]. It is an annotated index of popular articles and important information for improving and adding functionalities to the installed Arch system.

Official page [Graphical user interface](https://wiki.archlinux.org/title/General_recommendations#Graphical_user_interface)


## TOC

1. [Display server](#display-server)  
1.1 [Xorg](#xorg)  
1.2 [Wayland](#wayland)  
2. [Display drivers](#display-drivers)  
2.1 [Intel Graphics](#intel-graphics)  
2.1.1 [Vulkan support](#vulkan-support)  
2.1.2 [Module-based options](#module-based-options)  
2.2 [NVIDIA Optimus](#nvidia-optimus)  
2.2.1 [Bumblebee](#bumblebee)  
2.2.2 [Nvidia-xrun](#nvidia-xrun)  
3. [Desktop environment](#desktop-environment)  
3.1 [GNOME](#gnome)  
3.2 [KDE Plasma](#kde-plasma)  
4. [Window managers](#window-managers)  
4.1 [i3](#i3)  
4.1.1 [Installation](#installation)  
4.1.2 [Configuration](#configuration)  
5. [Display manager](#display-manager)  
6. [User directories](#user-directories)  
7. [Change Log](#change-log)  


## Display server

There are two types of display server: [Xorg][xorg] and [Wayland][wayland]. XWayland provides a compatibility layer to seamlessly run legacy X11 applications in Wayland.

### Xorg

>[Xorg][xorg] (commonly referred as simply X) is the most popular display server among Linux users. Its ubiquity has led to making it an ever-present requisite for GUI applications, resulting in massive adoption from most distributions.

Xorg installation

```bash
pacman -S --noconfirm xorg-server

# package group
pacman -S --noconfirm xorg-apps xorg
```

Identify graphic card

```bash
# lspci | grep -e VGA -e 3D
00:02.0 VGA compatible controller: Intel Corporation UHD Graphics 620 (rev 07)
01:00.0 3D controller: NVIDIA Corporation GP108M [GeForce MX150] (rev ff)
```

Xorg searches for installed drivers automatically:

* If it cannot find the specific driver installed for the hardware, it first searches for fbdev ([xf86-video-fbdev](https://www.archlinux.org/packages/?name=xf86-video-fbdev)).
* If that is not found, it searches for *vesa* ([xf86-video-vesa](https://www.archlinux.org/packages/?name=xf86-video-vesa)), the generic driver, which handles a large number of chipsets but does not include any 2D or 3D acceleration.
* If *vesa* is not found, Xorg will fall back to [kernel mode setting](https://wiki.archlinux.org/title/Kernel_mode_setting), which includes GLAMOR acceleration.


### Wayland

>[Wayland][wayland] is a protocol for a compositing window manager to talk to its clients, as well as a library implementing the protocol. It is supported on some desktop environments like *GNOME* and *KDE*. There is also a compositor reference implementation called Weston. XWayland provides a compatibility layer to seamlessly run legacy X11 applications in Wayland.

Most Wayland compositors only work on systems using [Kernel mode setting](https://wiki.archlinux.org/title/Kernel_mode_setting). Wayland by itself does not provide a graphical environment; for this you also need a compositor such as [#Weston](https://wiki.archlinux.org/title/Wayland#Weston) or [Sway](https://wiki.archlinux.org/title/Sway), or a desktop environment that includes a compositor like GNOME or KDE.

For Wayland:

* If you use [GNOME](https://wiki.archlinux.org/title/GNOME), Wayland is the default display.
* If you use [KDE Plasma](https://wiki.archlinux.org/title/KDE), to enable support for Wayland in Plasma, also install the `plasma-wayland-session` package. (see [KDE#Plasma](https://wiki.archlinux.org/title/KDE#Plasma))


## Display drivers

The default *vesa* display driver will work with most video cards, but performance can be significantly improved and additional features harnessed by installing the appropriate driver for [AMD](https://wiki.archlinux.org/title/Xorg#AMD), [Intel](https://wiki.archlinux.org/title/Intel), or [NVIDIA](https://wiki.archlinux.org/title/NVIDIA) products.

```bash
# lspci -k | grep -A 2 -E "(VGA|3D)"

00:02.0 VGA compatible controller: Intel Corporation UHD Graphics 620 (rev 07)
	Subsystem: Huawei Technologies Co., Ltd. UHD Graphics 620
	Kernel driver in use: i915
--
01:00.0 3D controller: NVIDIA Corporation GP108M [GeForce MX150] (rev a1)
	Subsystem: Huawei Technologies Co., Ltd. GP108M [GeForce MX150]
	Kernel driver in use: nouveau
```

### Intel Graphics

[Intel graphics](https://wiki.archlinux.org/title/Intel_graphics) says:

```bash
# mesa provides the DRI driver for 3D acceleration
pacman -S --noconfirm mesa
# lib32-mesa package is from the multilib repository
pacman -S --noconfirm lib32-mesa

# for the DDX driver (which provides 2D acceleration in Xorg), often not recommend, see https://wiki.archlinux.org/title/Intel_graphics#Installation
# pacman -S --noconfirm xf86-video-intel
```

> For Vulkan support (Ivy Bridge and newer), install the vulkan-intel package.


To check if the Intel card is used:

```bash
# from https://wiki.archlinux.org/title/PRIME#Installation
glxinfo | grep "OpenGL renderer"

# OpenGL renderer string: Mesa Intel(R) UHD Graphics 620 (KBL GT2)
```

#### Vulkan support

[Vulkan](https://wiki.archlinux.org/title/Vulkan) is a low-overhead, cross-platform 3D graphics and compute API.

[MBXP][mbxp] use *Intel Corporation UHD Graphics 620*, I find list in [Vulkan Hardware Database](http://vulkan.gpuinfo.org):

* [Intel(R) UHD Graphics 620](http://vulkan.gpuinfo.org/listreports.php?devicename=Intel%28R%29%20UHD%20Graphics%20620)
* [Intel(R) UHD Graphics 620 (Kabylake GT2)](http://vulkan.gpuinfo.org/listreports.php?devicename=Intel%28R%29%20UHD%20Graphics%20620%20%28Kabylake%20GT2%29)

>For Vulkan support (Ivy Bridge and newer), install the `vulkan-intel` package.
>
>To run a Vulkan application, you will need to install the `vulkan-icd-loader` package (and `lib32-vulkan-icd-loader` if you also want to run 32-bit applications), as well as the Vulkan drivers for your graphics card(s): Intel: `vulkan-intel`, NVIDIA: `nvidia`


```bash
pacman -S --noconfirm vulkan-intel vulkan-icd-loader

# support 32-bit applications
pacman -S --noconfirm lib32-vulkan-icd-loader

# verification
ls /usr/share/vulkan/icd.d/
```

**Attention**: [Bumblebee][bumblebee] doesn't support Vulkan. ([link](https://github.com/Bumblebee-Project/Bumblebee/issues/769#issuecomment-218016574))

#### Module-based options

Processor [Intel(R) Core(TM) i7-8550U](https://ark.intel.com/content/www/us/en/ark/products/122589/intel-core-i7-8550u-processor-8m-cache-up-to-4-00-ghz.html "Intel") belongs to 8th generation [Kaby Lake](https://en.wikipedia.org/wiki/Kaby_Lake#List_of_8th_generation_Kaby_Lake_R_processors "WikiPedia") R processors. Its GPU is *Intel Corporation UHD Graphics 620* which use kernel drive [i915](https://www.kernel.org/doc/html/latest/gpu/i915.html "Kernel").

There is a GitHub [gist](https://gist.github.com/Brainiarc7/aa43570f512906e882ad6cdd835efe57) shows how to tune Intel-based Skylake, Kabylake and beyond Integrated Graphics Core for performance and reliability through GuC and HuC firmware usage on Linux.


```bash
# /usr/src/linux/drivers/gpu/drm/i915/Kconfig.debug
lsmod

# https://wiki.archlinux.org/title/Intel_graphics#Module-based_options
# list all options along with short descriptions and default values
modinfo -p i915

# check enabled options
# sudo systool -m i915 -av
sudo systool -m i915 -av | sed -r -n '/parameters:/I,/^$/{p}'

# Available parameter and value see command
modinfo i915
```

```bash
# - Enable GuC / HuC firmware loading
# https://wiki.archlinux.org/title/Intel_graphics#Enable_GuC_/_HuC_firmware_loading
# Enabling GuC/HuC firmware loading can cause issues on some systems; disable it if you experience freezing (for example, after resuming from hibernation).
# It is possible to enable both GuC/HuC firmware loading and GuC submission by using the enable_guc=3 module parameter, although this is generally discouraged and may even negatively affect your system stability.
# enable_guc:Enable GuC load for GuC submission and/or HuC load. Required functionality can be selected using bitmask values. (-1=auto, 0=disable [default], 1=GuC submission, 2=HuC load) (int)
enable_guc=0

# - Framebuffer compression (enable_fbc)
# https://wiki.archlinux.org/title/Intel_graphics#Framebuffer_compression_(enable_fbc)
# Making use of Framebuffer compression (FBC) can reduce power consumption while reducing memory bandwidth needed for screen refreshes.
# enable_fbc:Enable frame buffer compression for power savings (default: -1 (use per-chip default)) (int)
enable_fbc=1

# - Fastboot
# https://wiki.archlinux.org/title/Intel_graphics#Fastboot
# The goal of Intel Fastboot is to preserve the frame-buffer as setup by the BIOS or bootloader to avoid any flickering until Xorg has started.
# fastboot:Try to skip unnecessary mode sets at boot time (0=disabled, 1=enabled) Default: -1 (use per-chip default) (int)
fastboot=1
```

```bash
tee /etc/modprobe.d/i915.conf 1>/dev/null <<EOF
options i915 enable_guc=0 enable_fbc=1 fastboot=1
EOF
```

#### Disable Discrete GPU

If you want to turn off the high-performance graphics processor (discrete NVIDIA GPU) to save battery power, please follow wiki [Fully Power Down Discrete GPU](https://wiki.archlinux.org/title/Hybrid_graphics#Fully_Power_Down_Discrete_GPU).

>With a NVidia GPU, this can be more safely done using [bbswitch](https://wiki.archlinux.org/title/Bbswitch), which consists of a kernel package that automatically issues the correct ACPI calls to disable the discrete GPU when not needed, or automatically at boot.

```bash
pacman -S --noconfirm bbswitch

# https://wiki.archlinux.org/title/Nvidia-xrun#Use_bbswitch_to_manage_nvidia
# https://wiki.archlinux.org/title/Bumblebee#Power_management

# Load bbswitch module on boot
[[ -d /etc/modules-load.d/ ]] && echo 'bbswitch ' > /etc/modules-load.d/bbswitch.conf
# Disable the nvidia module on boot
echo 'options bbswitch load_state=0 unload_state=1' > /etc/modprobe.d/bbswitch.conf

```


### NVIDIA Optimus

Wiki [NVIDIA](https://wiki.archlinux.org/title/NVIDIA) says:

>If you have a laptop with hybrid Intel/NVIDIA graphics, see [NVIDIA Optimus][nvidia_optimus] instead.

[MBXP][mbxp] use both *Intel Corporation UHD Graphics 620* and *NVIDIA Corporation GP108M [GeForce MX150]*. So we need to follow documentation [NVIDIA Optimus][nvidia_optimus].

[NVIDIA Optimus][nvidia_optimus] is a technology that allows an Intel integrated GPU and discrete NVIDIA GPU to be built into and accessed by a laptop.

There are several methods available:

* disabling one of the devices in BIOS
  * pros: improved battery life if the NVIDIA device is disabled
  * cons: not be available with all BIOSes and does not allow GPU switching
* using the official Optimus support included with the proprietary NVIDIA driver
  * pros: offer the best NVIDIA performance
  * cons: not allow GPU switching and can be more buggy than the open-source driver
* using the [PRIME](https://wiki.archlinux.org/title/PRIME) functionality of the open-source nouveau driver
  * pros: allow GPU switching and powersaving
  * cons: offer poor performance compared to the proprietary NVIDIA driver and may cause issues with sleep and hibernate
* using the third-party [Bumblebee][bumblebee] program to implement Optimus-like functionality
  * pros: offer GPU switching and powersaving
  * cons: requires extra configuration
* using the nvidia-xrun utility to run separate X sessions with discrete nvidia graphics with full performance

Also see [optimus-manager wiki](https://github.com/Askannz/optimus-manager/wiki#the-problem).

#### Bumblebee

>Bumblebee is an effort to make NVIDIA Optimus enabled laptops work in GNU/Linux systems. Such feature involves two graphics cards with two different power consumption profiles plugged in a layered way sharing a single framebuffer.

[Bumblebee#Installation](https://wiki.archlinux.org/title/Bumblebee#Installation) lists packages needed.

[NVIDIA#Installation](https://wiki.archlinux.org/title/NVIDIA#Installation) says:

>Avoid installing the NVIDIA driver through the package provided from the NVIDIA website. Installation through pacman allows upgrading the driver together with the rest of the system.
>
>For GeForce 600-900 and Quadro/Tesla/Tegra K-series cards and newer [NVE0, NV110 and NV130 family cards from around 2010-2019], install the `nvidia` or `nvidia-lts` package.
>
>For 32-bit application support, also install the corresponding lib32 nvidia package from the multilib repository (e.g. `lib32-nvidia-utils` or `lib32-nvidia-390xx-utils`).

Configuration file path `/etc/bumblebee/bumblebee.conf`.

```bash
pacman -S --noconfirm bumblebee mesa nvidia

# [ERROR]Module bbswitch could not be loaded (timeout?)
pacman -S --noconfirm bbswitch

# Optional dependencies for bumblebee
#     bbswitch: switch on/off discrete card [installed]
#     nvidia: NVIDIA kernel driver [installed]
#     nvidia-390xx: NVIDIA kernel driver for old devices
#     nvidia-340xx: NVIDIA kernel driver for even older devices
#     primus: faster back-end for optirun
#     lib32-virtualgl: run 32bit applications with optirun
#     lib32-primus: faster back-end for optirun

# from multilib repository
pacman -S --noconfirm lib32-nvidia-utils lib32-virtualgl
nvidia-xrun
# If you run into trouble with CUDA not being available, run nvidia-modprobe first.
# nvidia-modprobe

# Don't forget to add yourself to the 'bumblebee' group to use Bumblebee
gpasswd -a $USER bumblebee
# usermod -a -G bumblebee $USER

# enable service
systemctl --now enable bumblebeed.service
```

Testing, if the window with animation shows up, Optimus with Bumblebee is working.

```bash
pacman -S --noconfirm mesa-demos

optirun glxgears -info
# if fails, try
optirun glxspheres64
```

Show system journal log about *bumblebee*.

```bash
journalctl -b -g "bbswitch|bumblebee"
```

output

```txt
May 01 09:12:06 MBXP kernel: bbswitch: loading out-of-tree module taints kernel.
May 01 09:12:06 MBXP kernel: bbswitch: module verification failed: signature and/or required key missing - tainting kernel
May 01 09:12:06 MBXP kernel: bbswitch: version 0.8
```

General useage sees wiki [Bumblebee#General_usage](https://wiki.archlinux.org/title/Bumblebee#General_usage).

**Attention**: [Bumblebee][bumblebee] doesn't support Vulkan (Intel Graphics).


#### Nvidia-xrun

>[Nvidia-xrun][nvidia-xrun] is a utility to allow Nvidia optimus enabled laptops run X server with discrete nvidia graphics on demand. This solution offers full GPU utilization, compatibility and better performance than [Bumblebee][bumblebee].

```bash
pacman -S --noconfirm nvidia bbswitch
# /etc/X11/nvidia-xorg.conf
# If run yay as root/sudo, prompt error info 'Please avoid running yay as root/sudo.' and 'Refusing to install AUR Packages as root, Abourting.'.
# nvidia-xrun version is too old  Last Updated: 2016-10-20 21:48 https://aur.archlinux.org/packages/nvidia-xrun/
yay -S --noconfirm nvidia-xrun-git

# Use bbswitch to manage nvidia https://wiki.archlinux.org/title/Nvidia-xrun#Use_bbswitch_to_manage_nvidia
# Load bbswitch module on boot
echo 'bbswitch ' > /etc/modules-load.d/bbswitch.conf

# Disable the nvidia module on boot
echo 'options bbswitch load_state=0 unload_state=1' > /etc/modprobe.d/bbswitch.conf

tee /usr/lib/modprobe.d/nvidia-xrun.conf 1>/dev/null <<EOF
blacklist nvidia
blacklist nvidia-drm
blacklist nvidia-modeset
blacklist nvidia-uvm
blacklist nouveau
EOF
# echo -e "blacklist nvidia\nblacklist nvidia-drm\nblacklist nvidia-modeset\nblacklist nvidia-uvm\nblacklist nouveau" >  /usr/lib/modprobe.d/nvidia-xrun.conf

tee ~/.nvidia-xinitrc 1>/dev/null <<EOF
if [ $# -gt 0 ]; then
    $*
else
    # https://wiki.archlinux.org/title/GNOME#Manually
    export GDK_BACKEND=x11
    exec gnome-session
fi
EOF
# echo -e "if [ \$# -gt 0 ]; then\n    \$*\nelse\n    # https://wiki.archlinux.org/title/GNOME#Manually\n    export GDK_BACKEND=x11\n    exec gnome-session\nfi" > ~/.nvidia-xinitrc

# check bbswitch's status
cat /proc/acpi/bbswitch

# To force the card to turn on/off respectively run:
# tee /proc/acpi/bbswitch <<<OFF
# tee /proc/acpi/bbswitch <<<ON
```

Usage: once the system boots, from the virtual console (<kbd>Crtl</kbd>+<kbd>Alt</kbd>+<kbd>F{1,6}</kbd>), login to your user, and run `nvidia-xrun <application>`.But desktop environments are prone to crashing when switching between virtual terminals while the nvidia driver is running.


#### Optimus Manager

[Optimus manager][optimus-manager] is a Linux program to handle GPU switching on Optimus laptops.

But it only sopports Xorg sessions (no [Wayland][wayland]). Supported display managers are : SDDM, LightDM, GDM.

**Attention**:

* [Optimus manager][optimus-manager] is incompatible with [Bumblebee][bumblebee] since both tools would be trying to control GPU power switching at the same time.
* If you have previously installed [nvidia-xrun][nvidia-xrun] by following [instructions](https://wiki.archlinux.org/title/Nvidia-xrun#Configuration) on the Arch Wiki, make sure all its configuration files are removed. See [issue](https://github.com/Askannz/optimus-manager/issues/135).


```bash
# https://github.com/Askannz/optimus-manager/wiki

pacman -S --noconfirm nvidia

# https://aur.archlinux.org/packages/optimus-manager/
yay -S --noconfirm optimus-manager

# - For Gnome and GDM users
# https://github.com/Askannz/optimus-manager#important--gnome-and-gdm-users
# Replace default gdm package. The patch was written by Canonical for Ubuntu and simply adds two script entry points specifically for Prime switching. The package is otherwise identical to the official one.

sudo systemctl stop gdm.service
sudo pacman -R --noconfirm gdm
yay -S --noconfirm gdm-prime
[[ -f /etc/gdm/custom.conf ]] && sudo sed -r -i '/WaylandEnable=/{s@.*@WaylandEnable=false@g}' /etc/gdm/custom.conf

# - For Manjaro user
# https://github.com/Askannz/optimus-manager#important--manjaro-kde-users
[[ -f /etc/sddm.conf ]] && sudo sed -r -i '/^#*(DisplayCommand|DisplayStopCommand)/{s@^#*@@g;p}' /etc/sddm.conf

# - Configuration
# https://github.com/Askannz/optimus-manager/wiki/A-guide--to-power-management-options
# https://github.com/Askannz/optimus-manager/blob/master/optimus-manager.conf
[[ -f /usr/share/optimus-manager.conf && ! -f /etc/optimus-manager/optimus-manager.conf ]] && sudo cp /usr/share/optimus-manager.conf /etc/optimus-manager/optimus-manager.conf

# For previously installed nvidia-xrun
sudo rm -f /etc/modules-load.d/bbswitch.conf /etc/modprobe.d/bbswitch.conf
rm -f ~/.nvidia-xinitrc

# power switch by bbswitch https://github.com/Askannz/optimus-manager/wiki/A-guide--to-power-management-options#configuration-3--bbswitch
sudo pacman -S --noconfirm bbswitch

# [optimus]
# switching=bbswitch
# pci_power_control=no
# pci_remove=no
# pci_reset=no
sudo sed -r -i '/switching=/{s@([^=]*=).*@\1bbswitch@g}' /etc/optimus-manager/optimus-manager.conf
sudo sed -r -i '/(pci_power_control|pci_remove|pci_reset)=/{s@([^=]*=).*@\1yes@g}' /etc/optimus-manager/optimus-manager.conf
```

In Gnome, after I run `optimus-manager --switch nvidia`, system auto log out then into black screen with a under score on the left top screen. Following *Another quirk of GDM is that the X server may not automatically restart after a GPU switch. If you see an empty black screen or a black screen with a blinking cursor, try switching back to an empty TTY (with Ctrl+Alt+F5 for instance), then back to TTY1 with Ctrl+Alt+F1. See this [FAQ question](https://github.com/Askannz/optimus-manager/wiki/FAQ,-common-issues,-troubleshooting#after-trying-to-switch-gpus-i-am-stuck-with-a-black-screen-or-a-black-screen-with-a-blinking-cursor-or-a-tty-login-screen).* The whole screen becomes black without anything. I have to reboot to recovery my system.

Uninstall

```bash
sudo optimus-manager --cleanup
sudo systemctl stop optimus-manager.service
yay -R --noconfirm optimus-manager

# if install gdm-prime
yay -R --noconfirm gdm-prime
sudo rm -f /etc/gdm/custom.conf.pacsave
sudo pacman -S --noconfirm gdm
# may relogin
sudo systemctl restart gdm.service
```


## Desktop environment

[Desktop environments](https://wiki.archlinux.org/title/Desktop_environment) such as GNOME, KDE, LXDE, and Xfce bundle together a wide range of X clients, such as a window manager, panel, file manager, terminal emulator, text editor, icons, and other utilities.

Login screen sessions saved in directory `/usr/share/xsessions/`.

### GNOME

[GNOME](https://wiki.archlinux.org/title/GNOME) is a desktop environment that aims to be simple and easy to use.

GNOME has three available sessions, all using GNOME Shell. GNOME is the default which uses *Wayland*. Traditional X applications are run through Xwayland. When the GNOME session is used, GNOME applications will be run using Wayland.

The base desktop consists of GNOME Shell, a plugin for the Mutter window manager. It can be installed separately with gnome-shell.

```bash
pacman -Sg gnome | cut -d' ' -f2 | sed ':a;N;$!ba;s@\n@ @g'
# pack num 67
```

>baobab cheese eog epiphany evince file-roller gdm gedit gnome-backgrounds gnome-books gnome-calculator gnome-calendar gnome-characters gnome-clocks gnome-color-manager gnome-contacts gnome-control-center gnome-dictionary gnome-disk-utility gnome-documents gnome-font-viewer gnome-getting-started-docs gnome-keyring gnome-logs gnome-maps gnome-menus gnome-music gnome-photos gnome-remote-desktop gnome-screenshot gnome-session gnome-settings-daemon gnome-shell gnome-shell-extensions gnome-system-monitor gnome-terminal gnome-themes-extra gnome-todo gnome-user-docs gnome-user-share gnome-video-effects gnome-weather grilo-plugins gvfs gvfs-afc gvfs-goa gvfs-google gvfs-gphoto2 gvfs-mtp gvfs-nfs gvfs-smb mousetweaks mutter nautilus networkmanager orca rygel sushi totem tracker tracker-miners vino xdg-user-dirs-gtk yelp gnome-boxes gnome-software simple-scan


```bash
pacman -Sg gnome-extra | cut -d' ' -f2 | sed ':a;N;$!ba;s@\n@ @g'
# pack num 35
```

>accerciser dconf-editor devhelp evolution five-or-more four-in-a-row ghex glade gnome-builder gnome-chess gnome-devel-docs gnome-klotski gnome-mahjongg gnome-mines gnome-nettool gnome-nibbles gnome-robots gnome-sound-recorder gnome-sudoku gnome-taquin gnome-tetravex gnome-tweaks hitori iagno lightsoff nautilus-sendto polari quadrapassel swell-foop sysprof tali gnome-code-assistance gnome-multi-writer gnome-recipes gnome-usage


```bash
pacman -S --noconfirm gnome
# without prompt
# pacman -Sg gnome | cut -d' ' -f2 | xargs pacman -S --noconfirm

# remove unneed packages from 'gnome'
pacman -R --noconfirm gnome-books gnome-boxes gnome-weather gnome-todo gnome-contacts gnome-maps gnome-music gnome-photos epiphany
# https://wiki.archlinux.org/title/GNOME#Extensions
pacman -S --noconfirm gnome-tweaks dconf-editor gnome-nettool gnome-usage

# pacman -S --noconfirm gnome-extra
# gnome-extra remove game:   accerciser dconf-editor devhelp ghex gnome-builder gnome-devel-docs gnome-nettool gnome-sound-recorder gnome-tweaks sysprof tali gnome-code-assistance gnome-multi-writer gnome-usage

#  display manager
pacman -S --noconfirm gdm
systemctl enable gdm.service

# group gnome includes package networkmanager
pacman -S --noconfirm network-manager-applet
systemctl disable netctl
systemctl enable NetworkManager
```

### KDE Plasma

[KDE](https://wiki.archlinux.org/title/KDE#Plasma) is a software project currently comprising a desktop environment known as Plasma, a collection of libraries and frameworks (KDE Frameworks) and several applications (KDE Applications) as well.

List packages included in group package

```bash
pacman -Sg plasma | cut -d' ' -f2 | sed ':a;N;$!ba;s@\n@ @g'
# pack num 43

# pacman -Sg kde-applications | cut -d' ' -f2 | sed ':a;N;$!ba;s@\n@ @g'
# pack num 165
```

```bash
# - KDE Plasma
# Before installing Plasma, make sure you have a working Xorg installation on your system.
# plasma / plasma-meta / plasma-desktop
pacman -S --noconfirm plasma-desktop plasma-wayland-session powerdevil

# kde-applications / kde-applications-meta
pacman -S --noconfirm kde-applications

# an integrated Plasma power managing service
pacman -S --noconfirm powerdevil

# https://wiki.archlinux.org/title/NetworkManager#KDE_Plasma
# add it to the KDE taskbar via the Panel options > Add widgets > Networks menu
pacman -S --noconfirm plasma-nm

#  display manager
pacman -S --noconfirm sddm
systemctl enable sddm.service
```


## Window managers

>A full-fledged desktop environment provides a complete and consistent graphical user interface, but tends to consume a considerable amount of system resources. Users seeking to maximize performance or otherwise simplify their environment may opt to install a [window manager](https://wiki.archlinux.org/title/Window_manager) alone and hand-pick desired extras. Most desktop environments allow use of an alternative window manager as well. [Dynamic](https://wiki.archlinux.org/title/Category:Dynamic_WMs), [stacking](https://wiki.archlinux.org/title/Category:Stacking_WMs), and [tiling](https://wiki.archlinux.org/title/Category:Tiling_WMs) window managers differ in their handling of window placement.

* `Stacking` (aka floating) window managers provide the traditional desktop metaphor used in commercial operating systems like Windows and OS X. Windows act like pieces of paper on a desk, and can be stacked on top of each other.
* `Tiling` window managers "tile" the windows so that none are overlapping. They usually make very extensive use of key-bindings and have less (or no) reliance on the mouse. Tiling window managers may be manual, offer predefined layouts, or both.
* `Dynamic` window managers can dynamically switch between tiling or floating window layout.

Here I choose tiling window manager [i3][i3] as my second window manager.

### i3

>[i3][i3] is a dynamic tiling window manager inspired by wmii that is primarily targeted at developers and advanced users.

#### Installation

>`i3-wm` includes *i3.desktop* as [Xsession](https://wiki.archlinux.org/title/Xsession) which starts the window manager. `i3-with-shmlog.desktop` enables logs (useful for debugging).

```bash
pacman -S --noconfirm i3-wm
pacman -S --noconfirm i3status    # Error: status_command not found (exit 127)
# https://i3wm.org/i3status/manpage.html
# https://wiki.archlinux.org/title/I3#Shutdown,_reboot,_lock_screen
pacman -S --noconfirm i3lock polkit
# application launcher
pacman -S --noconfirm dmenu

# application light
# light change file /sys/class/backlight/intel_backlight/brightness which file group is video, login user need to add into group 'video' to make command light effect.
pacman -S --noconfirm light
sudo usermod -a -G video "$USER" 2> /dev/null

# https://wiki.archlinux.org/title/I3#Iconic_fonts_in_the_status_bar
# https://fontawesome.com/
# https://ionicons.com/
# pacman -S --noconfirm ttf-font-awesome ttf-ionicons

# http://kumarcode.com/Adding-your-custom-font-to-i3status/
# ctrl + v + u + unicodesequence
```

#### Configuration

When i3 is first started, it offers to run the configuration wizard *i3-config-wizard*. This tool creates `~/.config/i3/config` by rewriting a template configuration file in `/etc/i3/config.keycodes`.

```bash
# generate config file ~/.config/i3/config
i3-config-wizard
# /etc/i3/config, ~/.config/i3/config, ~/.i3/config
```

[i3](https://i3wm.org/) provides official document [i3 User’s Guide](https://i3wm.org/docs/userguide.html).

Change settings in config file `~/.config/i3/config`.

i3status configuration

```bash
# i3 status
# https://wiki.archlinux.org/title/I3#i3status
# https://i3wm.org/i3status/manpage.html
cp /etc/i3status.conf ~/.config/i3/
```

File content `~/.config/i3/i3status.conf`

```bash
# i3status configuration file.
# see "man i3status" for documentation.

# It is important that this file is edited as UTF-8.
# The following line should contain a sharp s:
# ß
# If the above line is not correctly displayed, fix your editor first!

general {
	output_format="i3bar"
    colors = true
    interval = 5
}

order += "volume master"
#order += "ipv6"
#order += "disk /"
order += "load"
order += "wireless _first_"
#order += "ethernet _first_"
order += "battery all"
order += "tztime local"

wireless _first_ {
	format_up = "W: %essid%quality"
	# format_up = "W: (%quality at %essid, %bitrate / %frequency) %ip"
	format_down = "W: down"
}

ethernet _first_ {
    # if you use %speed, i3status requires root privileges
    format_up = "E: %ip (%speed)"
    format_down = "E: down"
}

battery all {
	format = "%status %percentage %remaining"
	# format = "%status %percentage %remaining %emptytime"
	hide_seconds= true
	integer_battery_capacity= true
	format_down = "No battery"
	status_chr = "CHR"
	status_bat = "BAT"
	status_unk = "UNK"
	status_full = "FULL"
	path = "/sys/class/power_supply/BAT%d/uevent"
	low_threshold = 10
}


tztime local {
    format = "%a %b %d, %Y %H:%M"
}

load {
    format = "Load: [%5min %15min]"
}

disk "/" {
    format = "%avail"
}

volume master {
	format = "♪: %volume"
	format_muted = "♪: Muted"
	# format_muted = "♪: Muted (%volume)"
	device = "default"
	mixer = "Master"
	mixer_idx = 0
}
```

autostart

```bash
# add into ~/.config/i3/config

# autostart
exec terminator
```

i3bar

```bash
# add into ~/.config/i3/config
bar {
    # output LVDS1
    status_command i3status --config ~/.config/i3/i3status.conf
    position top # top/bottom
    mode dock #  dock/hide/invisible
    workspace_buttons yes # yes/no
    tray_output no # none/primary/<output>
    # separator_symbol ":|:"
    strip_workspace_numbers yes
    strip_workspace_name yes
    binding_mode_indicator yes

    colors {
        background #000000
        statusline #ffffff
        separator #666666

        focused_workspace  #4c7899 #285577 #ffffff
        active_workspace   #333333 #5f676a #ffffff
        inactive_workspace #333333 #222222 #888888
        urgent_workspace   #2f343a #900000 #ffffff
        binding_mode       #2f343a #900000 #ffffff
    }
}
```

Workspace variables (optional)

```bash
# Define names for default workspaces for which we configure key bindings later on.
# We use variables to avoid repeating the names in multiple places.
set $ws1 "1:Term"
set $ws2 "2:Web"
set $ws3 "3:Code"
set $ws4 "4:Media"
set $ws5 "5"

# switch to workspace
bindsym $mod+1 workspace $ws1

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace $ws1
```

[Screensaver and power management](https://wiki.archlinux.org/title/I3#Screensaver_and_power_management)

```bash

# Shutdown, reboot, lock screen
# https://wiki.archlinux.org/title/I3#Shutdown,_reboot,_lock_screen
# set $Locker i3lock && sleep 1
set $Locker i3lock
set $mod_w Mod4  # Windows key

# color
# Black 	#000000 	(0,0,0)
# violet 	#EE82EE 	(238,130,238)

# Lock screen
bindsym $mod_w+l exec --no-startup-id $Locker -c 000000, mode "default"

#set $mode_system System (l) lock, (e) logout, (s) suspend, (h) hibernate, (r) reboot, (Shift+s) shutdown
set $mode_system System (l) logout, (s) suspend, (h) hibernate, (r) reboot, (Shift+s) shutdown
mode "$mode_system" {
    #bindsym l exec --no-startup-id $Locker, mode "default"
    #bindsym l exec --no-startup-id $Locker -c 000000, mode "default"
    bindsym l exec --no-startup-id i3-msg exit, mode "default"
    bindsym s exec --no-startup-id $Locker -c EE82EE && sleep 1 && systemctl suspend, mode "default"
    bindsym h exec --no-startup-id $Locker -c EE82EE && sleep 1 && systemctl hibernate, mode "default"
    bindsym r exec --no-startup-id systemctl reboot, mode "default"
    bindsym Shift+s exec --no-startup-id systemctl poweroff -i, mode "default"
    # back to normal: Enter or Escape
    bindsym Return mode "default"
    bindsym Escape mode "default"
}

# bindsym $mod+Pause mode "$mode_system"
bindsym Control+Shift+Delete mode "$mode_system"
```

Screen brightness and Pulse Audio controls

```bash
# https://wiki.archlinux.org/title/Backlight
# https://askubuntu.com/questions/823669/volume-sound-and-screen-brightness-controls-not-working
# https://www.reddit.com/r/archlinux/comments/9zo44d/i3_config_xbacklight_increasedecrease_with_fnf/
pacman -S --noconfirm light

# Screen brightness controls
# increase screen brightness
bindsym XF86MonBrightnessUp exec --no-startup-id  "light -A 5"
# decrease screen brightness
bindsym XF86MonBrightnessDown exec --no-startup-id "light -U 10"

# Pulse Audio controls
# https://wiki.archlinux.org/title/PulseAudio#Keyboard_volume_control
bindsym XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume 0 +2% #increase sound volume
bindsym XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume 0 -5% #decrease sound volume
bindsym XF86AudioMute exec --no-startup-id pactl set-sink-mute 0 toggle # mute sound
```

Touchpad enable tap to click

```bash
# add into ~/.config/i3/config

# https://wiki.archlinux.org/title/Libinput
# https://cravencode.com/post/essentials/enable-tap-to-click-in-i3wm/
# https://www.reddit.com/r/i3wm/comments/9y0e9g/how_can_i_enable_vertical_scrolling_and/

# method 1
# echo -e "Section \"InputClass\"\n        Identifier \"touchpad\"\n        MatchIsTouchpad \"on\"\n        Driver \"libinput\"\n        Option \"Tapping\" \"on\"\nEndSection" > /etc/X11/xorg.conf.d/30-touchpad.conf

# method 2
touchpad_device=$(xinput 2> /dev/null | sed -r -n '/touchpad/I{s@^[^[:upper:]]*(.*?)id=.*$@\1@g;s@[[:space:]]+$@@g;p}')
[[ -n "${touchpad_device}" ]] && echo -e "#touchpad enable tap to click\nexec --no-startup-id xinput set-prop \"${touchpad_device}\" \"libinput Tapping Enabled\" 1" >> "${i3_config_path}"
```

ScreenShot

```bash
# https://help.gnome.org/users/gnome-help/stable/screen-shot-record.html.en
# fullscreen screenshoot (PrnScr)
# window screenshoot(Shift + PrnScr)
# select windows screenshoot(Alt + Shift + PrnScr)

set $ScreenShot gnome-screenshot
bindsym Print exec --no-startup-id $ScreenShot # Grab the entire screen
bindsym Shift+Print exec --no-startup-id $ScreenShot -w # Grab a window instead of the entire screen
bindsym Control+Shift+Print exec --no-startup-id $ScreenShot -a # Grab an area of the screen instead of the entire screen
```

HiDPI scaling in i3

```bash
# font is too tiny to see by default
# xdpyinfo utility from the xorg-xdpyinfo package
pacman -S xorg-xdpyinfoindex
# detect physical dimensions of monitor
xdpyinfo | grep -B 2 resolution
# screen #0:
#   dimensions:    3000x2000 pixels (346x230 millimeters)
#   resolution:    220x221 dots per inch

# X Resources
# https://wiki.archlinux.org/title/HiDPI#X_Resources

tee ~/.Xresources << EOF
Xft.dpi: 220
Xft.autohint: 0
Xft.lcdfilter: lcddefault
Xft.hintstyle: hintfull
Xft.hinting: 1
Xft.antialias: 1
Xft.rgba: rgb
EOF
# echo -e "Xft.dpi: 220\nXft.autohint: 0\nXft.lcdfilter: lcddefault\nXft.hintstyle: hintfull\nXft.hinting: 1\nXft.antialias: 1\nXft.rgba: rgb" > ~/.Xresources

echo -e "xrdb -merge ~/.Xresources" >> ~/.xinitrc
```


## Display manager

Most desktop environments include a [display manager](https://wiki.archlinux.org/title/Display_manager) for automatically starting the graphical environment and managing user logins. Users without a desktop environment can install one separately. Alternatively you may [start X at login](https://wiki.archlinux.org/title/Start_X_at_login) as a simple alternative to a display manager.

>A display manager, or login manager, is typically a graphical user interface that is displayed at the end of the boot process in place of the default shell. There are various implementations of display managers, just as there are various types of window managers and desktop environments. There is usually a certain amount of customization and themeability available with each one.

GNOME uses [GDM](https://wiki.archlinux.org/title/GDM) (The GNOME Display Manager) as its display manager.


## User directories

>Well-known user directories like Downloads or Music are created by the *xdg-user-dirs-update.service* user service, that is provided by `xdg-user-dirs` and enabled by default upon install. If your desktop environment or window manager does not pull in the package, you can install it and run `xdg-user-dirs-update` manually as per [XDG user directories#Creating default directories](https://wiki.archlinux.org/title/XDG_user_directories#Creating_default_directories).

```bash
pacman -S --noconfirm xdg-user-dirs

# Creating a full suite of localized default user directories within the $HOME directory
xdg-user-dirs-update
# ~/.config/user-dirs.dirs: used by applications to find and use home directories specific to an account.
# ~/.config/user-dirs.locale: used to set the language according to the locale in use.
```


If you wanna create custom directories, see [here](https://wiki.archlinux.org/title/XDG_user_directories#Creating_custom_directories).


## Change Log

* Jun 27, 2019 09:15 Thu ET
  * First draft


[mbxp]:https://consumer.huawei.com/en/laptops/matebook-x-pro/
[general_recommendations]:https://wiki.archlinux.org/title/General_recommendations "Annotated index of post-installation tutorials and other popular articles."
[xorg]:https://wiki.archlinux.org/title/Xorg
[wayland]:https://wiki.archlinux.org/title/Wayland
[i3]:https://wiki.archlinux.org/title/I3 "i3 is a tiling window manager, completely written from scratch."
[nvidia_optimus]:https://wiki.archlinux.org/title/NVIDIA_Optimus
[nvidia-xrun]: https://wiki.archlinux.org/title/Nvidia-xrun
[bumblebee]:https://wiki.archlinux.org/title/Bumblebee
[optimus-manager]:https://github.com/Askannz/optimus-manager

<!-- End -->