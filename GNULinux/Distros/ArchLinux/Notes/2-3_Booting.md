# Booting

This note is based official document [General recommendations][general_recommendations]. It is an annotated index of popular articles and important information for improving and adding functionalities to the installed Arch system.

Official page [Booting](https://wiki.archlinux.org/title/General_recommendations#Booting)


## TOC

1. [Hardware auto-recognition](#hardware-auto-recognition)  
1.1 [Udev](#udev)  
2. [Microcode](#microcode)  
2.1 [Installation](#installation)  
2.1.1 [Detecting available microcode update](#detecting-available-microcode-update)  
2.2 [Early microcode updates](#early-microcode-updates)  
2.3 [Late microcode updates](#late-microcode-updates)  
3. [Retaining boot messages](#retaining-boot-messages)  
4. [Num Lock activation](#num-lock-activation)  
5. [Change Log](#change-log)  


## Hardware auto-recognition

>Hardware should be auto-detected by [udev](https://wiki.archlinux.org/title/Udev) during the boot process by default. A potential improvement in boot time can be achieved by disabling module auto-loading and specifying required modules manually, as described in [Kernel modules](https://wiki.archlinux.org/title/Kernel_modules). Additionally, [Xorg](https://wiki.archlinux.org/title/Xorg) should be able to auto-detect required drivers using *udev*, but users have the option to configure the X server manually too.

```bash
# lsmod | grep -i huawei
huawei_wmi             16384  0
ledtrig_audio          16384  3 snd_hda_codec_generic,huawei_wmi,snd_hda_codec_realtek
sparse_keymap          16384  1 huawei_wmi
wmi                    32768  5 intel_wmi_thunderbolt,huawei_wmi,wmi_bmof,mxm_wmi,nouveau
```

### Udev

*udev* is now part of [systemd](https://wiki.archlinux.org/title/Systemd) and is installed by default.

*udev* rules written by the administrator go in `/etc/udev/rules.d/`, their file name has to end with *.rules*. The udev rules shipped with various packages are found in `/usr/lib/udev/rules.d/`. If there are two files by the same name under `/usr/lib` and `/etc`, the ones in `/etc` take precedence.

[Udev#Tips_and_tricks](https://wiki.archlinux.org/title/Udev#Tips_and_tricks)

* Mounting drives in rules
* Accessing firmware programmers and USB virtual comm devices


## Microcode

[Microcode](https://en.wikipedia.org/wiki/Microcode) says:

>Processor manufacturers release stability and security updates to the processor microcode. While microcode can be updated through the BIOS, the Linux kernel is also able to apply these updates during boot. These updates provide bug fixes that can be critical to the stability of your system. Without these updates, you may experience spurious crashes or unexpected system halts that can be difficult to track down.

### Installation

```bash
# For AMD processors
pacman -S --noconfirm amd-ucode

# For Intel processors
pacman -S --noconfirm intel-ucode
```

#### Detecting available microcode update

>It is possible to find out if the `intel-ucode.img` contains a microcode image for the running CPU with [iucode-tool](https://www.archlinux.org/packages/?name=iucode-tool).

```bash
pacman -S --noconfirm iucode-tool
modprobe cpuid
bsdtar -Oxf /boot/intel-ucode.img | iucode_tool -tb -lS -
```

Output lists selected microcodes

```txt
iucode_tool: system has processor(s) with signature 0x000806ea
microcode bundle 1: (stdin)
selected microcodes:
  001/172: sig 0x000806e9, pf_mask 0xc0, 2019-04-01, rev 0x00b4, size 99328
  001/173: sig 0x000806e9, pf_mask 0x10, 2019-04-01, rev 0x00b4, size 98304
  001/174: sig 0x000806ea, pf_mask 0xc0, 2019-04-01, rev 0x00b4, size 99328
  001/175: sig 0x000806eb, pf_mask 0xd0, 2019-03-30, rev 0x00b8, size 98304
  001/176: sig 0x000806ec, pf_mask 0x94, 2019-03-30, rev 0x00b8, size 97280
```

### Early microcode updates

```bash
# GRUB automatic method
grub-mkconfig -o /boot/grub/grub.cfg
```

### Late microcode updates

Late loading of microcode updates happens after the system has booted. It uses files in */usr/lib/firmware/amd-ucode/* and */usr/lib/firmware/intel-ucode/*.

* For AMD processors the microcode update files are provided by `linux-firmware`.
* For Intel processors no package provides the microcode update files ([FS#59841](https://bugs.archlinux.org/task/59841)). To use late loading you need to manually extract intel-ucode/ from Intel's provided [archive](https://github.com/intel/Intel-Linux-Processor-Microcode-Data-Files).

How to enable/disable late microcode updates, see [Late microcode updates](https://wiki.archlinux.org/title/Microcode#Late_microcode_updates).


## Retaining boot messages

After the boot process, the screen is cleared and the login prompt appears, leaving users unable to gather feedback from the boot process.

[Disable clearing of boot messages](https://wiki.archlinux.org/title/General_troubleshooting#Console_messages) to overcome this limitation.

```bash
# list kernel message after booting
dmesg

# list all logs after booting
journalctl -b
```


## Num Lock activation

[MBXP][mbxp] has no Numlock, so I don't set it. More details in [Activating Numlock on Bootup](https://wiki.archlinux.org/title/Activating_Numlock_on_Bootup).


## Change Log

* Jun 26, 2019 13:50 Wed ET
  * First draft


[mbxp]:https://consumer.huawei.com/en/laptops/matebook-x-pro/
[general_recommendations]:https://wiki.archlinux.org/title/General_recommendations "Annotated index of post-installation tutorials and other popular articles."

<!-- End -->
