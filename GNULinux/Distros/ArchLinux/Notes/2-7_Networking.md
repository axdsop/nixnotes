# Networking


This note is based official document [General recommendations][general_recommendations]. It is an annotated index of popular articles and important information for improving and adding functionalities to the installed Arch system.

Official page [Networking](https://wiki.archlinux.org/title/General_recommendations#Networking)


## TOC

1. [Clock synchronization](#clock-synchronization)  
2. [DNS Security](#dns-security)  
3. [Setting up a firewall](#setting-up-a-firewall)  
4. [Resource sharing](#resource-sharing)  
5. [Change Log](#change-log)  


## Clock synchronization

See [Time synchronization](https://wiki.archlinux.org/title/Time_synchronization) for implementations of such protocol.


## DNS Security

For better security while browsing the web, paying online, connecting to SSH services and similar tasks consider using [DNSSEC](https://wiki.archlinux.org/title/DNSSEC)-enabled DNS resolver that can validate signed DNS records, and an encrypted protocol such as DNS over TLS, DNS over HTTPS or DNSCrypt. See Domain name resolution for details.


## Setting up a firewall

See [Category:Firewalls](https://wiki.archlinux.org/title/Category:Firewalls) for available guides.

Here I choose [ufw](https://wiki.archlinux.org/title/Uncomplicated_Firewall).

```bash
# https://help.ubuntu.com/community/UFW

pacman -S --noconfirm ufw
# Disable ipv6
[[ -f /etc/default/ufw ]] && sed -r -i '/^IPV6=/{s@(IPV6=).*@\1no@g;}' /etc/default/ufw 2> /dev/null

# setting default rule (deny incoming, allow outgoing)
ufw default deny incoming
ufw default allow outgoing
ufw --force enable
ufw status verbose

# If allow SSH
# ufw allow ssh
# ufw allow from 192.168.0.0/24 to any port 22
# ufw limit from 192.168.0.0/24 to any port 22
```

```bash
# sudo ufw status verbose
Status: active
Logging: on (low)
Default: allow (incoming), allow (outgoing), disabled (routed)
New profiles: skip
```


## Resource sharing

See also [Category:Network sharing](https://wiki.archlinux.org/title/Category:Network_sharing).


## Change Log

* Jun 27, 2019 10:50 Thu ET
  * First draft


[general_recommendations]:https://wiki.archlinux.org/title/General_recommendations "Annotated index of post-installation tutorials and other popular articles."

<!-- End -->
