# Prerequisites

This note is based official document [Installation guide][installation_guide]. It is a guide for installing [Arch Linux][archlinux] from the live system booted with the official installation image.

## TOC

1. [Hardware Requirements](#hardware-requirements)  
2. [Downloading ISO](#downloading-iso)  
3. [Verifying signature](#verifying-signature)  
3.1 [Via Checksum](#via-checksum)  
3.2 [Via PGP signature](#via-pgp-signature)  
4. [Bootable Flash Drive](#bootable-flash-drive)  
5. [BIOS](#bios)  
5.1 [BIOS Setting](#bios-setting)  
5.2 [BIOS Version](#bios-version)  
5.2.1 [Local manchine version](#local-manchine-version)  
5.2.2 [Online version](#online-version)  
6. [Boot The Live Environment](#boot-the-live-environment)  
7. [Change Log](#change-log)  


## Hardware Requirements

[Arch Linux][archlinux] just supports [x86_64](https://en.wikipedia.org/wiki/X86-64) compatible machine ([link](https://wiki.archlinux.org/title/Frequently_asked_questions#What_architectures_does_Arch_support? "What architectures does Arch support?")). The basic installation needs a minimum of *512* MB RAM and less then *800* MB of disk space. The installation process needs a working Internet connection.


## Downloading ISO

Download latest release version ISO from [Download][archlinux_download] page.

ISO release lists: [Releases](https://www.archlinux.org/releng/releases/)

Extracting latest release version

```bash
# Extracting latest release version
download_method='wget -qO-'
[[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && download_method='curl -fsSL'

$download_method 'https://www.archlinux.org/download/' | sed -r -n '/>(Current Release|Included Kernel|ISO Size|MD5|SHA1):/I{s@.*strong>[[:space:]]*([^<]+).*$@\1@g;p}' | sed ':a;N;$!ba;s@\n@|@g'
```

Current Release|Included Kernel|ISO Size|MD5|SHA1
---|---|---|---|---
2020.04.01|5.5.13|649.0�MB|e8eeda7b3d609fddf6497f05a60d63d2|bbece70b6e6a3f1d5f90a7d330657ea3c9280355

## Verifying signature

Verifying PGP signature or checksum (md5, sha1).

### Via Checksum

```bash
version_no='2020.04.01'

# md5
md5sum archlinux-${version_no}-x86_64.iso.sig
openssl dgst -md5 archlinux-${version_no}-x86_64.iso.sig

# sha1
sha1sum archlinux-${version_no}-x86_64.iso.sig
openssl dgst -sha1 archlinux-${version_no}-x86_64.iso.sig
```

### Via PGP signature

PGP signature file `.sig` can be found on [Download][archlinux_download] page.

Using personal custom function [GPGSignatureVerification](/CyberSecurity/GnuPG/Notes/2-1_UsageExample.md#package-signature-verifying) to verify PGP signature.

Usage

```bash
# https://www.archlinux.org/iso/2020.04.01/archlinux-2020.04.01-x86_64.iso.sig

# fn_GPGSignatureVerification 'ISOPATH' 'SIGPATH'
fn_GPGSignatureVerification 'archlinux-2020.04.01-x86_64.iso' 'archlinux-2020.04.01-x86_64.iso.sig'
# fn_GPGSignatureVerification archlinux-2020.04.01-x86_64.iso{,.sig}
```

Output

```bash
Importing PGP key
gpg: keybox '/tmp/VsWNAWU6/gpg_import.gpg' created
gpg: assuming signed data in 'archlinux-2020.04.01-x86_64.iso'
gpg: Signature made Tue 31 Mar 2020 11:38:31 PM EDT
gpg:                using RSA key 4AA4767BBC9C4B1D18AE28B77F2D434B9741E8AC
gpg: key 7F2D434B9741E8AC: 52 signatures not checked due to missing keys
gpg: key 7F2D434B9741E8AC: public key "Pierre Schmitz <pierre@archlinux.de>" imported
gpg: no ultimately trusted keys found
gpg: Total number processed: 1
gpg:               imported: 1
gpg: Good signature from "Pierre Schmitz <pierre@archlinux.de>" [unknown]
gpg: WARNING: This key is not certified with a trusted signature!
gpg:          There is no indication that the signature belongs to the owner.
Primary key fingerprint: 4AA4 767B BC9C 4B1D 18AE  28B7 7F2D 434B 9741 E8AC

Verifying signature
gpg: Signature made Tue 31 Mar 2020 11:38:31 PM EDT
gpg:                using RSA key 4AA4767BBC9C4B1D18AE28B77F2D434B9741E8AC
gpg: Good signature from "Pierre Schmitz <pierre@archlinux.de>" [unknown]
gpg: WARNING: This key is not certified with a trusted signature!
gpg:          There is no indication that the signature belongs to the owner.
Primary key fingerprint: 4AA4 767B BC9C 4B1D 18AE  28B7 7F2D 434B 9741 E8AC

Temporary directory /tmp/VsWNAWU6 has been removed.
```

Keywords `Good signature` indicates signature verification approves.


## Bootable Flash Drive

Creating the bootable flash drive with the ISO image.

For [*nix](https://en.wikipedia.org/wiki/Unix-like), just using command `dd`

```bash
iso_path="$HOME/Downloads/archlinux-2020.04.01-x86_64.iso"
usb_path='/dev/sdX' # via command 'fdisk -l', e.g. /dev/sda, /dev/sdb

# dd if=path/to/archlinux.iso of=/dev/sdx bs=4M status=progress oflag=sync
dd if="${iso_path}" of="${usb_path}" bs=4M status=progress oflag=sync && sync
```

For Windows, using [Rufus](https://rufus.ie/).

More options and details in wiki [USB flash installation media](https://wiki.archlinux.org/title/USB_flash_installation_media).


## BIOS

[MBXP][mbxp] is shipped with Microsoft [Windows 10][windows] by default. Since [Huawei][huawei] isn't a [vendor](https://fwupd.org/lvfs/vendorlist) of [LVFS][lvfs] team, BIOS can't not be [updated][mbxp_bios_update] by service `fwupd` on GNU/Linux. I'll preserve existed [Windows 10][windows] operating system, install GNU/Linux ([Arch Linux][archlinux]) in dual boot.

### BIOS Setting

To access the Boot menu, you need to disable *Secure Boot* in the BIOS.

Inserting the bootable flash drive into a USB port, then reboot.

Hold down key <kbd>F2</kbd> to enter BIOS:

>*Security Setting* --> *Secure Boot* --> *Disable* --> <kbd>F10</kbd>  --> *Save and Exit*.

Hold down key <kbd>F2</kbd> to enter the Boot menu:

>*Boot Option Menu* --> *EFI Boot Devices* --> *EFI USB Device* (General USB Flash Disk)


### BIOS Version

#### Local manchine version

```bash
/sys/devices/virtual/dmi/id/bios_date
# 11/12/2019

/sys/devices/virtual/dmi/id/bios_vendor
# HUAWEI

/sys/devices/virtual/dmi/id/bios_version
# 1.29
```

#### Online version

```bash
now_timestamp=$(date +'%s')
random_num=$(( ( RANDOM % 10 )  + 1 ))
old_timestamp=$((now_timestamp-random_num))
download_method='curl -fsL' # wget -qO-
$download_method "https://ccpce-de.consumer.huawei.com/ccpcmd/services/dispatch/secured/CCPC/EN/knowledgeBase/getServicePolicy/1000?jsonp=jQuery111304503105252517855_${old_timestamp}&knowTypeId=6310&orderType=1&page=1&pageSize=20&offeringCode=OFFE00055341&cflag=1&channelCode=WEBSITE&countryCode=US&langCode=en&country=US&language=en&siteCode=en_US&_=${now_timestamp}" | sed -r -n 's@^[^{]+(.*?)\)@\1@g;p' | sed -r -n 's@\{@\n\n&@g;p' | sed -r -n '/MateBook_X_Pro_BIOS/{s@","@"\n"@g;s@\{@@g;p}' | sed -r -n '/(resourceTitle|updateTime|downloadUrl|versionNumber)/{s@":"@|@g;s@"@@g;p}'

# downloadUrl|https://consumer-tkb.huawei.com/tkbapp/downloadWebsiteService?websiteId=962390
# resourceTitle|MateBook_X_Pro_BIOS
# updateTime|2020/3/24 14:16:17
# versionNumber|1.29

# date --date='2019/4/17 19:54:05' +'%F'
```

Output table

Key|Value
---|---
downloadUrl|https://consumer-tkb.huawei.com/tkbapp/downloadWebsiteService?websiteId=962390
resourceTitle|MateBook_X_Pro_BIOS
updateTime|2020/3/24 14:16:17
versionNumber|1.29

<!-- downloadUrl|https://consumer-tkb.huawei.com/tkbapp/downloadWebsiteService?websiteId=962390
resourceTitle|MateBook_X_Pro_BIOS_1.28
updateTime|2019/4/17 19:54:05
versionNumber|1.28 -->


## Boot The Live Environment

The live environment will be booted from a USB flash drive.

1. When the Arch menu appears, select *Boot Arch Linux* and press <kbd>Enter</kbd> to enter the installation environment.
2. See [README.bootparams](https://projects.archlinux.org/archiso.git/tree/docs/README.bootparams) for a list of [boot parameters](https://wiki.archlinux.org/title/Kernel_parameters#Configuration), and packages.x86_64 for a list of included packages.
3. You will be logged in on the first [virtual console](https://wiki.archlinux.org/title/Linux_console#Virtual_consoles) as the `root` user, and presented with a *Zsh* shell prompt.

We'll install [Arch Linux][archlinux] via virtual console.


## Change Log

* Jun 25, 2019 16:27 Tue ET
  * First draft
* Jul 13, 2019 12:34 Sat ET
  * Add BIOS version detection
* Aug 10, 2019 09:54 Sat ET
  * Add gpg signature verification function
* Apr 20, 2020 06:45 Mon ET
  * Update BIOS version from `1.28` to `1.29`


[huawei]:https://www.huawei.com "Huawei - Building a Fully Connected, Intelligent World"
[mbxp]:https://consumer.huawei.com/en/laptops/matebook-x-pro/
[archlinux]:https://www.archlinux.org "A lightweight and flexible Linux� distribution"
[archlinux_download]:https://www.archlinux.org/download/
[installation_guide]:https://wiki.archlinux.org/title/Installation_guide "Guide through the process of installing Arch Linux."
[windows]:https://www.microsoft.com/en-us/windows
[mbxp_bios_update]:https://consumer.huawei.com/en/support/how-to/detail-troubleshooting/?resourceId=en-us00698596
[lvfs]:https://fwupd.org "Linux Vendor Firmware Service"

<!-- End -->
