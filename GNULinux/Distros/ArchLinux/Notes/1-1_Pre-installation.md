# Pre-installation

This note is based official document [Installation guide][installation_guide]. It is a guide for installing [Arch Linux][archlinux] from the live system booted with the official installation image.

## TOC

1. [Console Font](#console-font)  
1.1 [Temporary Change](#temporary-change)  
1.2 [Persistent Change](#persistent-change)  
2. [Keyboard layout](#keyboard-layout)  
3. [Verify the boot mode](#verify-the-boot-mode)  
3.1 [Via efivars](#via-efivars)  
3.2 [Via UEFI firmware bitness](#via-uefi-firmware-bitness)  
4. [Internet Connection](#internet-connection)  
5. [Update the system clock](#update-the-system-clock)  
6. [Change Log](#change-log)  


## Console Font

As [MBXP][mbxp] use [HiDPI][arch_hidpi] display, the virtual console font is too tiny to see.

[HiDPI#Linux console](https://wiki.archlinux.org/title/HiDPI#Linux_console) says:

>The default Linux console font will be very small on hidpi displays, the largest font present in the kbd package is `latarcyrheb-sun32` and other packages like `terminus-font` contain further alternatives, such as `ter-132n`(normal) and `ter-132b`(bold).

[Set the keyboard layout](https://wiki.archlinux.org/title/Installation_guide#Set_the_keyboard_layout) says:

>[Console fonts](https://wiki.archlinux.org/title/Console_fonts) are located in `/usr/share/kbd/consolefonts/` and can likewise be set with `setfont`(8).

### Temporary Change

```bash
# shows a table of glyphs or letters of a font
showconsolefont

# temporary change console font on HiDPI display
setfont latarcyrheb-sun32
```

The virtual console font will become bigger.

### Persistent Change

[Linux_console#Persistent_configuration](https://wiki.archlinux.org/title/Linux_console#Persistent_configuration) says:

>The `FONT` variable in `/etc/vconsole.conf` is used to set the font at boot, persistently for all consoles.

```bash
# persistent configuration
vconsole_conf='/etc/vconsole.conf'
[[ -f "${vconsole_conf}" ]] && sed -r -i '/^FONT=/d' "${vconsole_conf}"
echo "FONT=latarcyrheb-sun32" >> "${vconsole_conf}"
```


## Keyboard layout

The default [console keymap](https://wiki.archlinux.org/title/Console_keymap) is `US`.

Available layouts can be listed with:

```bash
ls /usr/share/kbd/keymaps/**/*.map.gz
```

To modeify the layout

```bash
loadkeys us
# loadkeys de-latin1
```

Here I use the default console map.


## Verify the boot mode

To check if the UEFI mode is enabled, we have 2 methods:

```bash
if [[ -d /sys/firmware/efi/efivars || -f /sys/firmware/efi/fw_platform_size ]]; then
    echo 'Boot mode is UEFI.'
fi
```

### Via efivars

If the `efivars` directory `/sys/firmware/efi/efivars` existed, the boot mode is *UEFI* mode, otherwise it may be *BIOS* or *CSM* mode.

### Via UEFI firmware bitness

Also we can use [UEFI firmware bitness](https://wiki.archlinux.org/title/Unified_Extensible_Firmware_Interface#UEFI_firmware_bitness) to verify if the boot mode is *UEFI*.

>The firmware bitness can be checked from a booted operating system.

```bash
cat /sys/firmware/efi/fw_platform_size
# 64 / 32
```

>It will return 64 for a 64-bit (x86_64) UEFI or 32 for a 32-bit (IA32) UEFI. If the file does not exist, then you have not booted in UEFI mode.


## Internet Connection

As [MBXP][mbxp] doesn't provide RJ45 connector, we need to connect to Internet via [wireless LAN](https://wiki.archlinux.org/title/Wireless_network_configuration). ~~Here I use [netctl][netctl] to login onto Wi-Fi network, use *wifi-menu* to generate netctl profile.~~ [Arch Linux][archlinux] live iso doesn't provides utility *wifi-menu* by default. So I change to use utility `iwctl`.

<details>
<summary>Click to view iwctl introduction</summary>

[Iwd#Installation](https://wiki.archlinux.org/title/Iwd#Installation) says:

>The iwd package provides the client program `iwctl`, the daemon `iwd` and the Wi-Fi monitoring tool `iwmon`.


</details>


<details>
<summary>Click to view netctl introduction</summary>

[Netctl#Installation](https://wiki.archlinux.org/title/Netctl#Installation) says:

>~~netctl is part of the `base` group, so it should already be installed on your system.~~

[Arch Linux][archlinux] removes group *base* on the base package. ([commit](https://github.com/archlinux/svntogit-packages/commit/8ad22f471b17d4788a3cd541270173c4fdf78263#diff-3e341d2d9c67be01819b25b25d5e53ea3cdf3a38d28846cda85a195eb9b7203a) Oct 6, 2019).

[Netctl#Configuration](https://wiki.archlinux.org/title/Netctl#Configuration) says:

>netctl uses profiles to manage network connections and different modes of operation to start profiles automatically or manually on demand.
>
>For wireless settings, you can use *wifi-menu -o* as root to generate the profile file in `/etc/netctl/`. The `dialog` package is required to use *wifi-menu*.

[Obfuscate wireless passphrase](https://wiki.archlinux.org/title/Netctl#Obfuscate_wireless_passphrase) says:

>You can also follow the following step to obfuscate the wireless passphrase (`wifi-menu` does it automatically when using the `-o` flag).

</details>


```bash
# list network interface
ip link
# ip link set wlp60s0 up

# deprecated
# profiles list in /etc/netctl/, wifi-menu will auto start profile
# wifi-menu
# wifi-menu -o  # obfuscate wireless passphrase

# Via iwctl
SSID='xxxxx'
iwctl station wlan0 connect $SSID

# Testing (wait seconds to make effect)
# ping -c 3 archlinux.org
ping -c 3 google.com
```

**Attention**: If fail to connect, command `system status netctl@wlp60s0XXXXX` prompts *The interface of network profile 'wlp60s0XXXXX' is already up* , likes [Netctl can't use interface, interface is up](https://bbs.archlinux.org/viewtopic.php?pid=1266983#p1266983). Try

```bash
ip link set wlp60s0 down
netctl start wlp60s0XXXXX
```

## Update the system clock

Active network time synchronization

```bash
timedatectl set-ntp true
```

Check service status

```bash
timedatectl status

# disable
System clock synchronized: no
              NTP service: inactive

# enable
System clock synchronized: yes
              NTP service: active
```


## Change Log

* Jun 25, 2019 16:50 Tue ET
  * First draft
* Nov 22, 2020 08:24 Sun ET
  * Change wifi connection from `wifi-menu` to `iwctl`


[mbxp]:https://consumer.huawei.com/en/laptops/matebook-x-pro/
[archlinux]:https://www.archlinux.org "A lightweight and flexible Linux� distribution"
[installation_guide]:https://wiki.archlinux.org/title/Installation_guide "Guide through the process of installing Arch Linux."
[arch_hidpi]:https://wiki.archlinux.org/title/HiDPI
[netctl]:https://wiki.archlinux.org/title/Netctl

<!-- End -->
