# Partition

This note is based official document [Installation guide][installation_guide]. It is a guide for installing [Arch Linux][archlinux] from the live system booted with the official installation image.

[MBXP][mbxp] use `Samsung NVMe SSD` which block device name is `nvme0n1`.

## TOC

1. [Official Documents](#official-documents)  
2. [Existed Partitions](#existed-partitions)  
3. [Partition Layouts Analysis](#partition-layouts-analysis)  
3.1 [/boot](#boot)  
3.2 [SWAP](#swap)  
4. [Create Partitions](#create-partitions)  
5. [Format Partitions](#format-partitions)  
6. [Mount the file systems](#mount-the-file-systems)  
7. [Change Log](#change-log)  


## Official Documents

Arch Linux [Wiki resources][archlinux_wiki]

* [Partitioning](https://wiki.archlinux.org/title/Partitioning)
* [EFI system partition](https://wiki.archlinux.org/title/EFI_system_partition)
  * [GPT partitioned disks](https://wiki.archlinux.org/title/EFI_system_partition#GPT_partitioned_disks)
  * [Format the partition](https://wiki.archlinux.org/title/EFI_system_partition#Format_the_partition)
  * [Mount the partition](https://wiki.archlinux.org/title/EFI_system_partition#Mount_the_partition)
* [Parted](https://wiki.archlinux.org/title/Parted)


## Existed Partitions

[MBXP][mbxp] is shipped with Microsoft [Windows 10][windows] by default.

Disk path detection

```bash
# disk path detection
# nvme0n1
disk_name=$(lsblk -l | sed -r -n '/:0.*disk/{/^sd/d;s@^([^[:space:]]+).*$@\1@g;p}')
# /dev/nvme0n1
nvme_path="/dev/${disk_name}"
```

NVMe testing

```bash
# https://wiki.archlinux.org/title/Solid_state_drive/NVMe#Testing
sudo hdparm -Tt --direct "${nvme_path}"

# /dev/nvme0n1:
#  Timing O_DIRECT cached reads:   3720 MB in  1.99 seconds = 1866.26 MB/sec
#  HDIO_DRIVE_CMD(identify) failed: Inappropriate ioctl for device
#  Timing O_DIRECT disk reads: 5456 MB in  3.00 seconds = 1818.05 MB/sec
```

Print partition info

```bash
parted "${nvme_path}" print free
```

Number | Start | End | Size | File system | Name | Flags
---|---|---|---|---|---|---
1 | 1049KB | 106MB | 105MB | fat32 | EFI system partition | boot, esp
2 | 106MB | 123MB | 16.8MB | | Microsoft reserved partition | msftres
3 | 123MB | 135GB | 135GB | ntfs | Basic data partition | msftdata
4 | 135GB | 350GB | 215GB | ntfs | Basic data partition | msftdata
  | 350GB | 512GB | 162GB | Free Space

It has a pre-instlled Windows 10, here available disk space is [350, 512] GB.

**Notice**: `GiB` size expressed in powers of *2*, `GB` size expressed in powers of *10*.


## Partition Layouts Analysis

UEFI with GPT example layouts (350GB ~ 512GB)

Mount point | Partition | Partition type | Filesystem Type | Size | Partition Size
---|---|---|---|---|---
/mnt/boot | /dev/nvme0n1p5 | EFI system partition | fat32 | 1 GB | 350 ~ 351 GB
/mnt | /dev/nvme0n1p6 | Linux x86-64 root (/) | xfs | 50 GB | 351 ~ 401 GB
[SWAP] | /dev/nvme0n1p7 | Linux swap | linux-swap | 24 GB | 401 ~ 425 GB
/mnt/home | /dev/nvme0n1p8 | Linux x86-64 home (/home) | xfs | 45 GB | 425 ~ 470 GB
/mnt/opt | /dev/nvme0n1p9 | Linux x86-64 opt (/opt) | xfs | 35 GB | 470 ~ 505 GB


### /boot

The *EFI system partitFormat the partitionsion* (also called `ESP`) is an OS independent partition that acts as the storage place for the EFI bootloaders, applications and drivers to be launched by the UEFI firmware.

[GRUB - UEFI systems](https://wiki.archlinux.org/title/GRUB#Installation_2) says:

>To boot from a disk using UEFI, an EFI system partition is required. Follow [EFI system partition#Check](https://wiki.archlinux.org/title/EFI_system_partition#Check_for_an_existing_partition) for an existing partition to find out if you have one already, otherwise you need to create it.

[Mount the partition](https://wiki.archlinux.org/title/EFI_system_partition#Mount_the_partition) says:

>The kernels, initramfs files, and, in most cases, the processor's microcode, need to be accessible by the boot loader or UEFI itself to successfully boot the system.
>
>Mount ESP to `/boot`. This is the preferred method when directly booting a [EFISTUB](https://wiki.archlinux.org/title/EFISTUB) kernel from UEFI.

So just use `/boot`, don't create `/efi` or `/boot/efi`.


### SWAP

[Systemd](https://wiki.archlinux.org/title/Systemd) began to support for *suspend-then-hibernate* from version [239](https://github.com/systemd/systemd/blob/master/NEWS "CHANGES WITH 239") in June 2018 . A sleep mode where the system initially suspends, and after a timeout resumes and hibernates again. Currently [systemd](https://jlk.fjfi.cvut.cz/arch/manpages/man/systemctl.1) version in [core/systemd](https://www.archlinux.org/packages/core/x86_64/systemd/) is `242.29-3`.

In order to support *suspend-then-hibernate*, I decide to use swap space. From [RedHat 7 - Recommended swap space if allowing for hibernation](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/installation_guide/sect-disk-partitioning-setup-x86#idm139724490385440), swap space is *16 x 1.5 = 24* GB.

```bash
# total memory size GB
free --giga | sed -r -n '/^Mem:/{s@^[^[:space:]]+[[:space:]]*([^[:space:]]+).*$@\1@g;p}'
```

## Create Partitions

```bash
# https://wiki.archlinux.org/title/Parted#UEFI/GPT_examples

# parted [Disk Name] [mkpart] [Partition Type] [Filesystem Type] [Partition Start Size] [Partition End Size]
# - Partition Type: primary, logical, extended
# - Filesystem Type: xfs, btrfs, ext4, fat32, ntfs, linux-swap, ...

parted /dev/nvme0n1 print

# remove partitions except Windows 10 partitions
parted /dev/nvme0n1 print | sed -r -n '1,/msftdata/d;/msftdata/d;/^$/d;s@^[[:space:]]*([^[:space:]]+).*$@\1@g;p' | sort -r | while read -r num; do
    [[ -n "${num}" ]] && parted /dev/nvme0n1 rm "${num}"
done


# 5 1GB
parted /dev/nvme0n1 mkpart extended fat32 350GB 351GB
# (parted) set 5 boot on
# https://wiki.archlinux.org/title/EFI_system_partition#GPT_partitioned_disks
# GNU Parted: Create a partition with fat32 as the file system type and set the 'esp' flag on it.
parted /dev/nvme0n1 set 5 esp on

# 6 50GB
parted /dev/nvme0n1 mkpart extended xfs 351GB 401GB
# 7 24GB
parted /dev/nvme0n1 mkpart extended linux-swap 401GB 425GB
# 8 45GB
parted /dev/nvme0n1 mkpart extended xfs 425GB 470GB
# 9 35GB
parted /dev/nvme0n1 mkpart extended xfs 470GB 505GB
```

## Format Partitions

```bash
# https://wiki.archlinux.org/title/EFI_system_partition#Format_the_partition

mkfs.fat -F32 /dev/nvme0n1p5

mkfs.xfs -f /dev/nvme0n1p6

mkswap /dev/nvme0n1p7
# swapon /dev/nvme0n1p7

mkfs.xfs -f /dev/nvme0n1p8
mkfs.xfs -f /dev/nvme0n1p9
```

## Mount the file systems

Create any remaining mount points (such as `/mnt/boot`) and mount their corresponding partitions.

First mount `/mnt`, then mount `/mnt/boot`

```bash
# https://wiki.archlinux.org/title/EFI_system_partition#Mount_the_partition

# mount root partition to /mnt
mount /dev/nvme0n1p6 /mnt

# mount boot partition to /mnt/boot/
mkdir -p /mnt/boot
mount /dev/nvme0n1p5 /mnt/boot/

swapon /dev/nvme0n1p7

# mount home directory to /mnt/home
mkdir -p /mnt/home
mount /dev/nvme0n1p8 /mnt/home

# mount /opt directory to /mnt/opt
mkdir -p /mnt/opt
mount /dev/nvme0n1p9 /mnt/opt
```


## Change Log

* Jun 25, 2019 16:50 Tue ET
  * First draft


[mbxp]:https://consumer.huawei.com/en/laptops/matebook-x-pro/
[windows]:https://www.microsoft.com/en-us/windows
[archlinux]:https://www.archlinux.org "A lightweight and flexible Linux� distribution"
[archlinux_wiki]:https://wiki.archlinux.org/
[installation_guide]:https://wiki.archlinux.org/title/Installation_guide "Guide through the process of installing Arch Linux."


<!-- End -->
