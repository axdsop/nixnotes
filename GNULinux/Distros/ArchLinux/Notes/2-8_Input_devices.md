# Input devices

This note is based official document [General recommendations][general_recommendations]. It is an annotated index of popular articles and important information for improving and adding functionalities to the installed Arch system.

Official page [Input devices](https://wiki.archlinux.org/title/General_recommendations#Input_devices)


[general_recommendations]:https://wiki.archlinux.org/title/General_recommendations "Annotated index of post-installation tutorials and other popular articles."

<!-- End -->
