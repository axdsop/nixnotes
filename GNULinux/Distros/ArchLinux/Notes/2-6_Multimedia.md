# Multimedia

This note is based official document [General recommendations][general_recommendations]. It is an annotated index of popular articles and important information for improving and adding functionalities to the installed Arch system.

Official page [Multimedia](https://wiki.archlinux.org/title/General_recommendations#Multimedia)


## TOC

1. [Sound](#sound)  
2. [Browser plugins](#browser-plugins)  
3. [Codes](#codes)  
4. [Change Log](#change-log)  


## Sound

[Sound](https://wiki.archlinux.org/title/Sound) is provided by kernel sound drivers:

* [ALSA](https://wiki.archlinux.org/title/ALSA) is included with the kernel and is recommendeOSSd because usually it works out of the box (it just needs to be [unmuted](https://wiki.archlinux.org/title/Advanced_Linux_Sound_Architecture#Unmuting_the_channels)).
* [OSS](https://wiki.archlinux.org/title/OSS) is a viable alternative in case ALSA does not work.

Users may additionally wish to install and configure a sound server such as [PulseAudio](https://wiki.archlinux.org/title/PulseAudio). For advanced audio requirements, see [professional audio](https://wiki.archlinux.org/title/Professional_audio).


## Browser plugins

For access to certain web content, [browser plugins](https://wiki.archlinux.org/title/Browser_plugins) such as Adobe Acrobat Reader, Adobe Flash Player, and Java can be installed.


## Codes

[Codecs](https://wiki.archlinux.org/title/Codecs_and_containers) are utilized by multimedia applications to encode or decode audio or video streams. In order to play encoded streams, users must ensure an appropriate codec is installed.


## Change Log

* Jun 27, 2019 10:35 Thu ET
  * First draft


[general_recommendations]:https://wiki.archlinux.org/title/General_recommendations "Annotated index of post-installation tutorials and other popular articles."

<!-- End -->
