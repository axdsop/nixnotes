# Power Management

This note is based official document [General recommendations][general_recommendations]. It is an annotated index of popular articles and important information for improving and adding functionalities to the installed Arch system.

Official page [Power management](https://wiki.archlinux.org/title/General_recommendations#Power_management)

See [Power management](https://wiki.archlinux.org/title/Power_management) for more general overview.


## TOC

1. [ACPI events](#acpi-events)  
2. [CPU frequency scaling](#cpu-frequency-scaling)  
3. [Userspace Tools](#userspace-tools)  
3.1 [Powertop](#powertop)  
3.2 [TLP](#tlp)  
4. [Power Management With Systemd](#power-management-with-systemd)  
4.1 [Suspend and Hibernate](#suspend-and-hibernate)  
4.2 [Hibernate on low battery level](#hibernate-on-low-battery-level)  
5. [Power saving](#power-saving)  
6. [Change Log](#change-log)  


## ACPI events

Users can configure how the system reacts to ACPI events such as pressing the power button or closing a laptop's lid. For the new (recommended) method using [systemd](https://wiki.archlinux.org/title/Systemd), see [Power management with systemd](https://wiki.archlinux.org/title/Power_management#Power_management_with_systemd). For the old method, see [acpid](https://wiki.archlinux.org/title/Acpid).


## CPU frequency scaling

Modern processors can decrease their frequency and voltage to reduce heat and power consumption. Less heat leads to more quiet system and prolongs the life of hardware. See [CPU frequency scaling](https://wiki.archlinux.org/title/CPU_frequency_scaling) for details.

>CPU frequency scaling enables the operating system to scale the CPU frequency up or down in order to save power. CPU frequencies can be scaled automatically depending on the system load, in response to ACPI events, or manually by userspace programs.
>
>CPU frequency scaling is implemented in the Linux kernel, the infrastructure is called `cpufreq`.
>
>Userspace tools like `cpupower`, `acpid`, [Laptop Mode Tools](https://wiki.archlinux.org/title/Laptop_Mode_Tools), or GUI tools provided for your desktop environment, may still be used for advanced configuration.

Here I choose [TLP][tlp] manage CPU frequency scaling.


## Userspace Tools

[System76 Battery Life Improvements](https://support.system76.com/articles/battery/) says:

>We recommend using TLP to quickly reduce overall power consumption and using powertop to check what software is consuming the battery.

### Powertop

PowerTop a diagnostic tool provided by Intel to identify and report issues with power consumption and management. It can be used as a reporting tool, an automated power management tool or both.

```bash
# https://wiki.archlinux.org/title/Powertop
pacman -S --noconfirm powertop

# produce a report
# powertop --html=powerreport.html

# calibration to prevent inaccurate measurement
# powertop --calibrate

# enable autotune service  https://wiki.manjaro.org/index.php?title=Power_Management#Automated_Tuning_with_PowerTop
echo -e '[Unit]\nDescription=PowerTop\n\n[Service]\nType=oneshot\nRemainAfterExit=true\nExecStart=/usr/bin/powertop --auto-tune\n\n[Install]\nWantedBy=multi-user.target\n' > /etc/systemd/system/powertop.service
sudo systemctl enable --now powertop.service

```

### TLP

TLP brings you the benefits of advanced power management for Linux without the need to understand every technical detail. TLP comes with a default configuration already optimized for battery life, so you may just install and forget it. Nevertheless TLP is highly customizable to fulfill your specific requirements.

Official documents

* [Quick Installing](https://linrunner.de/en/tlp/docs/tlp-linux-advanced-power-management.html)
* [TLP Settings](https://linrunner.de/en/tlp/docs/tlp-configuration.html)
* [TLP FAQ](https://linrunner.de/en/tlp/docs/tlp-faq.html)

```bash
# https://wiki.archlinux.org/title/TLP
pacman -S --noconfirm tlp tlp-rdw
tlp_config='/etc/default/tlp'
[[ -f /etc/tlp.conf ]] && tlp_config='/etc/tlp.conf'
[[ -f "${tlp_config}" ]] &&  sed -r -i '/^[[:space:]]*#*[[:space:]]*TLP_ENABLE=/{s@^[[:space:]]*#*[[:space:]]*([^=]+=).*@\11@g;}; /^[[:space:]]*#*[[:space:]]*TLP_DEFAULT_MODE/{s@^[[:space:]]*#*[[:space:]]*([^=]+=).*@\1BAT@g;}' "${tlp_config}"
# /^[[:space:]]*#*[[:space:]]*TLP_PERSISTENT_DEFAULT=/{s@^[[:space:]]*#*[[:space:]]*([^=]+=).*@\11@g;}

systemctl enable tlp.service
systemctl enable tlp-sleep.service
systemctl mask systemd-rfkill.service
systemctl mask systemd-rfkill.socket
# tlp-stat -s
# Notice: tlp.service is not enabled -- invoke "systemctl enable tlp.service" to correct this!
# Notice: tlp-sleep.service is not enabled -- invoke "systemctl enable tlp-sleep.service" to correct this!
# Notice: systemd-rfkill.service is not masked -- invoke "systemctl mask systemd-rfkill.service" to correct this!
# Notice: systemd-rfkill.socket is not masked -- invoke "systemctl mask systemd-rfkill.socket" to correct this!
```


## Power Management With Systemd

For a general overview of laptop-related articles and recommendations, see [Laptop](https://wiki.archlinux.org/title/Laptop).

### Suspend and Hibernate

In order to use hibernation, you need to create a swap partition or file.

Using a swap file instead of a swap partition requires an additional kernel parameter resume_offset=swap_file_offset. The value of swap_file_offset can be obtained by running `filefrag -v swap_file`, the output is in a table format and the required value is located in the first row of the physical_offset column.

```bash
# https://wiki.archlinux.org/title/Power_management/Suspend_and_hibernate
# https://wiki.archlinux.org/title/Power_management#Suspend_and_hibernate

funcConfigForGRUB(){
    local variable=${1:-}
    local parameter=${2:-}
    local config_path=${3:-'/etc/default/grub'}

    if [[ -f "${config_path}" && -n "${variable}" && -n "${parameter}" ]]; then
        if [[ -z $(sed -r -n '/'"${variable}"'=/{/'"${parameter%%=*}"'/{p}}' "${config_path}") ]]; then
            [[ -n "${swap_partition_uuid}" ]] && sed -r -i '/'"${variable}"'=/{s@([^=]+="[^"]+)(.*)@\1 '"${parameter}"'\2@g;}' "${config_path}"
        fi
    fi
}

funcConfigForMkinitcpio(){
    # https://wiki.archlinux.org/title/Mkinitcpio#Configuration
    local variable=${1:-} # MODULES/BINARIES/FILES/HOOKS/COMPRESSION/COMPRESSION_OPTIONS
    local parameter=${2:-}
    local config_path=${3:-'/etc/mkinitcpio.conf'}
    if [[ -f "${config_path}" && -n "${variable}" && -n "${parameter}" ]]; then
        variable="${variable^^}"
        if [[ -z $(sed -r -n '/^'"${variable}"'=/{/'"${parameter}"'/{p}}' "${config_path}") ]]; then
            sed -r -i '/^'"${variable}"'=/{s@([^\)]+)(.*)@\1 '"${parameter}"'\2@g;s@\([[:space:]]*@\(@g;}' "${config_path}"
        fi
    fi
}

# - 2-1 kernel parameters
# /dev/nvme0n1p7: UUID="e8fb1a17-9ebd-486c-b00f-8f85474dad66" TYPE="swap" PARTLABEL="extended" PARTUUID="654dd89b-77d5-4ecb-b11b-6dd5c125b436"

# grub_default_path='/etc/default/grub'
# if [[ -f "${grub_default_path}" && -z $(sed -r -n '/GRUB_CMDLINE_LINUX_DEFAULT=/{/resume=/{p}}' "${grub_default_path}") ]]; then
#     swap_partition_uuid=$(blkid | sed -r -n '/swap/I{s@[^(UUID)]*UUID="([^"]+)".*$@\1@g;p}')
#     [[ -n "${swap_partition_uuid}" ]] && sed -r -i '/GRUB_CMDLINE_LINUX_DEFAULT=/{s@([^=]+="[^"]+)(.*)@\1 resume=UUID='"${swap_partition_uuid}"'\2@g;}' "${grub_default_path}"
# fi

# blkid -o list | sed -r -n '/[[:space:]]+swap[[:space:]]+/{s@[[:space:]]*$@@g;s@.*[[:space:]]+([^[:space:]]+)@\1@g;p}' | sed '$!d'
# blkid -o full | sed -r -n '/type="swap"/I{s@.*UUID="([^"]+)".*@\1@g;p}' | sed '$!d'

swap_partition_uuid=${swap_partition_uuid:-}
swap_partition_uuid=$(blkid | sed -r -n '/type="swap"/I{s@.*UUID="([^"]+)".*@\1@g;p}' | sed '$!d')  # blkid need root permission
[[ -z "${swap_partition_uuid}" ]] && swap_partition_uuid=$(blkid -o full | sed -r -n '/type="swap"/I{s@.*UUID="([^"]+)".*@\1@g;p}' | sed '$!d')
[[ -z "${swap_partition_uuid}" ]] && swap_partition_uuid=$(blkid -o list | sed -r -n '/[[:space:]]+swap[[:space:]]+/{s@[[:space:]]*$@@g;s@.*[[:space:]]+([^[:space:]]+)@\1@g;p}' | sed '$!d')

[[ -n "${swap_partition_uuid}" ]] && funcConfigForGRUB 'GRUB_CMDLINE_LINUX_DEFAULT' "resume=UUID=${swap_partition_uuid}"
# - 2-2 configure the initramfs
# https://wiki.archlinux.org/title/Power_management/Suspend_and_hibernate#Configure_the_initramfs
# Whether by label or by UUID, the swap partition is referred to with a udev device node, so the 'resume' hook must go after the 'udev' hook.
# HOOKS=(base udev autodetect modconf block filesystems keyboard fsck)
# mkinitcpio_conf_path='/etc/mkinitcpio.conf'
funcConfigForMkinitcpio 'HOOKS' 'resume'
```


### Hibernate on low battery level

```bash
# https://wiki.archlinux.org/title/Laptop#Hibernate_on_low_battery_level

# Suspend the system when battery level drops to 5% or lower
# SUBSYSTEM=="power_supply", ATTR{status}=="Discharging", ATTR{capacity}=="[0-5]", RUN+="/usr/bin/systemctl suspend"

battery_low_threshold=${battery_low_threshold:-6}
[[ -d /etc/udev/rules.d/ ]] && echo -e "# Suspend the system when battery level drops to ${battery_low_threshold}% or lower\nSUBSYSTEM==\"power_supply\", ATTR{status}==\"Discharging\", ATTR{capacity}==\"[0-${battery_low_threshold}]\", RUN+=\"/usr/bin/systemctl hibernate\"" > /etc/udev/rules.d/99-lowbat.rules
# udevadm monitor --property  # testing battery report discharge events
```

## Power saving

Wiki [Power_management#Power_saving](https://wiki.archlinux.org/title/Power_management#Power_saving)

By default, audio power saving is turned off by most drivers. It can be enabled by setting the power_save parameter; a time (in seconds) to go into idle mode.

```bash
audio_kernel_driver=${audio_kernel_driver:-'snd_hda_intel'}
audio_kernel_driver=$(lspci -k | sed -r -n '/Audio/,/driver/{/driver/!d;s@.*?:[[:space:]]*([^[:space:]]+).*@\1@g;p;q}')
echo "options ${audio_kernel_driver} power_save=1" > /etc/modprobe.d/audio_powersave.conf
```

## Battery Protection

For Huawei laptop, if you wanna set up battery protection, see note [MBXP](/GNULinux/Laptop/MBXP/README.md) section *Battery Protection*.


```bash
huawei_wmi_charge_control_path='/sys/devices/platform/huawei-wmi/charge_control_thresholds'
start_charge_threshold=${start_charge_threshold:-60}
stop_charge_threshold=${stop_charge_threshold:-90}

[[ -f "${huawei_wmi_charge_control_path}" ]] && sudo bash -c "echo '${start_charge_threshold} ${stop_charge_threshold}' > ${huawei_wmi_charge_control_path}"
```


## Change Log

* Jun 27, 2019 10:23 Thu ET
  * First draft
* Apr 19, 2020 10:03 Sun ET
  * TLP add config file */etc/tlp.conf*, fix swap partition uuid extraction issue
* May 22, 2020 09:18 Fri ET
  * Add tool Powertop


[general_recommendations]:https://wiki.archlinux.org/title/General_recommendations "Annotated index of post-installation tutorials and other popular articles."
[tlp]:https://linrunner.de/en/tlp/tlp.html "TLP Linux Advanced Power Management"

<!-- End -->