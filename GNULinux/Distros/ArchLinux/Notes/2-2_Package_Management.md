# Package Management

This note is based official document [General recommendations][general_recommendations]. It is an annotated index of popular articles and important information for improving and adding functionalities to the installed Arch system.

Official page [Package management](https://wiki.archlinux.org/title/General_recommendations#Package_management)


## TOC

1. [Pacman](#pacman)  
1.1 [Tips and tricks](#tips-and-tricks)  
1.1.1 [Maintenance](#maintenance)  
1.1.2 [Installation And Recovery](#installation-and-recovery)  
1.1.3 [Performance](#performance)  
2. [Repositories](#repositories)  
3. [Mirrors](#mirrors)  
3.1 [Reflector Systemd Automation](#reflector-systemd-automation)  
3.1.1 [Reflector](#reflector)  
3.1.2 [Pacman-mirrors](#pacman-mirrors)  
4. [Arch Build System](#arch-build-system)  
5. [Arch User Repository](#arch-user-repository)  
5.1 [MAKEFLAGS](#makeflags)  
5.2 [yay](#yay)  
6. [Change Log](#change-log)  


## Pacman

>[pacman](https://wiki.archlinux.org/title/Pacman) is the Arch Linux package manager: all users are required to become familiar with it before reading any other articles.

See [pacman/Tips and tricks](https://wiki.archlinux.org/title/Pacman/Tips_and_tricks) for suggestions on how to improve your interaction with pacman and package management in general.

### Tips and tricks

#### Maintenance

Removing unused packages (orphans)

```bash
pacman -Rns $(pacman -Qtdq)
# pacman -Qtdq | xargs pacman -Rns --noconfirm
```

#### Installation And Recovery

List of installed packages

```bash
pacman -Qqe > pkglist.txt
```

Install packages from a list

```bash
pacman -S --needed - < pkglist.txt
```

Reinstalling all packages

```bash
pacman -Qqn | pacman -S -
```

Find applications that use libraries from older packages

```bash
pacman -S --noconfirm lsof
lsof +c 0 | grep -w DEL | awk '1 { print $1 ": " $NF }' | sort -u
# lsof +c 0 | awk 'match($0,/DEL/){print $1 ": "$NF}' | sort -u
```

#### Performance

[Download speeds](https://wiki.archlinux.org/title/Pacman/Tips_and_tricks#Download_speeds)

When downloading packages pacman uses the mirrors in the order they are in */etc/pacman.d/mirrorlist*. The mirror which is at the top of the list by default however may not be the fastest for you. To select a faster mirror, see [Mirrors](https://wiki.archlinux.org/title/Mirrors).

Pacman's speed in downloading packages can also be improved by using a different application to download packages, instead of pacman's built-in file downloader.


## Repositories

Enable multilib

>If you plan on using 32-bit applications, you will want to enable the [multilib](https://wiki.archlinux.org/title/Multilib) repository.

```bash
# https://wiki.archlinux.org/title/Official_repositories#Enabling_multilib
[[ -f /etc/pacman.conf ]] && sed -r -i '/^#*\[multilib\]/,/^$/{/^#+/{s@^#*@@g;}}' /etc/pacman.conf
```


## Mirrors

>Visit the [Mirrors](https://wiki.archlinux.org/title/Mirrors) article for steps on taking full advantage of using the fastest and most up to date mirrors of the official repositories.

* [Pacman Mirrorlist Generator](https://www.archlinux.org/mirrorlist/)
* [Mirror Status](https://www.archlinux.org/mirrors/status/)

### Reflector Systemd Automation

[Server-side ranking](https://wiki.archlinux.org/title/Mirrors#Server-side_ranking)

Using [Reflector](https://wiki.archlinux.org/title/Reflector) to update */etc/pacman.d/mirrorlist* automatically.

#### Reflector

```bash
# https://wiki.archlinux.org/title/Reflector#Systemd_service
tee /etc/systemd/system/reflector.service 1>/dev/null << EOF
[Unit]
Description=Pacman mirrorlist update
Wants=network-online.target
# After=network-online.target

[Service]
Type=oneshot
ExecStart=/usr/bin/reflector -a 24 --latest 50 --number 30 --protocol https --sort rate --save /etc/pacman.d/mirrorlist

[Install]
RequiredBy=multi-user.target
EOF

# sed ':a;N;$!ba;s@\n@\\n@g' /etc/systemd/system/reflector.service
# echo -e "[Unit]\nDescription=Pacman mirrorlist update\nWants=network-online.target\n# After=network-online.target\n\n[Service]\nType=oneshot\nExecStart=/usr/bin/reflector -a 24 --latest 50 --number 30 --protocol https --sort rate --save /etc/pacman.d/mirrorlist\n\n[Install]\nRequiredBy=multi-user.target" > /etc/systemd/system/reflector.service

# https://wiki.archlinux.org/title/Reflector#Systemd_timer
# https://wiki.archlinux.org/title/Systemd/Timers
tee /etc/systemd/system/reflector.timer 1>/dev/null <<EOF
[Unit]
Description=Run reflector periodically

[Timer]
OnBootSec=20min
OnUnitActiveSec=8h
RandomizedDelaySec=30min
# OnCalendar=weekly
# RandomizedDelaySec=15h
Persistent=true

[Install]
WantedBy=timers.target
EOF

# sed ':a;N;$!ba;s@\n@\\n@g' /etc/systemd/system/reflector.timer
# echo -e "[Unit]\nDescription=Run reflector periodically\n\n[Timer]\nOnBootSec=20min\nOnUnitActiveSec=8h\nRandomizedDelaySec=30min\n# OnCalendar=weekly\n# RandomizedDelaySec=15h\nPersistent=true\n\n[Install]\nWantedBy=timers.target" > /etc/systemd/system/reflector.timer

systemctl disable reflector.service
systemctl enable reflector.timer # just enable reflector.timer
```

#### Pacman-mirrors

It seems that [Reflector package **not** in Repository](https://forum.manjaro.org/t/reflector-package-not-in-repository/63464) on [Manjaro][manjaro].

File */etc/pacman.d/mirrorlist* contains one line

>Use pacman-mirrors to modify

[Manjaro][manjaro] uses [Pacman-mirrors][pacman-mirrors] which is a [Manjaro][manjaro] specific utility for generating and maintaining the system mirrorlist. [Pacman-mirrors][pacman-mirrors] uses the information available on the [Mirrorservice](https://repo.manjaro.org/).

```bash
# https://wiki.archlinux.org/title/Reflector#Systemd_service
tee /etc/systemd/system/reflector.service 1>/dev/null << EOF
[Unit]
Description=Pacman mirrorlist update
Wants=network-online.target
# After=network-online.target

[Service]
Type=oneshot
ExecStart=/usr/bin/pacman-mirrors -g -m rank -b stable -t 3

[Install]
RequiredBy=multi-user.target
EOF

# sed ':a;N;$!ba;s@\n@\\n@g' /etc/systemd/system/reflector.service
# echo -e "[Unit]\nDescription=Pacman mirrorlist update\nWants=network-online.target\n# After=network-online.target\n\n[Service]\nType=oneshot\nExecStart=/usr/bin/pacman-mirrors -g -m rank -b stable -t 3\n\n[Install]\nRequiredBy=multi-user.target" > /etc/systemd/system/reflector.service

# https://wiki.archlinux.org/title/Reflector#Systemd_timer
# https://wiki.archlinux.org/title/Systemd/Timers
tee /etc/systemd/system/reflector.timer 1>/dev/null <<EOF
[Unit]
Description=Run pacman-mirrors periodically

[Timer]
OnBootSec=20min
OnUnitActiveSec=8h
RandomizedDelaySec=30min
# OnCalendar=weekly
# RandomizedDelaySec=15h
Persistent=true

[Install]
WantedBy=timers.target
EOF

# sed ':a;N;$!ba;s@\n@\\n@g' /etc/systemd/system/reflector.timer
# echo -e "[Unit]\nDescription=Run pacman-mirrors periodically\n\n[Timer]\nOnBootSec=20min\nOnUnitActiveSec=8h\nRandomizedDelaySec=30min\n# OnCalendar=weekly\n# RandomizedDelaySec=15h\nPersistent=true\n\n[Install]\nWantedBy=timers.target" > /etc/systemd/system/reflector.timer

systemctl disable reflector.service
systemctl enable reflector.timer # just enable reflector.timer
```


## Arch Build System

*Ports* is a system initially used by BSD distributions consisting of build scripts that reside in a directory tree on the local system. Simply put, each port contains a script within a directory intuitively named after the installable third-party application.

The [Arch Build System](https://wiki.archlinux.org/title/Arch_Build_System) offers the same functionality by providing build scripts called [PKGBUILDs](https://wiki.archlinux.org/title/PKGBUILD), which are populated with information for a given piece of software: integrity hashes, project URL, version, license and build instructions. These PKGBUILDs are parsed by [makepkg](https://wiki.archlinux.org/title/Makepkg), the actual program that generates packages that are cleanly manageable by pacman.

Every package in the repositories along with those present in the AUR are subject to recompilation with *makepkg*.


## Arch User Repository

The [Arch User Repository](https://wiki.archlinux.org/title/Arch_User_Repository) (AUR) is a community-driven repository for Arch users. It contains package descriptions ([PKGBUILDs](https://wiki.archlinux.org/title/PKGBUILD)) that allow you to compile a package from source with [makepkg](https://wiki.archlinux.org/title/Makepkg) and then install it via [pacman](https://wiki.archlinux.org/title/Pacman#Additional_commands).

[Arch_User_Repository#Prerequisites](https://wiki.archlinux.org/title/Arch_User_Repository#Prerequisites) says:

>Packages in the AUR assume that the `base-devel` group is installed, i.e. they do not list the group's members as dependencies explicitly.

```bash
pacman -S --needed --noconfirm base-devel
```

### MAKEFLAGS

[makepkg][makepkg] is a script to automate the building of packages. The requirements for using the script are a build-capable Unix platform and a [PKGBUILD][pkgbuild].

[makepkg][makepkg] is provided by the `pacman` package.

>You may wish to adjust */etc/makepkg.conf* to optimize for your processor prior to building packages from the AUR. A significant improvement in compile times can be realized on systems with multi-core processors by adjusting the `MAKEFLAGS` variable. Users can also enable hardware-specific optimizations in [GCC](https://wiki.archlinux.org/title/GCC) via the `CFLAGS` variable. See [makepkg](https://wiki.archlinux.org/title/Makepkg) for more information.

[Building from files in memory](https://wiki.archlinux.org/title/Makepkg#Building_from_files_in_memory) says:

>As compiling requires many I/O operations and handling of small files, moving the working directory to a [tmpfs](https://wiki.archlinux.org/title/Tmpfs) may bring improvements in build times.

[Tmpfs#Usage](https://wiki.archlinux.org/title/Tmpfs#Usage) says:

>Arch uses a tmpfs */run* directory, with */var/run* and */var/lock* simply existing as symlinks for compatibility. It is also used for */tmp* by the default systemd setup and does not require an entry in [fstab](https://wiki.archlinux.org/title/Fstab) unless a specific configuration is needed.

So we can use *BUILDDIR=/tmp/makepkg* directly without any other configuration.

```bash
makepkg_conf=${makepkg_conf:-'/etc/makepkg.conf'}

if [[ -f "${makepkg_conf}" ]]; then
    [[ -f "${makepkg_conf}.origin" ]] || cp "${makepkg_conf}" "${makepkg_conf}.origin"

    # Building optimized binaries
    sed -r -i '/^#*[[:space:]]*CFLAGS=/{s@^#*[[:space:]]*@@g;s@-mtune=[^[:space:]]+[[:space:]]+@@g;s@(-march=)[^[:space:]]+@\1native@g}' "${makepkg_conf}"    # CFLAGS="-march=native -O2 -pipe -fno-plt"
    sed -r -i '/^#*[[:space:]]*CXXFLAGS=/{s@^#*[[:space:]]*([^=]+=).*@\1\"\$\{CFLAGS\}\"@g}' "${makepkg_conf}"    # CXXFLAGS="${CFLAGS}"
    sed -r -i '/^#*[[:space:]]*RUSTFLAGS=/{s@^#*[[:space:]]*([^=]+=.*?opt-level=[[:digit:]]+).*?(")$@\1 -C target-cpu=native\2@g}' "${makepkg_conf}"    # From pacman version 5.2.2,  # RUSTFLAGS="-C opt-level=2 -C target-cpu=native"

    # Parallel compilation
    # Enable multi-core processors
    local processor_num=${processor_num:-0}
    processor_num=$(nproc 2> /dev/null) # /usr/bin/nproc is owned by coreutils
    [[ "${processor_num}" -eq 0 ]]&& processor_num=$(sed -r -n '/^processor/{p}' /proc/cpuinfo | wc -l)
    [[ "${processor_num}" -gt 0 ]] && sed -r -i '/^#*[[:space:]]*MAKEFLAGS=/{s@^#*[[:space:]]*([^=]+=).*@\1"-j'${processor_num}'"@g}' "${makepkg_conf}"

    # Building from files in memory
    # The BUILDDIR variable can be temporarily exported to makepkg to set the build directory to an existing tmpfs.
    # https://wiki.archlinux.org/title/Tmpfs#Usage
    sed -r -i '/^#*[[:space:]]*BUILDDIR=/{s@^#*[[:space:]]*@@g;}' "${makepkg_conf}"

    # Utilizing multiple cores on compression
    # COMPRESSXZ     # xz    -T threads, --threads=threads    Specify  the  number of worker threads to use.  Setting threads to a special value 0 makes xz use as many threads as there are CPU cores on the system.
    sed -r -i '/^COMPRESSXZ=/{s@(.*-c).*?(-z.*)$@\1 -T 0 \2@g;}' "${makepkg_conf}"
    sed -r -i '/^COMPRESSZST=/{s@--threads=[[:digit:]]+@@g;s@(-q -)\)$@\1--threads=0\)@g}' "${makepkg_conf}"    # COMPRESSZST=(zstd -c -z -q - --threads=0)

    # Create uncompressed packages
    # https://wiki.archlinux.org/title/Makepkg#Use_other_compression_algorithms
    # https://wiki.manjaro.org/index.php?title=Makepkg#Create_uncompressed_packages
    sed -r -i '/^#*PKGEXT=/{s@^#*([^=]+=).*@\1'\''.pkg.tar'\'' # .pkg.tar.xz create uncompressed packages@g;}' "${makepkg_conf}"

    # Enable ccache for makepkg
    # https://wiki.archlinux.org/title/Ccache
    pacman -S --noconfirm ccache
    # ccache -s # ~/.ccache/ccache.conf /etc/ccache.conf  max_size = 5.0G
    # ccache -C # clear cache completely
    sed -r -i '/^BUILDENV=/{s@!(ccache)@\1@g;}' "${makepkg_conf}"
fi
```

### yay

Here use [Yet another Yogurt](https://github.com/Jguer/yay "An AUR Helper written in Go").


```bash
temp_yay_dir='/tmp/yay-bin'
mkdir -p "${temp_yay_dir}"
chmod 777 "${temp_yay_dir}" # directory yay-bin needs write permission

# - mothod 1
pacman -S --noconfirm git # base-devel see https://wiki.archlinux.org/title/Arch_User_Repository#Prerequisites
git clone https://aur.archlinux.org/yay-bin.git "${temp_yay_dir}"

# - method 2
# curl -fsL https://aur.archlinux.org/cgit/aur.git/snapshot/yay-bin.tar.gz | tar xfz - -C "${temp_yay_dir}" --strip-components=1


cd "${temp_yay_dir}"
# ERROR: Running makepkg as root is not allowed as it can cause permanent, catastrophic damage to your system
makepkg -si --noconfirm
# -s/--syncdeps automatically resolves and installs any dependencies with pacman before building. If the package depends on other AUR packages, you will need to manually install them first.
# -i/--install installs the package if it is built successfully. Alternatively the built package can be installed with pacman -U package_name.pkg.tar.xz.
cd ..
[[ -d "${temp_yay_dir}" ]] && rm -rf "${temp_yay_dir}"

# If run yay as root/sudo, prompt error info 'Please avoid running yay as root/sudo.' and 'Refusing to install AUR Packages as root, Abourting.'.
yay -Syu --cleanafter --noconfirm
```


## Change Log

* Jun 25, 2019 20:46 Tue ET
  * First draft
* Oct 29, 2019 08:47 Tue ET
  * Add another method to install *yay*
* Oct 29, 2019 20:36 Tue ET
  * *yay* install method optimization
* Oct 30, 2019 09:19 Wed ET
  * Add *Pacman-mirrors* systemd automation for Manjaro
* Mar 21, 2021 10:16 Sun ET
  * Update *makepkg* config optimization


[manjaro]:https://manjaro.org/ "Manjaro - enjoy the simplicity"
[pacman-mirrors]:https://wiki.manjaro.org/Pacman-mirrors
[general_recommendations]:https://wiki.archlinux.org/title/General_recommendations "Annotated index of post-installation tutorials and other popular articles."
[makepkg]:https://wiki.archlinux.org/title/Makepkg
[pkgbuild]:https://wiki.archlinux.org/title/PKGBUILD


<!-- End -->
