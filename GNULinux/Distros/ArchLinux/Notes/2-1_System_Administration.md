# System Administration

This note is based official document [General recommendations][general_recommendations]. It is an annotated index of popular articles and important information for improving and adding functionalities to the installed Arch system.

Official page [System administration](https://wiki.archlinux.org/title/General_recommendations#System_administration)

## TOC

1. [Privilege Escalation](#privilege-escalation)  
2. [Users and groups](#users-and-groups)  
3. [Service management](#service-management)  
4. [System maintenance](#system-maintenance)  
5. [Change Log](#change-log)  


## Privilege Escalation

>Both the [su](https://wiki.archlinux.org/title/Su) and [sudo](https://wiki.archlinux.org/title/Sudo) commands allow you to run commands as another user. By default *su* drops you to a login shell as the root user, and *sudo* by default temporarily grants you root privileges for a single command. See their respective articles for differences.

See also [Security#Restricting root](https://wiki.archlinux.org/title/Security#Restricting_root).

[Sudo](https://wiki.archlinux.org/title/Sudo) setting, sudo group name in [Arch Linux][archlinux] is `wheel`.

```bash
# package sudo exists in group package 'base-devel'
[[ -f /etc/sudoers ]] || pacman -S --noconfirm sudo
[[ -f /etc/sudoers ]] && sed -r -i 's@#*[[:space:]]*(%wheel[[:space:]]+ALL=\(ALL\)[[:space:]]+ALL)@# \1@;s@#*[[:space:]]*(%wheel[[:space:]]+ALL=\(ALL\)[[:space:]]+NOPASSWD: ALL).*@\1,!/bin/su@' /etc/sudoers
```


## Users and groups

>A new installation leaves you with only the superuser account, better known as "root". Logging in as root for prolonged periods of time, possibly even exposing it via SSH on a server, is insecure. Instead, you should create and use unprivileged user account(s) for most tasks, only using the root account for system administration.

Add new normal user and add it into sudo group *wheel*. This user is forced to change password while first login in.

```bash
normal_user_name='flying'
default_password="ArchLinux_$(date +'%Y')"  # eg This year is 2019, password is ArchLinux_2019

login_shell=${login_shell:-'/bin/bash'}
# If you have executed 'chage -d0 root', it will prompts error 'You are required to change your password immediately (administrator enforced)', 'useradd: PAM: Authentication token is no longer valid; new one required'
useradd -m -N -G wheel -s "${login_shell}" "${normal_user_name}"
# useradd -mN -G wheel "${normal_user_name}"
# usermod -s "${login_shell}" "${normal_user_name}" # change login shell
echo "${normal_user_name}:${default_password}" | chpasswd
# echo -e "${default_password}\n${default_password}" | passwd "${normal_user_name}"
chage -d0 "${normal_user_name}"  # new created user have to change passwd when first login
```


## Service management

Official page [Service management](https://wiki.archlinux.org/title/Systemd#Basic_systemctl_usage)

* [systemd](https://wiki.archlinux.org/title/Systemd)
* [init](https://wiki.archlinux.org/title/Init)


## System maintenance

Official page [System maintenance](https://wiki.archlinux.org/title/System_maintenance)

Read [Security](https://wiki.archlinux.org/title/Security) for recommendations and best practices on hardening the system.


## Change Log

* Jun 25, 2019 20:42 Tue ET
  * First draft


[archlinux]:https://www.archlinux.org "A lightweight and flexible Linux� distribution"
[general_recommendations]:https://wiki.archlinux.org/title/General_recommendations "Annotated index of post-installation tutorials and other popular articles."

<!-- End -->
