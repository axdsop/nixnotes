# Installation

This note is based official document [Installation guide][installation_guide]. It is a guide for installing [Arch Linux][archlinux] from the live system booted with the official installation image.

## TOC

1. [Mirror Selection](#mirror-selection)  
1.1 [Sorting via rankmirrors](#sorting-via-rankmirrors)  
1.2 [Sorting via Reflector (Prefer)](#sorting-via-reflector-prefer)  
2. [base packages](#base-packages)  
3. [Change Log](#change-log)  


## Mirror Selection

Packages to be installed must be downloaded from mirror servers, which are defined in */etc/pacman.d/mirrorlist*. The higher a mirror is placed in the list, the more priority it is given when downloading a package.

This file will later be copied to the new system by `pacstrap`, so it is worth getting right.

[mirrors#Official_mirrors](https://wiki.archlinux.org/title/mirrors#Official_mirrors) says:

>The official Arch Linux mirror list is available from the `pacman-mirrorlist` package. To get an even more up-to-date list of mirrors, use the [Pacman Mirrorlist Generator](https://www.archlinux.org/mirrorlist/) page on the main site.

Force pacman to refresh the package lists

```bash
# https://wiki.archlinux.org/title/System_maintenance#Avoid_certain_pacman_commands
# Avoid doing partial upgrades. In other words, never run pacman -Sy; instead, always use pacman -Syu.
# Passing two --refresh or -y flags will force a refresh of all package databases, even if they appear to be up-to-date.  -- man pacman
pacman -Syyu --noconfirm
```

### Sorting via rankmirrors

I'm not recommend to use `rankmirrors`, this script use `while`, `for` control flow, test url one by one, consume too much time.

```bash
# The pacman-contrib package provides a Bash script /usr/bin/rankmirrors
pacman -S --noconfirm pacman-contrib

fastest_url_num=15

# - Method 1: Ranking an existing mirror list
[[ -f /etc/pacman.d/mirrorlist.backup ]] || cp /etc/pacman.d/mirrorlist{,.backup}
sed -i 's/^#Server/Server/' /etc/pacman.d/mirrorlist.backup
rankmirrors -n "${fastest_url_num}" /etc/pacman.d/mirrorlist.backup > /etc/pacman.d/mirrorlist

# - Method 2: Fetching and ranking a live mirror list
cp /etc/pacman.d/mirrorlist{,.backup}
curl -s "https://www.archlinux.org/mirrorlist/?country=FR&country=GB&protocol=https&use_mirror_status=on" | sed -e 's/^#Server/Server/' -e '/^#/d' | rankmirrors -n "${fastest_url_num}" - > /etc/pacman.d/mirrorlist
```

### Sorting via Reflector (Prefer)

[Reflector](http://xyne.archlinux.ca/projects/reflector/) is a script which can retrieve the latest mirror list from the [MirrorStatus](https://www.archlinux.org/mirrors/status/) page, filter the most up-to-date mirrors, sort them by speed and overwrite the file */etc/pacman.d/mirrorlist*.

```bash
# install the reflector package
pacman -Sy --noconfirm reflector

[[ -f /etc/pacman.d/mirrorlist.backup ]] || cp /etc/pacman.d/mirrorlist{,.backup}

# reflector --list-countries

country_code=${country_code:-'US'} # US or United States
# ipinfo.io   The limit is 1,000 requests per day from an IP address.
# ip-api.com  The limit is 150 requests per minute from an IP address.
country_code=$(timeout 3 curl ipinfo.io/country 2> /dev/null || timeout 3 curl ip-api.com/line/?fields=countryCode 2> /dev/null)
# select 50 HTTPS mirrors synchronized within the last 24 hours most recently, sort then by download speed, and overwrite the file /etc/pacman.d/mirrorlist
reflector --verbose -a 24 --latest 100 --number 50 --protocol https --sort rate --country "${country_code}" --threads 10 --save /etc/pacman.d/mirrorlist
```


## base packages

Use the `pacstrap` script to install the package group `base`, `base-devel`.

>This group does not include all tools from the live installation, such as btrfs-progs or specific wireless firmware;

```bash
pacstrap /mnt base base-devel
```

Included packages

```bash
# Package group - base
# pacman -Sg base | cut -d' ' -f2 | sed ':a;N;$!ba;s@\n@ @g'
bash bzip2 coreutils cryptsetup device-mapper dhcpcd diffutils e2fsprogs file filesystem findutils gawk gcc-libs gettext glibc grep gzip inetutils iproute2 iputils jfsutils less licenses linux linux-firmware logrotate lvm2 man-db man-pages mdadm nano netctl pacman pciutils perl procps-ng psmisc reiserfsprogs s-nail sed shadow sysfsutils systemd-sysvcompat tar texinfo usbutils util-linux vi which xfsprogs

# Package group - base-devel
# pacman -Sg base-devel | cut -d' ' -f2 | sed ':a;N;$!ba;s@\n@ @g'
autoconf automake binutils bison fakeroot file findutils flex gawk gcc gettext grep groff gzip libtool m4 make pacman patch pkgconf sed sudo systemd texinfo util-linux which
```


## Change Log

* Jun 25, 2019 19:20 Tue ET
  * First draft


[archlinux]:https://www.archlinux.org "A lightweight and flexible Linux� distribution"
[installation_guide]:https://wiki.archlinux.org/title/Installation_guide "Guide through the process of installing Arch Linux."


<!-- End -->
