# Desktop Environment Package Groups


[General recommendations](https://wiki.archlinux.org/title/General_recommendations#Desktop_environments) list a lot of [desktop environments](https://wiki.archlinux.org/title/Desktop_environment), such as GNOME, KDE, LXDE, Xface and so on.

## TOC

1. [Desktop Environments List](#desktop-environments-list)  
2. [Package Groups Overview](#package-groups-overview)  
2.1 [Package Groups Overview](#package-groups-overview-1)  
3. [Budgie](#budgie)  
4. [GNOME](#gnome)  
4.1 [gnome](#gnome)  
4.2 [gnome-extra](#gnome-extra)  
5. [Cinnamon](#cinnamon)  
6. [Deepin](#deepin)  
6.1 [deepin](#deepin)  
6.2 [deepin-extra](#deepin-extra)  
7. [Enlightenment](#enlightenment)  
8. [KDE Plasma](#kde-plasma)  
8.1 [plasma](#plasma)  
8.2 [kde-applications](#kde-applications)  
9. [LXDE](#lxde)  
9.1 [lxde](#lxde)  
9.2 [lxde-gtk3](#lxde-gtk3)  
10. [MATE](#mate)  
10.1 [mate](#mate)  
10.2 [mate-extra](#mate-extra)  
11. [Sugar](#sugar)  
11.1 [sugar-fructose](#sugar-fructose)  
12. [Xface](#xface)  
12.1 [xfce4](#xfce4)  
12.2 [xfce4-goodies](#xfce4-goodies)  
13. [Change Log](#change-log)


## Desktop Environments List

[Officially supported](https://wiki.archlinux.org/title/Desktop_environment#Officially_supported) desktop envrionments list

Name | Arch Wiki | Display Manager | Package
---|---|---|---
[Budgie][budgie] | [Wiki](https://wiki.archlinux.org/title/Budgie) | [GDM][gdm] | budgie-desktop 53<br>budgie-extras 101<br>gnome 67 (416) optional
[GNOME][gnome] | [Wiki](https://wiki.archlinux.org/title/GNOME) | [GDM][gdm] |gnome 67 (416)<br>gnome-extra 35 (259)
[Cinnamon][cinnamon] | [Wiki](https://wiki.archlinux.org/title/Cinnamon) | | cinnamon 94
[Deepin][deepin] | [Wiki](https://wiki.archlinux.org/title/Deepin_Desktop_Environment) | [LightDM][lightdm] | deepin 27 (205)<br>deepin-extra 14 (235)
[Enlightenment][enlightenment] | [Wiki](https://wiki.archlinux.org/title/Enlightenment) |*| enlightenment 40<br>terminology 22
[KDE Plasma][plasma] | [Wiki](https://wiki.archlinux.org/title/KDE) | [SDDM][sddm] | plasma 44 (253)<br>plasma-wayland-session<br>kde-applications 166 (optional)
[LXDE][lxde] | [Wiki](https://wiki.archlinux.org/title/LXDE) | [LXDM][lxdm] | lxde 17 (27)<br>lxde-gtk3 17 (24) experimental
[MATE][mate] | [Wiki](https://wiki.archlinux.org/title/MATE) |macro| mate 14 (64)<br>mate-extra 23 (129)
[Sugar][sugar] | [Wiki](https://wiki.archlinux.org/title/Sugar) | * | sugar-fructose 13 (150)
[Xfce][xfce] | [Wiki](https://wiki.archlinux.org/title/Xfce) |[Xfwm][xfwm]|xfce4 15<br>xfce4-goodies 38 (80)


## Package Groups Overview

[Package group](https://wiki.archlinux.org/title/Package_group) says:

>A **package group** is a set of related packages, defined by the packager, which can be installed or uninstalled simultaneously by using the group name as a substitute for each individual package name. While a group is not a package, it can be installed in a similar fashion to a package, see [Pacman#Installing package groups](https://wiki.archlinux.org/title/Pacman#Installing_package_groups) and [PKGBUILD#groups](https://wiki.archlinux.org/title/PKGBUILD#groups).

[Difference to a meta package](https://wiki.archlinux.org/title/Package_group#Difference_to_a_meta_package)

>A meta package, often (though not always) titled with the "-meta" suffix, provides similar functionality to a package group in that it enables multiple related packages to be installed or uninstalled simultaneously. Meta packages can be installed just like any other package. The only difference between a meta package and a regular package is that a meta package is empty and exists purely to link related packages together via dependencies.
>
>The *advantage* of a meta package, compared to a group, is that any new member packages will be installed when the meta package itself is updated with a new set of dependencies. This is in contrast to a group where new group members will not be automatically installed.
>
>The *disadvantage* of a meta package is that it is not as flexible as a group; you can choose which group members you wish to install but you cannot choose which meta package dependencies you wish to install. Likewise, you can uninstall group members without having to remove the entire group. However, you cannot remove meta package dependencies without having to uninstall the meta package itself.


Official document [Installing package groups](https://wiki.archlinux.org/title/pacman#Installing_package_groups) says using command `pacman -Sg PACKAGE` to see packages belong to specific group.

### Package Groups Overview

[Package Groups Overview](https://www.archlinux.org/groups/)

Extracting package groups info

```bash
download_method='wget -qO-'
[[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && download_method='curl -fsSL'

# list name
${download_method} https://www.archlinux.org/groups/ | sed -r -n '/<tbody>/,/<\/tbody>/{/<tr[^>]*>/,/<\/tr>/{s@^[[:space:]]*@@g;p}}' | sed ':a;N;$!ba;s@\n@@g;' | sed -r -n 's@<\/tr>@\n@g;s@href="([^"]+)"@>\1<\/td><@g;s@<\/td>@|@g;s@<[^>]+>@@g;p'  | sed '/^$/d' |cut -d\| -f3 | sed ':a;N;$!ba;s@\n@ @g'

# list markdown output
${download_method} https://www.archlinux.org/groups/ | sed -r -n '/<tbody>/,/<\/tbody>/{/<tr[^>]*>/,/<\/tr>/{s@^[[:space:]]*@@g;p}}' | sed ':a;N;$!ba;s@\n@@g;' | sed -r -n 's@<\/tr>@\n@g;s@href="([^"]+)"@>\1<\/td><@g;s@<\/td>@|@g;s@<[^>]+>@@g;p' | awk -F\| -v official_site='https://www.archlinux.org' 'BEGIN{OFS="|";print "Arch|Group Name|Package Count|Last Updated\n---|---|---|---"}{if ($0!="") {$2=official_site$2; printf("%s|[%s](%s)|%s|%s\n",$1,$3,$2,$4,$5)}}'
```


```txt
alsa arduino base-devel coin-or coq deepin deepin-extra dlang dlang-dmd dlang-ldc dssi-plugins fcitx-im fcitx5-im feeluown-full firefox-addons fprint gambas3 gnome gnome-extra gnustep-core greenbone-vulnerability-manager i3 ipa-fonts jami kde-accessibility kde-applications kde-education kde-games kde-graphics kde-multimedia kde-network kde-pim kde-system kde-utilities kdepim kdesdk kf5 kf5-aids kodi-addons kodi-addons-audioencoder kodi-addons-game kodi-addons-inputstream kodi-addons-peripheral kodi-addons-screensaver kodi-addons-visualization ladspa-plugins libretro linux-tools liri lv2-plugins lxde lxde-gtk3 lxqt mate mate-extra mingw-w64 mingw-w64-toolchain multilib-devel non-daw pantheon pantheon-unstable pd-externals plasma pro-audio pyqt5 qt qt5 qt6 qtcurve realtime risc-v soundfonts sugar-fructose telepathy telepathy-kde tesseract-data texlive-lang texlive-most ukui vamp-plugins vim-plugins vst-plugins vst3-plugins vulkan-devel x-apps xfce4 xfce4-goodies xorg xorg-apps xorg-drivers xorg-fonts
```

Arch|Group Name|Package Count|Last Updated
---|---|---|---
x86_64|[alsa](https://www.archlinux.org/groups/x86_64/alsa/)|2|2020-11-12
x86_64|[arduino](https://www.archlinux.org/groups/x86_64/arduino/)|1|2020-10-19
x86_64|[base-devel](https://www.archlinux.org/groups/x86_64/base-devel/)|26|2020-11-11
x86_64|[coin-or](https://www.archlinux.org/groups/x86_64/coin-or/)|7|2020-07-09
x86_64|[coq](https://www.archlinux.org/groups/x86_64/coq/)|3|2020-08-29
x86_64|[deepin](https://www.archlinux.org/groups/x86_64/deepin/)|31|2020-11-22
x86_64|[deepin-extra](https://www.archlinux.org/groups/x86_64/deepin-extra/)|18|2020-11-22
x86_64|[dlang](https://www.archlinux.org/groups/x86_64/dlang/)|10|2020-11-11
x86_64|[dlang-dmd](https://www.archlinux.org/groups/x86_64/dlang-dmd/)|3|2020-11-11
x86_64|[dlang-ldc](https://www.archlinux.org/groups/x86_64/dlang-ldc/)|2|2020-11-11
x86_64|[dssi-plugins](https://www.archlinux.org/groups/x86_64/dssi-plugins/)|3|2020-11-20
x86_64|[fcitx-im](https://www.archlinux.org/groups/x86_64/fcitx-im/)|2|2020-11-22
x86_64|[fcitx5-im](https://www.archlinux.org/groups/x86_64/fcitx5-im/)|4|2020-11-03
x86_64|[feeluown-full](https://www.archlinux.org/groups/x86_64/feeluown-full/)|12|2020-11-15
x86_64|[firefox-addons](https://www.archlinux.org/groups/x86_64/firefox-addons/)|13|2020-11-20
x86_64|[fprint](https://www.archlinux.org/groups/x86_64/fprint/)|2|2020-09-14
x86_64|[gambas3](https://www.archlinux.org/groups/x86_64/gambas3/)|88|2020-10-14
x86_64|[gnome](https://www.archlinux.org/groups/x86_64/gnome/)|72|2020-11-22
x86_64|[gnome-extra](https://www.archlinux.org/groups/x86_64/gnome-extra/)|40|2020-11-22
x86_64|[gnustep-core](https://www.archlinux.org/groups/x86_64/gnustep-core/)|2|2020-05-02
x86_64|[greenbone-vulnerability-manager](https://www.archlinux.org/groups/x86_64/greenbone-vulnerability-manager/)|2|2020-08-09
x86_64|[i3](https://www.archlinux.org/groups/x86_64/i3/)|5|2020-11-15
x86_64|[ipa-fonts](https://www.archlinux.org/groups/x86_64/ipa-fonts/)|3|2020-06-29
x86_64|[jami](https://www.archlinux.org/groups/x86_64/jami/)|3|2020-10-24
x86_64|[kde-accessibility](https://www.archlinux.org/groups/x86_64/kde-accessibility/)|6|2020-11-14
x86_64|[kde-applications](https://www.archlinux.org/groups/x86_64/kde-applications/)|338|2020-11-16
x86_64|[kde-education](https://www.archlinux.org/groups/x86_64/kde-education/)|44|2020-11-14
x86_64|[kde-games](https://www.archlinux.org/groups/x86_64/kde-games/)|81|2020-11-14
x86_64|[kde-graphics](https://www.archlinux.org/groups/x86_64/kde-graphics/)|24|2020-11-16
x86_64|[kde-multimedia](https://www.archlinux.org/groups/x86_64/kde-multimedia/)|20|2020-11-14
x86_64|[kde-network](https://www.archlinux.org/groups/x86_64/kde-network/)|48|2020-11-14
x86_64|[kde-pim](https://www.archlinux.org/groups/x86_64/kde-pim/)|1|2020-11-14
x86_64|[kde-system](https://www.archlinux.org/groups/x86_64/kde-system/)|9|2020-11-14
x86_64|[kde-utilities](https://www.archlinux.org/groups/x86_64/kde-utilities/)|43|2020-11-14
x86_64|[kdepim](https://www.archlinux.org/groups/x86_64/kdepim/)|34|2020-11-14
x86_64|[kdesdk](https://www.archlinux.org/groups/x86_64/kdesdk/)|26|2020-11-14
x86_64|[kf5](https://www.archlinux.org/groups/x86_64/kf5/)|88|2020-11-15
x86_64|[kf5-aids](https://www.archlinux.org/groups/x86_64/kf5-aids/)|9|2020-11-15
x86_64|[kodi-addons](https://www.archlinux.org/groups/x86_64/kodi-addons/)|31|2020-11-10
x86_64|[kodi-addons-audioencoder](https://www.archlinux.org/groups/x86_64/kodi-addons-audioencoder/)|4|2020-10-25
x86_64|[kodi-addons-game](https://www.archlinux.org/groups/x86_64/kodi-addons-game/)|13|2020-11-10
x86_64|[kodi-addons-inputstream](https://www.archlinux.org/groups/x86_64/kodi-addons-inputstream/)|2|2020-10-25
x86_64|[kodi-addons-peripheral](https://www.archlinux.org/groups/x86_64/kodi-addons-peripheral/)|1|2020-10-25
x86_64|[kodi-addons-screensaver](https://www.archlinux.org/groups/x86_64/kodi-addons-screensaver/)|7|2020-10-25
x86_64|[kodi-addons-visualization](https://www.archlinux.org/groups/x86_64/kodi-addons-visualization/)|4|2020-10-25
x86_64|[ladspa-plugins](https://www.archlinux.org/groups/x86_64/ladspa-plugins/)|17|2020-09-16
x86_64|[libretro](https://www.archlinux.org/groups/x86_64/libretro/)|39|2020-11-11
x86_64|[linux-tools](https://www.archlinux.org/groups/x86_64/linux-tools/)|20|2020-11-16
x86_64|[liri](https://www.archlinux.org/groups/x86_64/liri/)|3|2020-03-11
x86_64|[lv2-plugins](https://www.archlinux.org/groups/x86_64/lv2-plugins/)|58|2020-11-20
x86_64|[lxde](https://www.archlinux.org/groups/x86_64/lxde/)|17|2020-10-27
x86_64|[lxde-gtk3](https://www.archlinux.org/groups/x86_64/lxde-gtk3/)|17|2020-10-27
x86_64|[lxqt](https://www.archlinux.org/groups/x86_64/lxqt/)|21|2020-11-14
x86_64|[mate](https://www.archlinux.org/groups/x86_64/mate/)|14|2020-08-21
x86_64|[mate-extra](https://www.archlinux.org/groups/x86_64/mate-extra/)|25|2020-11-12
x86_64|[mingw-w64](https://www.archlinux.org/groups/x86_64/mingw-w64/)|5|2020-10-21
x86_64|[mingw-w64-toolchain](https://www.archlinux.org/groups/x86_64/mingw-w64-toolchain/)|5|2020-10-21
x86_64|[multilib-devel](https://www.archlinux.org/groups/x86_64/multilib-devel/)|4|2020-11-09
x86_64|[non-daw](https://www.archlinux.org/groups/x86_64/non-daw/)|2|2020-07-14
x86_64|[pantheon](https://www.archlinux.org/groups/x86_64/pantheon/)|52|2020-10-12
x86_64|[pantheon-unstable](https://www.archlinux.org/groups/x86_64/pantheon-unstable/)|2|2020-11-17
x86_64|[pd-externals](https://www.archlinux.org/groups/x86_64/pd-externals/)|3|2020-11-07
x86_64|[plasma](https://www.archlinux.org/groups/x86_64/plasma/)|44|2020-11-22
x86_64|[pro-audio](https://www.archlinux.org/groups/x86_64/pro-audio/)|181|2020-11-22
x86_64|[pyqt5](https://www.archlinux.org/groups/x86_64/pyqt5/)|12|2020-11-12
x86_64|[qt](https://www.archlinux.org/groups/x86_64/qt/)|41|2020-11-22
x86_64|[qt5](https://www.archlinux.org/groups/x86_64/qt5/)|41|2020-11-22
x86_64|[qt6](https://www.archlinux.org/groups/x86_64/qt6/)|13|2020-11-20
x86_64|[qtcurve](https://www.archlinux.org/groups/x86_64/qtcurve/)|3|2020-02-11
x86_64|[realtime](https://www.archlinux.org/groups/x86_64/realtime/)|7|2020-11-12
x86_64|[risc-v](https://www.archlinux.org/groups/x86_64/risc-v/)|5|2020-10-15
x86_64|[soundfonts](https://www.archlinux.org/groups/x86_64/soundfonts/)|1|2020-08-16
x86_64|[sugar-fructose](https://www.archlinux.org/groups/x86_64/sugar-fructose/)|13|2020-10-27
x86_64|[telepathy](https://www.archlinux.org/groups/x86_64/telepathy/)|4|2020-11-11
x86_64|[telepathy-kde](https://www.archlinux.org/groups/x86_64/telepathy-kde/)|24|2020-11-14
x86_64|[tesseract-data](https://www.archlinux.org/groups/x86_64/tesseract-data/)|106|2020-07-01
x86_64|[texlive-lang](https://www.archlinux.org/groups/x86_64/texlive-lang/)|6|2020-06-17
x86_64|[texlive-most](https://www.archlinux.org/groups/x86_64/texlive-most/)|12|2020-06-17
x86_64|[ukui](https://www.archlinux.org/groups/x86_64/ukui/)|22|2020-11-22
x86_64|[vamp-plugins](https://www.archlinux.org/groups/x86_64/vamp-plugins/)|1|2020-01-06
x86_64|[vim-plugins](https://www.archlinux.org/groups/x86_64/vim-plugins/)|33|2020-11-20
x86_64|[vst-plugins](https://www.archlinux.org/groups/x86_64/vst-plugins/)|16|2020-11-20
x86_64|[vst3-plugins](https://www.archlinux.org/groups/x86_64/vst3-plugins/)|2|2020-10-25
x86_64|[vulkan-devel](https://www.archlinux.org/groups/x86_64/vulkan-devel/)|7|2020-11-13
x86_64|[x-apps](https://www.archlinux.org/groups/x86_64/x-apps/)|2|2020-06-28
x86_64|[xfce4](https://www.archlinux.org/groups/x86_64/xfce4/)|15|2020-11-09
x86_64|[xfce4-goodies](https://www.archlinux.org/groups/x86_64/xfce4-goodies/)|38|2020-11-10
x86_64|[xorg](https://www.archlinux.org/groups/x86_64/xorg/)|48|2020-11-20
x86_64|[xorg-apps](https://www.archlinux.org/groups/x86_64/xorg-apps/)|35|2020-11-20
x86_64|[xorg-drivers](https://www.archlinux.org/groups/x86_64/xorg-drivers/)|16|2020-11-17
x86_64|[xorg-fonts](https://www.archlinux.org/groups/x86_64/xorg-fonts/)|2|2020-05-19


## Budgie

Budgie is the default desktop of Solus Operating System, written from scratch.

Install the `budgie-desktop` package for the latest stable or budgie-desktop-git (AUR) for current git master. It's recommended to install its optional dependencies also to get a more complete desktop environment. It's recommended also to install the `gnome` group, which contains applications required for the standard GNOME experience.

Installing command

```bash
pacman -S budgie-desktop budgie-extras
# pacman -S gnome
```


## GNOME

GNOME is a desktop environment that aims to be simple and easy to use.

Two groups are available:

* `gnome` contains the base GNOME desktop and a subset of well-integrated applications;
* `gnome-extra` contains further GNOME applications, including an archive manager, disk manager, text editor, and a set of games. Note that this group builds on the gnome group.

The base desktop consists of GNOME Shell, a plugin for the Mutter window manager. It can be installed separately with `gnome-shell`.

Installing command

```bash
pacman -S gnome gnome-extra
```

### gnome

```bash
pacman -Sg gnome

# pacman -Sg gnome | cut -d' ' -f2 | sed ':a;N;$!ba;s@\n@ @g'
```

Package nums 66

```bash
baobab cheese eog epiphany evince file-roller gdm gedit gnome-backgrounds gnome-books gnome-calculator gnome-calendar gnome-characters gnome-clocks gnome-color-manager gnome-contacts gnome-control-center gnome-disk-utility gnome-documents gnome-font-viewer gnome-getting-started-docs gnome-keyring gnome-logs gnome-maps gnome-menus gnome-music gnome-photos gnome-remote-desktop gnome-screenshot gnome-session gnome-settings-daemon gnome-shell gnome-shell-extensions gnome-software gnome-system-monitor gnome-terminal gnome-themes-extra gnome-user-docs gnome-user-share gnome-video-effects gnome-weather grilo-plugins gvfs gvfs-afc gvfs-goa gvfs-google gvfs-gphoto2 gvfs-mtp gvfs-nfs gvfs-smb mutter nautilus networkmanager orca rygel sushi totem tracker tracker-miners tracker3 tracker3-miners vino xdg-user-dirs-gtk yelp gnome-boxes simple-scan
```

### gnome-extra

```bash
pacman -Sg gnome-extra

# pacman -Sg gnome-extra | cut -d' ' -f2 | sed ':a;N;$!ba;s@\n@ @g'
```

Package nums 35

```txt
accerciser dconf-editor devhelp evolution five-or-more four-in-a-row ghex glade gnome-builder gnome-chess gnome-devel-docs gnome-klotski gnome-mahjongg gnome-mines gnome-nettool gnome-nibbles gnome-robots gnome-sound-recorder gnome-sudoku gnome-taquin gnome-tetravex gnome-todo gnome-tweaks gnome-usage hitori iagno lightsoff polari quadrapassel swell-foop sysprof tali gnome-code-assistance gnome-multi-writer gnome-recipes
```


## Cinnamon

Cinnamon is a desktop environment which combines a traditional desktop layout with modern graphical effects.

Cinnamon can be installed with the package `cinnamon`.

Installing command

```bash
pacman -S cinnamon
```


## Deepin

The Deepin Desktop Environment (DDE) is the desktop environment of the Chinese Deepin Linux distribution.

To get a minimal desktop interface, you may start by installing `deepin` group. This will pull all the basic components.

The `deepin-extra` groups contains some extra applications for a more complete desktop environment.

To be able to use the integrated network administration, the `networkmanager` package is required, and the *NetworkManager.service* must be started and enabled.

Installing command

```bash
pacman -S deepin deepin-extra networkmanager
```

### deepin

```bash
pacman -Sg deepin # need to intsall 109 packages

# pacman -Sg deepin | cut -d' ' -f2 | sed ':a;N;$!ba;s@\n@ @g'
```

Package nums 31

```txt
deepin-account-faces deepin-anything deepin-api deepin-calendar deepin-control-center deepin-daemon deepin-desktop-base deepin-desktop-schemas deepin-dock deepin-file-manager deepin-gtk-theme deepin-icon-theme deepin-image-viewer deepin-launcher deepin-menu deepin-metacity deepin-network-utils deepin-polkit-agent deepin-polkit-agent-ext-gnomekeyring deepin-qt5integration deepin-qt5platform-plugins deepin-screensaver deepin-session-shell deepin-session-ui deepin-shortcut-viewer deepin-sound-theme deepin-system-monitor deepin-turbo deepin-wallpapers deepin-wm startdde
```


### deepin-extra

```bash
pacman -Sg deepin-extra # need to intsall 91 packages

# pacman -Sg deepin-extra | cut -d' ' -f2 | sed ':a;N;$!ba;s@\n@ @g'
```

Package nums 18

```txt
deepin-album deepin-boot-maker deepin-calculator deepin-clipboard deepin-clone deepin-community-wallpapers deepin-compressor deepin-draw deepin-editor deepin-movie deepin-music deepin-picker deepin-printer deepin-reader deepin-screen-recorder deepin-screensaver-pp deepin-terminal deepin-voice-note
```


## Enlightenment

This comprises both the Enlightenment window manager and Enlightenment Foundation Libraries (EFL), which provide additional desktop environment features such as a toolkit, object canvas, and abstracted objects.

Install the `enlightenment` package.

You might also want to install `terminology`, which is an EFL-based terminal emulator that integrates well with Enlightenment.

Installing command

```bash
pacman -S enlightenment terminology
```


## KDE Plasma

KDE is a software project currently comprising a desktop environment known as Plasma, a collection of libraries and frameworks (KDE Frameworks) and several applications (KDE Applications) as well.

Before installing Plasma, make sure you have a working [Xorg](https://wiki.archlinux.org/title/Xorg) installation on your system.

Install the `plasma-meta` meta-package or the `plasma` group. For differences between `plasma-meta` and `plasma` reference Package group. Alternatively, for a more minimal Plasma installation, install the `plasma-desktop` package.

To enable support for Wayland in Plasma, also install the `plasma-wayland-session` package.

To install the full set of KDE Applications, install the `kde-applications` group or the `kde-applications-meta` meta-package. Note that this will only install applications, it will not install any version of Plasma.


Installing command

```bash
# group
pacman -S plasma # need to install 176 packages
# pacman -S plasma-meta # need to install 177 packages
# pacman -S plasma-desktop # minimal need to install 123 packages

pacman -S plasma-wayland-session

pacman -S kde-applications # need to install 437 packages
# pacman -S kde-applications-meta
```


### plasma

```bash
pacman -Sg plasma

# pacman -Sg plasma | cut -d' ' -f2 | sed ':a;N;$!ba;s@\n@ @g'
```

Package nums 44

```txt
bluedevil breeze breeze-gtk discover drkonqi kactivitymanagerd kde-cli-tools kde-gtk-config kdecoration kdeplasma-addons kgamma5 khotkeys kinfocenter kmenuedit knetattach kscreen kscreenlocker ksshaskpass ksysguard kwallet-pam kwayland-integration kwayland-server kwin kwrited libkscreen libksysguard milou oxygen plasma-browser-integration plasma-desktop plasma-disks plasma-integration plasma-nm plasma-pa plasma-sdk plasma-thunderbolt plasma-vault plasma-workspace plasma-workspace-wallpapers polkit-kde-agent powerdevil sddm-kcm systemsettings xdg-desktop-portal-kde
```


### kde-applications

```bash
pacman -Sg kde-applications

# pacman -Sg kde-applications | cut -d' ' -f2 | sed ':a;N;$!ba;s@\n@ @g'
```

Package nums 165

```txt
akonadi-calendar-tools akonadi-import-wizard akonadiconsole akregator ark artikulate audiocd-kio blinken bomber bovo cantor cervisia dolphin dolphin-plugins dragon elisa ffmpegthumbs filelight granatier grantlee-editor gwenview juk k3b kaddressbook kajongg kalarm kalgebra kalzium kamera kamoso kanagram kapman kapptemplate kate katomic kbackup kblackbox kblocks kbounce kbreakout kbruch kcachegrind kcalc kcharselect kcolorchooser kcron kde-dev-scripts kde-dev-utils kdebugsettings kdeconnect kdegraphics-mobipocket kdegraphics-thumbnailers kdenetwork-filesharing kdenlive kdepim-addons kdesdk-kioslaves kdesdk-thumbnailers kdf kdialog kdiamond keditbookmarks kfind kfloppy kfourinline kgeography kget kgoldrunner kgpg khangman khelpcenter kig kigo killbots kimagemapeditor kio-extras kio-gdrive kipi-plugins kirigami-gallery kiriki kiten kjumpingcube kleopatra klettres klickety klines kmag kmahjongg kmail kmail-account-wizard kmines kmix kmousetool kmouth kmplot knavalbattle knetwalk knights knotes kolf kollision kolourpaint kompare konqueror konquest konsole kontact kopete korganizer kpatience krdc kreversi krfb kross-interpreters kruler kshisen ksirk ksnakeduel kspaceduel ksquares ksudoku ksystemlog kteatime ktimer ktouch ktuberling kturtle kubrick kwalletmanager kwave kwordquiz kwrite lokalize lskat marble mbox-importer minuet okular palapeli parley picmi pim-data-exporter pim-sieve-editor poxml print-manager rocs signon-kwallet-extension spectacle step svgpart sweeper telepathy-kde-accounts-kcm telepathy-kde-approver telepathy-kde-auth-handler telepathy-kde-call-ui telepathy-kde-common-internals telepathy-kde-contact-list telepathy-kde-contact-runner telepathy-kde-desktop-applets telepathy-kde-filetransfer-handler telepathy-kde-integration-module telepathy-kde-send-file telepathy-kde-text-ui umbrello yakuake zeroconf-ioslave
```


## LXDE

The "Lightweight X11 Desktop Environment" is an extremely fast-performing and energy-saving desktop environment.

LXDE requires at least `lxde-common`, `lxsession` and `openbox` (or another window manager) to be installed. The `lxde` group contains the full desktop.

An experimental GTK+ 3 build of LXDE can be installed with the `lxde-gtk3` group.

Installing command

```bash
pacman -S lxde
# pacman -S lxde-common lxsession openbox #minimal

pacman -S lxde-gtk3
```

### lxde

```bash
pacman -Sg lxde

# pacman -Sg lxde | cut -d' ' -f2 | sed ':a;N;$!ba;s@\n@ @g'
```

Package nums 17

```txt
gpicview lxappearance lxappearance-obconf lxde-common lxde-icon-theme lxdm lxhotkey lxinput lxlauncher lxmusic lxpanel lxrandr lxsession lxtask lxterminal openbox pcmanfm
```

### lxde-gtk3

```bash
pacman -Sg lxde-gtk3

# pacman -Sg lxde-gtk3 | cut -d' ' -f2 | sed ':a;N;$!ba;s@\n@ @g'
```

Package nums 17


```txt
gpicview-gtk3 lxappearance-gtk3 lxappearance-obconf-gtk3 lxde-common lxde-icon-theme lxdm-gtk3 lxhotkey-gtk3 lxinput-gtk3 lxlauncher-gtk3 lxmusic-gtk3 lxpanel-gtk3 lxrandr-gtk3 lxsession-gtk3 lxtask-gtk3 lxterminal openbox pcmanfm-gtk3
```


## MATE

The MATE Desktop Environment is the continuation of GNOME 2. It provides an intuitive and attractive desktop environment using traditional metaphors for Linux and other Unix-like operating systems.

MATE is available in the official repositories and can be installed with one of the following:

* The `mate` group contains the core desktop environment required for the standard MATE experience.
* The `mate-extra` group contains additional utilities and applications that integrate well with the MATE desktop. Installing just the mate-extra group will not pull in the whole mate group via dependencies. If you want to install all MATE packages then you will need to explicitly install both groups.

The base desktop consists of `marco`, `mate-panel` and `mate-session-manager`.


Installing command

```bash
pacman -S mate mate-extra
```

### mate

```bash
pacman -Sg mate

# pacman -Sg mate | cut -d' ' -f2 | sed ':a;N;$!ba;s@\n@ @g'
```

Package nums 14

```txt
caja marco mate-backgrounds mate-control-center mate-desktop mate-icon-theme mate-menus mate-notification-daemon mate-panel mate-polkit mate-session-manager mate-settings-daemon mate-themes mate-user-guide
```

### mate-extra

```bash
pacman -Sg mate-extra

# pacman -Sg mate-extra | cut -d' ' -f2 | sed ':a;N;$!ba;s@\n@ @g'
```

Package nums 23

```txt
atril caja-image-converter caja-open-terminal caja-sendto caja-share caja-wallpaper caja-xattr-tags engrampa eom mate-applets mate-calc mate-icon-theme-faenza mate-media mate-netbook mate-power-manager mate-screensaver mate-sensors-applet mate-system-monitor mate-terminal mate-user-share mate-utils mozo pluma
```


## Sugar

For the core system (Glucose), install `sugar`. It provides the graphical interface and a desktop session, but not very useful on its own.

The `sugar-fructose` group contains the base activities (Fructose) including a web browser, a text editor, a media player and a terminal emulator.

The `sugar-runner` package provides a helper script that makes it possible to launch Sugar within another desktop environment, or from the command line directly.

Installing command

```bash
pacman -S sugar sugar-fructose sugar-runner
```

### sugar-fructose

```bash
pacman -Sg sugar-fructose

# pacman -Sg sugar-fructose | cut -d' ' -f2 | sed ':a;N;$!ba;s@\n@ @g'
```

Package nums 13

```txt
sugar-activity-browse sugar-activity-calculate sugar-activity-chat sugar-activity-clock sugar-activity-imageviewer sugar-activity-jukebox sugar-activity-log sugar-activity-paint sugar-activity-pippy sugar-activity-read sugar-activity-terminal sugar-activity-turtleart sugar-activity-write
```


## Xface

Install the `xfce4` group. You may also wish to install the `xfce4-goodies` group which includes extra plugins and a number of useful utilities such as the mousepad editor. Xfce uses the Xfwm window manager by default.

Installing command

```bash
pacman -S xfce4 xfce4-goodies
```

### xfce4

```bash
pacman -Sg xfce4

# pacman -Sg xfce4 | cut -d' ' -f2 | sed ':a;N;$!ba;s@\n@ @g'
```

Package nums 15

```txt
exo garcon thunar thunar-volman tumbler xfce4-appfinder xfce4-panel xfce4-power-manager xfce4-session xfce4-settings xfce4-terminal xfconf xfdesktop xfwm4 xfwm4-themes
```


Pckages optional dependencies

```bash
Optional dependencies for thunar
    gvfs: for trash support, mounting with udisk and remote filesystems [installed]
    xfce4-panel: for trash applet [pending]
    tumbler: for thumbnail previews [pending]
    thunar-volman: manages removable devices [pending]
    thunar-archive-plugin: create and deflate archives
    thunar-media-tags-plugin: view/edit id3/ogg tags

Optional dependencies for tumbler
    ffmpegthumbnailer: for video thumbnails
    poppler-glib: for PDF thumbnails
    libgsf: for ODF thumbnails
    libopenraw: for RAW thumbnails
    freetype2: for font thumbnails

Optional dependencies for xfce4-session
    gnome-keyring: for keyring support when GNOME compatibility is enabled
    xfce4-screensaver: for locking screen with xflock4
    xscreensaver: for locking screen with xflock4
    gnome-screensaver: for locking screen with xflock4
    xlockmore: for locking screen with xflock4
    slock: for locking screen with xflock4
```

### xfce4-goodies

```bash
pacman -Sg xfce4-goodies

# pacman -Sg xfce4-goodies | cut -d' ' -f2 | sed ':a;N;$!ba;s@\n@ @g'
```

Package nums 38

```txt
mousepad orage ristretto thunar-archive-plugin thunar-media-tags-plugin xfburn xfce4-artwork xfce4-battery-plugin xfce4-clipman-plugin xfce4-cpufreq-plugin xfce4-cpugraph-plugin xfce4-datetime-plugin xfce4-dict xfce4-diskperf-plugin xfce4-eyes-plugin xfce4-fsguard-plugin xfce4-genmon-plugin xfce4-mailwatch-plugin xfce4-mount-plugin xfce4-mpc-plugin xfce4-netload-plugin xfce4-notes-plugin xfce4-notifyd xfce4-pulseaudio-plugin xfce4-screensaver xfce4-screenshooter xfce4-sensors-plugin xfce4-smartbookmark-plugin xfce4-systemload-plugin xfce4-taskmanager xfce4-time-out-plugin xfce4-timer-plugin xfce4-verve-plugin xfce4-wavelan-plugin xfce4-weather-plugin xfce4-xkb-plugin parole xfce4-whiskermenu-plugin
```


## Change Log

* Jun 15, 2019 15:00 Sat ET
  * first draft
* May 15, 2020 00:03 Thu ET
  * Update group packages info
* Nov 22, 2020 09:37 Sun ET
  * Update group packages info


[budgie]:https://getsol.us/home/
[gnome]:https://www.gnome.org/gnome-3/
[cinnamon]:https://projects.linuxmint.com/cinnamon/
[deepin]:https://www.deepin.org/en/
[enlightenment]:https://www.enlightenment.org
[plasma]:https://kde.org/plasma-desktop
[lxde]:https://lxde.org
[mate]:https://mate-desktop.org
[sugar]:https://sugarlabs.org
[xfce]:https://xfce.org

[gdm]:https://wiki.archlinux.org/title/GDM
[lightdm]:https://wiki.archlinux.org/title/LightDM
[sddm]:https://wiki.archlinux.org/title/SDDM
[lxdm]:https://wiki.archlinux.org/title/LXDM
[xfwm]:https://wiki.archlinux.org/title/Xfwm

<!-- End -->