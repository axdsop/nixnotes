# Optimization

This note is based official document [General recommendations][general_recommendations]. It is an annotated index of popular articles and important information for improving and adding functionalities to the installed Arch system.

Official page [Optimization](https://wiki.archlinux.org/title/General_recommendations#Optimization)


## Benchmarking

[Benchmarking](https://wiki.archlinux.org/title/Benchmarking) is the act of measuring performance and comparing the results to another system's results or a widely accepted standard through a unified procedure.

## Improving performance

The [Improving performance](https://wiki.archlinux.org/title/Improving_performance) article gathers information and is a basic rundown about gaining performance in Arch Linux.


## Solid state drives

The [Solid State Drives](https://wiki.archlinux.org/title/Solid_State_Drives) article covers many aspects of solid state drives, including configuring them to maximize their lifetimes.

### Periodic TRIM

Most SSDs support the ATA_TRIM command for sustained long-term performance and wear-leveling. As of Linux kernel version 3.8 onwards, support for TRIM was continually added for the different filesystems ([Table](https://wiki.archlinux.org/title/Solid_state_drive#TRIM)).

The *util-linux* package provides `fstrim.service` and `fstrim.timer` systemd unit files. Enabling the timer will activate the service weekly. The service executes fstrim(8) on all mounted filesystems on devices that support the discard operation.

```bash
systemctl enable fstrim.timer
systemctl start fstrim.timer
```


## Change Log

* Oct 31, 2019 16:25 Thu ET
  * Add TRIM for SSD


[general_recommendations]:https://wiki.archlinux.org/title/General_recommendations "Annotated index of post-installation tutorials and other popular articles."

<!-- End -->
