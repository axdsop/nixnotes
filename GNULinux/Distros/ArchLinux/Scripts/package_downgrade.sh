#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails

# Target: Downgrading package to previous specific version for Arch Linux.
# Change Log
# - Aug 04, 2021 Wed 08:52 ET - add existed version identifier in version selection menu
# - Aug 03, 2021 Tue 10:22 ET - fix version info contain ':' which transfer to '%3A' in url
# - Jul 29, 2021 Wed 09:38 ET - fix version list sort issue, add color output 
# - Jul 28, 2021 Wed 19:20~21:35 ET - first release


# Official Doc
# - https://wiki.archlinux.org/title/Downgrading_packages
# - https://wiki.archlinux.org/title/Arch_Linux_Archive#How_to_downgrade_one_package


#########  0-1. Variables Setting  #########
readonly arch_archive_page='https://archive.archlinux.org/packages'
check_version=${check_version:-0}
name_specify=${name_specify:-}
download_method=${download_method:-} # curl -fsL / wget -qO-
readonly select_num_limit=15


#########  0-2. getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | bash -s -- [options] ...

Downgrading package to previous specific version for Arch Linux.

Note: package release versions just show the latest ${select_num_limit} items.

[available option]
    -h    --help, show help info
    -n pack_name    --specify package name (Match pattern '^PATTERN.*')
    -c    --check the latest release version of specified package, use with '-n'
\e[0m"
}

while getopts "hcn:" option "$@"; do
    case "$option" in
        c ) check_version=1 ;;
        n ) name_specify="$OPTARG" ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done


#########  0-3 Custom Functions  #########
fn_CommandExistIfCheck(){
    # $? -- 0 is find, 1 is not find
    local l_name="${1:-}"
    local l_output=${l_output:-1}
    [[ -n "${l_name}" && -n $(which "${l_name}" 2> /dev/null || command -v "${l_name}" 2> /dev/null) ]] && l_output=0
    return "${l_output}"
}

fn_ExitStatement(){
    local l_str="$*"
    if [[ -n "${l_str}" ]]; then
        echo -e "${l_str}\n"
    fi
    exit 0
}


#########  1-1. Processing  #########
fn_InitializationCheck(){
    # - Distro info
    # /etc/arch-release  /   # cat /etc/os-release  'NAME="Arch Linux"'
    [[ -f /etc/os-release && $(sed -r -n '/^NAME=/{s@^[^=]+=@@g;s@"@@g;p}' /etc/os-release 2> /dev/null) == 'Arch Linux' ]] || fn_ExitStatement 'Sorry, this script just works for Arch Linux.'

    # - Variables check
    [[ -z "${name_specify}" ]] && fn_ExitStatement "Please specify package name via '-n' first."
    name_specify="${name_specify,,}"

    # - Download method
    if fn_CommandExistIfCheck 'curl'; then
        download_method='curl -fsL'
    elif fn_CommandExistIfCheck 'wget'; then
        download_method='wget -qO-'
    else
        fn_ExitStatement 'Sorry, this script needs package curl/wget'
    fi

    # - gawk
    fn_CommandExistIfCheck 'gawk' || fn_ExitStatement 'Sorry, this script needs package gawk'
}

fn_InitializationCheck

#########  1-1. Processing  #########
package_choose=${package_choose:-}
version_choose=${version_choose:-}
package_zst_link=${package_zst_link:-}

# - Packages selection menu
# https://archive.archlinux.org/packages/v/
alphabet_page_link="${arch_archive_page}/${name_specify:0:1}/"
matched_pack_list=${matched_pack_list:-}
matched_pack_list=$(${download_method} "${alphabet_page_link}" 2> /dev/null | sed -r -n '/href=/{s@.*href="([^"]+)".*$@\1@g;s@\/$@@g;/^'"${name_specify}"'/!d;p}')
[[ -z "${matched_pack_list}" ]] && fn_ExitStatement "Sorry, package '${name_specify}' not exists."

if [[ $(wc -l <<< "${matched_pack_list}") -eq 1 ]]; then
    package_choose="${matched_pack_list}"
    echo -e "Available package match '^${name_specify}' is '${package_choose}'.\n"
else
    IFS_BAK=${IFS_BAK:-"$IFS"}  # Backup IFS
    IFS='|' # Setting temporary IFS
    echo -e '\nAvailable Candiate Package List: '
    PS3='Choose package number(e.g. 1, 2,...): '

    select item in $(sed ':a;N;$!ba;s@\n@|@g' <<< "${matched_pack_list}"); do
        package_choose="${item}"
        [[ -n "${package_choose}" ]] && break
    done < /dev/tty

    IFS=${IFS_BAK}  # Restore IFS
    unset IFS_BAK
    unset PS3

    echo -e "\nPackage you choose is '${package_choose}'.\n"
fi

local_pack_info=${local_pack_info:-}
local_pack_version=${local_pack_version:-}
local_pack_info=$(pacman -Q --info "${package_choose}" 2> /dev/null)

if [[ -n "${local_pack_info}" ]]; then
    echo -e "Local existed package info:\n${local_pack_info}"
    local_pack_version=$(sed -r -n '/^version/I{s@^[^[:digit:]]+@@g;s@[[:space:]]*@@g;p;q}' <<< "${local_pack_info}")
fi

# - Versions selection menu
# https://archive.archlinux.org/packages/v/vim/
package_page_link="${alphabet_page_link%/}/${package_choose}/"
release_version_list=${release_version_list:-}
release_version_list=$(${download_method} "${package_page_link}" 2> /dev/null |  sed -r -n '/href=.*.zst</{s@.*href="([^"]+)".*?<\/a>[[:space:]]*([^[:space:]]+).*$@\1|\2@g;s@^[^[:digit:]]+(.*)-([^\.]+).pkg.*[^\|]+.*@&|\1@g;s@%3A@:@g;p}' | sed -r -n 's@^([^\|]+)\|([^\|]+)\|(.*)$@\3|\2|\1@g;p' | awk -F\| 'BEGIN{OFS="|"}{a=gensub(/-/,".","g",$1);print a,$0}' | sort -b -t"." -k 1,1rn -k 2,2rn -k 3,3rn -k 4,4r -k 5,5r | cut -d\| --output-delimiter="|" -f2,3,4)
# sed -r -n '/href=.*.zst</{s@.*href="([^"]+)".*?<\/a>[[:space:]]*([^[:space:]]+).*$@\1|\2@g;s@^[^[:digit:]]+([[:digit:].-]+)-[^\|]+.*@&|\1@g;p}' # not work if version contains letter

# version|release date|package name
# 8.2.2891-1|28-May-2021|vim-8.2.2891-1-x86_64.pkg.tar.zst
# 671.0e509fb-1|22-Mar-2020|vim-gitgutter-671.0e509fb-1-any.pkg.tar.zst

[[ -z "${release_version_list}" ]] && fn_ExitStatement "Sorry, fail to extract release version of package '${package_choose}'."

if [[ "${check_version}" -eq 1 ]]; then
    echo -e -n "Latest release version for '${package_choose}' is "
    sed -r -n '1{s@^([^\|]+)\|([^\|]+)\|.*$@\1 (\2)@g;p}' <<< "${release_version_list}"
    fn_ExitStatement ''
fi

if [[ $(wc -l <<< "${release_version_list}") -eq 1 ]]; then
    version_choose="${release_version_list%%\|*}"
    echo -e "Available version is '${version_choose}'.\n"
else
    IFS_BAK=${IFS_BAK:-"$IFS"}  # Backup IFS
    IFS='|' # Setting temporary IFS
    echo -e "\nAvailable Candiate Version List: (num limit ${select_num_limit}, 'L' stands for existed version)\n"
    PS3='Choose version number(e.g. 1, 2,...): '

    selection_list=${selection_list:-}
    selection_list=$(sed -r -n 's@^([^\|]+)\|([^\|]+).*@\1 (\2)@g;p' <<< "${release_version_list}" | head -n "${select_num_limit}")
    [[ -n "${local_pack_version}" ]] && selection_list=$(sed -r -n '/^'"${local_pack_version}"'[[:space:]]*/{s@[[:punct:]]+$@ L&@g};p' <<< "${selection_list}")

    select item in $(sed ':a;N;$!ba;s@\n@|@g' <<< "${selection_list}"); do
        version_choose="${item}"
        [[ -n "${version_choose}" ]] && break
    done < /dev/tty
    # 8.2.2891-1 (28-May-2021)|8.2.2885-1 (25-May-2021)|...|8.2.0100-1 (07-Jan-2020)

    IFS=${IFS_BAK}  # Restore IFS
    unset IFS_BAK
    unset PS3

    version_choose="${version_choose%% *}"  # 8.2.2891-1 (28-May-2021) --> 8.2.2891-1
    echo -e "\nPackage version choose is '${version_choose}'.\n"
fi

package_zst_link=$(sed -r -n '/^'"${version_choose}"'\|/{s@.*\|([^\|]+)$@'"${package_page_link}"'\1@g;p}' <<< "${release_version_list}")
# https://archive.archlinux.org/packages/v/vim/vim-8.2.0100-1-x86_64.pkg.tar.zst

echo -e "To ignore it while pacman upgrading pacakages, append it to vairable 'IgnorePkg' in file /etc/pacman.conf\n\nIf you wanna install package ${package_choose} (${version_choose}), please execute:\n\n  \e[31;1msudo pacman -U ${package_zst_link}\e[0m\n\n"  # \e[31;1m red
# sudo pacman -U https://archive.archlinux.org/packages/r/rclone/rclone-1.55.1-1-x86_64.pkg.tar.zst


# Script End