# Arch Linux Operation Notes

In order to install [Arch Linux][archlinux] on Huawei [MateBook X Pro][mbxp] (i7 MX150). I read official [Wiki][archlinux_wiki] multi times and take some learning notes. These notes show step-by-step installing [Arch Linux][archlinux] with UEFI mode on [MBXP][mbxp].

## TOC

1. [Official Documents](#official-documents)  
2. [Learning Notes](#learning-notes)  
2.1 [Installation Guide](#installation-guide)  
2.2 [General Recommendations](#general-recommendations)  
3. [Power And Performance](#power-and-performance)  
4. [Shell Script](#shell-script)  
4.1 [alis.sh](#alissh)  
4.2 [Others](#others)  
5. [Change Log](#change-log)  


## Official Documents

Official [Wiki][archlinux_wiki]: [*Installation guide*][installation_guide], [*General recommendations*][general_recommendations].

<!-- Replace nerd-fonts-source-code-pro with community/ttf-sourcecodepro-nerd? [Y/n] -->

## Learning Notes

### Installation Guide

1. [Prerequisites](./Notes/1-0_Prerequisites.md)
2. [Pre-installation](./Notes/1-1_Pre-installation.md)
3. [Partition](./Notes/1-2_Partition.md)
4. [Installation](./Notes/1-3_Installation.md)
5. [Configure The System](./Notes/1-4_Configure_The_System.md)

### General Recommendations

1. [System Administration](./Notes/2-1_System_Administration.md)
2. [Package Management](./Notes/2-2_Package_Management.md)
3. [Booting](./Notes/2-3_Booting.md)
4. [Graphical user interface](./Notes/2-4-Graphical_User_Interface.md)
5. [Power Management](./Notes/2-5_Power_Management.md)
6. [Multimedia](./Notes/2-6_Multimedia.md)
7. [Networking](./Notes/2-7_Networking.md)
8. [Input devices](./Notes/2-8_Input_devices.md) (uncomplete)
9. [Optimization](./Notes/2-9_Optimization.md) (uncomplete)
10. [System service](./Notes/2-10_System_service.md) (uncomplete)
11. [Appearance](./Notes/2-11_Appearance.md) (uncomplete)
12. [Console improvements](./Notes/2-12_Console_improvements.md) (uncomplete)


## Power And Performance

* [Power Management](https://wiki.archlinux.org/title/Power_management)
* [Performance Improving](https://wiki.archlinux.org/title/Improving_performance)


## Shell Script

### alis.sh

I write a Shell script [alis.sh](/GNULinux/Distros/ArchLinux/alis.sh) to install [Arch Linux][archlinux] automatically.

Booting via bootable USB stick (<kbd>F12</kbd>), default font size is too tiny. You need to change font size, then configure wifi setting.

```bash
setfont latarcyrheb-sun32    # Console Font For HiDPI display
# wifi-menu -o    # Wireless Network Configuration

# Via 'iwctl'
INTERFACE='wlan0'
SSID='xxxxx'
iwctl station $INTERFACE connect $SSID

alis_url='https://gitlab.com/axdsop/nixnotes/raw/master/GNULinux/Distros/ArchLinux/alis.sh'
curl -fsL "${alis_url}" > alis.sh
# or
# curl -fsL https://axdlog.com/script/alis.sh > alis.sh

# list help info
bash alis.sh -h

# reboot, need to change password immediately, defaul password is "Arch_$(date +'%Y')", e.g This year is 2019, password is Arch_2019

# script generated from script alis.sh (gsettings)
# bash ~/postinstallation.sh

# Connect wifi network via 'nmcli'
nmcli --ask d wifi connect $SSID
```

### Others

Other scripts listed in directory [Scripts](./Scripts/).

* [package_downgrade.sh](./Scripts/package_downgrade.sh)


## Change Log

* Jun 25, 2019 15:34 Tue ET
  * First draft
* Jul 08, 2019 14:12 Mon ET
  * Add NordVPN usage
* Jul 09, 2019 17:01 Tue ET
  * Add Docker usage, but has problem
* Jul 13, 2019 15:36 Sat ET
  * Add NordVPN setting up Shell script
* Apr 15, 2020 16:34 Wed ET
  * Remove Docker, NordVPN notes
* Jul 28, 2021 21:38 Wed ET
  * Add script *package_downgrade.sh*


[mbxp]:https://consumer.huawei.com/en/laptops/matebook-x-pro/
[archlinux]:https://www.archlinux.org "A lightweight and flexible LinuxÂ® distribution"
[archlinux_wiki]:https://wiki.archlinux.org/
[installation_guide]:https://wiki.archlinux.org/title/Installation_guide "Guide through the process of installing Arch Linux."
[general_recommendations]:https://wiki.archlinux.org/title/General_recommendations "Annotated index of post-installation tutorials and other popular articles."

<!-- End -->