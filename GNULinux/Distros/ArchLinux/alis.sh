#!/usr/bin/env bash
set -u  #Detect undefined variable
set -o pipefail #Return return code in pipeline fails

# Prerequisite
# setfont latarcyrheb-sun32    # Console Font For HiDPI display
# iwctl station $INTERFACE connect $SSID  # Wireless Network Configuration ('wifi-menu -o' is deprecated)


# Target: Installing Arch Linux On HUAWEI MateBook X Pro 2018 (MBXP 2018) From Virtual Console
# Developer: MaxdSre

# Change Log:
# - Jul 21, 2021 10:08 Wed ET - add MAC address spoofing in networkmanager
# - Dec 03, 2020 18:34 Thu ET - add logical volume delete operation while remove disk partitions via parted
# - Nov 20, 2020 11:28 Fri ET - follow the latest Installation guide wiki, disk partition choose LVM method, remove i3 stack installation
# - May 20, 2020 16:29 Wed ET - vim configuration update, vimrc add plugin spoort
# - May 14, 2020 22:30 Thu ET - just install i3, not install desktop environment and Nvidia discrete card drive
# - Apr 11, 2020 12:44 Sat ET - add parameter about desktop environment choose, custom operation choose, kernel version choose
# - Mar 27, 2020 09:02 Fri ET - fix variable name 'normal_user_default' in DM_i3
# - Oct 31, 2019 20:18 Thu ET - add periodic TRIM for SSD, change vimrc permission
# - Oct 29, 2019 09:16 Tue ET - change variable name format in custom functions
# - Oct 17, 2019 16:36 Thu ET - git repo url update, change custom function name format
# - Jun 27, 2019 11:20 Thu ET - add i3 configuration
# - Jun 19, 2019 10:47 Wen ET - first draft

# https://hackmd.io/_9YaDfUPRmuAMA328n2hJw#
# https://github.com/danny8376/arch_install_script/blob/master/arch.sh
# https://github.com/LukeSmithxyz/LARBS


#########  0-0. Variables Setting  #########
normal_user_default=${normal_user_default:-'arch'}
hostname_default=${hostname_default:-'MBXP'}
default_password="Arch_$(date +'%Y')"  # eg This year is 2019, password is Arch_2019
multilib_enable=${multilib_enable:-0}
custom_operation_enable=${custom_operation_enable:-0}
desktop_environment_choose=${desktop_environment_choose:-}
graphic_discrete_card_enable=${graphic_discrete_card_enable:-0}
kernel_version_choose=${kernel_version_choose:-0}
custom_git_repo_url='https://gitlab.com/axdsop/nixnotes/raw/master'


#########  1-1 getopts Operation  #########
fn_HelpInfo(){
echo -e "\e[33mUsage:
    script [options] ...
    script | bash -s -- [options] ...

Installing Arch Linux On HUAWEI MateBook X Pro (MBXP) From Virtual Console.

Default password for root and normal user you specify is \e[31;1m${default_password}\e[0m.\e[33m

New created user is forced to change passwd when first login.

[available option]
    -h    --help, show help info
    -u username    --specify normal user name used for login, default is user 'arch'.
    -H hostname    --specify host name, default is 'MBXP'.
    -d desktop_environment    --specify desktop environment (gnome|plasma), default is empty.
    -g val    --if enable Nvidia card (1 enable, 0 disable), default is 0
    -k val    --specify kernel version (0 latest version, 1 both latest version and lts version), default is 0.
    -c val    -- if enable custom configuration (input method) (1 is enable, 0 is default), default is 0.
    -m val    -- if enable multilib repository and install 32-bit applications (1 is enable, 0 is default), default is 0. 
\e[0m"
}

while getopts "hu:H:c:d:g:k:m:" option "$@"; do
    case "$option" in
        u ) normal_user_default="$OPTARG" ;;
        H ) hostname_default="$OPTARG" ;;
        c ) custom_operation_enable="$OPTARG" ;;
        d ) desktop_environment_choose="$OPTARG" ;;
        g ) graphic_discrete_card_enable="$OPTARG" ;;
        k ) kernel_version_choose="$OPTARG" ;;
        m ) multilib_enable="$OPTARG" ;;
        h|\? ) fn_HelpInfo && exit ;;
    esac
done


#########  2-0 Preapration & Config File Configuration  #########
fn_ParameterChecking(){
    [[ "${custom_operation_enable}" -ne 0 ]] && custom_operation_enable=1
    [[ "${multilib_enable}" -ne 0 ]] && multilib_enable=1
}

fn_PreparationOperation(){
    fn_ParameterChecking

    # - Country Code
    country_code=${country_code:-'US'} # US or United States
    # ipinfo.io   The limit is 1,000 requests per day from an IP address.
    # ip-api.com  The limit is 150 requests per minute from an IP address.
    country_code=$(timeout 3 curl ipinfo.io/country 2> /dev/null || timeout 3 curl ip-api.com/line/?fields=countryCode 2> /dev/null)

    # - Boot Mode Type
    boot_mode=${boot_mode:-'BIOS'}
    if [[ -d /sys/firmware/efi/efivars || -f /sys/firmware/efi/fw_platform_size ]]; then
        # echo 'Boot mode is UEFI.'
        boot_mode='UEFI'
    fi
}

fn_ConfigForGRUB(){
    local l_variable=${1:-}
    local l_parameter=${2:-}
    local l_config_path=${3:-'/etc/default/grub'}

    if [[ -f "${l_config_path}" && -n "${l_variable}" && -n "${l_parameter}" ]]; then
        if [[ -z $(sed -r -n '/'"${l_variable}"'=/{/'"${l_parameter%%=*}"'/{p}}' "${l_config_path}") ]]; then
            [[ -n "${swap_partition_uuid}" ]] && sed -r -i '/'"${l_variable}"'=/{s@([^=]+="[^"]+)(.*)@\1 '"${l_parameter}"'\2@g;}' "${l_config_path}"
        fi
    fi
}

fn_ConfigForMkinitcpio(){
    # https://wiki.archlinux.org/title/Mkinitcpio#Configuration
    local l_variable=${1:-} # MODULES/BINARIES/FILES/HOOKS/COMPRESSION/COMPRESSION_OPTIONS
    local l_parameter=${2:-}
    local l_config_path=${3:-'/etc/mkinitcpio.conf'}

    if [[ -f "${l_config_path}" && -n "${l_variable}" && -n "${l_parameter}" ]]; then
        l_variable="${l_variable^^}"
        if [[ -z $(sed -r -n '/^'"${l_variable}"'=/{/'"${l_parameter}"'/{p}}' "${l_config_path}") ]]; then
            sed -r -i '/^'"${l_variable}"'=/{s@([^\)]+)(.*)@\1 '"${l_parameter}"'\2@g;s@\([[:space:]]*@\(@g;}' "${l_config_path}"
        fi
    fi
}


#########  2-1 Before Chroot  #########
# Change partition setting for your personal requirements.
fnBC_Partition_LVM(){
    partition_size_suffix='MiB'
    boot_partition_allocate_size=${boot_partition_allocate_size:-512} # 0.5GiB
    lvm_partition_allocate_size=${lvm_partition_allocate_size:-153600} # 150GiB
    root_partition_allocate_size=${root_partition_allocate_size:-81902} # 80GiB
    memeroty_total_size=$(free -m | sed -r -n '/^Mem:/{s@^[^:]+:[[:space:]]*([^[:space:]]+).*@\1@g;p}')
    swap_allocate_size=${swap_allocate_size:-"${memeroty_total_size}"}
    # swap_allocate_size=$(echo "var=${memeroty_total_size};var*=1.5;var" | bc)
    swap_allocate_size=$(python -c $'print(int('"${memeroty_total_size}"'*1.5))')

    volume_group_name=${volume_group_name:-'Arch_LVM'}
    logical_volume_name_root=${logical_volume_name_root:-'arch_root'}
    logical_volume_name_swap=${logical_volume_name_swap:-'arch_swap'}
    logical_volume_name_home=${logical_volume_name_home:-'arch_home'}

    # nvme0n1
    disk_name=$(lsblk -l | sed -r -n '/:0.*disk/{/^sd/d;s@^([^[:space:]]+).*$@\1@g;p}')
    # /dev/nvme0n1
    nvme_path="/dev/${disk_name}"
    # NVMe testing    # https://wiki.archlinux.org/title/Solid_state_drive/NVMe#Testing
    # hdparm -Tt --direct "${nvme_path}"

    # Partition print
    parted ${nvme_path} print

    # Keep Windows 10 partitions
    # parted ${nvme_path} print | sed -r -n '1,/msftdata/d;/msftdata/d;/^$/d;s@^[[:space:]]*([^[:space:]]+).*$@\1@g;p' | sort -r | while read -r num; do
    #     [[ -n "${num}" ]] && parted ${nvme_path} rm "${num}"
    # done

    parted ${nvme_path} print | sed -r -n '1,/msftdata/d;/msftdata/d;/^$/d;s@^[[:space:]]*([^[:space:]]+).*$@\1@g;p' | sort -r | while read -r num; do
        if [[ -n "${num}" ]]; then
            # remove logical volume existed
            vg_name_extract=${vg_name_extract:-}
            vg_name_extract=$(parted ${nvme_path} print | sed -r -n '/^[[:space:]]+'"${num}"'[[:space:]]+.*[[:space:]]+lvm$/{s@.*[[:space:]]+([^[:space:]]+)[[:space:]]+lvm$@\1@g;s@-@_@g;p}')

            if [[ -n "${vg_name_extract}" ]]; then
                find "/dev/${vg_name_extract}" -type l -print | sed '/^$/d' | while read -r line; do
                    case "${line##*/}" in
                        *swap) swapoff "${line}" ;;
                    esac

                    lvremove -f "${line}"
                done

                vgremove "${vg_name_extract}"
            fi

            parted ${nvme_path} rm "${num}"
        fi
    done

    # Partition print
    parted ${nvme_path} print

    last_partition_info=$(parted ${nvme_path} unit "${partition_size_suffix}" print | sed -r -n '/^$/d;p' | sed -r -n '$!d;s@^[[:space:]]*@@g;s@[[:space:]]{2,}@\|@g;s@([[:digit:]]+)MiB@\1@g;p')
    # Number|Start|End|Size|File system|Name|Flags
    # 4|129985|232385|102400|Basic data partition|msftdata
    last_partition_num=${last_partition_num:-0}
    last_partition_end_size=${last_partition_end_size:-}
    last_partition_num=$(cut -d\| -f1 <<< "${last_partition_info}")
    last_partition_end_size=$(cut -d\| -f3 <<< "${last_partition_info}")

    # - Boot partition
    current_partition_num="${last_partition_num}"
    current_partition_end_size=$((last_partition_end_size + boot_partition_allocate_size))
    # boot 512MiB
    # lvm  153600MiB (150GiB) {root 80GiB, swap mem_total*1.5 GiB, home left space}
    let "current_partition_num+=1"
    boot_partition_device_path="${nvme_path}p${current_partition_num}"
    parted ${nvme_path} mkpart 'Arch-Boot' fat32 ${last_partition_end_size}${partition_size_suffix} ${current_partition_end_size}${partition_size_suffix}
    parted ${nvme_path} set "${current_partition_num}" esp on  # Flags shows 'boot, esp'
    mkfs.fat -F32 "${boot_partition_device_path}"

    # - LVM partition
    last_partition_end_size="${current_partition_end_size}"
    current_partition_end_size=$((last_partition_end_size + lvm_partition_allocate_size))
    let "current_partition_num+=1"
    lvm_partition_device_path="${nvme_path}p${current_partition_num}"
    parted ${nvme_path} mkpart 'Arch-LVM' xfs ${last_partition_end_size}${partition_size_suffix} ${current_partition_end_size}${partition_size_suffix}
    parted ${nvme_path} set "${current_partition_num}" lvm on  # Flags shows 'lvm'

    # - LVM Operation
    # https://wiki.archlinux.org/title/LVM
    pvcreate ${lvm_partition_device_path}
    vgcreate "${volume_group_name}" "${lvm_partition_device_path}"

    lvcreate -n "${logical_volume_name_root}" -L ${root_partition_allocate_size}${partition_size_suffix} "${volume_group_name}"
    # WARNING: swap signature detected on /dev/Test_Arch_LVM/test_arch_swap at offset 4086. Wipe it? [y/n]:
    # https://github.com/chef-cookbooks/lvm/issues/45
    lvcreate -n "${logical_volume_name_swap}" -L ${swap_allocate_size}${partition_size_suffix} "${volume_group_name}"
    lvcreate -n "${logical_volume_name_home}" -l 100%FREE "${volume_group_name}"

    mkfs.xfs -L arch_root /dev/${volume_group_name}/${logical_volume_name_root}
    mkfs.xfs -L arch_home /dev/${volume_group_name}/${logical_volume_name_home}

    mkswap /dev/${volume_group_name}/${logical_volume_name_swap}
    swapon /dev/${volume_group_name}/${logical_volume_name_swap}

    mount /dev/${volume_group_name}/${logical_volume_name_root} /mnt
    mkdir -p /mnt/{boot,home}
    mount /dev/${volume_group_name}/${logical_volume_name_home} /mnt/home
    mount "${boot_partition_device_path}" /mnt/boot
}


fnBC_Partition_Deprecated(){
    # nvme0n1
    disk_name=$(lsblk -l | sed -r -n '/:0.*disk/{/^sd/d;s@^([^[:space:]]+).*$@\1@g;p}')
    # /dev/nvme0n1
    nvme_path="/dev/${disk_name}"
    # NVMe testing    # https://wiki.archlinux.org/title/Solid_state_drive/NVMe#Testing
    # hdparm -Tt --direct "${nvme_path}"

    # Partition print
    parted /dev/nvme0n1 print

    # keep Windows 10 partitions
    parted /dev/nvme0n1 print | sed -r -n '1,/msftdata/d;/msftdata/d;/^$/d;s@^[[:space:]]*([^[:space:]]+).*$@\1@g;p' | sort -r | while read -r num; do
        [[ -n "${num}" ]] && parted /dev/nvme0n1 rm "${num}"
    done

    # Partition print
    parted /dev/nvme0n1 print

    # 5 1GB
    parted /dev/nvme0n1 mkpart extended fat32 350GB 351GB
    parted /dev/nvme0n1 set 5 esp on
    # 6 50GB
    parted /dev/nvme0n1 mkpart extended xfs 351GB 401GB
    # 7 24GB
    parted /dev/nvme0n1 mkpart extended linux-swap 401GB 425GB
    # 8 45GB
    parted /dev/nvme0n1 mkpart extended xfs 425GB 470GB
    # 9 35GB
    parted /dev/nvme0n1 mkpart extended xfs 470GB 505GB

    mkfs.fat -F32 /dev/nvme0n1p5
    mkfs.xfs -f /dev/nvme0n1p6
    mkswap /dev/nvme0n1p7
    # swapon /dev/nvme0n1p7
    mkfs.xfs -f /dev/nvme0n1p8
    mkfs.xfs -f /dev/nvme0n1p9

    # mount root partition to /mnt
    mount /dev/nvme0n1p6 /mnt

    # mount boot partition to /mnt/boot/
    mkdir -p /mnt/boot
    mount /dev/nvme0n1p5 /mnt/boot/

    swapon /dev/nvme0n1p7

    # mount home directory to /mnt/home
    mkdir -p /mnt/home
    mount /dev/nvme0n1p8 /mnt/home

    # mount /opt directory to /mnt/opt
    mkdir -p /mnt/opt
    mount /dev/nvme0n1p9 /mnt/opt

    # Partition print
    parted /dev/nvme0n1 print
}

fn_BeforeChroot(){
    # Console Font Scaling For HiDPI
    setfont latarcyrheb-sun32

    echo "Boot mode is ${boot_mode}".

    # Internet network testing
    timeout 3 ping -c 2 google.com

    # Disk partition
    fnBC_Partition_LVM

    [[ -f /etc/pacman.d/mirrorlist.backup ]] || cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.backup
    # mirror xtom.com is faster, so don't update mirror list via reflector
    echo -e 'Server = https://mirrors.xtom.com/archlinux/$repo/os/$arch\nServer = https://mirror.math.princeton.edu/pub/archlinux/$repo/os/$arch' > /etc/pacman.d/mirrorlist

    # https://wiki.archlinux.org/title/mirrors#Sorting_mirrors
    # pacman -Syy --noconfirm reflector

    # select 30 HTTPS mirrors synchronized within the last 24 hours most recently, sort then by download speed, and overwrite the file /etc/pacman.d/mirrorlist
    # reflector --verbose -a 24 --latest 60 --number 30 --protocol https --sort rate --country "${country_code}" --threads 10 --save /etc/pacman.d/mirrorlist

    # Synchronizing package database
    # force a refresh of all package databases, even if they appear to be up-to-date
    # pacman -Syyu --noconfirm

    # Install the base system
    # Install base-devel to install yay (Yet another Yogurt - An AUR Helper written in Go)
    pacstrap /mnt base base-devel linux linux-firmware lvm2 python bash mkinitcpio curl

    # https://wiki.archlinux.org/title/Install_Arch_Linux_on_LVM#Adding_mkinitcpio_hooks
    # add 'lvm2' before 'filesystems' in configuration file /etc/mkinitcpio.conf
    sed -r -i '/^#*HOOK/{s@^#*(.*?)(filesystems.*)$@\1lvm2 \2@g}' /mnt/etc/mkinitcpio.conf

    genfstab -U /mnt >> /mnt/etc/fstab
    # echo -e "Please execute: arch-chroot /mnt"
}


#########  2-2 After Chroot  #########
fnAC_TimezoneLocaleHostname(){
    time_zone=${time_zone:-'America/New_York'}
    time_zone=$(timeout 3 curl ipinfo.io/timezone 2> /dev/null || timeout 3 curl ip-api.com/line/?fields=timezone 2> /dev/null)

    ln -sf /usr/share/zoneinfo/${time_zone} /etc/localtime
    # run hwclock(8) to generate /etc/adjtime
    hwclock --systohc

    # https://wiki.archlinux.org/title/Systemd-timesyncd
    # https://unix.stackexchange.com/questions/336566/how-to-fix-time-in-arch-linux
    local timesyncd_conf='/etc/systemd/timesyncd.conf'
    if [[ -f "${timesyncd_conf}" ]]; then
        # Sometimes domain pool.ntp.org fail to resolve, so add other ntp servers provides by nist, windows, apple, google
        local ntp_list='time.nist.gov time.windows.com time.apple.com time.google.com'
        # 0.arch.pool.ntp.org 1.arch.pool.ntp.org 2.arch.pool.ntp.org 3.arch.pool.ntp.org 
        local ntp_fallback_list='0.pool.ntp.org 1.pool.ntp.org 2.pool.ntp.org'
        sed -r -i '/\[Time\]/,${/^#*[[:space:]]*NTP=/{s@.*@NTP='"${ntp_list}"'@g}; /^#*[[:space:]]*FallbackNTP=/{s@.*@FallbackNTP='"${ntp_fallback_list}"'@g}}' "${timesyncd_conf}"
    fi

    timedatectl set-ntp true
    systemctl restart systemd-timesyncd.service
    timedatectl status
    timedatectl show-timesync --all

    # https://wiki.archlinux.org/title/Locale
    sed -r -i '/^#?[^[:space:]]+_'"${country_code^^}"'\.UTF-8/{s@^#?[[:space:]]*@@g;}' /etc/locale.gen

    locale_default=${locale_default:-'en_US.UTF-8'}  # zh_TW.UTF-8 / zh_CN.UTF-8
    locale_list=$(sed -r -n '/^#?[^[:space:]]+_'"${country_code^^}"'\.UTF-8/{s@^#?[[:space:]]*@@g;s@^([^[:space:]]+).*$@\1@g;p}' /etc/locale.gen)
    locale_list_count=$(echo "${locale_list}" | wc -l)
    [[ "${locale_list_count}" -eq 1 ]] && locale_default="${locale_list}"

    # generate locales from /etc/locale.gen
    locale-gen
    localectl list-locales
    # Setting the system locale
    # https://wiki.archlinux.org/title/Locale#Setting_the_system_locale
    # localectl set-locale LANG=${locale_default}
    echo "LANG=${locale_default}" > /etc/locale.conf
    # if file /etc/locale.conf not exists or not locale, gnome-terminal will fail to start
    export LANG="${locale_default}"

    # hostname_default=${hostname_default:-'MBXP2019'}
    # https://wiki.archlinux.org/title/Network_configuration#Set_the_hostname
    echo "${hostname_default}" > /etc/hostname
    # https://wiki.archlinux.org/title/Network_configuration#Local_hostname_resolution
    echo -e "127.0.0.1   localhost\n::1         localhost\n127.0.1.1   ${hostname_default}.localdomain  ${hostname_default}" >> /etc/hosts
}

fnAC_UserOperation(){
    # sudo setting
    pacman -S --noconfirm sudo
    [[ -f /etc/sudoers ]] && sed -r -i 's@#*[[:space:]]*(%wheel[[:space:]]+ALL=\(ALL\)[[:space:]]+ALL)@# \1@;s@#*[[:space:]]*(%wheel[[:space:]]+ALL=\(ALL\)[[:space:]]+NOPASSWD: ALL).*@\1,!/bin/su@' /etc/sudoers

    # root password
    # default_password="Arch_$(date +'%Y')"  # eg This year is 2019, password is Arch_2019
    echo "root:${default_password}" | chpasswd
    # echo -e "${default_password}\n${default_password}" | passwd root

    # chage -d0 root  # new created user have to change passwd when first login
    # Don't execute 'chage -d0 root' here, it will prompts error 'You are required to change your password immediately (administrator enforced)', 'useradd: PAM: Authentication token is no longer valid; new one required'

    # Add new normal user
    # normal_user_default='archlinux'
    login_shell=${login_shell:-'/bin/bash'}
    useradd -m -N -G wheel -s "${login_shell}" "${normal_user_default}"
    # useradd -mN -G wheel "${normal_user_default}"
    # usermod -s "${login_shell}" "${normal_user_default}" # change login shell
    echo "${normal_user_default}:${default_password}" | chpasswd
    # echo -e "${default_password}\n${default_password}" | passwd "${normal_user_default}"
    # chage -d0 "${normal_user_default}"  # new created user have to change passwd when first login
    normal_user_home="/home/${normal_user_default}"
}

fnAC_PackageManager(){
    # - Pacman
    local l_pacman_conf=${l_pacman_conf:-'/etc/pacman.conf'}

    if [[ -f "${l_pacman_conf}" ]]; then
        # https://wiki.archlinux.org/title/Official_repositories#Enabling_multilib
        [[ "${multilib_enable}" -eq 1 ]] && sed -r -i '/^#*\[multilib\]/,/^$/{/^#+/{s@^#*@@g;}}' "${l_pacman_conf}"

        # Misc optinos
        sed -r -i '/^#*[[:space:]]*(Color|TotalDownload|CheckSpace|VerbosePkgLists)/{s@^#*[[:space:]]*@@g;}' "${l_pacman_conf}"
    fi
    # https://wiki.archlinux.org/title/System_maintenance#Avoid_certain_pacman_commands
    # Avoid doing partial upgrades. In other words, never run pacman -Sy; instead, always use pacman -Syu.
    pacman -Syyu --noconfirm

    # - reflector systemd
    pacman -Sy --noconfirm reflector

    if [[ -d /etc/systemd/system/ ]]; then
        echo -e "[Unit]\nDescription=Pacman mirrorlist update\nWants=network-online.target\n# After=network-online.target\n\n[Service]\nType=oneshot\nExecStart=/usr/bin/reflector -a 24 --latest 50 --number 30 --protocol https --sort rate --save /etc/pacman.d/mirrorlist\n\n[Install]\nRequiredBy=multi-user.target" > /etc/systemd/system/reflector.service
        echo -e "[Unit]\nDescription=Run reflector periodically\n\n[Timer]\nOnBootSec=20min\nOnUnitActiveSec=8h\nRandomizedDelaySec=30min\n# OnCalendar=weekly\n# RandomizedDelaySec=15h\nPersistent=true\n\n[Install]\nWantedBy=timers.target" > /etc/systemd/system/reflector.timer

        systemctl disable reflector.service
        systemctl disable reflector.timer
    fi

    # makepkg is a script to automate the building of packages. The requirements for using the script are a build-capable Unix platform and a PKGBUILD.
    # makepkg is provided by the pacman package.
    # MAKEFLAGS optimization https://wiki.archlinux.org/title/Makepkg
    local l_makepkg_conf=${l_makepkg_conf:-'/etc/makepkg.conf'}

    if [[ -f "${l_makepkg_conf}" ]]; then
        [[ -f "${l_makepkg_conf}.origin" ]] || cp "${l_makepkg_conf}" "${l_makepkg_conf}.origin"

        # Building optimized binaries
        sed -r -i '/^#*[[:space:]]*CFLAGS=/{s@^#*[[:space:]]*@@g;s@-mtune=[^[:space:]]+[[:space:]]+@@g;s@(-march=)[^[:space:]]+@\1native@g}' "${l_makepkg_conf}"    # CFLAGS="-march=native -O2 -pipe -fno-plt"
        sed -r -i '/^#*[[:space:]]*CXXFLAGS=/{s@^#*[[:space:]]*([^=]+=).*@\1\"\$\{CFLAGS\}\"@g}' "${l_makepkg_conf}"    # CXXFLAGS="${CFLAGS}"
        sed -r -i '/^#*[[:space:]]*RUSTFLAGS=/{s@^#*[[:space:]]*([^=]+=.*?opt-level=[[:digit:]]+).*?(")$@\1 -C target-cpu=native\2@g}' "${l_makepkg_conf}"    # From pacman version 5.2.2,  # RUSTFLAGS="-C opt-level=2 -C target-cpu=native"

        # Parallel compilation
        # Enable multi-core processors
        local l_processor_num=${l_processor_num:-0}
        l_processor_num=$(nproc 2> /dev/null) # /usr/bin/nproc is owned by coreutils
        [[ "${l_processor_num}" -eq 0 ]]&& l_processor_num=$(sed -r -n '/^processor/{p}' /proc/cpuinfo | wc -l)
        [[ "${l_processor_num}" -gt 0 ]] && sed -r -i '/^#*[[:space:]]*MAKEFLAGS=/{s@^#*[[:space:]]*([^=]+=).*@\1"-j'${l_processor_num}'"@g}' "${l_makepkg_conf}"

        # Building from files in memory
        # The BUILDDIR variable can be temporarily exported to makepkg to set the build directory to an existing tmpfs.
        # https://wiki.archlinux.org/title/Tmpfs#Usage
        sed -r -i '/^#*[[:space:]]*BUILDDIR=/{s@^#*[[:space:]]*@@g;}' "${l_makepkg_conf}"

        # Utilizing multiple cores on compression
        # COMPRESSXZ     # xz    -T threads, --threads=threads    Specify  the  number of worker threads to use.  Setting threads to a special value 0 makes xz use as many threads as there are CPU cores on the system.
        sed -r -i '/^COMPRESSXZ=/{s@(.*-c).*?(-z.*)$@\1 -T 0 \2@g;}' "${l_makepkg_conf}"
        sed -r -i '/^COMPRESSZST=/{s@--threads=[[:digit:]]+@@g;s@(-q -)\)$@\1--threads=0\)@g}' "${l_makepkg_conf}"    # COMPRESSZST=(zstd -c -z -q - --threads=0)

        # Create uncompressed packages
        # https://wiki.archlinux.org/title/Makepkg#Use_other_compression_algorithms
        # https://wiki.manjaro.org/index.php?title=Makepkg#Create_uncompressed_packages
        sed -r -i '/^#*PKGEXT=/{s@^#*([^=]+=).*@\1'\''.pkg.tar'\'' # .pkg.tar.xz create uncompressed packages@g;}' "${l_makepkg_conf}"

        # Enable ccache for makepkg
        # https://wiki.archlinux.org/title/Ccache
        pacman -S --noconfirm ccache
        # ccache -s # ~/.ccache/ccache.conf /etc/ccache.conf  max_size = 5.0G
        # ccache -C # clear cache completely
        sed -r -i '/^BUILDENV=/{s@!(ccache)@\1@g;}' "${l_makepkg_conf}"
    fi

    # - Arch User Repository
    # After normal user creaded
    # https://wiki.archlinux.org/title/Arch_User_Repository#Prerequisites
    pacman -S --noconfirm git # base-devel
    # cd /tmp
    git clone https://aur.archlinux.org/yay-bin.git
    chmod 777 yay-bin
    cd yay-bin
    # ERROR: Running makepkg as root is not allowed as it can cause permanent, catastrophic damage to your system
    sudo -u "${normal_user_default}" makepkg -si --noconfirm
    cd ..
    rm -rf yay-bin
    # If run yay as root/sudo, prompt error info 'Please avoid running yay as root/sudo.' and 'Refusing to install AUR Packages as root, Abourting.'.
    # yay -Syu --noconfirm
    sudo -u "${normal_user_default}" yay -Syu --cleanafter --noconfirm
}

fnAC_GRUB(){
    # https://wiki.archlinux.org/title/Mkinitcpio
    # mkinitcpio is a Bash script used to create an initial ramdisk (initramfs) environment.
    mkinitcpio -P

    # https://wiki.archlinux.org/title/Microcode
    model_name=$(sed -r -n '/model name/I{s@.*@\L&@p;q}' /proc/cpuinfo 2> /dev/null)

    if [[ "${model_name}" =~ intel ]]; then
        pacman -S --noconfirm intel-ucode
    elif [[ "${model_name}" =~ amd ]]; then
        pacman -S --noconfirm amd-ucode
    fi

    # https://wiki.archlinux.org/title/GRUB#Installation_2
    pacman -S --noconfirm grub efibootmgr
    # https://wiki.archlinux.org/title/GRUB#Detecting_other_operating_systems
    pacman -S --noconfirm os-prober ntfs-3g
    # detect existed Windows boot menu
    os-prober

    grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=grub

    # https://wiki.archlinux.org/title/GRUB/Tips_and_tricks
    local l_default_grub=${l_default_grub:-'/etc/default/grub'}

    if [[ -f "${l_default_grub}" ]]; then
        # change grub boot menu font
        sed -r -i '/^#*[[:space:]]*GRUB_GFXMODE/{s@^#*[[:space:]]*@@g;s@^([^=]+=).*@\11600x1200x32@g;}' "${l_default_grub}"

        # https://wiki.archlinux.org/title/GRUB#Additional_arguments
        # Uncomment to disable generation of recovery mode menu entries
        sed -r -i '/^#*[[:space:]]*GRUB_DISABLE_RECOVERY=/{s@^#*[[:space:]]*@@g;}' "${l_default_grub}"

        # Disable submenu
        sed -r -i '/^#*[[:space:]]*GRUB_DISABLE_SUBMENU=/d' "${l_default_grub}"
        sed -r -i '/^#*[[:space:]]*GRUB_TIMEOUT=/a GRUB_DISABLE_SUBMENU=y' "${l_default_grub}"
    fi

    grub-mkconfig -o /boot/grub/grub.cfg
    # cat /boot/grub/grub.cfg
}

fnAC_Grahpics(){
    # - Intel Graphics
    pacman -S --noconfirm mesa
    # https://wiki.archlinux.org/title/Vulkan
    # Vulkan is a low-overhead, cross-platform 3D graphics and compute API.
    pacman -S --noconfirm vulkan-intel vulkan-icd-loader
    # ls /usr/share/vulkan/icd.d/  # verification
    # [[ "${multilib_enable}" -eq 1 ]] && pacman -S --noconfirm lib32-mesa lib32-vulkan-icd-loader

    # - disable discrete NVIDIA GPU on boot via bbswitch
    pacman -S --noconfirm bbswitch
    # Load bbswitch module on boot
    [[ -d /etc/modules-load.d/ ]] && echo 'bbswitch ' > /etc/modules-load.d/bbswitch.conf
    # Disable the nvidia module on boot
    [[ -d /etc/modprobe.d/ ]] && echo 'options bbswitch load_state=0 unload_state=1' > /etc/modprobe.d/bbswitch.conf

    # Discrete Graphic Card    
    if [[ "${graphic_discrete_card_enable}" -ne 0 ]]; then
        # - nvidia-xrun
        pacman -S --noconfirm nvidia
        # [[ "${multilib_enable}" -eq 1 ]] && pacman -S --noconfirm lib32-nvidia-utils
        # If run yay as root/sudo, prompt error info 'Please avoid running yay as root/sudo.' and 'Refusing to install AUR Packages as root, Abourting.'.
        # nvidia-xrun version is too old  Last Updated: 2016-10-20 21:48 https://aur.archlinux.org/packages/nvidia-xrun/
        sudo -u "${normal_user_default}" yay -S --noconfirm nvidia-xrun-git

        [[ -d /usr/lib/modprobe.d/ ]] && echo -e "blacklist nvidia\nblacklist nvidia-drm\nblacklist nvidia-modeset\nblacklist nvidia-uvm\nblacklist nouveau" >  /usr/lib/modprobe.d/nvidia-xrun.conf

        echo -e "if [ \$# -gt 0 ]; then\n    \$*\nelse\n    # https://wiki.archlinux.org/title/GNOME#Manually\n    export GDK_BACKEND=x11\n    exec gnome-session\nfi" > /home/${normal_user_default}/.nvidia-xinitrc  #  ~/.nvidia-xinitrc
    fi

    # check bbswitch's status
    [[ -f /proc/acpi/bbswitch ]] && cat /proc/acpi/bbswitch
    # To force the card to turn on/off respectively run:
    # tee /proc/acpi/bbswitch <<<OFF
    # tee /proc/acpi/bbswitch <<<ON
}

fnAC_DesktopEnvrionment(){
    # Desktop Environment
    case "${desktop_environment_choose,,}" in
        plasma|p|p* )
            # Before installing Plasma, make sure you have a working Xorg installation on your system.
            # plasma / plasma-meta / plasma-desktop
            pacman -S --noconfirm plasma-desktop plasma-wayland-session
            # kde-applications / kde-applications-meta
            pacman -S --noconfirm kde-applications
            # an integrated Plasma power managing service
            pacman -S --noconfirm powerdevil
            # https://wiki.archlinux.org/title/NetworkManager#KDE_Plasma
            # add it to the KDE taskbar via the Panel options > Add widgets > Networks menu
            pacman -S --noconfirm plasma-nm
            #  display manager
            pacman -S --noconfirm sddm
            systemctl enable sddm.service
            ;;
        gnome|g|g* )
            # - desktop environment package
            pacman -S --noconfirm gnome
            # pacman -Sg gnome | cut -d' ' -f2 | xargs pacman -S --noconfirm
            # https://wiki.archlinux.org/title/GNOME#Extensions
            pacman -S --noconfirm gnome-shell-extensions gnome-tweaks gnome-control-center gnome-terminal gnome-power-manager gedit gnome-system-monitor
            # remove unneed packages from 'gnome'
            pacman -R --noconfirm gnome-books gnome-boxes gnome-weather gnome-todo gnome-contacts gnome-maps gnome-music epiphany

            # - display manager
            pacman -S --noconfirm gdm
            systemctl enable gdm.service

            # - networkmanager service
            pacman -S --noconfirm networkmanager network-manager-applet
            systemctl disable netctl
            systemctl enable NetworkManager

            # GNOME Desktop Setting
            if [[ "${custom_operation_enable}" -eq 1 ]]; then
                # - Gnome Extensions
                sudo -u "${normal_user_default}" yay -S --noconfirm gnome-shell-extension-dash-to-dock
                # https://wiki.archlinux.org/title/CPU_frequency_scaling
                # yay -S gnome-shell-extension-cpufreq-git
                sudo -u "${normal_user_default}" yay -S --noconfirm gnome-shell-extension-cpupower-git

                # GTK Theme
                # https://github.com/adapta-project/adapta-gtk-theme
                # sudo -u "${normal_user_default}" yay -S --noconfirm adapta-gtk-theme
                # https://numixproject.github.io/
                # sudo -u "${normal_user_default}" yay -S --noconfirm numix-gtk-theme-git numix-circle-icon-theme-git numix-cursor-theme-git
                # # gsettings set org.gnome.desktop.interface gtk-theme "Numix"
                # # gsettings set org.gnome.desktop.wm.preferences theme "Numix"
                # echo -e "gsettings set org.gnome.desktop.interface gtk-theme \"Numix\"\ngsettings set org.gnome.desktop.wm.preferences theme \"Numix\"" >> "${postinstallation_script}"

                # https://drasite.com/flat-remix-gtk      'flat-remix' is icon theme  has 4 colors  4 x 10 choices
                # sudo -u "${normal_user_default}" yay -S --noconfirm flat-remix-gtk flat-remix
                # gsettings set org.gnome.desktop.interface gtk-theme "Flat-Remix-GTK-Blue"
            fi
            ;;

    esac
}

fnAC_InputMethodAndFonts(){
    if [[ "${custom_operation_enable}" -eq 1 ]]; then
        # fcitx-rime 21 packages, download size 32.83 MiB
        # ibus-rime  29 packages, download size 45.38 MiB

        # - Input method - Fcitx
        pacman -S --noconfirm fcitx-rime fcitx-im fcitx-configtool
        # Run fcitx-config-gtk3 after fcitx-configtool is installed. Unset Only Show Current Language if you want to enable an input method for a different language.
        # echo -e "GTK_IM_MODULE=fcitx\nQT_IM_MODULE=fcitx\nXMODIFIERS=@im=fcitx" > "${normal_user_home}/.pam_environment"
        # fcitx-remote is a commandline tool that can be used to control the fcitx state.
        # fcitx-remote -r

        # https://confluence.jaytaala.com/display/TKB/Japanese+input+with+i3+and+Arch+based+distros
        [[ -f "${normal_user_home}/.xinitrc" ]] && sed -r -i '/Fcitx Start/,/Fcitx End/{/export/{s@^#*[[:space:]]*@@g;}}' "${normal_user_home}/.xinitrc" 2> /dev/null
        # fcitx -d      run as daemon(default)
    fi

    # - Fonts
    # https://wiki.archlinux.org/title/Fonts
    # 1.47 MiB
    pacman -S --noconfirm gnu-free-fonts wqy-microhei
    pacman -S --noconfirm ttf-font-awesome ttf-hack

    # ttf-hanazono - 花園明朝(HanaMin)
    # 20.9 MB
    pacman -S --noconfirm ttf-hanazono
    # 176 MiB
    # pacman -S --noconfirm adobe-source-han-sans-otc-fonts adobe-source-han-serif-otc-fonts
    # 146 MiB
    # pacman -S --noconfirm noto-fonts noto-fonts-cjk
    # sudo -u "${normal_user_default}" yay -S --noconfirm ttf-consolas-with-yahei
}

fnAC_Firewall(){
    pacman -S --noconfirm ufw
    # Disable ipv6
    [[ -f /etc/default/ufw ]] && sed -r -i '/^IPV6=/{s@(IPV6=).*@\1no@g;}' /etc/default/ufw 2> /dev/null

    # If allow SSH
    # ufw allow ssh
    # ufw limit from 192.168.0.0/24 to any port 22 # or 'ufw allow'

    # setting default rule (deny incoming, allow outgoing)
    ufw default deny incoming
    ufw default allow outgoing
    systemctl enable ufw.service
    ufw --force enable # non-interactive
    # ufw enable # interactive  Command may disrupt existing ssh connections. Proceed with operation (y|n)?
    ufw status verbose
}

fnAC_EssentialPackages(){
    curl -fsL https://gitlab.com/axdsop/nix-dotfiles/raw/master/Configs/axdsop/axdsop_boost_tool.sh > "${normal_user_home}/boost.sh"

    fnAC_InputMethodAndFonts
    fnAC_Firewall

    # kernel version
    pacman -S --noconfirm linux-headers
    [[ "${kernel_version_choose}" -eq 1 ]] && pacman -S --noconfirm linux-lts linux-lts-headers
    
    pacman -S --noconfirm linux-firmware
    
    # https://wiki.archlinux.org/title/Xinit
    pacman -S --noconfirm xorg-server xorg-xinit

    # https://wiki.archlinux.org/title/XDG_user_directories
    # https://wiki.archlinux.org/title/Xdg-utils#xdg-open
    pacman -S --noconfirm xdg-user-dirs xdg-utils
    sudo -u "${normal_user_default}" xdg-user-dirs-update

    # font is too tiny to see by default
    # pacman -S --noconfirm xorg-xdpyinfo   # xdpyinfo utility from the xorg-xdpyinfo package
    # xdpyinfo | grep -B 2 resolution    # detect physical dimensions of monitor
    # screen #0:
    #   dimensions:    3000x2000 pixels (346x230 millimeters)
    #   resolution:    220x221 dots per inch

    # wifi
    pacman -S --noconfirm wireless_tools wpa_supplicant dialog

    # https://wiki.archlinux.org/title/Touchpad_Synaptics
    # https://wiki.archlinux.org/title/Libinput
    # Note: If you want to configure touchpad via GNOME control center, you need to use the 'libinput' driver.
    # You may also want to install xorg-xinput to be able to change settings at runtime.
    pacman -S --noconfirm libinput xorg-xinput
    # libinput list-devices / xinput list

    # https://wiki.archlinux.org/title/File_systems
    pacman -S --noconfirm exfat-utils
    pacman -S --noconfirm bash-completion openssh git wmctrl
    # The wmctrl program is a UNIX/Linux command line tool to interact with an EWMH/NetWM compatible X Window Manager.
    pacman -S --noconfirm libfido2 # Library functionality for FIDO 2.0, including communication with a device over USB

    # Web Browser
    # pacman -S --noconfirm firefox
    pacman -S --noconfirm dbus-glib # firefox dependency

    # pacman -S --noconfirm vim
    pacman -S --noconfirm neovim python-pynvim

    # systemd-random-seed.service https://bbs.archlinux.org/viewtopic.php?id=249106
    pacman -S --noconfirm haveged
    systemctl enable haveged
}

fnAC_NetworkManagement(){
    # - networkmanager service
    pacman -S --noconfirm networkmanager
    pacman -S --noconfirm openresolv
    # https://wiki.archlinux.org/title/NetworkManager#Unmanaged_/etc/resolv.conf
    echo -e "[main]\ndns=none\nsystemd-resolved=false" > /etc/NetworkManager/conf.d/dns.conf
    # https://wiki.archlinux.org/title/NetworkManager#Use_openresolv
    echo -e "[main]\nrc-manager=resolvconf" > /etc/NetworkManager/conf.d/rc-manager.conf
    # https://wiki.archlinux.org/title/Unbound#Local_DNS_server
    sed -r -i '/^(name_servers|resolv_conf_options)=/d' /etc/resolvconf.conf
    echo -e 'name_servers="::1 127.0.0.1"\nresolv_conf_options="trust-ad"' >> /etc/resolvconf.conf

    # https://wiki.archlinux.org/title/MAC_address_spoofing
    # https://wiki.archlinux.org/title/NetworkManager#Configuring_MAC_address_randomization
    echo -e "# https://wiki.archlinux.org/title/MAC_address_spoofing\n# https://wiki.archlinux.org/title/NetworkManager#Configuring_MAC_address_randomization\n\n[device-mac-randomization]\n# 'yes' is already the default for scanning\nwifi.scan-rand-mac-address=yes\n\n[connection-mac-randomization]\n# mode: stable, random\n# Randomize MAC for every WIFI/ethernet connection\n# Generate a random MAC for each WiFi/ethernet and associate the two permanently.\nethernet.cloned-mac-address=stable\nwifi.cloned-mac-address=random" > /etc/NetworkManager/conf.d/wifi_rand_mac.conf

    # - Via systemd-resolved
    # https://cyfeng.science/2020/05/20/about-linux-dns-service/
    # https://superuser.com/questions/1427311/activation-via-systemd-failed-for-unit-dbus-org-freedesktop-resolve1-service
    systemd_resolve_path='/etc/systemd/resolved.conf'
    [[ -f "${systemd_resolve_path}.backup" ]] || cp "${systemd_resolve_path}"{,.backup}
    # dns server list '9.9.9.9 1.1.1.1'  # or just 9.9.9.9
    sed -r -i '/^#*DNS=/{s@^#*([^=]+=).*@\19.9.9.9 1.1.1.1@g}; /^#*(LLMNR|DNSStubListener)=/{s@^#*([^=]+=).*@\1no@g}' "${systemd_resolve_path}"
    # [[ -f /run/systemd/resolve/resolv.conf && -f /etc/resolv.conf ]] && ln -sf /run/systemd/resolve/resolv.conf /etc/resolv.conf
    systemctl enable systemd-resolved.service

    # - Via unbound
    # choose unbound as dns server instead of 'systemd-resolved', but unbound listens port 53
    # https://wiki.archlinux.org/title/Unbound
    # https://calomel.org/unbound_dns.html
    # https://wiki.archlinux.org/title/Unbound#Local_DNS_server
    # https://wiki.archlinux.org/title/Unbound#Using_openresolv_2
    # pacman -S --noconfirm unbound
    # sed -r -i '/^(unbound_conf)=/d' /etc/resolvconf.conf
    # echo -e 'unbound_conf=/etc/unbound/resolvconf.conf' >> /etc/resolvconf.conf
    # systemctl enable unbound.service

    [[ -f /etc/resolv.conf ]] && rm -rf /etc/resolv.conf
    resolvconf -u
    # systemctl disable netctl
    systemctl enable NetworkManager.service
    systemctl disable NetworkManager-wait-online.service
}

fnAC_SystemService(){
    # Periodic TRIM  https://wiki.archlinux.org/title/Solid_state_drive#TRIM
    systemctl enable fstrim.timer
}

fnAC_HiDPIScreen(){
    # HiDPI font persistent configuration
    # https://wiki.archlinux.org/title/HiDPI#Linux_console
    local l_vconsole_conf=${l_vconsole_conf:-'/etc/vconsole.conf'}
    [[ -f "${l_vconsole_conf}" ]] && sed -r -i '/^FONT=/d' "${l_vconsole_conf}"
    echo "FONT=latarcyrheb-sun32" >> "${l_vconsole_conf}"

    # Disable touch screen
    # https://unix.stackexchange.com/questions/127443/how-do-i-disable-the-touch-screen-on-my-laptop
    local l_libinput_conf=${l_libinput_conf:-'/usr/share/X11/xorg.conf.d/40-libinput.conf'}
    # Change MatchIsTouchscreen val from 'on' to 'off'
    [[ -f "${l_libinput_conf}" ]] && sed -r -i '/touchscreen/,/Driver/{/MatchIsTouchscreen/{s@"on"@"off"@g;}}' "${l_libinput_conf}"
}

fnAC_PowerManagement(){
    # https://wiki.archlinux.org/title/Power_management
    # https://wiki.archlinux.org/title/Improving_performance
    # http://www.thinkwiki.org/wiki/How_to_reduce_power_consumption

    # - Intel GPU
    # https://wiki.archlinux.org/title/Intel_graphics#Module-based_options
    # Enabling GuC/HuC firmware loading can cause issues on some systems; disable it if you experience freezing (for example, after resuming from hibernation).
    # # Making use of Framebuffer compression (FBC) can reduce power consumption while reducing memory bandwidth needed for screen refreshes.
    [[ -d /etc/modprobe.d/ ]] && echo -e "options i915 enable_guc=0 enable_fbc=1 fastboot=1" > /etc/modprobe.d/i915.conf
    # https://wiki.archlinux.org/title/kernel_mode_setting#Early_KMS_start
    # KMS is typically initialized after the initramfs stage. However it is possible to already enable KMS during the initramfs stage. Add the required module for the video driver to the MODULES array in /etc/mkinitcpio.conf:
    # Intel users may need to add 'intel_agp' before 'i915' to suppress the ACPI errors. This may be required for resuming from hibernation to work with a changed display configuration.
    fn_ConfigForMkinitcpio 'MODULES' 'intel_agp'
    fn_ConfigForMkinitcpio 'MODULES' 'i915'

    # Disabling modesetting https://wiki.archlinux.org/title/kernel_mode_setting#Disabling_modesetting
    # Along with 'nomodeset' kernel parameter, for Intel graphics card you need to add 'i915.modeset=0' and for Nvidia graphics card you need to add 'nouveau.modeset=0'. For Nvidia Optimus dual-graphics system, you need to add all the three kernel parameters (i.e. "nomodeset i915.modeset=0 nouveau.modeset=0").

    # - powertop    https://wiki.archlinux.org/title/Powertop
    pacman -S --noconfirm powertop

    #  - TLP   https://wiki.archlinux.org/title/TLP
    pacman -S --noconfirm tlp tlp-rdw
    
    local l_tlp_conf=${l_tlp_conf:-'/etc/default/tlp'}
    [[ -f /etc/tlp.conf ]] && l_tlp_conf='/etc/tlp.conf'
    [[ -f "${l_tlp_conf}" ]] && sudo sed -r -i '/^[[:space:]]*#*[[:space:]]*TLP_ENABLE=/{s@^[[:space:]]*#*[[:space:]]*([^=]+=).*@\11@g;}; /^[[:space:]]*#*[[:space:]]*TLP_DEFAULT_MODE/{s@^[[:space:]]*#*[[:space:]]*([^=]+=).*@\1BAT@g;}' "${l_tlp_conf}"
    # /^[[:space:]]*#*[[:space:]]*TLP_PERSISTENT_DEFAULT=/{s@^[[:space:]]*#*[[:space:]]*([^=]+=).*@\11@g;}

    systemctl enable tlp.service
    systemctl enable tlp-sleep.service
    systemctl mask systemd-rfkill.service
    systemctl mask systemd-rfkill.socket
    # tlp-stat -s
    # Notice: tlp.service is not enabled -- invoke "systemctl enable tlp.service" to correct this!
    # Notice: tlp-sleep.service is not enabled -- invoke "systemctl enable tlp-sleep.service" to correct this!
    # Notice: systemd-rfkill.service is not masked -- invoke "systemctl mask systemd-rfkill.service" to correct this!
    # Notice: systemd-rfkill.socket is not masked -- invoke "systemctl mask systemd-rfkill.socket" to correct this!

    # - Battery Protection
    huawei_wmi_charge_control_path='/sys/devices/platform/huawei-wmi/charge_control_thresholds'
    start_charge_threshold=${start_charge_threshold:-60}
    stop_charge_threshold=${stop_charge_threshold:-90}
    [[ -f "${huawei_wmi_charge_control_path}" ]] && echo "${start_charge_threshold} ${stop_charge_threshold}" > "${huawei_wmi_charge_control_path}"

    # - Hibernate on low battery level    https://wiki.archlinux.org/title/Laptop#Hibernate_on_low_battery_level
    # Suspend the system when battery level drops to 6% or lower
    local l_battery_low_threshold=${l_battery_low_threshold:-6}
    [[ -d /etc/udev/rules.d/ ]] && echo -e "# Suspend the system when battery level drops to ${l_battery_low_threshold}% or lower\nSUBSYSTEM==\"power_supply\", ATTR{status}==\"Discharging\", ATTR{capacity}==\"[0-${l_battery_low_threshold}]\", RUN+=\"/usr/bin/systemctl hibernate\"" > /etc/udev/rules.d/99-lowbat.rules
    # udevadm monitor --property  # testing battery report discharge events

    # - Suspend and hibernate
    # https://wiki.archlinux.org/title/Power_management/Suspend_and_hibernate
    # https://wiki.archlinux.org/title/Power_management#Suspend_and_hibernate
    # In order to use hibernation, you need to create a swap partition or file.
    # Using a swap file instead of a swap partition requires an additional kernel parameter resume_offset=swap_file_offset. The value of swap_file_offset can be obtained by running filefrag -v swap_file, the output is in a table format and the required value is located in the first row of the physical_offset column.
    # - 2-1 kernel parameters
    # /dev/nvme0n1p7: UUID="e8fb1a17-9ebd-486c-b00f-8f85474dad66" TYPE="swap" PARTLABEL="extended" PARTUUID="654dd89b-77d5-4ecb-b11b-6dd5c125b436"
    # grub_default_path='/etc/default/grub'
    # if [[ -f "${grub_default_path}" && -z $(sed -r -n '/GRUB_CMDLINE_LINUX_DEFAULT=/{/resume=/{p}}' "${grub_default_path}") ]]; then
    #     swap_partition_uuid=$(blkid | sed -r -n '/swap/{s@.*UUID="([^"]+)".*$@\1@g;p}')
    #     [[ -n "${swap_partition_uuid}" ]] && sed -r -i '/GRUB_CMDLINE_LINUX_DEFAULT=/{s@([^=]+="[^"]+)(.*)@\1 resume=UUID='"${swap_partition_uuid}"'\2@g;}' "${grub_default_path}"
    # fi

    swap_partition_uuid=${swap_partition_uuid:-}
    swap_partition_uuid=$(blkid | sed -r -n '/type="swap"/I{s@.*UUID="([^"]+)".*@\1@g;p}' | sed '$!d')  # blkid need root permission
    [[ -z "${swap_partition_uuid}" ]] && swap_partition_uuid=$(blkid -o full | sed -r -n '/type="swap"/I{s@.*UUID="([^"]+)".*@\1@g;p}' | sed '$!d')
    [[ -z "${swap_partition_uuid}" ]] && swap_partition_uuid=$(blkid -o list | sed -r -n '/[[:space:]]+swap[[:space:]]+/{s@[[:space:]]*$@@g;s@.*[[:space:]]+([^[:space:]]+)@\1@g;p}' | sed '$!d')

    [[ -n "${swap_partition_uuid}" ]] && fn_ConfigForGRUB 'GRUB_CMDLINE_LINUX_DEFAULT' "resume=UUID=${swap_partition_uuid}"
    
    # - 2-2 configure the initramfs
    # https://wiki.archlinux.org/title/Power_management/Suspend_and_hibernate#Configure_the_initramfs
    # Whether by label or by UUID, the swap partition is referred to with a udev device node, so the 'resume' hook must go after the 'udev' hook.
    # HOOKS=(base udev autodetect modconf block filesystems keyboard fsck)
    # mkinitcpio_conf_path='/etc/mkinitcpio.conf'
    fn_ConfigForMkinitcpio 'HOOKS' 'resume'

    # /etc/systemd/sleep.conf

    # - Audio
    # https://wiki.archlinux.org/title/Power_management#Audio
    [[ -d /etc/modprobe.d/ && -n $(lsmod | sed -r -n '/^snd_hda_intel/{p}') ]] && echo "options snd_hda_intel power_save=1" > /etc/modprobe.d/audio_powersave.conf

    # - Bluetooth    https://wiki.archlinux.org/title/Power_management#Bluetooth
    # -  2-1 To turn off bluetooth only temporarily, use rfkill:     rfkill block bluetooth
    [[ -d /etc/udev/rules.d/ ]] && echo -e "# disable bluetooth\nSUBSYSTEM==\"rfkill\", ATTR{type}==\"bluetooth\", ATTR{state}=\"0\"" > /etc/udev/rules.d/50-bluetooth.rules
    # -  2-2 To disable bluetooth completely, blacklist the 'btusb' and 'bluetooth' modules.
    # https://wiki.archlinux.org/title/Kernel_module#Blacklisting
    # echo -e "blacklist btusb\nblacklist bluetooth" > /etc/modprobe.d/nobluetooth.conf

    # Web camera    https://wiki.archlinux.org/title/Power_management#Web_camera
    # If you will not use integrated web camera then blacklist the 'uvcvideo' module.
    # echo -e "blacklist uvcvideo" > /etc/modprobe.d/nocamera.conf

    # Kernel parameters
    # https://wiki.archlinux.org/title/Power_management#Kernel_parameters
    # - Disabling NMI watchdog    # The NMI watchdog is a debugging feature to catch hardware hangs that cause a kernel panic. On some systems it can generate a lot of interrupts, causing a noticeable increase in power usage
    [[ -d /etc/sysctl.d/ ]] && echo -e "kernel.nmi_watchdog = 0" > /etc/sysctl.d/disable_watchdog.conf
    # - Writeback Time    # Increasing the virtual memory dirty writeback time helps to aggregate disk I/O together, thus reducing spanned disk writes, and increasing power saving. To set the value to 60 seconds (default is 5 seconds)
    [[ -d /etc/sysctl.d/ ]] && echo -e "vm.dirty_writeback_centisecs = 6000" > /etc/sysctl.d/dirty.conf
    # - Laptop Mode    # See the kernel documentation on the laptop mode 'knob.' "A sensible value for the knob is 5 seconds."
    [[ -d /etc/sysctl.d/ ]] && echo -e "vm.laptop_mode = 5" > /etc/sysctl.d/laptop.conf

    # Intel wireless cards (iwlwifi)
    # Additional power saving functions of Intel wireless cards with 'iwlwifi' driver can be enabled by passing the correct parameters to the kernel module.
    # Keep in mind that these power saving options are experimental and can cause an unstable system.
    # On kernels < 5.4 you can use option d0i3_disable=0 , but it will probably decrease your maximum throughput. From https://wiki.archlinux.org/title/Power_management#Network_interfaces  System log prompt kernel: iwlwifi: unknown parameter 'd0i3_disable' ignored
    [[ -d /etc/modprobe.d/ && -n $(lsmod | sed -r -n '/^iwlwifi/{p}') ]] && echo -e "options iwlwifi power_save=1 uapsd_disable=0\noptions iwldvm force_cam=0" > /etc/modprobe.d/iwlwifi.conf

    # https://wiki.archlinux.org/title/Power_management#Bus_power_management
    # Active-state power management (ASPM) is a power management mechanism for PCI Express devices to garner power savings while otherwise in a fully active state.
    # https://wiki.archlinux.org/title/Power_management#Active_State_Power_Management
    fn_ConfigForGRUB 'GRUB_CMDLINE_LINUX_DEFAULT' "pcie_aspm=force"

    # PCI Runtime Power Management

    # USB autosuspend https://wiki.archlinux.org/title/Power_management#USB_autosuspend
    # blacklist devices that are not working with USB autosuspend and enable it for all other devices:
    [[ -d /etc/udev/rules.d/ ]] && echo -e "# blacklist for usb autosuspend\nACTION==\"add\", SUBSYSTEM==\"usb\", ATTR{idVendor}==\"05c6\", ATTR{idProduct}==\"9205\", GOTO=\"power_usb_rules_end\"\n\nACTION==\"add\", SUBSYSTEM==\"usb\", TEST==\"power/control\", ATTR{power/control}=\"auto\"\nLABEL=\"power_usb_rules_end\"" > /etc/udev/rules.d/50-usb_power_save.rules


    # Watchdogs
    # https://wiki.archlinux.org/title/Improving_performance#Watchdogs
    # check the new configuration 'cat /proc/sys/kernel/watchdog' or 'wdctl'
    # - To disable watchdog timers (both software and hardware), append 'nowatchdog' to your boot parameters. Additionally disabling watchdog timers increases performance and lowers power consumption.
    # fn_ConfigForGRUB 'GRUB_CMDLINE_LINUX_DEFAULT' 'nowatchdog'
    # - After you disabled watchdogs, you can optionally avoid the loading of the module responsible of the hardware watchdog, too. Do it by blacklisting the related module, e.g. 'iTCO_wdt'.
    [[ -d /etc/modprobe.d/ && -n $(lsmod | sed -r -n '/^iTCO_wdt/{p}') ]] && echo -e "# black Watchdogs\nblacklist iTCO_wdt" > /etc/modprobe.d/nowatchdog.conf
    # Either action will speed up your boot and shutdown, because one less module is loaded.

}


fn_AfterChroot(){
    timeout 6 ping -c 2 google.com # timeout time 8s

    fnAC_TimezoneLocaleHostname
    fnAC_UserOperation
    fnAC_PackageManager # after normal user create
    fnAC_GRUB
    fnAC_Grahpics
    fnAC_DesktopEnvrionment

    fnAC_EssentialPackages
    fnAC_SystemService

    fnAC_HiDPIScreen
    fnAC_PowerManagement

    fnAC_NetworkManagement # make it last, otherwise the network connection will be disruptted

    # regenerate iniexfat-utils tramfs for /etc/mkinitcpio.conf settings
    grub-mkconfig -o /boot/grub/grub.cfg

    # Setup basic user directories (Documents, Pictures, etc...)
    # pacman -S --noconfirm xdg-user-dirs
    # xdg-user-dirs-update

    # chage -d0 "${normal_user_default}"  # new created user have to change passwd when first login, it may be fail to login if only use i3
    chown -R "${normal_user_default}" "${normal_user_home}"
    chgrp -R users "${normal_user_home}"
    # echo -e "exit\numount -R /mnt\nreboot"
}


#########  3 Executing Process  #########
fn_Main(){
    shell_name=$SHELL
    fn_PreparationOperation

    if [[ $shell_name =~ zsh$ ]]; then
        fn_BeforeChroot
        # cp script to /mnt
        script_path="$(readlink -f "$0")"
        cp "${script_path}" /mnt
        # https://wiki.archlinux.org/title/chroot#Using_arch-chroot
        arch-chroot /mnt /bin/bash "./${script_path##*/}" -u "${normal_user_default}" -H "${hostname_default}" -d "${desktop_environment_choose}" -m "${multilib_enable}" -g "${graphic_discrete_card_enable}" -k "${kernel_version_choose}" -c "${custom_operation_enable}"
        rm "/mnt/${script_path##*/}"
        # umount -R /mnt
    elif [[ $shell_name =~ bash$ ]]; then
        # arch-chroot /mnt   # Change root into the new system
        fn_AfterChroot
    fi
}

fn_Main


# Script End