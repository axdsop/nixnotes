# Kali Linux Operation Notes

[Kali Linux][kali-linux] is a Debian-based Linux distribution aimed at advanced Penetration Testing and Security Auditing. It is developed, funded and maintained by [Offensive Security][offensive-security], and was released on the 13th March, 2013 as a complete, top-to-bottom rebuild of BackTrack Linux, adhering completely to Debian development standards. It contains several hundred tools which are geared towards various information security tasks, such as Penetration Testing, Security research, Computer Forensics and Reverse Engineering. -- from [What is Kali Linux?](https://www.kali.org/docs/introduction/what-is-kali-linux/)

Official documentation sees page [Kali Docs](https://www.kali.org/docs/).

## Default Credentials

As of Kali Linux [2020.1](https://www.kali.org/releases/kali-linux-2020-1-release/), we do [not use root user](https://www.kali.org/news/kali-default-non-root-user/ "Kali Default Non-Root User") by default (root/toor). You can switch to root using the command `sudo bash`. See documentation [All about sudo](https://www.kali.org/docs/general-use/sudo/).

Officiall documentation [Kali Linux Default Passwords](https://www.kali.org/docs/introduction/kali-linux-default-passwords/) is deprecated. Latest version is [Kali's Default Credentials](https://www.kali.org/docs/introduction/default-credentials/).

Both *User* and *Password* are `kali`.


## Operation Notes

* [Getting Kali Linux](./Notes/1-0_GettingKaliLinux.md)


## Change Log

* May 13, 2020 Wed 09:20 ET
  * first draft


[kali-linux]:https://www.kali.org/ "Kali Linux | Penetration Testing and Ethical Hacking Linux Distribution"
[offensive-security]:https://www.offensive-security.com/ "Infosec Training and Penetration Testing"

<!-- End -->
