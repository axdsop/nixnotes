# Getting Kali Linux

## TOC

1. [Official Repositories](#official-repositories)  
1.1 [Image Types](#image-types)  
2. [Downloading](#downloading)  
2.1 [Downloading Links](#downloading-links)  
3. [Image Verification](#image-verification)  
3.1 [PGP Signature Verifying](#pgp-signature-verifying)  
3.2 [Hash Digest Verfication](#hash-digest-verfication)  
4. [Bootable Flash Drive](#bootable-flash-drive)  
5. [Installation](#installation)  
6. [Change Log](#change-log)  


## Official Repositories

[Official Kali Linux Mirrors](https://www.kali.org/docs/community/kali-linux-mirrors/) says [Kali Linux][kali-linux] has two repositories, which are mirrored world-wide:

Site|Mirror List|Type
---|---|---
[http.kali.org](https://http.kali.org/)|[Princeton](https://mirror.math.princeton.edu/pub/kali/)|the main package repository
[cdimage.kali.org](https://cdimage.kali.org/)|[Princeton](https://mirror.math.princeton.edu/pub/kali-images/)|the repository of pre-built Kali ISO images.

### Image Types

From [Which image to choose](https://www.kali.org/docs/installation/)

Type|Default packages|Network connections|Live System|Note
---|---|---|---|---
Installer|contain|support offline installation|No|just an installer image
NetInstaller|note contian|requires a network connection|No|just an installer image
Live|contain|depend|Yes|can't choose between desktop environments or to specify additional packages to install.


## Downloading

### Downloading Links

Latest released images see url <https://cdimage.kali.org/current/>

File|Date|Size
---|---|---
[SHA1SUMS](https://cdimage.kali.org/kali-2020.3/SHA1SUMS)|2020-08-18 14:31|483
[SHA1SUMS.gpg](https://cdimage.kali.org/kali-2020.3/SHA1SUMS.gpg)|2020-08-18 14:31|833
[SHA256SUMS](https://cdimage.kali.org/kali-2020.3/SHA256SUMS)|2020-08-18 14:31|627
[SHA256SUMS.gpg](https://cdimage.kali.org/kali-2020.3/SHA256SUMS.gpg)|2020-08-18 14:31|833
[kali-linux-2020.3-installer-amd64.iso](https://cdimage.kali.org/kali-2020.3/kali-linux-2020.3-installer-amd64.iso)|2020-07-28 20:30|3.7G
[kali-linux-2020.3-installer-i386.iso](https://cdimage.kali.org/kali-2020.3/kali-linux-2020.3-installer-i386.iso)|2020-07-28 20:53|3.2G
[kali-linux-2020.3-installer-netinst-amd64.iso](https://cdimage.kali.org/kali-2020.3/kali-linux-2020.3-installer-netinst-amd64.iso)|2020-07-28 20:31|430M
[kali-linux-2020.3-installer-netinst-i386.iso](https://cdimage.kali.org/kali-2020.3/kali-linux-2020.3-installer-netinst-i386.iso)|2020-07-28 20:55|435M
[kali-linux-2020.3-live-amd64.iso](https://cdimage.kali.org/kali-2020.3/kali-linux-2020.3-live-amd64.iso)|2020-07-28 20:26|2.9G
[kali-linux-2020.3-live-i386.iso](https://cdimage.kali.org/kali-2020.3/kali-linux-2020.3-live-i386.iso)|2020-07-28 20:49|2.6G


Your can use the following commands to extract latest released version info.

```bash
official_download_site='https://cdimage.kali.org'

download_method='wget -qO-'
[[ -n $(which curl 2> /dev/null || command -v curl 2> /dev/null) ]] && download_method='curl -fsSL'

current_release_version=$(${download_method} "${official_download_site}/current/SHA1SUMS" | sed -r -n '/kali-/{s@^[^[:space:]]+[[:space:]]*[^[:digit:]]+([^-]+).*@\1@g;p;q}' )  # 2020.3
current_release_download_site=$(${download_method} "${official_download_site}" | sed -r -n '/<td>.*href=.*'"${current_release_version}"'\/?/{s@.*href="([^"]+).*@'"${official_download_site}"'/\1@g;p}')

release_info_list=$($download_method "${current_release_download_site}" | sed -r -n '/<td/{s@<(tr|td|img)[^>]*>@@g;s@[[:space:]]*<\/t(r|d)>[[:space:]]*@|@g;s@\&nbsp\;@@g;s@\|?<a.*href="([^"]+)">([^<]+)[^\|]*@\1|'"${current_release_download_site}"'\2@g;/^\//d;s@\|{2,}@@g;p}')

echo "${release_info_list}"

# Markdown output
awk -F\| 'BEGIN{OFS="|";print "File|Date|Size\n---|---|---"}{printf("[%s](%s)|%s|%s\n",$1,$2,$3,$4)}' <<< "${release_info_list}"
```


## Image Verification

After downloading images and SHA256SUMS, we need to verify if these files is officially released by [Kali Linux][kali-linux].

[Kali Linux][kali-linux]’s official GPG key url <https://archive.kali.org/archive-key.asc>.


### PGP Signature Verifying

Official documentation [Downloading Kali Linux](https://www.kali.org/docs/introduction/download-official-kali-linux-images/) provides methods to verify downloaded Kali image.

Here using personal custom function [GPGSignatureVerification](/CyberSecurity/GnuPG/Notes/2-1_UsageExample.md#package-signature-verifying) to verify PGP signature.

Usage

```bash
sha_digest_name='SHA256SUMS' # kali-linux-2020.3-SHA256SUMS

# fn_GPGSignatureVerification 'ISOPATH' 'SIGPATH' 'GPGPATH'
fn_GPGSignatureVerification "${sha_digest_name}"{,.gpg} 'https://www.kali.org/archive-key.asc'
```

Output

```bash
Importing PGP key
gpg: keybox '/tmp/3YhyPh8o/gpg_import.gpg' created
gpg: key ED444FF07D8D0BF6: public key "Kali Linux Repository <devel@kali.org>" imported
gpg: Total number processed: 1
gpg:               imported: 1

Verifying signature
gpg: Signature made Tue 18 Aug 2020 10:31:15 AM EDT
gpg:                using RSA key 44C6513A8E4FB3D30875F758ED444FF07D8D0BF6
gpg: Good signature from "Kali Linux Repository <devel@kali.org>" [unknown]
gpg: WARNING: This key is not certified with a trusted signature!
gpg:          There is no indication that the signature belongs to the owner.
Primary key fingerprint: 44C6 513A 8E4F B3D3 0875  F758 ED44 4FF0 7D8D 0BF6

Temporary directory /tmp/3YhyPh8o has been removed.
```

**Good signature from "Kali Linux Repository <devel@kali.org>"** indicates file *SHA256SUMS* is officially released by [Kali Linux][kali-linux]. Then we can use this file to verify images' hash digest (SHA256).

### Hash Digest Verfication

Assuming these files saved in same directory.

```bash
target_name='kali-linux-2020.3-installer-amd64.iso'

# method 1
sed -r -n '/'"${target_name}"'/{p}' SHA256SUMS | shasum -a 256 -c

# method 2
grep "${target_name}" SHA256SUMS | shasum -a 256 -c
```

Output

```bash
kali-linux-2020.3-installer-amd64.iso: OK
```


## Bootable Flash Drive

Creating the bootable flash drive with the verified ISO image.

Official documentation [Making a Kali Bootable USB Drive](https://www.kali.org/docs/usb/kali-linux-live-usb-install/). For [*nix](https://en.wikipedia.org/wiki/Unix-like), just using command `dd`

```bash
iso_path="$HOME/Downloads/kali-linux-2020.3-installer-amd64.iso"
usb_path='/dev/sdX' # via command 'fdisk -l', e.g. /dev/sda, /dev/sdb

dd if="${iso_path}" of="${usb_path}" bs=4M status=progress oflag=sync && sync
# dd if=kali-linux-2020.3-installer-amd64.iso of=/dev/sda bs=4M status=progress oflag=sync
```

If you wanna add persistenct to "live" USB drive, just read other documentations listed in section [USB](https://www.kali.org/docs/usb/).


## Installation

See documentation section [Installation](https://www.kali.org/docs/installation/).

Installation Prerequisites ([Kali Linux installation requirements](https://www.kali.org/docs/introduction/installation-requirements/))

* A minimum of 20 GB disk space for the Kali Linux install.
* RAM for i386 and amd64 architectures, minimum: 1GB, recommended: 2GB or more.
* CD-DVD Drive / USB boot support

[Kali Linux][kali-linux] installation procedure see documentation [Kali Linux Installation Procedure](https://www.kali.org/docs/installation/kali-linux-hard-disk-install/) which provides screen snapshots.


## Change Log

* May 13, 2020 Wed 13:49 ET
  * first draft
* Oct 17, 2020 Sat 10:39 ET
  * Update latest release info detect method


[kali-linux]:https://www.kali.org/ "Kali Linux | Penetration Testing and Ethical Hacking Linux Distribution"

<!-- End -->