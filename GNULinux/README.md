# GNU/Linux Distros Introduction & Setting up


There are so many Linux [distributions][distrowatch], but the most popular of them are belong to the following familaries: [Red Hat][rhel] ([CentOS][centos]/[Fedora][fedora]), [Debian][debian] ([Ubuntu][ubuntu]), [SUSE][sles] ([openSUSE][opensuse]), [Arch Linux][archlinux] ([Manjaro][manjaro]).

**Note**: Dotfiles have been moved to another project **[Unix-like Dotfiles](https://gitlab.com/axdsop/nix-dotfiles)** on Oct 08, 2020.


## Distros Official Info

Distribution|Documentation<br>Download Page<br>Mirror Sites|Release Notes|Life Cycle|Security
---|---|---|---|---
[Red Hat][rhel] |[doc](https://access.redhat.com/documentation/en-US/)<br>[download page](https://access.redhat.com/downloads/)|[release1](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/)/ [release2](https://access.redhat.com/articles/3078)|[cycle](https://access.redhat.com/support/policy/updates/errata/)|[sec1](https://access.redhat.com/security/security-updates/#/security-advisories) / [sec channel](https://www.redhat.com/en/blog/channel/security) / [sec data](https://www.redhat.com/security/data/metrics/) / [oval](https://www.redhat.com/security/data/oval/)
[CentOS][centos] |[doc](https://wiki.centos.org/Documentation)<br>[download page](https://www.centos.org/download/)<br>[mirror sites](https://www.centos.org/download/mirrors/)|[release](https://wiki.centos.org/Manuals/ReleaseNotes) / [announce archives](https://lists.centos.org/pipermail/centos-announce/)|[cycle](https://access.redhat.com/support/policy/updates/errata/)||
[Fedora][fedora] |[doc](https://docs.fedoraproject.org/en-US/docs/) / [Fedora magazine](https://fedoramagazine.org/)<br>[download page](https://getfedora.org/)<br>[mirror sites](https://admin.fedoraproject.org/mirrormanager/mirrors)|[release](https://docs.fedoraproject.org/en-US/fedora/rawhide/release-notes/)|[cycle1](https://fedoraproject.org/wiki/End_of_life?rd=LifeCycle/EOL) / [cycle2](https://fedoraproject.org/wiki/Fedora_Release_Life_Cycle)||
[Debian][debian] |[doc](https://www.debian.org/doc/)<br>[download page](https://www.debian.org/distrib/)<br>[mirror sites](https://www.debian.org/mirror/list)|[release1](https://www.debian.org/releases/) / [release2](https://wiki.debian.org/DebianReleases)|[cycle](https://wiki.debian.org/LTS)|[tracker](https://security-tracker.debian.org/tracker/) / [recent advisories](https://www.debian.org/security/#DSAS)<br>[sec announce archive](https://lists.debian.org/debian-security-announce/) / [oval](https://www.debian.org/security/oval/)|
[Ubuntu][ubuntu] |[doc1](https://help.ubuntu.com/) / [doc2](https://docs.ubuntu.com/)<br>[download page](https://ubuntu.com/download)<br>[mirror sites](https://launchpad.net/ubuntu/+cdmirrors)|[release](https://wiki.ubuntu.com/Releases) / [announce archive](https://lists.ubuntu.com/archives/ubuntu-announce/)|[cycle](https://ubuntu.com/about/release-cycle)|[sec](https://usn.ubuntu.com/) / [tracker](https://people.canonical.com/~ubuntu-security/cve/)<br>[sec announce archive](https://lists.ubuntu.com/archives/ubuntu-security-announce/) / [oval](https://people.canonical.com/~ubuntu-security/oval/)|
[SUSE][sles] |[doc](https://documentation.suse.com/)<br>[download page](https://www.suse.com/download-linux/)|[release](https://www.suse.com/releasenotes/)|[cycle](https://www.suse.com/lifecycle/)| [sec](https://www.suse.com/support/security/)<br>[sec announce archive](http://lists.suse.com/pipermail/sle-security-updates/)/[oval](http://ftp.suse.com/pub/projects/security/oval/)|
[openSUSE][opensuse] |[doc1](https://en.opensuse.org/Portal:Documentation) / [doc2](https://doc.opensuse.org/)<br>[download page](https://software.opensuse.org/)<br>[mirror sites](https://mirrors.opensuse.org/)|[release](https://en.opensuse.org/openSUSE:Release_Notes)|[cycle](https://en.opensuse.org/Lifetime)|[sec](https://en.opensuse.org/Portal:Security) / [bug list](https://bugzilla.opensuse.org/buglist.cgi?bug_status=NEW&bug_status=ASSIGNEDcmdtype%3Ddoit&query_format=advanced&short_desc=vul-&short_desc_type=allwordssubstr)<br>[sec announce archive](http://lists.opensuse.org/opensuse-security-announce/)
[Arch Linux][archlinux] |[doc](https://wiki.archlinux.org/)<br>[download page](https://www.archlinux.org/download/)<br>[mirror sites](https://www.archlinux.org/mirrors/)||[cycle](https://www.archlinux.org/releng/releases/)|[sec](https://security.archlinux.org/)
[Manjaro][manjaro] |[doc](https://manjaro.org/support/userguide/)<br>[download page](https://manjaro.org/download/)<br>[mirror sites](https://repo.manjaro.org/)|[release](https://manjaro.org/news/)||
[Gentoo][gentoo] | [doc](https://www.gentoo.org/support/documentation/)<br>[download page](https://www.gentoo.org/downloads/)<br>[mirror sites](https://www.gentoo.org/downloads/mirrors/)|[release](https://wiki.gentoo.org/wiki/Project:RelEng)||[sec](https://www.gentoo.org/support/security/) / [sec advisories](https://security.gentoo.org/)|


## Chnage Log

* Apr 12, 2020 Sun 21:00 ET
  * first draft
* May 13, 2020 Wed 10:40 ET
  * Add mirror sites url


[distrowatch]:https://distrowatch.com " Put the fun back into computing. Use Linux, BSD."
[rhel]:https://www.redhat.com/en "Red Hat - We make open source technologies for the enterprise"
[centos]:https://www.centos.org/ "CentOS"
[fedora]:https://getfedora.org/ "Fedora"
[debian]:https://www.debian.org/ "Debian -- The Universal Operating System"
[ubuntu]:https://ubuntu.com/ "The leading operating system for PCs, IoT devices, servers and the cloud"
[sles]:https://www.suse.com/ "Open Source Solutions for Enterprise Servers, Cloud & Storage"
[opensuse]:https://www.opensuse.org/ "The makers' choice for sysadmins, developers and desktop users"
[archlinux]:https://www.archlinux.org/ "Arch Linux"
[manjaro]:https://manjaro.org/ "Manjaro - enjoy the simplicity"
[gentoo]:https://www.gentoo.org/ "Gentoo Linux"


<!-- End -->